<?php 
$employee_password_authrozie = $this->Employee->get_Logged_SessionData('EMPLOYEE_PSSWORD_AUTHROZIE');
$get_page_name = $this->uri->segment(2);   
if($get_page_name!='login') 
{
	$current_siteurl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];			
	$this->session->set_userdata('CURRENT_SITEURL',$current_siteurl);
	if(!$employee_password_authrozie) { redirect('home/login'); }  
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('company').' -- '.lang('common_powered_by').' PHP SYNTAX PLC( For any Problem Call +255 232 402465, +255 782566633)' ?></title>
	 <link type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" rel="shortcut icon">  
	<?php	foreach(get_css_files() as $css_file)
	{	?>
		<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().$css_file['path'].'?'.APPLICATION_VERSION;?>" media="<?php echo $css_file['media'];?>" />	<?php	}	?>	
	<script type="text/javascript">
	var SITE_URL= "<?php echo site_url(); ?>";
	<?php if(isset($get_page_name) && $get_page_name!='login' && $this->config->item('configuration_power_options')==1){?> 
	setTimeout(function () 
	{  var previous_time = '<?php echo $this->Employee->get_Logged_SessionData('EMPLOYEE_START_LOGIN_TIME');?>';
	   var currentdate = '<?php echo time();?>';	 
		window.location.href = "<?php echo base_url();?>/home/login"; 	
	}, '<?php echo ($this->config->item('DEFAULT_MEMBERS_LOGOUT_TIME') * 1000);?>');
	<?php }?>
	</script>
	<?php
	foreach(get_js_files() as $js_file)
	{	?>
		<script src="<?php echo base_url().$js_file['path'].'?'.APPLICATION_VERSION;?>" type="text/javascript" language="javascript" charset="UTF-8"></script>	<?php	}	?>	
	<script type="text/javascript">	 
	Date.format = '<?php echo get_js_date_format(); ?>';
	$.ajaxSetup ({ 	cache: false	});
	</script>	
<script type="text/javascript" src="<?php echo base_url(); ?>js/ajaxupload.3.6.js"></script>
<style type="text/css">html {    overflow: auto;}</style>
</head>
<body>
<div id="menubar">
	<table id="menubar_container">
		<tr id="menubar_navigation">
			<td class="menu_item menu_item_home">
				<div style="margin-top:16px;"><a href="<?php echo site_url(); ?>"><?php echo lang('common_home_page_logo_title'); ?></a></div>
			</td>
			<?php
			foreach($allowed_modules->result() as $module)
			{
			 if($module->module_id=="payments" || $module->module_id=="history" || $module->module_id=="investigation" || $module->module_id=="medicine" || $module->module_id=="ipd_in_patient" || $module->module_id=="diabetes_care" || $module->module_id=="uss_xrays" || $module->module_id=="dead_body" 
			 || $module->module_id=="dental" || $module->module_id=="eyeclinic" || $module->module_id=="specialist" || $module->module_id=="otherservice" || $module->module_id=="ministry_reporting" || $module->module_id=="laboratory" || $module->module_id=="nhif_patient" || $module->module_id=="rch" 
			 || $module->module_id=="tools" || $module->module_id=="born_management"  || $module->module_id=="bed_board" || $module->module_id=="bag_reports" || $module->module_id=="mailbox" || $module->module_id=="notice_board" || $module->module_id=="doctor_forum" || $module->module_id=="stores" || $module->module_id=="appointment") { } else { 			?>
			<td class="menu_item menu_item_<?php echo $module->module_id;?>">
				<a href="<?php echo site_url("$module->module_id");?>"><?php echo lang("module_".$module->module_id) ?></a>
			</td>
			<?php
			}
			}
			?>
		</tr>	</table>
</div>
<div id="content_area_wrapper">
<div id="content_area"><?php if(isset($controller_name) && $controller_name!='' && $controller_name=='home'){?><script src="<?php echo base_url(); ?>js/flowplayer/flowplayer-3.js"></script><script src="<?php echo base_url(); ?>js/flowplayer/flowplayer.js"></script> <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/flowplayer/playlist.css"><style>	/* container has a background image */a.player {		display:block;	width:500px;	height:340px;	text-align:center;	color:#fff;	text-decoration:none;	cursor:pointer;	background:#000 url(http://static.flowplayer.org/img/global/gradient/h500.png) repeat-x 0 0;	background:-moz-linear-gradient(top, rgba(55, 102, 152, 0.9), rgba(6, 6, 6, 0.9));	-moz-box-shadow:0 0 40px rgba(100, 118, 173, 0.5);}a.player:hover {	background:-moz-linear-gradient(center top, rgba(73, 122, 173, 0.898), rgba(6, 6, 6, 0.898));	}/* splash image */a.player img {	margin-top:125px;	border:0;	}	a.player {		margin-top:0px;			}</style><?php }?>