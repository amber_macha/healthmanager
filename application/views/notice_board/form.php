<style>fieldset div.field_row { font-weight: normal;}
fieldset div.field_row label {width: 135px;}
fieldset div.field_row div.form_field select {width:110px;}
#reject_reason_sec{display:<?php if($item_info->status==4 || $item_info->reject_reason!='')echo 'block';else echo 'none';?>;}
</style>
<?php 
if($view_type=='show' || $item_info->status==4){ $form_input ="form_input_readonly"; $form_textarea = 'form_textarea_readonly'; $disabled ='disabled="disabled"';}
else { $form_input='form_input'; $form_textarea = 'form_textarea'; $disabled =''; }
echo form_open('notice_board/save/'.$item_info->id,array('id'=>'notice_form'));
?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("notice_board_form_title"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('notice_board_subject').':', 'notice_board_subject',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'notice_board_subject','id'=>'notice_board_subject','value'=>$item_info->notice_board_subject,'style'=>'width:352px'));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('notice_board_description').':', 'description',array('class'=>'required')); ?>
	<div class='form_field'><?php echo $form_textarea(array('name'=>'notice_board_description','id'=>'notice_board_description','rows'=>10,'cols'=>60,'value'=>$item_info->notice_board_description));?></div>
</div>
	
<div class="field_row clearfix">	
<?php if($item_info->from_date!='' && $item_info->from_date!='0000-00-00') { $from_date = date('d/m/Y',strtotime($item_info->from_date));} else $from_date='';
	echo form_label(lang('notice_board_from_date').':', 'from_date',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'from_date','id'=>'from_date','value'=>$from_date,'style'=>'width:98px'));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php if($item_info->to_date!='' && $item_info->to_date!='0000-00-00') { $to_date = date('d/m/Y',strtotime($item_info->to_date));} else $to_date='';
	echo form_label(lang('notice_board_to_date').':', 'to_date',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'to_date','id'=>'to_date','value'=>$to_date,'style'=>'width:98px'));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('notice_board_status').':', 'reports_status',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('notice_status',array('0'=>lang('common_status_active'),'1'=>lang('common_status_inactive')),$item_info->status, $disabled);?>	
	</div>
</div>

<?php if($view_type=='edit' && $item_info->status!=4){ echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));}
	else { echo form_buttons(array('name'=>'save','value'=>lang('common_close'),'class'=>'delete-button','onclick'=>'close_form();'));} ?>
</fieldset>
<?php echo form_close();?>


<script type='text/javascript'>
function getStatus(status_type)
{
  if(status_type==3) { $('#reject_reason_sec').show(); } 
  else { $('#reject_reason_sec').hide(); $('#reject_reason').val(''); }
}


$(document).ready(function()
{	
	$('#from_date').datePicker({startDate: '01-01-2015'}); 
	$('#to_date').datePicker({startDate: '01-01-2015'}); 
	var submitting = false;
	$('#notice_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			notice_board_subject: "required", 
    		notice_board_description: "required", 
			from_date: "required",	
			to_date: "required",				
   		},
		messages: 
		{
     		notice_board_subject: <?php echo json_encode(lang('notice_board_subject_required')); ?>,
     		notice_board_description: <?php echo json_encode(lang('notice_board_description_required')); ?>,
			from_date: <?php echo json_encode(lang('notice_board_from_date_required')); ?>,
			to_date: <?php echo json_encode(lang('notice_board_to_date_required')); ?>,					
		}
	});
});
</script>