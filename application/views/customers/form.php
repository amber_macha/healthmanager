<style>#spinner004{display:none;}</style>
<?php echo form_open('customers/save/'.$person_info->person_id,array('id'=>'customer_form','enctype'=>'multipart/form-data', 'onsubmit'=>'return save_CustomerData();'));?>
<?php if($person_info->registration_type=='1') $RapidSection = 'style="display:none"'; else $RapidSection = 'style="display:block"';?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
<?php $this->load->view("customers/form_basic_info_patient"); ?>

<div class="field_row clearfix">	
<?php echo form_label(lang('patient_type'). ':', 'patient_type',array('class'=>'required wide')); ?>
	<div class='form_field'>
			<select name="patient_type" id="patient_type" class="valid" style="width:193px;">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='0') echo 'selected';?>><?php echo lang('normal_patient'); ?></option>
			<option value="1" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='1') echo 'selected';?>><?php echo lang('private_patient'); ?></option>
			<option value="2" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='2') echo 'selected';?>><?php echo lang('fast_track_patient'); ?></option>
			</select>
	</div>
</div>

<div class="field_row clearfix RapidSection" <?php echo $RapidSection;?>>	
<?php echo form_label('Ten Cell Leader:', 'leader'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'leader',
		'id'=>'leader',
		'value'=>$person_info->leader)
	);?>
	</div>
</div>
<div class="field_row clearfix RapidSection" <?php echo $RapidSection;?>>	
<?php echo form_label('Religion:', 'religion'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'religion',
		'id'=>'religion',
		'value'=>$person_info->religion,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>
<div class="field_row clearfix RapidSection" <?php echo $RapidSection;?>>	
<?php echo form_label('Next of Kin:', 'kin'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'kin',
		'id'=>'kin',
		'value'=>$person_info->kin)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Sex:', 'sex',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('sex',array(''=>'Select ','Male'=>'Male', 'Female'=>'Female'), $person_info->sex);?>	
	</div>
</div>

<div class="field_row clearfix">

<?php echo form_label('Age:', 'Age',array('class'=>'required wide')); ?>
	<div class='form_field'>	
	<?php echo lang('day');?> <select id="age_day" class="valid" name="age_day">
							  <option value=""><?php echo lang('day');?></option>
							  <?php foreach($days as $key=>$value){?>
							  <option value="<?php echo $key;?>" <?php if(isset($person_info->age_day) && $person_info->age_day!='' && $key==$person_info->age_day)echo 'selected'?>><?php echo $value;?></option>
							  <?php }?>
							</select>
   <?php echo lang('month');?>  <select id="age_month" class="valid" name="age_month">
   								<option value=""><?php echo lang('month');?></option>
								<option value="0" <?php if(isset($person_info->age_month) && $person_info->age_month!='' && $person_info->age_month==0)echo 'selected'?>>00</option>
								<?php foreach($months as $key=>$value){?>
								<option value="<?php echo $key;?>"<?php if(isset($person_info->age_month) && $person_info->age_month!='' && $key==$person_info->age_month)echo 'selected'?>><?php echo $key;?></option>
								<?php }?>
								</select>
		
  <?php echo lang('year');?><select id="age_year" class="valid" name="age_year" style="width:65px;">
							<option value=""><?php echo lang('year');?></option>
							<?php for($k=0;$k<150;$k++){?>
							<option value="<?php echo $k;?>"<?php if(isset($person_info->age) && $person_info->age!='' && $k==$person_info->age)echo 'selected'?>><?php echo $k;?></option>
							<?php }?>
							</select>	

	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label('Occupation:', 'company_name',array('class'=>'required wide')); ?>
	<div class='form_field'>
			<select name="occupation" id="occupation" class="valid" onchange="getOccupationType(this.value)" style="width:193px;">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($OccupatiosData->num_rows()>0){
			foreach($OccupatiosData->result() as $rowdata){?>
			<option value="<?=$rowdata->occupation_id;?>" <?php if(isset($person_info->occupation) && $person_info->occupation==$rowdata->occupation_id) echo 'selected';?>><?php echo ucfirst($rowdata->occupation_name); ?></option>
			<?php }}?></select>
<img id="spinner005" src="<?php echo base_url();?>images/loading.gif">			
	</div>	
</div>

<div id="occupation_group_sec"><?php echo $this->savesetting->GetOccupationGroup($person_info->occupation,$person_info->occupation_registration_no); ?></div>

<?php if(isset($person_info->nationality) && $person_info->nationality!='') $nationality = $person_info->nationality; else $nationality = $this->config->item('default_country'); ?>
<div class="field_row clearfix">	
<?php echo form_label('Nationality:', 'nationality'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('nationality',$this->Customer->GetCountry(), $nationality ,'style="width:193px;"');?>	
	</div>
</div>


	<div class="field_row clearfix">	
	<?php echo form_label(lang('customers_account_number').':', 'account_number'); ?>
		<div class='form_field'>
		<?php if(isset($person_info->person_id) && $person_info->person_id!=''){
		echo form_input_readonly(array(
			'name'=>'account_number',
			'id'=>'account_number',
			'value'=>$person_info->account_number)
		);
		}else {
				echo form_input_readonly(array(
					'name'=>'account_number',
					'id'=>'account_number',
					'value'=>'' )
				);
			}	
		?>
		</div>
	</div> 
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('common_health_card_no').':', 'health_card_no'); ?>
		<div class='form_field'>
		<?php echo form_input(array('name'=>'health_card_no',
									'id'=>'health_card_no',
									'maxlength'=>'19',
									'onclick'=>'clicktohide(this);',
									'value'=>$person_info->health_card_no)
									);?>
									<img id="spinner004" src="<?php echo base_url();?>images/loading.gif">
			</div>
			<div id="get_validcard" class="get_valid" style="font-weight:normal;"></div>
	</div>

	<div class="field_row clearfix">	
	<label><?php echo lang('common_patient_image');?> </label>	
	<div class='form_field'> <input type="file" name="patient_image" id="patient_image"> </div>	
	<?php if(isset($person_info->patient_profile_image) && $person_info->patient_profile_image!='' && $person_info->patient_profile_image!='0'){?> <div class="patient_image-div"><img class="patient_image" src="<?php echo base_url();?>images/patient_images/<?='thumb_'.$person_info->patient_profile_image;?>"> </div><?php }?>	
	</div>	

<?php echo form_submit(array('value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
</fieldset>
<?php echo form_close();?>

<script type='text/javascript'>
function Check_Valid_HealthCard(health_card_no)
{	
	if(health_card_no==''){  $("#health_card_no").css("background", "#f2dede");$("#health_card_no").css("border","1px solid #FF0000");	return false; }				
	//var  authrizeUrl = "<?php echo base_url().'customers/check_validHealthCard/'.$person_info->person_id;?>"; 
	//alert('here u r');alert(authrizeUrl);
	$('#spinner004').show(); 
	$.ajax({
			url: "<?php echo base_url().'customers/check_validHealthCard/'.$person_info->person_id;?>/",
			type: 'post',
			data: 'health_card_no='+ health_card_no,
			dataType:'json',
			success: function(data)
			{ 	$('#spinner004').hide(); 
				if(data.success==false) 
				{  	$("#health_card_no").val('');
				    $('#get_validcard').removeClass('get_valid'); $('#get_validcard').addClass('get_invalid'); 				  
				    $('#get_validcard').html(data.message);					   
				}
				else 
				{ 	$('#get_validcard').removeClass('get_invalid'); $('#get_validcard').addClass('get_valid');
				    $('#get_validcard').html(data.message);					 													
				}					
			}
		});	
	return false;		
}

function getOccupationType(type)
{   
   var group_name = '<?php $person_info->occupation_registration_no;?>';  
   var GetDataURL ='<?php echo base_url();?>customers/getOccupationGroupData/';
   $('#spinner005').show();
   $.ajax({ 
		url: GetDataURL,
		type: 'post',
		data: 'occupation_id=' + type + '&group_name=' + group_name ,
		success: function(response) 
			{	$('#spinner005').hide();			
				$("#occupation_group_sec").html(response);  
			}
	 });
  if(type=='0'){ $('#pat_student_sec').show();$('#pat_employee_sec').hide();$('#pat_others_sec').hide(); }
  if(type=='1'){ $('#pat_employee_sec').show();$('#pat_student_sec').hide();$('#pat_others_sec').hide(); }
  if(type=='2'){ $('#pat_others_sec').show();$('#pat_student_sec').hide();$('#pat_employee_sec').hide(); }
}

function getPatientType(p_type)  
{  $('#membership_no').val(''); $('#member_card_no').val('');
   if(p_type==4) { $('#nhif_patient_field').show(); } else { $('#nhif_patient_field').hide();}
}
function Check_Valid_Customers(member_card_no)
{	
	if(member_card_no==''){  document.getElementById('member_card_no').style.background = "#FFFF00";  return false; }	
	var person_id  = '<?php echo $person_info->person_id;?>';			
	if(member_card_no!='' || member_card_no!= '0')
	{				
		var  authrizeUrl = 'https://verification.nhif.or.tz/NHIFService/breeze/Verification/GetCard?CardNo='+ member_card_no; 			
		$('#spinner003').show(); 
		$.ajax({
				url: authrizeUrl,
				dataType:'json',
				success: function(data)
				{  
				    $('#spinner003').hide(); 
					if(data.MembershipNo!='') 
					{  Check_Exist_Membership_NO(data.MembershipNo,person_id);					  
					   $('#get_valid').html('<?php echo lang('customers_valid_connection');?>');
					   $('#membership_no').val(data.MembershipNo); 
					   $('#first_name').val(data.FirstName); 
					   $('#last_name').val(data.LastName); 
					   $("#sex").val(data.Gender).change();
					}
					else 
					{ $('#get_valid').addClass('get_invalid');
					  $('#get_valid').html('<?php echo lang('customers_invalid_connection');?>');
					  $('#membership_no').val('');
					  $('#member_card_no').val(''); 					  
					  $('#first_name').val(''); 
					  $('#last_name').val(''); 
					  $("#sex").val('').change();
					  $('#member_card_no').style.background = "#FFFF00";														
					}					
				}
			});	
	}		
}

function Check_Exist_Membership_NO(member_code,person_id)
{  
  if(member_code!='')
   {    
    $.ajax({
			url: "<?php echo base_url();?>customers/Exist_Membership_No",
			type: 'post',
			data: 'member_code='+member_code+'&person_id='+person_id,
			dataType:'json',
			success: function(response) 
			{ 	
			   if(response.success)
			   {  $('#get_valid').addClass('get_invalid'); 
				  $('#get_valid').html(response.message);
				  $('#membership_no').val('');
				  $('#member_card_no').val(''); 					  
				  $('#first_name').val(''); 
				  $('#last_name').val(''); 
				  $("#sex").val('').change();				  
				  return false;
			   } 	
			  else return true;								 
			}    
		}); 
	}		
}


function getRegistrationType(R_Type)  
{  
   if(R_Type==1) { $('.RapidSection').hide(); $('#insurance_company').val('');  } 
   else { $('.RapidSection').show();}
}
	
//validation and submit handling
$(document).ready(function()
{   
	var submitting = false;
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{   tb_remove();
				if(response.account_number!='')
				{
					var searchURL = "<?php echo base_url().'customers/search/'?>";
					$('#search').val(response.account_number);
					do_search(true,function(){ $('#search').val();}); 
				}
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required",
			last_name: "required",
    		email: "email",
			patient_type: "required",
			sex: "required",
			//age_day: "required",
			//age_month: "required",
			age_year: "required",
			occupation: "required",			
   		},
		messages: 
		{
     		first_name: <?php echo json_encode(lang('common_first_name_required')); ?>,
     		last_name: <?php echo json_encode(lang('common_last_name_required')); ?>,
     		email: <?php echo json_encode(lang('common_email_invalid_format')); ?>,
			patient_type: <?php echo json_encode(lang('common_patient_type_required')); ?>,
			sex: <?php echo json_encode(lang('common_sex_required')); ?>,
			age_day: <?php echo json_encode(lang('common_age_day_required')); ?>,
			age_month: <?php echo json_encode(lang('common_age_month_required')); ?>,
			age_year: <?php echo json_encode(lang('common_age_year_required')); ?>,
			occupation: <?php echo json_encode(lang('common_occupation_required')); ?>,
		}
	});
});
</script>