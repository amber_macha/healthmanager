<?php $this->load->view("partial/header"); ?>
<link href="<?php echo base_url();?>js/calender/glDatePicker.default.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>js/calender/jquery.min.js"></script> 
<script src="<?php echo base_url();?>js/calender/glDatePicker.min.js"></script>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('patient_repeated_attendance'); ?>
		</td>
		
	</tr>
</table>
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				{?>
					<?php echo anchor("customers",$this->lang->line('view_customers_customer'),
						array('class'=>'none new', 'title'=>$this->lang->line('view_customers_customer')));
					?>					
				
				<?php }	?>				
			

			</div>
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
			
			
		<div id="table_holder">			
	<table class="tablesorter">
	
	<tr><th class="leftmost"></th></tr>
	
	<tr> <td width="90%" style="background-color: #ffffff; border-left: 1px solid #BBBBBB;">
	 <div class="attendance-loader"> <img id="searchspiner" src="<?php echo base_url();?>images/spinner_small.gif" style="display: none;"> </div>
	  <div id="messagediv" class="messagediv"></div>
			 <div style="width:100%; margin-top:50px;margin-bottom:100px; height: 280px;">			
				<div class="output"> 				
				<span style="margin-left:150px"><input id="mydate" type="text" gldp-id="mydate" value="" ></span>				
				<div gldp-el="mydate" style="width:500px; height:400px; position:absolute; margin-top:-35px; margin-left:-40px;"> </div>		
				</div> 	
			</div>
	
		</td>
	</tr>
	
	</table>
				
			 </div>
			
			</div>
			
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>


<script type="text/javascript">$('#mycal').glDatePicker();			
//http://glad.github.io/glDatePicker/
	$(window).load(function()
	{ 		
		   $('#mydate').glDatePicker(
			{ 
				showAlways: true,
				//cssName: 'darkneon',				
				selectedDate: new Date(<?php echo $year;?>, <?php echo $month;?>, <?php echo $day;?>),
				//selectedDate: new Date(2013, 10, 25),
				specialDates: [
				<?php if(isset($manage_report) && $manage_report!='' && $manage_report->num_rows()>0){
				foreach($manage_report->result() as $data){ 
				$attent_year = ''; $attent_month = ''; $attent_day = '';
				if($data->attendance_date!=''){
				$attendance = explode('-',$data->attendance_date);
				$attent_year = $attendance[0];  $attent_month = $attendance[1]-1; $attent_day = $attendance[2];
				
				?>
					{
						date: new Date(<?=$attent_year;?>, <?=$attent_month;?>, <?=$attent_day;?>)
					},
					<?php }}}?>
					
				],
				onClick: function(target, cell, date, data) 
				{					
					var month  = date.getMonth()+1;
					var attent_date = date.getFullYear() + '-' +	month + '-' +date.getDate();
					//alert(attent_date);
					//target.val(attent_date);
					// add ajax function 
					if (confirm('<?php echo lang('added_conformation_repeated_attendance');?>')) 
					 {	
						var searchURL ='<?php echo base_url().'customers/add_repeated_attendance/'.$patient_id;?>';
						$('#searchspiner').show();	
							$.ajax({
							url: searchURL,
							type: 'post',
							data: 'attent_date='+attent_date,
							success: function(data) 
							{ $('#searchspiner').hide(); 
							// alert(data);
							 if(data==0){ $('#messagediv').html('<?php echo lang('added_repeated_attendance_failed'); ?>'); return false; }
							 if(data==1){ $('#messagediv').html('<?php echo lang('added_repeated_attendance_exists'); ?>'); return false;}
							 setTimeout( function(){ $('#messagediv').html(''); } , 5000);
							 if(data==2)
							  {   $('#messagediv').html('<?php echo lang('added_repeated_attendance_success'); ?>');
							      window.location = "<?php echo base_url().'customers/repeated_attendance/'.$patient_id.'/';?>" + attent_date;
							  }
							}
							}); 
					 } else return false;	
				}
			});	
		
	});
</script>


<?php $this->load->view("partial/footer"); ?>