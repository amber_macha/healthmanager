<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<fieldset id="customer_basic_info" style="margin-bottom: 30px;">
<legend><?php echo lang("common_patient_information"); ?></legend>   

<div id="report_search">
	<div class="field_row clearfix">	
	<?php echo form_label(lang('start_date').':', 'start_date'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'search_start_date',
			'id'=>'search_start_date',
			'readonly'=>'readonly',
			'value'=>'')
		);?>
		</div>
	</div>
	<div class="field_row clearfix">	
	<?php echo form_label(lang('end_date').':', 'end_date'); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'search_end_date',
			'id'=>'search_end_date',
			'readonly'=>'readonly',
			'value'=>'')
		);?>
		</div>
	</div>
<?php echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right','onclick'=>'Search_PatientReports();'));?>
<img id="searchReports" class="searchReports" src="<?php echo base_url();?>images/spinner_small.gif">
</div>

<div id="report_data"></div>

</fieldset>

<script type='text/javascript'>

function Search_PatientReports()
{
	search_start_date = $("#search_start_date").val(); 
	search_end_date = $("#search_end_date").val();
	if(search_start_date==''){ alert('<?php echo lang('start_date_validation');?>');return false; }  
	if(search_end_date==''){ alert('<?php echo lang('end_date_validation');?>');return false; }	
	if(search_start_date > search_end_date){ alert('<?php echo lang('invalid_date_selection');?>');return false; }    
	var GetDataURL ="<?php echo base_url().'customers/view_patient_reports/'.$person_id?>";
	$('#searchReports').show();
	$.ajax({ 
			url: GetDataURL,
			type: 'post',
			data: 'start_date=' + search_start_date + '&end_date=' + search_end_date,
			success: function(data)  
				{  $('#searchReports').hide();
				   $('#report_search').hide();
				   $('#report_data').html(data);					
				}
		   }); 	
	return false;
}

//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('#print_profile').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);	
    }	
	$("#customer_basic_info").click(function(){ $('#print_profile').show(); });	

//validation and submit handling
$(document).ready(function()
{
   $('#search_start_date').datePicker({startDate: '01-01-1970'}); 
   $('#search_end_date').datePicker({startDate: '01-01-1970'}); 	
});
</script>