<?php echo form_open('customers/save_profile/'.$customer_id,array('id'=>'customer_form','onsubmit'=>'return validateSearch();'));?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("common_update_profile_form"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_select_category').':', 'select_category',array('class'=>'required wide')); ?>
	<div class='form_field'>
			<select name="category_name" id="category_name" class="valid" style="width:200px">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_list->num_rows()>0){
			foreach($category_list->result() as $data){?>
				<option value="<?=$data->id;?>"><?=ucfirst($data->category_name);?></option>
			<?php }}?>
			</select>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_measurement').':', 'measurement',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'measurement_data',
		'id'=>'measurement_data',
		'style'=>'width:186px',
		'value'=>'')
	);?>
	</div>
</div>


<?php echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
</fieldset>
<?php echo form_close();?>




<script type='text/javascript'>
$(document).ready(function()
{
	var submitting = false;
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{ 
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			category_name: "required",
			measurement_data: "required",    		
   		},
		messages: 
		{
     		category_name: <?php echo json_encode(lang('common_select_category_required')); ?>,
     		measurement_data: <?php echo json_encode(lang('common_measurement_required')); ?>,			
		}
	});
});
</script>