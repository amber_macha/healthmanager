<?php $action_desable='';$form_action_type ='form_input';if(isset($method_type) && $method_type=='diabetes_customers'){ $action_desable ='disabled="disabled"';$form_action_type ='form_input_readonly';}//$checked=''; if(!isset($person_info->patient_category) || $person_info->patient_category==''){ $checked ='checked';}?><?php if($person_info->registration_type=='1') $RapidSection = 'style="display:none"'; else $RapidSection = 'style="display:block"';?><div class="field_row clearfix"> 		<label style="width:160px;font-weight: normal;"><input type="radio" name="registration_type" value="0" <?php echo $action_desable; if($person_info->registration_type=='0' || $person_info->registration_type=='')echo 'checked="checked"';?> onclick="getRegistrationType(0)"> <?=lang('common_registration_type_normal');?> </label>		<label style="width: 160px;font-weight: normal;"><input type="radio" name="registration_type" value="1" <?php echo $action_desable; if($person_info->registration_type=='1')echo 'checked="checked"';?> onclick="getRegistrationType(1)"> <?=lang('common_registration_type_rapid');?> </label></div><div class="field_row clearfix"> <?php $patient_category ='0'; $nhif_patient_field = 'style="display:none"'; if(isset($person_info->patient_category) && $person_info->patient_category!='') {  $patient_category = $person_info->patient_category;    if($person_info->patient_category==4){ $nhif_patient_field = 'style="display:block"'; }} ?>		<label style="width:108px;font-weight: normal;"><input type="radio" name="patient_type_category" value="0" <?php echo $action_desable; if($patient_category=='0')echo 'checked="checked"';?> onclick="getPatientType(0)"> <?=lang('common_patient_category_type1');?> </label>		<label style="width: 60px;font-weight: normal;"><input type="radio" name="patient_type_category" value="1" <?php echo $action_desable; if($patient_category=='1')echo 'checked="checked"';?> onclick="getPatientType(1)"> <?=lang('common_patient_category_type2');?> </label>	<label style="width:100px;font-weight: normal;"><input type="radio" name="patient_type_category" value="2" <?php echo $action_desable; if($patient_category=='2')echo 'checked="checked"';?> onclick="getPatientType(2)"> <?=lang('common_patient_category_exemption');?> </label>	<label style="width:165px;font-weight: normal;"><input type="radio" name="patient_type_category" value="3" <?php echo $action_desable; if($patient_category=='3')echo 'checked="checked"';?> onclick="getPatientType(3)"> <?=lang('common_patient_temporary_exemption');?> </label>	<label style="width:108px;font-weight: normal;"><input type="radio" name="patient_type_category" value="4" <?php echo $action_desable; if($patient_category=='4')echo 'checked="checked"';?> onclick="getPatientType(4)"> <?=lang('common_patient_nhif');?> </label>	<label style="width:108px;font-weight: normal;"><input type="radio" name="patient_type_category" value="5" <?php echo $action_desable;if($patient_category=='5')echo 'checked="checked"';?> onclick="getPatientType(5)"> <?=lang('common_patient_nssf');?> </label>	<label style="width:118px;font-weight: normal;"><input type="radio" name="patient_type_category" value="6" <?php echo $action_desable; if($patient_category=='6')echo 'checked="checked"';?> onclick="getPatientType(6)"> <?=lang('common_patient_chf');?> </label></div><div id="nhif_patient_field" <?php echo $nhif_patient_field;?>>	<div class="field_row clearfix">		<?php echo form_label(lang('customers_member_card_no').':', 'member_card_no'); ?>		<div class='form_field'>		<?php echo $form_action_type(array(			'name'=>'member_card_no',			'id'=>'member_card_no',			'value'=>$person_info->member_card_no,			'onchange' =>'Check_Valid_Customers(this.value)',			'onclick'=>'clicktohide(this);')		);?>		<img id="spinner003" src="<?php echo base_url();?>images/loading.gif">		</div>		<div id="get_valid" class="get_valid" style="font-weight:normal;"></div>	</div>	<div class="field_row clearfix">		<?php echo form_label(lang('customers_membership_no').':', 'membership_no'); ?>		<div class='form_field'>		<?php echo form_input_readonly(array(			'name'=>'membership_no',			'id'=>'membership_no',			'value'=>$person_info->membership_no,			'onclick'=>'clicktohide(this);')		);?>		</div>	</div></div><div class="field_row clearfix RapidSection" <?php echo $RapidSection;?>>			<?php echo form_label(lang('common_insurance_company').':', 'company_name',array('class'=>'wide'));	$insurance_company = $this->savesetting->Get_Table_Data('insurance_company','company_name');?>	<div class='form_field'>			<select name="insurance_company" id="insurance_company" style="width:193px;" <?php echo $action_desable;?>>			<option value=""><?php echo lang('common_select_type');?></option>			<?php if($insurance_company->num_rows()>0){			foreach($insurance_company->result() as $rowdata){?>			<option value="<?=$rowdata->id;?>" <?php if($person_info->insurance_company==$rowdata->id) echo 'selected';?>><?php echo ucfirst($rowdata->company_name); ?></option>			<?php }}?></select>	</div></div>	<div class="field_row clearfix">
<?php echo form_label(lang('common_first_name').':', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'first_name',
		'id'=>'first_name',		'value'=>$person_info->first_name,		'onclick'=>'clicktohide(this);')
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_last_name').':', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'value'=>$person_info->last_name,		'onclick'=>'clicktohide(this);')
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_phone_number').':', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$person_info->phone_number));?>
	</div>
</div>
<div class="field_row clearfix">	<?php echo form_label(lang('common_address_1'). ':', 'common_address_1',array('class'=>'wide')); ?>	<div class='form_field'>			<select name="address_1" id="address_1" class="valid" style="width:193px;" <?php echo $action_desable;?>>			<option value=""><?php echo lang('common_residence_select');?></option>			<?php if($ResidenceData->num_rows()){			foreach($ResidenceData->result() as $rowdata){?>			<option value="<?php echo $rowdata->residence_id;?>" <?php if($rowdata->residence_id==$person_info->address_1)echo 'selected="selected"';?>><?php echo stripslashes($rowdata->residence_name);?></option>			<?php }}?>			</select>	</div></div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_address_2').':', 'address_2'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'value'=>$person_info->address_2));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_country').':', 'country'); if(isset($person_info->country) && $person_info->country!='') $country = $person_info->country; else $country = $this->config->item('default_country'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('country',$this->Customer->GetCountry(), $country,$action_desable.' '.'style="width:193px;"');?>	
	</div>
</div>