<input type="button" id="print_profile" class="print_profile"  value="<?=strtoupper(lang('common_print_profile'));?>" onclick="$.jPrintArea('#report_data');">

<div class="profile-sec">		
  		    <div class="profile-info"><div class="profileinfo01"><?php echo lang('patient_name');?> :</div> <div class="peopleinfo2"><?=ucwords(strtolower($person_info->first_name)).' '. ucwords(strtolower($person_info->last_name)); ?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_age');?> :</div> <div class="peopleinfo2"><?=$person_info->age .' Years';?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_sex');?> :</div> <div class="peopleinfo2"><?=$person_info->sex;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_address_1');?>:</div> <div class="peopleinfo2"><?=$person_info->address_1;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_address_2');?> :</div> <div class="peopleinfo2"><?=$person_info->address_2;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_country');?> :</div> <div class="peopleinfo2"><?=$person_info->country;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('patient_type');?>:</div> <div class="peopleinfo2"><?=$person_info->patient_type;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_ten_cell_leader');?> :</div> <div class="peopleinfo2"><?=$person_info->leader;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_religion');?> :</div> <div class="peopleinfo2"><?=$person_info->religion;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('occupation');?>:</div> <div class="peopleinfo2"><?=$person_info->occupation;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_next_of_kin');?> :</div> <div class="peopleinfo2"><?=$person_info->kin;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_mobile_number');?> :</div> <div class="peopleinfo2"><?=$person_info->phone_number;?></div>  </div> 
			<div class="profile-info"><div class="profileinfo01"><?php echo lang('common_blood_group');?> :</div> <div class="peopleinfo2">_</div>  </div> 
</div>
<?php $start_date=date('Y-m-d');$end_date=date('Y-m-d');?>


<div id="measure_table_report">
		<!--********DISPLAY REPORTS FOR BLOOD PRESSURE**************-->	
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_blood_pressure');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_measurement');?></th>
			</tr>
			<?php if($blood_preasure_data->num_rows()>0){  $i = 1;
			foreach($blood_preasure_data->result() as $rowdata) 
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=$rowdata->recording_data;?></td>				
			</tr>   
			<?php $i++;}}?>   		
        </table>	

		<!--********DISPLAY REPORTS FOR PULSE/HEART RATE**************-->	
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_pulse_heart_rate');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_pulse_heart_rateper_minute');?></th>
			</tr>
			<?php if($pulseheart_data->num_rows()>0){  $i = 1;
			foreach($pulseheart_data->result() as $rowdata) 
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=$rowdata->recording_data;?></td>				
			</tr>   
			<?php $i++;}}?>   		
        </table>	
		
		<!--********DISPLAY REPORTS FOR TEMPRATURE**************-->	
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_temperature');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_measurement');?></th>
			</tr>
			<?php if($temperature_data->num_rows()>0){  $i = 1;
			foreach($temperature_data->result() as $rowdata) 
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=to_temperature($rowdata->recording_data);?></td>				
			</tr>   
			<?php $i++;}}?>   		
        </table>			
		
		<!--********DISPLAY REPORTS FOR WEIGHT**************-->
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_weight');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_measurement');?></th>
			</tr>
			<?php if($weight_data->num_rows()>0){  $i = 1;
			foreach($weight_data->result() as $rowdata)
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=to_weight($rowdata->recording_data);?></td>				
			</tr>   
			<?php $i++;}}?>   		
        </table>	
		
		<!--********DISPLAY REPORTS FOR HEIGHT**************-->	
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_height');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_measurement');?></th>
			</tr>
			<?php if($height_data->num_rows()>0){  $i = 1;
			foreach($height_data->result() as $rowdata) 
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=to_height($rowdata->recording_data);?></td>				
			</tr>   
			<?php $i++;}}?>   		
        </table>		

		<!--********DISPLAY REPORTS FOR RESPIRATORY RATE**************-->	
		<div class="measure_table_sec"> 
			<div class="measure_title_left"><center><?=lang('common_respiratory_rate');?></center></div>  
			<div class="measure_title_right"><?=lang('common_from_date');?> : <span class="peopleinfo2"><?=$start_date;?></span> <?=lang('common_to');?>  <span class="peopleinfo2"><?=$end_date;?></span> </div>  
		</div>	
		<table class="report_table">
			<tr>
				<th class="leftmost"><?=lang('common_date');?></th>
				<th><?=lang('common_time');?></th>
				<th><?=lang('common_respiratory_rate_per_mninut');?></th>
			</tr>
			<?php if($respiratory_rate_data->num_rows()>0){  $i = 1; 
			foreach($respiratory_rate_data->result() as $rowdata) 
			{  if($i%2==0){ $bgStyle = 'style="background-color: #e9e9e9;"';}else {$bgStyle='';}
			?>			
			<tr>				
				<td width="30%" <?=$bgStyle;?>><?=date('d-m-Y',strtotime($rowdata->recording_time));?></td>
				<td width="30%" <?=$bgStyle;?>><?=date('H-i:s',strtotime($rowdata->recording_time));?></td>	
				<td width="30%" <?=$bgStyle;?>><?=$rowdata->recording_data;?></td>	  			
			</tr>   
			<?php $i++;}}?>   		
        </table>	
		
</div>

