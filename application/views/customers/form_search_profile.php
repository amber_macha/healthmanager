<?php echo form_open('customers/search_profile/clinical',array('id'=>'search_customers','onsubmit'=>'return validateSearch();'));?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("common_search_profile_form"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('start_date').':', 'start_date'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'search_start_date',
		'id'=>'search_start_date',
		'readonly'=>'readonly',
		'value'=>'')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('end_date').':', 'end_date'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'search_end_date',
		'id'=>'search_end_date',
		'readonly'=>'readonly',
		'value'=>'')
	);?>
	</div>
</div>

<?php echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
</fieldset>
<?php echo form_close();?>


<script type='text/javascript'>

function validateSearch()
{
	search_start_date = $("#search_start_date").val(); 
	search_end_date = $("#search_end_date").val();
	if(search_start_date==''){ alert('<?php echo lang('start_date_validation');?>');return false; }  
	if(search_end_date==''){ alert('<?php echo lang('end_date_validation');?>');return false; }	
	if(search_start_date > search_end_date){ alert('<?php echo lang('invalid_date_selection');?>');return false; } 
    var searchURL = "<?php echo base_url();?>customers/index/clinical/" + search_start_date +'/' + search_end_date	
	window.location.href=searchURL;
	return false;
}	

//validation and submit handling
$(document).ready(function()
{
   $('#search_start_date').datePicker({startDate: '01-01-1970'}); 
   $('#search_end_date').datePicker({startDate: '01-01-1970'}); 	
});
</script>