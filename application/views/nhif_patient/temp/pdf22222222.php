<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PDF Created</title>

<style type="text/css">
body {
    font-family: "Lucida Grande",Arial;
    margin: auto;
    padding: 0;
    width: 1020px;
}
#content_area {
    background: none repeat scroll 0 0 #ffffff;
    margin: 1px auto;
    padding: 20px 10px 10px;
    width: 960px;
}

.nhif_patient_title{font-size: 20px; margin-right: 108px;}
.nhif_report_sec{ float: left;}
.nhif_report_logo{ width:100px;}
#nhif_table_holder {  height: auto;  width: 100%;text-align: center;}
#nhif_table_report{ width:100%; }
#nhif_table_report th {
    background-color: #C0C0C0;
    border: 1px solid #808080;
    color: #000000;
    font-size: 11px;
    height: 30px;
    padding: 0 5px;
    text-align: left;
	text-align: center;
}
#nhif_table_report th.leftmost {
    background-position: left -80px;
}
#nhif_table_report th.rightmost {
    background-position: right -80px;
}
#nhif_table_report .headerSortUp {
    background-position: right 0;
}
#nhif_table_report .headerSortDown {
    background-position: right -40px;
}
#nhif_table_report .rightmost {
    border-right: medium none;
}
#nhif_table_report td {
    border: 1px solid #BBBBBB;
    color: #444444;
     font-size: 12px;
    height: 30px;
    padding: 0 px;
    vertical-align: middle;
}
#nhif_table_report td.rightmost {
    padding: 0;
}
#nhif_table_report td.over {
    background-color: #CCCCFF;
    background-image: none;
}
#patient_data {
	display:none;
	padding: 5px;
	min-height: 520px;
	background-color: #ffffff;
	margin-top: 20px;
	margin-left: -52px;
	width: 1050px;
    border: 4px solid #DDDDDD;
    border-radius: 3px;
    position: absolute !important;
    z-index: 1000000;
}

#content_area {
    background: none repeat scroll 0 0 #ffffff;
    margin: 1px auto;
    padding: 20px 10px 10px;
    width: 960px;
}
.table_particular{float:left;width:100%;font-size:14px;}
.table_particular td {padding:2px;}
.table_sec_A{font-weight:bold;float:left;}
.table_sec_B{font-weight:bold;float:left;}
.table_sec_td1{float:left;}
.table_top_sec{float:left;width:100%;}
.table_sec_illness_type{float:left;text-align: left;}
.table_hospital_sec1{float:left;}
.table_hospital_sec2{float:left;margin-left:25px;}
.nhif_report_serial_sec{float:right;width:200px;font-size:14px;margin-right: 52px;}
.nhif_report_serial{float:right;}


#title_bar {
    font-size: 1.75em;
    height: 50px;
    margin: auto;
    width: 960px;
}
table {
    border-collapse: collapse;
    width: 100%;
}

.nhif_report_logo {
    width: 100px;
}

</style>
</head>
<body>
<div id="content_area_wrapper">
<div id="content_area">

	<div class="nhif_report_serial_sec" style="float:right;width:200px;font-size:14px;margin-right: 52px;">
	<div class="nhif_report_serial"><?=lang('common_nhif_form_refernce_title_name');?></div>
	<div class="nhif_report_serial"><?=lang('common_serial_no');?> : <?php echo $reference_code;?></div>
	</div>

	<p>The PDF you are looking at is being generated dynamically by HTML2PDF.</p>

	<code><div class="nhif_report_serial_sec">
	<div class="nhif_report_serial"><?=lang('common_nhif_form_refernce_title_name');?></div>
	<div class="nhif_report_serial"><?=lang('common_serial_no');?> : <?php echo $reference_code;?></div>
	</div></code>

</div>
</div>


</body>
</html>