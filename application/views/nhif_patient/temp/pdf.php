<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PDF Created</title>

<style type="text/css">

body {
    font-family: "Lucida Grande",Arial;
    margin: auto;
    padding: 0;
    width: 1020px;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 padding: 12px 10px 12px 10px;
}

#content_area {
    background: none repeat scroll 0 0 #ffffff;
    margin: 1px auto;
    padding: 20px 10px 10px;
    width: 960px;
}
.nhif_patient_title{font-size: 20px; margin-right: 108px;}
.nhif_report_sec{ float: left;}
.nhif_report_logo{ width:100px;}
#nhif_table_holder {  height: auto;  width: 100%;text-align: center;}
#nhif_table_report{ width:100%; }
#nhif_table_report th {
    background-color: #C0C0C0;
    border: 1px solid #808080;
    color: #000000;
    font-size: 11px;
    height: 30px;
    padding: 0 5px;
    text-align: left;
	text-align: center;
}
#nhif_table_report th.leftmost {
    background-position: left -80px;
}
#nhif_table_report th.rightmost {
    background-position: right -80px;
}
#nhif_table_report .headerSortUp {
    background-position: right 0;
}
#nhif_table_report .headerSortDown {
    background-position: right -40px;
}
#nhif_table_report .rightmost {
    border-right: medium none;
}
#nhif_table_report td {
    border: 1px solid #BBBBBB;
    color: #444444;
     font-size: 12px;
    height: 30px;
    padding: 0 px;
    vertical-align: middle;
} 
#nhif_table_report td.rightmost {
    padding: 0;
}
#nhif_table_report td.over {
    background-color: #CCCCFF;
    background-image: none;
}
#patient_data {
	display:none;
	padding: 5px;
	min-height: 520px;
	background-color: #ffffff;
	margin-top: 20px;
	margin-left: -52px;
	width: 1050px;
    border: 4px solid #DDDDDD;
    border-radius: 3px;
    position: absolute !important;
    z-index: 1000000;
}

#content_area {
    background: none repeat scroll 0 0 #ffffff;
    margin: 1px auto;
    padding: 20px 10px 10px;
    width: 960px;
}
.table_particular{float:left;width:100%;font-size:14px;}
.table_particular td {padding:2px;}
.table_sec_A{font-weight:bold;float:left;}
.table_sec_B{font-weight:bold;float:left;}
.table_sec_td1{float:left;}
.table_top_sec{float:left;width:100%;}
.table_sec_illness_type{float:left;text-align: left;}
.table_hospital_sec1{float:left;}
.table_hospital_sec2{float:left;margin-left:25px;}
.nhif_report_serial_sec{float:left;width:225px;font-size:14px;margin-top:10px;}
.nhif_report_serial{float:left;}


#title_bar {
    font-size: 1.75em;
    height: 50px;
    margin: auto;
    width: 960px;
}
table {
    border-collapse: collapse;
    width: 100%;
}

.nhif_report_logo {
    width: 100px;
}




</style>
</head>
<body>
<h1><?php echo lang('common_nhif_patient_form_title2'); ?></h1>
<div class="nhif_report_serial_sec"><?=lang('common_nhif_form_refernce_title_name');?> </div>
<div class="nhif_report_serial_sec"><?=lang('common_serial_no');?> : <?php echo $reference_code;?></div>

<table id="nhif_table_holder" >
<tr><td>
<table id="title_bar">
		<tr> 
		<td width="150"><div class="nhif_report_sec"><img class="nhif_report_logo" src="<?php echo base_url();?>images/nhiflogo.jpg"></div></td> 
		<td><div class="nhif_patient_title"><?php echo lang('common_nhif_patient_form_title'); ?></div></td> 
		</tr>
	<tr>
	</tr>
</table>
</td></tr>
<tr><td>&nbsp;</tr></td>
<tr><td>
<table class="table_particular">
	<tr><td width="13%" class="table_sec_A"><?php echo lang('common_particulars_a'); ?>: </td></tr>
	<tr>
		<td width="100%" class="table_sec_td1">
			<div class="table_top_sec">
				<span class="table_hospital_sec1">1. <?=lang('common_nhif_form_hospital_name');?> : <?php echo $this->config->item('company');?> </span>
				<span class="table_hospital_sec2">2. <?=lang('common_nhif_form_file_accreditation_no');?> : -------- </span>
			</div>
		</td>
	</tr>
	<tr>
		<td width="100%" class="table_sec_td1">
			<div class="table_top_sec">
				<span class="table_hospital_sec1">3. <?=lang('common_address');?> : <?php echo $this->config->item('address');?> </span>
				<span class="table_hospital_sec2">4. <?=lang('common_registration_fees');?> : <?php echo to_currency($this->config->item('nhif_registration_fee'));?> </span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td width="100%" class="table_sec_td1">
			<div class="table_top_sec">
				<span class="table_hospital_sec1">5. <?=lang('common_name_of_patient');?> : <?=$Cust_Data->first_name .' '.$Cust_Data->last_name;?> </span>
				<span class="table_hospital_sec2">6. <?=lang('common_age');?> : <?=lang('year').' '. $Cust_Data->age;?> </span>
				<span class="table_hospital_sec2">7. <?=lang('common_sex_type');?> : <?=$Cust_Data->sex;?> </span>
				<span class="table_hospital_sec2">8. <?=lang('common_membership_no');?> : <?=$Cust_Data->membership_no;?> </span>
			</div>
		</td>
	</tr>	

	
	<tr>
		<td width="100%" class="table_sec_td1">
			<div class="table_top_sec">
				<span class="table_hospital_sec1">9. <?=lang('common_occupation');?>  : <?=$this->Customer->GetOccupation_ColumData($Cust_Data->occupation,'occupation_name');?> </span>
				<span class="table_hospital_sec2">10. <?=lang('common_date_of_attendance');?> : <?php if(isset($Attendance_Date['Attandance_Start_Date']) && isset($Attendance_Date['Attandance_End_Date']) && $Attendance_Date['Attandance_Start_Date']!='' && $Attendance_Date['Attandance_End_Date']!=''){echo lang('common_from') .' : '.date('d-m-Y',strtotime($Attendance_Date['Attandance_Start_Date'])) .' '. lang('common_to').' '.date('d-m-Y',strtotime($Attendance_Date['Attandance_End_Date']));}?> </span>
			</div>
		</td>
	</tr>	
	
	<tr> <td width="100%" class="table_sec_illness_type">11. <?=lang('common_illness_type_code');?> : <?=$Diagnosis_List;?></td> </tr>	
	
	<tr><td>&nbsp;</tr></td>
	
</table>
</td></tr>	

<tr><td width="16%" class="table_sec_B"><?php echo lang('common_particulars_b'); ?> : </td></tr>
<tr><td>&nbsp;</td></tr>

	<tr><td>
	  <div id="nhif_table_report">
	    <div style="width:100%" id="table_holder2">
	      <table class="tablesorter" id="sortable_table">			
			<tr>
				<th class="leftmost"><?php echo lang('common_invastigation'); ?></th>
				<th class="leftmost"><?php echo lang('common_medicine_category'); ?> </th>
				<th class="leftmost"><?php echo lang('common_in_patient'); ?></th>
				<th class="leftmost"><?php echo lang('common_surgery'); ?></th>
			</tr>
				
		     <tr style="cursor: pointer;">
				<td width="22%">
					<table class="nhif_table_internal" style="float:left;text-align: center;">  		
					<tr>
						<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_type');?></th>
						<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_codes');?></th>
						<th style="background-color: #ffffff;border-right:none;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_costs');?></th>
					</tr>
					<?php $Investigation_Total_nhif_price ='0.00';
					if($Investigation_Data->num_rows()>0){
					foreach($Investigation_Data->result() as $RowData){
					$Investigation_Total_nhif_price += $RowData->unit_nhif_price;?>
					<tr>
						<td width="30%"><?php echo $RowData->items_generic_code;?></td>
						<td width="30%"><?php echo $RowData->nhif_code_number;?></td>
						<td width="30%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>
					</tr>
					<?php }}			
					if($USS_xrays_Data->num_rows()>0){
					foreach($USS_xrays_Data->result() as $RowData){
					$Investigation_Total_nhif_price += $RowData->unit_nhif_price;?>
					<tr>
						<td width="30%"><?php echo $RowData->items_generic_code;?></td>
						<td width="30%"><?php echo $RowData->nhif_code_number;?></td>
						<td width="30%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>
					</tr>
					<?php }}			
					if($Dentals_Data->num_rows()>0){
					foreach($Dentals_Data->result() as $RowData){
					$Investigation_Total_nhif_price += $RowData->unit_nhif_price;?>
					<tr>
						<td width="30%"><?php echo $RowData->items_generic_code;?></td>
						<td width="30%"><?php echo $RowData->nhif_code_number;?></td>
						<td width="30%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>
					</tr>
					<?php }}
					if($Eye_Clinic->num_rows()>0){
					foreach($Eye_Clinic->result() as $RowData){
					$Investigation_Total_nhif_price += $RowData->unit_nhif_price;?>
					<tr>
						<td width="30%"><?php echo $RowData->items_generic_code;?></td>
						<td width="30%"><?php echo $RowData->nhif_code_number;?></td>
						<td width="30%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>
					</tr>
					<?php }}
					
					$Temp_Row_Limit = ($Maximum_Limit - $Total_Investigation);
					if($Temp_Row_Limit>0){ for($k=0; $k<$Temp_Row_Limit; $k++){?>
					<tr>
						<td width="30%">----</td>
						<td width="30%">----</td>
						<td width="30%" style="border-right: none;">----</td>
					</tr>
					<?php }}?>				
					</table>				
				</td>

				<td width="22%">
					<table class="nhif_table_internal" style="float:left;text-align: center;">  	
					<tr>
						<th colspan="2" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_type_generic');?></th>
						<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_codes');?></th>
						<th style="background-color: #ffffff;border-right:1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_quantity_drugs');?></th>
						<th style="background-color: #ffffff;border-right:none;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_costs');?></th>
					</tr>
					<?php $Medicine_Total_nhif_price ='0.00';$Total_Drugs_Quantity = '0.00';
					if($Medicine_Data->num_rows()>0){ 
					foreach($Medicine_Data->result() as $RowData){ 
					$Medicine_Total_nhif_price += ($RowData->quantity * $RowData->unit_nhif_price);
					$Total_Drugs_Quantity +=  $RowData->quantity;
					?>
					<tr>
						<td colspan="2" width="6%"><?php echo $RowData->items_generic_code;?></td>
						<td width="14%"><?php echo $RowData->nhif_code_number;?></td>
						<td width="14%"><?php echo $RowData->quantity;?></td>
						<td width="30%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>					
					</tr>
					<?php }}
					if(isset($Maximum_Limit) && $Maximum_Limit!=''){
					$Temp_Row_Limit = ($Maximum_Limit - $Medicine_Data->num_rows());
					if($Temp_Row_Limit>0){ for($k=0; $k<$Temp_Row_Limit; $k++){?>
					<tr>
						<td colspan="2" width="6%">----</td>
						<td width="14%">----</td>
						<td width="14%">----</td>
						<td width="30%" style="border-right: none;">----</td>
					</tr>
					<?php }}}?>	
				  </table>				
			  </td> 	

				
			  <td width="22%">
				<table class="nhif_table_internal" style="float:left;text-align: center;">  		
					<tr>
						<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_admission_date');?></th>
						<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_discharged_on');?> </th>
						<th colspan="2" style="background-color: #ffffff;border-right:1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_total_days');?></th>
						<th colspan="2" style="background-color: #ffffff;border-right:none;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_total_cost');?></th>
					</tr>
					
					<?php if(isset($ipdin_info->person_id) && $ipdin_info->person_id!='') $ipdin_Rows = $Maximum_Limit-1; else $ipdin_Rows = $Maximum_Limit;
					if(isset($ipdin_info->person_id) && $ipdin_info->person_id!=''){?>
					<tr>
						<td width="6%" style="border-right: 1px solid #bbbbbb;"><?php echo date('d-m-Y',strtotime($ipdin_info->registration_time));?></td>
						<td width="14%" style="border-right: 1px solid #bbbbbb;"><?php if($ipdin_info->discharge_status=='1' && $ipdin_info->discharge_date!='0000-00-00 00:00:00' && $ipdin_info->discharge_date!='')echo date('d-m-Y',strtotime($ipdin_info->discharge_date));else echo '---';?></td>
						<td colspan="2" width="30%" style="border-right: 1px solid #bbbbbb;"><?=$Total_Days;?></td>
						<td colspan="2" width="30%" style="border-right: none;"><?=to_currency($Total_Costs);?></td>				
					</tr>
					<?php } if($ipdin_Rows>0){ for($k=0; $k<$ipdin_Rows; $k++){?>
					<tr>
						<td width="6%">----</td>
						<td width="14%">----</td>
						<td colspan="2" width="30%">----</td>
						<td colspan="2" width="30%" style="border-right: 1px solid #bbbbbb;">----</td>
					</tr>
					<?php }}?>			
				</table>				
			 </td> 	
		
				
				<td width="22%">
					<table class="nhif_table_internal" style="float:left;text-align: center;">  		
						<tr> 
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('items_type_specialized');?></th>
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('items_type_major');?></th>
							<th style="background-color: #ffffff;border-right:1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('items_type_minor');?></th>
							<th style="background-color: #ffffff;border-right:1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('items_type_procedure');?></th>
							<th style="background-color: #ffffff;border-right:1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_codes');?></th>
							<th style="background-color: #ffffff;border-right:none;border-left: none;border-top: none;border-bottom: none;"><?php echo lang('common_costs');?></th>
						</tr>
					
						<?php  $Otherservices_Total_nhif_price ='0.00';
							  if($otherservices->num_rows()>0){
							  foreach($otherservices->result() as $RowData){
							  $Otherservices_Total_nhif_price += $RowData->unit_nhif_price;?>
						<tr>
							<td width="6%"> <?php if($RowData->service_sub_category1)echo lang('common_yes');else echo lang('common_no');?></td>
							<td width="14%"><?php if($RowData->service_sub_category2)echo lang('common_yes');else echo lang('common_no');?></td>
							<td width="14%"><?php if($RowData->service_sub_category3)echo lang('common_yes');else echo lang('common_no');;?></td>
							<td width="14%"><?php if($RowData->service_sub_category4)echo lang('common_yes');else echo lang('common_no');;?></td>
							<td width="14%"><?php echo $RowData->nhif_code_number;?></td>
							<td width="14%" style="border-right: none;"><?php echo to_currency($RowData->unit_nhif_price);?></td>					
						</tr>
						<?php }}
						$Temp_Row_Limit = ($Maximum_Limit - $otherservices->num_rows());
						if($Temp_Row_Limit>0){ for($k=0; $k<$Temp_Row_Limit; $k++){?>
						<tr>
							<td width="6%">----</td>
							<td width="14%">----</td>
							<td width="14%">----</td>
							<td width="14%">----</td>
							<td width="14%">----</td>
							<td width="14%" style="border-right: none;">----</td>
						</tr>
						<?php }}?>					
					 </table>				
				</td> 			
	       </tr>
				
				<tr style="cursor: pointer;">
					<td width="22%">
						<table class="nhif_table_internal" style="float:left;text-align: center;">  		
						<tr>			
							<th width="32%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold"><?php echo lang('common_sub_total');?></th>
							<th width="20%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th width="30%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold;background-color: #e2e2e2;"><?=to_currency($Investigation_Total_nhif_price);?></th>
						</tr>			
						</table>				
					</td> 
					
					<td width="22%">
						<table class="nhif_table_internal" style="float:left;text-align: center;">  	
						<tr>
							<th width="31%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold"><?php echo lang('common_sub_total');?></th>
							<th width="20%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th width="17%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold;background-color: #e2e2e2;"><?=$Total_Drugs_Quantity;?></th>
							<th width="28%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold;background-color: #e2e2e2;"><?=to_currency($Medicine_Total_nhif_price);?></th>
						</tr>			
					  </table>				
					</td> 	
					
					<td width="22%">
						<table class="nhif_table_internal" style="float:left;text-align: center;">  	
						  <tr>
							<th width="13%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th width="5%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>					
							<th width="24%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold"><?php echo lang('common_sub_total');?></th>			
							<th width="24%" colspan="2" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;background-color: #e2e2e2;"><?=to_currency($Total_Costs);?></th>
						  </tr>			
						</table>				
					</td> 	
					
					
					<td width="22%">
						<table class="nhif_table_internal" style="float:left;text-align: center;"> 
						  <tr>						
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th width="19%" style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold"><?php echo lang('common_sub_total');?></th>
							<th width="15%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold"><?=to_currency($Otherservices_Total_nhif_price);?></th>
						 </tr>		
						</table>				
					</td>						
				</tr>			
    
				<tr style="cursor: pointer;">
					<td width="22%">&nbsp;</td> 
					<td width="22%">&nbsp;</td> 
					<td width="22%">&nbsp;</td> 
					<td width="22%">
						<table class="nhif_table_internal" style="float:left;text-align: center;">
						<tr>	
							<th style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold">&nbsp;</th>
							<th style="background-color: #ffffff;border-right: 1px solid #bbbbbb;border-left: none;border-top: none;border-bottom: none;font-weight: bold;font-size: 18px;"><?php echo lang('common_total');?></th>
							<th width="40%" style="background-color: #ffffff;border-right: none;border-left: none;border-top: none;border-bottom: none;font-weight: bold;font-size: 18px;"><?=to_currency($Investigation_Total_nhif_price + $Medicine_Total_nhif_price + $Otherservices_Total_nhif_price + $Total_Costs + $this->config->item('nhif_registration_fee'));?></th>
						 </tr>
						 </table>				
					</td>	
				</tr>				
			
		  
		  </table>			
		</div>		
	   </div>
	</td></tr>
	
	
	<tr><td>&nbsp;</tr></td>
	<tr><td>
	<table class="table_particular">
		<tr>
			<td width="100%" class="table_sec_td1">
				<div class="table_top_sec"> 
					<span class="table_hospital_sec1"><b> <?php echo lang('common_name_attending_clinician');?> :</b>  <?=$user_info->first_name .' '. $user_info->last_name;?>  </span>
					<span class="table_hospital_sec2"> <?php echo lang('common_qualifications');?> :  <?=$user_info->qualifications;?> </span> 
					<span class="table_hospital_sec2"> <?php echo lang('common_signature');?> : ---------------- </span>
				</div>
			</td>
		</tr>	
	</table>
	</td></tr>
		
	<tr><td>
		<table class="table_particular">
		<tr><td width="17%" class="table_hospital_sec1" style="padding: 3px;"><b><?php echo lang('common_patient_certification');?> :</b> </td></tr> 
		<tr>
			<td width="100%" class="table_sec_td1">
				<div class="table_top_sec">
					<span class="table_hospital_sec1"><?php echo lang('common_nhif_patient_certify_received_service_name');?> : <?=$Cust_Data->first_name .' '.$Cust_Data->last_name;?> </span>
					<span class="table_hospital_sec2"><?php echo lang('common_signature');?> :  ---------------------- </span>
					<span class="table_hospital_sec2"><?php echo lang('common_tel_phone');?> - <?=$user_info->phone_number;?> </span> 
				</div>
			</td>
			
			<td width="100%" class="table_sec_td1">
				<div class="table_top_sec">
					<span class="table_hospital_sec1"><?php echo lang('common_employer_name_address');?> :<?=$Clinical_Doctor_Details;?>  </span>
				</div>
			</td>
			
		</tr>	

		<tr>
			<td width="100%" class="table_sec_td1">
				<div class="table_top_sec">
					<span class="table_hospital_sec1"> <?php echo lang('common_nhif_patient_description_Out_in_patient_management');?> </span>
				</div>
				<div id="signature">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</div>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>	
		</table>
	</td></tr>
	
	<tr><td>
		<table class="table_particular">
		<tr>
			<td width="100%" class="table_sec_td1">
				<div class="table_top_sec">
					<span class="table_hospital_sec1"> <?php echo lang('common_claimant_certification');?>:-------------- </span>
					<span class="table_hospital_sec2"><?php echo lang('common_signature');?> :  ----------------- </span>
					<span class="table_hospital_sec2"><?php echo lang('common_official_stamp');?> : -------------- </span>
				</div>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td width="90%" class="table_hospital_sec1" ><b><?php echo lang('common_fill_triplicate_submit_original_form');?></b> </td></tr>	
		<tr><td width="74%" class="table_hospital_sec1"><b><?php echo lang('common_any_falsified_information_subject');?></b> </td></tr>
		</table>
	</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	
<!--close main table -->
</table>



</body>
</html>