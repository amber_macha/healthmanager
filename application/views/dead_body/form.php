<style>fieldset div.field_row { font-weight: normal;}</style>
<?php echo form_open('dead_body/save/'.$person_info->person_id,array('id'=>'customer_form','enctype'=>'multipart/form-data'));
$form_action_type ='form_input';
if(isset($person_info->person_id) && $person_info->person_id!=''){ $form_action_type ='form_input_readonly';}
?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("dead_body_basic_information"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('customers_account_numbers').':', 'account_no'); ?>
	<div class='form_field'>
	<input type="hidden" id="dead_body_id" value="<?php echo $dead_body_id;?>" name="dead_body_id">
	<?php echo form_input(array('name'=>'account_no',
							    'id'=>'account_no',
							    'value'=>$person_info->account_number,
							    'onclick'=>'clicktohide(this);',
							    'onchange'=>'return GetAccount_Data(this.value);'
							    ));?>
		<span id="form_loader_sec_img" style="display: none;"><img width="15" src="<?php echo base_url();?>images/loading2.gif"></span>
		<div id="account_valid_message" class="error_sex3"></div>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('common_first_name').':', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'value'=>$person_info->first_name,
		'onclick'=>'clicktohide(this);')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_last_name').':', 'last_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'last_name',
		'id'=>'last_name',
		'value'=>$person_info->last_name,
		'onclick'=>'clicktohide(this);')
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_address').':', 'common_address',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'patient_address',
		'id'=>'patient_address',
		'value'=>$person_info->address_2,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_tribe_name').':', 'patient_tribe'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
						 'name'=>'patient_tribe',
						 'id'=>'patient_tribe',
						 'value'=>$person_info->patient_tribe,
						 'onclick'=>'clicktohide(this);')
						 );?>
	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label(lang('common_sex').':', 'sex',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('sex',array(''=>'Select ','Male'=>'Male', 'Female'=>'Female'), $person_info->sex);?>	
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_birth_date').':', 'birth_date',array('class'=>'required')); 
$date_of_birth ='';
if(isset($person_info->dateOfBirth) && $person_info->dateOfBirth!='' && $person_info->dateOfBirth!='0000-00-00'){  $date_of_birth =date('d-m-Y',strtotime($person_info->dateOfBirth)); } ?>
	<div class='form_field'>
	<?php echo form_input_readonly(array('name'=>'date_of_birth',
										 'id'=>'date_of_birth',
										 'value'=>$date_of_birth,
										 'onclick'=>'clicktohide(this);')
										 );?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_date').':', 'dead_body_date',array('class'=>'required')); 
$date_of_death ='';
if(isset($person_info->date_of_death) && $person_info->date_of_death!='' && $person_info->date_of_death!='0000-00-00'){ $date_of_death =date('d-m-Y',strtotime($person_info->date_of_death)); } ?>
	<div class='form_field'>
	<?php echo form_input_readonly( array('name'=>'date_of_death',
										 'id'=>'date_of_death',
										 'value'=>$date_of_death,
										 'onclick'=>'clicktohide(this);')
										 );?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('common_age').':', 'Age',array('class'=>'required wide'));?>
	<div class='form_field'>	
	<?php echo lang('day');?> <select id="age_day" class="valid" name="age_day">
							  <option value=""><?php echo lang('day');?></option>
							  <?php foreach($days as $key=>$value){?>
							  <option value="<?php echo $key;?>" <?php if(isset($person_info->age_day) && $person_info->age_day!='' && $key==$person_info->age_day)echo 'selected'?>><?php echo $value;?></option>
							  <?php }?>
							</select>
   <?php echo lang('month');?>  <select id="age_month" class="valid" name="age_month">
   								<option value=""><?php echo lang('month');?></option>
								<option value="0" <?php if(isset($person_info->age_month) && $person_info->age_month!='' && $person_info->age_month==0)echo 'selected'?>>00</option>
								<?php foreach($months as $key=>$value){?>
								<option value="<?php echo $key;?>"<?php if(isset($person_info->age_month) && $person_info->age_month!='' && $key==$person_info->age_month)echo 'selected'?>><?php echo $key;?></option>
								<?php }?>
								</select>
		
  <?php echo lang('year');?><select id="age_year" class="valid" name="age_year" style="width:65px;">
							<option value=""><?php echo lang('year');?></option>
							<?php for($k=0;$k<150;$k++){?>
							<option value="<?php echo $k;?>"<?php if(isset($person_info->age) && $person_info->age!='' && $k==$person_info->age)echo 'selected'?>><?php echo $k;?></option>
							<?php }?>
							</select>	

	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_reason_of_death').':', 'reason_of_death',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'reason_of_death',
		'id'=>'reason_of_death',
		'value'=>$person_info->reason_of_death));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_car_no_brought_dead_body').':', 'car_no_brought_dead_body',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'car_no_brought_dead_body',
		'id'=>'car_no_brought_dead_body',
		'value'=>$person_info->car_no_brought_dead_body_in_hospital,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_name_of_drive_relative').':', 'name_of_drive_relative'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name_of_drive_relative',
		'id'=>'name_of_drive_relative',
		'value'=>$person_info->name_of_drive_relative,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_relative_relationship').':', 'relationship'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'relationship',
		'id'=>'relationship',
		'value'=>$person_info->relationship,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('dead_body_relative_mobile_no').':', 'mobile_no_relative',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'mobile_no_relative',
		'id'=>'mobile_no_relative',
		'value'=>$person_info->mobile_no_relative,
		'onClick'=>'clicktohide(this);'
		));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_city').':', 'common_city'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'city_name',
		'id'=>'city_name',
		'value'=>$person_info->city,
		'onClick'=>'clicktohide(this);'
		));?>
	</div>
</div>

<?php if(isset($person_info->country) && $person_info->country!='') $nationality = $person_info->nationality; else $nationality = $this->config->item('default_country'); ?>
<div class="field_row clearfix">	
<?php echo form_label('Nationality:', 'nationality'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('nationality',$this->Customer->GetCountry(), $nationality);?>	
	</div>
</div>

<div class="field_row clearfix">	
	<?php echo form_label(lang('dead_body_account_number').':', 'account_no'); ?>
		<div class='form_field'> <?php echo form_input_readonly(array('name'=>'dead_body_account_no','id'=>'dead_body_account_no','value'=>$person_info->other_account_number));?> </div>
	</div> 

<div class="field_row clearfix">	
	<label style="width: 130px;"><?php echo lang('common_profile_image');?> </label>	
	<div class='form_field'> <input type="file" name="patient_image" id="patient_image" onchange="return validate_certifate(this.value,'patient_image');"> </div>	
	<?php if(isset($person_info->dead_body_images) && $person_info->dead_body_images!='' && $person_info->dead_body_images!='0'){?> <div class="patient_image-div"><img class="patient_image" src="<?php echo base_url();?>images/dead_body_document/dead_body_images/<?='thumb_'.$person_info->dead_body_images;?>"> </div><?php }?>	
</div>	

<div class="field_row clearfix">	
	<label style="width: 130px;"><?php echo lang('dead_body_upload_certificate');?> </label>	
	<div class='form_field'> <input type="file" name="death_certificate" id="death_certificate" onchange="return validate_certifate(this.value,'death_certificate');"> </div>	
</div>

<div class="field_row clearfix">
        <span id="error_clinical_daignosis_box" style="display:none"></span>

			<div class="diagnosis_serach"> <?php echo lang('dead_body_diagnoses');?>: <?php echo lang('search_diseases');?> 		
			<input type="text" class="search_daignosis" name="search_lab_service" id="search_lab_service" value="">
			<img src="<?php echo base_url();?>images/spinner_small.gif" class="search-spiner01" id="searchspiner" style="margin-left:-20px;">		
			</div>		
			<div id="labcategory" class="diagnoses_div_sec">
			<?php if(isset($diseases_list) && $diseases_list!=''){ echo $diseases_list; }?>
			</div>		
</div>			

<?php
echo form_submit(array(
	'name'=>'save',	
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>


<script type='text/javascript'>
//**************START CODE FOR LAB DIAGNOSES SEARCH*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)  
      search_daignosis(existingString);
    else   $(this).data('timer', setTimeout(search_daignosis(existingString), 500));
});


function search_daignosis(existingString) { 
	var checkedval='';  //error_clinical_daignosis_box
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'dead_body/searchDeadBodyDiagnosesList/'.$person_info->person_id?>'; 
	$('#searchspiner').show();
	$.ajax({ 
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval,
            success: function(data) 
				{ 
					$('#labcategory').html(data);  $('#searchspiner').hide(); 
				}
        }); 
}

function validate_certifate(fileName,data_fieled)
{
    var allowed_extensions = new Array("jpg","png","gif",'jpeg','pdf','doc','docx');
    var file_extension = fileName.split('.').pop(); 
    for(var i = 0; i <= allowed_extensions.length; i++)
    {
        if(allowed_extensions[i]==file_extension)
        {
			//$("#imgMsg").hide(); 
            return true; // valid file extension
        }
    }
    $('#'+data_fieled).val('');	
	alert('Please upload only jpg,png,gif,jpeg,pdf,doc,docx file');
    return false;
}

function GetAccount_Data(account_no){ 
	if(account_no=='')	 
	{  $('#account_name').val('');	$('#account_valid_message').html(''); 
	   $("#account_no").css("background", "#FFFF00");$("#account_no").css("border", "1px solid #FF0000"); 
	   return false;
	}
	search(account_no);
}


function search(account_no)
{ 	
	$('#account_name').val('');	$('#patient_name').val('');	
	$('#patient_address').val('');	$('#city_name').val('');	
	$('#account_valid_message').html('');  	
	$('#form_loader_sec_img').show();
	$.ajax({
			url: "<?php echo base_url().'customers/GetAccountData'?>",
			type: 'post',
			data: 'account_no='+account_no, 
			dataType:'json',
			success: function(data)
			{ 
   			  $('#account_valid_message').html(''); $('#form_loader_sec_img').hide();
			  if(data.success==true)
			  {		
				$('#dead_body_id').val(data.customer_id);
				$('#first_name').val(data.first_name);
				$('#last_name').val(data.last_name);
				$('#patient_address').val(data.address);
				$("#sex").val(data.sex).change();
				$("#age_day").val(data.age_day).change();
				$("#age_month").val(data.age_month).change();
				$("#age_year").val(data.age_year).change();
				
				$('#city_name').val(data.city_name);
				$("#nationality").val(data.nationality).change();
			  }
			  else { 
					$('#account_valid_message').show(); $('#account_valid_message').html(data.message); 
					$("#account_no").css("background", "#FFFF00");$("#account_no").css("border", "1px solid #FF0000"); 
				  }
			}
	     });	   
}

$(document).ready(function()
{
	$('#date_of_birth').datePicker({startDate: '01-01-1970'}); 
	$('#date_of_death').datePicker({startDate: '01-01-1970'}); 
	
	var submitting = false;
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			first_name: "required", 
			last_name: "required", 
    		patient_address: "required", 
			sex: "required",			
			date_of_birth: "required",
			date_of_death: "required",
			reason_of_death: "required",  
			car_no_brought_dead_body: "required",
			mobile_no_relative: "required",
   		},
		messages: 
		{
     		first_name: <?php echo json_encode(lang('common_first_name_required')); ?>,
			last_name: <?php echo json_encode(lang('common_last_name_required')); ?>,
     		patient_address: <?php echo json_encode(lang('common_address_name_required')); ?>,
			sex: <?php echo json_encode(lang('dead_body_sex_type_required')); ?>,			
			date_of_birth: <?php echo json_encode(lang('common_birth_date_required')); ?>,
			date_of_death: <?php echo json_encode(lang('dead_body_death_date_required')); ?>,
			reason_of_death: <?php echo json_encode(lang('dead_body_reason_of_death_required')); ?>,
			car_no_brought_dead_body: <?php echo json_encode(lang('dead_body_car_no_brought_required')); ?>,
			mobile_no_relative: <?php echo json_encode(lang('dead_body_mobile_no_relative_required')); ?>,
		}
	});
});
</script>