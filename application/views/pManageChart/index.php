<?php //$this->load->view("partial/header"); ?>
 <style>
  div.folder { cursor: hand; cursor: pointer; }
  a.smallLinkGrey:link     { text-decoration: none; color: #6A6A6A; }
  a.smallLinkGrey:visited  { text-decoration: none; color: #6A6A6A; }
  a.smallLinkGrey:hover    { text-decoration: underline; color: #6A6A6A; }
  a.smallLinkBlack:link    { text-decoration: none; color: #000000; }
  a.smallLinkBlack:visited { text-decoration: none; color: #000000; }
  a.smallLinkBlack:hover   { text-decoration: underline; color: #000000; }
 </style>

<body>

<?php
 /* Files that we don't want to see in the tree */
 $Exclusion = array(".","..","index.php","buildAll.cmd","pictures","resources","delayedLoader","sandbox","imageMap");

 /* Determine the current package version */
 $FileHandle  = fopen(BASEPATH."/libraries/pChart2.1.4/readme.txt", "r");
 for ($i=0; $i<=5; $i++) { $buffer = fgets($FileHandle, 4096); }
 fclose($FileHandle);
 $Values  = preg_split("/:/",$buffer);
 $Values  = preg_split("/ /",$Values[1]);
 $Version = strip_tags($Values[1]);


?>

<table><tr><td valign='top'>

<table style="border: 2px solid #FFFFFF;"><tbody><tr><td>
<div style="font-size: 11px; padding: 2px; color: #FFFFFF; background-color: #666666; border-bottom: 3px solid #484848; width: 222px;">&nbsp;Release 2.1.4</div>
<div style="border: 3px solid #D0D0D0; border-top: 1px solid #FFFFFF; background-color: #FAFAFA; width: 220px; overflow: auto">
<div style="padding: 1px; padding-bottom: 3px; color: #000000; background-color:#D0D0D0;">
 <table><tbody><tr>
  <td><img width="16" height="16" alt="" src="resources/application_view_list.png"></td>
  <td>&nbsp;Examples folder contents</td>
 </tr></tbody></table>
</div>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Area Chart&quot;);" id="Area Chart_main" class="folder" style="font-weight: bold;">&nbsp;Area Chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: inline;" id="Area Chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAreaChart.enhanced.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawAreaChart.enhanced.php&quot;);" href="#" class="smallLinkGrey">drawAreaChart.enhanced</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAreaChart.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawAreaChart.php&quot;);" href="#" class="smallLinkGrey">drawAreaChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAreaChart.simple.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawAreaChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawAreaChart.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAreaChart.threshold.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawAreaChart.threshold.php&quot;);" href="#" class="smallLinkGrey">drawAreaChart.threshold</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Bar Chart&quot;);" id="Bar Chart_main" class="folder" style="font-weight: normal;">&nbsp;Bar Chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Bar Chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.borders.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.borders.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.borders</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.can.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.can.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.can</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.floating.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.floating.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.floating</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.palette.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.palette.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.palette</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.php&quot;);" href="#" class="smallLinkGrey">drawBarChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.poll.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.poll.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.poll</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.shaded.php">&nbsp;<a onclick="render(&quot;example.drawBarChart.shaded.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.shaded</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.simple.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.spacing.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.spacing.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.spacing</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.span.php" style="font-weight: normal;">&nbsp;<a onclick="render(&quot;example.drawBarChart.span.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.span</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBarChart.vertical.php" style="font-weight: bold;">&nbsp;<a onclick="render(&quot;example.drawBarChart.vertical.php&quot;);" href="#" class="smallLinkGrey">drawBarChart.vertical</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Barcode&quot;);" id="Barcode_main" class="folder">&nbsp;Barcode</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Barcode"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.barcode.php">&nbsp;<a onclick="render(&quot;example.barcode.php&quot;);" href="#" class="smallLinkGrey">barcode</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.barcode128.php">&nbsp;<a onclick="render(&quot;example.barcode128.php&quot;);" href="#" class="smallLinkGrey">barcode128</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.barcode39.php">&nbsp;<a onclick="render(&quot;example.barcode39.php&quot;);" href="#" class="smallLinkGrey">barcode39</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.singlebarcode128.php">&nbsp;<a onclick="render(&quot;example.singlebarcode128.php&quot;);" href="#" class="smallLinkGrey">singlebarcode128</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.singlebarcode39.php">&nbsp;<a onclick="render(&quot;example.singlebarcode39.php&quot;);" href="#" class="smallLinkGrey">singlebarcode39</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Bubble chart&quot;);" id="Bubble chart_main" class="folder">&nbsp;Bubble chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Bubble chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBubbleChart.php">&nbsp;<a onclick="render(&quot;example.drawBubbleChart.php&quot;);" href="#" class="smallLinkGrey">drawBubbleChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBubbleChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawBubbleChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawBubbleChart.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBubbleChart.square.php">&nbsp;<a onclick="render(&quot;example.drawBubbleChart.square.php&quot;);" href="#" class="smallLinkGrey">drawBubbleChart.square</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Combo&quot;);" id="Combo_main" class="folder">&nbsp;Combo</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Combo"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.Combo.area.lines.php">&nbsp;<a onclick="render(&quot;example.Combo.area.lines.php&quot;);" href="#" class="smallLinkGrey">Combo.area.lines</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.Combo.php">&nbsp;<a onclick="render(&quot;example.Combo.php&quot;);" href="#" class="smallLinkGrey">Combo</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.Mixed.classes.php">&nbsp;<a onclick="render(&quot;example.Mixed.classes.php&quot;);" href="#" class="smallLinkGrey">Mixed.classes</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Drawing&quot;);" id="Drawing_main" class="folder">&nbsp;Drawing</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Drawing"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAntialiasPixel.php">&nbsp;<a onclick="render(&quot;example.drawAntialiasPixel.php&quot;);" href="#" class="smallLinkGrey">drawAntialiasPixel</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawArrow.php">&nbsp;<a onclick="render(&quot;example.drawArrow.php&quot;);" href="#" class="smallLinkGrey">drawArrow</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawArrowLabel.php">&nbsp;<a onclick="render(&quot;example.drawArrowLabel.php&quot;);" href="#" class="smallLinkGrey">drawArrowLabel</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBezier.php">&nbsp;<a onclick="render(&quot;example.drawBezier.php&quot;);" href="#" class="smallLinkGrey">drawBezier</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawCircle.php">&nbsp;<a onclick="render(&quot;example.drawCircle.php&quot;);" href="#" class="smallLinkGrey">drawCircle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFilledCircle.php">&nbsp;<a onclick="render(&quot;example.drawFilledCircle.php&quot;);" href="#" class="smallLinkGrey">drawFilledCircle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFilledRectangle.php">&nbsp;<a onclick="render(&quot;example.drawFilledRectangle.php&quot;);" href="#" class="smallLinkGrey">drawFilledRectangle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFromGIF.php">&nbsp;<a onclick="render(&quot;example.drawFromGIF.php&quot;);" href="#" class="smallLinkGrey">drawFromGIF</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFromJPG.php">&nbsp;<a onclick="render(&quot;example.drawFromJPG.php&quot;);" href="#" class="smallLinkGrey">drawFromJPG</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFromPNG.php">&nbsp;<a onclick="render(&quot;example.drawFromPNG.php&quot;);" href="#" class="smallLinkGrey">drawFromPNG</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawGradientArea.php">&nbsp;<a onclick="render(&quot;example.drawGradientArea.php&quot;);" href="#" class="smallLinkGrey">drawGradientArea</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLegend.php">&nbsp;<a onclick="render(&quot;example.drawLegend.php&quot;);" href="#" class="smallLinkGrey">drawLegend</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLine.php">&nbsp;<a onclick="render(&quot;example.drawLine.php&quot;);" href="#" class="smallLinkGrey">drawLine</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawPolygon.php">&nbsp;<a onclick="render(&quot;example.drawPolygon.php&quot;);" href="#" class="smallLinkGrey">drawPolygon</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawRectangle.php">&nbsp;<a onclick="render(&quot;example.drawRectangle.php&quot;);" href="#" class="smallLinkGrey">drawRectangle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawRoundedFilledRectangle.php">&nbsp;<a onclick="render(&quot;example.drawRoundedFilledRectangle.php&quot;);" href="#" class="smallLinkGrey">drawRoundedFilledRectangle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawRoundedRectangle.php">&nbsp;<a onclick="render(&quot;example.drawRoundedRectangle.php&quot;);" href="#" class="smallLinkGrey">drawRoundedRectangle</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSpline.php">&nbsp;<a onclick="render(&quot;example.drawSpline.php&quot;);" href="#" class="smallLinkGrey">drawSpline</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawText.php">&nbsp;<a onclick="render(&quot;example.drawText.php&quot;);" href="#" class="smallLinkGrey">drawText</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.setFontProperties.php">&nbsp;<a onclick="render(&quot;example.setFontProperties.php&quot;);" href="#" class="smallLinkGrey">setFontProperties</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.setShadow.php">&nbsp;<a onclick="render(&quot;example.setShadow.php&quot;);" href="#" class="smallLinkGrey">setShadow</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Labels&quot;);" id="Labels_main" class="folder">&nbsp;Labels</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Labels"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawIndicator.php">&nbsp;<a onclick="render(&quot;example.drawIndicator.php&quot;);" href="#" class="smallLinkGrey">drawIndicator</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.barchart.php">&nbsp;<a onclick="render(&quot;example.drawLabel.barchart.php&quot;);" href="#" class="smallLinkGrey">drawLabel.barchart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.bubble.php">&nbsp;<a onclick="render(&quot;example.drawLabel.bubble.php&quot;);" href="#" class="smallLinkGrey">drawLabel.bubble</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.caption.php">&nbsp;<a onclick="render(&quot;example.drawLabel.caption.php&quot;);" href="#" class="smallLinkGrey">drawLabel.caption</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.php">&nbsp;<a onclick="render(&quot;example.drawLabel.php&quot;);" href="#" class="smallLinkGrey">drawLabel</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.scatter.php">&nbsp;<a onclick="render(&quot;example.drawLabel.scatter.php&quot;);" href="#" class="smallLinkGrey">drawLabel.scatter</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.stacked.php">&nbsp;<a onclick="render(&quot;example.drawLabel.stacked.php&quot;);" href="#" class="smallLinkGrey">drawLabel.stacked</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLabel.vertical.php">&nbsp;<a onclick="render(&quot;example.drawLabel.vertical.php&quot;);" href="#" class="smallLinkGrey">drawLabel.vertical</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Line chart&quot;);" id="Line chart_main" class="folder">&nbsp;Line chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Line chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLineChart.network.php">&nbsp;<a onclick="render(&quot;example.drawLineChart.network.php&quot;);" href="#" class="smallLinkGrey">drawLineChart.network</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLineChart.php">&nbsp;<a onclick="render(&quot;example.drawLineChart.php&quot;);" href="#" class="smallLinkGrey">drawLineChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLineChart.plots.php">&nbsp;<a onclick="render(&quot;example.drawLineChart.plots.php&quot;);" href="#" class="smallLinkGrey">drawLineChart.plots</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLineChart.reversed.php">&nbsp;<a onclick="render(&quot;example.drawLineChart.reversed.php&quot;);" href="#" class="smallLinkGrey">drawLineChart.reversed</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawLineChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawLineChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawLineChart.simple</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Mathematical&quot;);" id="Mathematical_main" class="folder">&nbsp;Mathematical</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Mathematical"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.createFunctionSerie.php">&nbsp;<a onclick="render(&quot;example.createFunctionSerie.php&quot;);" href="#" class="smallLinkGrey">createFunctionSerie</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.createFunctionSerie.scatter.php">&nbsp;<a onclick="render(&quot;example.createFunctionSerie.scatter.php&quot;);" href="#" class="smallLinkGrey">createFunctionSerie.scatter</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAnscombeQuartet.php">&nbsp;<a onclick="render(&quot;example.drawAnscombeQuartet.php&quot;);" href="#" class="smallLinkGrey">drawAnscombeQuartet</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawBestfit.php">&nbsp;<a onclick="render(&quot;example.drawBestfit.php&quot;);" href="#" class="smallLinkGrey">drawBestfit</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawDerivative.php">&nbsp;<a onclick="render(&quot;example.drawDerivative.php&quot;);" href="#" class="smallLinkGrey">drawDerivative</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawDerivative.simple.php">&nbsp;<a onclick="render(&quot;example.drawDerivative.simple.php&quot;);" href="#" class="smallLinkGrey">drawDerivative.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawGeometricMean.php">&nbsp;<a onclick="render(&quot;example.drawGeometricMean.php&quot;);" href="#" class="smallLinkGrey">drawGeometricMean</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawHarmonicMean.php">&nbsp;<a onclick="render(&quot;example.drawHarmonicMean.php&quot;);" href="#" class="smallLinkGrey">drawHarmonicMean</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawMedian.php">&nbsp;<a onclick="render(&quot;example.drawMedian.php&quot;);" href="#" class="smallLinkGrey">drawMedian</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterBestFit.php">&nbsp;<a onclick="render(&quot;example.drawScatterBestFit.php&quot;);" href="#" class="smallLinkGrey">drawScatterBestFit</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStandardDeviation.php">&nbsp;<a onclick="render(&quot;example.drawStandardDeviation.php&quot;);" href="#" class="smallLinkGrey">drawStandardDeviation</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.normalize.php">&nbsp;<a onclick="render(&quot;example.normalize.php&quot;);" href="#" class="smallLinkGrey">normalize</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Misc&quot;);" id="Misc_main" class="folder">&nbsp;Misc</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Misc"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.addRandomValues.php">&nbsp;<a onclick="render(&quot;example.addRandomValues.php&quot;);" href="#" class="smallLinkGrey">addRandomValues</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.basic.php">&nbsp;<a onclick="render(&quot;example.basic.php&quot;);" href="#" class="smallLinkGrey">basic</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.cache.php">&nbsp;<a onclick="render(&quot;example.cache.php&quot;);" href="#" class="smallLinkGrey">cache</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawAreaMirror.php">&nbsp;<a onclick="render(&quot;example.drawAreaMirror.php&quot;);" href="#" class="smallLinkGrey">drawAreaMirror</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawingObjects.php">&nbsp;<a onclick="render(&quot;example.drawingObjects.php&quot;);" href="#" class="smallLinkGrey">drawingObjects</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSimple.php">&nbsp;<a onclick="render(&quot;example.drawSimple.php&quot;);" href="#" class="smallLinkGrey">drawSimple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawThreshold.labels.php">&nbsp;<a onclick="render(&quot;example.drawThreshold.labels.php&quot;);" href="#" class="smallLinkGrey">drawThreshold.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawThreshold.php">&nbsp;<a onclick="render(&quot;example.drawThreshold.php&quot;);" href="#" class="smallLinkGrey">drawThreshold</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawThresholdArea.php">&nbsp;<a onclick="render(&quot;example.drawThresholdArea.php&quot;);" href="#" class="smallLinkGrey">drawThresholdArea</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawXThreshold.php">&nbsp;<a onclick="render(&quot;example.drawXThreshold.php&quot;);" href="#" class="smallLinkGrey">drawXThreshold</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawXThresholdArea.php">&nbsp;<a onclick="render(&quot;example.drawXThresholdArea.php&quot;);" href="#" class="smallLinkGrey">drawXThresholdArea</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawZoneChart.php">&nbsp;<a onclick="render(&quot;example.drawZoneChart.php&quot;);" href="#" class="smallLinkGrey">drawZoneChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.text.alignment.php">&nbsp;<a onclick="render(&quot;example.text.alignment.php&quot;);" href="#" class="smallLinkGrey">text.alignment</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.transparent.background.php">&nbsp;<a onclick="render(&quot;example.transparent.background.php&quot;);" href="#" class="smallLinkGrey">transparent.background</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.writeBounds.php">&nbsp;<a onclick="render(&quot;example.writeBounds.php&quot;);" href="#" class="smallLinkGrey">writeBounds</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Pie charts&quot;);" id="Pie charts_main" class="folder">&nbsp;Pie charts</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Pie charts"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw2DPie.labels.php">&nbsp;<a onclick="render(&quot;example.draw2DPie.labels.php&quot;);" href="#" class="smallLinkGrey">draw2DPie.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw2DPie.php">&nbsp;<a onclick="render(&quot;example.draw2DPie.php&quot;);" href="#" class="smallLinkGrey">draw2DPie</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw2DRing.php">&nbsp;<a onclick="render(&quot;example.draw2DRing.php&quot;);" href="#" class="smallLinkGrey">draw2DRing</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw2DRingValues.php">&nbsp;<a onclick="render(&quot;example.draw2DRingValues.php&quot;);" href="#" class="smallLinkGrey">draw2DRingValues</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw3DPie.labels.php">&nbsp;<a onclick="render(&quot;example.draw3DPie.labels.php&quot;);" href="#" class="smallLinkGrey">draw3DPie.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw3DPie.php">&nbsp;<a onclick="render(&quot;example.draw3DPie.php&quot;);" href="#" class="smallLinkGrey">draw3DPie</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw3DPie.transparent.php">&nbsp;<a onclick="render(&quot;example.draw3DPie.transparent.php&quot;);" href="#" class="smallLinkGrey">draw3DPie.transparent</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw3DRing.php">&nbsp;<a onclick="render(&quot;example.draw3DRing.php&quot;);" href="#" class="smallLinkGrey">draw3DRing</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.draw3DRingValues.php">&nbsp;<a onclick="render(&quot;example.draw3DRingValues.php&quot;);" href="#" class="smallLinkGrey">draw3DRingValues</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawPieLegend.php">&nbsp;<a onclick="render(&quot;example.drawPieLegend.php&quot;);" href="#" class="smallLinkGrey">drawPieLegend</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Plot chart&quot;);" id="Plot chart_main" class="folder">&nbsp;Plot chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Plot chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawPlotChart.icon.php">&nbsp;<a onclick="render(&quot;example.drawPlotChart.icon.php&quot;);" href="#" class="smallLinkGrey">drawPlotChart.icon</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawPlotChart.php">&nbsp;<a onclick="render(&quot;example.drawPlotChart.php&quot;);" href="#" class="smallLinkGrey">drawPlotChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawPlotChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawPlotChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawPlotChart.simple</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Polar and radars&quot;);" id="Polar and radars_main" class="folder">&nbsp;Polar and radars</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Polar and radars"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.polar.php">&nbsp;<a onclick="render(&quot;example.polar.php&quot;);" href="#" class="smallLinkGrey">polar</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.polar.values.php">&nbsp;<a onclick="render(&quot;example.polar.values.php&quot;);" href="#" class="smallLinkGrey">polar.values</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.radar.fixedmax.php">&nbsp;<a onclick="render(&quot;example.radar.fixedmax.php&quot;);" href="#" class="smallLinkGrey">radar.fixedmax</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.radar.labels.php">&nbsp;<a onclick="render(&quot;example.radar.labels.php&quot;);" href="#" class="smallLinkGrey">radar.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.radar.php">&nbsp;<a onclick="render(&quot;example.radar.php&quot;);" href="#" class="smallLinkGrey">radar</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.radar.values.php">&nbsp;<a onclick="render(&quot;example.radar.values.php&quot;);" href="#" class="smallLinkGrey">radar.values</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Progress bars&quot;);" id="Progress bars_main" class="folder">&nbsp;Progress bars</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Progress bars"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawProgress.php">&nbsp;<a onclick="render(&quot;example.drawProgress.php&quot;);" href="#" class="smallLinkGrey">drawProgress</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Scaling&quot;);" id="Scaling_main" class="folder">&nbsp;Scaling</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Scaling"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.color.php">&nbsp;<a onclick="render(&quot;example.drawScale.color.php&quot;);" href="#" class="smallLinkGrey">drawScale.color</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.custom.php">&nbsp;<a onclick="render(&quot;example.drawScale.custom.php&quot;);" href="#" class="smallLinkGrey">drawScale.custom</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.dates.php">&nbsp;<a onclick="render(&quot;example.drawScale.dates.php&quot;);" href="#" class="smallLinkGrey">drawScale.dates</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.floating.php">&nbsp;<a onclick="render(&quot;example.drawScale.floating.php&quot;);" href="#" class="smallLinkGrey">drawScale.floating</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.labels.php">&nbsp;<a onclick="render(&quot;example.drawScale.labels.php&quot;);" href="#" class="smallLinkGrey">drawScale.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.manual.php">&nbsp;<a onclick="render(&quot;example.drawScale.manual.php&quot;);" href="#" class="smallLinkGrey">drawScale.manual</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.multiple.php">&nbsp;<a onclick="render(&quot;example.drawScale.multiple.php&quot;);" href="#" class="smallLinkGrey">drawScale.multiple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.php">&nbsp;<a onclick="render(&quot;example.drawScale.php&quot;);" href="#" class="smallLinkGrey">drawScale</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScale.reverse.php">&nbsp;<a onclick="render(&quot;example.drawScale.reverse.php&quot;);" href="#" class="smallLinkGrey">drawScale.reverse</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Scatter chart&quot;);" id="Scatter chart_main" class="folder">&nbsp;Scatter chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Scatter chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatter.shape.php">&nbsp;<a onclick="render(&quot;example.drawScatter.shape.php&quot;);" href="#" class="smallLinkGrey">drawScatter.shape</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatter.simple.php">&nbsp;<a onclick="render(&quot;example.drawScatter.simple.php&quot;);" href="#" class="smallLinkGrey">drawScatter.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterLineChart.php">&nbsp;<a onclick="render(&quot;example.drawScatterLineChart.php&quot;);" href="#" class="smallLinkGrey">drawScatterLineChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterLineChart.trigo.php">&nbsp;<a onclick="render(&quot;example.drawScatterLineChart.trigo.php&quot;);" href="#" class="smallLinkGrey">drawScatterLineChart.trigo</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterPlotChart.php">&nbsp;<a onclick="render(&quot;example.drawScatterPlotChart.php&quot;);" href="#" class="smallLinkGrey">drawScatterPlotChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterScale.manual.php">&nbsp;<a onclick="render(&quot;example.drawScatterScale.manual.php&quot;);" href="#" class="smallLinkGrey">drawScatterScale.manual</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterScale.php">&nbsp;<a onclick="render(&quot;example.drawScatterScale.php&quot;);" href="#" class="smallLinkGrey">drawScatterScale</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawScatterSplineChart.php">&nbsp;<a onclick="render(&quot;example.drawScatterSplineChart.php&quot;);" href="#" class="smallLinkGrey">drawScatterSplineChart</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Spline chart&quot;);" id="Spline chart_main" class="folder">&nbsp;Spline chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Spline chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFilledSplineChart.php">&nbsp;<a onclick="render(&quot;example.drawFilledSplineChart.php&quot;);" href="#" class="smallLinkGrey">drawFilledSplineChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFilledSplineChart.threshold.php">&nbsp;<a onclick="render(&quot;example.drawFilledSplineChart.threshold.php&quot;);" href="#" class="smallLinkGrey">drawFilledSplineChart.threshold</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSplineChart.php">&nbsp;<a onclick="render(&quot;example.drawSplineChart.php&quot;);" href="#" class="smallLinkGrey">drawSplineChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSplineChart.reversed.php">&nbsp;<a onclick="render(&quot;example.drawSplineChart.reversed.php&quot;);" href="#" class="smallLinkGrey">drawSplineChart.reversed</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSplineChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawSplineChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawSplineChart.simple</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Split chart&quot;);" id="Split chart_main" class="folder">&nbsp;Split chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Split chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.split.php">&nbsp;<a onclick="render(&quot;example.split.php&quot;);" href="#" class="smallLinkGrey">split</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Spring chart&quot;);" id="Spring chart_main" class="folder">&nbsp;Spring chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Spring chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.spring.complex.php">&nbsp;<a onclick="render(&quot;example.spring.complex.php&quot;);" href="#" class="smallLinkGrey">spring.complex</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.spring.labels.php">&nbsp;<a onclick="render(&quot;example.spring.labels.php&quot;);" href="#" class="smallLinkGrey">spring.labels</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.spring.php">&nbsp;<a onclick="render(&quot;example.spring.php&quot;);" href="#" class="smallLinkGrey">spring</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.spring.relations.php">&nbsp;<a onclick="render(&quot;example.spring.relations.php&quot;);" href="#" class="smallLinkGrey">spring.relations</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Stacked chart&quot;);" id="Stacked chart_main" class="folder">&nbsp;Stacked chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Stacked chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedAreaChart.normalized.php">&nbsp;<a onclick="render(&quot;example.drawStackedAreaChart.normalized.php&quot;);" href="#" class="smallLinkGrey">drawStackedAreaChart.normalized</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedAreaChart.php">&nbsp;<a onclick="render(&quot;example.drawStackedAreaChart.php&quot;);" href="#" class="smallLinkGrey">drawStackedAreaChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedAreaChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawStackedAreaChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawStackedAreaChart.simple</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.border.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.border.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart.border</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.can.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.can.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart.can</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.pyramid.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.pyramid.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart.pyramid</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.rounded.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.rounded.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart.rounded</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStackedBarChart.shaded.php">&nbsp;<a onclick="render(&quot;example.drawStackedBarChart.shaded.php&quot;);" href="#" class="smallLinkGrey">drawStackedBarChart.shaded</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Step chart&quot;);" id="Step chart_main" class="folder">&nbsp;Step chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Step chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawFilledStepChart.php">&nbsp;<a onclick="render(&quot;example.drawFilledStepChart.php&quot;);" href="#" class="smallLinkGrey">drawFilledStepChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStepChart.php">&nbsp;<a onclick="render(&quot;example.drawStepChart.php&quot;);" href="#" class="smallLinkGrey">drawStepChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStepChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawStepChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawStepChart.simple</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Stock chart&quot;);" id="Stock chart_main" class="folder">&nbsp;Stock chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Stock chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStockChart.median.php">&nbsp;<a onclick="render(&quot;example.drawStockChart.median.php&quot;);" href="#" class="smallLinkGrey">drawStockChart.median</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStockChart.php">&nbsp;<a onclick="render(&quot;example.drawStockChart.php&quot;);" href="#" class="smallLinkGrey">drawStockChart</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-noleaf.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawStockChart.simple.php">&nbsp;<a onclick="render(&quot;example.drawStockChart.simple.php&quot;);" href="#" class="smallLinkGrey">drawStockChart.simple</a></div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="">
 <tbody><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/folder.png"></td>
  <td><div onclick="showHideMenu(&quot;Surface chart&quot;);" id="Surface chart_main" class="folder">&nbsp;Surface chart</div></td>
 </tr>
</tbody></table>
<table cellspacing="0" cellpadding="0" noborder="" style="display: none;" id="Surface chart"><tbody><tr>
 </tr><tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-blank.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawContour.php">&nbsp;<a onclick="render(&quot;example.drawContour.php&quot;);" href="#" class="smallLinkGrey">drawContour</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-blank.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSurface.palette.php">&nbsp;<a onclick="render(&quot;example.drawSurface.palette.php&quot;);" href="#" class="smallLinkGrey">drawSurface.palette</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-blank.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSurface.php">&nbsp;<a onclick="render(&quot;example.drawSurface.php&quot;);" href="#" class="smallLinkGrey">drawSurface</a></div></td>
 </tr>
 <tr valign="middle">
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-blank.png"></td>
  <td><img width="16" height="20" alt="" src="resources/dash-explorer-last.png"></td>
  <td><img width="16" height="16" alt="" src="resources/application_view_tile.png"></td>
  <td><div id="example.drawSurface.simple.php">&nbsp;<a onclick="render(&quot;example.drawSurface.simple.php&quot;);" href="#" class="smallLinkGrey">drawSurface.simple</a></div></td>
 </tr>
</tbody></table>
</div>
</td></tr></tbody></table>
</td><td width=20></td>

<td valign='top' style='padding-top: 5px; font-size: 12px;'>

<div style='display:table-cell; padding: 10px; border: 2px solid #FFFFFF; vertical-align: middle; overflow: auto; background-image: url("resources/dash.png");'>
 <div style='font-size: 10px;' id=render>
  <table><tr><td><img src='resources/accept.png' width=16 height=16 alt=""/></td><td>Click on an example to render it!</td></tr></table>
 </div>
</div>

<br/><br/>

<table><tr>
 <td><img src='resources/application_view_list.png' width=16 height=16 alt=''/></td>
 <td>&nbsp;Source area</td>
</tr></table>

<div style='display:table-cell; padding: 10px;  border: 2px solid #FFFFFF; vertical-align: middle; overflow: auto; background-image: url("resources/dash.png");'>
 <div style='font-size: 10px;' id='source' style='width: 700px;'>
  <table><tr><td><img src='resources/accept.png' width=16 height=16 alt=""/></td><td>Click on an example to get its source!</td></tr></table>
 </div>
</div>

</td></tr></table>
</body>
<script>
 URL        = "";
 SourceURL  = "";
 LastOpened = "";
 LastScript = "";

 function showHideMenu(Element)
  {
   if ( document.getElementById(Element).style.display == "none"  )
    {
     if ( LastOpened != "" && LastOpened != Element ) { showHideMenu(LastOpened); }

     document.getElementById(Element).style.display = "inline";
     document.getElementById(Element+"_main").style.fontWeight = "bold";
     LastOpened = Element;
    }
   else
    {
     document.getElementById(Element).style.display = "none";
     document.getElementById(Element+"_main").style.fontWeight = "normal";
     LastOpened = "";
    }
  }

 function render(PictureName)
  {
   if ( LastScript != "" ) { document.getElementById(LastScript).style.fontWeight = "normal"; }
   document.getElementById(PictureName).style.fontWeight = "bold";
   LastScript = PictureName;

   opacity("render",100,0,100);

   RandomKey = Math.random(100);
   URL       = PictureName + "?Seed=" + RandomKey;
   SourceURL = PictureName;

   ajaxRender(URL);
  }

 function StartFade()
  {
   Loader     = new Image();   
   Loader.src = URL;   
   setTimeout("CheckLoadingStatus()", 200);   
  }

 function CheckLoadingStatus()   
  {   
   if ( Loader.complete == true )   
    {
     changeOpac(0, "render");
     HTMLResult = "<center><img src='" + URL + "' alt=''/></center>";
     document.getElementById("render").innerHTML = HTMLResult;

     opacity("render",0,100,100);
     view(SourceURL);
    }
   else  
    setTimeout("CheckLoadingStatus()", 200);   
  }   

 function changeOpac(opacity, id)   
  {   
   var object = document.getElementById(id).style;   
   object.opacity = (opacity / 100);   
   object.MozOpacity = (opacity / 100);   
   object.KhtmlOpacity = (opacity / 100);   
   object.filter = "alpha(opacity=" + opacity + ")";   
  }   

 function wait()
  {
   HTMLResult = "<center><img src='resources/wait.gif' width=24 height=24 alt=''/><br>Rendering</center>";
   document.getElementById("render").innerHTML = HTMLResult;
   changeOpac(20, "render");
  }

 function opacity(id, opacStart, opacEnd, millisec)
  {
   var speed = Math.round(millisec / 100);
   var timer = 0;

   if(opacStart > opacEnd)
    {
     for(i = opacStart; i >= opacEnd; i--)
      {
       setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
       timer++;
      }
     setTimeout("wait()",(timer * speed));
    }
   else if(opacStart < opacEnd)
    {
     for(i = opacStart; i <= opacEnd; i++)
      {
       setTimeout("changeOpac(" + i + ",'" + id + "')",(timer * speed));
       timer++;
      }
    }
  }

 function ajaxRender(URL)
  {  
   var xmlhttp=false;   
   /*@cc_on @*/  
   /*@if (@_jscript_version >= 5)  
    try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (E) { xmlhttp = false; } }  
   @end @*/  
  
   if (!xmlhttp && typeof XMLHttpRequest!='undefined')   
    { try { xmlhttp = new XMLHttpRequest(); } catch (e) { xmlhttp=false; } }   
  
   if (!xmlhttp && window.createRequest)   
    { try { xmlhttp = window.createRequest(); } catch (e) { xmlhttp=false; } }   
  
   xmlhttp.open("GET", URL,true);

   xmlhttp.onreadystatechange=function() { if (xmlhttp.readyState==4) { StartFade();  } }   
   xmlhttp.send(null)   
  }

 function view(URL)
  {
   var xmlhttp=false;   
   /*@cc_on @*/  
   /*@if (@_jscript_version >= 5)  
    try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); } catch (E) { xmlhttp = false; } }  
   @end @*/  
  
   URL = "index.php?Action=View&Script=" + URL;

   if (!xmlhttp && typeof XMLHttpRequest!='undefined')   
    { try { xmlhttp = new XMLHttpRequest(); } catch (e) { xmlhttp=false; } }   
  
   if (!xmlhttp && window.createRequest)   
    { try { xmlhttp = window.createRequest(); } catch (e) { xmlhttp=false; } }   
  
   xmlhttp.open("GET", URL,true);

   xmlhttp.onreadystatechange=function() { if (xmlhttp.readyState==4) { Result = xmlhttp.responseText; document.getElementById("source").innerHTML = Result.replace("/\<BR\>/");  } }   
   xmlhttp.send(null)   
  }
</script>

<?php

 function goCheck($Script)
  {
   $Script = stripslashes($Script);
   $Script = preg_replace("/\//","",$Script);
   $Script = preg_replace("/\:/","",$Script);

   if ( file_exists($Script) ) 
    { highlight_file($Script); }
   else
    { echo "Script source code cannot be fetched."; }
   exit();
  }

 function size($Value)
  {
   if ( $Value < 1024 ) { return($Value." o."); }
   if ( $Value >= 1024 && $Value < 1024000 ) { return(floor($Value/1024)." ko."); }
   return(floor($Value/1024000))." mo.";
  }

 function left($value,$NbChar)  
  { return substr($value,0,$NbChar); }  
 
 function right($value,$NbChar)  
  { return substr($value,strlen($value)-$NbChar,$NbChar); }  
 
 function mid($value,$Depart,$NbChar)  
  { return substr($value,$Depart-1,$NbChar); }  
?>

<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>