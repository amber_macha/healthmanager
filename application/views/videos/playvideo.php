<script>
$(function() {	
	// setup player without "internal" playlists
	$f("player2", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
		clip: {  autoPlay: true,
                 autoBuffering: true,
				 baseUrl: '<?php echo base_url();?>'
			  } 		
	// use playlist plugin. again loop is true
	}).playlist("div.petrol", {loop:true});	
});
</script>

<a class="player plain" id="player2" style="float: left;">
	<a href="<?php echo base_url(); ?>videos/<?php echo $video_title; ?>"></a>
</a>

<div class="clips petrol" style="float:left;display:none;">
<a href="<?php echo base_url(); ?>videos/<?php echo $video_title; ?>"></a>
</div>
