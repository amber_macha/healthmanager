<style>fieldset div.field_row { font-weight: normal;}
fieldset div.field_row label {width: 125px;}
fieldset div.field_row div.form_field select {width:193px;}
#groups_sec{display:none;}
#sub_groups_sec{display:none;}
#employee_sec{display:none;}
#search_patinet_spiner2 {
    display: none;
    margin-left: 220px;
    margin-top: 8px;
    position: absolute;
    z-index: 5000;
}
.ui-menu { margin: 0 136px;}

</style>
<?php 
if($view_type=='show'){ $form_input ="form_input_readonly"; $form_textarea = 'form_textarea_readonly'; }
else { $form_input='form_input'; $form_textarea = 'form_textarea'; }
echo form_open('mailbox/save',array('id'=>'report_form','onsubmit'=>'return SendEmail();'));?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<fieldset id="customer_basic_info">
<legend><?php echo lang("mailbox_form_title"); ?></legend>

<?php if($view_type=='send'){?>
<div class="field_row clearfix">	
	<label><?php echo lang('mailbox_selection_list'); ?>:</label>	
	<div class="form_field">
		<select id="email_type" name="email_type" onclick="clicktohide(this);" onchange="viewActionData(this.value)">
			<option value="0"><?php echo lang('mailbox_all_employee'); ?></option>
			<option value="1"><?php echo lang('mailbox_group_message'); ?></option>
			<option value="2"><?php echo lang('mailbox_individual_employee'); ?></option>
		</select>
	</div>
</div>	
			
<div class="field_row clearfix" id="groups_sec">
<?php echo form_label(lang('common_group_name').':', 'group_name',array('class'=>'required')); ?>
	<div class='form_field'>
		<select class="valid" id="group_name" name="group_name" style="width: 193px;" onclick="clicktohide(this);" onchange="GetSubCategory(this.value);">
			<option value=""><?php echo lang('common_select_type');?></option> 
			<?php if(isset($GroupsData) && $GroupsData!='' && $GroupsData->num_rows()>0){
			foreach($GroupsData->result() as $rowdata){?>
			<option value="<?=$rowdata->id;?>"><?=ucfirst($rowdata->group_name);?></option>
		 <?php }}?>
		</select>	
			<img id="search_group" class="searchgroup" src="<?php echo base_url();?>images/spinner_small.gif">
	</div>
</div>
<div id="sub_groups_sec"><?php echo $SubGroupsData;?></div>

<div class="field_row clearfix showhide_css" id="employee_sec">	
<?php echo form_label(lang('mailbox_employee_name').':', 'employee_name',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'employee_name','id'=>'employee_name','value'=>'','onclick'=>'clicktohide(this);','style'=>'width:180px'));?>
		<img id="search_patinet_spiner2" src="<?php echo base_url();?>images/spinner_small.gif" style="margin-left: 0px;">
		<div id="showlist2"></div>
		<input type="hidden" value="" name="employee_id" id="employee_id">
	</div>
</div>
<?php }?>
<div class="field_row clearfix">	
<?php echo form_label(lang('mailbox_subject').':', 'subject',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'mail_subject','id'=>'mail_subject','value'=>$item_info->mail_subject,'onclick'=>'clicktohide(this);','style'=>'width:360px'));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('mailbox_description').':', 'mail_body',array('class'=>'required')); ?>
	<div class='form_field'><?php echo $form_textarea(array('name'=>'mail_content','id'=>'mail_content','rows'=>10,'cols'=>60,'value'=>$item_info->mail_body,'onclick'=>'clicktohide(this);'));?></div>
</div>	

<?php if($view_type=='send'){ echo form_submit(array('name'=>'save','value'=>lang('mailbox_send_mail'),'class'=>'submit_button float_right'));}
	else { echo form_buttons(array('name'=>'save','value'=>lang('common_close'),'class'=>'delete-button','onclick'=>'close_view_mailbox_form();'));} ?>
</fieldset>
<?php echo form_close();?>


<script type='text/javascript'>

function SendEmail()
{	
	var email_type = $('#email_type').val();

	if($('#email_type').val()==''){ $("#email_type").css("background", "#f2dede");$("#email_type").css("border", "1px solid #FF0000");	return false; }
	if(email_type==1)
	{
		if($('#group_name').val()=='') { $("#group_name").css("background", "#f2dede");$("#group_name").css("border", "1px solid #FF0000");	return false; }
	}
	if(email_type==2)
	{
		if($('#employee_id').val()=='' || $('#employee_name').val()=='') { $("#employee_name").css("background", "#f2dede");$("#employee_name").css("border", "1px solid #FF0000");	return false; }
	}	
	if($('#mail_subject').val()=='') { $("#mail_subject").css("background", "#f2dede");$("#mail_subject").css("border", "1px solid #FF0000"); return false; }
	if($('#mail_content').val()=='') { $("#mail_content").css("background", "#f2dede");$("#mail_content").css("border", "1px solid #FF0000"); return false; }
	
	$('#loadmasksec').show();
	var searchURL ='<?php echo base_url();?>mailbox/save' ;
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'email_type=' + email_type + '&group_name=' + $('#group_name').val() + '&sub_group_name=' + $('#sub_group_name').val() + '&employee_id=' + $('#employee_id').val() + '&mail_subject=' + $('#mail_subject').val() + '&mail_content=' + $('#mail_content').val(),
			dataType:'json',
            success: function(response) 
			{  $('#loadmasksec').hide();
			   tb_remove();
			   post_person_form_submit(response);
			}
        }); 
	return false;
}

$('#employee_name').keyup(function(e) { 
		$('#employee_id').val(''); 
		var existingString = $("#employee_name").val();
		if(existingString==''){ return false; }
		clearTimeout($.data(this, 'timer'));
		if (e.keyCode == 13)
		search_employee(existingString);
		else   $(this).data('timer', setTimeout(search_employee(existingString), 500));
});

function search_employee(existingString) { 
	var searchURL ='<?php echo base_url();?>employees/search_employee/';
	$('#search_patinet_spiner2').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'search_keyword='+existingString,
            success: function(data) 
			{   
			    $('#showlist2').html(data);  
			    $('#search_patinet_spiner2').hide(); 
			}
        }); 
}

function getPFnumberData(eid,ef_name)
{  $('#employee_id').val(eid); 
   $('#employee_name').val(ef_name); 
   $('#showlist2').html('');      
}

function viewActionData(type)
{  
	$('#groups_sec').hide(); $('#sub_groups_sec').hide(); $('#employee_sec').hide(); $('#group_name').val('');  $('#sub_group_name').val('');  $('#employee_name').val('');$('#employee_id').val('');
	if(type==1){ $('#groups_sec').show(); }
	if(type==2){ $('#employee_sec').show(); }
}

function GetSubCategory(group_id)
{  
  if(group_id=='') { return false; }
  var sub_group_id = $("#sub_group_id").val(); 
  var searchURL = "<?php echo base_url().'employees/get_SubGroupList/'?>";
  $('#search_group').show();	
	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'group_name='+group_id + '&sub_group_name=' + sub_group_id,
			dataType:'json',
            success: function(response) 
			{   $('#sub_groups_sec').show();	
				$('#search_group').hide();	
			    if(response.success) { $('#sub_groups_sec').html(response.groupData); }					    
			}
        }); 
}

function getStatus(status_type)
{
  if(status_type==3) { $('#reject_reason_sec').show(); } 
  else { $('#reject_reason_sec').hide(); $('#reject_reason').val(''); }
}


</script>