<?php echo form_open('eyeclinic/savereport/'.$person_id.'/'.$invest_id,array('id'=>'history_form')); ?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("eyeclinic_laboratory_report_post"); ?></legend>
<div class="field_row clearfix">	
<?php echo form_label(lang('eyeclinic_laboratory_report_title').'', 'eyeclinic_report',array('class'=>'required add-post-report-title')); ?>
	<div class="form_field">
	<?php echo form_textarea(array(
		'name'=>'laboratory_report',
		'id'=>'laboratory_report',
		'rows'=>'15',
		'cols'=>'60')		
	);?>
	</div>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right add-post-report')
);?>
</fieldset>
<?php echo form_close();?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
			});		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			laboratory_report: "required"
   		},
		messages: 
		{     		
     		laboratory_report: <?php echo json_encode(lang('eyeclinic_laboratory_report_post')); ?>
		}
	});
});
</script>