<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html lang="en-GB"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="<?php echo base_url();?>css/login/screen.css?<?php echo APPLICATION_VERSION; ?>" rel="stylesheet" type="text/css">
<link type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" rel="shortcut icon">    
<title><?php echo lang('login_reset_password'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function()
{
$("#login_form input:first").focus();
});
</script>
 </head>

<script type="text/javascript">
$(document).ready(function() {	$("#login_form input:first").focus();});

function validate_resetpass()
{ 
	if(document.getElementById('username_or_email').value==''){ $("#username_or_email").css("background", "#FFC0CB");$("#username_or_email").css("border", "1px solid #FF0000");return false;}	
}
function clicktohide(obj){document.getElementById(obj.id).style.background = ""; document.getElementById(obj.id).style.border = ""; }
</script>
 <body class="modal-form">

<div class="tr_logo">
  <p id="tr_head"><?=lang('login_welcome_message');?></p>
  <p id="tr_tail"><?=$this->config->item('company');?></p>
</div>

<div class="modal-form">



<div class="tr_logp"> <p><?php echo lang('login_reset_password');?></p></div>

<fieldset id="config_info">
<?php if (validation_errors()) {?>	<div id="welcome_message" class="top_message_error"><?php echo validation_errors(); ?>	</div><?php } ?>
<?php echo form_open('login/do_reset_password_notify',array('onsubmit'=>'return validate_resetpass();','autocomplete'=>'off'))?>
<div id="tr_usr"  style="width: 135px;"><label for="horde_user"><?php echo lang('login_username'); ?> / <?php echo lang('common_email'); ?></label></div>
<div id="tr_input">
    <input class="tr_uname" autocapitalize="off" autocorrect="off" id="username_or_email" name="username_or_email" style="direction:ltr" type="text" onclick="clicktohide(this)">
  </div>


<div> <input id="login-button" name="login_button" class="horde-default submit-button" style="width: 200px;" value="<?php echo lang('login_reset_password');?>" type="submit"></div>
<div class="tr_advert">
<p>Visit the Government Portal <a href="<?php echo base_url();?>" target="_blank">www.gove.phpsyntax.co.uk</a>
  “A One Stop Centre for Public Information and Services”. For technical
 inquiry on services provided by eGA, email us on : <?php echo $this->config->item('default_contact_us');?></p>
</div>

</form>
</fieldset>
</div>

</body></html>