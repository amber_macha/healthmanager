<?php $this->load->view("partial/header"); ?><style>.login_form{ margin-top: 50px;} fieldset div.field_row label { margin-left: 200px; width: 120px;}fieldset input[type="submit"] { margin-left: 340px;}#content_area_wrapper {min-height: 470px;}</style><script>function validate_login(){ 	if(document.getElementById('user_password').value=='')	{  	  $("#user_password").css("background", "#FFC0CB");$("#user_password").css("border", "1px solid #FF0000");	return false; 	}	var authrizeUrl = "<?php echo base_url().'home/authorization_login';?>";	$('#spinner').show(); 	$.ajax({			url: authrizeUrl,			type: 'post',			data: $("#SignForm").serialize(),			dataType:'json',			success: function(response)			{ $('#spinner').hide(); 			  if(!response.success)				{					set_feedback(response.message,'error_message',true);				}			 				else				{					set_feedback(response.message,'success_message',false);						setTimeout(function () { window.location.href = response.current_siteurl; }, '2000');				}										}		});		return false;			}</script>
<?php echo form_open('home/authorization_login',array('id'=>'SignForm','onsubmit'=>'return validate_login();'));?>
<div id="config_wrapper">	
		<fieldset id="config_info">
		<legend><?php echo lang("config_member_authorization_form"); ?></legend>				<div class="login_form">	
				<div class="field_row clearfix">	
				<?php echo form_label(lang('employees_password').':', 'password'); ?>					<div class='form_field'>
					<?php echo form_password(array('name'=>'user_password','id'=>'user_password','style'=>'width:250px','onclick'=>'clicktohide(this)','value'=>''));?>
					</div>
				</div>
			<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>			<span id="spinner"><img src='<?php echo base_url()?>images/loading.gif'/></span>		</div>	
		</fieldset>
</div>
<?php echo form_close();?>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>


