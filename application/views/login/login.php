<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html lang="en-GB"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="<?php echo base_url();?>css/login/screen.css?<?php echo APPLICATION_VERSION; ?>" rel="stylesheet" type="text/css">
<link type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" rel="shortcut icon">    
	  <title>GoT HoMIS <?php echo lang('login_login'); ?></title>
 </head>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>

<script type="text/javascript">
$(document).ready(function() {	$("#login_form input:first").focus();});

function validate_login()
{ 
	if(document.getElementById('username').value==''){ $("#username").css("background", "#FFC0CB");$("#username").css("border", "1px solid #FF0000");return false;}	
	if(document.getElementById('password').value==''){ $("#password").css("background", "#FFC0CB");$("#password").css("border", "1px solid #FF0000");return false; }		
}
function clicktohide(obj){document.getElementById(obj.id).style.background = ""; document.getElementById(obj.id).style.border = ""; }
</script>
 <body class="modal-form">

<div class="tr_logo">
  <p id="tr_head"><?=lang('login_welcome_message');?></p>
  <p id="tr_tail"><?=$this->config->item('company');?></p>
</div>

<div class="modal-form">



<div class="tr_logp"> <p><?=lang('common_home_page_logo_title');?></p></div>

<fieldset id="config_info">
<legend><?=lang('login_form_title');?></legend>
<?php if (validation_errors()) {?>	<div id="welcome_message" class="top_message_error"><?php echo validation_errors(); ?>	</div><?php } ?>
<?php echo form_open('login',array('onsubmit'=>'return validate_login();','autocomplete'=>'off'))?>
<div id="tr_usr"><label for="horde_user"><?php echo lang('login_username'); ?></label></div>
<div id="tr_input">
    <input class="tr_uname" autocapitalize="off" autocorrect="off" id="username" name="username" style="direction:ltr" type="text" onclick="clicktohide(this)">

  </div>

<div id="tr_usr"><label for="horde_pass"><?php echo lang('login_password'); ?></label></div>
<div id="tr_input">
    <input class="tr_upass" id="password" name="password" value="" style="direction:ltr" type="password" onclick="clicktohide(this)">
  </div>


<div id="horde_select_view_div" style="" class="tr_hsel">
 <div id="tr_slabel"><label for="horde_select_view"><?php echo lang('login_mode'); ?></label></div>
 <div>
  <select id="horde_select_view" name="horde_select_view" class="tr_sview">
   <option selected="selected" value="auto"><?php echo lang('login_automatic'); ?></option>
   <option value="" disabled="disabled">- - - - - - - - - -</option>
   <!--option value="basic" selected="selected">Basic</option-->
   <option value="dynamic">Computer (PC/Laptop)</option>
   <option value="smartmobile">Mobile (Smartphone/Tablet)</option>
   <option value="mobile">Mobile (Minimal)</option>
   
  </select>
 </div>
</div>

<div> <input id="login-button" name="login_button" class="horde-default submit-button" value="Log in" type="submit"></div>
<div class="reset_password_sec"><div class="reset_password"><?php echo anchor('login/reset_password', lang('login_reset_password')); ?></div></div>
<div class="tr_advert">
<!--<p>Visit the Government Portal <a href="<?php echo base_url();?>" target="_blank">www.gove.phpsyntax.co.uk</a>
  “A One Stop Centre for Public Information and Services”. For technical
 inquiry on services provided by eGA, email us on : <?php echo $this->config->item('default_contact_us');?></p>
 -->
</div>

</form>
</fieldset>
</div>

</body></html>