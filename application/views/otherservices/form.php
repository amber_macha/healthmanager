<style>.field_row div.form_field input {  width: auto;}</style>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("otherservice_header"); ?></legend>
<div class="form_field"> <label class="required lab-search-2"><?php echo lang('lab_investigation_category'); ?> :</label>
<label class="add_new_lab_search" for="first_name" style="margin-left: 168px;"><?php echo lang('search_lab_services'); ?>: 
<img id="searchspiner" class="search-spiner" src="<?php echo base_url();?>images/spinner_small.gif">
<input type="text" class="lab-search-box" id="search_lab_service" value="" name="search_lab_service">
</label>	
</div>
<?php echo form_open('otherservice/save/'.$person_id,array('id'=>'history_form','onsubmit'=>'return validateCustomer()')); ?>
	<input type="hidden" name="category_list" id="category_list" value="">
		<div class="field_row clearfix" style="margin:0px;">	
			<div class='form_field'>		
				<div id="labcategory" class="labcategory_div">
					<?php echo $lab_category_list; ?>
				</div>
			</div>
		</div>
<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
<input id="active_deactive" class="active_deactive" type="button" onclick="ActiveDeActivate();" value="<?php echo $this->lang->line("active_deactive_lab_servics");?>">

<?php echo form_close();?>
</fieldset>
<script type='text/javascript'>
//**Active Deactivate Lab Services code**//
function ActiveDeActivate()
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('otherservice_services_empty_change_status');?>');return false; }  
	if (confirm('<?php echo lang('lab_services_change_status');?>')) 
	{
	var searchURL ='<?php echo base_url().'otherservice/activeDeActivate/'.$person_id.'/';?>' +checkedval;
	$('#searchspiner').show();
	$.get(searchURL, function(data) { $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').html(''); } , 5000); search(true); });
	}	
	else{return false;}
}
//**************lab searching code*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});
function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  	var searchURL ='<?php echo base_url();?>otherservice/getservices/';	$('#searchspiner').show();	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval,
            success: function(data) { $('#labcategory').html(data);  $('#searchspiner').hide(); }
        }); }
//**************lab searching code*********************//
function validateCustomer()
{
	var n = $( "input:checked" ).length;
	if(n==0){ return false; }
	else { 
		$(':checkbox:checked').each(function(i){
		if($(this).val()==0){ alert('<?php echo lang('otherservice_services_empty_lab_services_to_added');?>'); $('#lab_'+id).attr('checked', false); return false; }				}); 
	document.getElementById('category_list').value = n;  
	} 
 }  
//validation and submit handling
$(document).ready(function()
{	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			category_list: "required"
   		},
		messages: 
		{     		
     		category_list: <?php echo json_encode(lang('otherservice_services_empty_lab_services_to_added')); ?>
		}	});});
</script>