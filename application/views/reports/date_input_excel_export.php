<?php $this->load->view("partial/header"); ?><style></style>
<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>	<div id='report_date_range_simple'>
		<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>		<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, '', 'id="report_date_range_simple"'); ?>	</div>	
	<div id='report_date_range_complex'>
		<input type="radio" name="report_type" id="complex_radio" value='complex' />
		<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
		<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
		<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>		-
		<?php echo form_dropdown('end_month',$months, $selected_month, 'id="end_month"'); ?>
		<?php echo form_dropdown('end_day',$days, $selected_day, 'id="end_day"'); ?>
		<?php echo form_dropdown('end_year',$years, $selected_year, 'id="end_year"'); ?>
	</div>
	<?php if($input_label=='customerssummary') { $sex=array('all'=>lang('common_select_type'),'Male'=>'Male','Female'=>'Female');?>
	<br />		<table style="width:90%">		 <?php if(basename($_SERVER['REQUEST_URI'])!='summary_categories' && basename($_SERVER['REQUEST_URI'])!='summary_discounts' && basename($_SERVER['REQUEST_URI'])!='summary_items' && basename($_SERVER['REQUEST_URI'])!='summary_payments' && basename($_SERVER['REQUEST_URI'])!='summary_suppliers'				  && basename($_SERVER['REQUEST_URI'])!='summary_taxes' && basename($_SERVER['REQUEST_URI'])!='detailed_receivings' && basename($_SERVER['REQUEST_URI'])!='deleted_sales' && basename($_SERVER['REQUEST_URI'])!='deleted_sales'){?>
		<tr><td>Sex : </td><td><?php echo form_dropdown('sex',$sex, 'male', 'id="sex"'); ?></td><td><b>AGE</b> &nbsp; &nbsp; &nbsp; From : </td><td><input type="text"  name="age_min" id="age_min" /> </td><td>To : </td><td><input type="text"  name="age_max" id="age_max" /> </td></tr>		<tr><td>&nbsp;</td></tr>		<tr>		<td><?=lang('common_registration_type');?> : </td>		<td id="view_left_menu_div"> <?php echo $this->Customer->Customer_Registration_Type_Dropdown('registration_type','registration_type','option_filter',$selected='');?></td>		<td><?=lang('common_patient_category_type');?> : </td>		<td id="view_left_menu_div"> <?php echo $this->Customer->Customer_Registration_Type_Dropdown('patient_category','patient_category','option_filter',$selected='');?></td>		<td>&nbsp;</td><td>&nbsp;</td>		</tr><?php }?>		</table>
	<?php } ?>
	<?php echo form_label(lang('reports_sale_type'), 'reports_sale_type_label', array('class'=>'required sales_type_hide')); ?>
	<div id='report_sale_type'  class="sales_type_hide">
		<?php echo form_dropdown('sale_type',array('all' => lang('reports_all'), 'sales' => lang('reports_sales'), 'returns' => lang('reports_returns')), 'all', 'id="sale_type"'); ?>	</div>	
	<div>
		<?php echo lang('reports_export_to_excel'); ?>: <input type="radio" name="export_excel" id="export_excel_yes" value='1' /> <?php echo lang('common_yes'); ?>		<input type="radio" name="export_excel" id="export_excel_no" value='0' checked='checked' /> <?php echo lang('common_no'); ?>	</div>
<?php echo form_button(array('name'=>'generate_report','id'=>'generate_report','content'=>lang('common_submit'),'class'=>'submit_button'));?>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{
		var sale_type = $("#sale_type").val();
		var export_excel = 0;
		if ($("#export_excel_yes").attr('checked'))
		{
			export_excel = 1;
		}
		if ($("#simple_radio").attr('checked'))
		{	
			<?php if($input_label=='customerssummary') {?>
			var F_String ='';		
			 <?php if(basename($_SERVER['REQUEST_URI'])!='summary_categories' && basename($_SERVER['REQUEST_URI'])!='summary_discounts' && basename($_SERVER['REQUEST_URI'])!='summary_items' && basename($_SERVER['REQUEST_URI'])!='summary_payments' && basename($_SERVER['REQUEST_URI'])!='summary_suppliers'				  && basename($_SERVER['REQUEST_URI'])!='summary_taxes' && basename($_SERVER['REQUEST_URI'])!='detailed_receivings' && basename($_SERVER['REQUEST_URI'])!='deleted_sales' && basename($_SERVER['REQUEST_URI'])!='deleted_sales'){?>			F_String = $("#sex").val();				F_String = F_String + '/' + $("#registration_type").val() + '/' + $("#patient_category").val();							if($("#age_min").val()!='') 			{ 	if (isNaN($("#age_min").val())) { alert("<?php echo lang('common_invalid_age_selection');?>");return false;  }				else { F_String = F_String + '/' + $("#age_min").val(); }						}						if($("#age_max").val()!='') 			{ 				if (isNaN($("#age_max").val())) { alert("<?php echo lang('common_invalid_age_selection');?>");return false;}				else { F_String = F_String + '/' + $("#age_max").val(); }			} 			<?php }?>					window.location = window.location+'/'+$("#report_date_range_simple option:selected").val() + '/'+sale_type+'/'+export_excel+'/'+ F_String;			<?php } else {?>
			window.location = window.location+'/'+$("#report_date_range_simple option:selected").val() + '/'+sale_type+'/'+export_excel;			<?php } ?>
		}
		else
		{	      
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();
			<?php if($input_label=='customerssummary') {?>			var F_String ='';
		   <?php if(basename($_SERVER['REQUEST_URI'])!='summary_categories' && basename($_SERVER['REQUEST_URI'])!='summary_discounts' && basename($_SERVER['REQUEST_URI'])!='summary_items' && basename($_SERVER['REQUEST_URI'])!='summary_payments' && basename($_SERVER['REQUEST_URI'])!='summary_suppliers'				  && basename($_SERVER['REQUEST_URI'])!='summary_taxes' && basename($_SERVER['REQUEST_URI'])!='detailed_receivings' && basename($_SERVER['REQUEST_URI'])!='deleted_sales' && basename($_SERVER['REQUEST_URI'])!='deleted_sales'){?>			F_String = $("#sex").val();						F_String = F_String + '/' + $("#registration_type").val() + '/' + $("#patient_category").val();						if($("#age_min").val()!='') 			{ 	if (isNaN($("#age_min").val())) { alert("<?php echo lang('common_invalid_age_selection');?>");return false;  }				else { F_String = F_String + '/' + $("#age_min").val(); }						}						if($("#age_max").val()!='') 			{ 				if (isNaN($("#age_max").val())) { alert("<?php echo lang('common_invalid_age_selection');?>");return false;}				else { F_String = F_String + '/' + $("#age_max").val(); }			}			<?php }?>				
			window.location = window.location+'/'+start_date + '/'+ end_date + '/'+sale_type+'/'+ export_excel+'/' + F_String;			<?php } else {?>
			window.location = window.location+'/'+start_date + '/'+ end_date + '/'+sale_type+'/'+ export_excel;			<?php } ?>
		}
	});	
	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()	{
		$("#complex_radio").attr('checked', 'checked');
	});	
	$("#report_date_range_simple").click(function()
	{
		$("#simple_radio").attr('checked', 'checked');
	});
});
</script>