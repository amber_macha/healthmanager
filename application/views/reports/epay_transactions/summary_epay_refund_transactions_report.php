<?php
if($export_excel == 1)
{
	$rows = array();
	$row = array();
	foreach($headers as $header) 
	{
		$row[] = strip_tags($header['data']);
	}
	
	$rows[] = $row;
	
	foreach($data as $datarow)
	{
		$row = array();
		foreach($datarow as $cell)
		{
			$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;
	}
	
	$content = array_to_csv($rows);	
	force_download(strip_tags($title) . '.csv', $content);
	exit;	
} 
	$arr1 = explode('-',$start_date); 
	$selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];

	$arr2 = explode('-',$end_date);
	$selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];

?>
<?php $this->load->view("partial/header"); ?>

<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
<div style="width:100%">	
		<span id='report_date_range_simple'>
			<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
			<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, $selected_date_range, 'id="report_date_range_simple"'); ?>	
		</span>
						<span id='report_date_range_complex' class="mthua-report">
							<input type="radio" name="report_type" id="complex_radio" value='complex'/>
							<?php echo form_dropdown('start_month',get_months(), $selected_month, 'id="start_month"'); ?>
							<?php echo form_dropdown('start_day',get_days(), $selected_day, 'id="start_day"'); ?>
							<?php echo form_dropdown('start_year',get_years(), $selected_year, 'id="start_year"'); ?>
							-
							<?php echo form_dropdown('end_month',get_months(),$selected_month2, 'id="end_month"'); ?>
							<?php echo form_dropdown('end_day',get_days(),$selected_day2, 'id="end_day"'); ?>
							<?php echo form_dropdown('end_year',get_years(), $selected_year2, 'id="end_year"'); ?>
						</span>
				
</div>	
<div style="width:200px;float:left;display:inline">	
<button id="generate_report" class="submit_button" style="display:inline" name="generate_report" type="button"><?=lang('common_submit');?></button>
<button style="display:inline;cursor: pointer;" class="submit_button" type="button" onclick="$.jPrintArea('#table_holder');"><?=$this->lang->line('print_out');?></button>
</div>

<table id="table_holder">
<tr><td>
<table id="title_bar">
		<tr> <td><div class="epay-report-title"><?php echo strtoupper(lang('report_epay_refund_transactions_reports')); ?></div></td> </tr> 
		<tr> <td><div class="hospital-title"><?php echo '<b>'.lang('common_hospital_name').'</b>'; ?> : <?=$this->config->item('company');?>
		<span class="hospital-title-right"><?php echo '<b>'.lang('common_district_name').'</b>';?> : <?=$this->config->item('district');?></span>
		</div>
		</td> </tr>
		<tr> <td>
			<div class="hospital-title2"style="text-align:center;margin-left:0px;">
				<?php echo '<b>'.lang('common_from').'</b>'; ?> : <?=$selected_day.'-'.$selected_month.'-'.$selected_year;?> -
				<?php echo '<b>'.lang('common_to').'</b>'; ?> : <?=$selected_day2.'-'.$selected_month2.'-'.$selected_year2;?>
			</div>
		</td> </tr>
	<tr>
		<td>&nbsp;</td>
		<td><small></small></td>
	</tr>
</table>
</tr></td>
<tr><td>
<div id="item_table">
	<div id="table_holder" style="width:100%">
			<table id="sortable_table" class="tablesorter report">
			<tr>
			<?php foreach ($headers as $header) { ?> 
			<th align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
			<?php } ?>			
			</tr>
			
			
       <?php $ListDiagnoses = $this->Customer->GetAllListDiagnoses('dead_body');	 
	        if($ReportsData->num_rows()>0){
			foreach($ReportsData->result() as $row){ 
			$Employee_Data = $this->Employee->get_info($row->employee_id);
			$edit_image ='<img title="'.lang('common_transactions_view_details').'" src="'.base_url().'images/show.gif">';
	   ?>
			<tr style="cursor: pointer;">
				<td width="15%"><?php echo ucwords($row->first_name) .' '.ucwords($row->last_name);?></td>
				<td width="13%"><?php echo $row->account_number;?></td>
				<td width="12%"><?php echo $row->health_card_no;?></td>
				<td width="10%"><?php echo $row->epay_transaction_id;?></td>
				<td width="10%"><?php echo to_currency($row->transaction_amount);?></td>
				<td width="10%"><?php echo date('d-m-Y H:i:s',strtotime($row->transaction_time));?></td>
				<td width="10%"><?php echo $row->computer_ip_address;?></td>
				<td width="15%"><?=ucwords(strtolower($Employee_Data->first_name)).' '. ucwords(strtolower($Employee_Data->last_name));?></td>
				<td><?php echo anchor($controller_name."/view_epay_transactions_summary/$row->transaction_id/width~550", $edit_image ,array('class'=>'thickbox','title'=>lang('common_transactions_view_details')));?></td>
			</tr>
        <?php }}?>

		</table>			
	</div>
		
		<div id="report_summary" class="tablesorter report" style="margin-right:2px;float: right;"><?php echo $SummaryData;?></div>		
</div>
<tr><td>
</table>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{		
		if ($("#simple_radio").attr('checked'))
		{			
		    window.location = '<?php base_url();?>reports/epay_refund_transactions_summary/' + $("#report_date_range_simple option:selected").val();
		}
		else
		{  
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();			
			window.location = '<?php base_url();?>reports/epay_refund_transactions_summary/'+start_date + '/'+ end_date ;
		}
	});	

	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});	

	$("#report_date_range_simple").click(function()
	{	
		$("#simple_radio").attr('checked', 'checked');
	});
});


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	

</script>