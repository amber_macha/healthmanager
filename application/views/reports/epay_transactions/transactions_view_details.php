<div style="color: #444444;float: left;margin-top: 10px;">	
		<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_name_of_patient');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=ucfirst($items_info->first_name).' '.ucfirst($items_info->last_name); ?></div>  </div> 
		<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('customers_account_number');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->account_number;?></div>  </div> 
		<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_health_card_no');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->health_card_no;?></div>  </div>   
		<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('customers_member_card_no');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->member_card_no; ?></div>  </div> 
</div> 
	
<div style="border-bottom: 2px solid #cccccc;float: left;margin-top: 10px;padding: 2px;width: 97%;"></div> 	
<div style="color: #444444;float: left;margin-top: 10px;">	
	<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('sales_trans_id');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->epay_transaction_id; ?></div>  </div> 
	<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('report_transactions_date');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=date('d-m-Y H:i:s',strtotime($items_info->transaction_time));?></div>  </div> 
	<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('sales_amount');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=to_currency($items_info->transaction_amount);?></div>  </div>   
	<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_cashiers_name');?>:</div> <div style=" color: #800000;font-size: 14px;"><?php $Employee_Data = $this->Employee->get_info($items_info->employee_id);echo ucwords(strtolower($Employee_Data->first_name)).' '. ucwords(strtolower($Employee_Data->last_name)) ;?></div>  </div> 
	<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_cashiers_computer_ip');?>:</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->computer_ip_address;?></div>  </div> 
</div> 

<div style="border-bottom: 2px solid #cccccc;float: left;margin-top: 10px;padding: 2px;width: 97%;"></div> 

<div style="color: #444444;float: left;margin-top: 10px;">	
		<div style="float: left;margin-left: 20px;padding: 1px;width: 97%;">
				<div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_transactions_status');?> :</div> 
				<div style=" color: #800000;font-size: 14px;">
					<?php if($items_info->transaction_status==0) echo lang('common_transactions_status_not_complete');
						  if($items_info->transaction_status==1) echo lang('common_transactions_status_complete');
						  if($items_info->transaction_status==2) echo lang('common_transactions_status_refund');?>
				</div>  
		</div>
	  <div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"></div> <div style=" color: #800000;font-size: 14px;"></div>  </div> 

	<?php if($items_info->transaction_status==2){?>	
		  <div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_transactions_refund_date');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=date('d-m-Y H:i:s',strtotime($items_info->refund_time)); ?></div>  </div> 
		  <div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_transactions_refund_employee_name');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->refund_by_epay_employee;?></div>  </div> 
		  <div style="float: left;margin-left: 20px;padding: 1px;width: 97%;"><div style="float: left;font-size: 14px; width: 230px;"><?php echo lang('common_transactions_refund_reason');?> :</div> <div style=" color: #800000;font-size: 14px;"><?=$items_info->refund_description;?></div>  </div> 
  <?php }?>
	</div> 
