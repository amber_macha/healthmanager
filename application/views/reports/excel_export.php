<?php $this->load->view("partial/header"); ?>
<style>.field_row{border-bottom: none;width:100%;float: left;}</style>
<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
 
?>
	<div>
		<?php echo lang('reports_export_to_excel');?>: <input type="radio" name="export_excel" id="export_excel_yes" value='1' /> <?php echo lang('common_yes'); ?>
		<input type="radio" name="export_excel" id="export_excel_no" value='0' checked='checked' /> <?php echo lang('common_no'); ?>
	</div>
	
	<?php if(basename($_SERVER['REQUEST_URI'])=='inventory_low' || basename($_SERVER['REQUEST_URI'])=='inventory_summary' ){?>
		<div class="field_row">
			<label class="wide" for="store_name" style=" width: 150px;"><?=lang('items_multi_store_name');?>:</label>	
			<div class='form_field' id='view_left_menu_div'>
				<select name="multi_store_item" id="multi_store_item" class="option_filter">
				<option value=""><?php echo lang('items_multi_store_selection');?></option>
				<?php if($multi_stores_items->num_rows()>0){
				foreach($multi_stores_items->result() as $rowdata){?>
				<option value="<?=$rowdata->id;?>"><?php echo ucfirst($rowdata->lab_name); ?></option>
				<?php }}?></select>
			</div>
		</div>
	<?php } ?>
	
<?php
echo form_button(array(
	'name'=>'generate_report',
	'id'=>'generate_report',
	'content'=>lang('common_submit'),
	'class'=>'submit_button')
);
?>

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{   var sel = '';
	$("#generate_report").click(function()
	{
		var export_excel = 0;
		if ($("#export_excel_yes").attr('checked'))
		{
			export_excel = 1;
		}
		if($("#multi_store_item").val()!=''){ sel = '/' + $("#multi_store_item").val(); }
		window.location = window.location+'/' + export_excel + sel;
	});	
});
</script>