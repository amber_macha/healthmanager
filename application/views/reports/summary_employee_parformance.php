<?php $this->load->view("partial/header"); ?>
<style>.report-title-sections2{width:70%;}</style>
<div class="report-title-sections2">
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('reports_welcome_message'); ?>' />
		</td>
		<td id="title"><?php echo lang('reports_reports'); ?> - <?php echo $title ?></td>
	</tr>
	
</table>
</div>
<div id="report-logo-sections"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>



<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
<div style="width:100%">	
		<span id='report_date_range_simple'>
			<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
			<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, $selected_date_range, 'id="report_date_range_simple"'); ?>	
		</span>
						<span id='report_date_range_complex' class="mthua-report">
							<input type="radio" name="report_type" id="complex_radio" value='complex'/>
							<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
							<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
							<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
							-
							<?php echo form_dropdown('end_month',$months, $selected_month2, 'id="end_month"'); ?>
							<?php echo form_dropdown('end_day',$days, $selected_day2, 'id="end_day"'); ?>
							<?php echo form_dropdown('end_year',$years, $selected_year2, 'id="end_year"'); ?>
						</span>
				
</div>	
<div style="width:200px;float:left;display:inline">	
<button id="generate_report" class="submit_button" style="display:inline" name="generate_report" type="button"><?=lang('common_submit');?></button>
<button style="display:inline;cursor: pointer;" class="submit_button" type="button" onclick="$.jPrintArea('#item_table');"><?=$this->lang->line('print_out');?></button>
</div>

<br/>


<br />
<table id="contents">
	<tr>
		<td id="item_table">
			<div id="table_holder" style="width: 960px;">
				<table class="tablesorter report" id="sortable_table">
					<thead>
						<tr>
							<?php foreach ($headers as $header) { ?>
							<th align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $row) { ?>
						<tr>
							<?php foreach ($row as $cell) { ?>
							<td align="<?php echo $cell['align'];?>"><?php echo $cell['data']; ?></td>
							<?php } ?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>	
				<div id="report_summary" class="tablesorter report" style="margin-right:2px;float: right;">
					<div class="summary_row"><strong><?php echo lang('common_total_lab_investigation');?></strong>: <?php echo $Total_Lab_Reports;?></div>
					<div class="summary_row"><strong><?php echo lang('common_total_uss_xrays');?></strong>: <?php echo $Total_USS_XRAYS_Reports;?></div>
					<div class="summary_row"><strong><?php echo lang('common_total_eyeclinic');?></strong>: <?php echo $Total_EYECLINIC_Reports;?></div>	
					<div class="summary_row"><strong><?php echo lang('common_total_specialist');?></strong>: <?php echo $Total_SPECILIST_Reports;?></div>
					<div class="summary_row"><strong><?php echo lang('common_total_otherservices');?></strong>: <?php echo $Total_OTHERSERVICES_Reports;?></div>	
					<div class="summary_row"><strong><?php echo lang('reports_total');?></strong>: <?php echo $Total_Reports;?></div>	
				</div>
			
		</td>
	</tr>
</table>


<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{		
		if ($("#simple_radio").attr('checked'))
		{			
		    window.location = '<?php base_url();?>reports/employee_parformance_summary' +'/'+$("#report_date_range_simple option:selected").val();
		}
		else
		{  
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();			
			window.location = '<?php base_url();?>reports/employee_parformance_summary/'+start_date + '/'+ end_date ;
		}
	});	

	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});	

	$("#report_date_range_simple").click(function()
	{		
		$("#simple_radio").attr('checked', 'checked');
	});
});


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	

</script>