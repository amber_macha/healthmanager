<?php
 $start_date = $selected_year.'-'.$selected_month.'-'.$selected_day; $end_date = $selected_year2.'-'.$selected_month2.'-'.$selected_day2;
?>
<?php $this->load->view("partial/header"); ?>

<div class="report-title-sections" style=" width: 86%;">
	<table id="title_bar">
		<tr>
			<td id="title_icon">
				<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('tika_pateint_report_summary'); ?>' />
			</td>
			<td id="title"><?=lang('reports_reports'); ?> - <?=lang('tika_pateint_report_summary'); ?></td>
		</tr>	
	</table>

	<?php if(isset($error)){ echo "<div class='error_message'>".$error."</div>"; }?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
	<div style="width:100%">	
			<span id='report_date_range_simple'>
				<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
				<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, $selected_date_range, 'id="report_date_range_simple"'); ?>	
			</span>
							<span id='report_date_range_complex' class="mthua-report">
								<input type="radio" name="report_type" id="complex_radio" value='complex'/>
								<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
								<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
								<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
								-
								<?php echo form_dropdown('end_month',$months, $selected_month2, 'id="end_month"'); ?>
								<?php echo form_dropdown('end_day',$days, $selected_day2, 'id="end_day"'); ?>
								<?php echo form_dropdown('end_year',$years, $selected_year2, 'id="end_year"'); ?>
							</span>				
	</div>	
</div>
<div class="report-logo-sections"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>
	
<div style="width:200px;float:left;display:inline; margin-bottom: 12px;">	
<button id="generate_report" class="submit_button" style="display:inline" name="generate_report" type="button"><?=lang('common_submit');?></button>
<button style="display:inline;cursor: pointer;" class="submit_button" type="button" onclick="$.jPrintArea('#table_holder');"><?=$this->lang->line('print_out');?></button>
</div>


<table id="table_holder" style="width:100%;margin-top:40px;" >

<tr><td>

<div id="tika_table_report">
	<div id="item_table" style="width:100%">			
             <table id="sortable_table" class="tablesorter report">				
						<tr>
							<th align="left" width="20%"><?=lang('common_patient_name');?></th>
							<th align="left" width="20%"><?=lang('tika_registration_number');?></th>
							<th align="left" width="20%"><?=lang('common_patient_file_no');?></th>
							<th align="left" width="20%"><?=lang('tika_membership_expire_date');?></th>
							<th align="left" width="20%"><?=lang('common_total');?></th>														
						</tr>
					<?php $Total_Spend='0';
					if(isset($patient_data) && $patient_data!='' && $patient_data->num_rows()){ 
					foreach($patient_data->result() as $row_data){
					$Total = $this->summary_tika_patient->getTotalSummaryData($row_data->person_id,$start_date,$end_date);
					$Total_Spend += $Total; 
					?>
						<tr>
							<td align="left"><?=ucwords(strtolower($row_data->first_name)). ucwords(strtolower($row_data->last_name)); ?></td>
							<td align="left">__</td>
							<td align="left"><?=$row_data->account_number;?></td>
							<td align="left"><?=$this->customer->Membership_ExpireDate($row_data->person_id);?></td>
							<td align="left"><?=to_currency($Total);?></td>
						</tr>
					<?php }}?>													
				</table>
	</div>			
</div>			
		<div class="tablesorter  tika-report">			
		  <table class="tablesorter report">	 
		  <tr> <td align="left" width="60%"><strong><?=lang('tika_total_registeration');?></strong>  </td> <td align="left" width="40%">: <?=$patient_data->num_rows();?></td></tr>
		  <tr> <td align="left"><strong><?=lang('tika_attendance_repeaters');?></strong>  </td> <td align="left">: <?php if(isset($reapeated_attendance) && $reapeated_attendance>0)echo $reapeated_attendance;else echo '0';?></td></tr>
		  <tr> <td align="left"><strong><?=lang('tika_new_registeration_total_amount');?></strong>  </td> <td align="left">: <?=to_currency($this->summary_tika_patient->getTotal_Admission_Fee($start_date,$end_date));?></td></tr>
		  <tr> <td align="left"><strong><?=lang('tika_patient_total_spend_amount');?></strong>  </td> <td align="left">: <?=to_currency($Total_Spend);?></td></tr>
		  </table>				
		</div>
		
		
</td></tr>
</table>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{
		var export_excel = 0;
		if ($("#export_excel_yes").attr('checked'))
		{
			export_excel = 1;
		}
		if ($("#simple_radio").attr('checked'))
		{			
		    window.location = '<?php base_url();?>reports/tika_patient_summary' +'/'+$("#report_date_range_simple option:selected").val();
		}

		else
		{  
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();			
			window.location = '<?php base_url();?>reports/tika_patient_summary/'+start_date + '/'+ end_date +'/'+ export_excel;
				}
	});	

	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});	

	$("#report_date_range_simple").click(function()
	{		
		$("#simple_radio").attr('checked', 'checked');
	});
});


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('.date-field').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	
	$("#item_table").click(function(){ $('.date-field').show(); });

</script>