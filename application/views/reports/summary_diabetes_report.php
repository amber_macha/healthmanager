<?php
if($export_excel == 1)
{
	$rows = array();
	$row = array();
	foreach($headers as $header) 
	{
		$row[] = strip_tags($header['data']);
	}
	
	$rows[] = $row;
	
	foreach($data as $datarow)
	{
		$row = array();
		foreach($datarow as $cell)
		{
			$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;
	}
	
	$content = array_to_csv($rows);	
	force_download(strip_tags($title) . '.csv', $content);
	exit;	
}
 $start_date = $selected_year.'-'.$selected_month.'-'.$selected_day; $end_date = $selected_year2.'-'.$selected_month2.'-'.$selected_day2;
?>
<?php $this->load->view("partial/header"); ?>

<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
<div style="width:100%">	
		<span id='report_date_range_simple'>
			<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
			<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, $selected_date_range, 'id="report_date_range_simple"'); ?>	
		</span>
						<span id='report_date_range_complex' class="mthua-report">
							<input type="radio" name="report_type" id="complex_radio" value='complex'/>
							<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
							<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
							<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
							-
							<?php echo form_dropdown('end_month',$months, $selected_month2, 'id="end_month"'); ?>
							<?php echo form_dropdown('end_day',$days, $selected_day2, 'id="end_day"'); ?>
							<?php echo form_dropdown('end_year',$years, $selected_year2, 'id="end_year"'); ?>
						</span>
				
</div>	
<div style="width:200px;float:left;display:inline">	
<button id="generate_report" class="submit_button" style="display:inline" name="generate_report" type="button"><?=lang('common_submit');?></button>
<button style="display:inline;cursor: pointer;" class="submit_button" type="button" onclick="$.jPrintArea('#table_holder');"><?=$this->lang->line('print_out');?></button>
</div>

<table id="table_holder">
<tr><td>
 <center>
<table id="title_bar">
       
		<tr> <td> <center><div class="mthua-report-title" style=" margin-left:0%;"><?php echo strtoupper(lang('report_diabetes_reports_title')); ?></div></center></td> </tr> 
		<tr> <td> <center><div class="mthua-report-title" style=" margin-left: 0%;"><?php echo strtoupper(lang('report_diabetes_clinic_services_title')); ?></div></center></td> </tr> 

		<tr> <td>
		<center><div class="hospital-title" style=" margin-left: 0%;">
			<span class="hospital-title-right"><?php echo '<b>'.lang('report_for_the_month_name').'</b>'; ?> : <?=$Selected_Month_Name;?>
			<span class="hospital-title-right"><?php echo '<b>'.lang('year').'</b>';?> : <?=$selected_year;?></span><br>
			<?php echo '<b>'.lang('common_hospital_name').'</b>'; ?> : <?=$this->config->item('company');?>
			<span class="hospital-title-right"><?php echo '<b>'.lang('common_district_name').'</b>';?> : <?=$this->config->item('district');?></span>
		</div></center>
		</td> </tr>		 
		<tr> <td>
			<center><div class="hospital-title2" style=" margin-left: 0%;">
			<?php echo '<b>'.lang('report_contact_person_name').'</b>'; ?> : <?=$Selected_Month_Name;?><br>
			<span class="hospital-title-right"><?php echo '<b>'.lang('common_telephone_number').'</b>';?> : <?=$this->config->item('phone');?></span>
			<span class="hospital-title-right"><?php echo '<b>'.lang('common_email').'</b>';?> : <?=$this->config->item('email');?></span><br>
			<span class="hospital-title-right"><?php echo '<b>'.lang('report_generate_by').'</b>';?> : <?=$Reported_BY;?></span>
			</div></center>
		</td> </tr>
	<tr>
		<td>&nbsp;</td>
		<td><small></small></td>
	</tr>
</table>
 </center>
</tr></td>
<tr><td>
<div id="item_table_report">
	<div id="table_holder2" style="width:100%">
			<table id="sortable_table" class="tablesorter">
			<tr><th class="leftmost"><?=lang('sr_no')?></th>
			<th><?=lang('mthua_list_descriptions')?></th>
			<th><div class="th001"><?=lang('mathua_report_age_range1')?></div><div class="th002"> <span class="report-table"><?=lang('mthua_sex_male')?></span><span class="mthuha-report-right"><?=lang('mthua_sex_female')?></span></div></th>
			<th> <?=lang('mthua_list_total')?></th>
			<th><div class="th011"><?=lang('mathua_report_age_range2')?></div><div class="th002"> <span class="report-table"><?=lang('mthua_sex_male')?></span><span class="mthuha-report-right"><?=lang('mthua_sex_female')?></span></div></th>
			<th><?=lang('mthua_list_total')?></th>
			<th><div class="th012"><?=lang('mathua_report_age_range3')?></div><div class="th002"> <span class="report-table"><?=lang('mthua_sex_male')?></span><span class="mthuha-report-right"><?=lang('mthua_sex_female')?></span></div></th>
			<th><?=lang('mthua_list_total')?></th>
			<th><div class="th011"><?=lang('mathua_report_age_range4')?></div><div class="th002"> <span class="report-table"><?=lang('mthua_sex_male')?></span><span class="mthuha-report-right"><?=lang('mthua_sex_female')?></span></div></th>
			<th><?=lang('mthua_list_total')?></th>
			</tr>
			
			
       <?php $ListDiagnoses = $this->Customer->GetAllListDiagnoses('dead_body');
	   $k=1;
	   if(isset($ListDiagnoses) && $ListDiagnoses!='' && $ListDiagnoses->num_rows()>0){
	   foreach($ListDiagnoses->result() as $row){ 
	    //$att1 = $this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'male',0,$row->id); 
	   ?>
			<tr style="cursor: pointer;">
				<td width="6%" class=""><?=$k;?></td>
				<td width="14%" ><?php echo ucwords(stripslashes($row->lab_name));?></td>
				<td width="14%" style="" class="">
					<div class="mthuha-report-left"><?php $OPD_attendance_male = $this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Male',0,$row->id);echo $OPD_attendance_male;?></div>
					<div class="mthuha-report-right"><?php $OPD_attendance_female=$this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Female',0,$row->id);echo $OPD_attendance_female;?></div> </td>
				<td width="6%" style="" class=""><?=($OPD_attendance_male+$OPD_attendance_female);?></td>
				
				<td width="14%" style="" class="">
					<div class="mthuha-report-left"><?php $OPD_attendance_male = $this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Male',1,$row->id);echo $OPD_attendance_male;?></div>
					<div class="mthuha-report-right"><?php $OPD_attendance_female=$this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Female',1,$row->id);echo $OPD_attendance_female;?></div> </td>
				<td width="6%" style="" class=""><?=($OPD_attendance_male+$OPD_attendance_female);?></td>
				
				<td width="14%" style="" class="">
					<div class="mthuha-report-left"><?php $OPD_attendance_male = $this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Male',2,$row->id);echo $OPD_attendance_male;?></div>
					<div class="mthuha-report-right"><?php $OPD_attendance_female=$this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Female',2,$row->id);echo $OPD_attendance_female;?></div> </td>
				<td width="6%" style="" class=""><?=($OPD_attendance_male+$OPD_attendance_female);?></td>
				
				<td width="14%" style="" class="">
					<div class="mthuha-report-left"><?php $OPD_attendance_male = $this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Male',3,$row->id);echo $OPD_attendance_male;?></div>
					<div class="mthuha-report-right"><?php $OPD_attendance_female=$this->summary_diabetesreport->getOPD_Diagnoses_attendance($start_date,$end_date,'Female',3,$row->id);echo $OPD_attendance_female;?></div> </td>
				<td width="6%" style="" class=""><?=($OPD_attendance_male+$OPD_attendance_female);?></td>
				
			</tr>
        <?php $k++;}}?>

		</table>			
	</div>
			
</div>
<tr><td>
</table>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{
		var export_excel = 0;
		if ($("#export_excel_yes").attr('checked'))
		{
			export_excel = 1;
		}
		if ($("#simple_radio").attr('checked'))
		{			
		    window.location = '<?php base_url();?>reports/dead_body_summary' +'/'+$("#report_date_range_simple option:selected").val();
		}

		else
		{  
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();			
			window.location = '<?php base_url();?>reports/dead_body_summary/'+start_date + '/'+ end_date +'/'+ export_excel;
				}
	});	

	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});	

	$("#report_date_range_simple").click(function()
	{		
		$("#simple_radio").attr('checked', 'checked');
	});
});


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('.date-field').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	
	$("#item_table").click(function(){ $('.date-field').show(); });

</script>