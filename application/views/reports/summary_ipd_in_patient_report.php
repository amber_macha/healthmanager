<?php
if($export_excel == 1)
{
		$content = array_to_csv($Execle_Patient_Data);
		force_download(lang('ipd_patient_department_summary') . '.csv', $content);		
	exit;
}
?>


<?php
 $start_date = $selected_year.'-'.$selected_month.'-'.$selected_day; $end_date = $selected_year2.'-'.$selected_month2.'-'.$selected_day2;
?>
<?php $this->load->view("partial/header"); ?>

<div class="report-title-sections" style=" width: 86%;">

	<table id="title_bar">
		<tr>
			<td id="title_icon">
				<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('ipd_patient_department_summary'); ?>' />
			</td>
			<td id="title"><?=lang('reports_reports'); ?> - <?=lang('ipd_patient_department_summary'); ?></td>
		</tr>	
	</table>

	<?php if(isset($error)){ echo "<div class='error_message'>".$error."</div>"; }?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
	<div style="width:100%;margin-bottom: 10px;">	
			<span id='report_date_range_simple'>
				<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
				<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, $selected_date_range, 'id="report_date_range_simple"'); ?>	
			</span>
							<span id='report_date_range_complex' class="mthua-report">
								<input type="radio" name="report_type" id="complex_radio" value='complex'/>
								<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
								<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
								<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
								-
								<?php echo form_dropdown('end_month',$months, $selected_month2, 'id="end_month"'); ?>
								<?php echo form_dropdown('end_day',$days, $selected_day2, 'id="end_day"'); ?>
								<?php echo form_dropdown('end_year',$years, $selected_year2, 'id="end_year"'); ?>
							</span>				
	</div>	
	
	 <label class="required" for="report_date_range_label"><?=lang('ipd_choose_block'); ?></label>
	<div style="width:100%;margin-bottom: 10px;">
			<select id="block_name" name="block_name" style="width:150px;">
			<option value="showAll">All</option>
			<?php if(isset($Block_List) && $Block_List->num_rows()>0){
			    foreach($Block_List->result() as $row){?>	
					<option value="<?=$row->block_name;?>" <?php if(isset($Block_type) && $Block_type!='' && $Block_type==$row->block_name)echo 'selected';?>><?=ucwords($row->block_name);?></option>
				<?php }}?>	
					
			</select>	
			 <label class="ipd_sales_type" for="report_date_range_label"><?=lang('reports_sales_type'); ?></label>
			<span id='report_ipd_report_type' class="ipd-report-sec">
				<input type="radio" name="ipd_report_type" id="all"       value='0' <?php if(isset($report_type) && $report_type=='0')echo "checked='checked'";?>/><span class="ipd-inpaid-type"> <?=lang('reports_all'); ?></span>
				<input type="radio" name="ipd_report_type" id="discharge" value='1' <?php if(isset($report_type) && $report_type=='1')echo "checked='checked'";?>/><span class="ipd-inpaid-type"> <?=lang('reports_ipd_type1'); ?></span>
    			<input type="radio" name="ipd_report_type" id="new_admit" value='2' <?php if(isset($report_type) && $report_type=='2')echo "checked='checked'";?>/><span class="ipd-inpaid-type"> <?=lang('reports_ipd_type2'); ?></span>
				<input type="radio" name="ipd_report_type" id="new_paid"  value='3' <?php if(isset($report_type) && $report_type=='3')echo "checked='checked'";?>/><span class="ipd-inpaid-type"> <?=lang('reports_ipd_type3'); ?></span>
				<input type="radio" name="ipd_report_type" id="all_paid"  value='4' <?php if(isset($report_type) && $report_type=='4')echo "checked='checked'";?>/><span class="ipd-inpaid-type"> <?=lang('reports_ipd_type4'); ?></span>
			</span>		
		</div>

		 <?=lang('reports_export_to_excel'); ?>: <input type="radio" value="1" id="export_excel_yes" name="export_excel"> <?=lang('common_yes'); ?>
		<input type="radio" checked="checked" value="0" id="export_excel_no" name="export_excel"> <?=lang('common_no'); ?>
		
</div>
<div class="report-logo-sections"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>
	
<div style="width:200px;float:left;display:inline; margin-bottom: 12px;">	
<button id="generate_report" class="submit_button" style="display:inline" name="generate_report" type="button"><?=lang('common_submit');?></button>
<button style="display:inline;cursor: pointer;" class="submit_button" type="button" onclick="$.jPrintArea('#table_holder');"><?=$this->lang->line('print_out');?></button>
</div>


<table id="table_holder" style="width:100%;margin-top:40px;" >

<tr><td>

	<div id="tika_table_report">
		<div id="item_table" style="width:100%">			
				 <?php echo $table_data_array['ipd_patient_data'];?>
		</div>			
	</div>			
	<div class="tablesorter  tika-report">			
	  <table class="tablesorter report">	 
	  <tr> <td align="left" width="60%"><strong><?=lang('reports_total_ipd_patient');?></strong>    </td> <td align="left" width="40%">: <?=$table_data_array['ipd_patient_total_patient'];?></td></tr>
	  <tr> <td align="left"><strong><?=lang('reports_total_ipd_patient_paid_amount');?></strong>    </td> <td align="left">: <?=@to_currency($table_data_array['ipd_patient_total_paid']);?></td></tr>
	  <tr> <td align="left"><strong><?=lang('reports_total_ipd_patient_balance_amount');?></strong> </td> <td align="left">: <?=@to_currency($table_data_array['ipd_patient_total_balance']);?></td></tr>
	  </table>				
	</div>
		
		
</td></tr>
</table>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	$("#generate_report").click(function()
	{
	
	   // GET Block name  and IPD Patient type//		
		var block_name = $("#block_name").val();
		var ipd_report_type = $('input:radio[name=ipd_report_type]:checked').val();
		var ipdstring = '/' + block_name +'/' + ipd_report_type
		
		//alert(block_name); alert(ipd_report_type);
		var export_excel = 0;
		if ($("#export_excel_yes").attr('checked'))
		{
			export_excel = 1;
		}
		if ($("#simple_radio").attr('checked'))
		{			
		    window.location = '<?php base_url();?>reports/ipd_in_patient_summary' +'/'+$("#report_date_range_simple option:selected").val() + ipdstring + '/' + export_excel;
		}

		else
		{  
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();			
			window.location = '<?php base_url();?>reports/ipd_in_patient_summary/'+start_date + '/'+ end_date +  ipdstring + '/'+ export_excel;
		}
	});	

	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").click(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});	

	$("#report_date_range_simple").click(function()
	{		
		$("#simple_radio").attr('checked', 'checked');
	});
});


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('.date-field').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	
	$("#item_table").click(function(){ $('.date-field').show(); });

</script>