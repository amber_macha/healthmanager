
<?php $this->load->view("partial/header"); ?>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('reports_welcome_message'); ?>' />
		</td>
		<td id="title"><?php echo lang('reports_reports'); ?></td>
	</tr>
</table>

<ul id="report_list">
<?php if ($this->Employee->has_module_action_permission('reports', 'view_customers', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="full"> 
		<h3><?php echo lang('reports_customers'); ?></h3>
		<ul>	
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_customers');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_customers');?>"><?php echo lang('reports_summary_reports'); ?></a></li>
			<li class="detailed"><a href="<?php echo site_url('reports/specific_customer');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>		
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_employees', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="full">
		<h3><?php echo lang('reports_employees'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_employees');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_employees');?>"><?php echo lang('reports_summary_reports'); ?></a></li>
			<li class="detailed"><a href="<?php echo site_url('reports/specific_employee');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_sales', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="full">
		<h3><?php echo lang('reports_sales'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_sales');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_sales');?>"><?php echo lang('reports_summary_reports'); ?></a></li>
			<li class="detailed"><a href="<?php echo site_url('reports/detailed_sales');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_register_logs', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<?php if ($this->config->item('track_cash')) { ?>
	<li class="third">
		<h3><?php echo lang('reports_register_log_title'); ?></h3>
		<ul>
			<li>&nbsp;</li>
			<li>&nbsp;</li>
			<li class="detailed"><a href="<?php echo site_url('reports/detailed_register_log');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>			
		</ul>
	</li>
	<?php } ?>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_categories', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_categories'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_categories');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_categories');?>"><?php echo lang('reports_summary_reports'); ?></a></li>		
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_discounts', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_discounts'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_discounts');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_discounts');?>"><?php echo lang('reports_summary_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_items', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_items'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_items');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_items');?>"><?php echo lang('reports_summary_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_item_kits', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('module_item_kits'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_item_kits');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_item_kits');?>"><?php echo lang('reports_summary_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_payments', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_payments'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_payments');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_payments');?>"><?php echo lang('reports_summary_reports'); ?></a></li>	
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_suppliers', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_suppliers'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_suppliers');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_suppliers');?>"><?php echo lang('reports_summary_reports'); ?></a></li>
			<li class="detailed"><a href="<?php echo site_url('reports/specific_supplier');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>					
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_taxes', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_taxes'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/graphical_summary_taxes');?>"><?php echo lang('reports_graphical_reports'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_taxes');?>"><?php echo lang('reports_summary_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_receivings', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="third">
		<h3><?php echo lang('reports_receivings'); ?></h3>
		<ul>
			<li>&nbsp;</li>
			<li>&nbsp;</li>
			<li class="detailed"><a href="<?php echo site_url('reports/detailed_receivings');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_inventory_reports', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="second">
		<h3><?php echo lang('reports_inventory_reports'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/inventory_low');?>"><?php echo lang('reports_low_inventory'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/inventory_summary');?>"><?php echo lang('reports_inventory_summary'); ?></a></li>	
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_deleted_sales', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="first">
		<h3><?php echo lang('reports_deleted_sales'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/deleted_sales');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>
		</ul>
	</li>	
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_giftcards', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="full">
		<h3><?php echo lang('reports_giftcards'); ?></h3>
		<ul>
			<li>&nbsp;</li>
			<li class="summary"><a href="<?php echo site_url('reports/summary_giftcards');?>"><?php echo lang('reports_summary_reports'); ?></a></li>			
			<li class="detailed"><a href="<?php echo site_url('reports/detailed_giftcards');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_attendance', $this->Employee->get_logged_in_employee_info()->person_id)){?>
		<li class="first">
		<h3><?php echo lang('reports_Attendance'); ?></h3>
		<ul> 
		     <li class="graphical"><a href="<?php echo site_url('customers/detailed_attendance');?>"><?php echo lang('reports_detailed_reports'); ?></a></li>			
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_expire_items', $this->Employee->get_logged_in_employee_info()->person_id)){?>
	<li class="first">
		<h3><?php echo lang('reports_expire_items'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/expire_items_summary');?>"><?php echo lang('reports_expire_items2'); ?></a></li>
		</ul>
	</li>	
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_mtuha_reports', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
		<li class="first">
		<h3><?php echo lang('mthua_report'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/mtuha_report');?>"><?php echo lang('mthua_report'); ?></a></li>
			<li class="graphical"><a href="<?php echo site_url('reports/clinical_daignosis_report');?>"><?php echo lang('reports_clinical_daignosis'); ?></a></li>
		</ul>
	</li>
<?php } if ($this->Employee->has_module_action_permission('reports', 'view_tika_customers', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
		<li class="first">
		<h3><?php echo lang('tika_pateint'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/tika_patient_summary');?>"><?php echo lang('tika_registration'); ?></a></li>
		</ul>
	</li>
<?php } 
if ($this->Employee->has_module_action_permission('reports', 'view_ipd_in_patient', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
		<li class="second">
		<h3><?php echo lang('ipd_patient_department'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/ipd_in_patient_summary');?>"><?php echo lang('ipd_patient_department_report'); ?></a></li>
			<li class="summary"><a href="<?php echo site_url('reports/ipd_in_patient_summary');?>"><?php echo lang('ipd_patient_department_report'); ?></a></li>
			<li class="detailed"><a href="<?php echo site_url('reports/ipd_in_patient_summary');?>"><?php echo lang('ipd_patient_department_report'); ?></a></li>					
		</ul>
	</li>
<?php } if($this->Employee->has_module_action_permission('reports', 'view_dead_body', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
	<li class="first">
		<h3><?php echo lang('report_dead_body_title'); ?></h3>
		<ul>
			<li class="graphical"><a href="<?php echo site_url('reports/dead_body_summary');?>"><?php echo lang('report_dead_body_reports'); ?></a></li>
		</ul>
	</li>
	<?php }?>
	
	<li class="first">
		<h3><?php echo lang('module_diabetes_care'); ?></h3>
		<ul><li class="graphical"><a href="<?php echo site_url('reports/diabetes_summary');?>"><?php echo lang('report_diabetes_reports'); ?></a></li></ul>
	</li>
	
	<?php if ($this->Employee->has_module_action_permission('reports', 'view_doctor_parformance', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
	<li class="first">
		<h3><?php echo lang('reports_doctor_parformance'); ?></h3>
		<ul><li class="graphical"><a href="<?php echo site_url('reports/doctor_parformance_summary');?>"><?php echo lang('reports_doctor_parformance_reports'); ?></a></li></ul>
	</li>
	<?php }?>
	
	<?php if ($this->Employee->has_module_action_permission('reports', 'view_employee_parformance', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
	<li class="first">
		<h3><?php echo lang('reports_employee_parformance'); ?></h3>
		<ul><li class="graphical"><a href="<?php echo site_url('reports/employee_parformance_summary');?>"><?php echo lang('reports_employee_parformance_reports'); ?></a></li></ul>
	</li>
	<?php }?>
	
	<?php if ($this->Employee->has_module_action_permission('reports', 'view_epay_transactions', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
	<li class="first">
		<h3><?php echo lang('common_transactions'); ?></h3>
		<ul><li class="graphical"><a href="<?php echo site_url('reports/epay_transactions_summary');?>"><?php echo lang('reports_epay_transactions'); ?></a></li></ul>
	</li>
	<?php }?>
	
		<?php if ($this->Employee->has_module_action_permission('reports', 'view_epay_refund_transactions', $this->Employee->get_logged_in_employee_info()->person_id)){?>	
	<li class="first">
		<h3><?php echo lang('reports_epay_refund_transactions'); ?></h3>
		<ul><li class="graphical"><a href="<?php echo site_url('reports/epay_refund_transactions_summary');?>"><?php echo lang('reports_epay_refund_transactions'); ?></a></li></ul>
	</li>
	<?php }?>
	
	</ul>
<?php $this->load->view("partial/footer"); ?>