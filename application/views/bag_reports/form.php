<style>fieldset div.field_row { font-weight: normal;}
fieldset div.field_row label {width: 145px;}
fieldset div.field_row div.form_field select {width:200px;}
#reject_reason_sec{display:<?php if($item_info->status==3 || $item_info->reject_reason!='')echo 'block';else echo 'none';?>;}
</style>
<?php 
if($view_type=='show' || $item_info->status==4){ $form_input ="form_input_readonly"; $form_textarea = 'form_textarea_readonly'; $disabled ='disabled="disabled"';}
else { $form_input='form_input'; $form_textarea = 'form_textarea'; $disabled =''; }
echo form_open('bag_reports/save/'.$item_info->id,array('id'=>'report_form'));
?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("bag_reports_form_title"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('bag_reports_subject').':', 'reports_subject',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'report_subject','id'=>'report_subject','value'=>$item_info->report_subject,'onclick'=>'clicktohide(this);','style'=>'width:335px'));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('bag_reports_description').':', 'reports_description',array('class'=>'required')); ?>
	<div class='form_field'><?php echo $form_textarea(array('name'=>'report_description','id'=>'report_description','rows'=>10,'cols'=>60,'value'=>$item_info->description));?></div>
</div>

<?php if(isset($item_info->id) && $item_info->id>0){?>
<div class="field_row clearfix">	
<?php echo form_label(lang('bag_reports_action_explanation').':', 'action_explanation',array('class'=>'required')); ?>
	<div class='form_field'><?php echo $form_textarea(array('name'=>'action_explanation','id'=>'action_explanation','rows'=>10,'cols'=>60,'value'=>$item_info->action_explanation));?></div>
</div>
<?php }?>

<div class="field_row clearfix">
<?php echo form_label(lang('bag_reports_module_report').':', 'module_report',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<select id="module_id" name="module_id" <?=$disabled;?>>
		<option value=""><?php echo lang('common_select_type');?></option>
		<?php foreach($this->Module->get_all_modules('module_id')->result_array() as $RowData){?>
		<option value="<?=$RowData['module_id'];?>" <?php if($item_info->module_id==$RowData['module_id'])echo 'selected="selected"';?>><?php echo lang("module_".$RowData['module_id']);?></option>
		<?php }?>
		</select>	
	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label(lang('bag_reports_status').':', 'reports_status',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('reports_status',$this->bagreports->bags_reports_status(),$item_info->status,"onchange='getStatus(this.value)' $disabled");?>	
	</div>
</div>


<div class="field_row clearfix" id="reject_reason_sec">	
<?php echo form_label(lang('bag_reports_reject_reason').':', 'reject_reason',array('class'=>'required')); ?>
	<div class='form_field'>
		<?php echo $form_input(array('name'=>'reject_reason','id'=>'reject_reason','value'=>$item_info->reject_reason,'onclick'=>'clicktohide(this);','style'=>'width:340px'));?>
	</div>
</div>	
	

<?php if($view_type=='edit' && $item_info->status!=4){ echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));}
	else { echo form_buttons(array('name'=>'save','value'=>lang('common_close'),'class'=>'delete-button','onclick'=>'close_form();'));} ?>
</fieldset>
<?php echo form_close();?>


<script type='text/javascript'>
function getStatus(status_type)
{
  if(status_type==3) { $('#reject_reason_sec').show(); } 
  else { $('#reject_reason_sec').hide(); $('#reject_reason').val(''); }
}


$(document).ready(function()
{	var submitting = false;
	$('#report_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			report_subject: "required", 
    		report_description: "required", 
			module_id: "required",
			<?php if(isset($item_info->id) && $item_info->id>0){echo 'action_explanation: "required"'; } ?>
   		},
		messages: 
		{
     		report_subject: <?php echo json_encode(lang('bag_reports_subject_required')); ?>,
     		report_description: <?php echo json_encode(lang('bag_reports_description_required')); ?>,
			module_id: <?php echo json_encode(lang('bag_reports_department_required')); ?>,	
			<?php if(isset($item_info->id) && $item_info->id>0){ echo 'action_explanation:'. json_encode(lang('bag_reports_action_explanation_required')); }?>	
		}
	});
});
</script>