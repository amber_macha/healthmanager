<?php
echo form_open_multipart('dead_body/do_excel_import/',array('id'=>'item_form')); 
?>
<div id="required_fields_message"><?php echo lang('customers_mass_import_from_excel'); ?></div>
<ul id="error_message_box"></ul>
<b><a href="<?php echo site_url('dead_body/excel'); ?>"><?php echo lang('customers_download_excel_import_template'); ?></a></b>
<fieldset id="item_basic_info">
<legend><?php echo lang('customers_import'); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_file_path').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_upload(array(
		'name'=>'file_path',
		'id'=>'file_path',
		'onchange'=>'return validate_upload_file(this.value);',
		'value'=>'')
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submitf',
	'id'=>'submitf',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>
function validate_upload_file(fileName)
{  
    var allowed_extensions = new Array('csv');
    var file_extension = fileName.split('.').pop(); 
    for(var i = 0; i <= allowed_extensions.length; i++)
    {
        if(allowed_extensions[i]==file_extension)
        {
            return true; // valid file extension
        }
    }
    $('#file_path').val('');	 
	alert('Please upload only csv file');
    return false;
}

//validation and submit handling
$(document).ready(function()
{	
	var submitting = false;
	$('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			file_path:"required"
   		},
		messages: 
		{
   			file_path:<?php echo json_encode(lang('customers_full_path_to_excel_required')); ?>
		}
	});
});
</script>