<?php $this->load->view("partial/header"); ?>
<style>table {border-color:#FFFFFF;}</style>

<script type="text/javascript">
function Change_Status(id)
{ 
	 var searchURL ='<?php echo base_url().$controller_name."/chanage_status";?>';	 
	if (confirm('<?php echo lang('common_confirm_change_status');?>')) 
	 {
	 	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'items_id=' + id,
			dataType:'json',
            success: function(response) 
			{ 
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true);
				}
				
				do_search(true,function()
				{
					set_feedback(response.message,'success_message',false);
				});
			}
        });   
	 }
     else {return false;}	 
}

function SendRequestTo_Cashier(id)
{ 
	var searchURL ='<?php echo base_url()."settings/SendRequestToCashier/$person_info->person_id";?>';	 
	if (confirm('<?php echo lang('common_confirm_cashier_send_request_status');?>')) 
	 {
	 	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'items_id=' + id + '&category_type=dentals',
			dataType:'json',
            success: function(response) 
			{ 
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true); 
					return false;
				}
				
				do_search(true,function()
				{
					set_feedback(response.message,'success_message',false);
				});
			}
        });   
	 }
     else {return false;}	 
}

$(document).ready(function() 
{ 
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$person_info->person_id");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{ 
		set_feedback(response.message,'error_message',true);	
	}
	else
	{   
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{ 
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else 
		{  
			do_search(true,function()
			{
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search/$person_info->person_id",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				{?>
					<?php echo anchor("$controller_name/view/$person_info->person_id/width~700",
					$this->lang->line($controller_name.'_new'),	array('class'=>'thickbox none new', 'title'=>$this->lang->line($controller_name.'_new')));
				}	
				?>
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) {?>
					<?php echo anchor("$controller_name/delete/$person_info->person_id",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); ?>
				<?php } ?>
			</div>
			<?php $this->load->view('partial/patient_waiting_counter');?>	
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
		<div id="table_holder">			
	<table class="tablesorter">
	
	<tr><th class="leftmost"></th></tr>
	
	<tr> <td width="90%" style="background-color:#A0CFEC; border-left: 1px solid #BBBBBB;">

	   <?php $this->load->view('partial/history_customers_menu');?>		
		  <div class="heigthdiv"></div>
		  <?=$manage_history_menu;?>
		   <div class="heigthdiv1"></div>		   
		        <div class="comment-header"><?php echo lang('dental_lab'); ?></div>		   
				<table id="sortable_table" class="tablesorter" border="6">
				<tbody><?php echo $manage_AllLabTest;?></tbody>
				</table>			
		</td>
	</tr>
	
	</table>
				
			 </div>
			
			</div>
			<div id="pagination">
				<?php //echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>