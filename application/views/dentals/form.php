<style>
.mthu_diagnoses_sec{width:274px;float:left;}
.mthu_sec_left{float:left;width:330px;}
.mthu_sec_right{float:right;width:320px;}
fieldset div.field_row label { width: 260px;}
fieldset div.field_row {  font-size: 12px; font-weight: normal;}
.field_row label {width: auto;margin-left: 0px;padding: 5px;} 
.field_row div.form_field input {width:auto; } 

fieldset div.field_row div.form_field input[type="checkbox"] { margin-top: 2px;}
</style>

<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("dental_header"); ?></legend>
<div class="form_field"> <label class="required lab-search-2"><?php echo lang('lab_investigation_category'); ?> :</label>
<label class="add_new_lab_search" for="first_name" style="margin-left: 253px;"><?php echo lang('search_lab_services'); ?>: 
<img id="searchspiner" class="search-spiner" src="<?php echo base_url();?>images/spinner_small.gif">
<input type="text" class="lab-search-box" id="search_lab_service" value="" name="search_lab_service">
</label>	
</div>
	<?php echo form_open('dental/save/'.$person_id,array('id'=>'history_form','onsubmit'=>'return validateCustomer()')); ?>
	<input type="hidden" name="category_list" id="category_list" value="">
		<div class="field_row clearfix" style="margin:0px;">	
			<div class='form_field'>		
				<div id="labcategory" class="labcategory_div" style="width: 625px;">
					<?php echo $lab_category_list; ?>
				</div>
			</div>
		</div>
		
    
	<div class="field_row clearfix">
        <span id="error_message_box" style="display:none"></span>	
		<div class='form_field'>
		     <div class="mthu_sec_left">			 
					<div class="form_field" style="margin-top: 4px;"> 
						<label class="required lab-search-2"><?php echo lang('list_diseases'); ?> : <span style="color:#000000">MTHUA DIAGNOSES</span></label>
						<div class="lab-search"><?php echo lang('search_diseases'); ?>: 
						<img id="searchspiner1" class="search-spiner01" src="<?php echo base_url();?>images/spinner_small.gif">
						<input type="text" class="lab-search-box" id="search_lab_diagnoses" value="" name="search_lab_diagnoses" style="padding: 3px;">
						</div>	
					</div>			 
			 
					<div id="labcategory_diagnoses" style="float:left; margin:5px 5px;overflow-y:scroll;width: 300px; height:130px;">
					<?php if(isset($diseases_list) && $diseases_list!=''){ echo $diseases_list; }?>
					</div>
			</div>	
			
			<div class="mthu_sec_right">
					<div class="form_field" style="margin-top: 4px;"> 
						<label class="required lab-search-2"><?php echo lang('list_diseases'); ?> : <span style="color:#000000">DIABETES CARE</span></label>
						<div class="lab-search"><?php echo lang('search_diabetes'); ?>: 
						<img id="searchspiner2" class="search-spiner01" src="<?php echo base_url();?>images/spinner_small.gif">
						<input type="text" class="lab-search-box" id="search_lab_diabetes" value="" name="search_lab_diabetes" style="padding: 3px;">
						</div>	
					</div>
			
					<div id="diabetes_list" style="float:left; margin:5px 5px;overflow-y:scroll;width: 320px; height:130px;">
					<?php if(isset($diabetes_list) && $diabetes_list!=''){ echo $diabetes_list; }?>
					</div>
			</div>	
			
		</div>
	</div>				
<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
<input id="active_deactive" class="active_deactive" type="button" onclick="ActiveDeActivate();" value="<?php echo $this->lang->line("active_deactive_lab_servics");?>">

<?php echo form_close();?>
</fieldset>
<script type='text/javascript'>
//**Active Deactivate Lab Services code**//
function ActiveDeActivate()
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('dental_services_empty_change_status');?>');return false; }  
	if (confirm('<?php echo lang('lab_services_change_status');?>')) 
	{
	var searchURL ='<?php echo base_url().'dental/activeDeActivate/'.$person_id.'/';?>' +checkedval;
	$('#searchspiner').show();
	$.get(searchURL, function(data) { $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').html(''); } , 5000); search(true); });
	}	
	else{return false;}
}

//**************START CODE FOR LAB DIAGNOSES SEARCH*********************//
$('#search_lab_diagnoses').keyup(function(e) { 
 var existingString = $("#search_lab_diagnoses").val();
 //if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search_diagnoses(existingString), 500));
});


function search_diagnoses(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'eyeclinic/searchDiseasList/'.$person_id ?>';
	$('#searchspiner1').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'searchtype=mtuha_daignoses&service='+existingString+'&checkedval='+checkedval+'&item_id=-1',
            success: function(data) 
				{ 
					$('#labcategory_diagnoses').html(data);  $('#searchspiner1').hide(); 
				}
        }); 
}

//**************END CODE FOR LAB DIAGNOSES SEARCH***********************//

//**************START CODE FOR LAB DIABETES SEARCH*********************//
$('#search_lab_diabetes').keyup(function(e) { 
 var existingString = $("#search_lab_diabetes").val();
 //if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)  search_diabetes(existingString);
    else   $(this).data('timer', setTimeout(search_diabetes(existingString), 500));
});


function search_diabetes(existingString) {   
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'eyeclinic/searchDiseasList/'.$person_id ?>';
	$('#searchspiner2').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'searchtype=daibetes&service='+existingString+'&checkedval='+checkedval+'&item_id=-1',
            success: function(data) 
				{ 
					$('#diabetes_list').html(data);  $('#searchspiner2').hide(); 
				}
        }); 
}

//**************lab searching code*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});
function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  	var searchURL ='<?php echo base_url();?>dental/getservices/';	$('#searchspiner').show();	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval,
            success: function(data) { $('#labcategory').html(data);  $('#searchspiner').hide(); }
        }); }
//**************lab searching code*********************//
function validateCustomer()
{
	var n = $( "input:checked" ).length;
	if(n==0){ return false; }
	else { 
		$(':checkbox:checked').each(function(i){
		if($(this).val()==0){ alert('<?php echo lang('dental_services_empty_lab_services_to_added');?>'); $('#lab_'+id).attr('checked', false); return false; }				}); 
	document.getElementById('category_list').value = n;  
	} 
 }  
//validation and submit handling
$(document).ready(function()
{	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			category_list: "required"
   		},
		messages: 
		{     		
     		category_list: <?php echo json_encode(lang('dental_services_empty_lab_services_to_added')); ?>
		}	});});
</script>