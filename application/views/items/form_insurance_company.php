<style> #company_name { font-size: 14px; font-weight: normal;}
fieldset div.field_row div.form_field input {width:250px;}
.patient_image{margin-left:191px; width: 100px;}
</style>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save_insurance_company/'.$item_info->id,array('id'=>'item_form','enctype'=>'multipart/form-data'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_information"); ?></legend>


	<div class="field_row clearfix">
	<?php echo form_label(lang('common_company_name').':', 'name',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'company_name',
			'id'=>'company_name',
			'value'=>$item_info->company_name)
		);?>
		</div>
	</div>
	
	<div class="field_row clearfix"> 
	<?php echo form_label(lang('common_company_type').':', 'company_profile',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'company_profile',
			'id'=>'company_profile',
			'value'=>$item_info->company_profile)
		);?>
		</div>
	</div>

<div class="field_row clearfix">
<?php echo form_label(lang('common_phone_number').':', 'phone_number',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'company_phone_no',
		'id'=>'company_phone_no',
		'value'=>$item_info->company_phone_no)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('common_address').':', 'company_address',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'company_address',
		'id'=>'company_address',
		'value'=>$item_info->company_address)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('common_company_website').':', 'website',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'company_website',
		'id'=>'company_website',
		'value'=>$item_info->company_website)
	);?>
	</div>
</div>

	<div class="field_row clearfix">	
		<label><?php echo lang('common_company_logo');?> </label>	
		<div class='form_field'> <?php echo form_upload(array('name'=>'company_logo','id'=>'company_logo'));?></div>	
		<?php if(isset($item_info->company_logo) && $item_info->company_logo!='' && $item_info->company_logo!='0'){?> <div class="patient_image-div"><img class="patient_image" src="<?php echo base_url();?>images/insurance_company_logo/<?='thumb_'.$item_info->company_logo;?>"> </div><?php }?>	
	</div>	


<?php echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{ 
	var submitting = false;
	$('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				submitting = false;
				tb_remove();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			company_name:"required",
			company_profile:"required",
			company_phone_no:"required",
			company_address:"required",
   		},
		messages:
		{
			company_name:<?php echo json_encode(lang('common_address_name_required')); ?>,
			company_profile:<?php echo json_encode(lang('items_company_type_required')); ?>,
			company_phone_no:<?php echo json_encode(lang('common_phone_member_required')); ?>,
			company_address:<?php echo json_encode(lang('common_address_name_required')); ?>,
		}
	});
});
</script>