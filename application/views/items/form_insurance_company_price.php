<!-- ***********START INSURANCE COMPANY PRICE DISPLAY SECTIONS**********************-->
<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_nssf_price').':', 'unit_nssf_price',array('class'=>'wide'));
if(isset($item_info->unit_nssf_price) && $item_info->unit_nssf_price!='') $unit_nssf_price = $item_info->unit_nssf_price;else $unit_nssf_price='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_nssf_price',
		'size'=>'8',
		'id'=>'unit_nssf_price',
		'value'=>$unit_nssf_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_strategis_insurance').':', 'strategis_insurance',array('class'=>'wide')); 
if(isset($item_info->unit_strategis_insurance) && $item_info->unit_strategis_insurance!='') $unit_strategis_insurance = $item_info->unit_strategis_insurance;else $unit_strategis_insurance='';
?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_strategis_insurance',
		'size'=>'8',
		'id'=>'unit_strategis_insurance',
		'value'=>$unit_strategis_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_jubilee_insurance').':', 'jubilee_insurance',array('class'=>'wide')); 
if(isset($item_info->unit_jubilee_insurance) && $item_info->unit_jubilee_insurance!='') $unit_jubilee_insurance = $item_info->unit_jubilee_insurance;else $unit_jubilee_insurance='';
?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_jubilee_insurance',
		'size'=>'8',
		'id'=>'unit_jubilee_insurance',
		'value'=>$unit_jubilee_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_arr_insurance').':', 'arr_insurance',array('class'=>'wide'));
if(isset($item_info->unit_arr_insurance) && $item_info->unit_arr_insurance!='') $unit_arr_insurance = $item_info->unit_arr_insurance;else $unit_arr_insurance='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_arr_insurance',
		'size'=>'8',
		'id'=>'unit_arr_insurance',
		'value'=>$unit_arr_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_reliance_insurance').':', 'reliance_insurance',array('class'=>'wide'));
if(isset($item_info->unit_reliance_insurance) && $item_info->unit_reliance_insurance!='') $unit_reliance_insurance = $item_info->unit_reliance_insurance;else $unit_reliance_insurance='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_reliance_insurance',
		'size'=>'8',
		'id'=>'unit_reliance_insurance',
		'value'=>$unit_reliance_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_resolution_insurance').':', 'resolution_insurance',array('class'=>'wide'));
if(isset($item_info->unit_resolution_insurance) && $item_info->unit_resolution_insurance!='') $unit_resolution_insurance = $item_info->unit_resolution_insurance;else $unit_resolution_insurance='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_resolution_insurance',
		'size'=>'8',
		'id'=>'unit_resolution_insurance',
		'value'=>$unit_resolution_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_billing_patient_price').':', 'billing_patient_price',array('class'=>'wide'));
if(isset($item_info->unit_billing_patient_price) && $item_info->unit_billing_patient_price!='') $unit_billing_patient_price = $item_info->unit_billing_patient_price;else $unit_billing_patient_price='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_billing_patient_price',
		'size'=>'8',
		'id'=>'unit_billing_patient_price',
		'value'=>$unit_billing_patient_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_nic_insurance').':', 'nic_insurance',array('class'=>'wide'));
if(isset($item_info->unit_nic_insurance) && $item_info->unit_nic_insurance!='') $unit_nic_insurance = $item_info->unit_nic_insurance;else $unit_nic_insurance='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_nic_insurance',
		'size'=>'8',
		'id'=>'unit_nic_insurance',
		'value'=>$unit_nic_insurance)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_phoenix_insurance').':', 'phoenix_insurance',array('class'=>'wide')); 
if(isset($item_info->unit_phoenix_insurance) && $item_info->unit_phoenix_insurance!='') $unit_phoenix_insurance = $item_info->unit_phoenix_insurance;else $unit_phoenix_insurance='';
?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_phoenix_insurance',
		'size'=>'8',
		'id'=>'unit_phoenix_insurance',
		'value'=>$unit_phoenix_insurance)
	);?>
	</div>
</div>


<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_imara_insurance').':', 'imara_insurance',array('class'=>'wide'));
if(isset($item_info->unit_imara_insurance) && $item_info->unit_imara_insurance!='') $unit_imara_insurance = $item_info->unit_imara_insurance;else $unit_imara_insurance='';
 ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_imara_insurance',
		'size'=>'8',
		'id'=>'unit_imara_insurance',
		'value'=>$unit_imara_insurance)
	);?>
	</div>
</div>
<!-- ***********END INSURANCE COMPANY PRICE DISPLAY SECTIONS**********************-->