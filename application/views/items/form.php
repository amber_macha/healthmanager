<style> #supplier_id{width:250px;}</style>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save/'.$item_info->item_id,array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_information"); ?></legend>

<div class="field_row clearfix">	
<?php echo form_label(lang('items_item_type').':', 'item_type'); ?>
<?php 
$item_type ='0'; $items_disply_style ='';
$sub_category_sec_disply_style='style="display: none;"'; 
$services_disply_style ='style="display: none;"';
$items_type_label_disply_style='style="display:none;"';
$items_type_sec001_disply_style='';
$items_generic_code = $item_info->items_generic_code;
if(isset($item_info->item_type) && $item_info->item_type!='') 
{ $item_type = $item_info->item_type;  
  if($item_info->item_type==1) 
  { $items_disply_style='style="display:none;"';$services_disply_style='style="display:block;"';$items_type_label_disply_style='style="display:block;"';
	if($item_info->service_category==5){ $sub_category_sec_disply_style=''; $items_generic_code = '0'; $items_type_sec001_disply_style='style="display:none;"';}
	
  }
} ?>	
	<div class='form_field22'>
		<label style="width: 108px;font-weight: normal;"><input type="radio" name="item_category" value="0" onclick="getServiceType(0)" <?php if($item_type=='0')echo 'checked="checked"';?>> <?=lang('common_medicine');?> </label>	
		<label style="width: 108px;font-weight: normal;"><input type="radio" name="item_category" value="1" onclick="getServiceType(1)" <?php if($item_type=='1')echo 'checked="checked"';?>> <?=lang('common_service');?> </label>	
	</div>
</div>

<div class="field_row clearfix">		
	<?php echo form_label(lang('items_multi_store_name').':', 'company_name',array('class'=>'wide required'));?>
	<div class='form_field'>
			<select name="multi_store_item" id="multi_store_item" style="width:194px;">
			<option value=""><?php echo lang('items_multi_store_selection');?></option>
			<?php if($multi_stores_items->num_rows()>0){
			foreach($multi_stores_items->result() as $rowdata){?>
			<option value="<?=$rowdata->id;?>" <?php if($item_info->lab_category_id==$rowdata->id) echo 'selected';?>><?php echo ucfirst($rowdata->lab_name); ?></option>
			<?php }}?></select>
	</div>
</div>

<div class="field_row clearfix" id="services_dropdown_sec" <?=$services_disply_style;?>>	
<?php echo form_label(lang('common_service_type').':', 'service_type',array('class'=>'wide')); ?>
	<div class='form_field'>
		<select name="service_category" id="service_category" class="valid" style="width:194px;" onchange="getServiceCategory(this.value)">
		<option value=""><?php echo lang('common_select_type');?></option>
		<option value="1" <?php if(isset($item_info->service_category) && $item_info->service_category=='1') echo 'selected';?>><?php echo lang('common_laboratory_service'); ?></option>
		<option value="2" <?php if(isset($item_info->service_category) && $item_info->service_category=='2') echo 'selected';?>><?php echo lang('common_radiology_service'); ?></option>
		<option value="3" <?php if(isset($item_info->service_category) && $item_info->service_category=='3') echo 'selected';?>><?php echo lang('common_dental_service'); ?></option>
		<option value="4" <?php if(isset($item_info->service_category) && $item_info->service_category=='4') echo 'selected';?>><?php echo lang('common_eye_clinic_service'); ?></option>
		<option value="5" <?php if(isset($item_info->service_category) && $item_info->service_category=='5') echo 'selected';?>><?php echo lang('common_others_service'); ?></option>
		</select>
	</div>	
</div>

<div class="field_row clearfix">
    <?php echo form_label(lang('items_item_number').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'> <?php echo form_input(array('name'=>'item_number','id'=>'item_number','value'=> $item_info->item_number ));?> </div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_name').':', 'name',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$item_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix" id="items_type_sec001" <?php echo $items_type_sec001_disply_style;?>> 
<label class="required wide" for="items_generic" id="items_generic" <?php echo $items_disply_style;?>><?=lang('items_generic_code');?>:</label>
<label class="required wide" for="items_type" id="items_type_label" <?php echo $items_type_label_disply_style;?>><?=lang('items_type');?>:</label>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'items_generic_code', 
		'id'=>'items_generic_code',
		'value'=>$items_generic_code)
	);?>
	</div>
</div>

<div class="field_row clearfix" id="sub_category_sec" <?php echo $sub_category_sec_disply_style;?>>
	<div class='form_field' style='width: 300px;'>
	<?php 
	 $checked1 =  FALSE; $checked2 =  FALSE; $checked3 =  FALSE; $checked4 =  FALSE;
	 if($item_info->service_sub_category1==1){$checked1 =  TRUE;}if($item_info->service_sub_category2==1){$checked2 =  TRUE;}
	 if($item_info->service_sub_category3==1){$checked3 =  TRUE;}if($item_info->service_sub_category4==1){$checked4 =  TRUE;}
	 
	 echo form_label(lang('items_type_specialized').':', 'specialized',array('class'=>'wide','style'=>'width: 86px;'));
	 echo form_checkbox(array('name'=>'specialized','id'=>'specialized','value'=>1,'checked'=>$checked1,'style'=>'margin-top: 11px;'));
	
	 echo form_label(lang('items_type_major').':', 'major',array('class'=>'wide','style'=>'width: 86px;'));	 
	 echo form_checkbox(array('name'=>'major','id'=>'major','value'=>1,'checked'=>$checked2,'style'=>'margin-top: 11px;'));
	 
	 echo form_label(lang('items_type_minor').':', 'minor',array('class'=>'wide','style'=>'width: 86px;'));	 
	 echo form_checkbox(array('name'=>'minor','id'=>'minor','value'=>1,'checked'=>$checked3,'style'=>'margin-top: 11px;'));
	 
	 echo form_label(lang('items_type_procedure').':', 'procedure',array('class'=>'wide','style'=>'width: 86px;'));	 
	 echo form_checkbox(array('name'=>'procedure','id'=>'procedure','value'=>1,'checked'=>$checked4,'style'=>'margin-top: 11px;'));?>
	</div>
</div>

	<div class="field_row clearfix"> 
	<?php echo form_label(lang('items_nhif_code_number').':', 'nhif_code',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'nhif_code',
			'id'=>'nhif_code',
			'value'=>$item_info->nhif_code_number)
		);?>
		</div>
	</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_category').':', 'category',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_info->category)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_supplier').':', 'supplier',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, $selected_supplier);?>
	</div>
</div>

<?php
if ($this->Employee->has_module_action_permission('items','see_cost_price', $this->Employee->get_logged_in_employee_info()->person_id) or $item_info->name=="")
{
?>
<div class="field_row clearfix">
<?php echo form_label(lang('items_cost_price').':', 'cost_price',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'size'=>'8',
		'id'=>'cost_price',
		'value'=>$item_info->cost_price)
	);?>
	</div>
</div>
<?php
}
else
{
	echo form_hidden('cost_price', $item_info->cost_price);
}
?>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_price').':', 'unit_price',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'size'=>'8',
		'id'=>'unit_price',
		'value'=>$item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_private_price').':', 'items_unit_private_price',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'private_price',
		'size'=>'8',
		'id'=>'private_price',
		'value'=>$item_info->unit_private_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_fast_track_price').':', 'items_unit_fast_track_price',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'fast_track_price',
		'size'=>'8',
		'id'=>'fast_track_price',
		'value'=>$item_info->unit_fast_track_price)
	);?>
	</div>
</div>

<!-- ***********START INSURANCE COMPANY PRICE DISPLAY SECTIONS********************-->
<?php $this->load->view('items/form_insurance_company_price');?>
<!-- ***********END INSURANCE COMPANY PRICE DISPLAY SECTIONS**********************-->


<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_nhif_price').':', 'nhif_price',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'nhif_price',
		'size'=>'8',
		'id'=>'nhif_price',
		'value'=>$item_info->unit_nhif_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_promo_price').':', 'promo_price',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_input(array(
        'name'=>'promo_price',
        'size'=>'8',
        'id'=>'unit_price',
        'value'=>$item_info->promo_price)
    );?>
    </div><br>
    <div style="text-align:center;">
            <?php echo lang('items_promo_start_date'); ?>:
            <?php echo form_dropdown('start_month',$months, $selected_start_month, 'id="start_month"'); ?>
            <?php echo form_dropdown('start_day',$days, $selected_start_day, 'id="start_day"'); ?>
            <?php echo form_dropdown('start_year',$years, $selected_start_year, 'id="start_year"'); ?>
            | <?php echo lang('items_promo_end_date'); ?>:
            <?php echo form_dropdown('end_month',$months, $selected_end_month, 'id="end_month"'); ?>
            <?php echo form_dropdown('end_day',$days, $selected_end_day, 'id="end_day"'); ?>
            <?php echo form_dropdown('end_year',$years, $selected_end_year, 'id="end_year"'); ?>
            <input type="hidden" id="hdn_start_date" name="hdn_start_date" value="<?php echo $item_info->start_date;?>" />
            <input type="hidden" id="hdn_end_date" name="hdn_end_date" value="<?php echo $item_info->end_date;?>" />
    </div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_1',
		'size'=>'8',
		'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_1',
		'size'=>'3',
		'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
	);?>
	%
	<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_2',
		'size'=>'8',
		'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_2',
		'size'=>'3',
		'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
	);?>
	%
	<?php echo form_checkbox('tax_cumulatives[]', '1', isset($item_tax_info[1]['cumulative']) && $item_tax_info[1]['cumulative'] ? (boolean)$item_tax_info[1]['cumulative'] : (boolean)$default_tax_2_cumulative); ?>
    <span class="cumulative_label">
	<?php echo lang('common_cumulative'); ?>
    </span>
	</div>
</div>

<div class="medicine_sec" <?php echo $items_disply_style;?>>
	<div class="field_row clearfix medicine_sec">
	<?php echo form_label(lang('items_quantity').':', 'quantity',array('class'=>'required wide ')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'quantity',
			'id'=>'quantity',
			'value'=>$item_info->quantity)
		);?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label(lang('items_reorder_level').':', 'reorder_level',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'reorder_level',
			'id'=>'reorder_level',
			'value'=>$item_info->reorder_level)
		);?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php $items_expire_y = date('Y'); $items_expire_d = date('d');;  $items_expire_m = date('m');; 
	if(isset($item_info->items_expire_date) && $item_info->items_expire_date!='')
	 {   $expire_date_array = explode('-',$item_info->items_expire_date);  //print_r($expire_date_array[0]);
		 $items_expire_y = $expire_date_array[0]; $items_expire_m = $expire_date_array[1];  $items_expire_d = $expire_date_array[2]; 
	 } ?>
	 <?php echo form_label(lang('items_expire_date').':', 'items_expire_date',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('items_expire_m',$months, $items_expire_m, 'id="items_expire_m"'); ?>
		<?php echo form_dropdown('items_expire_d',$days, $items_expire_d, 'id="items_expire_d"'); ?>
		<?php echo form_dropdown('items_expire_y',$years, $items_expire_y, 'id="items_expire_y"'); ?>
		
		</div>
	</div>	
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_location').':', 'location',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'location',
		'id'=>'location',
		'value'=>$item_info->location)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_info->description,
		'rows'=>'5',
		'cols'=>'17')
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_allow_alt_desciption').':', 'allow_alt_description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'allow_alt_description',
		'id'=>'allow_alt_description',
		'value'=>1,
		'checked'=>($item_info->allow_alt_description)? 1  :0)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_is_serialized').':', 'is_serialized',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'is_serialized',
		'id'=>'is_serialized',
		'value'=>1,
		'checked'=>($item_info->is_serialized)? 1 : 0)
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function getServiceType(type)
{  $('#service_type').val('');
   $('#sub_category_sec').hide(); 
   $('#specialized').attr('checked', false);$('#major').attr('checked', false);
   $('#minor').attr('checked', false);$('#procedure').attr('checked', false);
  if(type=='1')
  {  $('.medicine_sec').hide(); $('#quantity').val(0); $('#reorder_level').val(0); $('#services_dropdown_sec').show();  
     $('#items_generic_code').val(''); $('#items_generic').hide(); $('#items_type_label').show(); 
	 if($('#service_category').val()==5){ $('#sub_category_sec').show(); $('#items_generic_code').val(0); $('#items_type_sec001').hide(); }
  }
  else 
  { $('#quantity').val(''); $('#reorder_level').val(''); $('.medicine_sec').show(); $('#services_dropdown_sec').hide(); 
    $('#items_generic_code').val(''); $('#items_generic').show(); $('#items_type_label').hide(); $('#items_type_sec001').show();
  }
}

function getServiceCategory(type)
{  
  if(type=='5'){ $('#sub_category_sec').show(); $('#items_generic_code').val(0); $('#items_type_sec001').hide(); }
  else { $('#sub_category_sec').hide();$('#items_generic_code').val('');  $('#items_type_sec001').show(); }
}

//validation and submit handling
$(document).ready(function()
{ 
    $('#start_year,#start_month,#start_day,#end_year,#end_month,#end_day').change(function()
    {
        $("#hdn_start_date").val($("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val());
        $("#hdn_end_date").val($("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val());
    });

	$( "#category" ).autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});

	var submitting = false;

	$('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				submitting = false;
				tb_remove();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			multi_store_item:"required",
			name:"required",
			items_generic_code:"required",
			nhif_code:"required",
			category:"required",
			cost_price:
			{
				required:true,
				number:true
			},

			unit_price:
			{
				required:true,
				number:true
			},
			
			private_price:
			{
				required:true,
				number:true
			},
			
			fast_track_price:
			{
				required:true,
				number:true
			},
			
			tax_percent:
			{
				required:true,
				number:true
			}, 
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{	multi_store_item:<?php echo json_encode(lang('items_multi_store_name_required')); ?>,
			name:<?php echo json_encode(lang('items_name_required')); ?>,
			items_generic_code:<?php echo json_encode(lang('items_generic_code_required')); ?>,
			nhif_code:<?php echo json_encode(lang('items_nhif_code_required')); ?>,
			category:<?php echo json_encode(lang('items_category_required')); ?>,
			cost_price:
			{
				required:<?php echo json_encode(lang('items_cost_price_required')); ?>,
				number:<?php echo json_encode(lang('items_cost_price_number')); ?>
			},
			unit_price:
			{
				required:<?php echo json_encode(lang('items_unit_price_required')); ?>,
				number:<?php echo json_encode(lang('items_unit_price_number')); ?>
			},
			
			private_price:
			{
				required:<?php echo json_encode(lang('items_private_price_required')); ?>,
				number:<?php echo json_encode(lang('items_private_price_number')); ?>
			},
			
			private_price:
			{
				required:<?php echo json_encode(lang('items_fast_track_price_required')); ?>,
				number:<?php echo json_encode(lang('items_fast_track_price_number')); ?>
			},
			
			tax_percent:
			{
				required:<?php echo json_encode(lang('items_tax_percent_required')); ?>,
				number:<?php echo json_encode(lang('items_tax_percent_number')); ?>
			},
			quantity:
			{
				required:<?php echo json_encode(lang('items_quantity_required')); ?>,
				number:<?php echo json_encode(lang('items_quantity_number')); ?>
			},
			reorder_level:
			{
				required:<?php echo json_encode(lang('items_reorder_level_required')); ?>,
				number:<?php echo json_encode(lang('items_reorder_level_number')); ?>
			}

		}
	});
});
</script>