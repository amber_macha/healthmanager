<style>.field_row div.form_field input { width: auto;}</style>
<div id="dhtmltooltip"></div>
<script type="text/javascript">
var offsetxpoint=-60
var offsetypoint=20 
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""

function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function ddrivetip(thetext, thecolor, thewidth){
thecolor='green';
if(thetext==''){return false;}
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}

function positiontip(e){
if (enabletip){
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20
var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20

var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000

//if the horizontal distance isn't enough to accomodate the width of the context menu
offsetypoint1=-250;
if (rightedge<tipobj.offsetWidth)
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px"
else if (curX<leftedge)
tipobj.style.left="5px"
else
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetxpoint+offsetypoint1+"px"

//same concept with the vertical position
offsetypoint2=-60;
tipobj.style.top=curY+offsetypoint+offsetypoint2+"px"
tipobj.style.visibility="visible"

}
}

function hideddrivetip(){ 
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}
document.onmousemove=positiontip

</script>


<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("lab_investigation_header"); ?></legend>
<div class="form_field"> <label class="required lab-search-2"><?php echo lang('lab_investigation_category'); ?> :</label>
<label class="add_new_lab_search" for="first_name" style="margin-left: 152px;"><?php echo lang('search_lab_services'); ?>: 
<img id="searchspiner" class="search-spiner" src="<?php echo base_url();?>images/spinner_small.gif">
<input type="text" class="lab-search-box" id="search_lab_service" value="" name="search_lab_service">
</label>	
</div>

	<?php echo form_open('investigation/save/'.$person_id,array('id'=>'history_form','onsubmit'=>'return validateCustomer()')); ?>
	<input type="hidden" name="category_list" id="category_list" value="">
	<div class="field_row clearfix" style="margin:0px;">	
		<div class='form_field'>		
			<div id="labcategory" class="lab_invest_category_sec"><?php echo $lab_category;?></div>
		</div>
	</div>
<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
<input id="active_deactive" class="active_deactive" type="button" onclick="ActiveDeActivate();" value="<?php echo $this->lang->line("active_deactive_lab_servics");?>">

<?php echo form_close();?>

</fieldset>

<!---start section for clarification description popup-->
<div class="popup_section" id="Dosagethem_pop" style="width: 450px;">
	<div><label class="required clarification"><?php echo lang('labservices_clarification_description_form'); ?> </label></div>	
	<div class="updatemessage" id="updatemessage"></div>
	<input type="hidden" id="service_id" value="" name="service_id" >
	<input type="hidden" id="service_name" value="" name="service_name" >
	<div class="form_field" style=" margin-bottom: 20px;margin-top:20px;" > 
		<label class="lab-search" style="color:#000000;"><?php echo lang('common_description'); ?>: 
		<input type="text" class="add-clarification" id="dosage_description" name="dosage_description" value="" MaxLength="50" onClick="clicktohide(this);"></label>	
	</div>		
<div class="move002" style="float:left;">
<input class="add-button" type="button" onclick="saveItemsDesciption();" value="<?php echo lang("common_submit");?>" name="importsubmit">
<input class="add-button" type="button" onclick="showThem('hideDosagethem');" value="<?php echo $this->lang->line("close_div");?>">
</div>
</div>
<!---end section for clarification description popup-->

<script type='text/javascript'>
//**Active Deactivate Lab Services code**//
function ActiveDeActivate()
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('lab_services_empty_active_deactivate');?>');return false; }  
	if (confirm('<?php echo lang('lab_services_change_status');?>')) 
	{
	var searchURL ='<?php echo base_url().'investigation/activeDeActivate/'.$person_id.'/';?>' +checkedval;
	$('#searchspiner').show();
	$.get(searchURL, function(data) { $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').html(''); } , 5000); search(true); });
	}	
	else{return false;}
}

function GetItemsDesciption(id)
{   
   var GetDataURL = "<?php echo base_url().'medicine/getMedicne_Drugs_Data/'.$person_id;?>";
     $.ajax({
            url: GetDataURL,
            type: 'post',
			data: 'services_id='+id ,
			dataType: "json",
            success: function(data) 
				{	
                  if(data.success) 
				  { 
					$("#dosage_description").val(data.description); 
					$("#service_name").val(data.service_name); 
					$("#service_id").val(id); 			  
					showThem('viewDosagethem');
				  }	
				}
        }); 
}

//**************save Dosase conetnt in databse*********************//
function saveItemsDesciption()
{
	dosage_description = $("#dosage_description").val(); 
	service_name = $("#service_name").val(); 
	service_id = $("#service_id").val();
	if(service_id==''){ alert('<?php echo lang('items_selection_empty');?>');return false; } 
	if(dosage_description==''){ alert('<?php echo lang('common_clarification_description_required');?>');return false; }  
	var searchURL ="<?php echo base_url().'investigation/addClarificationDesciption/'.$person_id;?>" ;
    $.ajax({
            url: searchURL,
            type: 'post',
			data: 'description='+dosage_description + '&service_id='+service_id ,
            success: function(data) 
				{ 	$('#updatemessage').html(''); 				
					if(data=='0'){ $('#updatemessage').html('<?php echo lang('common_clarification_added_failed');?>'); return false; }
					else{  
						 showThem('hideDosagethem'); 
						 $('#error_message_box').html('<?php echo lang('common_clarification_added_successful');?>'); 
						 setTimeout( function(){ $('#error_message_box').html(''); },5000);
						 search(service_name='');
					}
				}
        }); 
}
//**************popup form display code*********************//
function showThem(type)
{   if(type=='viewDosagethem'){$('#Dosagethem_pop').show(); }
	if(type=='hideDosagethem')
	{
		$("#service_id").val(''); $("#dosage_description").val(''); 
		document.getElementById('dosage_description').style.background = '';
		$('#Dosagethem_pop').hide();
	}	
}
//**************lab searching code*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});
function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  	var searchURL ="<?php echo base_url().'investigation/getservices/'.$person_id;?>";	$('#searchspiner').show();	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval,
            success: function(data) { $('#labcategory').html(data);  $('#searchspiner').hide(); }
        }); }
//**************lab searching code*********************//
function validateCustomer()
{
	var n = $( "input:checked" ).length;
	if(n==0){ return false; }
	else { 
		$(':checkbox:checked').each(function(i){
		if($(this).val()==0){ alert('<?php echo lang('lab_services_not_selected');?>'); $('#lab_'+id).attr('checked', false); return false; }				}); 
	document.getElementById('category_list').value = n;  
	} 
 }  
//validation and submit handling
$(document).ready(function()
{	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			category_list: "required"
   		},
		messages: 
		{     		
     		category_list: <?php echo json_encode(lang('select_lab_categort')); ?>
		}	});});
</script>