<?php echo form_open('investigation/savereport/'.$person_id.'/'.$invest_id,array('id'=>'history_form','enctype'=>'multipart/form-data')); ?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("post_lab_investigation_report"); ?></legend><div class="field_row clearfix">
<?php echo form_label(lang('postinvestigation_result').'', 'postinvestigation_result',array('class'=>'required add-post-report-title')); ?><div class="clarification_result"> <span class="clarification_result_title"><?=lang('common_clarification');?>:</span> <?php echo $this->Customer->getClarificationDesciption($invest_id,'investigation');?></div> 		<div class="field_row clearfix">			<label style="width: 130px;"><?php echo lang('common_upload_result');?> </label>			<div class='form_field'> <input type="file" name="report_image" id="report_image"> </div>		</div>			
	<div class="form_field">
	<?php echo form_textarea(array(
		'name'=>'investigation_report',
		'id'=>'investigation_report',
		'rows'=>'15',
		'cols'=>'60'));?>
	</div>
</div>
<?php echo form_submit(array('name'=>'upload','id'=>'upload','value'=>lang('common_submit'),'class'=>'submit_button float_right add-post-report'));?>
</fieldset>
<?php echo form_close();?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			investigation_report: "required"
   		},
		messages: 
		{     		
     		investigation_report: <?php echo json_encode(lang('post_investigation_report')); ?>
		}
	});
});
</script>