<?php echo form_open('investigation/savereport/'.$person_id.'/'.$invest_id,array('id'=>'history_form')); ?>
<fieldset id="customer_basic_info">
<legend><?php echo lang("view_lab_investigation_report"); ?></legend>

	

<div class="field_row clearfix">	
<label class="required add-view-report-title" style="width:206px;" for="<?php echo lang('investigation_result');?>"><?php echo lang('investigation_result');?></label>
	<div class="form_field">
	<?php echo form_textarea(array(
		'name'=>'investigation_report',
		'id'=>'investigation_report',
		'value'=>$viewreport_details,
		'readonly'=>'readonly',
		'rows'=>'15',
		'cols'=>'60')		
	);?>
	</div>
</div>