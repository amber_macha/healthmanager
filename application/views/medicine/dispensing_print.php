<?php $this->load->view("partial/header");?>
<?php if(isset($error_message)){ echo '<h1 style="text-align: center;">'.$error_message.'</h1>';exit; } ?>

<div id="receipt_wrapper">
	<div id="receipt_header">
		<div id="company_name"><?php echo $this->config->item('company'); ?></div>
		<?php if($this->config->item('company_logo')) {?>
		<div id="company_logo"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>
		<?php } ?>
		<div id="company_address"><?php echo nl2br($this->config->item('address')); ?></div>
		<div id="company_phone"><?php echo $this->config->item('phone'); ?></div>
		<div id="sale_time"><?php echo date('d-m-Y h:i:s a');?></div>
	</div>
	<div id="receipt_general_info">
		<div id="employee"><?php echo lang('customers_customer').": ".ucwords(strtolower($person_info->first_name)).' '. ucwords(strtolower($person_info->last_name)); ?></div>
        Account No : <?php echo $person_info->account_number; ?>  
	</div>

	<table id="receipt_items">
	<tr>
		<th style="width:20%;text-align:left;"><?php echo lang('medicine_drugs_list'); ?></th>
		<th style="width:15%;text-align:left;"><?php echo lang('common_doctor_name'); ?></th>
		<th style="width:10%;text-align:left;"><?php echo lang('medicine_quantity'); ?></th>
		<th style="width:15%;text-align:left;"><?php echo lang('medicine_dispensing'); ?></th>
		<th style="width:30%;text-align:left;"><?php echo lang('medicine_dosage_title'); ?></th>
		<th style="width:10%;text-align:center;"><?php echo lang('date'); ?></th>
	</tr>
	
	<?php foreach($manage_AllLabTest->result() as $data )
		{ 
			$dosage_description = $this->medicinetest->get_medicine_drugs_dosage($data->lab_category_id,$person_info->person_id);?>
		<tr>
			<td style="text-align:left;"><?php echo ucwords(strtolower($data->name)); ?></td>
			<td style="text-align:left;"><?php echo ucwords(strtolower($data->first_name)).' &nbsp; '.ucwords(strtolower($data->last_name)); ?></td>
			<td style='text-align:left;'><?php echo $data->quantity; ?></td>
			<td style='text-align:left;'><?php if($data->quantity_dispansed=='1') echo lang('medicine_dispensed');else echo lang('medicine_not_dispensed'); ?></td>
			<td style='text-align:left;'><?php echo $dosage_description; ?></td>
			<td style='text-align:center;'><?php echo date('d/m/Y',strtotime($data->added_date)); ?></td>
		
		</tr>
		<?php }	?>

	 <tr><td colspan="6">&nbsp;</td></tr>

	</table>

	<div id="signature">----------------------------------------------</div>
	
		<?php echo OurCompanySignature();?>

</div>

<?php $this->load->view("partial/footer"); ?>
<?php if ($this->Appconfig->get('print_after_sale')){?>
<script type="text/javascript"> $(window).load(function(){ window.print(); window.close(); });</script> 
<?php }?>