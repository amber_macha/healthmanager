<?php $this->load->view("partial/header"); ?>
<style>table {border-color:#FFFFFF;}
.button_green {margin-left: auto;}
.button_red {margin-left: auto;}
</style>
<script type="text/javascript">
function SendRequestTo_Cashier(id)
{ 
	var searchURL ='<?php echo base_url()."settings/SendRequestToCashier/$person_info->person_id";?>';	 
	if (confirm('<?php echo lang('common_confirm_cashier_send_request_status');?>')) 
	 {
	 	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'items_id=' + id + '&category_type=medicine_drugs', 
			dataType:'json',
            success: function(response) 
			{   tb_remove();   
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true); 
					return false;
				}
				
				do_search(true,function()
				{
					set_feedback(response.message,'success_message',false);
				});
			}
        });   
	 }
     else {return false;}	 
}

$(document).ready(function() 
{ 
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$person_info->person_id");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success) { set_feedback(response.message,'error_message',true); }	
	else
	{   
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{ 
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else 
		{  
			do_search(true,function()
			{
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function Print_Dosage(id)
{   
	var getURL ='<?php echo base_url().$controller_name."/print_dosage/$person_info->person_id/";?>'; 
	var setURL ='<?php echo base_url().$controller_name."/setPrint_Dosage/$person_info->person_id/";?>'; 	
	if (confirm('<?php echo lang('common_confirm_printing');?>')) 
	 {
	    $.ajax({
            url: setURL,
            type: 'post',
            data: 'items_id=' + id,
			dataType:'json',
            success: function(response) 
			{ 
				if(response.success)
				{
					 window.open(getURL, '_blank');
				}			
				else {alert('Please try again!');}
			}
        }); 	   
	 }
      else { return false; }	 
}

function Print_Selected_Dosage()
{ 
   var checkedval='';
    $(':checkbox:checked').each(function(i){ 
		if($(this).val()!='on')
		{ 
			if(checkedval=='') { checkedval = $(this).val(); } else { checkedval = checkedval + ',' + $(this).val(); } 
		} 
	});
	if(checkedval=='') { alert('<?php echo lang('medicine_print_selected_empty')?>'); return false; }
	if (confirm('<?php echo lang('common_confirm_printing');?>')) 
	 {
	    $.ajax({
            url: '<?php echo base_url().$controller_name."/setPrint_Dosage/$person_info->person_id/";?>', 
            type: 'post',
            data: 'items_id=' + checkedval,
			dataType:'json',
            success: function(response) 
			{ 
				if(response.success)
				{
					 window.open('<?php echo base_url().$controller_name."/print_dosage/$person_info->person_id/";?>', '_blank');
				}			
				else { alert('<?php echo lang('medicine_print_selected_empty')?>');}
			}
        }); 	   
	 }
      else { return false; }	 
}

function Change_Status(id)
{ 
	 var searchURL ='<?php echo base_url().$controller_name."/chanage_status";?>';	 
	if (confirm('<?php echo lang('common_confirm_change_status');?>')) 
	 {
	 	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'items_id=' + id,
			dataType:'json',
            success: function(response) 
			{  
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true);
				}
				
				do_search(true,function()
				{
					set_feedback(response.message,'success_message',false);
				});
			}
        });   
	 }
     else {return false;}	 
}

</script>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/$search_url/$person_info->person_id",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">
				<?php if($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				{?>
					<?php echo anchor("$controller_name/view/$person_info->person_id/width~600",
					$this->lang->line($controller_name.'_new'),	array('class'=>'thickbox none new', 'title'=>$this->lang->line($controller_name.'_new')));
				}
				if($this->Employee->has_module_action_permission($controller_name, 'drugs_profile', $this->Employee->get_logged_in_employee_info()->person_id))
				{
					if($method_name=='drugs_profile')
					{ 
					  echo anchor("medicine/index/$person_info->person_id/$Diabetes_Customer_Type",$this->lang->line('medicine_medicine_drugs_list'),array('class'=>' none new', 'title'=>$this->lang->line('medicine_medicine_drugs_list'))); 
					}
					else { echo anchor("medicine/drugs_profile/$person_info->person_id/$Diabetes_Customer_Type",$this->lang->line('medicine_drugs_profile'),array('class'=>' none new', 'title'=>$this->lang->line('medicine_drugs_profile')));}
			    }
			   if($method_name!='drugs_profile')
				{ 
					if ($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id))
					{ echo anchor("$controller_name/delete/$person_info->person_id",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); }
				} ?>	
			</div>
			<?php $this->load->view('partial/patient_waiting_counter');?>	
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
		<div id="table_holder">			
	<table class="tablesorter">
	
	<tr><th class="leftmost"></th></tr>
	
	<tr> <td width="90%" style="background-color:#A0CFEC; border-left: 1px solid #BBBBBB;">
	   <?php $this->load->view('partial/history_customers_menu');?>		   
				<div class="heigthdiv"></div>
				<?=$manage_history_menu;?>
				<div class="heigthdiv1"></div>	 
				
		        <div class="comment-header"><?php echo $list_title_name; ?></div>		   
				<table id="sortable_table" class="tablesorter" border="6">
				<tbody><?php echo $medicine_Drugs_List;?></tbody>
				</table>	
		</td>
	</tr>
	
	</table>
				
			 </div>
			
			</div>
			<div id="pagination">
				<?php //echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>