<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("medicine_drug_management"); ?></legend>

<div class="profile_div">	
	<?php /*?><div class="profile_info_right"><input type="button" class="button_blue"  value="<?=lang('common_patient_profile');?>" onclick=""></div><?php */?>

	<div class="profile_info_left">		
		<div class="profile-info"><div class="profileinfo01"><?php echo lang('patient_name');?> :</div> <div class="peopleinfo2"><?=ucwords(strtolower($person_info->first_name)).' '. ucwords(strtolower($person_info->last_name)); ?></div>  </div> 
		<div class="profile-info"><div class="profileinfo01"><?php echo lang('patient_account');?> :</div> <div class="peopleinfo2"><?=$person_info->account_number;?></div>  </div> 
		<div class="profile-info"><div class="profileinfo01"><?php echo lang('medicine_drug_name');?> :</div> <div class="peopleinfo2"><?=ucfirst($drug_name);?></div>  </div> 
	</div>
</div>
<div class="field_row_dotted1"></div>

<div class="field_row_sec clearfix"> 
	<label class="dosage_count"><?=lang('common_dosage_count');?> : <b><?=$count_dosage;?></b></label>
	 <div class="dosage_update_sec2">
		<input type="button" class="button_blue2" value="<?=lang('common_change_count');?>" onclick="showThem('viewthemChange');">
		<input type="button" class="button_green" value="<?=lang('medicine_drug_dispanse');?>"  onclick="showThem('viewthemDispanse');">
		<input type="button" class="button_red" value="<?=lang('medicine_drug_cancel');?>" onclick="showThem('viewthemCancel');">
		
		<?php $exist_request = $this->functions->existCashierRequest($customer_id,$category_type='medicine_drugs',$medicine_id);
		if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
		{
			if($exist_request) {?><input type="button" class="send_not_request_1" value="<?=lang('common_send_to_cashier');?>">
			<?php }else{?>
					<input type="button" class="send_request_1" value="<?=lang('common_send_to_cashier');?>" onclick="SendRequestTo_Cashier('<?=$medicine_id;?>');">
		<?php }}?>		
	 </div>
</div>

<div class="field_row_dotted" style=" margin-top: 10px;"></div>

<?php echo form_open('medicine/save_medicine_history/'.$customer_id,array('id'=>'history_form')); ?>

<div class="field_row_sec clearfix"> 
	<label class="dosage_count"><?=lang('medicine_dispensing_option');?> :</label> 
	 <div class="dosage_update_sec">
		<input type="radio" name="dispensing_option" value="0" <?php if($medicine_data->dispensing_options=='0')echo 'checked="checked"';?>> <?=lang('medicine_automatic_dispencing');?>
		<input type="radio" name="dispensing_option" value="1" <?php if($medicine_data->dispensing_options=='1')echo 'checked="checked"';?>> <?=lang('medicine_manually_dispencing');?>
	 </div>
</div> 

<div class="field_row_sec clearfix"> 
	<label class="dosage_count"><?=lang('common_patient_status');?> :</label> 
	 <div class="dosage_update_sec">
		<input type="radio" name="patient_status" value="0" <?php if($medicine_data->patient_status=='0')echo 'checked="checked"';?>> <?=lang('common_opd');?>
		<input type="radio" name="patient_status" value="1" <?php if($medicine_data->patient_status=='1')echo 'checked="checked"';?>> <?=lang('common_ipd');?>
	 </div>
</div> 

<div class="field_row_sec clearfix"> 
	<label class="dosage_count"><?=lang('medicine_drug_comment');?> :</label> 
	<div class="dosage_update_sec">	
		<div class='form_field'><textarea id="medicine_comment" name="medicine_comment" rows="4" cols="30" onclick="clicktohide(this);"><?php echo $medicine_data->medicine_comment;?></textarea></div>
		<img id="searchspiner" class="search-spiner03" src="<?php echo base_url();?>images/spinner_small.gif">
		<input class="button_green" style="margin-top:5px;margin-left: 68px;" type="button" value="<?=lang('common_submit_comment');?>" onclick="return save_Comment()"/>
	</div> 	
</div>
<?php echo form_close();?>
</fieldset>

<!---start pupup div section for change counts-->
<div id="drugs_popup_change" class="popup_section">
	<div class="field_row clearfix">	
		<label><?php echo lang('common_change_count'); ?>:</label>	
		<div class="form_field"><input type="text" onclick="clicktohide(this);" id="change_count" value="" name="change_count"></div>
	</div>

	<div class="field_row clearfix">	
		<label><?php echo lang('common_change_reason'); ?>:</label>	
		<div class="form_field"><input type="text" onclick="clicktohide(this);" id="change_reason" value="" name="change_reason"></div>
	</div>

	<div class="move003">
		<img id="searchspiner1" class="search-spiner04" src="<?php echo base_url();?>images/spinner_small.gif">	
		<input class="add-button" type="button" onclick="updateCount();" value="<?php echo lang("common_submit");?>">
		<input class="add-button" type="button" onclick="showThem('hidepopup_Change');" value="<?php echo $this->lang->line("close_div");?>">
	</div>	
</div>


<!---start pupup div section for cancel drugs-->
<div id="drugs_popup_cancel" class="popup_section">	
	<div class="field_row clearfix">	
		<label><?php echo lang('common_cancel_reason'); ?>:</label>	
		<div class="form_field"><input type="text" onclick="clicktohide(this);" id="cancel_reason" value="" name="cancel_reason"></div>
	</div>

	<div class="move003">
		<img id="searchspiner2" class="search-spiner04" src="<?php echo base_url();?>images/spinner_small.gif">	
		<input class="add-button" type="button" onclick="drugs_Cancel();" value="<?php echo lang("common_submit");?>">
		<input class="add-button" type="button" onclick="showThem('hidepopup_Cancel');" value="<?php echo $this->lang->line("close_div");?>">
	</div>	
</div>

<!---start pupup div section for dispanse counts-->
<div id="drugs_popup_dispanse" class="popup_section">
	<div class="field_row clearfix">	
		<label style="width:116px"><?php echo lang('common_dispanse_count'); ?>:</label>	
		<div class="form_field"><input type="text" onclick="clicktohide(this);" id="dispanse_count" value="" name="dispanse_count"></div>
	</div>

	<div class="field_row clearfix">	
		<label style="width:116px"><?php echo lang('common_dispanse_reason'); ?>:</label>	
		<div class="form_field"><input type="text" onclick="clicktohide(this);" id="dispanse_reason" value="" name="dispanse_reason"></div>
	</div>

	<div class="move004">
		<img id="searchspiner3" class="search-spiner04" src="<?php echo base_url();?>images/spinner_small.gif">	
		<input class="add-button" type="button" onclick="updateDispanse();" value="<?php echo lang("common_submit");?>">
		<input class="add-button" type="button" onclick="showThem('hidepopup_Dispanse');" value="<?php echo $this->lang->line("close_div");?>">
	</div>	
</div>


<script type='text/javascript'>
var dispensing_options = '<?php echo $medicine_data->dispensing_options;?>';

function save_Comment()
{
  //var medicine_comment = $("#medicine_comment").val();
  //if(medicine_comment==''){  $("#medicine_comment").css("background", "#FFFF00 ");$("#medicine_comment").css("border", "1px solid #FF0000"); return false; }
  $('#searchspiner').show();	
	$.ajax({
            url: '<?php echo base_url().'medicine/save_medicine_history/'.$customer_id.'/'.$medicine_id.'/dosage_comment';?>',
            type: 'post',
            data: $("#history_form").serialize(),
			dataType:'json',
            success: function(data) 
			{  
			   $('#searchspiner').hide();
			   tb_remove();
			   post_person_form_submit(data);
			}
        }); 
		
}

function updateCount()
{ 
  var change_count = $("#change_count").val(); 
  var change_reason = $("#change_reason").val();
  if(change_count=='')   { $("#change_count").css("background","#FFFF00");  $("#change_count").css("border", "1px solid #FF0000"); return false; }
  if(isNaN(change_count)){ $("#change_count").css("background","#FFFF00");  $("#change_count").css("border", "1px solid #FF0000"); return false; } 
  if(change_reason=='')  { $("#change_reason").css("background","#FFFF00"); $("#change_reason").css("border", "1px solid #FF0000"); return false; }

  $('#searchspiner1').show();	
	$.ajax({
            url: '<?php echo base_url().'medicine/save_medicine_history/'.$customer_id.'/'.$medicine_id.'/change_count';?>',
            type: 'post',
            data: 'change_count='+change_count + '&change_reason='+change_reason,
			dataType:'json',
            success: function(data) 
			{  $('#searchspiner1').hide();
			   tb_remove();
			   post_person_form_submit(data);
			}
        }); 
}

function updateDispanse()
{ 
  var dispanse_count = $("#dispanse_count").val(); 
  var dispanse_reason = $("#dispanse_reason").val();
  if(dispanse_count=='')   { $("#dispanse_count").css("background","#FFFF00");  $("#dispanse_count").css("border", "1px solid #FF0000"); return false; }
  if(isNaN(dispanse_count)){ $("#dispanse_count").css("background","#FFFF00");  $("#dispanse_count").css("border", "1px solid #FF0000"); return false; } 
  if(dispanse_reason=='')  { $("#dispanse_reason").css("background","#FFFF00"); $("#dispanse_reason").css("border", "1px solid #FF0000"); return false; }

  $('#searchspiner3').show();	
	$.ajax({
            url: '<?php echo base_url().'medicine/save_medicine_history/'.$customer_id.'/'.$medicine_id.'/change_dispanse';?>',
            type: 'post',
            data: 'dispanse_count='+dispanse_count + '&dispanse_reason='+dispanse_reason,
			dataType:'json',
            success: function(data) 
			{  $('#searchspiner3').hide();
			   tb_remove();
			   post_person_form_submit(data);
			}
        }); 
}

function drugs_Cancel()
{
  var cancel_reason = $("#cancel_reason").val();
  if(cancel_reason==''){  $("#cancel_reason").css("background", "#FFFF00 ");$("#cancel_reason").css("border", "1px solid #FF0000"); return false; }
  if(confirm('<?php echo lang('medicine_drugs_dosage_confirm_cancel');?>')) 
	{ 
	   $('#searchspiner2').show();	
		$.ajax({
				url: '<?php echo base_url().'medicine/save_medicine_history/'.$customer_id.'/'.$medicine_id.'/cancelDrugs';?>',
				type: 'post',
				data: 'cancel_reason='+cancel_reason,
				dataType:'json',
				success: function(data) 
				{  
				   $('#searchspiner2').hide();
				   tb_remove();
				   post_person_form_submit(data);
				}
			}); 
	} else { return false; }		
}

function showThem(type)
{ 
	if(type=='viewthemChange')  { $('#drugs_popup_change').show(); }
	if(type=='viewthemCancel')  { $('#drugs_popup_cancel').show(); }
	if(type=='viewthemDispanse')
	{ 
	   if(dispensing_options==1) { alert('<?php echo lang('medicine_drugs_dispanse_disabled');?>'); return false; }	
	   $('#drugs_popup_dispanse').show(); 
	}
	if(type=='hidepopup_Change'){ $("#change_count").val('');  $("#change_reason").val('');  $('#drugs_popup_change').hide(); }
	if(type=='hidepopup_Cancel'){ $("#cancel_reason").val('');  $('#drugs_popup_cancel').hide(); }
	if(type=='hidepopup_Dispanse'){ $("#dispanse_count").val(''); $("#dispanse_reason").val(''); $('#drugs_popup_dispanse').hide(); }
}

</script>