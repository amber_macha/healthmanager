<style>
.bill_area{width:800px; margin-left: 5%;}
.request-section {
   border: 1px solid #000000;
    border-radius: 14px;
    color: #000000;
    font-weight: bold;
    height: 85px;
    padding: 1px;
    width: 250px;
}
.item_type{font-weight:bold;padding:5px;}
.item_type_right{float:right;}
.item_type3{font-weight:normal;padding:5px;}
.item_type_sec{width:auto;}
.item_type_sec1{}
.unit_consume_sec{ border: 1px solid #000000;
				   border-radius: 12px;
				   color: #000000;
				   font-weight: bold;
				   height: 36px;
				   padding: 1px;
				   width: 240px;
				   margin-top: 8px; margin-bottom: 20px;
				 }
.company_logo_image { width: auto;float:right;position:absolute;margin: -22px -42px;}
</style>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" bgcolor="#f2f2f2">
	<table cellspacing="0" cellpadding="0" bgcolor="#f2f2f2" width="100%">
		<tr>
			<td valign="top">
				<table align="center" cellpadding="0" cellspacing="0" width="900" style="text-align:left;">
					<tr><td height="5" style="line-height:0;font-size:0;"></td></tr>					
					<tr>
						<td valign="top">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td height="11" style="line-height:0;font-size:0;"></td>
								</tr>
								<tr>
									<td valign="top">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td width="27" bgcolor="#d6d5d5"></td>
												<td width="552" height="50" valign="top" bgcolor="#d6d5d5">
													<font face="Verdana, Geneva, Arial, sans-serif" color="#151515" size="2" style="font-size:22px;line-height:29px;"><b><?=$subject;?></b></font>
												</td>
												<td align='center' bgcolor="#ffffff" width="12%"><img class="company_logo_image" src="<?php echo base_url();?>images/thumb_gov_tanzania_logo.png"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
								</tr>
							</table>
						</td>
					</tr>
	<tr>
	<td valign="top">
	<table cellpadding="0" cellspacing="0" width="900">
	<tr>
		<td width="1" bgcolor="#f0f0f0"></td>
		<td width="1" bgcolor="#ededed"></td>
		<td width="1" bgcolor="#eae9e9"></td>
		<td width="1" bgcolor="#e5e5e5"></td>
		<td width="1" bgcolor="#e1e1e1"></td>
		<td width="1" bgcolor="#dfdede"></td>
		<td width="1" bgcolor="#dddcdc"></td>
		<td bgcolor="#ffffff" valign="top">	

			<div class="bill_area">
			<table  border=0>			
			<tr>
			<td><font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;">Hi, <?=$employe_name;?></font></td>
			</tr>
			<tr><td><font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;"><?=lang('items_notification_updating');?></font></td>	</tr>
			<tr><td></td></tr>
			</table>

			<div style="float:left;width:100%;margin-bottom:25px;height: 50px;font-size:20px;"><span style=" font-weight:bold"><?=lang('items_item_number');?> :</span> <?=$Notification_Data['items_item_number'];?>  
			<br><span style=" font-weight:bold"> <?=lang('items_name');?> :</span> <?=$Notification_Data['item_name'];?>
			</div>

			<div style="margin-top: 20px;padding: 5px 5px 10px;">
				<div style="width:35%; float:left;">
					<div class="item_type_sec"> <span class="item_type"> <?=lang('items_category');?> :</span> <span class="item_type_right"> <?=$Notification_Data['Category'];?> </span></div>
					<div class="item_type_sec"> <span class="item_type"><?=lang('items_item_type');?> : </span> <span class="item_type_right"> <?=$Notification_Data['item_type'];?> </span></div>
					<div class="item_type_sec"> <span class="item_type"> <?=lang('items_cost_price');?>: </span> <span class="item_type_right"> <?=$Notification_Data['items_cost_price'];?> </span></div>
				</div>
				<div style="width:35%; float:right;">
						<div class="item_type_sec">  <span class="item_type"> <?=lang('items_quantity');?>  :</span>  	<span class="item_type_right"> <?=$Notification_Data['Quantity'];?> </span></div>
						<div class="item_type_sec">  <span class="item_type"> <?=lang('items_reorder_level');?>  :</span>  	<span class="item_type_right"> <?=$Notification_Data['reorder_level'];?> </span></div>
						<div class="item_type_sec">  <span class="item_type"> <?=lang('items_expire_date');?> : </span>  <span class="item_type_right"> <?=$Notification_Data['items_expire_date'];?> </span></div>
				</div>
			</div>

					<div style="padding: 5px 10px 10px;margin-top:120px;float:left;">
					<font face="Verdana, Geneva, Arial, sans-serif" color="#3f3f3f" size="2" style="font-size:14px;line-height:19px;"><?=$signature;?></font>	
					</div>
			</div>

				
	      </td>
	    </tr>
	 </table>
	</td>
	</tr>

	<tr>
	<td height="18" style="line-height:0;font-size:0;"></td>
	</tr>					
	</table>

			</td>
		</tr>
	</table>
</body>