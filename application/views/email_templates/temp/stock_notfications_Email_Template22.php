<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" bgcolor="#f2f2f2">
	<table cellspacing="0" cellpadding="0" bgcolor="#f2f2f2" width="100%">
		<tr>
			<td valign="top">
				<table align="center" cellpadding="0" cellspacing="0" width="599" style="text-align:left;">
					<tr>
						<td height="5" style="line-height:0;font-size:0;"></td>
					</tr>
					<tr>
						<td height="6" style="line-height:0;font-size:0;">
							<img src="<?php echo base_url();?>images/email_images/img-header-top.gif" width="594" height="6" align="left" vspace="0" hspace="0">
						</td>
					</tr>
					
					<tr>
						<td valign="top" bgcolor="#d6d5d5">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td height="11" style="line-height:0;font-size:0;"></td>
								</tr>
								<tr>
									<td valign="top">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td width="27"></td>
												<td width="552" valign="top">
													<font face="Verdana, Geneva, Arial, sans-serif" color="#151515" size="2" style="font-size:26px;line-height:29px;"><b><?php echo $subject;?></b></font>
												</td>
												<td width="20"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="14" style="line-height:0;font-size:0;"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<table cellpadding="0" cellspacing="0" width="594">
								<tr>
									<td width="1" bgcolor="#f0f0f0"></td>
									<td width="1" bgcolor="#ededed"></td>
									<td width="1" bgcolor="#eae9e9"></td>
									<td width="1" bgcolor="#e5e5e5"></td>
									<td width="1" bgcolor="#e1e1e1"></td>
									<td width="1" bgcolor="#dfdede"></td>
									<td width="1" bgcolor="#dddcdc"></td>
									<td width="587" bgcolor="#ffffff" valign="top">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td colspan="4" height="24" style="line-height:0;font-size:0;"></td>
											</tr>
											<tr>
												<td width="31"></td>
												<td width="500" valign="top">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td height="13" style="line-height:0;font-size:0;"></td>
														</tr>
														<tr>
															<td>
																<font face="Verdana, Geneva, Arial, sans-serif" color="#3f3f3f" size="2" style="font-size:14px;line-height:19px;">Hi,  <?=ucwords($customer_name);?>,</font>
															</td>
														</tr>														
														<tr>
															<td>
																<font face="Verdana, Geneva, Arial, sans-serif" color="#3f3f3f" size="2" style="font-size:14px;line-height:19px;">Thanks for your registration!</font>
															</td>
														</tr>
                                                        
                                                        
                        <tr>
                        <td height="19" style="line-height:0;font-size:0;"></td>
                        </tr>
                        <tr>
                        <td>
                        <font face="Verdana, Geneva, Arial, sans-serif" color="#ea9d25" size="2" style="font-size:14px;line-height:19px;"> Please use your login details as given below<br /></font>
						<font face="Verdana, Geneva, Arial, sans-serif" color="#3f3f3f" size="2" style="font-size:14px;line-height:19px;">Account No : <?=$LoginUsername;?> <br>  Password : <?=$password;?> <br> 
                        
						<font face="Verdana, Geneva, Arial, sans-serif" color="#ea9d25" size="2" style="font-size:14px;line-height:19px;"> You can login by click as given below link<br/></font>
						<a href="<?=$setting_url;?>" style="color:#1c6589 !important;text-decoration:underline;"><font color="#1c6589">click here</font></a> </font>
                        </td>
                        </tr>
                           			
													
														<tr>
															<td height="90" style="line-height:0;font-size:0;"></td>
														</tr>
														<tr>
															<td>
																<font face="Verdana, Geneva, Arial, sans-serif" color="#3f3f3f" size="2" style="font-size:14px;line-height:19px;"><?php echo $signature;?></font>
															</td>
														</tr>
														<tr>
															<td height="52" style="line-height:0;font-size:0;"></td>
														</tr>
													</table>
												</td>
												<td width="13"></td>
												<td width="233" valign="top">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td height="35" style="line-height:0;font-size:0;"></td>
														</tr>
														<tr>
															<td valign="top">
																<table cellpadding="0" cellspacing="0" width="203">
																	<tr>
																		<td width="10"></td>
																		<td width="193" bgcolor="#f3f3f3" valign="top">
																			
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td height="60" style="line-height:0;font-size:0;"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="7" style="line-height:0;font-size:0;">
							<img src="<?php echo base_url();?>images/email_images/img-footer.gif" width="594" height="7" align="left" vspace="0" hspace="0">
						</td>
					</tr>
					<tr>
						<td height="18" style="line-height:0;font-size:0;"></td>
					</tr>					
				</table>
			</td>
		</tr>
	</table>
</body>