<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('company').' -- '.lang('common_powered_by').' PHP SYNTAX PLC( For any Problem Call +255 232 402465, +255 782566633)' ?></title>
	 <link type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" rel="shortcut icon">  
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/phppos.css?'.APPLICATION_VERSION;?>" media="all" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/general.css?'.APPLICATION_VERSION;?>" media="all" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/tables.css?'.APPLICATION_VERSION;?>" media="all" />	
</head>
<body>

<div id="content_area_wrapper">
<div id="content_area">	

<table  border=0>	
		<tr>
			<td valign="top">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td height="11" style="line-height:0;font-size:0;"></td>
					</tr>
					<tr>
						<td valign="top">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="27" bgcolor="#d6d5d5"></td>
									<td width="552" height="50" valign="top" bgcolor="#d6d5d5">
										<font face="Verdana, Geneva, Arial, sans-serif" color="#151515" size="2" style="font-size:22px;line-height:29px;"><b><?=$subject;?></b></font>
									</td>
									<td align='center' bgcolor="#ffffff" width="12%"><img style="width: auto;float:right;position:absolute;margin: -22px -42px;" src="<?php echo base_url();?>images/thumb_gov_tanzania_logo.png"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;&nbsp;</td></tr>

<tr>
<td>
<font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;">Hi, <?=$employe_name;?></font>
</td>
</tr>
<tr>
<td>
<font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;"><?=lang('items_notification_low_inventory');?></font>
</td>
</tr>
<tr><td></td></tr>
</table>


<div class="report-title-sections">
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('reports_welcome_message'); ?>' />
		</td>
		<td id="title"><?php echo lang('reports_reports'); ?> - <?php echo $Notification_Data['title'] ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</div>

<br />
<table id="contents">
	<tr>
		<td id="item_table">
			<div id="table_holder" style="width: 938px;">
				<table class="tablesorter report" id="sortable_table">
						<tr>
							<?php $headers = $Notification_Data['headers'];
							foreach ($headers as $header) { ?>
							<th align="<?php echo $header['align'];?>" style='background-color: #000000;background-position: -15px -80px; border-right: 1px solid #bbbbbb;color: #ffffff;font-size: 12px;height: 30px;padding: 0 10px; text-align: left;'><?php echo $header['data']; ?></th>
							<?php } ?>
						</tr>
						<?php $data = $Notification_Data['data']; foreach ($data as $row) { ?>
						<tr>
							<?php foreach ($row as $cell) { ?>
							<td align="<?php echo $cell['align'];?>" style='background-color: #e9e9e9;border-bottom: 1px solid #bbbbbb;border-right: 1px solid #bbbbbb;color: #444444;font-size: 12px;height: 30px;padding: 0 10px;vertical-align: middle;'><?php echo $cell['data']; ?></td>
							<?php } ?>
						</tr>
						<?php } ?>
					
				</table>
			</div>	
			<?php $summary_data = $Notification_Data['summary_data'];  if(isset($summary_data) && $summary_data!='expire_items_reports'){?>
				<div id="report_summary" class="tablesorter report" style="margin-right:0px;float: right;">
				<?php foreach($summary_data as $name=>$value) { ?>
					<div class="summary_row"><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.to_currency($value); ?></div>
				<?php }?>
				</div>
			<?php }?>
		</td>
	</tr>
</table>

