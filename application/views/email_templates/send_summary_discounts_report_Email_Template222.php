<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="<?php echo base_url();?>" />
	<title><?php echo $this->config->item('company').' -- '.lang('common_powered_by').' PHP SYNTAX PLC( For any Problem Call +255 232 402465, +255 782566633)' ?></title>
	 <link type="image/x-icon" href="<?php echo base_url();?>images/favicon.ico" rel="shortcut icon">  
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/phppos.css?'.APPLICATION_VERSION;?>" media="all" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/general.css?'.APPLICATION_VERSION;?>" media="all" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url().'css/tables.css?'.APPLICATION_VERSION;?>" media="all" />	
</head>
<body>

<div id="content_area_wrapper">
<div id="content_area">	

<table  border=0>	
<tr>
	<td>
	<font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;">Hi, <?=$employe_name;?></font>
	</td>
	</tr>
	<tr>
	<td>
	<font color="#3f3f3f" size="2" face="Verdana, Geneva, Arial, sans-serif" style="font-size:14px;line-height:19px;"><?=lang('items_notification_low_inventory');?></font>
	</td>
	</tr>
<tr><td>&nbsp;&nbsp;</td></tr>
</table>


<div class="report-title-sections">
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('reports_welcome_message'); ?>' />
		</td>
		<td id="title"><?php echo lang('reports_reports'); ?> - <?php echo $Notification_Data['title'] ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
<div class="report-logo-sections"><?php echo img(array('src' => $this->Appconfig->get_logo_image())); ?></div>

<br />
<table id="contents">
	<tr>
		<td id="item_table">
			<div id="table_holder" style="width: 960px;">
				<table class="tablesorter report" id="sortable_table">
					<thead>
						<tr>
							<?php $headers = $Notification_Data['headers'];
							foreach ($headers as $header) { ?>
							<th align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php $data = $Notification_Data['data']; foreach ($data as $row) { ?>
						<tr>
							<?php foreach ($row as $cell) { ?>
							<td align="<?php echo $cell['align'];?>"><?php echo $cell['data']; ?></td>
							<?php } ?>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>	
			<?php $summary_data = $Notification_Data['summary_data'];  if(isset($summary_data) && $summary_data!='expire_items_reports'){?>
				<div id="report_summary" class="tablesorter report" style="margin-right:30px;float: right;">
				<?php foreach($summary_data as $name=>$value) { ?>
					<div class="summary_row"><?php echo "<strong>".lang('reports_'.$name). '</strong>: '.to_currency($value); ?></div>
				<?php }?>
				</div>
			<?php }?>
		</td>
	</tr>
</table>
<?php if($this->router->fetch_method()=='send_summary_discounts_report'){?>
<div class="summary_title"><?php echo lang('reports_discounts_summary');?>
<div id="summary_table">
<table id="sortable_table" class="tablesorter report">
	<tr><td align="left"><?php echo lang('common_male');?></td>					<td align="center" class="summary_bgcolor"> <?=$Notification_Data['customer_All_Male'];?> </td></tr>
	<tr><td align="left"><?php echo lang('common_female');?></td>				<td align="center"> <?=$Notification_Data['customer_All_Female'];?> </td></tr>
	<tr><td align="left"><?php echo lang('mathua_report_age_under_5');?></td>	<td align="center" class="summary_bgcolor"> <?=$Notification_Data['customer_under_5'];?> </td></tr>
	<tr><td align="left"><?php echo lang('mathua_report_age_above_60');?></td>	<td align="center"> <?=$Notification_Data['customer_above_60'];?> </td></tr>
	<tr><td align="left"><?php echo lang('mathua_report_age_others');?></td>	<td align="center" class="summary_bgcolor"> <?=$Notification_Data['customer_all_others'];?> </td></tr>
	
</table>
</div>
</div>
<?php }?>
