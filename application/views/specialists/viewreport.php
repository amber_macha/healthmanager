<style>
.mthu_diagnoses_sec{width:285px;float:left;}
.mthu_sec_left{float:left;width:330px;}
.mthu_sec_right{float:right;width:330px;}
.field_row label {width: auto;margin-left: 0px;padding: 5px;}
.field_row div.form_field input {width:auto; }
</style>  
<fieldset id="customer_basic_info">
<legend><?php echo lang("specialist_view_lab_report"); ?></legend>
<div class="field_row clearfix">	
<label class="required add-view-report-title" style="width:206px; margin-top:-10px; padding:0px;"><?php echo lang('specialist_laboratory_report_title');?></label>
	<div class="form_field">
	<?php echo form_textarea(array(
		'name'=>'laboratory_report',
		'id'=>'laboratory_report',
		'value'=>$reportdata->test_report_details,
		'readonly'=>'readonly',
		'rows'=>'10',
		'cols'=>'75')		
	);?>
	</div>
	<div class="report001"> 
	   <div class="view-report-tech"><?php echo lang('employee_name'); ?>: <?=ucwords(strtolower($reportdata->technical_first_name)).'&nbsp;'.ucwords(strtolower($reportdata->technical_last_name));?></div> 
	  <div class="view-report-div"><?php echo lang('report_posted_date'); ?>: <span class="view-report-date"><?php if(isset($reportdata->tested_date) && $reportdata->tested_date!='')echo date('d/m/Y',strtotime($reportdata->tested_date));?></span></div> 
	</div>
</div>
</fieldset>
	
	<input type="hidden" name="category_list" id="category_list" value="">
	<div class="field_row clearfix">
        <span id="error_message_box" style="display:none"></span>	
		<div class='form_field'>
		     <div class="mthu_sec_left">
			 
					<div class="form_field" style="margin-top: 4px;"> 
						<label class="required lab-search-2"><?php echo lang('list_diseases'); ?> : <span style="color:#000000">MTHUA DIAGNOSES</span></label>
						<div class="lab-search"><?php echo lang('search_diseases'); ?>: 
						<img id="searchspiner" class="search-spiner01" src="<?php echo base_url();?>images/spinner_small.gif">
						<input type="text" class="lab-search-box" id="search_lab_service" value="" name="search_lab_service" style="padding: 3px;">
						</div>	
					</div>			 
			 
					<div id="labcategory" style="float:left; margin:5px 5px;overflow-y:scroll;width: 320px; height:130px;">
					<?php if(isset($diseases_list) && $diseases_list!=''){ echo $diseases_list; }?>
					</div>
					<input class="add-diseases" type="button" onclick="saveServices();" value="<?php echo lang("common_submit");?>" name="importsubmit">
			</div>	
			
			<div class="mthu_sec_right">
					<div class="form_field" style="margin-top: 4px;"> 
						<label class="required lab-search-2"><?php echo lang('list_diseases'); ?> : <span style="color:#000000">DIABETES CARE</span></label>
						<div class="lab-search"><?php echo lang('search_diabetes'); ?>: 
						<img id="searchspiner2" class="search-spiner01" src="<?php echo base_url();?>images/spinner_small.gif">
						<input type="text" class="lab-search-box" id="search_lab_diabetes" value="" name="search_lab_diabetes" style="padding: 3px;">
						</div>	
					</div>
			
					<div id="diabetes_list" style="float:left; margin:5px 5px;overflow-y:scroll;width: 320px; height:130px;">
					<?php if(isset($diabetes_list) && $diabetes_list!=''){ echo $diabetes_list; }?>
					</div>
					<input class="add-diseases" type="button" name="importsubmit" value="Submit" onclick="saveDaibetes();">
			</div>	
			
		</div>
	</div>

<script type='text/javascript'>
//**************Add New diagnoses code*********************//
function saveServices()
{ 
	var checkedval='';
	 $("#labcategory input:checked").each(function() { if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('diseases_services_empty');?>');return false; }  
	if (confirm('<?php echo lang('diseases_services_add');?>')) 
	{			
		var searchURL ='<?php echo base_url().'specialist/saveDiseases/'.$person_id.'/mtuha_daignoses/'.$item_id.'/';?>' +checkedval;
		$('#searchspiner').show();
		$.get(searchURL, function(data) { 
			  // alert(data);
			  $('#error_message_box').show();
			  $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').hide(); } , 5000); search(true); 
		   });
	}	
	else{return false;}
}


//**************Add New daibeted code*********************//
function saveDaibetes()
{ 
	var checkedval='';
	 $("#diabetes_list input:checked").each(function() { if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('diabetes_services_empty');?>');return false; }  
	if (confirm('<?php echo lang('diseases_services_add');?>'))  
	{		
		var searchURL ='<?php echo base_url().'specialist/saveDiseases/'.$person_id.'/daibetes/'.$item_id.'/';?>' +checkedval;
		$('#searchspiner2').show();
		$.get(searchURL, function(data) { 
			  $('#error_message_box').show();
			  $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').hide(); } , 5000); search_diabetes(true); 
		   });
	}	
	else{return false;}
}


//**************START CODE FOR LAB DIAGNOSES SEARCH*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 //if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});


function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'specialist/searchDiseasList/'.$person_id ?>';
	$('#searchspiner').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'searchtype=mtuha_daignoses&service='+existingString+'&checkedval='+checkedval+'&item_id='+<?=$item_id;?>,
            success: function(data) 
				{ 
					$('#labcategory').html(data);  $('#searchspiner').hide(); 
				}
        }); 
}

//**************END CODE FOR LAB DIAGNOSES SEARCH***********************//

//**************START CODE FOR LAB DIABETES SEARCH*********************//
$('#search_lab_diabetes').keyup(function(e) { 
 var existingString = $("#search_lab_diabetes").val();
 //if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)  search_diabetes(existingString);
    else   $(this).data('timer', setTimeout(search_diabetes(existingString), 500));
});


function search_diabetes(existingString) {   
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'specialist/searchDiseasList/'.$person_id ?>';
	$('#searchspiner2').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'searchtype=daibetes&service='+existingString+'&checkedval='+checkedval+'&item_id='+<?=$item_id;?>,
            success: function(data) 
				{ 
					$('#diabetes_list').html(data);  $('#searchspiner2').hide(); 
				}
        }); 
}

//**************END CODE FOR LAB DIAGNOSES SEARCH*********************//

</script>