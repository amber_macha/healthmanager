<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("specialist_header"); ?></legend>
<div class="form_field"> <label class="required lab-search-2"><?php echo lang('specialist_category'); ?> :</label>
<label class="add_new_lab_search" for="first_name" style="margin-left: 110px;"><?php echo lang('search_lab_services'); ?>: 
<img id="searchspiner" class="search-spiner" src="<?php echo base_url();?>images/spinner_small.gif">
<input type="text" class="lab-search-box" id="search_lab_service" value="" name="search_lab_service">
</label>	
</div>
<?php echo form_open('specialist/save/'.$person_id,array('id'=>'history_form','onsubmit'=>'return validateCustomer()')); ?>
	<input type="hidden" name="category_list" id="category_list" value="">
		<div class="field_row clearfix" style="margin:0px;">	
			<div class='form_field'>		
				<div id="labcategory" class="labcategory_div">
					<?php echo $lab_category_list; ?>
				</div>
			</div>
		</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
<input id="active_deactive" class="active_deactive" type="button" onclick="ActiveDeActivate();" value="<?php echo $this->lang->line("active_deactive_lab_servics");?>"><fieldset id="customer_basic_info" style="border: 4px solid #DDDDDD;width: 97%;"><legend><?php echo lang("lab_investigation_admin_services"); ?></legend>
<input id="import_services" class="add-button" type="button" value="<?php echo lang("import_lab_services");?>" onclick="GenerateAction('import')">
<input id="export_services" class="add-button" type="button" value="<?php echo lang("export_lab_services");?>" onclick="GenerateAction('export')">
<input id="add_services" class="add-button" type="button" onclick="showThem('viewthem');" value="<?php echo lang("common_add");?>">
<input id="delete_services" class="delete-button" type="button" onclick="DeleteServices();" value="<?php echo $this->lang->line("common_delete");?>">
</fieldset>
<!---start div to add new lab services popup-->
<div id="login_pop" style="display:none;">
<div class="form_field add-lab-pop4" > 
	<label class="lab_search_poup" for="first_name" style="color:#000000;"><?php echo lang('lab_investigation_category'); ?> : 
	<input type="text" class="add-lab-textbox" id="service" value="" name="service" onClick="clicktohide(this);"></label>	
</div>

<div class="form_field add-lab-pop3"> 
	<label class="lab-search" style="color:#000000;"><?php echo lang('lab_investigation_commision'); ?> &nbsp;:&nbsp; <input type="text" class="add-lab-textbox" id="service_commision" value="" name="service_commision" onClick="clicktohide(this);"></label>	
</div>

<div class="move001">	
<input class="add-button" type="button" onclick="saveServices();" value="<?php echo lang("common_submit");?>" name="importsubmit">
<input class="add-button" type="button" onclick="showThem('hidethem');" value="<?php echo $this->lang->line("close_div");?>">
</div>
</div>
<?php echo form_close();?>
</fieldset>
<!---start div to import lab services popup-->
<form id="file_import_form" action="<?php echo base_url().'specialist/do_excel_import/'.$person_id;?>" method="POST" enctype="multipart/form-data"><div id="labimport_pop" style="display:none;">
<div class="form_field add-lab-pop2" ><?php echo lang("items_mass_import_from_excel");?></div>
	<div class="form_field add-lab-pop1" > 
		<label class="lab-search" style="color:#000000; margin-left:75px;"><?php echo lang('common_file_path'); ?>: 
		<span id="file_path" class="file-upload-button"><?php echo lang("choose_file_to_upload");?></span> <span id="status" class="validationtxt"></span>
		</label>
	</div>
<div class="move002"><input class="add-button" type="button" onclick="showThem('importviewthem');" value="<?php echo $this->lang->line("close_div");?>"></div></div>
<?php echo form_close();?>
<script type='text/javascript'>
//**************import Services code*********************//
$(function(){
		var btnUpload=$('#file_path');
		var status=$('#status');	
		new AjaxUpload(btnUpload, {
			action: '<?php echo base_url(); ?>specialist/do_excel_import',
			name: 'file_path',
			onSubmit: function(file, ext){
				 if (! (ext && /^(csv)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only csv files are allowed');
					return false;
				}
				setTimeout( function(){ $('#status').html(''); },1000);
				 $('#searchspiner').show();	
			},
			onComplete: function(file, response){ showThem('importviewthem'); $('#error_message_box').html(response); setTimeout( function(){ $('#error_message_box').html(''); },5000); search(true); }		});
	});	
//**************Add New Services code*********************//
function GenerateAction(type)
{
  if(type=='export'){  window.location.href= '<?php echo base_url().'specialist/excel_export';?> '; }
  if(type=='import'){  $('#labimport_pop').show(); }}
//**************Active Deactivate Lab Services code*********************//
function ActiveDeActivate()
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });	if(checkedval==''){ alert('<?php echo lang('specialist_services_empty_change_status');?>');return false; }  	if (confirm('<?php echo lang('specialist_services_change_status');?>')) 	{
	var searchURL ='<?php echo base_url().'specialist/activeDeActivate/'.$person_id.'/';?>' +checkedval;	$('#searchspiner').show();	$.get(searchURL, function(data) { $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').html(''); } , 5000); search(true); });	}		else{return false;}}
//**************Add New Services code*********************////**************Add New Services code*********************//function DeleteServices(){ 
	var checkedval='';	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });	if(checkedval==''){ alert('<?php echo lang('specialist_services_empty_delete');?>');return false; }  	if (confirm('<?php echo lang('specialist_services_confirm_delete');?>')) 	{				var searchURL ='<?php echo base_url().'specialist/deletelabservices/'.$person_id.'/';?>' +checkedval;	$('#searchspiner').show();	$.get(searchURL, function(data) { $('#error_message_box').html(data); setTimeout( function(){ $('#error_message_box').html(''); } , 5000); search(true); });	}		else{return false;}}//**************Add New Services code*********************////**************Add New Services code*********************//function saveServices()
{ 
  var service = $("#service").val();
  var commision = $("#service_commision").val();
  if(service==''){ document.getElementById('service').style.background = "#FFDAB9"; return false; }
  if(commision==''){ document.getElementById('service_commision').style.background = "#FFDAB9"; return false; }
  var searchURL ='<?php echo base_url().'specialist/addservices/'.$person_id.'/';?>' +service;
  showThem('hidethem'); $('#searchspiner').show();	
	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'service='+service + '&commision='+commision,
            success: function(data) 
			{ $("#service_commision").val('');
			 $('#error_message_box').html(data); 
			  $('#error_message_box').show(); setTimeout( function(){ $('#error_message_box').html(''); } , 4000); search(service);			}
        }); 
}
//**************Add New Services code*********************//
function showThem(type)
{ 
if(type=='viewthem'){$('#login_pop').show(); }
if(type=='hidethem'){$("#service").val(''); document.getElementById('service').style.background = '';$('#login_pop').hide(); }
if(type=='importviewthem'){$('#labimport_pop').hide(); }
}
//**************lab searching code*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});
function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  	var searchURL ='<?php echo base_url();?>specialist/getservices/';	$('#searchspiner').show();	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval,
            success: function(data) { $('#labcategory').html(data);  $('#searchspiner').hide(); }
        }); }
//**************lab searching code*********************//
function validateCustomer()
{
	var n = $( "input:checked" ).length;
	if(n==0){ return false; }
	else { 
		$(':checkbox:checked').each(function(i){
		if($(this).val()==0){ alert('<?php echo lang('specialist_services_empty_lab_services_to_added');?>'); $('#lab_'+id).attr('checked', false); return false; }				}); 
	document.getElementById('category_list').value = n;  
	} 
 }  
//validation and submit handling
$(document).ready(function()
{	var submitting = false;
	$('#history_form').validate({ 
		submitHandler:function(form)
		{			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			category_list: "required"
   		},
		messages: 
		{     		
     		category_list: <?php echo json_encode(lang('specialist_services_empty_lab_services_to_added')); ?>
		}	});});
</script>