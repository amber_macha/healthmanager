<style>fieldset input[type="submit"] { background: none repeat scroll 0 0 #3BB63D;  border: 1px solid #3BB63D;  color: #FFFFFF; font-size: 14px; 
										font-weight: bold; line-height: 30px;  margin: 10px 10px 10px 192px;  padding: 7px;float:left;cursor:pointer;}
										</style>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("ipd_in_view_form_name"); ?></legend>
<?php
$per_day_charges='';
if(isset($person_info->payment_per_day_charges)&& $person_info->payment_per_day_charges!='') $per_day_charges = $person_info->payment_per_day_charges;
$patient_block_name='';
if(isset($person_info->patient_block_name)&& $person_info->patient_block_name!='') $patient_block_name = $person_info->patient_block_name;
$patient_room_name='';
if(isset($person_info->patient_room_name)&& $person_info->patient_room_name!='') $patient_room_name = $person_info->patient_room_name;
$ipd_id_number='';
if(isset($person_info->id)&& $person_info->id!='') $ipd_id_number = $person_info->id;

?>
<div class="field_row clearfix">	
<label class="ipd-data-field"><?php echo lang("ipd_in_assign_balance"); ?> </label>
	<div class='form_field'>
	    <input  type="text" id="daily_assign_payment" value="<?=$per_day_charges;?>" name="daily_assign_payment" onClick="clicktohide(this);">
		<img id="spiner001" class="search_ipd_mange01" src="<?php echo base_url();?>images/spinner_small.gif">
		<input type="button" onclick="GenerateAction('apply_Payment')" value="<?php echo lang("ipd_in_apply_services_button"); ?>" class="ipd-add-button" style=" border: 1px solid #1DA2E0;">
		<input id="edit_ipdservices" class="ipd-delete-button" type="button" value="<?php echo lang("ipd_in_edit_services_button"); ?>" onclick="GenerateAction('edit_IPDService');">
	</div>
</div>

<div class="field_row clearfix">	
<label class="ipd-data-field"><?php echo lang("ipd_in_block_name"); ?> </label>
	<div class='form_field'>
	<img id="search_ipd_data_spiner" src="<?php echo base_url();?>images/spinner_small.gif">
	<input  type="text" id="block_name" name="block_name" value="<?=$patient_block_name;?>" onClick="clicktohide(this);"></div>
	<div id="showlist" class="show-blocklist"></div>
	<br>
	<label class="ipd-data-field"><?php echo lang("ipd_in_room_no"); ?> </label>
	<div class='form_field'>
			<input  type="text" id="room_name" name="room_name" value="<?=$patient_room_name;?>" onClick="clicktohide(this);">
			<img id="spiner002" class="search_ipd_mange01" src="<?php echo base_url();?>images/spinner_small.gif">
			<input type="button" onclick="GenerateAction('apply_Room')" value="<?php echo lang("ipd_in_apply_services_button"); ?>" class="ipd-add-button" style=" border: 1px solid #1DA2E0;">
			<input id="edit_ipdservices" class="ipd-delete-button" type="button" value="<?php echo lang("ipd_in_edit_services_button"); ?>" onclick="GenerateAction('edit_IPDService');">
	       <div id="showRoomlist" class="show-blocklist"></div>
	</div>	
</div>

<div class="field_row clearfix">	
<label class="ipd-data-field"><?php echo lang("ipd_in_add_payment"); ?> </label>
	<div class='form_field'><input type="text" id="payment_amount" name="payment_amount" value="" onClick="clicktohide(this);">
	</div>
	<br>
	<label class="ipd-data-field"><?php echo lang("ipd_in_receiept_no"); ?> </label>
	<div class='form_field'><input type="text" id="slip_receiept_number" name="slip_receiept_number" value="" onClick="clicktohide(this);">
	<img id="spiner003" class="search_ipd_mange01" src="<?php echo base_url();?>images/spinner_small.gif">	
	<input  type="hidden" id="ipd_id_number" name="ipd_id_number" value="<?=$ipd_id_number;?>" onClick="clicktohide(this);">	
	<input type="button" onclick="GenerateAction('apply_addPayment')" value="<?php echo lang("ipd_in_add_payment"); ?>" class="ipd-add-button" style=" border: 1px solid #1DA2E0;">
	</div>	
</div>
<?php echo form_open('ipd_in_patient/discharges_patients/'.$person_info->person_id.'/'.$ipd_patient_id,array('id'=>'customer_form'));?>
<input  type="hidden" id="customer_ipd_id" name="customer_ipd_id" value="<?=$ipd_id_number;?>" onClick="clicktohide(this);">
<?php echo form_submit(array('name'=>'save','value'=>lang('ipd_in_patent_submit'),	'class'=>'float_right'));?>
<?php echo form_close();?>

<?php echo form_open('ipd_in_patient/closeform/'.$person_info->person_id,array('id'=>'customer_form3'));?>
<input type="submit" value="<?php echo lang("ipd_in_patient_close_form"); ?>" name="save" style=" background: none repeat scroll 0 0 #FF0000; border: 1px solid #FF0000;  color: #FFFFFF; float: left; font-size: 14px; font-weight: bold; line-height: 30px; margin: 10px 10px 10px 4px; padding: 7px;cursor:pointer;">
</fieldset>
<?php echo form_close();?>

<script type='text/javascript'>
function clicktohide(obj)	{ document.getElementById(obj.id).style.background = ""; } 

	//**********search auto suggest block name************//
	function getCustomerdata(pid)	{  $('#block_name').val(pid); $('#showlist').html('');  }
	
	$('#block_name').keyup(function(e) { 
			var existingString = $("#block_name").val();
			clearTimeout($.data(this, 'timer'));
			if (e.keyCode == 13)
			search_block(existingString);
			else   $(this).data('timer', setTimeout(search_block(existingString), 500));
	});

	function search_block(existingString) { 
		var searchURL ='<?php echo base_url();?>ipd_in_patient/search_block_list/';
		$('#search_ipd_data_spiner').show();
		$.ajax({
				url: searchURL,
				type: 'post',
				data: 'search_keyword='+existingString,
				success: function(data) 
				{   
					$('#showlist').html(data);  
					$('#search_ipd_data_spiner').hide(); 
				}
			}); 
	}

	//**********search auto suggest Room No ************//
	function getCustomerRoom(pid)	{  $('#room_name').val(pid); $('#showRoomlist').html('');  }
	
	$('#room_name').keyup(function(e) { 
			var existingString = $("#room_name").val();
			clearTimeout($.data(this, 'timer'));
			if (e.keyCode == 13)
			search_room(existingString);
			else   $(this).data('timer', setTimeout(search_room(existingString), 500));
	});

	function search_room(existingString) { 
		var searchURL ='<?php echo base_url();?>ipd_in_patient/search_room_name_list/';
		$('#spiner002').show();
		$.ajax({
				url: searchURL,
				type: 'post',
				data: 'search_keyword='+existingString,
				success: function(data) 
				{   
					$('#showRoomlist').html(data);  
					$('#spiner002').hide(); 
				}
			}); 
	}



function GenerateAction(type)
{
  //*****start assign paymet functionality*********//
  if(type=='apply_Payment')
  {  
	var feecharge = $("#daily_assign_payment").val();
	if(feecharge==''){ document.getElementById('daily_assign_payment').style.background = "#FFFF00"; return false; }
	if(feecharge!='')
		{    var m = feecharge.match(/^\d{0,5}(?:\.\d{0,2}){0,1}$/);
			 if (!m) { document.getElementById('daily_assign_payment').style.background = "#FFFF00"; return false; }
		}	
	var searchURL ='<?php echo base_url().'ipd_in_patient/add_daily_charges/'.$person_id.'/'.$ipd_patient_id.'/';?>';	 
	var result = confirm("<?php echo lang('ipd_in_patient_confirm_assign_payment');?>");	  
	if (result==false) {  return false;}
		else	
			{  $('#spiner001').show();
   			   $.ajax({
							url: searchURL,
							type: 'post',
							data: 'feecharge='+feecharge,
							dataType:'json',
							success: function(data) 
							{  $('#error_message_box').show();
							    $('#spiner001').hide();
								$('#error_message_box').html(data.message);
								if(data.success==false){ document.getElementById('daily_assign_payment').style.background = "#FFFF00"; }
								realod_page_ipd();
								 setTimeout( function(){ $('#error_message_box').html(''); } , 4000);								
							}
				     }); 
            }	
   }
   
    //*****start assign room functionality functionality*********//
   if(type=='apply_Room')
   {  
		var block_name = $("#block_name").val();
		var room_name = $("#room_name").val();
		if(block_name==''){ document.getElementById('block_name').style.background = "#FFFF00"; return false; }	 
		if(room_name==''){ document.getElementById('room_name').style.background = "#FFFF00"; return false; }	 
		var searchURL ='<?php echo base_url().'ipd_in_patient/add_room_patients/'.$person_id.'/'.$ipd_patient_id.'/';?>';	 
		var result = confirm("<?php echo lang('ipd_in_patient_confirm_assign_room');?>");	  
		if (result==false) {  return false;}
			else	
				{  $('#spiner002').show();
				   $.ajax({
							url: searchURL,
							type: 'post',
							data: 'block_name='+block_name+'&room_name='+room_name,
							dataType:'json',
							success: function(data) 
							{  $('#error_message_box').show();
							    $('#spiner002').hide();
								$('#error_message_box').html(data.message);
								if(data.success==false)
								{ document.getElementById('block_name').style.background = "#FFFF00"; 
									document.getElementById('room_name').style.background = "#FFFF00";  
								}
								 realod_page_ipd();
								 setTimeout( function(){ $('#error_message_box').html(''); } , 4000);								
							}
				     }); 
            }	
     }
     
	   //*****start add received payment/slip functionality*********//
  if(type=='apply_addPayment')
  {  
	var payment_amount  = $("#payment_amount").val();
	var slip_receiept	= $("#slip_receiept_number").val();
	var ipd_id_number	= $("#ipd_id_number").val();
	if(payment_amount==''){ document.getElementById('payment_amount').style.background = "#FFFF00"; return false; }
	if(payment_amount!='')
	{    var m = payment_amount.match(/^\d{0,5}(?:\.\d{0,2}){0,1}$/);
		 if (!m) { document.getElementById('payment_amount').style.background = "#FFFF00"; return false; }
	}	
	if(slip_receiept==''){ document.getElementById('slip_receiept_number').style.background = "#FFFF00"; return false; }
	var searchURL ='<?php echo base_url().'ipd_in_patient/received_IPDPayment/'.$person_id.'/';?>';	 
	var result = confirm("<?php echo lang('ipd_in_patient_confirm_received_payment');?>");	  
	if (result==false) {  return false;}
		else	
			{  $('#spiner003').show();
   			   $.ajax({
							url: searchURL,
							type: 'post',
							data: 'payment_amount='+payment_amount+'&slip_receiept='+slip_receiept+'&ipd_id_number='+ipd_id_number,
							dataType:'json',
							success: function(data) 
							{ 
							    $('#error_message_box').show();
							    $('#spiner003').hide();
								$('#error_message_box').html(data.message);
								if(data.success==false)
								{  document.getElementById('payment_amount').style.background = "#FFFF00"; 
								   document.getElementById('slip_receiept_number').style.background = "#FFFF00";
								}	
								realod_page_ipd();
								setTimeout( function(){ $('#error_message_box').html(''); } , 4000);								
							}
				     }); 
            }	
   }
   
}



$(document).ready(function()
{
	var submitting = false;
	$('#customer_form').validate({ 
		submitHandler:function(form)
		{  
			var result = confirm("<?php echo lang('discharge_confirmation');?>");
			if (result==false) {  return false;}
			else		
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{			
			age_year: "required",
			
			
   		},
		messages: 
		{     	
			patient_type: <?php echo json_encode(lang('common_patient_type_required')); ?>,		
		}
	});
});

$(document).ready(function()
{
	var submitting = false;
	$('#customer_form3').validate({ 
		submitHandler:function(form)
		{			
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{			
   		},
		messages: 
		{     	
		}
	});
});
</script>