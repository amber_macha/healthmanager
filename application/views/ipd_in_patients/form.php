<style> fieldset input[type="submit"] { background: none repeat scroll 0 0 #2005D0;border: 1px solid #2005D0;  
									color: #FFFFFF; font-size: 14px;font-weight: bold; line-height: 30px;  margin: 10px;  padding: 7px;float:left}
.ipd_search_list{ margin-left: 185px; margin-top: 2px; z-index: 233333;width: auto;cursor:pointer}
</style>										
<?php echo form_open('ipd_in_patient/save/'.$person_info->person_id,array('id'=>'search_patient_form','name'=>'search_patient_form'));?>	
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
		<div class="field_row clearfix">	
		<label class="ipd-data-field"><?php echo lang("ipd_in_patient_new_select"); ?> </label>
		<img id="search_patinet_spiner" src="<?php echo base_url();?>images/spinner_small.gif">
		<div class='form_field'><input id="search_keyword" type="text" value="" name="search_keyword" style="width:230px;" onClick="clicktohide(this);">
		<div id="showlist"></div>
		</div>
		</div>

<?php echo form_submit(array('name'=>'save','value'=>lang('common_submit'),	'class'=>'float_right'));?>
<div class="ipd-fom-notice-box"><span class="ipd-fom-notice-message"> <?php echo lang("ipd_in_patient_select_message"); ?></span></div>
</fieldset>
<?php echo form_close();?>	

<script type='text/javascript'>
function clicktohide(obj)	{ document.getElementById(obj.id).style.background = ""; } 
function getCustomerdata(pid)
{  
   $('#search_keyword').val(pid); $('#showlist').html('');  
}

$('#search_keyword').keyup(function(e) { 
		var existingString = $("#search_keyword").val();
		if(existingString==''){ return false; }
		clearTimeout($.data(this, 'timer'));
		if (e.keyCode == 13)
		search_key(existingString);
		else   $(this).data('timer', setTimeout(search_key(existingString), 500));
});

function search_key(existingString) { 
	var searchURL ='<?php echo base_url();?>ipd_in_patient/search_patients/';
	$('#search_patinet_spiner').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'search_keyword='+existingString,
            success: function(data) 
			{   
			    $('#showlist').html(data);  
			    $('#search_patinet_spiner').hide(); 
			}
        }); 
}


$(document).ready(function()
{
	var submitting = false;
	$('#search_patient_form').validate({ 
		submitHandler:function(form)
		{  
			if (document.getElementById('search_keyword').value == "") { document.getElementById('search_keyword').style.background = "#FFFF00"; return false; } 	
			var result = confirm("<?php echo lang('ipd_add_patients_confirmation');?>");
			if (result==false) {  return false;}
			else		
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{  //alert(response); //return false;
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{			
   		},
		messages: 
		{   	
				
		}
	});
});
</script>