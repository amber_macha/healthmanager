<?php $this->load->view('partial/header.php'); ?>
<style type="text/css">
.error { float: none !important; }
#register_container{text-align: center;width: 590px;}
.company_logo_image { width: 150px;}
fieldset input[type="submit"] {margin-left:88px;}
.closing_amount{float: right; margin-right: 106px; width: 100%;margin-bottom:10px;}
</style>
<div id="register_container" class="sales">
<div class="company_logo_sec"><img src="<?php echo base_url();?>images/gov_tanzania_logo.jpg" class="company_logo_image"></div>
<?php echo form_open('sales/closeregister' . $continue, array('id'=>'closing_amount_form'));?>
<fieldset id="item_basic_info">
<legend><?php echo lang("sales_closing_amount_desc"); ?></legend>
<div class="closing_amount"><?php echo sprintf(lang('sales_closing_amount_approx'), $closeout); ?></div>
<div class="field_row clearfix">
<?php echo form_label(lang('sales_closing_amount').':', 'closing_amount',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'closing_amount',
		'id'=>'closing_amount',
		'value'=>'')
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
</div>
<?php $this->load->view('partial/footer.php'); ?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;

	$('#closing_amount_form').validate({
		rules:
		{
			closing_amount: {
				required: true,
				number: true
			}
   		},
		messages:
		{
			closing_amount: {
				required: <?php echo json_encode(lang('sales_amount_required')); ?>,
				number: <?php echo json_encode(lang('sales_amount_number')); ?>
			}
		}
	});
});
</script>