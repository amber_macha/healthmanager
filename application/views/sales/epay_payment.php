<style>
#epay_payment_pop {
    background: #ffffff none repeat scroll 0 0;
    border: 4px solid #008000;
    border-radius: 3px;
    color: #cccccc;
    display: none;;
    height: 360px;
    margin-right: 440px;
    padding: 10px;
    position: absolute !important;
    right: 0 !important;
    text-align: left;
    top: 130px !important;
    width: 600px;
    z-index: 1000000;
}
.epay_border_top{ border-bottom: 2px solid #cccccc; float: left; height: 25px; margin-top: 0px; width: 100%;}
.epay_border_title{text-align:center;color:#FF0000;font-weight: bold; font-size: 16px;}
.epay_content_sec{width:100%; float:left;color: #000000; padding: 5px; font-size: 14px;}
.epay_content_sec_left{width:60%; float:left;}
.epay_content_sec_left2{width:55%; float:left;}
.epay_content_sec_left_border{border-bottom: 1px solid #cccccc;float: left;padding: 2px;width: 100%;}
.epay_content_sec_right{float:right;margin-right: 5px; width: 38%; text-align: center;}
.epay_content_sec_right2{float:right;margin-right: 2px; width: 42%; text-align: center;}

.epay_profile_photo_sec{ height: 130px;width: 130px;margin:10px 0px 15px 45px; border-radius: 15px;border: 2px solid #000000;}	
.epay_profile_photo{ height: 120px;margin-top: 5px;}
.epayreply_sec{margin-right: 5px; border: 1px solid #008000;height:100px; text-align: center;}
.epay_content{float:left;width: 100%; padding: 4px;}
.epay_content_span{font-weight:bold;}
.epay_border_right { border-bottom: 1px solid #000000; float: left; height: 5px; width: 100%;}
.lab-search-button { padding: 5px; font-size: 13px;}
.lab-delete-button { padding: 5px; font-size: 13px;}
.move007{float:left;}
.card_sec{margin-top:6px;}
#health_card_no{padding:5px; width: 175px;border: 1px solid #cccccc;}
.sales_id{color:red;font-weight:bold;}
#sales_epay_reply{ margin-top: 20px;}
</style>

<div id="epay_payment_pop">
	<div class="epay_border_top"><div class="epay_border_title"><?=lang('sales_epay_form_title');?></div></div>	
	<div id="updatemessage" class="updatemessage"></div>
	<div class="epay_content_sec">
		<div class="epay_content_sec_left"> 
		   <?php if(isset($customer_id) && $customer_id!=''){ $sale_customer_id = $customer_id;}else{ $sale_customer_id=-1; }
			$person_info  = $this->Customer->get_info($sale_customer_id); //print_r($person_info);?>
			<div class="epay_content"><span class="epay_content_span"><?=lang('config_facility_id');?> :</span> <?=$this->config->item('hospital_facility_id');?></div>
			<div class="epay_content"><span class="epay_content_span"><?=lang('employees_id');?> :</span> <?=$this->Employee->get_logged_in_employee_info()->employee_account_code;?></div>
			<div class="epay_content"><span class="epay_content_span"><?=lang('history_employee_name');?> :</span> <?php $Employee_Data = $this->Employee->get_info($this->Employee->get_logged_in_employee_info()->person_id);echo ucwords(strtolower($Employee_Data->first_name)).' '. ucwords(strtolower($Employee_Data->last_name)) ;?></div>
			<div class="epay_content"><span class="epay_content_span"><?=lang('config_computer_ip');?> :</span> <?=$_SERVER['REMOTE_ADDR'];?></div>			
			<div class="epay_content_sec_left_border"></div>			
			<div class="epay_content"><span class="epay_content_span"><?=lang('patient_account');?>:</span> <?=$person_info->account_number;?></div>
			<div class="epay_content"><span class="epay_content_span"><?=lang('customers_customer_id');?>:</span> <?=$person_info->person_id;?>  </div>
			<div class="epay_content"><span class="epay_content_span"><?=lang('common_patient_name');?>:</span> <?=ucwords(strtolower($person_info->first_name)).' '. ucwords(strtolower($person_info->last_name))?> </div>
			<div class="epay_content_sec_left_border"></div>
		</div>
		
		<div class="epay_content_sec_right">				
				<?php if(isset($person_info->patient_profile_image) && $person_info->patient_profile_image!='' && $person_info->patient_profile_image!='0'){?>
				<div class="epay_profile_photo_sec"><img class="epay_profile_photo" src="<?=base_url().'images/patient_images/thumb_'.$person_info->patient_profile_image;?>"></div>
				<?php }?>				
		</div>
	</div>	
		
	<div class="epay_content_sec">
		<div class="epay_content_sec_left2">		
			<div class="epay_content"><span class="epay_content_span"><?=lang('common_total_amount');?>:</span> <?php echo to_currency($total); ?></div>
			<div class="epay_content card_sec"><span class="epay_content_span"><?=lang('common_enter_card_no');?></span> <input type="text" name="health_card_no" id="health_card_no" value="<?=$person_info->health_card_no;?>" readonly="readonly" onclick="clicktohide(this);"></div>
		</div>
		
		<div class="epay_content_sec_right2">
				<div class="epayreply_sec"><?=lang('sales_epay_reply');?>
					<div class="epay_border_right"></div>
					<div id="sales_epay_reply">
					<?php if(isset($_SESSION['EPAY_SALES_TRANSACTION_ID']) && $_SESSION['EPAY_SALES_TRANSACTION_ID']!='') echo '<b>'.lang('sales_trans_id').'</b> : <span class="sales_id">'.$_SESSION['EPAY_SALES_TRANSACTION_ID']. ' </span>';?>
					</div>
				</div>
		</div>
	</div>		
	
	<div class="move007"> 
		<?php if(!isset($_SESSION['EPAY_SALES_TRANSACTION_ID']) || $_SESSION['EPAY_SALES_TRANSACTION_ID']==''){?><input type="button" name="importsubmit" value="<?=lang('common_submit');?>" onclick="GetPayment();" class="lab-search-button"><?php }?>
		<img id="spinner003" src="<?php echo base_url();?>images/loading.gif">
		<input type="button" value="<?=lang('common_close');?>" onclick="showThem('hidethem');" class="lab-delete-button">
	</div>
</div>

<script>
function GetPayment()
{	var health_card_no = $('#health_card_no').val();
	if(health_card_no=='')	{ $("#health_card_no").css("background", "#f2dede");$("#health_card_no").css("border","1px solid #FF0000");	return false; }	
    if(health_card_no.length!=19)	
	{ $("#health_card_no").css("background", "#f2dede");$("#health_card_no").css("border","1px solid #FF0000");	
	  alert("<?php echo lang('common_health_card_no_minlength');?>");
	  return false; 
	}		
	$('#payment_type').attr("disabled","disabled");	
	$('#item').attr("disabled","disabled");
	$('#spinner003').show(); 
	$.ajax({
			url: '<?php echo base_url()."sales/getEPayments/".$customer_id;?>',
			data:'Total_Amount='+<?=$total;?> + '&health_card_no=' + health_card_no,
			type:'POST',
			dataType:'json',
			success: function(data)
			{   $('#spinner003').hide(); //alert (data.success);  alert (data.message);
				if(data.success)
				{
					$('#sales_epay_reply').html(data.message);
					$('#payment_type').attr("disabled","disabled");	
					$('#item').attr("disabled","disabled");
					$('.lab-search-button').hide();
				}
				else { 
						$('#payment_type').removeAttr('disabled');
						$('#item').removeAttr('disabled');
						alert(data.message); 
					}
			}
		});	
}
</script>