<div class="field_row clearfix">	
<?php echo form_label(lang('common_first_name').':', 'first_name',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'first_name',
		'id'=>'first_name',
		'value'=>$person_info->first_name)
	);?>
	</div>
</div><div class="field_row clearfix"><?php echo form_label(lang('common_last_name').':', 'last_name',array('class'=>'required')); ?>	<div class='form_field'>	<?php echo form_input(array(		'name'=>'last_name',		'id'=>'last_name',		'value'=>$person_info->last_name)	);?>	</div></div><?php if($controller_name=='employees'){?><div class="field_row clearfix"><?php echo form_label(lang('employees_employee_pf_no').':', 'employee_pf_no'); ?>	<div class='form_field'>	<?php echo form_input_readonly(array('name'=>'employee_pf_no','id'=>'employee_pf_no','value'=>$person_info->employee_account_code));  ?>		</div></div><input type="hidden" name="sub_group_id" id="sub_group_id" value="<?=$person_info->employee_sub_group_id;?>"><div class="field_row clearfix"><?php echo form_label(lang('common_group_name').':', 'group_name',array('class'=>'required')); ?>	<div class='form_field'>		<select class="valid" id="group_name" name="group_name" style="width: 193px;" onchange="GetSubCategory(this.value);">			<option value=""><?php echo lang('common_select_type');?></option> 			<?php if(isset($GroupsData) && $GroupsData!='' && $GroupsData->num_rows()>0){			foreach($GroupsData->result() as $rowdata){?>			<option value="<?=$rowdata->id;?>" <?php if($rowdata->id==$person_info->employee_group_id)echo 'selected="selected"';?>><?=ucfirst($rowdata->group_name);?></option>		 <?php }}?>		</select>				<img id="search_group" class="searchgroup" src="<?php echo base_url();?>images/spinner_small.gif">	</div></div><div id="sub_group_sec"><?php echo $SubGroupsData;?></div><?php }?>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_email').':', 'email'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'email',
		'id'=>'email',
		'value'=>$person_info->email)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_phone_number').':', 'phone_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'phone_number',
		'id'=>'phone_number',
		'value'=>$person_info->phone_number));?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_address_1').':', 'address_1'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'address_1',
		'id'=>'address_1',
		'value'=>$person_info->address_1));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_address_2').':', 'address_2'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'address_2',
		'id'=>'address_2',
		'value'=>$person_info->address_2));?>
	</div>
</div><?php if($controller_name=='employees'){?><div class="field_row clearfix">	<?php echo form_label(lang('common_qualifications').':', 'qualifications'); ?>	<div class='form_field'>	<?php echo form_input(array(		'name'=>'qualifications',		'id'=>'qualifications',		'value'=>$person_info->qualifications));?>	</div></div><?php }?>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_city').':', 'city'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'city',
		'id'=>'city',
		'value'=>$person_info->city));?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('common_state').':', 'state'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'state',
		'id'=>'state',
		'value'=>$person_info->state));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('common_zip').':', 'zip'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'zip',
		'id'=>'zip',		'value'=>$person_info->zip));?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_country').':', 'country'); 	  if(isset($person_info->country) && $person_info->country!='') $country = $person_info->country; else $country = $this->config->item('default_country');  ?>
	<div class='form_field'><?php echo form_dropdown('country',$this->Customer->GetCountry(),$country,'style="width:193px;"');?></div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('common_comments').':', 'comments'); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'comments',
		'id'=>'comments',
		'value'=>$person_info->comments,
		'rows'=>'5',
		'cols'=>'17')		
	);?>
	</div>
</div>