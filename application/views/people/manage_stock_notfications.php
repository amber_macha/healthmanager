<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function() 
{ 
	var table_columns = ['','last_name','first_name','email','phone_number'];
	enable_sorting("<?php echo site_url("$controller_name/stock_notfications_sorting"); ?>",table_columns, <?php echo $per_page; ?>);
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function Change_Status(id)
{ var searchURL ='<?php echo base_url().$controller_name."/chanage_status";?>';	 
  if (confirm('<?php echo lang('common_notification_status_successful');?>')) 
  {
	$.ajax({
		url: searchURL,
		type: 'post',
		data: 'person_id=' + id + '&method_type=stock_notfications' ,
		dataType:'json',
		success: function(response) 
		{ 
			//tb_remove();
			post_person_form_submit(response);
		}
	});   
  } 
  else {return false;}	 
}

</script>

<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('module_'.$controller_name); ?>
		</td>
		
		<td id="title_search">
			<?php if($this->router->method=='online'){ echo form_open("$controller_name/search_online",array('id'=>'search_form')); } 
			else { echo form_open("$controller_name/search_stock_notfications",array('id'=>'search_form'));}?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
	<td>
		<div id="commands">
			<?php echo anchor("items",$this->lang->line('common_manage_items'),array('class'=>'none import','title'=>$this->lang->line('common_manage_items')));?>
		</div>
	</td>
	<td style="width:10px;"></td>
	<td>
			<div id="item_table">
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
				</div>
			<div id="pagination">
				<?php echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>