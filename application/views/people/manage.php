<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function() 
{ 	$('#from_date').datePicker({startDate: '01/01/2015'});
    $('#to_date').datePicker({startDate: '01/01/2015'});
	var table_columns = ['','','','',''];
	enable_sorting("<?php echo site_url("$controller_name/sorting"); ?>",table_columns, <?php echo $per_page; ?>);
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function SendRequestTo_Cashier(request_type)
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('common_none_selected');?>');return false; }  
	if(request_type=='customers_ipd') { var confirm_message = '<?php echo lang('common_confirm_cashier_send_request_status_for_ipd');?>';}
	else { var confirm_message = '<?php echo lang('common_confirm_cashier_send_request_status');?>';}
	if (confirm(confirm_message)) 
	 {
	 	$.ajax({
            url: '<?php echo base_url()."settings/SendRequestToCashier_FromCustomers";?>',
            type: 'post',
            data: 'items_id=' + checkedval + '&category_type=' + request_type,
			dataType:'json',
            success: function(response) 
			{ 
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true); 
					return false;
				}
				$('input[type=checkbox]').attr('checked',false);
				set_feedback(response.message,'success_message',false);
			}
        });   
	 }
     else {return false;}	 
}


function Toshow_AllSend_IPD_admitted()
{ 
   var total_admittied = '<?php echo $this->functions->getTotal_CashierRequest($RequestType='customers_admission_ipd');?>'; 
   if(total_admittied=='00') { alert('<?php echo lang('common_no_persons_to_display');?>'); return false;}
   //$('#method_type').val('IPD_ADMITTED_PATIENT');
   $('#spinner').show();
	$.ajax({
            url: '<?php echo base_url()."customers/get_List_IPD_Admitted_Patient";?>',
            type: 'post',
			dataType:'json',
            success: function(response) 
			{   $('#spinner').hide();
				$('#customers_ipd_sec').show();
				$('#customers_opd_sec').hide();
				$('#filter_patient_sec').hide();
				$('#table_holder').html(response.manage_table);	
				$('#pagination').html(response.pagination);				
			}
        });  
}

function SearchCustomers()
{  var registration_type = $('#registration_type').val();
   var patient_category  = $('#patient_category').val();
   var from_date = $('#from_date').val();
   var to_date   = $('#to_date').val();
   $('#search').val('');
   if($('#from_date').val()!='' && $('#to_date').val()==''){ $("#to_date").css("background", "#f2dede");$("#to_date").css("border","1px solid #FF0000"); return false;}
   if($('#to_date').val()!='' && $('#from_date').val()==''){ $("#from_date").css("background", "#f2dede");$("#from_date").css("border","1px solid #FF0000"); return false;}
   if(Date.parse(from_date)>Date.parse(to_date))
   {  
	  alert("<?php echo lang('common_invalid_date_selection');?>");  
	  $("#from_date").css("background", "#f2dede");$("#from_date").css("border","1px solid #FF0000");
	  $("#to_date").css("background", "#f2dede");$("#to_date").css("border","1px solid #FF0000");
	  return false;
   }
	$('#spinner003').show();	
	$.ajax({
            url: '<?php echo base_url()."customers/searchFilterCustomers";?>',
            type: 'post',
			data: 'registration_type='+registration_type + '&patient_category=' + patient_category + '&from_date=' + from_date + '&to_date=' + to_date,
			dataType:'json',
            success: function(response) 
			{ 	$('#spinner003').hide();
				$('#sortable_table tbody').html(response.manage_table);
				$('#pagination').html(response.pagination);	
				tb_init('#sortable_table a.thickbox');	
			}
        });  
}

</script>

<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('module_'.$controller_name); ?>
		</td>
		<?php if($controller_name=='customers'){?>				
		<td id="filter_patient_sec">
          <select name="filter_patient" id="filter_patient" class="option_filter">
				<option value="">Select</option>
				<option value="">Accomodation</option>
				<option value="">Admission</option>
				<option value="">Consulation</option>			
				<option value="">Consultation Private Patient</option>
		  </select>
		</td>		
		<td>
			<input type="button" onclick="Toshow_AllSend_IPD_admitted()" value="<?=strtoupper(lang('common_admission')). ' : ' .$this->functions->getTotal_CashierRequest($RequestType='customers_admission_ipd');?>" class="send_request_opd" style="background-color: #1da2e0;border: 1px solid #1da2e0;font-size: 14px;padding: 2px 4px 5px 8px;width:auto;cursor:pointer;float:right;margin-left:5px;">
		</td>
		<?php if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id)){?>
		<td id="customers_ipd_sec" style="display:none;">
			<input type="button" onclick="SendRequestTo_Cashier('customers_ipd')" value="<?=strtoupper(lang('common_send_to_cashier_for_ipd'));?>" class="send_request_opd" style="background-color: #1da2e0;border: 1px solid #1da2e0;font-size: 14px;padding: 2px 4px 5px 8px;width:auto;cursor:pointer;float:right;margin-right: 20px;">
		</td>
		<td id="customers_opd_sec">
			<input type="button" onclick="SendRequestTo_Cashier('customers_opd')" value="<?=strtoupper(lang('common_send_to_cashier'));?>" class="send_request_opd" style="background-color: #1da2e0;border: 1px solid #1da2e0;font-size: 14px;padding: 2px 4px 5px 8px;width:auto;cursor:pointer;float:right;margin-right:10px;">
		</td>		
		<?php }else echo '<td>&nbsp;&nbsp;&nbsp;</td>';  }?>		
		
		<td id="title_search">
			<?php if($this->router->method=='online'){ echo form_open("$controller_name/search_online",array('id'=>'search_form')); } 
			else { echo form_open("$controller_name/search",array('id'=>'search_form'));}?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id)) 
				{ echo anchor("$controller_name/view/-1/width~$form_width",	$this->lang->line($controller_name.'_new'),array('class'=>'thickbox none new', 'title'=>$this->lang->line($controller_name.'_new')));}	
				if($controller_name =='customers')
				{ echo anchor("$controller_name/residence/width~600",$this->lang->line('common_manage_Residence'),array('class'=>'thickbox none new','title'=>$this->lang->line('common_manage_Residence')));	
				  echo anchor("$controller_name/occupations/width~600",$this->lang->line('common_manage_occupation'),array('class'=>'thickbox none new','title'=>$this->lang->line('common_manage_occupation')));	
				  echo anchor("$controller_name/excel_import/width~$form_width",lang('common_excel_import'),array('class'=>'thickbox none import','title'=>lang('customers_import_customers_from_excel')));	
				} 
				if ($controller_name == 'customers' || $controller_name == 'employees') { echo anchor("$controller_name/excel_export",lang('common_excel_export'),array('class'=>'none import')); }
				if($this->router->method!='online') 
				{ 	if($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) 
					{ echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive'));  } 				
				}
				if($controller_name =='customers'){ echo anchor("$controller_name/cleanup",lang("customers_cleanup_old_customers"),array('id'=>'cleanup', 'class'=>'cleanup')); }
				if($controller_name =='employees') 
				{ 
				  if($this->router->method=='online') { echo anchor("$controller_name",ucfirst($controller_name),array('class'=>'none new')); }
				  else 
				  { 
					if($this->Employee->has_module_action_permission('employees', 'employees_chat', $this->Employee->get_logged_in_employee_info()->person_id))
					{ echo anchor("$controller_name/online",lang("employees_online_employees"),array('class'=>'none new')); }
				  }
				}
				?>				
			</div>
			<?php if($controller_name == 'customers'){?>
			<div id="view_left_menu_div"> 
			<?php echo $this->Customer->Customer_Registration_Type_Dropdown('registration_type','registration_type','option_filter',$selected='');?>
			</div>
			
			<div id="view_left_menu_div">
			<?php echo $this->Customer->Customer_Registration_Type_Dropdown('patient_category','patient_category','option_filter',$selected='');?>
			</div>			
			
			<div id="view_left_menu_calender">
				<input type="text" name="from_date" id="from_date" value="<?php if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='')echo date('m/d/Y',strtotime($_SESSION['CUSTOMER_SEARCH_START_DATE']));?>" readonly="readonly" onclick="clicktohide(this);">
				<input type="text" name="to_date" id="to_date" value="<?php if(isset($_SESSION['CUSTOMER_SEARCH_END_DATE']) && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')echo date('m/d/Y',strtotime($_SESSION['CUSTOMER_SEARCH_END_DATE']));?>" readonly="readonly" onclick="clicktohide(this);">
			 </div>
			 
			 <div id="view_left_menu_calender">
					<input type="button" id="submit" class="search_trans" onclick="SearchCustomers();" value="<?=lang('common_search');?>" name="submit">
					<img id="spinner003" src="<?php echo base_url();?>images/loading.gif">
			 </div>			 
			 <?php $this->load->view('partial/patient_waiting_counter'); }?>			 
			 
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
			<div id="table_holder">
			<?php echo $manage_table; ?>
			</div>
			</div>
			<div id="pagination">
				<?php echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>