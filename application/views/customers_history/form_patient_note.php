<?php echo form_open('history/saveClinicalDianosis/'.$person_id.'/'.$clinical_note_id.'/'.$Diabetes_Customer_Type,array('id'=>'history_form'));?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend> <center><div class="internal_sec_title"><?php echo lang('history_clinical_note');?></div></center><div class="legend_internal_sec"><div class="field_row2 clearfix">	<?php echo form_label(lang('history_main_complains').':', 'main_complains',array('class'=>'wide')); ?>	<div class='form_field'>
	<textarea id="main_complains" name="main_complains" <?=$disabled_data;?> rows="2" cols="60" class="valid"><?=stripslashes($main_complains);?></textarea>	</div></div><div class="field_row_dotted"></div><div class="field_row2 clearfix">	<?php echo form_label(lang('history_historical_present_illness').':', 'present_illness',array('class'=>'wide','style'=>'width: 201px;')); ?>	<div class='form_field'>
		<textarea id="present_illness" name="present_illness" <?=$disabled_data;?> rows="2" cols="60" class="valid"><?=stripslashes($person_info->historical_present_illness);?></textarea>
	</div></div>

<div class="field_row2 clearfix">	
<?php echo form_label(lang('history_physical_examination').':', 'physical_examination_details',array('class'=>'wide','style'=>'width: 201px;')); ?>
	<div class='form_field'>
		<textarea id="physical_examination_details" name="physical_examination_details" <?=$disabled_data;?> rows="2" cols="60" class="valid"><?=stripslashes($person_info->physical_examination_details);?></textarea>
	</div>
</div>

<div class="field_row2 clearfix">	
<?php echo form_label(lang('history_system_examination').':', 'system_examination_details',array('class'=>'wide','style'=>'width: 201px;')); ?>
	<div class='form_field'>
		<textarea id="system_examination_details" name="system_examination_details" <?=$disabled_data;?> rows="2" cols="60" class="valid"><?=stripslashes($person_info->system_examination_details);?></textarea>
	</div>
</div>
<div class="field_row_dotted1"></div>
<div class="field_row2 clearfix">
<?php if($clinical_note_id==-1){?>	<?php echo form_label(lang('history_past_medical_history').':', 'past_medical_history',array('class'=>'wide')); ?>	<span class='form_field'>
		<a onclick="GetOldClinic_Reports('<?=$clinical_note_id;?>');" href="javascript:;"><span class="use_last_one"><?php echo lang('history_use_past_medical_history');?> </span></a>
		<img src="<?php echo base_url();?>images/spinner_small.gif" class="search-spiner01" id="searchspiner2" style="margin-left:10px;">
	</span>
	<?php } if($Diabetes_Customer_Type=='diabetes'){?>
		<span class='physical_exam_span form_field'>
		<a onclick="showThem('viewthem');" href="javascript:;"><span class="physical_exam_link"><?php echo lang('history_physical_examination');?> </span></a>
	</span>
	<?php }?>	</div>
<div class="field_row_dotted1"></div>
<div id="daignosis_main_sec">
<div class="daignosis_main_sec">	<div class="daignosis_main_left">		<div class="field_row3 clearfix">			<?php echo form_label(lang('common_admission_status').':', 'past_medical_history',array('class'=>'wide')); ?>			<div class='form_field'>						<input type="radio" name="admission_status" value="1" <?=$disabled_data;?> <?php if($person_info->admission_status=='1')echo 'checked="checked"';?>> <?php echo lang('common_yes');?>			<input type="radio" name="admission_status" value="0" <?=$disabled_data;?> <?php if($person_info->admission_status=='0')echo 'checked="checked"';?>> <?php echo lang('common_no');?>			<input type="text" class="input_width_2" name="admission_status_content" id="admission_status_content" <?=$disabled_data;?> value="<?=stripslashes($person_info->admission_status_description);?>" style="width:78%;">				</div>					</div>	</div>		<div class="daignosis_main_right">		<div class="field_row3 clearfix">			<?php echo form_label(lang('history_blood_transfussion').':', 'past_medical_history',array('class'=>'wide')); ?>			<div class='form_field'>			<input type="radio" name="blood_transfussion" value="1" <?=$disabled_data;?> <?php if($person_info->blood_transfussion=='1')echo 'checked="checked"';?>> <?php echo lang('common_yes');?>			<input type="radio" name="blood_transfussion" value="0" <?=$disabled_data;?> <?php if($person_info->blood_transfussion=='0')echo 'checked="checked"';?>> <?php echo lang('common_no');?>			<input type="text" class="input_width_2" name="blood_transfussion_content" id="blood_transfussion_content" <?=$disabled_data;?> value="<?=stripslashes($person_info->blood_transfussion_description);?>" style="width:40%;">				</div>		</div>	</div>		<div class="field_row_dotted"></div>		<div class="daignosis_main_left">		<div class="field_row3 clearfix">			<?php echo form_label(lang('common_surgery_status').':', 'surgery_status',array('class'=>'wide')); ?>			<div class='form_field'>			<input type="radio" name="surgery_status" value="1" <?=$disabled_data;?> <?php if($person_info->surgery_status=='1')echo 'checked="checked"';?>> <?php echo lang('common_yes');?>			<input type="radio" name="surgery_status" value="0" <?=$disabled_data;?> <?php if($person_info->surgery_status=='0')echo 'checked="checked"';?>> <?php echo lang('common_no');?>			<input type="text" class="input_width_2" name="surgery_status_content" id="surgery_status_content" <?=$disabled_data;?> value="<?=stripslashes($person_info->surgery_status_description);?>" style="width:78%;">				</div>					</div>	</div>		<div class="daignosis_main_right">		<div class="field_row3 clearfix">			<?php echo form_label(lang('common_allegis_food_drugs').':', 'allegis_food_drugs',array('class'=>'wide')); ?>			<div class='form_field'>			<input type="radio" name="allegis_food_drugs" value="1" <?=$disabled_data;?> <?php if($person_info->allegis_food_drugs=='1')echo 'checked="checked"';?>> <?php echo lang('common_yes');?>			<input type="radio" name="allegis_food_drugs" value="0" <?=$disabled_data;?> <?php if($person_info->allegis_food_drugs=='0')echo 'checked="checked"';?>> <?php echo lang('common_no');?>			<input type="text" class="input_width_2" name="allegis_food_drugs_content" id="allegis_food_drugs_content" <?=$disabled_data;?> value="<?=stripslashes($person_info->allegis_food_drugs_description);?>" style="width:40%;">				</div>		</div>	</div>		</div>
</div>				<div class="field_row clearfix">       <span id="error_message_box" style="display:none"></span>		<div class="diagnosis_serach"> <?php echo lang('history_clinical_daignosis');?>: <?php echo lang('search_diseases');?> 					<input type="text" class="search_daignosis" name="search_lab_service" id="search_lab_service" value="">			<img src="<?php echo base_url();?>images/spinner_small.gif" class="search-spiner01" id="searchspiner" style="margin-left:-20px;">				</div>				<div id="labcategory" class="diagnoses_div_sec"><?php if(isset($diseases_list) && $diseases_list!=''){ echo $diseases_list; }?>	</div>		</div>
<?php if($action!='view') { echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right')); }	
	  echo '<input class="delete-button" type="button" name="saveNote" value="'.lang('common_close').'" onclick="close_form();">';	  
?></div>
</fieldset>


<!-- START PHYSICAL EXAMINATION FORM****************-->
<div id="physical_examination"> <?php $this->load->view('customers_history/form_physical_examination');?></div>
<!-- START PHYSICAL EXAMINATION FORM****************-->

<?php echo form_close();?>
<script type='text/javascript'>
function showThem(type)
{ 
	if(type=='viewthem'){ $('#physical_examination').show(); }
	if(type=='hidethem'){ $('#physical_examination').hide(); }
}

function GetPhysical_Examination(item_id)
{   
	$('#login_pop').show();
}

function GetOldClinic_Reports(item_id)
{   
	var GetDataURL ="<?php echo base_url().'history/getClinic_PreviousReports/'.$person_id?>";
	$('#searchspiner2').show();
	$.ajax({ 
			url: GetDataURL,
			type: 'post',
			data: 'clinical_note_id='+<?=$clinical_note_id;?>,
			success: function(data)  
				{	
				   $('#searchspiner2').hide();
				   $('#daignosis_main_sec').html(data);					
				}
		   }); 
}

//**************START CODE FOR LAB DIAGNOSES SEARCH*********************//
$('#search_lab_service').keyup(function(e) { 
 var existingString = $("#search_lab_service").val();
 //if(existingString==''){ return false; }
    clearTimeout($.data(this, 'timer'));
    if (e.keyCode == 13)
      search(existingString);
    else   $(this).data('timer', setTimeout(search(existingString), 500));
});


function search(existingString) { 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });		  
	var searchURL ='<?php echo base_url().'history/searchClinicalDiseasList/'.$person_id ?>';
	$('#searchspiner').show();
	$.ajax({
            url: searchURL,
            type: 'post',
			data: 'service='+existingString+'&checkedval='+checkedval+'&clinical_note_id='+<?=$clinical_note_id;?>,
            success: function(data) 
				{ 
					$('#labcategory').html(data);  $('#searchspiner').hide(); 
				}
        }); 
}


//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#history_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			main_complains: "required",
			present_illness: "required",
			admission_status: "required",
			blood_transfussion: "required",
			surgery_status: "required",
			allegis_food_drugs: "required",
   		},
		messages: 
		{     
     		main_complains: <?php echo json_encode(lang('history_main_complains_required')); ?>,
			present_illness: <?php echo json_encode(lang('history_historical_present_illness_required')); ?>,
			admission_status: <?php echo json_encode(lang('history_admission_status_required')); ?>,
			blood_transfussion: <?php echo json_encode(lang('blood_transfussion_required')); ?>,
			surgery_status: <?php echo json_encode(lang('surgery_status_required')); ?>,
			allegis_food_drugs: <?php echo json_encode(lang('history_allegis_food_drugs_required')); ?>,
		}
	});

});

</script>