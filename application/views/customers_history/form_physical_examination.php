<style>
fieldset div.field_row div.form_field select {width:193px;}
fieldset div.field_row {font-weight:normal;}
</style>
<fieldset id="customer_basic_info">

<div class="round_div_sec"><?php echo lang("history_physical_examination_form_title"); ?></div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_general_appearance') .':'); 
$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','general_appearance','category_name');?>
	<div class='form_field'>
		<select name="general_appearance" <?=$disabled_data;?> id="general_appearance" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_List->num_rows()>0){
			foreach($category_List->result() as $data){?>
				<option value="<?=$data->id;?>" <?php if($physical_info->general_appearance==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
			<?php }}?>
		</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_mental_status') .':'); ?>
	<div class='form_field'> <input type="text" id="mental_status" name="mental_status" value="<?=$physical_info->mental_status;?>" onclick="clicktohide(this);" <?=$disabled_data;?>> </div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_breasts_stage') .':'); 
$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','breasts_stage','category_name');?>
	<div class='form_field'>
		<select name="breasts_stage" <?=$disabled_data;?> id="breasts_stage" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_List->num_rows()>0){
			foreach($category_List->result() as $data){?>
				<option value="<?=$data->id;?>" <?php if($physical_info->breasts_stage==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
			<?php }}?>
			</select>		
	</div>
</div> 

<div class="field_row clearfix">	
<?php echo form_label(lang('history_pubic_hairstage') .':'); 
$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','pubic_hair_stage','category_name');?>
	<div class='form_field'>
		<select name="pubic_hair_stage" <?=$disabled_data;?> id="pubic_hair_stage" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_List->num_rows()>0){
			foreach($category_List->result() as $data){?> 
				<option value="<?=$data->id;?>" <?php if($physical_info->pubic_hair_stage==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
			<?php }}?>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_genitalia_normal') .':'); ?>
	<div class='form_field'>
		<select name="genitalia_normal" <?=$disabled_data;?> id="genitalia_normal" class="valid" onclick="clicktohide(this);">
		    <option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->genitalia_normal=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->genitalia_normal=='1') echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_testes_normal_right') .':'); ?>
	<div class='form_field'>
		<select name="testes_normal_right" <?=$disabled_data;?> id="testes_normal_right" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->testes_normal_right=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->testes_normal_right=='1')echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_testes_normal_left') .':'); ?>
	<div class='form_field'>
		<select name="testes_normal_left" <?=$disabled_data;?> id="testes_normal_left" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->testes_normal_left=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->testes_normal_left=='1')echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_eye_pupils_perrl') .':'); ?>
	<div class='form_field'>
		<select name="eye_pupils_perrl" <?=$disabled_data;?> id="eye_pupils_perrl" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->eye_pupils_perrl=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->eye_pupils_perrl=='1')echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_eye_lenses_right') .':'); ?>
	<div class='form_field'>
		<select name="eye_lenses_right" <?=$disabled_data;?> id="eye_lenses_right" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->eye_lenses_right=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->eye_lenses_right=='1')echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_eye_lenses_left') .':'); ?>
	<div class='form_field'>
		<select name="eye_lenses_left" <?=$disabled_data;?> id="eye_lenses_left" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if($physical_info->eye_lenses_left=='0')echo 'selected="selected"';?>><?=lang('common_yes');?></option>
			<option value="1" <?php if($physical_info->eye_lenses_left=='1')echo 'selected="selected"';?>><?=lang('common_no');?></option>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_thyroid_right_lobe') .':'); 
$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','thyroid_right_lobe','category_name');?>
	<div class='form_field'>
		<select name="thyroid_right_lobe" <?=$disabled_data;?> id="thyroid_right_lobe" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_List->num_rows()>0){
			foreach($category_List->result() as $data){?>
				<option value="<?=$data->id;?>" <?php if($physical_info->thyroid_right_lobe==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
			<?php }}?>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_thyroid_left_lobe') .':'); 
$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','thyroid_left_lobe','category_name');?>
	<div class='form_field'>
		<select name="thyroid_left_lobe" <?=$disabled_data;?> id="thyroid_left_lobe" class="valid" onclick="clicktohide(this);">
			<option value=""><?php echo lang('common_select_type');?></option>
			<?php if($category_List->num_rows()>0){
			foreach($category_List->result() as $data){?>
				<option value="<?=$data->id;?>" <?php if($physical_info->thyroid_left_lobe==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
			<?php }}?>
			</select>		
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('history_lungs') .':'); ?>
	<div class='form_field'> <?php echo lang('history_clear_to_auscultation');?> <input type="checkbox" name="history_lungs" <?=$disabled_data;?> id="history_lungs" value="1" <?php if($physical_info->history_lungs=='1')echo 'checked="checked"';?> style="width:auto;">
	</div>
</div>

<div class="round_div_sec"><?=lang('history_heart');?></div>
		<div class="field_row clearfix">	
			<?php echo form_label(lang('history_heart_regular') .':'); ?>
			<div class='form_field'> 
				<input type="radio" <?=$disabled_data;?> name="heart_regular" value="0" <?php if($physical_info->heart_regular=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
				<input type="radio" <?=$disabled_data;?> name="heart_regular" value="1" <?php if($physical_info->heart_regular=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
			</div>
		</div>

		<div class="field_row clearfix">	
			<?php echo form_label(lang('history_heart_murmur') .':'); ?>
			<div class='form_field'> 
				<input type="radio" <?=$disabled_data;?> name="heart_murmur" value="0" <?php if($physical_info->heart_murmur=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
				<input type="radio" <?=$disabled_data;?> name="heart_murmur" value="1" <?php if($physical_info->heart_murmur=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
			</div>
		</div>

<div class="round_div_sec"><?=lang('history_abdomen');?></div>
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_liver_enlarged') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="liver_enlarged" value="0" <?php if($physical_info->liver_enlarged=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="liver_enlarged" value="1" <?php if($physical_info->liver_enlarged=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_spleen_enlarged') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="spleen_enlarged" value="0" <?php if($physical_info->spleen_enlarged=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="spleen_enlarged" value="1" <?php if($physical_info->spleen_enlarged=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_tenderness') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="tenderness" value="0" <?php if($physical_info->tenderness=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="tenderness" value="1" <?php if($physical_info->tenderness=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_ascites') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="ascites" value="0" <?php if($physical_info->ascites=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="ascites" value="1" <?php if($physical_info->ascites=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>

<div class="round_div_sec"><?=lang('history_neurology');?></div>
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_tremor') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="tremor" value="0" <?php if($physical_info->tremor=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="tremor" value="1" <?php if($physical_info->tremor=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_reflexes_biceps') .':'); 
		$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','neurology_reflexes','category_name');?>
		<div class='form_field'>
			<select name="reflexes_biceps" <?=$disabled_data;?> id="reflexes_biceps" class="valid" onclick="clicktohide(this);">
				<option value=""><?php echo lang('common_select_type');?></option>
				<?php if($category_List->num_rows()>0){
				foreach($category_List->result() as $data){?>
					<option value="<?=$data->id;?>" <?php if($physical_info->reflexes_biceps==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_reflexes_knees') .':'); 
		$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','neurology_reflexes','category_name');?>
		<div class='form_field'>
			<select name="reflexes_knees" <?=$disabled_data;?> id="reflexes_knees" class="valid" onclick="clicktohide(this);">
				<option value=""><?php echo lang('common_select_type');?></option>
				<?php if($category_List->num_rows()>0){
				foreach($category_List->result() as $data){?>
					<option value="<?=$data->id;?>" <?php if($physical_info->reflexes_knees==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_reflexes_ankles') .':'); 
		$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','neurology_reflexes','category_name');?>
		<div class='form_field'>
			<select name="reflexes_ankles" <?=$disabled_data;?> id="reflexes_ankles" class="valid" onclick="clicktohide(this);">
				<option value=""><?php echo lang('common_select_type');?></option>
				<?php if($category_List->num_rows()>0){
				foreach($category_List->result() as $data){?>
					<option value="<?=$data->id;?>" <?php if($physical_info->reflexes_ankles==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>

<div class="round_div_sec"><?=lang('history_skin');?></div>
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_acne') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="skin_acne" value="0" <?php if($physical_info->skin_acne=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="skin_acne" value="1" <?php if($physical_info->skin_acne=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_scoliosis') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="skin_scoliosis" value="0" <?php if($physical_info->skin_scoliosis=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="skin_scoliosis" value="1" <?php if($physical_info->skin_scoliosis=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
			<?php echo form_label(lang('history_striae') .':'); ?>
			<div class='form_field'> 
				<input type="radio" <?=$disabled_data;?> name="skin_striae" value="0" <?php if($physical_info->skin_striae=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
				<input type="radio" <?=$disabled_data;?> name="skin_striae" value="1" <?php if($physical_info->skin_striae=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
			</div>
	</div>	

	<div class="field_row clearfix">	 
			<?php echo form_label(lang('history_skin_colour') .':'); ?>
			<div class='form_field'> 
				<input type="radio" <?=$disabled_data;?> name="skin_colour" value="0" <?php if($physical_info->skin_colour=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_skin_colour_pink');?>
				<input type="radio" <?=$disabled_data;?> name="skin_colour" value="1" <?php if($physical_info->skin_colour=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_skin_colour_purplish');?>
				<input type="radio" <?=$disabled_data;?> name="skin_colour" value="2" <?php if($physical_info->skin_colour=='2')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_skin_colour_pale');?>
			</div>
	</div>	

   <div class="field_row clearfix">	
		<?php echo form_label(lang('history_injection_sites') .':');  
		$category_List = $this->savesetting->Get_Table_Category_Data('measurement_category','injection_sites','category_name');?>
		<div class='form_field'>
			<select name="injection_sites" <?=$disabled_data;?> id="injection_sites" class="valid" onclick="clicktohide(this);">
				<option value=""><?php echo lang('common_select_type');?></option>
				<?php if($category_List->num_rows()>0){
				foreach($category_List->result() as $data){?> 
					<option value="<?=$data->id;?>" <?php if($physical_info->injection_sites==$data->id)echo 'selected="selected"';?>><?=ucfirst($data->category_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>
	
<div class="round_div_sec"><?=lang('history_right_foot');?></div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_dorsalis_pedis_pulse') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_dorsalis_pedis_pulse" value="0" <?php if($physical_info->right_dorsalis_pedis_pulse=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="right_dorsalis_pedis_pulse" value="1" <?php if($physical_info->right_dorsalis_pedis_pulse=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_tibialis_posterior_pulse') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_tibialis_posterior_pulse" value="0" <?php if($physical_info->right_tibialis_posterior_pulse=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="right_tibialis_posterior_pulse" value="1" <?php if($physical_info->right_tibialis_posterior_pulse=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_ulcers') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_ulcers" value="0" <?php if($physical_info->right_ulcers=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="right_ulcers" value="1" <?php if($physical_info->right_ulcers=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_signs_of_infection') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_signs_of_infection" value="0" <?php if($physical_info->right_signs_of_infection=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="right_signs_of_infection" value="1" <?php if($physical_info->right_signs_of_infection=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_toenail_abnormalities') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_toenail_abnormalities" value="0" <?php if($physical_info->right_toenail_abnormalities=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="right_toenail_abnormalities" value="1" <?php if($physical_info->right_toenail_abnormalities=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_monofilament_test') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="right_monofilament_test_normal" value="0" <?php if($physical_info->right_monofilament_test_normal=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_normal');?>
			<input type="radio" <?=$disabled_data;?> name="right_monofilament_test_normal" value="1" <?php if($physical_info->right_monofilament_test_normal=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_abnormal');?>		
		</div>
	</div>	
	
		
	<div class="round_div_sec"><?=lang('history_left_foot');?></div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_dorsalis_pedis_pulse') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_dorsalis_pedis_pulse" value="0" <?php if($physical_info->left_dorsalis_pedis_pulse=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="left_dorsalis_pedis_pulse" value="1" <?php if($physical_info->left_dorsalis_pedis_pulse=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_tibialis_posterior_pulse') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_tibialis_posterior_pulse" value="0" <?php if($physical_info->left_tibialis_posterior_pulse=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="left_tibialis_posterior_pulse" value="1" <?php if($physical_info->left_tibialis_posterior_pulse=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_ulcers') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_ulcers" value="0" <?php if($physical_info->left_ulcers=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="left_ulcers" value="1" <?php if($physical_info->left_ulcers=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_signs_of_infection') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_signs_of_infection" value="0" <?php if($physical_info->left_signs_of_infection=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="left_signs_of_infection" value="1" <?php if($physical_info->left_signs_of_infection=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_toenail_abnormalities') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_toenail_abnormalities" value="0" <?php if($physical_info->left_toenail_abnormalities=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_yes');?>
			<input type="radio" <?=$disabled_data;?> name="left_toenail_abnormalities" value="1" <?php if($physical_info->left_toenail_abnormalities=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('common_no');?>		
		</div>
	</div>	
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_monofilament_test') .':'); ?>
		<div class='form_field'> 
			<input type="radio" <?=$disabled_data;?> name="left_monofilament_test_normal" value="0" <?php if($physical_info->left_monofilament_test_normal=='0')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_normal');?>
			<input type="radio" <?=$disabled_data;?> name="left_monofilament_test_normal" value="1" <?php if($physical_info->left_monofilament_test_normal=='1')echo 'checked="checked"';?> style="width:auto;"> <?=lang('history_abnormal');?>		
		</div>
	</div>	

	<div class="field_row clearfix">	
		<?php echo form_label(lang('history_describe_deformities_foot') .':'); ?>
		<div class='form_field'><input type="text" id="describe_deformities_foot" name="describe_deformities_foot" value="<?=$physical_info->describe_deformities_foot;?>" onclick="clicktohide(this);" <?=$disabled_data;?>> </div>
	</div>
	<?php if($action!='view'){?><input class="add-button" type="button" value="Add" onclick="showThem('hidethem');"><?php }?>
								<input class="add-button" type="button" value="close" onclick="showThem('hidethem');">

</fieldset>