<?php $this->load->view("partial/header"); ?>

<script type="text/javascript">
$(document).ready(function() 
{   $('#from_date').datePicker({startDate: '01/01/2015'}); 
    $('#to_date').datePicker({startDate: '01/01/2015'});
	var table_columns = ['','last_name','module_id','report_subject ','added_date','status'];
	enable_sorting("<?php echo site_url("$controller_name/sorting"); ?>",table_columns, <?php echo $per_page; ?>);
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$person_info->person_id");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_delete(<?php echo json_encode(lang('history_observation_confirm_delete'));?>,<?php echo json_encode(lang('history_observation_none_selected'));?>);
}); 
</script>

<script type="text/javascript">
function post_person_form_submit(response)
{
	if(!response.success)
	{ 
		set_feedback(response.message,'error_message',true);	
	}
	else
	{   
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{ 
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else 
		{  
			do_search(true,function()
			{
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function getFilterData(status_type) 
{   
	var searchURL = "<?php echo base_url().'observations/search/'.$person_info->person_id;?>";
	$('#find_category_id').val(status_type);
	do_search(true,function(){ $('#method_type').val(); $('#find_category_id').val();});  
}

function SearchObservations(view_type)
{  
   $('#search').val('');
   var from_date = $('#from_date').val();
   var to_date = $('#to_date').val();
   if($('#from_date').val()!='' && $('#to_date').val()==''){ $("#to_date").css("background", "#f2dede");$("#to_date").css("border","1px solid #FF0000"); return false;}
   if($('#to_date').val()!='' && $('#from_date').val()==''){ $("#from_date").css("background", "#f2dede");$("#from_date").css("border","1px solid #FF0000"); return false;}
   if(Date.parse(from_date)>Date.parse(to_date))
   {  
	  alert("<?php echo lang('common_invalid_date_selection');?>");  
	  $("#from_date").css("background", "#f2dede");$("#from_date").css("border","1px solid #FF0000");
	  $("#to_date").css("background", "#f2dede");$("#to_date").css("border","1px solid #FF0000");
	  return false;
   }
	$('#spinner003').show();
	if(view_type=='download_PDF') { actionURL = '<?php echo base_url()."observations/create_reports_PDF/".$customer_id;?>';}
	else { actionURL = '<?php echo base_url()."observations/searchObservations/".$customer_id;?>'; }	
	$.ajax({
            url: actionURL,
            type: 'post',
			data: 'from_date=' + from_date + '&to_date=' + to_date,
			dataType:'json',
            success: function(response) 
			{ 	
				$('#spinner003').hide();
				if(view_type=='download_PDF')
				{
                  if(response.success) {  window.open('<?php echo base_url();?>' + response.message,'_blank');done = true; }
				  else { alert(response.message); }
				  return false;
				}				
				$('#sortable_table tbody').html(response.manage_table);				
				$('#pagination').html(response.pagination);	
				tb_init('#sortable_table a.thickbox');	
			}
        });  
}

</script>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo $page_title; ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search/$person_info->person_id",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<input type="hidden" name ="method_type" id="method_type" value="<?=$method_type;?>"/>
				<input type="hidden" name ="find_category_id" id="find_category_id" value=""/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">
			<?php 			  
				 if($this->Employee->has_module_action_permission('history', 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				 {
				   echo anchor("$controller_name/view/$customer_id/-1/width~$form_width",$this->lang->line('history_new_observation'),array('class'=>'thickbox none new','title'=>$this->lang->line('history_observation_form')));
				 }
				   echo anchor("history/index/$customer_id/",$this->lang->line('history_clinical_note'),array('class'=>'import new','title'=>$this->lang->line('history_clinical_note')));
				 if($this->Employee->has_module_action_permission('history', 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) 
				 {
					echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete','class'=>'delete_inactive'));
				 } ?>
			</div>
			
			<div id="view_calender_sec">
				<input type="text" name="from_date" id="from_date" value="" readonly="readonly" onclick="clicktohide(this);"> 
				<input type="text" name="to_date" id="to_date" value="" readonly="readonly" onclick="clicktohide(this);">
			</div>			 
			<div id="view_calender_sec">
			 	<input type="button" id="submit" class="search_left_menu" onclick="SearchObservations('Filters');" value="<?=lang('common_search');?>" name="submit">
				<input type="button" class="send_to_pdf_green" name="to_date" value="<?=lang('common_send_to_pdf');?>" onclick="SearchObservations('download_PDF');">
			    <img id="spinner003" src="<?php echo base_url();?>images/loading.gif">
			</div>
			 
			<?php $this->load->view('partial/patient_waiting_counter');?>			
			
		</td>
		<td style="width:10px;"></td>
	<td>
	 <div id="item_table">			
		<table class="tablesorter">		
			<tr><th class="leftmost"></th></tr>			
			<tr> 
			<td width="90%" style="background-color: #ffffff; border-left: 1px solid #BBBBBB;">
			<?php $this->load->view('partial/history_customers_menu');?>	
				  
				  <div class="heigthdiv"></div>		 
				   <?=$manage_history_menu;?>		  
				   <div class="heigthdiv1"></div>		   
					<div class="comment_header_sec"> <span class="comment_header_title" style="color: #000000;"><?php echo strtoupper(lang('history_observations')); ?></span>
						<span class="group_filter_sec">
							<select id="observations_status" name="observations_status" style="width:120px" onchange="getFilterData(this.value);">
								<option value=""><?=lang('common_select_type');?></option>
								<option value="0"><?=lang('common_opd');?></option>
								<option value="1"><?=lang('common_ipd');?></option>
							</select>
						</span>	 
					</div>		   
					<div id="observations_sec">
						<table id="sortable_table" class="tablesorter" border="6">				
							<div id="table_holder"><?php echo $manage_table; ?>	</div>
							<div id="pagination" class="pagination_sec"><?php echo $pagination;?></div>							
						</table>				
					</div>		
				</td>
			</tr>		
		</table>		
			
	</div>			
	</td>
 </tr>
</table>
<div id="feedback_bar"></div>
<div class="loadmask-msg loadmasksec" id="loadmasksec"><div>Please wait...</div></div>
<?php $this->load->view("partial/footer"); ?>