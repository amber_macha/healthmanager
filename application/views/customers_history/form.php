<?php 
echo form_open('history/save/'.$person_id,array('id'=>'history_form'));
?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>


<div class="field_row clearfix">	
<?php echo form_label(lang('comment').':', 'comment',array('class'=>'required')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'comments',
		'id'=>'comments',
		'value'=>$comments,
		'rows'=>'10',
		'cols'=>'60')		
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>


<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#history_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			comments: "required"
   		},
		messages: 
		{     		
     		comments: <?php echo json_encode(lang('comment_required')); ?>
		}
	});
});
</script>