<?php $this->load->view("partial/header"); ?>
<style>
table {border-color:#FFFFFF;}
#sortable_table a {color:#FF0000;font-weight:bold;}
#labcategory{width: 480px;}
</style>
<script type="text/javascript">
$(document).ready(function() 
{ 
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$person_info->person_id");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_email('<?php echo site_url("$controller_name/mailto")?>');
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{ 
		set_feedback(response.message,'error_message',true);	
	}
	else
	{   
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{ 
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{  
			do_search(true,function()
			{
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function getFilterComment(Group_id) 
{   
   //if(Group_id=='') return false;
	var searchURL = "<?php echo base_url().'history/getFilterComment/'.$person_info->person_id;?>";
	$('#method_type').val('search_by_group');
	$('#find_category_id').val(Group_id);
	do_search(true,function(){ $('#method_type').val(''); $('#find_category_id').val('');});
}


</script>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">	<?php echo lang('module_'.$controller_name); ?>	</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search/$person_info->person_id/$Diabetes_Customer_Type",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<input type="hidden" name ="method_type" id="method_type" value=""/>
				<input type="hidden" name ="find_category_id" id="find_category_id" value=""/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">
			<?php 
			   if($this->Employee->has_module_action_permission('customers', 'patient_profile', $this->Employee->get_logged_in_employee_info()->person_id))
				 {
				   echo anchor("customers/search_patient_reports/$person_info->person_id/width~600",$this->lang->line('common_patient_profile'),array('class'=>'thickbox none new', 'title'=>$this->lang->line('common_patient_profile')));
				 }
				 if($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				 {
				   echo anchor("$controller_name/view/$person_info->person_id/-1/$Diabetes_Customer_Type/width~$form_width",$this->lang->line('history_clinical_note'),array('class'=>'thickbox none new', 'title'=>$this->lang->line('history_patient_comment')));
				   echo anchor("$controller_name/diseasesForm/$person_info->person_id/width~600",$this->lang->line('history_add_clinical_daignosis'),array('class'=>'thickbox none new', 'title'=>$this->lang->line('history_clinical_daignosis')));
				 }
				 echo anchor("observations/index/$person_info->person_id/",$this->lang->line('history_observations'),array('class'=>'import new','title'=>$this->lang->line('history_observations')));
				 if($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) 
				 {
					echo anchor("$controller_name/delete/$person_info->person_id",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive'));
				 } ?>
			</div>
			
			<?php $this->load->view('partial/patient_waiting_counter');?>			
			
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
			
			
		<div id="table_holder">			
	<table class="tablesorter">
	
	<tr><th class="leftmost"></th></tr>
	
	<tr> <td width="90%" style="background-color: #ffffff; border-left: 1px solid #BBBBBB;">
	<?php $this->load->view('partial/history_customers_menu');?>	
		  
		  <div class="heigthdiv"></div>		 
		   <?=$manage_history_menu;?>		  
		   <div class="heigthdiv1"></div>		   
				<div class="comment_header_sec"> <span class="comment_header_title"><?php echo lang('doctor_comment'); ?></span>
					<span class="group_filter_sec"><?php echo lang('doctor_comment_filter_by'); ?> : &nbsp;
						<select class="group_filter" id="group_name" name="group_name" onchange="getFilterComment(this.value);">
						<option value=""><?php echo lang('common_select_type');?></option> 
						<?php if(isset($SubGroupsData) && $SubGroupsData!='' && $SubGroupsData->num_rows()>0){
						foreach($SubGroupsData->result() as $rowdata){?>
						<option value="<?=$rowdata->id;?>"><?=ucfirst($rowdata->group_name);?></option>
					 <?php }}?>
					</select>	
					</span>	 
				</div>		   
		
				<table id="sortable_table" class="tablesorter" border="6">
				<tbody><?php echo $manage_comment;?></tbody>
				</table>	
		</td>
	</tr>
	
	</table>
				
			 </div>
			
			</div>
			<div id="pagination">
				<?php //echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<div class="loadmask-msg loadmasksec" id="loadmasksec"><div>Please wait...</div></div>
<?php $this->load->view("partial/footer"); ?>