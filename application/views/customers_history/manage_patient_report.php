<?php $this->load->view("partial/header"); ?>
<!-- Add calender code and css nd jss files -->
<link rel="stylesheet" href="<?php echo base_url();?>js/datepicker/jquery-ui.css">
<script src="<?php echo base_url();?>js/datepicker/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>js/datepicker/jquery-ui.js"></script>
<script>$(function() { $( "#start_date" ).datepicker({ dateFormat: 'dd-mm-yy'});  $( "#end_date" ).datepicker({ dateFormat: 'dd-mm-yy'});});
</script>
<style>
.field_row { border-bottom: none;}
.form_field { padding: 5px;}
</style>
<!-- Add calender code and css nd jss files -->	

<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('patinte_report_title'); ?>
		</td>
		
	</tr>
</table>
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id))
				{?>
					<a class="none new" title="<?=$this->lang->line('print_out');?>" href="javascript:;" onclick="$.jPrintArea('#table_holder');"><?=$this->lang->line('print_out');?></a>
				
				<?php }	?>	
			</div>
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">			
		<div id="table_holder">			
		<table class="tablesorter">	
		<tr><th class="leftmost"></th></tr>

		<tr> <td width="90%" style="background-color: #ffffff; border-left: 1px solid #BBBBBB;">
		<?php $this->load->view('partial/history_customers_menu');?>
	      <?php echo form_open('history/patient_report/'.$person_info->person_id,array('id'=>'report_form','onsubmit'=>'return GenerateReport()')); ?>
		  <div class="document_sec">		  
		       <div class="document_class">  
				<input type="radio" name="documentation" value="1" <?php if((isset($_POST['documentation']) && $_POST['documentation']=='1') || !isset($_POST['documentation']))echo 'checked="checked"';?> onclick="GetReportDataType(1)"> <?php echo lang('patinte_report_title');?> &nbsp;&nbsp;  
				<input type="radio" name="documentation" value="2" onclick="GetReportDataType(2)" <?php if(isset($_POST['documentation']) && $_POST['documentation']=='2') echo 'checked="checked"';?>> <?php echo lang('common_referal');?>
			   </div>
			   <div id="document_referal_sec" <?php if(isset($_POST['documentation']) && $_POST['documentation']=='2') echo 'style="display:block"';?>>
					<div class="field_row clearfix">	
					  <?php echo form_label(lang('diabetes_zone_name').':', 'Zone');
					  $zone = $this->diabetescare->zone_name_List('zone_name');?>
					  <div class="form_field">
								<select class="valid report_option" id="zone_name" name="zone_name" onchange="GetListData(this.value)" onClick="clicktohide(this);">
								<option value=""><?php echo lang('diabetes_select_zone');?></option>
								<?php 
								foreach($zone->result() as $data){?>
								<option value="<?=$data->zone_name;?>" <?php if(isset($_POST['zone_name']) && $_POST['zone_name']==$data->zone_name)echo 'selected="selected"';?>><?=$data->zone_name;?></option>
								<?php }?>
								</select>
								<span id="zone_ajax_img" style="display: none;"><img width="15" src="<?php echo base_url();?>images/loading2.gif"></span>
						</div>
					</div>
					<div class="field_row clearfix">	
					  <?php echo form_label(lang('common_hospital_name').':', 'hospital_name');?>
					  <div class="form_field" id="hospital_sec">
						<?php if(isset($_POST['zone_name']) && $_POST['zone_name']!='') $zone_name = $_POST['zone_name']; else $zone_name='';
							  if(isset($_POST['hospital_name']) && $_POST['hospital_name']!='') $hospital_name = $_POST['hospital_name'];else $hospital_name='-1';
						echo $this->diabetescare->Hospital_DropDownList($zone_name, $hospital_name);?> 
					 </div>
					</div>
					
					<div class="document_class">  
						<input type="radio" name="documentation_type" value="3" onclick="GetReportDataType(3)" <?php if(isset($_POST['documentation_type']) && $_POST['documentation_type']=='3') echo 'checked="checked"';?>> <?php echo lang('common_referal_documentation');?> &nbsp;&nbsp; 
						<input type="radio" name="documentation_type" value="4" onclick="GetReportDataType(4)" <?php if(isset($_POST['documentation_type']) && $_POST['documentation_type']=='4') echo 'checked="checked"';?>> <?php echo lang('common_referal_paper_documentation');?>
					</div>
					<div id="documentation_type_sec" <?php if(isset($_POST['documentation_type']) && $_POST['documentation_type']=='4') echo 'style="display:block"';?>>
						<input type="radio" value="5" name="report_type" <?php if(isset($_POST['report_type']) && $_POST['report_type']=='5') echo 'checked="checked"';?>> <?php echo lang('common_excle');?> &nbsp;&nbsp; 
						<input type="radio" value="6" name="report_type" <?php if(isset($_POST['report_type']) && $_POST['report_type']=='6') echo 'checked="checked"';?>> <?php echo lang('common_pdf');?> &nbsp;&nbsp; 
						<input type="radio" value="7" name="report_type" <?php if(isset($_POST['report_type']) && $_POST['report_type']=='7') echo 'checked="checked"';?>> <?php echo lang('common_mysql');?>
                    </div>					
	          </div>
			  
				<div class="date-field" id="date-field">
					<?php echo lang('start_date'); ?> <input type="text" name="start_date" id="start_date" value="<?php if(isset($_POST['start_date']) && $_POST['start_date']!='') echo $_POST['start_date'];?>" class="date-field-input">&nbsp;&nbsp;&nbsp;&nbsp;
					<?php echo lang('end_date'); ?> <input type ="text" name="end_date" id="end_date" value="<?php if(isset($_POST['end_date']) && $_POST['end_date']!='') echo $_POST['end_date'];?>" class="date-field-input">&nbsp;&nbsp;&nbsp;&nbsp;
					<input class="add-button" type="submit" value=" <?php echo lang('go'); ?>">
					<span class="result-div"> <img id="searchspiner" src="<?php echo base_url();?>images/spinner_small.gif" style="display: none;"></span>
				</div>			  
		  </div>	   			   
		<?php echo form_close();?>
		
			<div id="sortable_table"> <?php echo $manage_report;?></div>
		</td>
	</tr>	
	</table>				
 </div>			
		</div>		
	</td>
</tr>
</table>
<div id="feedback_bar"></div>

<script>
function GetReportDataType(type)
{   if(type==1){ $('#document_referal_sec').hide();}
	if(type==2){ $('#document_referal_sec').show();}
	if(type==3){ $('#documentation_type_sec').hide();}
	if(type==4){ $('#documentation_type_sec').show();}
	
	//var searchURL ='<?php echo base_url().'diabetes_care/GetHospitals/';?>' +zone_name;
	//$.get(searchURL, function(data) { $('#hospital_sec').html(data); $('#zone_ajax_img').hide(); });
}


function GetListData(zone_name)
{
	$('#zone_ajax_img').show();
	var searchURL ='<?php echo base_url().'diabetes_care/GetHospitals/';?>' +zone_name;
	$.get(searchURL, function(data) { $('#hospital_sec').html(data); $('#zone_ajax_img').hide(); });
}

 setTimeout( function(){ $('.date-field').show();  } , 5000);
//**************Add New Services code*********************//
function GenerateReport()
{
	var documentation 	   = document.getElementsByName("documentation"); 
	var documentation_type = document.getElementsByName("documentation_type");
	var report_type 	   = document.getElementsByName("report_type");
	var patient_documentation = 1;
	var patient_documentation_type = 3;
	var view_report_type = '';
	if (documentation[1].checked == true) {
		if($("#zone_name").val()=='') { $("#zone_name").css("background", "#FFC0CB");$("#zone_name").css("border", "1px solid #FF0000"); return false; }	
		if($("#hospital_name").val()=='') { $("#hospital_name").css("background", "#FFC0CB");$("#hospital_name").css("border", "1px solid #FF0000");return false; }
	    patient_documentation = 2;
		if (documentation_type[1].checked == true) { patient_documentation_type = 4; }
		if (report_type[0].checked == true) { view_report_type = 5; }
		if (report_type[1].checked == true) { view_report_type = 6; }
		if (report_type[2].checked == true) { view_report_type = 7; }
	} 
	
	start_date = $("#start_date").val();
	end_date = $("#end_date").val();
	//alert(start_date);alert(end_date);
	if(start_date==''){ alert('<?php echo lang('start_date_validation'); ?>'); $("#start_date").focus(); return false; }
	if(end_date=='')  { alert('<?php echo lang('end_date_validation'); ?>'); $("#end_date").focus();  return false; }
	if(start_date > end_date)  { alert('<?php echo lang('invalid_date_selection'); ?>'); $("#end_date").focus();  return false; }

	
	
}

//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('.document_sec').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        //alert('Printing...'); thanks alex
        wait(1);
        document.body.removeChild(iframe);		
    }	
	$("#item_table").click(function(){ $('.document_sec').show(); });

</script>


<?php $this->load->view("partial/footer"); ?>