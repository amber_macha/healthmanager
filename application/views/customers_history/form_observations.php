<style>
fieldset div.field_row {font-weight: normal;}
.internal_sec_title {font-weight: normal;}
</style>
<?php echo form_open('observations/save/'.$person_id.'/'.$item_id,array('id'=>'observ_form'));?>
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend> 

<div class="field_row clearfix">	
<?php echo form_label(lang("common_location"), 'status',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('observation_status',array(''=>'Select ','0'=>lang('common_opd'), '1'=>lang('common_ipd')), $item_info->observ_status,'style="width:110px"');?>	
	</div>
</div>

<center><div class="internal_sec_title"><?php echo lang('common_patient_profile');?></div></center>
<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_blood_pressure"), 'observation_blood_pressure',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_blood_pressure','id'=>'observation_blood_pressure','value'=>$item_info->observ_blood_pressure));?></div>
</div>

<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_pulse_heart_rate"), 'observation_pulse_heart_rate',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_pulse_heart_rate','id'=>'observation_pulse_heart_rate','value'=>$item_info->observ_pulse_heart_rate));?></div>
</div>

<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_respiratory_rate"), 'observation_respiratory_rate',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_respiratory_rate','id'=>'observation_respiratory_rate','value'=>$item_info->observ_respiratory_rate));?></div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang("common_temperature"), 'observation_temperature',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_temperature','id'=>'observation_temperature','value'=>$item_info->observ_temperature));?></div>
</div>

<center><div class="internal_sec_title"><?php echo lang('common_input');?></div></center>
<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_iv"), 'observation_iv',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_iv','id'=>'observation_iv','value'=>$item_info->observ_iv));?></div>
</div>

<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_oral"), 'observation_oral',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_oral','id'=>'observation_oral','value'=>$item_info->observ_oral));?></div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang("common_ngt"), 'observation_ngt',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_ngt','id'=>'observation_ngt','value'=>$item_info->observ_ngt));?></div>
</div>

<center><div class="internal_sec_title"><?php echo lang('common_output');?></div></center>
<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_vomiting"), 'observation_vomiting',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_vomiting','id'=>'observation_vomiting','value'=>$item_info->observ_vomiting));?></div>
</div>

<div class="field_row clearfix" style="border-bottom: 0px solid #dddddd;">	
<?php echo form_label(lang("common_urine"), 'observation_urine',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_urine','id'=>'observation_urine','value'=>$item_info->observ_urine));?></div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang("common_stool"), 'observation_stool',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_stool','id'=>'observation_stool','value'=>$item_info->observ_stool));?></div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang("common_treatment"), 'observation_treatment',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_treatment','id'=>'observation_treatment','value'=>$item_info->observ_treatment));?></div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang("common_remarks"), 'observation_remarks',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_input(array('name'=>'observation_remarks','id'=>'observation_remarks','value'=>$item_info->observ_remarks));?></div>
</div>


<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right')); ?>
</fieldset>
<?php echo form_close();?>

<script type='text/javascript'>
function showThem(type)
{ 
	if(type=='viewthem'){ $('#physical_examination').show(); }
	if(type=='hidethem'){ $('#physical_examination').hide(); }
}

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#observ_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				tb_remove();
				post_person_form_submit(response);
				submitting = false;
			},
			dataType:'json'
		});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			observation_status: "required",			
   		},
		messages: 
		{     
     		observation_status: <?php echo json_encode(lang('history_observation_status_required')); ?>,			
		}
	});

});

</script>