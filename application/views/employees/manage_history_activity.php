<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
$(document).ready(function() 
{   $('#search_start_date').datePicker({startDate: '01-01-2000'}); 
    $('#search_end_date').datePicker({startDate: '01-01-2000'}); 
	var table_columns = ['activity_type','activity_type_category','added_date','activity_description'];
	enable_sorting("<?php echo site_url("$controller_name/sorting_activity/$employee_id"); ?>",table_columns, <?php echo $per_page; ?>);
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$method_type");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_delete(<?php echo json_encode(lang($controller_name."_confirm_delete"));?>,<?php echo json_encode(lang($controller_name."_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.person_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}

function SendRequestTo_Cashier(request_type)
{ 
	var checkedval='';
	$(':checkbox:checked').each(function(i){ if(checkedval=='') { checkedval = $(this).val(); } else {  checkedval = checkedval + '-' + $(this).val();  }  });
	if(checkedval==''){ alert('<?php echo lang('common_none_selected');?>');return false; }  
	if(request_type=='customers_ipd') { var confirm_message = '<?php echo lang('common_confirm_cashier_send_request_status_for_ipd');?>';}
	else { var confirm_message = '<?php echo lang('common_confirm_cashier_send_request_status');?>';}
	if (confirm(confirm_message)) 
	 {
	 	$.ajax({
            url: '<?php echo base_url()."settings/SendRequestToCashier_FromCustomers";?>',
            type: 'post',
            data: 'items_id=' + checkedval + '&category_type=' + request_type,
			dataType:'json',
            success: function(response) 
			{ 
				if(!response.success)
				{
					set_feedback(response.message,'error_message',true); 
					return false;
				}
				
				set_feedback(response.message,'success_message',false);
			}
        });   
	 }
     else {return false;}	 
}


function Toshow_AllSend_IPD_admitted()
{ 
   var total_admittied = '<?php echo $this->functions->getTotal_CashierRequest($RequestType='customers_admission_ipd');?>'; 
   if(total_admittied=='00') { alert('<?php echo lang('common_no_persons_to_display');?>'); return false;}
   //$('#method_type').val('IPD_ADMITTED_PATIENT');
   $('#spinner').show();
	$.ajax({
            url: '<?php echo base_url()."customers/get_List_IPD_Admitted_Patient";?>',
            type: 'post',
			dataType:'json',
            success: function(response) 
			{   $('#spinner').hide();
				$('#customers_ipd_sec').show();
				$('#customers_opd_sec').hide();
				$('#filter_patient_sec').hide();
				$('#table_holder').html(response.manage_table);	
				$('#pagination').html(response.pagination);				
			}
        });  
}

function search_activity()
{  var start_date = $('#search_start_date').val();
   var end_date   = $('#search_end_date').val();
   $('#spinner').show();
  $.ajax({
            url: '<?php echo base_url()."employees/search_activities/search_by_date";?>',
            type: 'post',
			data: 'search_start_date=' + start_date + '&search_end_date=' + end_date,
			dataType:'json',
            success: function(response) 
			{   $('#spinner').hide(); $('#search_start_date').val('');$('#search_end_date').val('');				
				$('#table_holder').html(response.manage_table);	
				$('#pagination').html(response.pagination);				
			}
        });  

}


//**************Add New Services code*********************//
jQuery.jPrintArea= function(el)
    { $('.leftmost').hide(); 
        var iframe=document.createElement('IFRAME');
        var doc=null;
        $(iframe).attr('style','position:absolute;width:0px;height:0px;left:-500px;top:-500px;font-size:12px;');
        document.body.appendChild(iframe);
        doc=iframe.contentWindow.document;
        var links=window.document.getElementsByTagName('link');
        for(var i=0;i<links.length;i++)
            if(links[i].rel.toLowerCase()=='stylesheet')
                doc.write('<link type="text/css" rel="stylesheet" href="'+links[i].href+'"></link>');
        doc.write('<div class="'+$(el).attr("class")+'">'+$(el).html()+'</div>');
        doc.close();
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        wait(1);
        document.body.removeChild(iframe);	
    }	
	$("#content_area").click(function(){ $('.leftmost').show(); }); 
	
</script>

<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.lang('employees_employee_history'); ?>
		</td>
		<td id="filter_patient_sec">
		<?php //echo form_open("$controller_name/history_activity/$employee_id",array('id'=>'search_form')); ?>
		   <input type="text" value="" name="search_start_date" id="search_start_date" style="width:110px;border: 1px solid #cccccc; line-height: 20px; padding: 2px;">
		   <input type="text" value="" name="search_end_date" id="search_end_date" style="width:110px;border: 1px solid #cccccc; line-height: 20px; padding: 2px;">
		   <input class="send_request_opd" type="button" style="background-color: #1da2e0;border: 1px solid #1da2e0;font-size: 14px;padding:0px;cursor:pointer;float:right;margin-right:10px;width:auto;" value="Search" onclick="search_activity()">
		</td>				
		<td id="title_search">
			<?php echo form_open("$controller_name/search_activities",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td>
			<div id="commands">				
				<a class="none import" href="<?php echo current_url(). '#'; ?>" id="printout" onclick="$.jPrintArea('#table_holder');"><?php echo lang("print_out");?></a>
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) {?>
					<?php echo anchor("$controller_name/delete",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); ?>
				<?php } ?>				
			</div>
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
			<div id="table_holder">
			<?php echo $manage_table; ?>
			</div>
			</div>
			<div id="pagination">
				<?php echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>