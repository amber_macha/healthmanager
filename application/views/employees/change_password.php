<?php $this->load->view('partial/header.php'); ?>
<style type="text/css">
.error { float: none !important; }
.change_pass{width: 665px; margin: auto;}
.company_logo_image { width: 150px;}
fieldset input[type="submit"] {margin-left:198px;}
#content_area_wrapper{min-height: 470px;}
.pass_error{color:red;margin-left: 189px;font-weight:normal;} 
.loadmasksec { left: 41%; top: 60%;}
.password_notice{color:red;text-align: center;font-weight:normal;} 
</style>
<div class="change_pass">
<?php echo form_open('employees/change_password', array('id'=>'change_pass','onsubmit'=>'return change_password();'));?>
<fieldset id="item_basic_info">
<legend><?php echo lang("employees_change_password_form"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('employees_current_password').':','current_password',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_password(array('name'=>'current_password','id'=>'current_password','onclick'=>'clicktohide(this);'));?></div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('employees_password').':','password',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array('name'=>'user_password','id'=>'user_password','onclick'=>'clicktohide(this);'));?>
	  <div class="pass_error" id="pass_error"></div>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('employees_confirm_password').':','confirm_password',array('class'=>'wide')); ?>
	<div class='form_field'><?php echo form_password(array('name'=>'user_confirm_password','id'=>'user_confirm_password','onclick'=>'clicktohide(this);'));?></div>
</div>

<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('employees_change_password'),'class'=>'submit_button float_right'));?>
<div class='password_notice'><?php echo $this->Employee->password_expire_message();?></div>
</fieldset>
<?php echo form_close();?>
</div>

<div id="feedback_bar"></div>
<div class="loadmask-msg loadmasksec" id="loadmasksec"><div>Please wait...</div></div>

<?php $this->load->view('partial/footer.php'); ?>

<script type='text/javascript'>
function clicktohide(obj)
{	
	document.getElementById(obj.id).style.background = "";	
	document.getElementById(obj.id).style.border = "";
	$("#pass_error").html(''); 
}

function change_password()
{   var user_password    = $("#user_password").val(); 
	var confirm_password = $("#user_confirm_password").val();    
	if($("#current_password").val()=='') { $("#current_password").css("background","#f2dede");$("#current_password").css("border","1px solid #FF0000"); return false; }
	if($("#user_password").val()==''){ $("#user_password").css("background","#f2dede");$("#user_password").css("border","1px solid #FF0000"); return false; }
	if(!/\d/.test(user_password)) 	 { $("#pass_error").html("<?php echo lang('employees_password_contain_digit');?>"); return false;}
	if(!/[a-z]/.test(user_password)) { $("#pass_error").html("<?php echo lang('employees_password_contain_lower_case');?>"); return false;}
	if(!/[A-Z]/.test(user_password)) { $("#pass_error").html("<?php echo lang('employees_password_contain_upper_case');?>"); return false;}
	if(user_password.length<8) {  $("#pass_error").html("<?php echo lang('employees_password_minlength');?>"); return false; }
	if(!/[^0-9a-zA-Z]/.test(user_password)) { $("#pass_error").html("<?php echo lang('employees_password_digit_limit');?>"); return false;}	
	if(user_password!='' || confirm_password!='')
	{	
		if(user_password!=confirm_password)
		{
			$("#user_password").css("background","#f2dede");$("#user_password").css("border","1px solid #FF0000");
			$("#user_confirm_password").css("background","#f2dede");$("#user_confirm_password").css("border","1px solid #FF0000");
			return false; 						
		}
	}		
	$('#loadmasksec').show(); 
	$.ajax({
			url: "<?php echo base_url();?>employees/password_setting",
			type: 'post',
			data: $("#change_pass").serialize(),
			dataType:'json',
			success: function(response)
			{  $('#loadmasksec').hide();
				$("#current_password").val('');$("#user_password").val('');$("#user_confirm_password").val('');
			   if(!response.success)
				{
				  set_feedback(response.message,'error_message',true);
				  return false;
				}
				else{ set_feedback(response.message,'success_message',false); $('.password_notice').html(response.expire_message);}
			  }
		});
		return false;
}
	
</script>