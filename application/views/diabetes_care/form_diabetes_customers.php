<div id="error_message_box" style="font-weight:bold;"></div>
<div id="account_search_sec">
	<form  action="" id="GetCustData" accept-charset="utf-8" method="post" onsubmit="return get_AccountData();" > 
	<input type="hidden" name="method_type" value="<?=$method_type;?>">
	<fieldset id="item_basic_info">
	<legend><?php echo lang("diabetes_patients_registration_form_title"); ?></legend>

	<div class="field_row clearfix">
	<?php echo form_label(lang('diabetes_registration_textbox_title').':', 'name',array('class'=>'wide','style'=>'margin-left: 95px; width: 70%;')); ?><br>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'account_no',
			'id'=>'account_no',
			'value'=>''
			,'style'=>'margin-left: 179px;' ,'onclick'=>'clicktohide(this);')
		);?>
		</div>
	</div>

	<?php echo form_submit(array('name'=>'submit','id'=>'submit','value'=>lang('common_submit'),'class'=>'submit_button float_right','style'=>'margin-left: 225px;')); ?>
	</fieldset>
	<?php echo form_close();?>
</div>

<div id="viewpatient_data"></div>

<script type='text/javascript'>
function clicktohide(obj) { document.getElementById(obj.id).style.background = ""; 	document.getElementById(obj.id).style.border = ""; }

function get_AccountData()
{ 
	$('#error_message_box').html('');
	$('#viewpatient_data').html('');
	if(document.getElementById('account_no').value=='')
	{  
		$("#account_no").css("background", "#f2dede");$("#account_no").css("border", "1px solid #FF0000");	return false; 
	}
	 var account_no = $('#account_no').val();
	 if(account_no!='')
	 {	
		var searchURL ='<?php echo base_url().'savediabetescare/getCustomer_PatientData';?>';
		$.ajax({
			url: searchURL,
			type: 'post',
			data: 'account_no=' + account_no,				
			success: function(response) 
			{  
				//if(response=='failed'){  $('#error_message_box').html('<?php echo lang('diabetes_invalid_account_no');?>');return false;}
				//if(response=='alreadyexists'){  $('#error_message_box').html('<?php echo lang('diabetes_already_added');?>');return false;}	
				if(response.length < '14'){  $('#error_message_box').html('<?php echo lang('diabetes_invalid_account_no');?>');return false;}
				if(response.length > '14' && response.length< '100'){  $('#error_message_box').html('<?php echo lang('diabetes_already_added');?>');return false;}						
				else{ $('#account_search_sec').hide(); $('#viewpatient_data').html(response); return false;}				   
			   //tb_remove();
				//post_item_form_submit(response);
			}
		});   
	 }
	 return false; 
}



</script>