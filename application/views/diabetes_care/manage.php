<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
function Change_Status(id)
{ 
	 var searchURL ='<?php echo base_url().$controller_name."/chanage_status";?>';
	 
	if (confirm('<?php echo lang('common_confirm_change_status');?>')) 
	 {
	 	$.ajax({
            url: searchURL,
            type: 'post',
            data: 'items_id=' + id + '&method_type=<?php echo $method_type;?>' ,
			dataType:'json',
            success: function(response) 
			{ 
			    if(!response.success)
				{
					set_feedback(response.message,'error_message',true);
				}
				
				do_search(true,function()
				{
					set_feedback(response.message,'success_message',false);
				});
			}
        });   
	 }
     else {return false;}	 
}

$(document).ready(function() 
{ 
	var table_columns = ['','last_name','first_name','email','phone_number'];
	enable_sorting("<?php echo site_url("$controller_name/sorting"); ?>",table_columns, <?php echo $per_page; ?>);
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest/$method_type");?>',<?php echo json_encode(lang("common_confirm_search"));?>);
    enable_delete(<?php echo json_encode(lang("diabetes_confirm_delete"));?>,<?php echo json_encode(lang("diabetes_none_selected"));?>);
	enable_cleanup(<?php echo json_encode(lang("customers_confirm_cleanup"));?>);
}); 


function post_person_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);	
	}
	else
	{
		//This is an update, just update one row
		if(jQuery.inArray(response.person_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.item_id,'<?php echo site_url("$controller_name/get_row/$method_type")?>');
			set_feedback(response.message,'success_message',false);	
			
		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.person_id);
				set_feedback(response.message,'success_message',false);		
			});
		}
	}
}
</script>

<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('common_list_of').' '.$page_title; ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search/$method_type",array('id'=>'search_form')); ?>
				<input type="text" name ='search' id='search'/>
				<input type="hidden" name ='search_type' id='search_type' value="<?php echo $this->router->fetch_method();?>"/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table>
<table id="contents">
	<tr>
		<td id="commands">
			<div id="new_button">
				<?php if ($this->Employee->has_module_action_permission($controller_name, 'add_update', $this->Employee->get_logged_in_employee_info()->person_id)) 
				{ 
				  echo anchor("$controller_name/view/$method_type/-1/width~$form_width",$this->lang->line('diabetes_new'),array('class'=>'thickbox none new','title'=>$add_new_title));
				  echo anchor("$controller_name/insulin_category/-1/width~600",$this->lang->line('diabetes_insulin_brand'),array('class'=>'thickbox none new', 'title'=>$this->lang->line('diabetes_insulin_brand')));
				}	
				?>
				<?php 
				if($method_type!='insulin_treatment')
				{ echo anchor("$controller_name/excel_import/$method_type/width~$form_width",lang('common_excel_import'),array('class'=>'thickbox none import','title'=>lang('customers_import_customers_from_excel')));
				  echo anchor("$controller_name/excel_export/$method_type",lang('common_excel_export'),array('class'=>'none import'));				
				}
				if ($this->Employee->has_module_action_permission($controller_name, 'delete', $this->Employee->get_logged_in_employee_info()->person_id)) 
				 {  echo anchor("$controller_name/delete/$method_type",$this->lang->line("common_delete"),array('id'=>'delete', 'class'=>'delete_inactive')); } 			
				if($method_type!='insulin_treatment'){ echo anchor("$controller_name/cleanup",lang("customers_cleanup_old_customers"),array('id'=>'cleanup','class'=>'cleanup')); }
				?>

			</div>
		</td>
		<td style="width:10px;"></td>
<td>
 <div id="item_table">
			<div id="table_holder">
			<?php echo $manage_table; ?>
			</div>
			</div>
			<div id="pagination">
				<?php echo $pagination;?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>