<style> fieldset div.field_row { font-weight: normal;}
		fieldset div.field_row label { width: 240px;}
		.valid{width:170px;}
</style>
<div id="account_search_sec2">
<form id="item_form" name="item_form" method="post" onsubmit="return save_Data();" > 
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<fieldset id="customer_basic_info">
<legend><?php echo lang("diabetes_insulin_treatment_form_title"); ?></legend>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('diabetes_before_breakfast').':', 'before_breakfast'); ?>
		<div class='form_field'>
			<select id="before_breakfast" class="valid" name="before_breakfast">
			    <option value=""><?php echo lang('common_select_category');?></option>
			    <?php if($insulin_category->num_rows()>0){
				foreach($insulin_category->result() as $row){?>
				<option value="<?=$row->id;?>" <?php if($item_info->before_breakfast==$row->id)echo 'selected="selected"';?>><?php echo ucfirst($row->insulin_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('diabetes_before_lunch').':', 'before_lunch'); ?>
		<div class='form_field'>
			<select id="before_lunch" class="valid" name="before_lunch">
			    <option value=""><?php echo lang('common_select_category');?></option>
			    <?php if($insulin_category->num_rows()>0){
				foreach($insulin_category->result() as $row){?>
				<option value="<?=$row->id;?>" <?php if($item_info->before_lunch==$row->id)echo 'selected="selected"';?>><?php echo ucfirst($row->insulin_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('diabetes_before_dinner').':', 'before_dinner'); ?>
		<div class='form_field'>
			<select id="before_dinner" class="valid" name="before_dinner">
			    <option value=""><?php echo lang('common_select_category');?></option>
			    <?php if($insulin_category->num_rows()>0){
				foreach($insulin_category->result() as $row){?>
				<option value="<?=$row->id;?>" <?php if($item_info->before_dinner==$row->id)echo 'selected="selected"';?>><?php echo ucfirst($row->insulin_name);?></option>
				<?php }}?>
			</select>		
		</div>
	</div>
 
 <div class="field_row clearfix">	
<?php echo form_label(lang('common_comments').':', 'comments',array('class'=>'wide required')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array( 'name'=>'comments',
									'id'=>'comments',
									'rows'=>'4',
									'cols'=>'26',
									'value'=> $item_info->treatments_comments)) ;?>
	</div>
</div>

<?php  echo form_submit(array('name'=>'save','value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
<img id="customer_image01" class="form_customer_image01" src="<?php echo base_url();?>images/loading2.gif">
</fieldset>
<?php echo form_close();?>
</div>




<script type='text/javascript'>
function clicktohide(obj) { document.getElementById(obj.id).style.background = ""; 	document.getElementById(obj.id).style.border = ""; }

function save_Data()
{ 
	$('#error_message_box').html('');
	if($("#before_breakfast").val()=='' && $("#before_lunch").val()=='' && $("#before_dinner").val()=='') 
	{ 
	  alert('<?php echo lang('diabetes_insulin_treatment_category_required');?>'); return false; 
	}
	
	$('#customer_image01').show();
	$.ajax({
		url: '<?php echo base_url().'savediabetescare/save_insulin_treatment/'.$item_info->id;?>',
		type: 'post',
		data: $("#item_form").serialize(),
		dataType:'json',
		success: function(response) 
			{	
				$('#customer_image01').hide();
				tb_remove();
				post_person_form_submit(response); 			   
			}
		}); 
   return false;
}


</script>