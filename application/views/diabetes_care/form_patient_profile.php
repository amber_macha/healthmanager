<style>fieldset div.field_row label{ width:70%; margin-left: 95px;}
fieldset div.field_row div.form_field input {margin-left: 179px;}
fieldset input[type="submit"] {margin-left: 225px;}
</style>
<ul id="error_message_box"></ul>
<?php echo form_open('savediabetescare/get_Customer_Patient_Data/',array('id'=>'item_form')); ?>
<input type="hidden" name="method_type" value="<?=$method_type;?>">
<fieldset id="item_basic_info">
<legend><?php echo lang("diabetes_patients_registration_form_title"); ?></legend>

<div class="field_row clearfix">
<?php echo form_label(lang('diabetes_registration_textbox_title').':', 'name',array('class'=>'wide')); ?><br>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_no',
		'id'=>'account_no',
		'value'=>'')
	);?>
	</div>
</div>


<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>



//validation and submit handling
$(document).ready(function()
{ 
    
	var submitting = false;

	$('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask(<?php echo json_encode(lang('common_wait')); ?>);
			$(form).ajaxSubmit({
			success:function(response)
			{
				submitting = false;
				tb_remove();
				post_item_form_submit(response);
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{			
			account_no: "required",
			
   		},
		messages:
		{
			account_no:<?php echo json_encode(lang('diabetes_required_account_no')); ?>,
		}
	});
});


</script>