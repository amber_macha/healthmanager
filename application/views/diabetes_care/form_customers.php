<?php 
$action_desable='';$form_action_type ='form_input';
if(isset($method_type) && $method_type=='diabetes_customers'){ $action_desable ='disabled="disabled"';$form_action_type ='form_input_readonly';}
?>

<style> fieldset div.field_row { font-weight: normal;}
		fieldset div.field_row label { width: 240px;}
		.valid{width:170px;}
		fieldset input[type='button'] {
		padding: 10px;
		line-height: 12px;
		margin: 10px;
		font-size: 14px;
		font-weight: bold;
		padding-bottom: 10px;
		color: #FFFFFF;
		background: #1E5A96;
		border: 1px solid #EEEEEE;
}
</style>
<div id="account_search_sec2">
<form id="item_form" name="item_form" method="post" onsubmit="return save_CustomerData();" > 
<div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
<input name="account_number" id="account_number" type="hidden" value="<?php echo $person_info->account_number;?>">

<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
<?php $this->load->view("customers/form_basic_info_patient"); ?>

<div class="field_row clearfix">	
<?php echo form_label(lang('patient_type'). ':', 'patient_type',array('class'=>'required wide')); ?>
	<div class='form_field'>
			<select name="patient_type" id="patient_type" class="valid" style="width:172px;" onclick="clicktohide(this);" <?php echo $action_desable;?>>
			<option value=""><?php echo lang('common_select_type');?></option>
			<option value="0" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='0') echo 'selected';?>><?php echo lang('normal_patient'); ?></option>
			<option value="1" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='1') echo 'selected';?>><?php echo lang('private_patient'); ?></option>
			<option value="2" <?php if(isset($person_info->patient_type) && $person_info->patient_type=='2') echo 'selected';?>><?php echo lang('fast_track_patient'); ?></option>
			</select>
	</div>
</div>


<div class="field_row clearfix">	
<?php echo form_label('Ten Cell Leader:', 'leader'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'leader',
		'id'=>'leader',
		'value'=>$person_info->leader)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Religion:', 'religion'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'religion',
		'id'=>'religion',
		'value'=>$person_info->religion,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label('Next of Kin:', 'kin'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'kin',
		'id'=>'kin',
		'value'=>$person_info->kin)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Sex:', 'sex',array('class'=>'required wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('sex',array(''=>'Select ','Male'=>'Male', 'Female'=>'Female'), $person_info->sex,$action_desable);?>	
	</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label('Occupation:', 'company_name'); ?>
	<div class='form_field'>
	<?php echo $form_action_type(array(
		'name'=>'occupation',
		'id'=>'occupation',
		'value'=>$person_info->occupation,
		'onClick'=>'clicktohide(this);'
		)
	);?>
	</div>
</div>


<?php if(isset($person_info->country) && $person_info->country!='') $nationality = $person_info->nationality; else $nationality = $this->config->item('default_country'); ?>
<div class="field_row clearfix">	
<?php echo form_label('Nationality:', 'nationality'); ?>
	<div class='form_field'>
	<?php echo form_dropdown('nationality',$this->Customer->GetCountry(), $nationality,$action_desable);?>	
	</div>
</div>


	<div class="field_row clearfix">	
	<?php echo form_label(lang('customers_account_number').':', 'account_number'); ?>
		<div class='form_field'>
		<?php echo form_input_readonly(array(
			'name'=>'account_number2',
			'id'=>'account_number2',
			'value'=>$person_info->account_number)
		);		
		?>
		</div>
	</div>    

<?php  echo form_submit(array('name'=>'save','value'=>lang('common_next'),'class'=>'submit_button float_right'));?>
<img id="customer_image01" class="form_customer_image01" src="<?php echo base_url();?>images/loading2.gif">
</fieldset>
<?php echo form_close();?>
</div>

<!--***************START DIBETES CARE CUSTOMER REGISTARION FROM & FILEDS******************-->

<div id="account_search_sec3">
<form id="final_form" name="final_form" method="post" onsubmit="return update_CustomerData();" > 
  <div id="required_fields_message"><?php echo lang('common_fields_required_message'); ?></div>
  <input name="diabetes_account_number" id="diabetes_account_number" type="hidden" value="<?php echo $patient_info->account_number;?>">

<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('father_name').':', 'father_name',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'father_name',
									 'id'=>'father_name',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->father_name)	
							 );?>
		</div>
	 </div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('father_contact_no').':', 'father_contact_no'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'father_contact_no',
									 'id'=>'father_contact_no',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->father_contact_no)	
							  );?>
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('mother_name').':', 'mother_name',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'mother_name',
									 'id'=>'mother_name',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->mother_name)	
							 );?>
		</div>
	 </div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('mother_contact_no').':', 'mother_contact_no'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'mother_contact_no',
									 'id'=>'mother_contact_no',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->mother_contact_no)	
							  );?>
		</div>
	</div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('guardians_name').':', 'guardians_name'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'guardians_name',
									 'id'=>'guardians_name',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->guardians_name)	
							 );?>
		</div>
	 </div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('guardians_contact_no').':', 'guardians_contact_no'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'guardians_contact_no',
									 'id'=>'guardians_contact_no',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->guardians_contact_no)	
							  );?>
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('ethnicity').':', 'ethnicity'); ?>
		<div class='form_field'>
			<select id="ethnicity" class="valid" name="ethnicity">
			<option value=""><?php echo lang('select_ethnicity');?></option>
			</select>
		
		</div>
	</div>

<div class="field_row clearfix">
	<?php $year=''; $month=''; $day=''; 
		 if(isset($patient_info->dateOfBirth) && $patient_info->dateOfBirth!='')
		  {
			$birth_array  = explode('-',$patient_info->dateOfBirth);
			$year = $birth_array[0]; $month = $birth_array[1]; $day = $birth_array[2];
		  }
	?>
<?php echo form_label(lang('birth_date').':', 'birth_date',array('class'=>'required wide','style'=>'width: 120px;')); ?>
	<div class='form_field'>	
		<?php echo lang('day');?> <select id="age_day" class="valid" name="age_day" style="width:76px;" onClick="clicktohide(this);">
									  <option value=""><?php echo lang('day');?></option>
									  <?php foreach($days as $key=>$value){?>
									  <option value="<?php echo $key;?>" <?php if($key==$day)echo 'selected'?>><?php echo $value;?></option>
									  <?php }?>
								 </select>
	   <?php echo lang('month');?>  <select id="age_month" class="valid" name="age_month" style="width:100px;" onClick="clicktohide(this);">
										<option value=""><?php echo lang('month');?></option>
										<?php foreach($months as $key=>$value){?>
										 <option value="<?php echo $key;?>" <?php if($key==$month)echo 'selected'?>><?php echo $value;?></option>
										<?php }?>
									</select>
			
	  <?php echo lang('year');?> <select id="age_year" class="valid" name="age_year" style="width:76px;" onClick="clicktohide(this);">
									<option value=""><?php echo lang('year');?></option>
									<?php for($k=date('Y');$k > date('Y')-150;$k--){?>
									<option value="<?php echo $k;?>"<?php if($k==$year)echo 'selected'?>><?php echo $k;?></option>
									<?php }?>
								</select>
	</div>
</div>

	<div class="field_row clearfix">	
		<?php echo form_label(lang('year_diagnosis_of_diabetes').':', 'year_diagnosis_of_diabetes'); ?>
		<div class='form_field'><select id="year_diagnosis_of_diabetes" class="valid" name="year_diagnosis_of_diabetes">
									<option value=""><?php echo lang('year');?></option>
									<?php for($k=date('Y');$k > date('Y')-150;$k--){?>
									<option value="<?php echo $k;?>"<?php if($k==$patient_info->year_diagnosis_of_diabetes)echo 'selected'?>><?php echo $k;?></option>
									<?php }?>
								</select>
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('year_diagnosis_of_hypertension').':', 'ethnicity'); ?>
		<div class='form_field'> <select id="year_diagnosis_of_hypertension" class="valid" name="year_diagnosis_of_hypertension">
									<option value=""><?php echo lang('year');?></option>
									<?php for($k=date('Y');$k > date('Y')-150;$k--){?>
									<option value="<?php echo $k;?>"<?php if($k==$patient_info->year_diagnosis_of_hypertension)echo 'selected'?>><?php echo $k;?></option>
									<?php }?>
								 </select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('year_diagnosis_of_abnormal_lipids').':', 'year_diagnosis_of_abnormal_lipids'); ?>
		<div class='form_field'> <select id="year_diagnosis_of_abnormal_lipids" class="valid" name="year_diagnosis_of_abnormal_lipids">
									<option value=""><?php echo lang('year');?></option>
									<?php for($k=date('Y');$k > date('Y')-150;$k--){?>
									<option value="<?php echo $k;?>"<?php if($k==$patient_info->year_diagnosis_of_abnormal_lipids)echo 'selected'?>><?php echo $k;?></option>
									<?php }?>
								 </select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('education_level').':', 'education_level',array('class'=>'required wide')); ?>
		<div class='form_field'>
		<?php echo $this->diabetescare->GetAll_Education_Level($patient_info->education_level, $class='valid', $name='education_level');?>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('house_mud_wattle_grass_thatched').':', 'house_mud_wattle_grass_thatched'); ?>
		<div class='form_field'>
			<select id="house_mud_wattle_grass_thatched" class="valid" name="house_mud_wattle_grass_thatched">
				<option value=""><?=lang('option_slecet_level');?></option>
				<option value="1" <?php if($patient_info->house_mud_wattle_grass_thatched=='1')echo 'selected'?>><?=lang('diabetes_yes');?></option>
				<option value="0" <?php if($patient_info->house_mud_wattle_grass_thatched=='0')echo 'selected'?>><?=lang('diabetes_no');?></option>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('house_with_electricity_solar_powe_supply').':', 'house_with_electricity_solar_powe_supply'); ?>
		<div class='form_field'>
			<select id="house_with_electricity_solar_powe_supply" class="valid" name="house_with_electricity_solar_powe_supply">
				<option value=""><?=lang('option_slecet_level');?></option>
				<option value="1" <?php if($patient_info->house_with_electricity_solar_powe_supply=='1')echo 'selected'?>><?=lang('diabetes_yes');?></option>
				<option value="0" <?php if($patient_info->house_with_electricity_solar_powe_supply=='0')echo 'selected'?>><?=lang('diabetes_no');?></option>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('house_with_piped_water').':', 'house_with_piped_water'); ?>
		<div class='form_field'>
			<select id="house_with_piped_water" class="valid" name="house_with_piped_water">
				<option value=""><?=lang('option_slecet_level');?></option>
				<option value="1" <?php if($patient_info->house_with_piped_water=='1')echo 'selected'?>><?=lang('diabetes_yes');?></option>
				<option value="0" <?php if($patient_info->house_with_piped_water=='0')echo 'selected'?>><?=lang('diabetes_no');?></option>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('house_with_flushing_toilet_inside_house').':', 'house_with_piped_water'); ?>
		<div class='form_field'>
			<select id="house_with_flushing_toilet_inside_house" class="valid" name="house_with_flushing_toilet_inside_house">
				<option value=""><?=lang('option_slecet_level');?></option>
				<option value="1" <?php if($patient_info->house_with_flushing_toilet_inside_house=='1')echo 'selected'?>><?=lang('diabetes_yes');?></option>
				<option value="0" <?php if($patient_info->house_with_flushing_toilet_inside_house=='0')echo 'selected'?>><?=lang('diabetes_no');?></option>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('house_with_kitchen_inside').':', 'house_with_kitchen_inside'); ?>
		<div class='form_field'>
			<select id="house_with_kitchen_inside" class="valid" name="house_with_kitchen_inside">
				<option value=""><?=lang('option_slecet_level');?></option>
				<option value="1" <?php if($patient_info->house_with_kitchen_inside=='1')echo 'selected'?>><?=lang('diabetes_yes');?></option>
				<option value="0" <?php if($patient_info->house_with_kitchen_inside=='0')echo 'selected'?>><?=lang('diabetes_no');?></option>
			</select>		
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('location_of_residence').':', 'location_of_residence',array('class'=>'required wide')); ?>
		<div class='form_field'>
			<?php echo $this->diabetescare->GetAll_Residence_Location($patient_info->location_of_residence, $class='valid', $name='location_of_residence');?>
		</div>
	</div>
	
	<div class="field_row clearfix">	
		<?php echo form_label(lang('number_of_relatives_with_diabetes').':', 'number_of_relatives_with_diabetes'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'number_of_relatives_with_diabetes',
									 'id'=>'number_of_relatives_with_diabetes',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->number_of_relatives_with_diabetes)	
							  );?>
		</div>
	</div>
	<div class="field_row clearfix">	
		<?php echo form_label(lang('number_of_relatives_with_hypertension').':', 'number_of_relatives_with_hypertension'); ?>
		<div class='form_field'>
		<?php echo form_input( array('name'=>'number_of_relatives_with_hypertension',
									 'id'=>'number_of_relatives_with_hypertension',
									 'onClick'=>'clicktohide(this);',
									 'value'=>$patient_info->number_of_relatives_with_hypertension)	
							  );?>
		</div>
	</div>
	
	
	
<?php echo form_submit(array(	'name'=>'save',	'value'=>lang('common_submit'),'class'=>'submit_button float_right'));?>
<input class="submit_button float_right" type="button" value="<?php echo lang('common_back');?>" name="save" onclick="return_CustomerForm()">

<img id="customer_image02" class="form_customer_image02" src="<?php echo base_url();?>images/loading2.gif">

</fieldset><?php echo form_close();?>
</div>

<!--***************START DIBETES CARE CUSTOMER REGISTARION FROM & FILEDS******************-->

<script type='text/javascript'>
function clicktohide(obj) { document.getElementById(obj.id).style.background = ""; 	document.getElementById(obj.id).style.border = ""; }
function return_CustomerForm()
{
	$('#account_search_sec3').hide();
	$('#account_search_sec2').show();
	return false;	
}

function save_CustomerData()
{ 
	$('#error_message_box').html('');
	if($("#first_name").val()=='')	 { $("#first_name").css("background", "#f2dede");$("#first_name").css("border", "1px solid #FF0000");return false; }
	if($("#last_name").val()=='')	 { $("#last_name").css("background", "#f2dede");$("#last_name").css("border", "1px solid #FF0000");return false; }
	if($("#patient_type").val()=='')	 { $("#patient_type").css("background", "#f2dede");$("#patient_type").css("border", "1px solid #FF0000");return false; }
	if($("#sex").val()=='') 	 { $("#sex").css("background", "#f2dede");$("#sex").css("border", "1px solid #FF0000");return false; }
	$('#customer_image01').show();
	$.ajax({
		url: '<?php echo base_url().'savediabetescare/save_Diabetes_CustomerData_Step1/'.$person_info->person_id;?>',
		type: 'post',
		data: $("#item_form").serialize(),
		dataType:'json',
		success: function(response) 
			{	
			    $('#customer_image01').hide();
                 $('#error_message_box').show();			
				if(response.success==false){   $('#error_message_box').html(response.message);return false;}
				else
				{  
					$('#account_search_sec2').hide();
					$('#account_search_sec3').show();	
					post_person_form_submit(response);
					
				}			   
			}
		}); 
   return false;
}

function update_CustomerData()
{ 
	$('#error_message_box').html('');
	if($("#father_name").val()=='')	 { $("#father_name").css("background", "#f2dede");$("#father_name").css("border", "1px solid #FF0000");return false; }
	if($("#mother_name").val()=='')	 { $("#mother_name").css("background", "#f2dede");$("#mother_name").css("border", "1px solid #FF0000");return false; }
	if($("#age_day").val()=='')	 	 { $("#age_day").css("background", "#f2dede");$("#age_day").css("border", "1px solid #FF0000");return false; }
	if($("#age_month").val()=='')	 { $("#age_month").css("background", "#f2dede");$("#age_month").css("border", "1px solid #FF0000");return false; }
	if($("#age_year").val()=='')	 { $("#age_year").css("background", "#f2dede");$("#age_year").css("border", "1px solid #FF0000");return false; }		
	if($("#education_level").val()=='') { $("#education_level").css("background", "#f2dede");$("#education_level").css("border", "1px solid #FF0000");return false; }
	if($("#location_of_residence").val()=='') { $("#location_of_residence").css("background", "#f2dede");$("#location_of_residence").css("border", "1px solid #FF0000");return false; }
	$('#customer_image02').show();
	$.ajax({
		url: '<?php echo base_url().'savediabetescare/save_Diabetes_CustomerData_Step2/'.$person_info->person_id;?>',
		type: 'post',
		data: $("#final_form").serialize(),
		dataType:'json',
		success: function(response) 
			{
			  $('#error_message_box').show();
			  $('#customer_image02').hide();
			  $('#error_message_box').html(response.message);
			  if(response.success==false){ return false;}
			  else
				{   tb_remove();
					post_person_form_submit(response);					
				}	
			}
		}); 
	return false;
}

</script>