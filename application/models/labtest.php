<?php
class Labtest extends Person
{	
	function valid_Lab_Test($customer_id, $lab_category)
	{
	   $lab_investigation = $this->db->dbprefix('lab_investigation');	
	    $SearchQuery = '';
		if(count($lab_category)>0)
		{
			for($i=0; $i<count($lab_category); $i++)
			{
			   if($SearchQuery=='') { $SearchQuery = "and ( lab_category_id ='".$lab_category[$i]."' "; }  else { $SearchQuery.= " OR lab_category_id ='".$lab_category[$i]."' "; }			}
			   if($SearchQuery!='') { $SearchQuery.=' )'; }
		}	   	   
        $query = "select * from  $lab_investigation  where person_id='".$customer_id."' $SearchQuery and added_date like '".date('Y-m-d')."%'  " ;
		$res = $this->db->query($query);	//echo $res->num_rows();	die;	
		if($res->num_rows()>0) return true; else return false;		
    }	
	function get_all($customer_id = NULL , $from_date = NULL , $to_date = NULL)
	{	$searchQuery ='';
     	if($from_date!='' && $to_date){ $searchQuery = " and c.added_date >= '".$from_date."' and c.added_date <= '".$to_date."' "; }
		$LabTest = $this->db->dbprefix('lab_investigation');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');	
	    $data = $this->db->query("select l.name,l.items_generic_code,l.unit_nhif_price,l.nhif_code_number,e.first_name,e.last_name,c.*  from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) where l.deleted='0' and c.person_id = '".$customer_id."' $searchQuery order by c.added_date asc"); 		return $data;
    }		
	function search($customer_id,$search, $column='added_date',$orderby='desc') 
	{
	  $searchquery='';
	   if($search!=''){ $searchquery = " and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%'  || CONCAT(e.first_name,' ',e.last_name) LIKE '%".$search."%' || l.name like '%".$search."%' ) "; }	   if($column==''){ $column=' c.added_date '; }   if($orderby==''){ $orderby='desc'; }	   $LabTest = $this->db->dbprefix('lab_investigation');
	   $employees = $this->db->dbprefix('people');
	   $lab_category = $this->db->dbprefix('items');
	   $query = "select e.first_name,e.last_name,l.name,l.item_status, c.* from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) where l.deleted='0' and c.person_id = '".$customer_id."' $searchquery order by $column $orderby "; 	   $data=$this->db->query($query);
       $commentData='';
       $commentData.='<tr style="cursor: pointer;">
						<th><input type="checkbox" id="select_all"></th>
						<th>'.lang('lab_investigation_list').'</th>
						<th>'.lang('employee_name').'</th>';
	  if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
	  {				
		 $commentData.='<th>'.lang('common_send_to_cashier').'</th>';
	  }	
	     $commentData.='<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						</tr>';
           foreach($data->result() as $data )
		   {    $bgdisabled=''; if($data->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
				$exist_request = $this->functions->existCashierRequest($customer_id,$category_type='investigation',$data->id);
				$commentData .=	'<tr '.$bgdisabled.'>
								 <td width="5%" '.$bgdisabled.'><input type="checkbox" value="'.$data->id.'" id="person_'.$data->id.'"></td>								 <td width="33%" '.$bgdisabled.'>'.ucwords(strtolower($data->name)).'</td>								 <td width="18%" '.$bgdisabled.'>'.ucwords(strtolower($data->first_name)).'&nbsp;'.ucwords(strtolower($data->last_name)).'</td>';						      if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
	          {	
				if($exist_request) { $commentData .= '<td width="20%" '.$bgdisabled.'><a href="javascript:;"><span class="send_not_request">'.lang('common_send_to_cashier').'</span></a></td>'; }
				else { $commentData.= '<td width="20%" '.$bgdisabled.'><a href="javascript:;" onclick="SendRequestTo_Cashier('.$data->id.')"><span class="send_request">'.lang('common_send_to_cashier').'</span></a></td>'; }	
			  }	
				if($data->status==0){ $commentData.='<td width="7%" '.$bgdisabled.'><a class="thickbox" title="'.lang('add_report_form').'" href="investigation/addreport/'.$data->person_id.'/'.$data->id.'/width~550">'.lang('add_post_report').'</a></td>'; }				else { $commentData.='<td width="7%" '.$bgdisabled.'><span style="background-color: #FF0000;">'.lang('add_post_report').'</span></td>';}				$commentData .='<td width="7%" '.$bgdisabled.'><a class="thickbox" title="'.lang('add_view_report_form').'" href="investigation/viewreport/'.$data->person_id.'/'.$data->id.'/width~700">'.lang('add_view_report').'</a></td>							   <td width="12%" '.$bgdisabled.'>'.date('d/m/Y',strtotime($data->added_date)).'</td>							   </tr>';
				   }
             $commentData.='';
             return $commentData; 	
	}	    
	function SearchLabCatgory( $string = NULL ,$value = NULL,$customer_id = NULL)
	{ 
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';$commentData='';
		$this->load->model('medicinetest');
		$catgoryType= " and item_type='1' and service_category='1' and deleted='0'";
		if($value!='') { 	$test2 = explode('-',$value); 
		$tempArraySelected = implode(',',$test2); $valid =  " and item_id NOT in ($tempArraySelected) ";
        $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id in ($tempArraySelected) $catgoryType ");
			if($ExecuteData->num_rows()>0)
			{  
				foreach($ExecuteData->result() as $result)  
				{ 
				  $lab_id= $result->item_id; $class_name='clarification_green'; 
				  $dosage_description = $this->medicinetest->get_medicine_drugs_dosage($result->item_id,$customer_id);
				  if(isset($dosage_description) && $dosage_description!=''){ $class_name='clarification_red'; }
				  $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
				  $SearchData_selected.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.' 
										 <div class="labcategory_checkbox">
											<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" checked="checked" name="lab_category[]" value="'.$result->item_id.'"> </span>
											<input type="button" style="width:105px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="GetItemsDesciption('.$result->item_id.')" value="'.strtoupper(lang("items_clarification")).'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 
										 </div>
									   </div>';
				}	
			}	
		} 
	    $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid and name LIKE '".$this->db->escape_like_str($string)."%' order by name ");			
		if($ExecuteData->num_rows()>0)
		 { $ids =array(); 
		   foreach($ExecuteData->result() as $result)  
		   { $ids[] = $result->item_id; $class_name='clarification_green'; 
			 $dosage_description = $this->medicinetest->get_medicine_drugs_dosage($result->item_id,$customer_id);
			 if(isset($dosage_description) && $dosage_description!=''){ $class_name='clarification_red'; }
			 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 			 
             $SearchData.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.' 
								<div class="labcategory_checkbox">
									<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> </span>
								   <input type="button" style="width:105px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="GetItemsDesciption('.$result->item_id.')" value="'.strtoupper(lang("items_clarification")).'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 
								</div>
							</div>';
		   }
		    $SearchData.= $SearchData_selected;
		    $ids_str= implode(',',$ids); 
            if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }	
		    $ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id NOT in($ids_str)  $catgoryType order by name");
			if($ExecuteData_pend->num_rows()>0)
			       { $ids2 = array();
		            foreach($ExecuteData_pend->result() as $result)  
					  { $ids2[] = $result->item_id; $class_name='clarification_green'; 
						$dosage_description = $this->medicinetest->get_medicine_drugs_dosage($result->item_id,$customer_id);
						if(isset($dosage_description) && $dosage_description!=''){ $class_name='clarification_red'; } 
						$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 	
					    $SearchData.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.' 
									   <div class="labcategory_checkbox">
										 <span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> </span>
										 <input type="button" style="width:105px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="GetItemsDesciption('.$result->item_id.')" value="'.strtoupper(lang("items_clarification")).'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 										
									   </div>
									 </div>';
					  }
				   }
		   return $SearchData ;  
		 }		 
		 else {	 
				$ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid order by name");
				if($ExecuteData_pend->num_rows()>0)
			    { 
				 foreach($ExecuteData_pend->result() as $result) 
				  {  $class_name='clarification_green'; 
					 $dosage_description = $this->medicinetest->get_medicine_drugs_dosage($result->item_id,$customer_id);
					 if(isset($dosage_description) && $dosage_description!=''){ $class_name='clarification_red'; } 
					 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
					 $SearchData.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.' 
								  <div class="labcategory_checkbox">
									 <span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> </span>
									 <input type="button" style="width:105px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="GetItemsDesciption('.$result->item_id.')" value="'.strtoupper(lang("items_clarification")).'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 
								   </div>
								 </div>';
				  }
			   }
				return $SearchData_selected.$SearchData;  
		 }
		 return $SearchData;
	}			
	function get_Report($customer_id,$invest_id)
	{
		$LabTest = $this->db->dbprefix('lab_investigation');
		$employees = $this->db->dbprefix('people');
	    $query = "select r.id, r.test_report_details,r.tested_date,r.lab_report_images, t.first_name as technical_first_name, t.last_name as technical_last_name from $LabTest r left join $employees  t on (r.tested_by_employee_id = t.person_id) where r.id = '".$invest_id."' and r.person_id = '".$customer_id."' "; 
		$data=$this->db->query($query)->row();		
		return $data;
    }	
	function DeleteLabCatgory($value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';	  	$lab_investigation = $this->db->dbprefix('lab_category');		if($value!='') 
		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 		    $result=$this->db->query("delete from $lab_investigation where id in($tempArraySelected) ");			if($result) return true; else return  false;		}
     }
	 
	function ActiveDeActivateCatgory($value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';
		if($value!='') 
		{ 	$test2 = explode('-',$value); $tempArraySelected = implode(',',$test2);
		     $ExecuteData=$this->db->query("select * from ".$this->db->dbprefix('items')." where item_id in($tempArraySelected) ");
			 foreach($ExecuteData->result() as $result)  
			  {  if($result->item_status==0) { $status='1';}  if($result->item_status==1){  $status =0;  }
		         $sucess = $this->db->query("update ".$this->db->dbprefix('items')." set item_status='".$status."' where item_id ='".$result->item_id."' ");
			  }		 
			return true; 
		}else return  false;
     }
	function import_excel($data1,$data2)
	{
		$success=false;  $catgoryType= " and category_type='investigation'";
		$lab_investigation = $this->db->dbprefix('lab_category');
		$query = "select * from $lab_investigation where lab_name='".$data1."' ";
		$ExecuteQuery = $this->db->query($query);
		if($ExecuteQuery->num_rows()>0)	
		{  $query1 = "update $lab_investigation set lab_name='".$data1."',status ='".$data2."' where lab_name='".$data1."' $catgoryType";		   $this->db->query($query1);		}
		else{ 
			$ExeQuery = "insert into $lab_investigation set lab_name='".$data1."',status ='".$data2."',category_type='investigation' "; 
			$result = $this->db->query($ExeQuery);
		}
		return true; 
	}
	function count_all()
	{
		$this->db->from('customers');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}	
	function delete_list($ids)
	{   $history = $this->db->dbprefix('lab_investigation');
	    $ids = implode(',',$ids);
	    $query = "delete from $history  where id in ($ids) ";
        $result = $this->db->query( $query );
		return  $result;
 	}	
	/*Returns all the customers*/
	function get_exportData()
	{  
		$lab_category=$this->db->dbprefix('lab_category');
		$customers=$this->db->dbprefix('customers');
		$data=$this->db->query("SELECT * FROM $lab_category where category_type='investigation' ORDER BY lab_name asc ");
		return $data;
	}	
	function cleanup()
	{
		$customer_data = array('account_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('customers',$customer_data);
	}
		function SearchDiseasList($customer_id, $string ,$invest_id,$value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $valid=''; $SearchData_selected='';  $disabled='';$disabled2='';
		$catgoryType= " and category_type='diseases'";
	  	$lab_category = $this->db->dbprefix('lab_category');
		$disesesArray = $this->GetList_ofDiseases($customer_id,$invest_id); 
		if(isset($disesesArray) && $disesesArray!='' && count($disesesArray)>0)
		{ 	
			$array = implode(',',$disesesArray);
			if($array!='') { $queryString = " and id in ($array) ";  }
			$Execute = $this->db->query("select id,lab_name from  $lab_category where status='0' $catgoryType $queryString order by lab_name ");
			if($Execute->num_rows()>0)
			{   
				$disabled ='disabled checked="checked" ';	$disabled2 ='disabled';
				foreach($Execute->result() as $result)  
				{			 
				$SearchData .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled.'> </span></div>';
				}	
			}	
		
			$queryString = " and id NOT in ($array) ";
			$ExecuteData = $this->db->query("select id,lab_name,status from $lab_category where status='0' $catgoryType $queryString and lab_name LIKE '".$this->db->escape_like_str($string)."%' order by lab_name ");			
			if($ExecuteData->num_rows()>0)
			 { $ids =array(); 
			   foreach($ExecuteData->result() as $result)  
			   { $ids[] = $result->id; 
				 $lab_id= $result->id;	
				 $SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$lab_id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
			   }
				$SearchData.= $SearchData_selected;	
				$ids_str= implode(',',$ids); 
				if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }
				$ExecuteData_pend = $this->db->query("select id,lab_name,status from $lab_category where id NOT in($ids_str) and status='0'  $catgoryType  order by lab_name");
				if($ExecuteData_pend->num_rows()>0)
					   { $ids2 =array();
						foreach($ExecuteData_pend->result() as $result) 
						  {  $ids2[] = $result->id;  
							 $lab_id= $result->id;	
							$SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$lab_id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
						  }
					   }
			   return $SearchData;
			 }		 
        }
		 else {	 
				$ExecuteData_pend = $this->db->query("select id,lab_name,status from $lab_category where status='0' and lab_name LIKE '".$this->db->escape_like_str($string)."%' $catgoryType order by lab_name");
				if($ExecuteData_pend->num_rows()>0)
					   { $ids2 =array();
						foreach($ExecuteData_pend->result() as $result)  
						  {  $ids2[] = $result->id; 
						     $lab_id= $result->id;	
							 $SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right; "><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$result->id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
						  }
					   }
				return $SearchData_selected.$SearchData;
		 }		 
		 return $SearchData;	
	}	
	function GetList_ofDiseases($customer_id,$invest_id)	{	  	$list_diseases = $this->db->dbprefix('list_diseases');	    $ExecuteData = $this->db->query("select lab_category_id from $list_diseases where person_id='".$customer_id."' and status='0' and investigation_id='".$invest_id."' order by added_date desc");	    $array =  array();		if($ExecuteData->num_rows()>0){		   foreach($ExecuteData->result() as $result)  {    $array[] = $result->lab_category_id;  }		 		   return($array);  		   }  else	 return false;	}
	
	function SearchDiabetesList($customer_id, $string ,$invest_id,$value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $valid=''; $SearchData_selected='';  $disabled='';$disabled2='';
		$catgoryType= " and category_type='diseases'";
	  	$lab_category = $this->db->dbprefix('lab_category');
		$disesesArray = $this->GetList_Of_Diabetes($customer_id,$invest_id); 
		if(isset($disesesArray) && $disesesArray!='' && count($disesesArray)>0)
		{ 	
			$array = implode(',',$disesesArray);
			if($array!='') { $queryString = " and id in ($array) ";  }
			$Execute = $this->db->query("select id,lab_name from  $lab_category where status='0' $catgoryType $queryString order by lab_name ");
			if($Execute->num_rows()>0)
			{   
				$disabled ='disabled checked="checked" ';	$disabled2 ='disabled';
				foreach($Execute->result() as $result)  
				{			 
				$SearchData .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled.'> </span></div>';
				}	
			}	
		
			$queryString = " and id NOT in ($array) ";
			$ExecuteData = $this->db->query("select id,lab_name,status from $lab_category where status='0' $catgoryType $queryString and lab_name LIKE '".$this->db->escape_like_str($string)."%' order by lab_name ");			
			if($ExecuteData->num_rows()>0)
			 { $ids =array(); 
			   foreach($ExecuteData->result() as $result)  
			   { $ids[] = $result->id; 
				 $lab_id= $result->id;	
				 $SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$lab_id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
			   }
				$SearchData.= $SearchData_selected;	
				$ids_str= implode(',',$ids); 
				if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }
				$ExecuteData_pend = $this->db->query("select id,lab_name,status from $lab_category where id NOT in($ids_str) and status='0'  $catgoryType  order by lab_name");
				if($ExecuteData_pend->num_rows()>0)
					   { $ids2 =array();
						foreach($ExecuteData_pend->result() as $result) 
						  {  $ids2[] = $result->id;  
							 $lab_id= $result->id;	
							$SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$lab_id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
						  }
					   }
			   return $SearchData;
			 }		 
        }
		 else {	 
				$ExecuteData_pend = $this->db->query("select id,lab_name,status from $lab_category where status='0' and lab_name LIKE '".$this->db->escape_like_str($string)."%' $catgoryType order by lab_name");
				if($ExecuteData_pend->num_rows()>0)
					   { $ids2 =array();
						foreach($ExecuteData_pend->result() as $result)  
						  {  $ids2[] = $result->id; 
						     $lab_id= $result->id;	
							 $SearchData.=' <div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right; "><input class="isAgeSelected" type="radio" name="lab_category[]" value="'.$result->id.'" id="'.$lab_id.'" '.$disabled2.'> </span></div>';
						  }
					   }
				return $SearchData_selected.$SearchData;
		 }		 
		 return $SearchData;	
	}	

	function GetList_Of_Diabetes($customer_id,$invest_id)
	{
	  	$list_diabetes = $this->db->dbprefix('list_diabetes');
	    $ExecuteData = $this->db->query("select lab_category_id from $list_diabetes where person_id='".$customer_id."' and status='0' and investigation_id='".$invest_id."' order by added_date desc");
	    $array =  array();
		if($ExecuteData->num_rows()>0){
		   foreach($ExecuteData->result() as $result)  {    $array[] = $result->lab_category_id;  }		
 		   return($array);  
		   }  else	 return false;
	}	  function saveDiseases($customer_id,$invest_id,$value=NULL,$savetype=NULL)	{		$ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';		$login_uerData = $this->Employee->get_logged_in_employee_info();		$employee_id = $login_uerData->person_id;				if($savetype=='mtuha_daignoses')  {	$list_diseases = $this->db->dbprefix('list_diseases'); }
		if($savetype=='daibetes')  		  {	$list_diseases = $this->db->dbprefix('list_diabetes'); } 		if($value!='') 		{ 	$ArraySelected = explode('-',$value);    	     $sucess='';			for($k=0; $k < count($ArraySelected) ; $k++)			  {			     $ExecuteQuery = $this->db->query("select id from $list_diseases where person_id = '".$customer_id."' and lab_category_id = '".$ArraySelected[$k]."' and investigation_id='".$invest_id."' order by added_date desc limit 0,1");			     if($ExecuteQuery->num_rows()==0)				   {				     $sucess = $this->db->query("insert into $list_diseases set lab_category_id ='".$ArraySelected[$k]."', investigation_id='".$invest_id."', person_id = '".$customer_id."',added_by_employee_id='".$login_uerData->person_id."',added_date='".date('Y-m-d')."', status='0' ");				   }			   		       } 			 if($sucess) return true;  else return  false;	    }else return  false;	}			function GetSearchAllDiseasList($string = NULL,$type_view = NULL)	{           $SearchData=''; $stringQuery ='';	  	$lab_category = $this->db->dbprefix('lab_category');
		$category_type = 'diseases';
		$item_colum_name = lang("diseases_name");
		        if(isset($string) && $string!='') { $stringQuery = "and (lab_name LIKE '".$this->db->escape_like_str($string)."%' or code_number LIKE '".$this->db->escape_like_str($string)."%')"; }
		if($type_view=='clinical_daignoses') { $category_type = 'clinic_diseases';}
		if($type_view=='dead_body') { $category_type = 'dead_body';}
		if($type_view=='multi_stores') { $category_type = 'multi_stores'; $item_colum_name = lang("items_multi_store_name");} 
		//echo "select * from $lab_category where category_type='".$category_type."' $stringQuery order by lab_name "; die;
		$ExecuteData = $this->db->query("select * from $lab_category where category_type='".$category_type."' $stringQuery order by lab_name ");		 		   $SearchData.='<table id="sortable_table" class="tablesorter">						<tr><th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
						<th>'.$item_colum_name.'</th>';		  if($type_view!='multi_stores')
		  {
			  $SearchData.='<th>'.lang("diseases_code").'</th>';
		  }					   $SearchData.='<th>'.lang("toedit_diseases").'</th>						</tr>';
		if($ExecuteData->num_rows()>0)
		{						foreach($ExecuteData->result() as $result)  			{  if(isset($result->lab_name) && $result->lab_name!='') $lab_name = stripslashes($result->lab_name);else $lab_name = '_';			   if(isset($result->code_number) && $result->code_number!='') $code_number = stripslashes($result->code_number);else $code_number = '_';								 $SearchData.='<tr style="cursor: pointer;">								<td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->id.'" id="'.$result->id.'"></td>								<td width="40%">'.$lab_name.'</td>';			 if($type_view!='multi_stores') { $SearchData.='<td width="40%">'.$code_number.'</td>';}			 $SearchData.='<td width="15%"><a onclick = "updateDiagnoses('.$result->id.')" title="'.lang("edit_diseases").'" href="javascript:;">'.lang("toedit_diseases").'</a></td>							</tr>';								}
		}
		else $SearchData.='<tr style="cursor: pointer;"><td colspan="5"><div class="warning_message" style="padding:7px;">'.lang("items_no_items_to_display").'</div></td></td>';				 $SearchData.='</table>';									 		 return  $SearchData;	}			
	function GetSearchAllDiabetes_CareList($string=NULL)
	{   
        $SearchData=''; 
	  	$lab_category = $this->db->dbprefix('lab_category');
		$stringQuery ='';
        if(isset($string) && $string!=''){ $stringQuery = "and lab_name LIKE '".$this->db->escape_like_str($string)."%' "; }
		$ExecuteData = $this->db->query("select * from $lab_category where category_type='diabetes' $stringQuery order by lab_name ");
				if($ExecuteData->num_rows()>0)
				{ 
				   $SearchData.='<table id="sortable_table" class="tablesorter">
								<tr>
									<th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
									<th>'.lang("diabetes_name").'</th>
									<th>'.lang("diabetes_code").'</th>
									<th>'.lang("toedit_diabetes").'</th>
								</tr>';
					foreach($ExecuteData->result() as $result)  
					{  if(isset($result->lab_name) && $result->lab_name!='') $lab_name = stripslashes($result->lab_name);else $lab_name = '_';
					   if(isset($result->code_number) && $result->code_number!='') $code_number = stripslashes($result->code_number);else $code_number = '_';					
					 $SearchData.='<tr style="cursor: pointer;">
										<td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->id.'" id="'.$result->id.'"></td>
										<td width="40%">'.$lab_name.'</td>
										<td width="40%">'.$code_number.'</td>
										<td width="15%"><a onclick = "updateDiagnoses('.$result->id.')" title="'.lang("edit_diabetes").'" href="javascript:;">'.lang("toedit_diabetes").'</a></td>
									</tr>';					
				 	}
                     $SearchData.='</table>';					
			 	}		

		 
		 return  $SearchData;
	}	
			function existsDiagnoses($servicename, $codeNumber,$service_id = NULL,$category_type=null)	 {			    if($category_type=='multi_stores') { $chk = " and lab_name='".$servicename."' "; }
		else { $chk = " and (lab_name='".$servicename."' || code_number='".$codeNumber."') ";  }		if($category_type=='mtuha_daignoses')    { $chk .= " and category_type='diseases' "; }
		if($category_type=='clinical_daignoses') { $chk .= " and category_type='clinic_diseases' "; }
		if($category_type=='daibetes')           { $chk .= " and category_type='diabetes' "; }
		if($category_type=='dead_body')          { $chk .= " and category_type='dead_body' "; }
		if($category_type=='multi_stores') 	     { $category_type = 'multi_stores';} 
		if(isset($service_id) && $service_id!=''){ $chk .= " and id!='".$service_id."' " ; }
		    $lab_category = $this->db->dbprefix('lab_category');
	   	$ExecuteQuery = $this->db->query("select * from $lab_category where 1=1 $chk ");        if($ExecuteQuery->num_rows()>0) return ('no');        else return ('yes');    }			function GetDiseasData_BYID($id,$category_type)	{	$Related_ItemData = ''; 			$lab_category = $this->db->dbprefix('lab_category');
		if($category_type=='mtuha_daignoses')   { $search_type = " and category_type='diseases' "; }
		if($category_type=='clinical_daignoses'){ $search_type = " and category_type='clinic_diseases' "; }
		if($category_type=='daibetes')          { $search_type = " and category_type='diabetes' "; }
		if($category_type=='dead_body')         { $search_type = " and category_type='dead_body' "; }
		if($category_type=='multi_stores') 	    { $search_type = " and category_type='multi_stores' "; } 
		//if($category_type=='laboratory_specimen'){ $search_type = " and category_type='laboratory_specimen' "; }
		$ExecuteQuery = $this->db->query("select * from $lab_category where 1=1 $search_type and id='".$id."' ");		if($ExecuteQuery->num_rows()>0) 		   { $ExecuteData = $ExecuteQuery->row();			 if($category_type=='mtuha_daignoses' || $category_type=='clinical_daignoses' || $category_type=='daibetes')   
			 {
				$this->load->model('medicinetest');
				$Related_ItemData = $this->medicinetest->Search_MedicneDrugs_Catgory($search='',$ExecuteData->related_item_id);
			 }			 echo json_encode(array('success'=>true,'name'=>$ExecuteData->lab_name,'code_number'=>$ExecuteData->code_number,'Related_ItemData'=>$Related_ItemData ));  die;			}		else { echo json_encode(array('success'=>false,'name'=>'','code_number'=>'','Related_ItemData'=>''));  die;};	}			function deleteAllDiagnoses($value=NULL,$category_type=NULL)	{  		if($category_type=='mtuha_daignoses'){ $search_type = " and category_type='diseases' "; }
		if($category_type=='clinical_daignoses'){ $search_type = " and category_type='clinic_diseases' "; }
		if($category_type=='daibetes'){ $search_type = " and category_type='diabetes' "; }
		if($category_type=='dead_body'){ $search_type = " and category_type='dead_body' "; }
		if($category_type=='daibetes'){ $search_type = " and category_type='diabetes' "; }
		if($category_type=='dead_body'){ $search_type = " and category_type='dead_body' "; }
		if($category_type=='multi_stores'){ $search_type = " and category_type='multi_stores' "; }
		if($value!='') 		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 				    $result=$this->db->query("delete from ".$this->db->dbprefix('lab_category')." where id in($tempArraySelected) $search_type ");			if($result) return true; else return  false;		}     } 
	 
	     /*	Get search suggestions to find customers */
	function get_search_suggestions($customer_id=NULL,$search,$limit=25)
	{
		$suggestions = array();
     	$LabTest = $this->db->dbprefix('lab_investigation');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');	   
		$query = "select e.first_name,e.last_name from $LabTest c 
		left join $employees e on (c.added_by_employee_id = e.person_id) 
		left join $lab_category l on (c. lab_category_id = l.item_id) where c.status='0' and l.item_status='0' and l.deleted='0' and c.person_id='".$customer_id."' and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%' || CONCAT(e.first_name,' ',e.last_name) LIKE '%".$search."%' ) group by e.first_name order by e.first_name asc";
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->first_name.' '.$row->last_name); }
		
		$query = "select l.name from $LabTest c 
		left join $employees e on (c.added_by_employee_id = e.person_id) 
		left join $lab_category l on (c. lab_category_id = l.item_id) where c.status='0' and l.item_status='0' and l.deleted='0' and c.person_id='".$customer_id."' and l.name like '%".$search."%' group by l.name order by l.name asc";
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->name); }
		
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	
	function valid_Active_Lab_category($value=NULL)
	{   
		if($value!='') 
		{ 	
		    $ExecuteQuery = $this->db->query("select item_id from ".$this->db->dbprefix('items')." where item_id in($value) and item_status='1' ");
			if($ExecuteQuery->num_rows()>0) return true; else return false;
		}
		return false;
     }	
}
?>
