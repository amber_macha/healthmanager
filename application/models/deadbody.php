<?php
class Deadbody extends Person
{	
	function exists($table_name,$person_id)
	{
		$this->db->from($table_name);	
		$this->db->where('person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	function exists_by_account($account_no)
	{
		$this->db->from('customers_dead_body');	
		$this->db->where('account_number',$account_no);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	/*	Returns all the dead customers	*/
	function get_all($limit=10000, $offset=0,$col='person_id',$order='desc')
	{   $dead_body = $this->db->dbprefix('customers_dead_body');
	    $people    = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');		
		$data=$this->db->query("SELECT $dead_body.*,$customers.*,$people.first_name,$people.last_name,$people.address_2,$people.city,$people.country FROM ".$dead_body." 
								STRAIGHT_JOIN ".$customers." ON ".$customers.".person_id = ".$dead_body.".person_id
								STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$customers.".person_id
								WHERE deleted ='0' and ".$customers.".patient_type='-1' ORDER BY ".$dead_body.".".$col." ". $order." LIMIT  ".$offset.",".$limit);									
		return $data;  
	}	
		function count_all()
	{
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('deleted','0');
		$this->db->where('customers.patient_type','-1');
		return $this->db->count_all_results();
	}	
	/*	Gets information about a particular dead customer */
	function get_info($item_id)
	{
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');		
		$this->db->where('customers_dead_body.person_id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields('customers_dead_body');
			foreach ($fields as $field){$person_obj->$field='';	}
			$fields = $this->db->list_fields('customers');
			foreach ($fields as $field){$person_obj->$field='';	}
			$fields = $this->db->list_fields('people');
			foreach ($fields as $field){$person_obj->$field='';	}
			return $person_obj;
		}
	}
	/*Inserts or updates a dead customer*/
	function save($dead_body_data,$customer_data,$person_data,$customer_id=false)
	{			
		if (!$customer_id or !$this->exists($table_name='customers_dead_body',$customer_id))
		{ 
			if (!$customer_id or !$this->exists($table_name='people',$customer_id))
			{ 
				$this->db->insert('people',$person_data);
				$Lastinsert_ID = $this->db->insert_id();				
				$customer_data = array_merge($customer_data,array('person_id'=>$Lastinsert_ID,'patient_type' => '-1'));
				$this->db->insert('customers',$customer_data);	
				$dead_body_data = array_merge($dead_body_data,array('person_id'=>$Lastinsert_ID));							
				$this->db->insert('customers_dead_body',$dead_body_data);
			}
		    else{   $person_id = array('person_id'=>$customer_id);
					$dead_body_data = array_merge($dead_body_data,$person_id);
					$this->db->insert('customers_dead_body',$dead_body_data);
					$this->db->where('person_id', $customer_id);	$this->db->update('people',$person_data);
					$customer_data = array_merge($customer_data,array('patient_type' => '-1'));
					$this->db->where('person_id', $customer_id); $this->db->update('customers',$customer_data);	
					$Lastinsert_ID = $customer_id;		
			}			
		}
		else
		{
			$this->db->where('person_id', $customer_id);	$this->db->update('people',$person_data);
			$this->db->where('person_id', $customer_id);	$this->db->update('customers',$customer_data);
			$this->db->where('person_id', $customer_id);	$this->db->update('customers_dead_body',$dead_body_data);
			$Lastinsert_ID = $customer_id;
		}	
		return $Lastinsert_ID;
	}
	
	/*Inserts or updates a dead customer*/
	function save_import_data($customer_data,$account_no=-1)
	{			
		if (!$account_no or !$this->exists_by_account($account_no))
		{   
			$sucess =  $this->db->insert('customers_dead_body',$customer_data);	
		}
		else
		{
			$this->db->where('account_number', $account_no);
			$sucess = $this->db->update('customers_dead_body',$customer_data);
		}		
		return $sucess;
	}	
	/*	Deletes a list of dead customers */	function delete_list($ids)
	{   
		if(count($ids)>0)
		{  
		   $result = false;	
		   for($i=0; $i < count($ids); $i++)
		   {
		     if($ExecuteQuery->num_rows()>0)
			 {
				$query = "update ".$this->db->dbprefix('customers')." set deleted='1' where person_id ='".$ids[$i]."' ";
				$result = $this->db->query($query);
			}
		  }	
		  return $result;
		}  
		return false;
 	}
	
	function cleanup()
	{	$dead_body = $this->db->dbprefix('customers_dead_body');
	    $people    = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$ExecuteQuery = $this->db->query("SELECT $dead_body.* FROM ".$dead_body." 
								STRAIGHT_JOIN ".$customers." ON ".$customers.".person_id = ".$dead_body.".person_id
								STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$customers.".person_id
								WHERE deleted ='1' and ".$customers.".patient_type='-1' ");									
		     if($ExecuteQuery->num_rows()>0)
			 {
			    foreach($ExecuteQuery->result() as $ExecuteData)
				{
					if(isset($ExecuteData->dead_body_images) && $ExecuteData->dead_body_images!='' && $ExecuteData->dead_body_images!='0')
					{ 					  
						$profile_image_path	 	  =  'images/dead_body_document/dead_body_images/'.'thumb_'.$ExecuteData->dead_body_images;
						$profile_image_path_thumb =  'images/dead_body_document/dead_body_images/'.$ExecuteData->dead_body_images;					
						@unlink($profile_image_path); @unlink($profile_image_path_thumb);
					}				
					if(isset($ExecuteData->death_certificate_with_signature) && $ExecuteData->death_certificate_with_signature!='' && $ExecuteData->death_certificate_with_signature!='0')
					{ 
						$death_certificate_path  =  'images/dead_body_document/dead_body_certificate/'.$ExecuteData->death_certificate_with_signature;					
						@unlink($death_certificate_path);
					}				
					$result = $this->db->query("delete from ".$this->db->dbprefix('people')." where person_id ='".$ExecuteData->person_id."' ");
					$result = $this->db->query("delete from ".$this->db->dbprefix('customers')." where person_id ='".$ExecuteData->person_id."' ");
					$result = $this->db->query("delete from ".$this->db->dbprefix('customers_dead_body')." where person_id ='".$ExecuteData->person_id."' ");
				 }	
			}
		
		return true;
	}

 	/*	Get search suggestions to find customers */
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$this->db->select('customers.other_account_number');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');		
		$this->db->like("customers.other_account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers.other_account_number");
		$this->db->order_by("customers.other_account_number", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->other_account_number);
		}
		
		$this->db->select('customers.account_number');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');		
		$this->db->like("customers.account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers.account_number");
		$this->db->order_by("customers.account_number", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->account_number);
		}
		
		$this->db->select('people.first_name,people.last_name');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 and patient_type='-1' ");			
		$this->db->group_by("first_name");
		$this->db->order_by("first_name", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);
		}					
		$this->db->select('customers_dead_body.patient_tribe');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->like("patient_tribe",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("patient_tribe");
		$this->db->order_by("patient_tribe", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->patient_tribe);	
		}				$this->db->select('customers_dead_body.date_of_death');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');		$this->db->like("date_of_death",date('Y-m-d',strtotime($search)), $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("date_of_death");		$this->db->order_by("date_of_death", "asc");		$by_name = $this->db->get();		foreach($by_name->result() as $row)		{			$suggestions[] = array('label'=> date('Y-m-d',strtotime($row->date_of_death)) );			}	
		
		$this->db->select('people.address_1');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->like("people.address_1",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("people.address_1");
		$this->db->order_by("people.address_1", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->address_1);	
		}
		
		$this->db->select('customers_dead_body.reason_of_death');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');
		$this->db->like("reason_of_death",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("reason_of_death");
		$this->db->order_by("reason_of_death", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->reason_of_death);	
		}
		
		$this->db->select('customers_dead_body.car_no_brought_dead_body_in_hospital');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');
		$this->db->like("car_no_brought_dead_body_in_hospital",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("car_no_brought_dead_body_in_hospital");
		$this->db->order_by("car_no_brought_dead_body_in_hospital", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->car_no_brought_dead_body_in_hospital);	
		}
		
		$this->db->select('people.city');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->like("people.city",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("people.city");
		$this->db->order_by("people.city", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->city);	
		}
		
		$this->db->select('people.country');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->like("people.country",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("people.country");
		$this->db->order_by("people.country", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->country);	
		}
		
		$this->db->select('customers_dead_body.mobile_no_relative');
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->like("mobile_no_relative",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("mobile_no_relative");
		$this->db->order_by("mobile_no_relative", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->mobile_no_relative);	
		}
				//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}	
	function search($search, $limit=20,$offset=0,$column='person_id',$orderby='desc')
	{
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');		
		$this->db->where("( first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or 
							other_account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
						    address_2 LIKE '%".$this->db->escape_like_str($search)."%' or 									patient_tribe LIKE '%".$this->db->escape_like_str($search)."%' or 
							date_of_death LIKE '%".date('Y-m-d',strtotime($search))."%' or 
							sex LIKE '%".$this->db->escape_like_str($search)."%' or 
							reason_of_death LIKE '%".$this->db->escape_like_str($search)."%' or 							car_no_brought_dead_body_in_hospital LIKE '%".$this->db->escape_like_str($search)."%' or 							name_of_drive_relative LIKE '%".$this->db->escape_like_str($search)."%' or 							relationship LIKE '%".$this->db->escape_like_str($search)."%' or 
							city LIKE '%".$this->db->escape_like_str($search)."%' or 
							country LIKE '%".$this->db->escape_like_str($search)."%' or 							mobile_no_relative LIKE '%".$this->db->escape_like_str($search)."%'	) and deleted='0' and patient_type='-1'");	
							$this->db->order_by("customers_dead_body.$column",$orderby);
							$this->db->limit($limit);
							$this->db->offset($offset);
							return $this->db->get();			
	}	
	function search_count_all($search, $limit=10000)
	{
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');		$this->db->where("( first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or 
							other_account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
						    address_2 LIKE '%".$this->db->escape_like_str($search)."%' or 		
							patient_tribe LIKE '%".$this->db->escape_like_str($search)."%' or 
							date_of_death LIKE '%".date('Y-m-d',strtotime($search))."%' or 
							sex LIKE '%".$this->db->escape_like_str($search)."%' or 
							reason_of_death LIKE '%".$this->db->escape_like_str($search)."%' or 
							car_no_brought_dead_body_in_hospital LIKE '%".$this->db->escape_like_str($search)."%' or 
							name_of_drive_relative LIKE '%".$this->db->escape_like_str($search)."%' or 
							relationship LIKE '%".$this->db->escape_like_str($search)."%' or 
							city LIKE '%".$this->db->escape_like_str($search)."%' or 
							country LIKE '%".$this->db->escape_like_str($search)."%' or 
							mobile_no_relative LIKE '%".$this->db->escape_like_str($search)."%'	) and deleted='0' and patient_type='-1'");
									$this->db->limit($limit);		return $this->db->count_all_results();			
	}	
   function GetDeadBody_document($item_id=-1,$type=NULL)	{		$this->db->select('dead_body_images,death_certificate_with_signature');
		$this->db->from('customers_dead_body');		$this->db->where('person_id',$item_id);		$query = $this->db->get();				if($query->num_rows()==1) 
		{	
			$data = $query->row(); 
			if($type=='profile_image') { if(isset($data->dead_body_images) && $data->dead_body_images!='') return($data->dead_body_images); } 
			if($type=='profile_cerificate') { if(isset($data->death_certificate_with_signature) && $data->death_certificate_with_signature!='') return($data->death_certificate_with_signature); } 				}
		return false;    }
	
	 /*	Gets information about a particular customer account month reding */
	function Exist_Patient_Dead_Body($account_no,$item_id)
	{	
		$this->db->select('customers_dead_body.person_id');	
		$this->db->from('customers_dead_body');
		$this->db->join('customers', 'customers.person_id = customers_dead_body.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('customers.patient_type','-1');	
		$this->db->where('customers.account_number',$account_no);
		if($item_id!='-1'){$this->db->where('customers_dead_body.person_id !=',$item_id);}	
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return true;
		}
		else return false;
	} 
	
	 /*	Gets information about a particular customer account month reding */
	function Check_valid_account_no($account_no)
	{	
		$this->db->from('customers');	
		$this->db->where('account_number',$account_no);
		$this->db->where('deleted','0');
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return true;
		}
		else return false;
	}	
}
?>
