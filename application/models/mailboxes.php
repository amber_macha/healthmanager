<?php
class Mailboxes extends Person
{	
	function exists($item_id)
	{
		$this->db->from('mailbox');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function get_all($method_type,$limit=10000, $offset=0,$col='id',$order='desc')
	{   $mailbox=$this->db->dbprefix('mailbox');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		if($method_type=='inbox')
		{			
		   $ExecteQuery = "SELECT $mailbox.*, $people.first_name,$people.last_name FROM ".$mailbox."
						   STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".from_employee_id
						   STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						   WHERE ".$mailbox.".deleted ='0' and ".$employees.".deleted ='0' and ".$mailbox.".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' ORDER BY ".$mailbox.".id ".$order." LIMIT  ".$offset.",".$limit;	
		}
		if($method_type=='outbox')
		{
		   $ExecteQuery = "SELECT $mailbox.*, $people.first_name,$people.last_name FROM ".$mailbox."
						  STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".to_employee_id
						  STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						  WHERE ".$mailbox.".deleted ='0' and ".$employees.".deleted ='0' and ".$mailbox.".from_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' ORDER BY ".$mailbox.".id ".$order." LIMIT  ".$offset.",".$limit;	
		}
		$QueryData = $this->db->query($ExecteQuery);
		return $QueryData;
	}	
		function count_all($method_type)
	{
		$mailbox=$this->db->dbprefix('mailbox');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		if($method_type=='inbox')
		{			
		   $ExecteQuery = "SELECT $mailbox.id FROM ".$mailbox."
						   STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".from_employee_id
						   STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						   WHERE ".$mailbox.".deleted ='0' and ".$employees.".deleted ='0' and ".$mailbox.".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' ";	
		}
		if($method_type=='outbox')
		{
		   $ExecteQuery = "SELECT $mailbox.id FROM ".$mailbox."
						  STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".to_employee_id
						  STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						  WHERE ".$mailbox.".deleted ='0' and ".$employees.".deleted ='0' and ".$mailbox.".from_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' ";	
		}
		$QueryData = $this->db->query($ExecteQuery);		
		return $QueryData->num_rows();
	}	
	function get_info($item_id)
	{
		$this->db->from('mailbox');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields('mailbox');
			foreach ($fields as $field){$person_obj->$field='';}
			return $person_obj;
		}
	}
	function get_Item_ArrayData($item_id) 
	{ 
	    $mailbox=$this->db->dbprefix('mailbox');
		$table_name = $mailbox;
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$QueryData = $this->db->query("SELECT $mailbox.*, $people.first_name,$people.last_name FROM ".$mailbox."
						STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".employee_id
						STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						WHERE ".$mailbox.".id ='".$item_id."' ");	
		if($QueryData->num_rows()==1) {	return $QueryData; }
		else
		{
			$item_obj=new stdClass();
			$fields = $this->db->list_fields($table_name);			
			return $fields;
		}
	}		
	function search($method_type,$search, $limit=20,$offset=0,$column='id',$orderby='desc')
	{		
		if($method_type=='inbox')
		{
			$this->db->from('mailbox');
			$this->db->join('employees','employees.person_id=mailbox.from_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');			
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_subject LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_body LIKE '%".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."'
							");
		}
		if($method_type=='outbox')
		{
			$this->db->from('mailbox');
			$this->db->join('employees','employees.person_id=mailbox.to_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');			
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_subject LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_body LIKE '%".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".from_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."'
							");
		}		
		$this->db->order_by($column,$orderby);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	

	function search_count_all($method_type,$search, $limit=10000)
	{
	    $this->db->select('mailbox.id');
		$this->db->from('mailbox');
		if($method_type=='inbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.from_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');			
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_subject LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_body LIKE '%".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."'
							");
		}
		if($method_type=='outbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.to_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');			
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_subject LIKE '%".$this->db->escape_like_str($search)."%' or 
							   mail_body LIKE '%".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".from_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."'
							");
		}	
		$this->db->limit($limit);
		$result=$this->db->get();
		return $result->num_rows();				
	}
	 	/*	Get search suggestions to find bad reports */
	function get_search_suggestions($method_type=NULL,$search = NULL,$limit=25)
	{
		$suggestions = array();
		$this->db->from('mailbox');		
		if($method_type=='inbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.from_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');
			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' 
							 ");
		}		
		if($method_type=='outbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.to_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');	

			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
							   last_name LIKE '".$this->db->escape_like_str($search)."%' or 
							   CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('mailbox').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' and ".$this->db->dbprefix('mailbox').".from_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' 
							 ");
		}	
		$this->db->group_by("people.first_name");
		$this->db->order_by("people.first_name", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);	}		
		
		$this->db->from('mailbox');
		if($method_type=='inbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.from_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');
			$this->db->where('mailbox.to_employee_id',$this->Employee->get_Logged_SessionData('person_id'));
		}	
		if($method_type=='outbox')
		{
			$this->db->join('employees','employees.person_id=mailbox.to_employee_id');
			$this->db->join('people','people.person_id=employees.person_id');
			$this->db->where('mailbox.from_employee_id',$this->Employee->get_Logged_SessionData('person_id'));
		}	
		$this->db->where('mailbox.deleted','0');	
		$this->db->where('employees.deleted','0');	
		$this->db->like("mail_subject",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("mail_subject");
		$this->db->order_by("mail_subject", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->mail_subject); }
		
		if(count($suggestions > $limit)) { $suggestions = array_slice($suggestions, 0,$limit); }
		return $suggestions;
	}	
	
	/*Inserts or updates a bag reports*/
	function save($item_data,$item_id=false)
	{			
		if (!$item_id or !$this->exists($item_id))
		{   
			$this->db->insert('mailbox',$item_data);	
			$Last_ID = $this->db->insert_id();			
		}
		else
		{
			$this->db->where('id', $item_id);
			$this->db->update('mailbox',$item_data);
			$Last_ID = $item_id;
		}		
		return $Last_ID;
	}
	
	/*Deletes a list of bag reports */	function delete_list($ids)
	{   
		if(count($ids)>0)
		{  
		   $result = false;	
		   for($i=0; $i < count($ids); $i++)
		   {	    						
				$result = $this->db->query("update ".$this->db->dbprefix('mailbox')." set deleted='1' where id ='".$ids[$i]."' ");			
		  }	
		  return $result;
		}  
		return false;
 	}		function cleanup()	{		$this->db->where('deleted', '1');		return $this->db->delete('mailbox');	}
	
	function bags_reports_status()
	{
		$status = array('0' => lang('bag_reports_status_open'),'1' => lang('bag_reports_status_pending'),'2' => lang('bag_reports_under_process'),'3' => lang('bag_reports_status_reject'),'4' => lang('bag_reports_status_close') );
		return $status;
	}
	
	function Mails_Counter()
	{
		$mailbox=$this->db->dbprefix('mailbox');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$ExecteQuery = "SELECT $mailbox.id FROM ".$mailbox."
						   STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$mailbox.".from_employee_id
						   STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						   WHERE ".$mailbox.".deleted ='0' and ".$employees.".deleted ='0' and ".$mailbox.".read_status ='0' and ".$mailbox.".to_employee_id='".$this->Employee->get_Logged_SessionData('person_id')."' ";	

		$QueryData = $this->db->query($ExecteQuery);			
		return $QueryData->num_rows();
	}
	
		
}
?>
