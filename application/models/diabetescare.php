<?php
class Diabetescare extends Person
{
	function exists($person_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}	
		
    function checkvalid_Account_No($account_no)
	{	   
	   $query = " select c.person_id from ".$this->db->dbprefix('customers')." c join ".$this->db->dbprefix('people')." p ON (c.person_id = p.person_id) where c.account_number='".$account_no."' and c.deleted = '0' ";
	   $xecuteData = $this->db->query($query);
	   if($xecuteData->num_rows()>0) return true; else return false;
	}
	
	function get_Patient_Data_By_Account_No($account_no)
	{	   
	   $Query = " select p.person_id from ".$this->db->dbprefix('customers')." c join ".$this->db->dbprefix('people')." p ON (c.person_id = p.person_id) where c.account_number='".$account_no."' and c.deleted = '0' ";
	   $ExcuteData = $this->db->query($Query);
	   if($ExcuteData->num_rows()>0) return ($ExcuteData->row()); else return false;
	}
	
	function exist_diabetes_patients($person_id)
	{	   
	   $Query = "select d.person_id from ".$this->db->dbprefix('customers_diabetescare')." d join ".$this->db->dbprefix('customers')." c on (d.person_id=c.person_id) where d.person_id='".$person_id."' and d.delete = '0' and c.deleted = '0' ";
	   $ExcuteData = $this->db->query($Query);
	   if($ExcuteData->num_rows()>0) return true; else return false;
	}
	
	function exist_diabetes_patients_by_account_no($account_no)
	{	   
	   $Query = "select d.person_id from ".$this->db->dbprefix('customers_diabetescare')." d join ".$this->db->dbprefix('customers')." c on (d.person_id=c.person_id) where c.account_number='".$account_no."' and d.delete = '0' and c.deleted = '0' ";
	   $ExcuteData = $this->db->query($Query);
	   if($ExcuteData->num_rows()>0) return true; else return false;
	}
	
	function Get_Facility_Data()
	{
   	   $QueryData = $this->db->query("select * from ".$this->db->dbprefix('daibetes_facility')." ");		
	   if($QueryData->num_rows()==1){ $data =  $QueryData->row(); return($data); }
	   else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields('daibetes_facility');
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	
		
	function facility_save( $Facility_Data , $item_id )
	{
		$success=false;		
		$this->db->trans_start();
		if ($item_id==-1)	{ $success = $this->db->insert('daibetes_facility',$Facility_Data);	}
		else
		{ 
			$this->db->where('id', $item_id);
			$success = $this->db->update('daibetes_facility',$Facility_Data);
		}						
		$this->db->trans_complete();		
		return $success;
	}	 

    function zone_name_List( $column_name = NULL)
	{
		 $QueryData = $this->db->query("select $column_name from ".$this->db->dbprefix('region_zone')." group by $column_name order by $column_name ");		
	     return($QueryData); 		
	}
	
	function hospital_List( $zone_name = NULL)
	{   $where = ''; 
		if($zone_name!=''){ $where = " and zone_name='".$zone_name."' "; }
		$QueryData = $this->db->query("select * from ".$this->db->dbprefix('hospitals')." where 1=1 $where order by hospital_name ");		
	    return($QueryData); 		
	}
	
	function Hospital_DropDownList($zone_name = NULL , $hospital_id = NULL)
	{
	   $String ='';		
	   if($zone_name!=''){ $String = " and zone_name = '".$zone_name."' "; }
	   $QueryData = $this->db->query("select * from ".$this->db->dbprefix('hospitals')." where status='0' $String order by hospital_name ");
		$ListData = '<select class="valid report_option" id="hospital_name" name="hospital_name" onClick="clicktohide(this);">
					<option value="">'.lang('common_hospital_name_select').'</option>';
			if($QueryData->num_rows()>0)
			{		
			  foreach($QueryData->result() as $data)
			  {  $selected ='';
  			     if(isset($hospital_id) && $hospital_id == $data->id){ $selected = 'selected="selected"'; }
				 $ListData .= '<option value="'.$data->id.'" '.$selected.'>'.$data->hospital_name.'</option>';
			  }
			}
		$ListData .= '</select>';
		return($ListData);
	}
	
	function get_Distict()
	{
		 $QueryData = $this->db->query("select * from ".$this->db->dbprefix('tanzania_disticts')." group by distict_name ");		
	     return($QueryData); 		
	}
	
	function Region_DropDownList($zone_name = NULL , $region_id = NULL)
	{
	   $String ='';		
	   if($zone_name!=''){ $String = " where zone_name = '".$zone_name."' "; }
		$ListData = '<select class="valid" id="region_name" name="region_name" style="width:171px;" >
					<option value="">'.lang('diabetes_select_region').'</option>';
	    $QueryData = $this->db->query("select id, region_name from ".$this->db->dbprefix('region_zone')." $String group by region_name order by region_name ");
			if($QueryData->num_rows()>0)
			{		
			  foreach($QueryData->result() as $data)
			  {  $selected ='';
  			     if(isset($region_id) && $region_id == $data->id){ $selected = 'selected="selected"'; }
				 $ListData .= '<option value="'.$data->id.'" '.$selected.'>'.$data->region_name.'</option>';
			  }
			}
		$ListData .= '</select>';
		return($ListData);
	}
	
	function count_all_tables($method_type=NULL)
	{  
		if($method_type=='diabetes_customers') 
		{  
			$this->db->select('customers_diabetescare.id');	
			$this->db->from('customers_diabetescare');	
			$this->db->join('customers', 'customers.person_id = customers_diabetescare.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->where('customers_diabetescare.delete','0');
			$this->db->where('customers.deleted','0');
		}
		
		if($method_type=='insulin_treatment') 
		{  
		  $Query = " select it.id from ".$this->db->dbprefix('insulin_treatments')." it left join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
						left join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
						left join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) "; 
			$num_rows = $this->db->query($Query)->num_rows();		
			return $num_rows;
		}
		
		return $this->db->count_all_results();	
	}
	
	function get_all_tables($method_type=null,$limit=10000, $offset=0,$col = NULL,$order='asc')
	{ 	   
		if($method_type=='diabetes_customers') 
		{  
		    $this->db->from('customers_diabetescare');	
			$this->db->join('customers', 'customers.person_id = customers_diabetescare.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->where('customers_diabetescare.delete','0');
			$this->db->where('customers.deleted','0');			
			$this->db->order_by('people.first_name', $order);
			$this->db->limit($limit);
			$this->db->offset($offset);	
			return $this->db->get();
		 }
		 
		if($method_type=='insulin_treatment') 
		{ 
		    $Query = " select it.*,bb.insulin_name as breakfast_insulin,bl.insulin_name as lunch_insulin,bd.insulin_name as dinner_insulin from ".$this->db->dbprefix('insulin_treatments')." it left join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
						left join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
						left join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) order by it.id desc limit $offset,$limit "; 
		  $ExecuteData = $this->db->query($Query);		
		  return $ExecuteData;
		} 		 
		 
		return $this->db->get();
     }	

    function search_count_all($method_type=null,$search=null,$limit=10000)
	{	$SearchString='';
		if($method_type=='diabetes_customers')
		{			
		   if ($this->config->item('speed_up_search_queries'))
		   {   
			$query = "select *	
			      from (
				  (select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where p.first_name like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where p.last_name like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union				
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.account_number like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union	
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where d.clinic_registration_no like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union	
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.sex like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.nationality like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.religion like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.delete,c.deleted,p.first_name,p.last_name,c.account_number,d.clinic_registration_no,c.sex,c.nationality,c.religion
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where CONCAT(p.first_name,' ',p.last_name) like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).")	
				) as search_results
				order by first_name asc limit ".$this->db->escape($limit);
				
				$result=$this->db->query($query);
			return $result->num_rows();
		   }
			else
			{
				$query = "select d.person_id from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id				
					where p.first_name like '".$this->db->escape_like_str($search)."%' or
					p.last_name LIKE '%".$this->db->escape_like_str($search)."%' or
					c.account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.sex LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.nationality LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.religion LIKE '%".$this->db->escape_like_str($search)."%' or 
					d.clinic_registration_no LIKE '%".$this->db->escape_like_str($search)."%' or 
					CONCAT(p.first_name,' ',p.last_name) LIKE '%".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0' order by p.first_name asc limit ".$this->db->escape($limit);	
				$result=$this->db->query($query);
				$result->num_rows();
			}
		}
		
		if($method_type=='insulin_treatment') 
		{  
			$searchQuery =" and ( bb.insulin_name LIKE '".$this->db->escape_like_str($search)."%' and bl.insulin_name LIKE '".$this->db->escape_like_str($search)."%' and bd.insulin_name LIKE '".$this->db->escape_like_str($search)."%' )";	
		    $Query = " select it.id from ".$this->db->dbprefix('insulin_treatments')." it left join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
						left join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
						left join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) where 1=1 $searchQuery "; 
			$num_rows = $this->db->query($Query)->num_rows();		
			return $num_rows;
		}
	}			

	function search($method_type=null,$search=null, $limit=20,$offset=0,$column=NULL,$orderby='asc')
	{	
		if($method_type=='diabetes_customers')
		{			
		   if ($this->config->item('speed_up_search_queries'))
		   {
			$query = "select *	
			      from (
		           	(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where p.first_name like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where p.last_name like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
				 	(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.account_number like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where d.clinic_registration_no like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.sex like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.nationality like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union
					
					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		           	from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		           	where c.religion like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		           	order by p.first_name asc limit ".$this->db->escape($limit).") union

					(select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country,c.sex,c.religion,d.delete,c.deleted
		    		from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id
		    		where CONCAT(p.first_name,' ',p.last_name)  like '".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0'
		    		order by p.first_name asc limit ".$this->db->escape($limit).")
				) as search_results
				order by first_name asc limit ".$this->db->escape($limit);
				return $this->db->query($query);				
		   }		
		
			else
			{  
				$query = "select d.person_id,d.clinic_registration_no,d.registration_date,c.account_number,c.dateOfBirth,c.nationality,p.first_name,p.last_name,p.country
					from ".$this->db->dbprefix('customers_diabetescare')." d
					STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON c.person_id = d.person_id
					STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON p.person_id = c.person_id				
					where p.first_name like '".$this->db->escape_like_str($search)."%' or
					p.last_name LIKE '%".$this->db->escape_like_str($search)."%' or
					c.account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.sex LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.nationality LIKE '%".$this->db->escape_like_str($search)."%' or 
					c.religion LIKE '%".$this->db->escape_like_str($search)."%' or 
					d.clinic_registration_no LIKE '%".$this->db->escape_like_str($search)."%' or 
					CONCAT(p.first_name,' ',p.last_name) LIKE '%".$this->db->escape_like_str($search)."%' and d.delete = '0' and c.deleted = '0' 
					order by p.first_name asc limit ".(int)$offset.",".$this->db->escape((int)$limit);
				return $this->db->query($query);								
			}			
		}
		
		if($method_type=='insulin_treatment') 
		{ 
			$searchQuery = " and ( bb.insulin_name LIKE '".$this->db->escape_like_str($search)."%' or bl.insulin_name LIKE '".$this->db->escape_like_str($search)."%' or bd.insulin_name LIKE '".$this->db->escape_like_str($search)."%' )";	
				  $Query = " select it.*,bb.insulin_name as breakfast_insulin,bl.insulin_name as lunch_insulin,bd.insulin_name as dinner_insulin from ".$this->db->dbprefix('insulin_treatments')." it 
							left join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
							left join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
							left join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) $searchQuery order by it.id desc limit $offset,$limit "; 
			$ExecuteData = $this->db->query($Query);		
			return $ExecuteData;
		} 
	}
   
     /*	Get search suggestions to find items*/
	function get_search_suggestions( $method_type,$search,$limit=25)
	{
		$suggestions = array();
		if($method_type=='diabetes_customers') 
		{  
			$this->db->from('customers_diabetescare');
			$this->db->join('customers','customers.person_id=customers_diabetescare.person_id');		
			$this->db->join('people','people.person_id=customers.person_id','left');		
			if ($this->config->item('speed_up_search_queries'))
			{
				$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
				last_name LIKE '".$this->db->escape_like_str($search)."%' or 
				CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('customers_diabetescare').".delete='0' and ".$this->db->dbprefix('customers').".deleted='0'");
			}
			else
			{
				$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
				last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
				CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('customers_diabetescare').".delete='0' and ".$this->db->dbprefix('customers').".deleted='0' ");			
			}
			$this->db->order_by("last_name", "asc");		
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)	{ $suggestions[]=array('label' => $row->first_name.' '.$row->last_name); }			
			
			$this->db->select('customers.account_number');
			$this->db->from('customers_diabetescare');
			$this->db->join('customers','customers.person_id=customers_diabetescare.person_id');		
			$this->db->join('people','people.person_id=customers.person_id','left');	
			$this->db->where('customers.deleted','0');
				$this->db->where('customers_diabetescare.delete','0');
			$this->db->like('customers.account_number', $search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->group_by('customers.account_number');
			$this->db->order_by('customers.account_number', 'ASC');
			$by_name = $this->db->get();	
			foreach($by_name->result() as $row) { $suggestions[]=array('label' => $row->account_number); }			
			
			$this->db->select('customers_diabetescare.clinic_registration_no');
			$this->db->from('customers_diabetescare');
			$this->db->join('customers','customers.person_id=customers_diabetescare.person_id');		
			$this->db->join('people','people.person_id=customers.person_id','left');	
			$this->db->where('customers.deleted','0');
				$this->db->where('customers_diabetescare.delete','0');
			$this->db->like('customers_diabetescare.clinic_registration_no', $search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->group_by('customers_diabetescare.clinic_registration_no');
			$this->db->order_by('customers_diabetescare.clinic_registration_no', 'ASC');
			$by_name = $this->db->get();	
			foreach($by_name->result() as $row) { $suggestions[]=array('label' => $row->clinic_registration_no); }						
		}

		if($method_type=='insulin_treatment') 
		{  
			$by_name = $this->db->query("select bb.insulin_name from ".$this->db->dbprefix('insulin_treatments')." it 
										join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
										join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
										join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) 
										where bb.insulin_name like '".$this->db->escape_like_str($search)."%'
										GROUP BY bb.insulin_name order by bb.insulin_name asc "); 
			foreach($by_name->result() as $row) { $suggestions[]=array('label' => $row->insulin_name); }
			
			$by_name = $this->db->query("select bl.insulin_name from ".$this->db->dbprefix('insulin_treatments')." it 
										join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
										join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
										join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) 
										where bl.insulin_name like '".$this->db->escape_like_str($search)."%'
										GROUP BY bl.insulin_name order by bl.insulin_name asc "); 
			foreach($by_name->result() as $row) { $suggestions[]=array('label' => $row->insulin_name); }
			
			$by_name = $this->db->query("select bd.insulin_name from ".$this->db->dbprefix('insulin_treatments')." it 
										join ".$this->db->dbprefix('insulin_category')." bb on (it.before_breakfast= bb.id) 
										join ".$this->db->dbprefix('insulin_category')." bl on (it.before_lunch = bl.id)
										join ".$this->db->dbprefix('insulin_category')." bd on (it.before_dinner = bd.id) 
										where bd.insulin_name like '".$this->db->escape_like_str($search)."%'
										GROUP BY bd.insulin_name order by bd.insulin_name asc "); 
			foreach($by_name->result() as $row) { $suggestions[]=array('label' => $row->insulin_name); }					
        }	
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
    }	

	function delete_list($method_type , $item_ids)
	{  
	  if($method_type=='diabetes_customers')
	  {		
			if(count($item_ids)>0)
			{
				for($i=0; $i<count($item_ids); $i++)
				{
					$this->db->where_in('person_id',$item_ids[$i]);
					$this->db->delete('customers_diabetescare');		         			 
				}
				return true;
			}
       }
 
		if($method_type=='insulin_treatment') { $table_name = 'insulin_treatments'; }
		if($method_type=='insulincategory')   { $table_name = 'insulin_category'; }		 	
		if(count($item_ids)>0)
		{
			for($i=0; $i<count($item_ids); $i++)
			{
				$this->db->where('id',$item_ids[$i]);
				$this->db->delete($table_name );		         			 
			}
			return true;
		}          
		else return false;
  }
	
	function get_Item_ArrayData($method_type , $item_id) 
	{	
		if($method_type=='diabetes_customers') 
		 {    
		    $table_name='customers_diabetescare';
			$this->db->from('customers_diabetescare');
			$this->db->join('customers','customers.person_id=customers_diabetescare.person_id');		
			$this->db->join('people','people.person_id=customers.person_id','left');	
			$this->db->where('customers_diabetescare.delete', '0'); 
			$this->db->where('customers.deleted', '0'); 				
			$this->db->where('customers_diabetescare.person_id', $item_id); 
			$query = $this->db->get();
			if($query->num_rows()==1) {	return $query->row(); }			
			else
			{
				//$item_obj=new stdClass();
				$table_name1='customers_diabetescare'; $table_name2='customers';  $table_name3='people';
				$person_obj=parent::get_info(-1);
				$fields = $this->db->list_fields( $table_name1);		
				foreach ($fields as $field)	{ $person_obj->$field='';}
				
				$fields = $this->db->list_fields( $table_name2);		
				foreach ($fields as $field)	{ $person_obj->$field='';}
				
				$fields = $this->db->list_fields( $table_name3);		
				foreach ($fields as $field)	{ $person_obj->$field='';}
				return $person_obj;			
			}			
		 }
		 
		 if($method_type=='insulin_treatment') 
		 {    
		    $table_name='insulin_treatments';
			$this->db->select('*');	
			$this->db->from('insulin_treatments');
			$this->db->where('id', $item_id); 
			$query = $this->db->get();
		 }	
		 
		if($query->num_rows()==1) {	return $query->row(); }
		else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields( $table_name);		
			foreach ($fields as $field)	{ $person_obj->$field='';}
			return $person_obj;			
		}
	}
	
	function GetAll_Education_Level($level=null, $class=null,$name ='education_level')
	{
	  $lavel_List =  array('1'=>'Below School Age','2'=>'Primary Level','3'=>'Secondary Level','4'=>'Higher School','5'=>'Higher Institution','6'=>'No Formal Education');
      $EducationData ='<select name="'.$name.'" id="'.$name.'" class="'.$class.'" onClick="clicktohide(this);">';
	  $EducationData .='<option value="">Select Education Level</option>';
	  foreach($lavel_List as $key=>$value)
	  {  $selected ='';
	     if($value == $level){ $selected = 'selected="selected"';}
		  $EducationData .='<option value="'.$value.'" '. $selected.'>'.$value.'</option>';
	  }	
	  $EducationData .='</select>';
	  return ($EducationData); 
	}
	
	function GetAll_Residence_Location($location=null, $class=null,$name ='location_of_residence')
	{
	  $location_List =  array('1'=>'City','2'=>'Municipal','3'=>'Town','4'=>'Village');
      $LocationData ='<select name="'.$name.'" id="'.$name.'" class="'.$class.'" onClick="clicktohide(this)">';
	  $LocationData .='<option value="">Select Location</option>';
	  foreach($location_List as $key=>$value)
	  {  $selected ='';
	     if($value == $location){ $selected = 'selected="selected"';}
		  $LocationData .='<option value="'.$value.'" '. $selected.'>'.$value.'</option>';
	  }	
	  $LocationData .='</select>';
	  return ($LocationData); 
	}
		
	function get_exportData($method_type)
	{  
		if($method_type=='diabetes_customers') 
		 {  
		   $data=$this->db->query("SELECT * from ".$this->db->dbprefix('customers_diabetescare')." where delete='0' ORDER BY first_name asc ");
		   return $data;
		 }
		else return false;		 
	}
	
	
	function import_excel($method_type, $person_id, $person_data)
	{
		if($method_type=='diabetes_customers') 
		 { 
			$query = "select * from ".$this->db->dbprefix('customers_diabetescare')." where delete='0' and person_id='".$person_id."' ";
			$ExecuteQuery = $this->db->query($query);
			if($ExecuteQuery->num_rows()>0)	
			{ 
				$this->db->where('person_id',$person_id);
				$this->db->where('delete','0');
				$this->db->update('customers_diabetescare',$person_data);	
			} 
			else{ $this->db->insert('customers_diabetescare',$person_data);	}
	     }	
		return true; 
	}
	
	
	function cleanup()
	{
		$this->db->where('delete', '1');
		return $this->db->delete('customers_diabetescare');
	}
	
	function get_years()
	{
		$years = array(); 
		for($k=date('Y')+2;$k > date('Y')-10;$k--)
		{
			$years[$k] = $k;
		}
	   return $years;
	}
	
	function exist_insulin_treatment($before_breakfast = NULL, $before_lunch = NULL, $before_dinner = NULL, $item_id)
	{	   
	   if($item_id!=-1)
			  {
				$this->db->from('insulin_treatments');
				$this->db->where('before_breakfast',$before_breakfast);	
				$this->db->where('before_lunch',$before_lunch);
				$this->db->where('before_dinner',$before_dinner);				
				$this->db->where_not_in('id',$item_id);	
				$query = $this->db->get();
			  }
			else 
			  {
				$this->db->from('insulin_treatments');
				$this->db->where('before_breakfast',$before_breakfast);	
				$this->db->where('before_lunch',$before_lunch);
				$this->db->where('before_dinner',$before_dinner);		
				$query = $this->db->get();
			   }		   
		return $query->num_rows();	
	}
	
	function searchAllInsulin_CategoryList($string=NULL)  
	{   
        $stringQuery ='';
        if(isset($string) && $string!=''){ $stringQuery = "and (insulin_name LIKE '".$this->db->escape_like_str($string)."%' or insulin_comments LIKE '".$this->db->escape_like_str($string)."%')"; }
		$ExecuteData = $this->db->query("select * from ".$this->db->dbprefix('insulin_category')." where 1=1 $stringQuery order by insulin_name ");
		$SearchData='<table id="sortable_table" class="tablesorter">
						<tr>
							<th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
							<th>'.lang("diabetes_insulin_brand_name").'</th>
							<th>'.lang("common_comments").'</th>
							<th>'.lang("common_edit").'</th>
						</tr>';
		if($ExecuteData->num_rows()>0)
		{ 		  
			foreach($ExecuteData->result() as $result)  
			{ 
			   $SearchData.='<tr style="cursor: pointer;">
							 <td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->id.'" id="'.$result->id.'"></td>
							 <td width="30%">'.$result->insulin_name.'</td> 
							 <td width="50%">'.substr($result->insulin_comments,0,50).'</td>
							 <td width="15%"><a onclick = "updateInsulinData('.$result->id.')" title="'.lang("edit_diseases").'" href="javascript:;">'.lang("common_edit").'</a></td>
							 </tr>';					
			}	
			$SearchData.='</table>';				
		}			 
		 return  $SearchData;
	}	
	
	function exists_InsulinBrand_Name($servicename = NULL, $service_id = NULL)
	 {		
	    $chk='';  		
		if(isset($service_id) && $service_id!=''){ $chk .= " and id!='".$service_id."' " ; }	
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('insulin_category')." where 1=1 $chk and (insulin_name='".$servicename."')");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }

	function GetInsulinBrandData_BYID($id)
	{		
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('insulin_category')." where id='".$id."' ");
		if($ExecuteQuery->num_rows()>0) 
		   { 
		     $ExecuteData = $ExecuteQuery->row();
			 echo json_encode(array('success'=>true,'name'=>$ExecuteData->insulin_name,'comments'=>$ExecuteData->insulin_comments)); die;
		   }
	  else { echo json_encode(array('success'=>false,'name'=>'','comments'=>''));  die;};
	}		
	

}
?>
