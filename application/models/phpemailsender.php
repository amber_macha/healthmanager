<?php
class Phpemailsender extends CI_Model
{	
	function send_Items_notification($Notification_Data = NULL,$Notification_type = NULL )
	{   
		$this->load->model('employee');	
		$employee_data = $this->Employee->get_all();
		if($this->config->item('stock_notifications')==0){ return false; }
		if($Notification_type=='stock_item_updation') { $notification_subject = lang('items_updated_notification_subject');}
		if($Notification_type=='stock_item_inventory_updation') {$notification_subject = lang('items_inventory_updated_notification_subject');}
		if($Notification_type=='stock_item_inventory_updation') {$notification_subject = lang('items_inventory_updated_notification_subject');}
		//$notification_subject .= ' - '. $Notification_Data['item_name']; 
		
		if($Notification_type=='low_inventory_report') { $notification_subject = lang('reports_low_inventory_report');}
		if($Notification_type=='summary_discounts_report') { $notification_subject = lang('reports_discounts_summary_report');}
		if($Notification_type=='summary_suppliers') { $notification_subject = lang('reports_suppliers_summary_report');}
		$sucess ='';
		$signature   = 'Thank you,<br>' . $this->config->item('company'). ' Team ';
		
		if($employee_data->num_rows() > 0)
		{  
			foreach($employee_data->result() as $RowData)
			{ 
			   if($RowData->email!='' && $RowData->notfication_status==1)
			   { 
					$sucess = $this->mail_send($RowData->email, $notification_subject, $this->ItemsUpdation_Body($RowData->email,$RowData->first_name,$this->config->item('company'),$Notification_Data,$signature,$Notification_type));
			   }
			}
			if($sucess) return true; else return false;
	  }	else return false;
	}	
	
	function ItemsUpdation_Body($email=NULL,$employe_name=NULL,$subject=NULL,$Notification_Data=NULL,$signature=NULL,$Notification_type=NULL)
	 {   
		if (! isset ( $email ) || empty ( $email )) {return FALSE;}
		$body = '';
		ob_start ();
		
		$data['Notification_Data'] = $Notification_Data;
		$data['subject'] 		   = $subject;
		$data['employe_name'] 	   = $employe_name;
		$data['signature'] 	   	   = $signature;
		
		if($Notification_type=='stock_item_updation') {$email_templates = 'stock_item_updation_Email_Template';}
		if($Notification_type=='stock_item_inventory_updation') {$email_templates = 'stock_item_inventory_updation_Email_Template';}
		if($Notification_type=='low_inventory_report' || $Notification_type=='summary_suppliers') {$email_templates = 'send_low_inventory_report_Email_Template';}
		if($Notification_type=='summary_discounts_report') {$email_templates = 'send_summary_discounts_report_Email_Template';}
		$this->load->view ("email_templates/$email_templates",$data);
		$body = ob_get_clean ();
		//echo $body; die;
		return $body;
	}		
	
	function mail_send( $receiver = NULL, $subject = NULL, $body = NULL)
	{
	    $from    = $this->config->item('email');
		$headers = "MIME-Version: 1.0\r\n";
		$headers.= "Content-type: text/html; charset=iso-8859-1\r\n"; 
		$headers.= "From: $from\r\n";
		//echo $body;die;
		$sucess=@mail($receiver, $subject, $body, $headers);
		if($sucess) { return TRUE;} else {	return FALSE; }
	}	
	
	
	

			
}



?>