<?php
class Savesetting extends CI_Model
{	
	function exists($table_name , $item_id)
	{
		$this->db->from($table_name);
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}	
	
	function exists_Column_Table_Data($table_name = NULL,$Column_Name = NULL, $item_name = NULL,$Column_By = NULL, $item_id = null)
	 {		
	    $chk='';  
		if(isset($item_id) && $item_id!=''){ $chk .= "and $Column_By!='".$item_id."' " ; }
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix($table_name)." where $Column_Name = '".$item_name."' $chk ");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }
	
	function existsTableData($table_name,$column_by,$item_id)
	{
		$this->db->from($table_name);
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	
	//**************Inserts or updates a item by tables**************************//
	function save_data($table_name , $item_data , $item_id=false)
	{   
		if (!$item_id or !$this->exists($table_name,$item_id))
		{ 
			if($this->db->insert($table_name,$item_data))
			{
				return true;
			}
			return false;
		}
		$this->db->where('id', $item_id);
		return $this->db->update($table_name,$item_data);
	}
	
	//**************Inserts item data in tables**************************//
	function addTableData($table_name, $item_data, $item_id=false)
	{   
		if($this->db->insert($table_name,$item_data)) {	return true; }
		return false;
	}
	
	//**************Inserts or updates a item by tables name**************************//
	function saveUpdateTable($table_name, $item_data, $column_by, $item_id=false)
	{   
		if (!$item_id or !$this->existsTableData($table_name,$column_by,$item_id))
		{ 
			if($this->db->insert($table_name,$item_data))
			{
				return true;
			}
			return false;
		}
		$this->db->where($column_by, $item_id);
		return $this->db->update($table_name,$item_data);
	}	
	
	function Get_Table_Category_Data($table_name=null,$category_type=NULL,$col=null,$order='asc',$limit=1000)
	{ 	
		$this->db->from($table_name);
		$this->db->where('status','0');
		$this->db->where('category_type',$category_type);
		$this->db->order_by($col, $order);
		$this->db->limit($limit);
		return $this->db->get();
	}
	
	function get_Table_Column_Data($table_name,$column_name=NULL,$column_by=NULL,$item_id=NULL)
	{   
		$this->db->select($column_name);	
		$this->db->from($table_name);	
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			$ExecuteData =  $query->row();
			return $ExecuteData->$column_name;
		}
		else{ return false;}
	}
	
	function Get_Table_Row_Data($table_name=null,$column_by=NULL,$item_id=NULL)
	{ 	
		$this->db->from($table_name);
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1) 
		{
			return $query->row();
		}
		else
		{
			$person_obj= new stdClass();
			$fields = $this->db->list_fields($table_name);
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	
	function Get_Table_Row_Data_InArray($table_name=null,$column_by=NULL,$item_id=NULL)
	{ 	
		$this->db->from($table_name);
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1) 
		{
			return $query;
		}
		else
		{
			$person_obj= new stdClass();
			$fields = $this->db->list_fields($table_name);
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}
	
	function Get_Table_Data($table_name=null,$col=null,$order='asc',$limit=1000)
	{ 	
		$this->db->from($table_name);
		$this->db->where('status','0');
		$this->db->order_by($col, $order);
		$this->db->limit($limit);
		return $this->db->get();
	}
	
	function Get_body_Measurement_Data($customer_id = NULL, $category_type = NULL, $column_name = NULL, $start_date = NULL, $end_date = NULL, $order='desc', $limit=1000)
	{ 	
		$this->db->from('body_measurement_recording');
		$this->db->where('category_id',$category_type);
		$this->db->where('recording_time  >=', $start_date);
		$this->db->where('recording_time  <=', $end_date);
		$this->db->where('status','0');
		$this->db->order_by($column_name, $order);
		$this->db->limit($limit);
		return $this->db->get();
	}	
	
	//**************Inserts or updates a item by tables**************************//
	function save_clinic_diagnoses_data($Diagnosis_Data, $lab_category=NULL, $customer_id=NULL, $history_id=NULL)
	{	
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('lab_clinical_diagnosis')." where person_id='".$customer_id."' and history_id='".$history_id."' ");
		if($ExecuteQuery->num_rows()> 0)
		{
			$DeleteQuery = $this->db->query("delete from ".$this->db->dbprefix('lab_clinical_diagnosis')." where person_id='".$customer_id."' and history_id='".$history_id."' ");
		}
		if($lab_category!='')
		{
			for($k=0;$k<count($lab_category);$k++)
			{
				$Temp_Array   = array('lab_category_id'	=>	$lab_category[$k]);
				$Diagnosis_Data = array_merge($Diagnosis_Data, $Temp_Array); 
				$this->db->insert('lab_clinical_diagnosis',$Diagnosis_Data);
			}
		}
		return true;
	}
	
	function existCashierRequest($customer_id = NULL,$category_type = NULL, $items_id = NULL) 
	{ 
		$this->db->from('manage_cashier_requests');		
		$this->db->where('person_id',$customer_id);
		$this->db->where('category_type',$category_type);
		$this->db->where('lab_test_id',$items_id);	
		$this->db->where('status','0');		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return true;
		}
		else{ return false; }
	}
	
   	function GetResidenceList($search = NULL) 
	{   
        $SearchData=''; $stringQuery ='';
        if(isset($search) && $search!='') { $stringQuery = " and residence_name LIKE '".$this->db->escape_like_str($search)."%' "; }
		$ExecuteData = $this->db->query("select * from ".$this->db->dbprefix('residence')." where 1=1 $stringQuery order by residence_name ");		
	    $SearchData.='<table id="sortable_table" class="tablesorter">
					<tr>
						<th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
						<th>'.lang("common_address_1").'</th>
						<th>'.lang("common_edit").'</th>
					</tr>';
		if($ExecuteData->num_rows()>0)
		{  foreach($ExecuteData->result() as $result)  
			{  
			 $SearchData.='<tr style="cursor: pointer;">
								<td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->residence_id.'" id="'.$result->residence_id.'"></td>
								<td width="60%">'.stripslashes($result->residence_name).'</td>
								<td width="15%"><a onclick = "GetItemData('.$result->residence_id.')" title="'.lang("common_edit").'" href="javascript:;">'.lang("common_edit").'</a></td>
							</tr>';					
			}					
		}
		$SearchData.='</table>';	
	
		return  $SearchData;
	}	
	
	function existsResidence($item_name=NULL,$item_id=null)
	 {		
	    $chk='';  
		if(isset($item_id) && $item_id!=''){ $chk .= " and residence_id!='".$item_id."' " ; }
	
	    $lab_category = $this->db->dbprefix('lab_category');
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('residence')." where 1=1 $chk and residence_name='".$item_name."'");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }

	function GetResidenceData_BYID($item_id)
	{
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('residence')." where residence_id='".$item_id."' ");
		if($ExecuteQuery->num_rows()>0) 
		{ $ExecuteData = $ExecuteQuery->row();
			echo json_encode(array('success'=>true,'name'=>$ExecuteData->residence_name));  die;
		}
		else { echo json_encode(array('success'=>false,'name'=>''));  die;};
	}	

    //**************Inserts or updates a item by tables name**************************//
	function saveResidenceData($item_data, $item_id=false)
	{   
		if (!$item_id or !$this->existsTableData($table_name='residence','residence_id',$item_id))
		{ 
			if($this->db->insert('residence',$item_data))
			{
				return true;
			}
			return false;
		}
		$this->db->where('residence_id', $item_id);
		return $this->db->update('residence',$item_data);
	}
	
	function deleteResidenceData($value=NULL)
	{
		if($value!='') 
		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 		
		    $result=$this->db->query("delete from ".$this->db->dbprefix('residence')." where residence_id in($tempArraySelected) ");
			if($result) return true; else return  false;
		}
     } 
	 
   //**********start occupation functions**************//
	function GetOccupationList($search = NULL) 
	{   
        $SearchData=''; $stringQuery ='';
        if(isset($search) && $search!='') { $stringQuery = " and (occupation_name LIKE '".$this->db->escape_like_str($search)."%' || occupation_group LIKE '".$this->db->escape_like_str($search)."%') "; }
		$ExecuteData = $this->db->query("select * from ".$this->db->dbprefix('occupations')." where 1=1 $stringQuery order by occupation_name ");		
	    $SearchData.='<table id="sortable_table" class="tablesorter">
					<tr>
						<th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
						<th>'.lang("common_occupation_name").'</th> 
						<th>'.lang("common_occupation_group").'</th> 
						<th>'.lang("common_edit").'</th>
					</tr>';
		if($ExecuteData->num_rows()>0)
		{  foreach($ExecuteData->result() as $result)  
			{  
			 $SearchData.='<tr style="cursor: pointer;">
								<td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->occupation_id.'" id="'.$result->occupation_id.'"></td>
								<td width="40%">'.stripslashes($result->occupation_name).'</td>
								<td width="40%">'.stripslashes($result->occupation_group).'</td>
								<td width="10%"><a onclick = "GetItemData('.$result->occupation_id.')" title="'.lang("common_edit").'" href="javascript:;">'.lang("common_edit").'</a></td>
							</tr>';					
			}					
		}
		$SearchData.='</table>';	
	
		return  $SearchData;
	}	
	
	function existsOccupation($item_name=NULL,$item_id=null)
	 {		
	    $chk='';  
		if(isset($item_id) && $item_id!=''){ $chk .= " and occupation_id!='".$item_id."' " ; }	
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('occupations')." where 1=1 $chk and occupation_name='".$item_name."'");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }

	function GetOccupationData_BYID($item_id)
	{
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('occupations')." where occupation_id='".$item_id."' ");
		if($ExecuteQuery->num_rows()>0) 
		{ $ExecuteData = $ExecuteQuery->row();
			echo json_encode(array('success'=>true,'name'=>$ExecuteData->occupation_name,'group_name'=>$ExecuteData->occupation_group));  die;
		}
		else { echo json_encode(array('success'=>false,'name'=>''));  die;};
	}	

    //**************Inserts or updates a item by tables name**************************//
	function saveOccupationData($item_data, $item_id=false)
	{   
		if (!$item_id or !$this->existsTableData($table_name='occupations','occupation_id',$item_id))
		{ 
			if($this->db->insert('occupations',$item_data))
			{
				return true;
			}
			return false;
		}
		$this->db->where('occupation_id', $item_id);
		return $this->db->update('occupations',$item_data);
	}
	
	function deleteOccupationData($value=NULL)
	{
		if($value!='') 
		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 		
		    $result=$this->db->query("delete from ".$this->db->dbprefix('occupations')." where occupation_id in($tempArraySelected) ");
			if($result) return true; else return  false;
		}
     } 
	 
	function GetOccupationGroup($occupation_id = NULL,$group_name = NULL) 
	{   $stringQuery =''; $SearchData='';
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('occupations')." where occupation_id='".$occupation_id."' ");
        if($ExecuteQuery->num_rows()>0){	
		$ExecuteData = $ExecuteQuery->row();
	    $SearchData.='<div id="pat_student_sec" class="field_row clearfix" style="display: block;">
					  <label>'.ucfirst($ExecuteData->occupation_group).':</label>
					  <div class="form_field">
					  <input id="registration_no" type="text" value="'.$group_name.'" name="registration_no">
					  </div>
					 </div>';
          }					 
		return  $SearchData;
	}

 /*	Change Status of items	of any table*/
	function Change_Status( $item_id, $method_type )
	{ 	if($method_type=='stock_notfications') { $table_name  = 'employees'; $column_by = 'person_id'; $status_column = 'notfication_status'; }	
		$this->db->from($table_name);
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{ 
		   $item_data = $query->row();
		   if($item_data->$status_column=='1')  $status ='0';  else $status ='1';
		   $item_data = array($status_column=>$status);
		   $this->db->where_in($column_by,$item_id);
		   return $this->db->update($table_name,$item_data);
		}				
		return false;
 	}	

	function check_Reference_Code($Reference_Code = NULL)
	{  
	   $ExecuteData = $this->db->query("select id from ".$this->db->dbprefix('nhif_patient_reports')." where reference_code_no='".$Reference_Code."' ");
	   if($ExecuteData->num_rows()>0) return true; else return false;
	}	
	
	function Get_All_LabCatgory($category_type = NULL,$items_ids = NULL)
	{   $category_items = array();
	    $query = "select * from  ".$this->db->dbprefix('lab_category')." where id in($items_ids) order by lab_name ";
	    $ExecuteData = $this->db->query($query);
	    if($ExecuteData->num_rows()>0)
		{  $category_items = array();
			foreach($ExecuteData->result() as $RowData)
			{  
			  $category_items[] = $RowData->code_number;
			}
		}
		$category_items = implode(',',$category_items);
	    return $category_items;		
	}
	
	//*******PATIENT MTHUA DIAGNOSES LIST*************//
	function get_Patient_All_Diagnosis_InString($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{   $mathua_Diagnosis = '';
		//***get all Lab Investigation Diagnoses***//
		$Clinical_Diagnosis_Diagnosis = $this->get_Patient_All_Clinical_Diagnosis_InString($customer_id , $from_date , $to_end);		
		//***get all Lab Investigation Diagnoses***//
		$Lab_Investigation_Diagnosis = $this->get_AllLab_Investigation_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		//***get all Medicine/Drugs Diagnoses***//
		$USS_Xray_Diagnosis = $this->get_AllLab_USS_xray_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		//***get all Dental Diagnoses***//
		$Dental_Diagnosis = $this->get_AllLab_Dental_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		//***get all Eye Clinic Diagnoses***//
		$Eye_Clinic_Diagnosis = $this->get_AllLab_Eye_Clinic_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		//***get all Specialist Diagnoses***//
		$Specialist_Diagnosis = $this->get_AllLab_Specialist_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		//***get all Other Services Diagnoses***//
		$Other_Services_Diagnosis = $this->get_AllLab_Other_Services_Diagnosis($diagnosis_type,$customer_id , $from_date , $to_end);
		
		if($Clinical_Diagnosis_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Clinical_Diagnosis_Diagnosis; else $mathua_Diagnosis = $Clinical_Diagnosis_Diagnosis; }
		if($Lab_Investigation_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Lab_Investigation_Diagnosis; else $mathua_Diagnosis = $Lab_Investigation_Diagnosis; }
		if($USS_Xray_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$USS_Xray_Diagnosis; else $mathua_Diagnosis = $USS_Xray_Diagnosis; }
		if($Dental_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Dental_Diagnosis; else $mathua_Diagnosis = $Dental_Diagnosis; }
		if($Eye_Clinic_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Eye_Clinic_Diagnosis; else $mathua_Diagnosis = $Eye_Clinic_Diagnosis; }
		if($Specialist_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Specialist_Diagnosis; else $mathua_Diagnosis = $Specialist_Diagnosis; }
		if($Other_Services_Diagnosis!=''){ if($mathua_Diagnosis!='') $mathua_Diagnosis = $mathua_Diagnosis .','.$Other_Services_Diagnosis; else $mathua_Diagnosis = $Other_Services_Diagnosis; }
		return $mathua_Diagnosis;
    }
	
	//*******PATIENT CLINICAL DIAGNOSIS LIST*************//
	function get_Patient_All_Clinical_Diagnosis_InString($customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{   $Clinical_Diagnosis = '';  
		$this->db->select('lab_clinical_diagnosis.lab_category_id');
		$this->db->from('customers_history');	
		$this->db->join('lab_clinical_diagnosis','lab_clinical_diagnosis.history_id = customers_history.id');				
		$this->db->where('customers_history.person_id',$customer_id);
		$this->db->where('customers_history.added_date >= ',$from_date);
		$this->db->where('customers_history.added_date <=',$to_end);
		$this->db->where('lab_clinical_diagnosis.status','0');
		$ExecuteQuery = $this->db->get(); 
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Clinical_Diagnosis = $this->Get_All_LabCatgory('clinic_diseases',$category_ids); 	
			}
		}
		return $Clinical_Diagnosis;	
    }
	
	//*******PATIENT INVESTIGATION DIAGNOSES LIST*************//
	function get_AllLab_Investigation_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_investigation');	
		$this->db->join($table_name,"$table_name.investigation_id = lab_investigation.id");				
		$this->db->where('lab_investigation.person_id',$customer_id);
		$this->db->where('lab_investigation.added_date >= ',$from_date);
		$this->db->where('lab_investigation.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }
	
	//*******PATIENT USS /X-ray DIAGNOSES LIST*************//
	function get_AllLab_USS_xray_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_uss_xrays');	
		$this->db->join($table_name,"$table_name.uss_xrays_id = lab_uss_xrays.id");				
		$this->db->where('lab_uss_xrays.person_id',$customer_id);
		$this->db->where('lab_uss_xrays.added_date >= ',$from_date);
		$this->db->where('lab_uss_xrays.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }
	
	//*******PATIENT DENTAL DIAGNOSES LIST*************//
	function get_AllLab_Dental_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_dentals');	
		$this->db->join($table_name,"$table_name.dental_id = lab_dentals.id");				
		$this->db->where('lab_dentals.person_id',$customer_id);
		$this->db->where('lab_dentals.added_date >= ',$from_date);
		$this->db->where('lab_dentals.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }
	//*******PATIENT Eye Clinic DIAGNOSES LIST*************//
	function get_AllLab_Eye_Clinic_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_eyeclinic');	
		$this->db->join($table_name,"$table_name.eyeclinic_id = lab_eyeclinic.id");				
		$this->db->where('lab_eyeclinic.person_id',$customer_id);
		$this->db->where('lab_eyeclinic.added_date >= ',$from_date);
		$this->db->where('lab_eyeclinic.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }
	
	//*******PATIENT Specialist DIAGNOSES LIST*************//
	function get_AllLab_Specialist_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_specialist');	
		$this->db->join($table_name,"$table_name.specialist_id = lab_specialist.id");				
		$this->db->where('lab_specialist.person_id',$customer_id);
		$this->db->where('lab_specialist.added_date >= ',$from_date);
		$this->db->where('lab_specialist.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }
	
	//*******PATIENT Other Services DIAGNOSES LIST*************//
	function get_AllLab_Other_Services_Diagnosis($diagnosis_type = NULL , $customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{	$Mthuha_Diagnosis = ''; 
		if($diagnosis_type=='diabetes') { $table_name = 'list_diabetes'; } else { $table_name = 'list_diseases'; }
		$this->db->select("$table_name.lab_category_id");
		$this->db->from('lab_otherservices');	
		$this->db->join($table_name,"$table_name.otherservices_id = lab_otherservices.id");				
		$this->db->where('lab_otherservices.person_id',$customer_id);
		$this->db->where('lab_otherservices.added_date >= ',$from_date);
		$this->db->where('lab_otherservices.added_date <=',$to_end);
		$this->db->where("$table_name.status",'0');
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
			foreach($ExecuteQuery->result() as $rowData) { $dataString[] = $rowData->lab_category_id; }
			if(count($dataString)>0)
			{ 
			   $dataString = array_unique($dataString);
			   $category_ids = implode(',',$dataString);
			   $Mthuha_Diagnosis = $this->Get_All_LabCatgory('diseases',$category_ids); 	
			}
		}
		return $Mthuha_Diagnosis;	
    }

	//*******PATIENT CLINICAL DIAGNOSIS LIST*************//
	function get_First_Clinical_Diagnosis_Doctor($customer_id = NULL , $from_date = NULL ,$to_end = NULL)
	{   $Clinical_Diagnosis = ''; 
		$this->db->select('employee_id,id');
		$this->db->from('customers_history');	
		$this->db->where('person_id',$customer_id);
		$this->db->where('added_date >= ',$from_date);
		$this->db->where('added_date <=',$to_end);
		$this->db->order_by('added_date','asc');
		$this->db->limit(1);
		$ExecuteQuery = $this->db->get();
		if($ExecuteQuery->num_rows()>0)
		{   $dataString = array();
		    $ExecuteData = $ExecuteQuery->row(); 
			$this->load->model('employee');
			$EmployeeData = $this->employee->get_info($ExecuteData->employee_id); 
			if($EmployeeData->address_1=='') $address = $EmployeeData->address_2; else $address = $EmployeeData->address_1; 
			$employee_detail = $EmployeeData->first_name .' '.$EmployeeData->last_name .' , '.$address;
			return $employee_detail;
		}
		return false;	
    }

	function exist_nhif_patient_reports( $from_date = NULL ,$to_end = NULL, $account_no = NULL)
	{   if($from_date=='' || $to_end == ''){ return false; }
		$this->db->select('nhif_patient_reports.id');
		$this->db->from('nhif_patient_reports');	
		$this->db->join('customers','customers.person_id = nhif_patient_reports.person_id');
		$this->db->join('people','people.person_id = customers.person_id');		
		$this->db->where('customers.deleted','0');
		$this->db->where('nhif_patient_reports.from_date  >= ',$from_date);
		$this->db->where('nhif_patient_reports.to_date  <=',$to_end);
		if($account_no!='') { $this->db->where('customers.account_number',$account_no); }
		$ExecuteQuery = $this->db->get();
	    if($ExecuteQuery->num_rows()>0) return true; else return false;
	}	
	
	function get_nhif_patient_reports( $from_date = NULL ,$to_end = NULL, $account_no = NULL,$limit=10000, $offset=0)
	{   if($from_date=='' || $to_end == ''){ return false; }
		$this->db->select('nhif_patient_reports.id,nhif_patient_reports.person_id,nhif_patient_reports.reference_code_no,customers.account_number,customers.sex,customers.membership_no,customers.age,people.first_name,people.last_name');
		$this->db->from('nhif_patient_reports');	
		$this->db->join('customers','customers.person_id = nhif_patient_reports.person_id');
		$this->db->join('people','people.person_id = customers.person_id');		
		$this->db->where('customers.deleted','0');
		$this->db->where('nhif_patient_reports.from_date  >= ',$from_date);
		$this->db->where('nhif_patient_reports.to_date  <=',$to_end);
		if($account_no!='') { $this->db->where('customers.account_number',$account_no); }
		$this->db->order_by('nhif_patient_reports.added_date','asc');
		if($account_no!='') { $this->db->limit(1); }
		else {
			$this->db->limit($limit);
			$this->db->offset($offset);
		}	
		$ExecuteQuery = $this->db->get(); 
	    return $ExecuteQuery; 
	}

	function get_nhif_patient_reports_by_sex( $from_date = NULL ,$to_end = NULL, $sex_type = NULL,$account_no = NULL)
	{   if($from_date=='' || $to_end == ''){ return false; }
		if($sex_type=='0') { $sex ='Male'; } if($sex_type=='1') { $sex ='Female'; }
		$this->db->select('nhif_patient_reports.person_id,nhif_patient_reports.reference_code_no,customers.account_number,customers.sex,customers.membership_no,customers.age,people.first_name,people.last_name');
		$this->db->from('nhif_patient_reports');	
		$this->db->join('customers','customers.person_id = nhif_patient_reports.person_id');
		$this->db->join('people','people.person_id = customers.person_id');		
		$this->db->where('customers.deleted','0');
		$this->db->where('nhif_patient_reports.from_date  >= ',$from_date);
		$this->db->where('nhif_patient_reports.to_date  <=',$to_end);
		if($account_no!='') { $this->db->where('customers.account_number',$account_no); }
		$this->db->where('customers.sex',$sex); 
		$this->db->order_by('nhif_patient_reports.added_date','asc');
		$ExecuteQuery = $this->db->get(); 
	    return $ExecuteQuery; 
	}

	function get_nhif_Total_Amount( $person_id = NULL ,$from_date_1 = NULL ,$to_date_1 = NULL)
	{   
		$this->load->model('eyeclinics');  
		$this->load->model('uss_xray'); 
		$this->load->model('medicinetest'); 
		$this->load->model('otherservices');
		$this->load->model('labtest');      
		$this->load->model('dentals'); 
		$this->load->model('ipd_in_patients');		
		$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59';		
		$Investigation_Data = $this->labtest->get_all($person_id,$from_date,$to_date);
		$USS_xrays_Data     = $this->uss_xray->get_all($person_id,$from_date,$to_date);
		$Dentals_Data       = $this->dentals->get_all($person_id,$from_date,$to_date);
		$Eye_Clinic         = $this->eyeclinics->get_all($person_id,$from_date,$to_date);
		$Medicine_Data      = $this->medicinetest->get_all($person_id,$from_date,$to_date);
		$otherservices      = $this->otherservices->get_all($person_id,$from_date,$to_date);
		$ipdin_info	    	= $this->ipd_in_patients->get_ipdin_PatientData($person_id);
		if(isset($ipdin_info->id) && $ipdin_info->id!='') $ipdin_info_id = $ipdin_info->id; else $ipdin_info_id='';
		$GetpaymentArray 	= $this->ipd_in_patients->GetTotalDeposit_Amount($person_id,$ipdin_info_id);
		$Total_Costs 		= $GetpaymentArray['Total_Amount'];				
		$Investigation_Total_nhif_price ='0.00'; 
		if($Investigation_Data->num_rows()>0){
			foreach($Investigation_Data->result() as $RowData) { $Investigation_Total_nhif_price += $RowData->unit_nhif_price; }
		}		
		if($USS_xrays_Data->num_rows()>0){
			foreach($USS_xrays_Data->result() as $RowData){ $Investigation_Total_nhif_price += $RowData->unit_nhif_price; }
		}		
		if($Dentals_Data->num_rows()>0){
			foreach($Dentals_Data->result() as $RowData){ $Investigation_Total_nhif_price += $RowData->unit_nhif_price; }
		}
		if($Eye_Clinic->num_rows()>0){
			foreach($Eye_Clinic->result() as $RowData){ $Investigation_Total_nhif_price += $RowData->unit_nhif_price; }
		}
		if($Medicine_Data->num_rows()>0){
			foreach($Medicine_Data->result() as $RowData)
			{  
			   $Investigation_Total_nhif_price += ($RowData->quantity * $RowData->unit_nhif_price);			
			}
		}
		if($otherservices->num_rows()>0){
			foreach($otherservices->result() as $RowData){ $Investigation_Total_nhif_price += $RowData->unit_nhif_price; }
		}		
		$Total_Amount  = ($Investigation_Total_nhif_price + $Total_Costs + $this->config->item('nhif_registration_fee'));
		return $Total_Amount;		
	}

	function count_Days($date)
	{
	  $now = time(); 
      $your_date = strtotime($date);
      $datediff = $now - $your_date;
      $days = floor($datediff/(60*60*24));
	  return $days; 
	} 
	
	function exists_insurance_company($item_name=NULL,$item_id=null)
	 {		
	    $chk='';  
		if(isset($item_id) && $item_id!=''){ $chk .= " and id!='".$item_id."' " ; }	
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('insurance_company')." where 1=1 $chk and company_name='".$item_name."'");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }
	

	
	
}
?>
