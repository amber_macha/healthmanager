<?php
class Summary_ipd_in_patient extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getData( $start_date, $end_date, $Block_type,$report_type )
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		
		$String = '';
		$date_range_query = " and date(i.registration_time) BETWEEN '".$start_date."' and '".$end_date."' ";
		
		if(isset($Block_type) && $Block_type!='' && $Block_type!='showAll'){ $String .= " and i.patient_block_name='".$Block_type."' "; }	
		
		if(isset($report_type) && $report_type!='')
		{
		  if($report_type=='1') { $String .= " and i.discharge_status='1' "; }
		  if($report_type=='2') { $start_date = date('Y').'-'.date('m').'-'.date('d'). ' 00:00:00'; $end_date = date('Y').'-'.date('m').'-'.date('d'). ' 23:59:59'; }
		}
		
		$date_range_query = " and date(i.registration_time) BETWEEN '".$start_date."' and '".$end_date."' ";
		
		$Query = "SELECT p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number,i.* 
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE c.deleted ='0' and i.status='0' $String $date_range_query ORDER BY p.first_name "; 
		$data=$this->db->query($Query);		
		return $data;
	}		
	
	 public function ipd_Patient_Summary($start_date = NULL,$end_date = NULL, $Block_type = NULL, $report_type = NULL)
	 {
	 
		   $table_data_rows = '<table id="sortable_table" class="tablesorter report">				
								<tbody><tr>
									<th width="20%" align="left">'.lang('common_patient_name').'</th>
									<th width="20%" align="left">'.lang('ipd_in_admit_date').'</th>
									<th width="20%" align="left">'.lang('customers_account_numbers').'</th>
									<th width="20%" align="left">'.lang('ipd_in_paid_amount').'</th>
									<th width="20%" align="left">'.lang('ipd_in_balance_amount').'</th>														
								</tr>';
								
			$patient_data = $this->getData($start_date, $end_date,$Block_type,$report_type);
			$CI = & get_instance();
			$total_patient ='0'; $TotalDeposit_IPD = '0'; $TotalBalance_IPD = '0';
			if(isset($patient_data) && $patient_data!='' && $patient_data->num_rows()>0)
			{ 
			  foreach($patient_data->result() as $row_data)
				{	
					$GetpaymentArray = $CI->ipd_in_patients->GetTotalDeposit_Amount($row_data->person_id,$row_data->id);
				   
					  if(isset($report_type) && $report_type=='3')
					  { 
						if((($GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount'] ) && $GetpaymentArray['TotalDeposit']>'0' && $GetpaymentArray['Balance_Amount']!='0' )
						  || ( $GetpaymentArray['Balance_Amount']>0 && $GetpaymentArray['TotalDeposit']=='0' && $GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount']) )
						   {
							 $total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
							 $table_data_rows .= '<tr>
													<td align="left">'.ucwords($row_data->first_name).'&nbsp;'.ucwords($row_data->last_name).'</td>
													<td align="left">'.date('d-m-Y',strtotime($row_data->registration_time)).'</td>
													<td align="left">'.$row_data->account_number.'</td>
													<td align="left">'.@to_currency($GetpaymentArray['TotalDeposit']).'</td>
													<td align="left">'.@to_currency($GetpaymentArray['Balance_Amount']).'</td>
												 </tr>';
						  }											
					   }
				   
					  if(isset($report_type) && $report_type=='4')
					  { 
						if($GetpaymentArray['TotalDeposit'] > '0' && $GetpaymentArray['Balance_Amount']=='0' && ($GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount']))
						   {  
							 $total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
							 $table_data_rows .= ' <tr>
													<td align="left">'.ucwords($row_data->first_name).'&nbsp;'.ucwords($row_data->last_name).'</td>
													<td align="left">'.date('d-m-Y',strtotime($row_data->registration_time)).'</td>
													<td align="left">'.$row_data->account_number.'</td>
													<td align="left">'.@to_currency($GetpaymentArray['TotalDeposit']).'</td>
													<td align="left">'.@to_currency($GetpaymentArray['Balance_Amount']).'</td>
												  </tr>';
						  }											
					   }
				 if($report_type!='' && ($report_type=='0' || $report_type=='1' || $report_type=='2'))    
					{		$total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
							$table_data_rows .= '<tr>
													<td align="left">'.ucwords($row_data->first_name).'&nbsp;'.ucwords($row_data->last_name).'</td>
													<td align="left">'.date('d-m-Y',strtotime($row_data->registration_time)).'</td>
													<td align="left">'.$row_data->account_number.'</td>
													<td align="left">'.@to_currency($GetpaymentArray['TotalDeposit']).'</td>
													<td align="left">'.@to_currency($GetpaymentArray['Balance_Amount']).'</td>
											     </tr>';	
						}									
				}				
			}
		$table_data_rows .= '</tbody></table>';	
	    $table_data_array = array();
		$table_data_array['ipd_patient_data']		    = $table_data_rows;	
		$table_data_array['ipd_patient_total_patient']  = $total_patient;
		$table_data_array['ipd_patient_total_paid'] 	= $TotalDeposit_IPD;	
		$table_data_array['ipd_patient_total_balance'] 	= $TotalBalance_IPD;			
        return $table_data_array;			
	 }
	
	
	public function get_Execle_Patient_Summary($start_date = NULL,$end_date = NULL, $Block_type = NULL, $report_type = NULL)
	 {	 
		   $table_data_rows = '';
		   
			$patient_data = $this->getData($start_date, $end_date,$Block_type,$report_type);
			$CI = & get_instance();
			$total_patient ='0'; $TotalDeposit_IPD = '0'; $TotalBalance_IPD = '0';
			
			$table_data_rows = array();
			$row = array('Patient', 'Date of Admit', 'Account No','Paid Amount', 'Balance');
			$table_data_rows[] = $row;
			
			if(isset($patient_data) && $patient_data!='' && $patient_data->num_rows()>0)
			{ 
			  $k=0;
			  foreach($patient_data->result() as $row_data)
				{	
					$GetpaymentArray = $CI->ipd_in_patients->GetTotalDeposit_Amount($row_data->person_id,$row_data->id);
				   
					  if(isset($report_type) && $report_type=='3')
					  { 
						if((($GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount'] ) && $GetpaymentArray['TotalDeposit']>'0' && $GetpaymentArray['Balance_Amount']!='0' )
						  || ( $GetpaymentArray['Balance_Amount']>0 && $GetpaymentArray['TotalDeposit']=='0' && $GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount']) )
						   {
							  $total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
							  $data_rows = array( ucwords($row_data->first_name).' '. ucwords($row_data->last_name),date('d-m-Y',strtotime($row_data->registration_time)), $row_data->account_number, $GetpaymentArray['TotalDeposit'],$GetpaymentArray['Balance_Amount'] );
						      $table_data_rows[] = $data_rows;
						  }											
					   }
				   
					  if(isset($report_type) && $report_type=='4')
					  { 
						if($GetpaymentArray['TotalDeposit'] > '0' && $GetpaymentArray['Balance_Amount']=='0' && ($GetpaymentArray['TotalDeposit']!= $GetpaymentArray['Balance_Amount']))
						   {  
								$total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
							    $data_rows = array( ucwords($row_data->first_name).' '. ucwords($row_data->last_name),date('d-m-Y',strtotime($row_data->registration_time)), $row_data->account_number, $GetpaymentArray['TotalDeposit'],$GetpaymentArray['Balance_Amount'] );
								$table_data_rows[] = $data_rows;
						  }											
					   }
				 if($report_type!='' && ($report_type=='0' || $report_type=='1' || $report_type=='2'))    
					{	$total_patient++;  $TotalDeposit_IPD += $GetpaymentArray['TotalDeposit']; $TotalBalance_IPD += $GetpaymentArray['Balance_Amount'];
					    $data_rows = array( ucwords($row_data->first_name).' '. ucwords($row_data->last_name),date('d-m-Y',strtotime($row_data->registration_time)), $row_data->account_number, $GetpaymentArray['TotalDeposit'],$GetpaymentArray['Balance_Amount'] );
					    $table_data_rows[] = $data_rows;
					}
 				  
				}				
			}
			
			$row = array('', '', '');
			$table_data_rows[] = $row;
			$row = array('Total IPD /In Patients', 'Total Paid IPD / In Patient ', 'Total Balance IPD / In Patient');
			$table_data_rows[] = $row;

			$row = array($total_patient, $TotalDeposit_IPD, $TotalBalance_IPD);
			$table_data_rows[] = $row;	   		
            return $table_data_rows;			
	 }	

}
?>