<?phprequire_once("report.php");
class Summary_customers extends Report
{	function __construct()
	{		parent::__construct();
	}
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_customer'), 'align'=>'left'), array('data'=>lang('reports_subtotal'), 'align'=> 'right'), array('data'=>lang('reports_total'), 'align'=> 'right'));	}
	public function getData()	{  
		$this->db->select('CONCAT(first_name, " ",last_name) as customer, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit', false);		$this->db->from('sales_items_temp');		$this->db->join('customers', 'customers.person_id = sales_items_temp.customer_id');		$this->db->join('people', 'customers.person_id = people.person_id');		if ($this->params['sale_type'] == 'sales')		{			$this->db->where('quantity_purchased > 0');		}
		elseif ($this->params['sale_type'] == 'returns')		{			$this->db->where('quantity_purchased < 0');		}		
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		if(isset($this->params['sex']) && $this->params['sex']!=''){ $this->db->where($this->db->dbprefix('customers').'.sex', $this->params['sex']); }
		if(isset($this->params['age_min']) && $this->params['age_min']!=''){ $this->db->where($this->db->dbprefix('customers').'.age >'.$this->params['age_min']); }
		if(isset($this->params['age_max']) && $this->params['age_max']!=''){ $this->db->where($this->db->dbprefix('customers').'.age < '.$this->params['age_max']); }		if(isset($this->params['registration_type']) && $this->params['registration_type']!=''){ $this->db->where('registration_type',$this->params['registration_type']); }		if(isset($this->params['category_type']) && $this->params['category_type']!=''){ $this->db->where('patient_category',$this->params['category_type']); }		$this->db->where($this->db->dbprefix('customers').'.patient_type !=', '-1');
		$this->db->group_by('customer_id');
		$this->db->order_by('last_name');
		return $this->db->get()->result_array();	
	}
	
	public function getSummaryData()
	{		$this->db->select('sum(subtotal) as subtotal, sum(total) as total');
		$this->db->from('sales_items_temp');
		$this->db->join('customers', 'customers.person_id = sales_items_temp.customer_id');
		$this->db->join('people', 'customers.person_id = people.person_id');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);		if(isset($this->params['sex']) && $this->params['sex']!=''){ $this->db->where($this->db->dbprefix('customers').'.sex', $this->params['sex']); }		if(isset($this->params['age_min']) && $this->params['age_min']!=''){ $this->db->where($this->db->dbprefix('customers').'.age >'.$this->params['age_min']); }		if(isset($this->params['age_max']) && $this->params['age_max']!=''){ $this->db->where($this->db->dbprefix('customers').'.age < '.$this->params['age_max']); }		if(isset($this->params['registration_type']) && $this->params['registration_type']!=''){ $this->db->where('registration_type',$this->params['registration_type']); }		if(isset($this->params['category_type']) && $this->params['category_type']!=''){ $this->db->where('patient_category',$this->params['category_type']); }		$this->db->where($this->db->dbprefix('customers').'.patient_type !=', '-1');
		return $this->db->get()->row_array();
	}
}
?>