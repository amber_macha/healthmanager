<?php
require_once("report.php");
class Expire_items_summary extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_item_name'), 'align' => 'left'), array('data'=>lang('reports_item_number'), 'align' => 'left'), array('data'=>lang('reports_description'), 'align' => 'left'), array('data' => lang('items_cost_price'), 'align' => 'right'),array('data' => lang('items_unit_price'), 'align' => 'right'), array('data'=>lang('reports_count'), 'align' => 'left'), array('data'=>lang('reports_reorder_level'), 'align' => 'left'), array('data'=>lang('item_expire_date'), 'align' => 'left'));
	}
	
	public function getData()
	{
		$this->db->select('name, item_number, quantity, cost_price, unit_price, reorder_level, description,items_expire_date');
		$this->db->from('items');
		$this->db->where('items_expire_date < ', date('Y-m-d'));
        $this->db->where('items_expire_date !=', '0000-00-00');		
		$this->db->order_by('name');
		
		return $this->db->get()->result_array();

	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(cost_price * quantity) as inventory_total,sum(unit_price * quantity) as inventory_sale_total');
		$this->db->from('items');
		$this->db->where('items_expire_date < ', date('Y-m-d'));
        $this->db->where('items_expire_date !=', '0000-00-00');
		return $this->db->get()->row_array();
	}
}
?>