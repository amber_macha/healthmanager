<?php
require_once("report.php");
class Summary_discounts extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_supplier'), 'align'=> 'left'),array('data'=>lang('reports_discount_percent'), 'align'=> 'left'),array('data'=>lang('reports_count'), 'align'=> 'left'),array('data'=>lang('reports_total'),'align'=>'left'));
	}
	
	public function getData()
	{   // $sales_items_temp =	$this->db->from('sales_items_temp');  print_r($sales_items_temp);die;
		$this->db->select('subtotal,total, discount_percent, CONCAT(first_name, " ",last_name) as supplier,sales_items_temp.supplier_id', false);
		$this->db->from('sales_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = sales_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id','left');
		$this->db->join('sales_payments', 'sales_payments.sale_id = sales_items_temp.sale_id','left');
		$this->db->where('discount_percent > 0');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('sales_items_temp.discount_percent');
		$this->db->group_by('sales_items_temp.supplier_id');
		$this->db->order_by('discount_percent');
		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		//$sales_items_temp =	$this->db->from('sales_items_temp');
	    //print_r($sales_items_temp);die;
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();		
	}
	
	function Exemption_Summary($start_date = NULL, $end_date = NULL, $sex_type = NULL, $age_range = NULL)
	{   $date_query	=	'';
		$query_string = '';
		if($age_range==5)  { $query_string = " and c.age <=5 "; }
		if($age_range==60) { $query_string = " and c.age >=60 "; }
		if($sex_type != ''){ $query_string .= " and c.sex = '".$sex_type."' ";}
		$query = " select s.customer_id from ".$this->db->dbprefix('sales ')." s left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
				   join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
				   join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
				   join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
				   join ".$this->db->dbprefix('customers')." c on (c.person_id = s.customer_id) 
				   where s.deleted='0' and s.suspended='0' and si.discount_percent > '0' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' $query_string  group by s.customer_id";
		$ExecuteQuery = $this->db->query($query);
	    return($ExecuteQuery->num_rows());
	}
}
?>