<?php
class Summary_tika_patient extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	

	public function getData( $start_date = NULL,$end_date = NULL )
	{ 
		$customers = $this->db->dbprefix('customers');
		$peoples = $this->db->dbprefix('people');			
	    $query = "select p.person_id, p.first_name,p.last_name, p.registration_date, c.account_number from $peoples p left join $customers c on (p.person_id = c.person_id) where c.patient_category='1' and c.deleted='0' and date(p.registration_date) BETWEEN '".$start_date."' and '".$end_date."'  "; 
		$payment_data = $this->db->query($query);					
		return $payment_data;
	}
	
	public function GetAllReapeated_Attendance_By_Date($start_date = Null,$end_date = NULL)
	 {
		$attendance = $this->db->dbprefix('repeated_attendance');
		$total ='';
		$patient_data = $this->getData($start_date, $end_date);
		if(isset($patient_data) && $patient_data!='' && $patient_data->num_rows()>0)
		{
			foreach($patient_data->result() as $row_data)
			{			
			  //echo "select * from $attendance where person_id='".$row_data->person_id."' and status='0' and date(attendance_date) BETWEEN '".$start_date."' and '".$end_date."' order by attendance_date desc ";die;
			  $ExecuteData = $this->db->query("select * from $attendance where person_id='".$row_data->person_id."' and status='0' and date(attendance_date) BETWEEN '".$start_date."' and '".$end_date."' order by attendance_date desc ");
			  if($ExecuteData->num_rows()>0){  $total += $ExecuteData->num_rows(); } 
			}
			return($total);
		}
		else return false;	   	
	 }	
	
	
	public function getTotalSummaryData($customer_id=NULL, $start_date = Null,$end_date = NULL)
	{
	    $total ='';
		$Query = "SELECT sale_time, date(sale_time) as sale_date, tumbi_sales_items.sale_id, customer_id,tumbi_items.item_id, quantity_purchased, item_cost_price, item_unit_price, category, discount_percent, (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,tumbi_sales_items.description as description, ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)+ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)) *(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total, ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)) *(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) - (item_cost_price*quantity_purchased) as profit FROM tumbi_sales_items INNER JOIN tumbi_sales ON tumbi_sales_items.sale_id=tumbi_sales.sale_id INNER JOIN tumbi_items ON tumbi_sales_items.item_id=tumbi_items.item_id LEFT OUTER JOIN tumbi_suppliers ON tumbi_items.supplier_id=tumbi_suppliers.person_id LEFT OUTER JOIN tumbi_sales_items_taxes ON tumbi_sales_items.sale_id=tumbi_sales_items_taxes.sale_id and tumbi_sales_items.item_id=tumbi_sales_items_taxes.item_id and tumbi_sales_items.line=tumbi_sales_items_taxes.line WHERE date(sale_time) BETWEEN '".$start_date."' and '".$end_date."' and tumbi_sales.customer_id='".$customer_id."' GROUP BY sale_id, item_id";
		$Execute_Query = $this->db->query($Query);
			if($Execute_Query->num_rows()>0)
			 {
				foreach($Execute_Query->result() as $row_data)
				{
					$total += $row_data->total;
				}
				$admision_fee = $this->get_Tiak_customer_admission_fee($customer_id, $start_date,$end_date );
				if(isset($admision_fee) && $admision_fee!='') {  $total = $total + $admision_fee; }
				return $total;
			}			
		else return false;
	}
	
	function get_Tiak_customer_admission_fee($customer_id=NULL, $start_date = Null,$end_date = NULL)
	  {
		 $admission_fee = $this->db->dbprefix('admission_fee');
	     $customers = $this->db->dbprefix('customers');
		 $ExecuteQuery = $this->db->query("select person_id from $customers where person_id='".$customer_id."' and deleted='0' and patient_category='1' ");
		 if($ExecuteQuery->num_rows()==1 )	 
		 {	 
			 $ExecuteQuery1 = $this->db->query("select * from $admission_fee where date( deposit_date ) BETWEEN '".$start_date."' and '".$end_date."' and person_id='".$customer_id."' ");
			 if($ExecuteQuery1->num_rows()>0 ) 
			 {   $ExecuteData = $ExecuteQuery1->row();
			     return($ExecuteData->admission_fee);
			 } else{ return false;}
		 }	 
		  else{ return false;}
	  }
	
	public function getTotal_Admission_Fee($start_date = Null,$end_date = NULL)
	 {		
		$Total_Fee ='';
		$patient_data = $this->getData($start_date, $end_date);
		if(isset($patient_data) && $patient_data!='' && $patient_data->num_rows()>0)
		{
			foreach($patient_data->result() as $row_data)
			{
              $Total_Fee +=  $this->get_Tiak_customer_admission_fee($row_data->person_id, $start_date,$end_date);		
			}
			return($Total_Fee);
		}
		else return false;	   	
	 }	
	
	
	
	
}
?>