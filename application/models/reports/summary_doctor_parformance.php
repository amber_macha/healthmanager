<?php
require_once("report.php");
class Summary_doctor_parformance extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('common_doctor_name'), 'align' => 'left'), array('data'=>lang('common_total_attend_patients'), 'align' => 'left'), array('data'=>lang('common_cancel'), 'align' => 'left'), array('data' => lang('common_complete'), 'align' => 'right'));
	}
	
	public function getData($start_date = NULL,$end_date = NULL,$group_id = NULL,$sub_group_id = NULL)
	{   $search='';  
		if($group_id!='' && $group_id!=0){ $search .= " and e.employee_group_id >= '".$group_id."' "; }
		if($sub_group_id!='' && $sub_group_id!=0){ $search .= " and e.employee_sub_group_id >= '".$sub_group_id."' "; }
		$Query = "select cp.*,p.first_name,p.last_name from ".$this->db->dbprefix('customer_appointment')." cp 
				  join ".$this->db->dbprefix('employees')." e on (e.person_id = cp.employee_id)
				  join ".$this->db->dbprefix('people')." p on (p.person_id = e.person_id)
				  join ".$this->db->dbprefix('employees_groups')." g on (g.id = e.employee_group_id)
				  where cp.close_date >= '".$start_date."' and cp.close_date <= '".$end_date."'	$search group by cp.employee_id order by p.first_name";
		//echo $Query; 
		$this->db->select('customer_appointment.*,people.first_name,people.last_name');
		$this->db->from('customer_appointment');
		$this->db->join('employees', 'employees.person_id = customer_appointment.employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->join('employees_groups', 'employees_groups.id = employees.employee_group_id');			
		$this->db->where('customer_appointment.close_date >= ', $start_date);
		$this->db->where('customer_appointment.close_date <= ', $end_date);
        $this->db->where('customer_appointment.status !=', '0');	
		$this->db->where('employees.deleted','0');
		if($group_id!='' && $group_id!=0){ $this->db->where('employees.employee_group_id',$group_id); }
		if($sub_group_id!='' && $sub_group_id!=0){ $this->db->where('employees.employee_sub_group_id',$sub_group_id); }
		$this->db->group_by('customer_appointment.employee_id');
		$this->db->order_by('people.first_name');		
		return $this->db->get()->result_array();
	}
	
	public function getSummaryData()
	{
		return true;
	}
	
	public function getTotal_Attend_appointment($employee_id = NULL,$start_date = NULL,$end_date = NULL,$status_type = NULL)
	{   
		$this->db->select('customer_appointment.id');
		$this->db->from('customer_appointment');
		$this->db->join('customers', 'customers.person_id = customer_appointment.person_id');	
		$this->db->join('people', 'people.person_id = customers.person_id');	
		$this->db->where('close_date >= ', $start_date);
		$this->db->where('close_date <= ', $end_date);
		$this->db->where('customer_appointment.employee_id = ', $employee_id);
		if($status_type=='1') { $this->db->where('status', '1');}
		if($status_type=='2') { $this->db->where('status', '2');}
        else { $this->db->where('status !=', '0'); }	
		$this->db->where('customers.deleted','0');		
		$this->db->order_by('people.first_name');		
		$Result_data =  $this->db->get();
		return $Result_data->num_rows();
	}
}
?>