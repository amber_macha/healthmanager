<?php
require_once("report.php");
class Summary_employee_parformance extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('history_employee_name'), 'align' => 'left'),array('data'=>lang('invastigation'), 'align' => 'center'), array('data'=>lang('uss_xray'), 'align' => 'center'), array('data' => lang('history_eye_clinic'), 'align' => 'center'), array('data' => lang('history_specialist'), 'align' => 'center'), array('data' => lang('history_other_services'), 'align' => 'center'));
	}
	
	public function getData($start_date = NULL,$end_date = NULL)
	{   $lab_employees = array();
		//get employey data from lab investigation//
		$this->db->select('lab_investigation.tested_by_employee_id');
		$this->db->from('lab_investigation');
		$this->db->join('items', 'items.item_id = lab_investigation.lab_category_id');	
		$this->db->join('employees', 'employees.person_id = lab_investigation.tested_by_employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');	
		$this->db->where('tested_date >= ', $start_date);
		$this->db->where('tested_date <= ', $end_date);
        $this->db->where('lab_investigation.status', '1');	
		$this->db->where('employees.deleted','0');
		$this->db->where('items.deleted','0');
		$this->db->group_by('lab_investigation.tested_by_employee_id');
		$this->db->order_by('people.first_name');		
		$ResultData =  $this->db->get();
		if($ResultData->num_rows()>0) {  foreach($ResultData->result() as $rowdata) {  $lab_employees[] = $rowdata->tested_by_employee_id; } }
		
		//get employey data from uss/xrays//
		$this->db->select('lab_uss_xrays.tested_by_employee_id');
		$this->db->from('lab_uss_xrays');
		$this->db->join('employees', 'employees.person_id = lab_uss_xrays.tested_by_employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');	
		$this->db->where('tested_date >= ', $start_date);
		$this->db->where('tested_date <= ', $end_date);
        $this->db->where('lab_uss_xrays.status', '1');	
		$this->db->where('employees.deleted','0');
		$this->db->group_by('lab_uss_xrays.tested_by_employee_id');
		$this->db->order_by('people.first_name');		
		$ResultData =  $this->db->get();
		if($ResultData->num_rows()>0) {  foreach($ResultData->result() as $rowdata) {  $lab_employees[] = $rowdata->tested_by_employee_id; } }
		
		//get employey data from eye clinic//
		$this->db->select('lab_eyeclinic.tested_by_employee_id');
		$this->db->from('lab_eyeclinic');
		$this->db->join('employees', 'employees.person_id = lab_eyeclinic.tested_by_employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');	
		$this->db->where('tested_date >= ', $start_date);
		$this->db->where('tested_date <= ', $end_date);
        $this->db->where('lab_eyeclinic.status', '1');	
		$this->db->where('employees.deleted','0');
		$this->db->group_by('lab_eyeclinic.tested_by_employee_id');
		$this->db->order_by('people.first_name');		
		$ResultData =  $this->db->get();
		if($ResultData->num_rows()>0) {  foreach($ResultData->result() as $rowdata) {  $lab_employees[] = $rowdata->tested_by_employee_id; } }
		
		//get employey data from specialist//
		$this->db->select('lab_specialist.tested_by_employee_id');
		$this->db->from('lab_specialist');
		$this->db->join('employees', 'employees.person_id = lab_specialist.tested_by_employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');	
		$this->db->where('tested_date >= ', $start_date);
		$this->db->where('tested_date <= ', $end_date);
        $this->db->where('lab_specialist.status', '1');	
		$this->db->where('employees.deleted','0');
		$this->db->group_by('lab_specialist.tested_by_employee_id');
		$this->db->order_by('people.first_name');		
		$ResultData =  $this->db->get();
		if($ResultData->num_rows()>0) {  foreach($ResultData->result() as $rowdata) {  $lab_employees[] = $rowdata->tested_by_employee_id; } }
		
		//get employey data from others services//
		$this->db->select('lab_otherservices.tested_by_employee_id');
		$this->db->from('lab_otherservices');
		$this->db->join('employees', 'employees.person_id = lab_otherservices.tested_by_employee_id');	
		$this->db->join('people', 'people.person_id = employees.person_id');	
		$this->db->where('tested_date >= ', $start_date);
		$this->db->where('tested_date <= ', $end_date);
        $this->db->where('lab_otherservices.status', '1');	
		$this->db->where('employees.deleted','0');
		$this->db->group_by('lab_otherservices.tested_by_employee_id');
		$this->db->order_by('people.first_name');		
		$ResultData =  $this->db->get();
		if($ResultData->num_rows()>0) {  foreach($ResultData->result() as $rowdata) {  $lab_employees[] = $rowdata->tested_by_employee_id; } }
		$lab_employees = array_unique($lab_employees);
		return($lab_employees);
		
	}
	
	public function getSummaryData()
	{
		return true;
	}
	
	public function GetTotal_Generate_Reports($table_name = NULL,$employee_id = NULL,$start_date = NULL,$end_date = NULL)
	{   
		if($table_name=='lab_investigation' || $table_name=='lab_uss_xrays' || $table_name=='lab_eyeclinic' || $table_name=='lab_otherservices')
		{
			$this->db->select("$table_name.id");
			$this->db->from("$table_name");
			$this->db->join('items', "items.item_id = $table_name.lab_category_id");	
			$this->db->join('employees', "employees.person_id = $table_name.tested_by_employee_id");	
			$this->db->join('people', 'people.person_id = employees.person_id');
			$this->db->where("$table_name.tested_by_employee_id = ", $employee_id);			
			$this->db->where('tested_date >= ', $start_date);
			$this->db->where('tested_date <= ', $end_date);
			$this->db->where("$table_name.status", '1');	
			$this->db->where('employees.deleted','0');
			$this->db->where('items.deleted','0');
			$Result_data =  $this->db->get();
			return $Result_data->num_rows();
		}
		
		if($table_name=='lab_specialist')
		{
			$this->db->select('lab_specialist.id');
			$this->db->from('lab_specialist');
			$this->db->join('lab_category', 'lab_category.id = lab_specialist.lab_category_id');	
			$this->db->join('employees', 'employees.person_id = lab_specialist.tested_by_employee_id');	
			$this->db->join('people', 'people.person_id = employees.person_id');
			$this->db->where('lab_specialist.tested_by_employee_id = ', $employee_id);			
			$this->db->where('tested_date >= ', $start_date);
			$this->db->where('tested_date <= ', $end_date);
			$this->db->where('lab_category.category_type', 'specialist');	
			$this->db->where('lab_specialist.status', '1');	
			$this->db->where('employees.deleted','0');
			$Result_data =  $this->db->get();
			return $Result_data->num_rows();
		}
		
	}
}
?>