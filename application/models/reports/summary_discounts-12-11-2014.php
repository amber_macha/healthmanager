<?php
require_once("report.php");
class Summary_discounts extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_supplier'), 'align'=> 'left'),array('data'=>lang('reports_discount_percent'), 'align'=> 'left'),array('data'=>lang('reports_count'), 'align'=> 'left'),array('data'=>lang('reports_total'),'align'=>'left'));
	}
	
	public function getData()
	{   // $sales_items_temp =	$this->db->from('sales_items_temp');  print_r($sales_items_temp);die;
		$this->db->select('subtotal,total, discount_percent, CONCAT(first_name, " ",last_name) as supplier,sales_items_temp.supplier_id', false);
		$this->db->from('sales_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = sales_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id','left');
		$this->db->join('sales_payments', 'sales_payments.sale_id = sales_items_temp.sale_id','left');
		$this->db->where('discount_percent > 0');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('sales_items_temp.discount_percent');
		$this->db->group_by('sales_items_temp.supplier_id');
		$this->db->order_by('discount_percent');
		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		//$sales_items_temp =	$this->db->from('sales_items_temp');
	    //print_r($sales_items_temp);die;
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();		
	}
}
?>