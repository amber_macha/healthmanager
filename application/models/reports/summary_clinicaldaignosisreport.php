<?php
require_once("report.php");
class Summary_clinicaldaignosisreport extends Report   
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_supplier'), 'align'=>'left'), array('data'=>lang('reports_subtotal'), 'align'=> 'right'), array('data'=>lang('reports_total'), 'align'=> 'right'), array('data'=>lang('reports_tax'), 'align'=> 'right'), array('data'=>lang('reports_profit'), 'align'=> 'right'));
	}
	
	public function getData()
	{
		$this->db->select('CONCAT(first_name, " ",last_name) as supplier, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax,sum(profit) as profit', false);
		$this->db->from('sales_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = sales_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('supplier_id');
		$this->db->order_by('last_name');
		
		return $this->db->get()->result_array();
	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		$this->db->join('suppliers', 'suppliers.person_id = sales_items_temp.supplier_id');
		$this->db->join('people', 'suppliers.person_id = people.person_id');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		return $this->db->get()->row_array();
	}
	
	//**********MTUHA OPD functions***************//
	
	function getOPD_attendance($start_date = NULL, $end_date = NULL, $sex_type = NULL, $age_range = NULL)
	{   $date_query='';
		if($sex_type==''){$sex_type='male';}  	if($age_range==''){$age_range=0;}
	    if($age_range==0){ $age_range =" and ((c.age='' || c.age='0') and c.age_month BETWEEN 0 and 1 and  c.age_day <= '31') "; }
		if($age_range==1){ $age_range =" and (c.age='' || c.age='0' || c.age='1') and (c.age_month > 1) "; }
		if($age_range==2){ $age_range =" and (c.age >1 and c.age <=5) "; }
		if($age_range==3){ $age_range = " and (c.age >5) "; }
		if($start_date!='' & $end_date!=''){ $start_date1 =$start_date.' 00:00:00'; $end_date1 =$end_date.' 23:59:59'; $date_query = " and (l.added_date BETWEEN '".$start_date1."' and '".$end_date1."' )"; }
	    $query ="select p.person_id from ".$this->db->dbprefix('people')." p join ".$this->db->dbprefix('customers')." c on (p.person_id = c.person_id) join ".$this->db->dbprefix('lab_clinical_diagnosis')." l on (l.person_id = c.person_id) where c.deleted='0' and c.sex='".$sex_type."' $age_range $date_query GROUP BY p.person_id "; 
	    $ExecuteQuery = $this->db->query($query);
		//echo $query;die;
	    return($ExecuteQuery->num_rows());
	}
	
	function getOPD_Diagnoses_attendance($start_date = NULL, $end_date = NULL, $sex_type = NULL, $age_range = NULL, $diagnoses_id= NULL )
	{   $date_query='';
		if($sex_type==''){$sex_type='male';}  	if($age_range==''){$age_range=0;}
		//if($start_date=='' && $end_date =='') {} 
	    if($age_range==0){ $age_range =" and ((c.age='' || c.age='0') and c.age_month BETWEEN 0 and 1 and  c.age_day <= '31') "; }
		if($age_range==1){ $age_range =" and (c.age='' || c.age='0' || c.age='1') and (c.age_month > 1) "; }
		if($age_range==2){ $age_range =" and (c.age >1 and c.age <=5) "; }
		if($age_range==3){ $age_range = " and (c.age >5) "; }
		if($diagnoses_id!='')
		{ 
			if($start_date!='' & $end_date!=''){ $start_date1 =$start_date.' 00:00:00'; $end_date1 =$end_date.' 23:59:59'; $date_query = " and (l.added_date BETWEEN '".$start_date1."' and '".$end_date1."' )"; }
			$query ="select p.person_id from ".$this->db->dbprefix('people')." p join ".$this->db->dbprefix('customers')." c on (p.person_id = c.person_id) join ".$this->db->dbprefix('lab_clinical_diagnosis')." l on (l.person_id = c.person_id) where deleted='0' and l. lab_category_id='".$diagnoses_id."' and c.sex='".$sex_type."' $age_range $date_query GROUP BY p.person_id "; 
			$ExecuteQuery = $this->db->query($query);
			//echo $query;die;
			return($ExecuteQuery->num_rows());
			}
		else return(0);
	}
		
		
	function getOPD_repeat_attendance($start_date = NULL, $end_date = NULL, $sex_type = NULL, $age_range = NULL)
	{   $date_query='';
		$customers = $this->db->dbprefix('customers');
		$people = $this->db->dbprefix('people');
		$attendance = $this->db->dbprefix('repeated_attendance');
		$total ='';
		if($sex_type==''){$sex_type='male';}  	if($age_range==''){$age_range=0;}
	    if($age_range==0){ $age_range =" and ((c.age='' || c.age='0') and c.age_month BETWEEN 0 and 1 and  c.age_day <= '31') "; }
		if($age_range==1){ $age_range =" and (c.age='' || c.age='0' || c.age='1') and (c.age_month > 1) "; }
		if($age_range==2){ $age_range =" and (c.age >1 and c.age <=5) "; }
		if($age_range==3){ $age_range = " and (c.age >5) "; }
		$date_query = " and (a.attendance_date BETWEEN '".$start_date."' and '".$end_date."' )"; 
	    $query ="select p.person_id,a.attendance_date from $people p join $customers c on (p.person_id = c.person_id) join $attendance a on (a.person_id = c.person_id) where deleted='0' and c.sex='".$sex_type."' $age_range $date_query "; 
	    $ExecuteQuery = $this->db->query($query);
		if($ExecuteQuery->num_rows()>0) return ($ExecuteQuery->num_rows());
		else return(0);
		//select p.person_id,c.age,c.age_month,c.age_day,l.lab_category_id from tumbi_people p join tumbi_customers c on (p.person_id = c.person_id) join tumbi_lab_investigation l on (l.person_id = c.person_id) where deleted='0' and c.sex='male' and (c.age >5) 
	}	
		
		
		
	
	function getOPD_attendance_last_month($start_date = NULL, $end_date = NULL, $sex_type = NULL, $age_range = NULL)
	{
	echo $last_start_date = date('Y-m-d',strtotime($start_date . ' -1 month'));
	echo $last_end_date = date('Y-m-d',strtotime($end_date . ' -1 month'));
	// echo $last_month = $string1[1];
	echo $attendance_forlast_month = $this->getOPD_attendance($last_start_date, $last_end_date, $sex_type, $age_range);
	// echo  $OPD_attendance = $this->getOPD_attendance($start_date, $end_date, $sex_type, $age_range);
	// $new_attendance = $OPD_attendance - $attendance_forlast_month;	
	}
}
?>