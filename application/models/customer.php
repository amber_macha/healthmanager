<?php
class Customer extends Person
{	
	/*	Determines if a given person_id is a customer*/
	function exists($person_id,$method_type=NULL)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		if($method_type!='sales'){ $this->db->where('customers.patient_type !=','-1');}
		$this->db->where('customers.person_id',$person_id);  
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	/*	Returns all the customers*/
	function get_all($limit=10000, $offset=0,$col='last_name',$order='asc')
	{   
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$health_cards=$this->db->dbprefix('customer_health_cards');
		$SearchString = '';
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')
		{
		  $SearchString  .= " and registration_type = '".$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']."' ";
		}
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')
		{
		  $SearchString  .= " and patient_category = '".$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']."' ";
		}		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		  $col ='registration_date'; $order ='desc'; 
		}			
		$data=$this->db->query("SELECT * FROM ".$people."
						STRAIGHT_JOIN ".$customers." ON ".$people.".person_id = ".$customers.".person_id
						LEFT JOIN ".$health_cards." ON ".$health_cards.".customer_id = ".$customers.".person_id
						WHERE deleted =0 and ".$customers.".patient_type!='-1' $SearchString ORDER BY ".$col." ". $order." 
						LIMIT  ".$offset.",".$limit);					
		return $data;
	}		

	function getAttendance()
	{
		$sales=$this->db->dbprefix('sales');
		$data=$this->db->query('SELECT customer_id , MAX(sale_time) from '.$sales.'
		WHERE customer_id IS NOT NULL GROUP BY customer_id
		HAVING MAX(sale_time) < date_sub(now(),interval 30 day);');
		return $data->result_array();
	}
	
	function count_all()
	{
	    $people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$health_cards=$this->db->dbprefix('customer_health_cards');
		$SearchString = '';
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')
		{
		  $SearchString  .= " and registration_type = '".$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']."' ";
		}
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')
		{
		  $SearchString  .= " and patient_category = '".$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']."' ";
		}
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		}
		$query = "SELECT ".$people.".person_id FROM ".$people." STRAIGHT_JOIN ".$customers." ON ".$people.".person_id = ".$customers.".person_id LEFT JOIN ".$health_cards." ON ".$health_cards.".customer_id = ".$customers.".person_id WHERE deleted ='0' and ".$customers.".patient_type!='-1' $SearchString ";		
		$ExecuteQuery =$this->db->query($query);		
		return $ExecuteQuery->num_rows();
	}

	function get_info($customer_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->join('customer_health_cards', 'customer_health_cards.customer_id = customers.person_id','left');
		$this->db->where('customers.person_id',$customer_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}

		else
		{
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);
			//Get all the fields from customer table append those fields to base parent object, we we have a complete empty object
			$fields = $this->db->list_fields('people');
			foreach ($fields as $field)	{ $person_obj->$field=''; }
			$fields = $this->db->list_fields('customers');
			foreach ($fields as $field)	{$person_obj->$field='';}
			$fields = $this->db->list_fields('customer_health_cards');
			foreach ($fields as $field)	{ $person_obj->$field=''; }
			return $person_obj;
		}
	}
	
	/*	Gets information about multiple customers*/

	function get_multiple_info($customer_ids)
	{
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
	    $this->db->join('customer_health_cards', 'customer_health_cards.customer_id = customers.person_id','left');		
		$this->db->where_in('customers.person_id',$customer_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}	

	/*	Inserts or updates a customer*/

	function save(&$person_data, &$customer_data,$customer_id=false)
	{
		$success=false;
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		if(parent::save($person_data,$customer_id))
		{
			if (!$customer_id or !$this->exists($customer_id))
			{   
				$customer_data['person_id'] = $person_data['person_id'];
				$success = $this->db->insert('customers',$customer_data);		
			}
			else
			{ 
				$this->db->where('person_id', $customer_id);
				$success = $this->db->update('customers',$customer_data);
			}
		}   
		$this->db->trans_complete();		
		return $success;
	}
	/*	Deletes one customer*/

	function delete($customer_id)
	{
		$this->db->where('person_id', $customer_id);
		return $this->db->update('customers', array('deleted' => 1));
	}

	
	/*Deletes a list of customers*/
	function delete_list($customer_ids)
	{
		if(count($customer_ids)>0)
		{
			for($i=0; $i<count($customer_ids); $i++)
			{
			 $this->db->where_in('person_id',$customer_ids[$i]);
			 $this->db->delete('customers');
             
              $this->db->where_in('person_id',$customer_ids[$i]);
			  $this->db->delete('people');			 
			}
		   return true;
		}
		else return false;
		//$this->db->where_in('person_id',$customer_ids);
		//return $this->db->delete('customers');
 	}

 	/*Get search suggestions to find customers*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$SearchString = '';
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')
		{
		  $SearchString  .= " and registration_type = '".$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']."' ";
		}
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')
		{
		  $SearchString  .= " and patient_category = '".$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']."' ";
		}
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		}		
		
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		if ($this->config->item('speed_up_search_queries'))
		{
			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and deleted=0 and patient_type!='-1' $SearchString");
		}
		else
		{
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 and patient_type!='-1' $SearchString");
		}	
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);	}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}
		$this->db->like("email",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("email", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row) { $suggestions[]=array('label'=> $row->email); }
		
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);
		$this->db->where('patient_type !=','-1');	
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}
		$this->db->like("phone_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("phone_number", "asc");		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row){ $suggestions[]=array('label'=> $row->phone_number); }

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}		
		$this->db->like("account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("account_number", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->account_number);	}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}
		$this->db->like("company_name",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)	{ $suggestions[]=array('label'=> $row->company_name); }
		
        $this->db->select('occupation_registration_no');
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}	
		$this->db->like("occupation_registration_no",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("occupation_registration_no", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)	{  $suggestions[]=array('label'=> $row->occupation_registration_no); }
		
		$this->db->select('pFNumber');
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}	
		$this->db->like("pFNumber",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("pFNumber", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)	{  $suggestions[]=array('label'=> $row->pFNumber ); }
		
		$this->db->select('customer_health_cards.health_card_no');
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->join('customer_health_cards','customer_health_cards.customer_id=customers.person_id','left');
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')	{ $this->db->where('registration_type',$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']); }		
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')	{ $this->db->where('patient_category',$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']); }		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!=''&& $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')	
		{ 
			$this->db->where('registration_date >=',$_SESSION['CUSTOMER_SEARCH_START_DATE']);
			$this->db->where('registration_date <=',$_SESSION['CUSTOMER_SEARCH_START_DATE']); 			
		}	
		$this->db->like("health_card_no",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("health_card_no", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)	{  $suggestions[]=array('label'=> $row->health_card_no ); }
		//only return $limit suggestions
		if(count($suggestions > $limit)) { $suggestions = array_slice($suggestions, 0,$limit);	}
		return $suggestions;
	}	

	/*Get search suggestions to find customers*/

	function get_customer_search_suggestions($search,$limit=25)
	{
		$SearchString='';
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		}	
		
		$suggestions = array();
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		if ($this->config->item('speed_up_search_queries'))
		{
			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
				last_name LIKE '".$this->db->escape_like_str($search)."%' or 
				CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and deleted=0 $SearchString ");
		}
		else
		{
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $SearchString ");			
		}
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('value'=> $row->person_id, 'label' => $row->first_name.' '.$row->last_name);		
		}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('value'=> $row->person_id, 'label' => $row->account_number);		
		}
		
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("other_account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("other_account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('value'=> $row->person_id, 'label' => $row->other_account_number);		
		}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("email",$search,$this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=array('value'=> $row->person_id, 'label' => $row->email);		
		}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("phone_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("phone_number", "asc");	
		$by_phone_number = $this->db->get();
		foreach($by_phone_number->result() as $row)
		{
			$suggestions[]=array('value'=> $row->person_id, 'label' => $row->phone_number);		
		}
		
		$this->db->select('customer_health_cards.health_card_no');
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->join('customer_health_cards','customer_health_cards.customer_id=customers.person_id','left');
		$this->db->where('deleted',0);	
		$this->db->where('patient_type !=','-1');		
		$this->db->like("health_card_no",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("health_card_no", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)	{  $suggestions[]=array('label'=> $row->health_card_no ); }

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	/*	Preform a search on customers*/

	function search($search, $limit=20,$offset=0,$column='last_name',$orderby='asc')
	{
		$SearchString='';
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')
		{
		  $SearchString  .= " and registration_type = '".$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']."' ";
		}
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')
		{
		  $SearchString  .= " and patient_category = '".$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']."' ";
		}		
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		  $column ='registration_date'; $orderby ='desc'; 
		}	
		
		if ($this->config->item('speed_up_search_queries'))
		{
			$query = "
				select *
				from (
		           	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		           	from ".$this->db->dbprefix('customers')."
		           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		           	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where first_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		           	order by `".$column."` ".$orderby.") union

				 	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		           	from ".$this->db->dbprefix('customers')."
		           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		           	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where last_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		           	order by `".$column."` ".$orderby.") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		         	from ".$this->db->dbprefix('customers')."
		          	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		          	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where email like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		          	order by `".$column."` ".$orderby.") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		        	from ".$this->db->dbprefix('customers')."
		        	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		        	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where phone_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		        	order by `".$column."` ".$orderby.") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		      		from ".$this->db->dbprefix('customers')."
		      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		      		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where account_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		      		order by `".$column."` ".$orderby.") union
					
					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		      		from ".$this->db->dbprefix('customers')."
		      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		      		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where occupation_registration_no like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		      		order by `".$column."` ".$orderby.") union
                    
					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		      		from ".$this->db->dbprefix('customers')."
		      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		      		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where pFNumber like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		      		order by `".$column."` ".$orderby.") union
					
					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		    		from ".$this->db->dbprefix('customers')."
		    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		    		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where CONCAT(`first_name`,' ',`last_name`)  like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type!='-1' $SearchString
		    		order by `".$column."` ".$orderby.") union
					
					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		    		from ".$this->db->dbprefix('customers')."
		    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		    		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where health_card_no like '".$this->db->escape_like_str($search)."%' and deleted =0 and patient_type!='-1' $SearchString
		    		order by `".$column."` ".$orderby.")
					
				) as search_results
				order by `".$column."` ".$orderby." limit ".(int)$offset.",".$this->db->escape((int)$limit);
				return $this->db->query($query);
		}
		else
		{
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');	
			$this->db->join('customer_health_cards','customer_health_cards.customer_id=customers.person_id','left');			
			$this->db->where("(first_name LIKE '%".$search."%' or 
			last_name LIKE '%".$search."%' or 
			email LIKE '%".$search."%' or 
			phone_number LIKE '%".$search."%' or 
			account_number LIKE '%".$search."%' or 
			company_name LIKE '%".$search."%' or 
			pFNumber LIKE '%".$search."%' or 
			occupation_registration_no LIKE '%".$search."%' or 
			health_card_no LIKE '%".$search."%' or 		
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$search."%') and deleted=0 and patient_type!='-1' $SearchString ");		
			$this->db->order_by($column,$orderby);
			$this->db->limit($limit);
			$this->db->offset($offset);
			return $this->db->get();
		}
	}
	

	function search_count_all($search, $limit=10000)
	{
		$SearchString='';
		if(isset($_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']) && $_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']!='')
		{
		  $SearchString  .= " and registration_type = '".$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE']."' ";
		}
		if(isset($_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']) && $_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']!='')
		{
		  $SearchString  .= " and patient_category = '".$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']."' ";
		}
		if(isset($_SESSION['CUSTOMER_SEARCH_START_DATE']) && $_SESSION['CUSTOMER_SEARCH_START_DATE']!='' && $_SESSION['CUSTOMER_SEARCH_END_DATE']!='')
		{
		  $SearchString  = " and registration_date >= '".$_SESSION['CUSTOMER_SEARCH_START_DATE']."' AND registration_date <= '".$_SESSION['CUSTOMER_SEARCH_END_DATE']."' ";
		}		
		if ($this->config->item('speed_up_search_queries'))
		{
			$query = "
				select *
				from (
		           	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		           	from ".$this->db->dbprefix('customers')."
		           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		           	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where first_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		           	order by `last_name` asc limit ".$this->db->escape($limit).") union

				 	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		           	from ".$this->db->dbprefix('customers')."
		           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		           	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where last_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		           	order by `last_name` asc limit ".$this->db->escape($limit).") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		         	from ".$this->db->dbprefix('customers')."
		          	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		          	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where email like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		          	order by `last_name` asc limit ".$this->db->escape($limit).") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		        	from ".$this->db->dbprefix('customers')."
		        	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		        	left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where phone_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		        	order by `last_name` asc limit ".$this->db->escape($limit).") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		      		from ".$this->db->dbprefix('customers')."
		      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		      		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where account_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		      		order by `last_name` asc limit ".$this->db->escape($limit).") union

					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		    		from ".$this->db->dbprefix('customers')."
		    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		    		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where CONCAT(`first_name`,' ',`last_name`)  like '".$this->db->escape_like_str($search)."%' and deleted = 0 and patient_type='-1' $SearchString
		    		order by `last_name` asc limit ".$this->db->escape($limit).") union
					
					(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('customers').".account_number
					, ".$this->db->dbprefix('customers').".taxable, ".$this->db->dbprefix('customers').".deleted, ".$this->db->dbprefix('customers').".patient_type, ".$this->db->dbprefix('customers').".patient_category
		    		from ".$this->db->dbprefix('customers')."
		    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('customers').".person_id = ".$this->db->dbprefix('people').".person_id
		    		left join ".$this->db->dbprefix('customer_health_cards')." ON ".$this->db->dbprefix('customer_health_cards').".customer_id = ".$this->db->dbprefix('customers').".person_id
					where health_card_no like '".$this->db->escape_like_str($search)."%' and deleted =0 and patient_type!='-1' $SearchString
		    		order by `".$column."` ".$orderby.")
				) as search_results
				order by `last_name` asc limit ".$this->db->escape($limit);
				$result=$this->db->query($query);
			return $result->num_rows();
		}
		else
		{
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->join('customer_health_cards','customer_health_cards.customer_id=customers.person_id','left');			
			$this->db->where("(first_name LIKE '%".$search."%' or 
			last_name LIKE '%".$search."%' or 
			email LIKE '%".$search."%' or 
			phone_number LIKE '%".$search."%' or 
			account_number LIKE '%".$search."%' or 
			company_name LIKE '%".$search."%' or 
			pFNumber LIKE '%".$search."%' or 
			occupation_registration_no LIKE '%".$search."%' or 
			health_card_no LIKE '%".$search."%' or 		
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$search."%') and deleted=0 and patient_type!='-1' $SearchString ");			
			$this->db->order_by("last_name", "asc");
			$this->db->limit($limit);
			$result=$this->db->get();				
			return $result->num_rows();		
		}
	}

	function cleanup()
	{
		$customer_data = array('account_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('customers',$customer_data);
	}

	function Generate_Customer_Account()
	 {
        $CustomerExecute = $this->db->query(" select account_number from ".$this->db->dbprefix('customers')." where (account_number!='' or account_number!='NULL') order by person_id desc limit 0,1")->row();
		if(isset($CustomerExecute->account_number) && $CustomerExecute->account_number!='')
		 {		    
			$TempArray = explode('/',$CustomerExecute->account_number);
			$temp_array1 = $TempArray[0];
			if( $TempArray[1]== date('Y') )
			{
				$TempArray_value = explode('-',$temp_array1);	
				$num1 = $TempArray_value[1];
				$num2 = $TempArray_value[2];
				$num3 = $TempArray_value[3];
				if($num3 < 99) {  $num3 = $num3+1;   if(strlen($num3)==1) $num3 = '0'.$num3;  $account_number = $num1.'-'.$num2. '-'. $num3;  } 
			    else if( $num3 ==99 && $num2 < 99 ) { $num3 = '00'; $num2 = $num2+1; if(strlen($num2)==1) $num2 = '0'.$num2;    $account_number = $num1.'-'.$num2. '-'. $num3;  }
				else if( $num3 ==99 && $num2 == 99 && $num1 < 99) { $num3 = '00'; $num2 = '00';  $num1 = $num1+1; if(strlen($num1)==1) $num1 = '0'.$num1;   $account_number = $num1.'-'.$num2. '-'. $num3;  }
			    else { $num3 = '00'; $num2 = '00';  $num1 = $num1+1; if(strlen($num1)==1) $num1 = '0'.$num1;   $account_number = $num1.'-'.$num2. '-'. $num3;  }
                $account_number  = $this->config->item('hospital_facility_id').'-'.$account_number.'/'.$TempArray[1];	
			}
           else	{  $account_number  = $this->config->item('hospital_facility_id').'-00-00-01/'.date('Y');  }
		   } 
		else { $account_number  = $this->config->item('hospital_facility_id').'-00-00-01/'.date('Y'); }
		$ExecuteQuery = $this->db->query(" select person_id from ".$this->db->dbprefix('customers')." where account_number='".$account_number."'");
		if($ExecuteQuery->num_rows()==0) return $account_number;		
		else return false;		
	}
	
	function Generate_Dead_Body_AccountNo()
	 {
		$dead_body = $this->db->dbprefix('customers_dead_body');
	    $people    = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');		
		$CustomerExecute = $this->db->query("SELECT $customers.other_account_number FROM ".$dead_body." 
								STRAIGHT_JOIN ".$customers." ON ".$customers.".person_id = ".$dead_body.".person_id
								STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$customers.".person_id 
								where ($customers.other_account_number!='' or $customers.other_account_number!='NULL') order by ".$dead_body.".id desc limit 0,1 ");									
		if($CustomerExecute->num_rows()==1)
		 {	$ExecuteData =  $CustomerExecute->row();	    
			if($ExecuteData->other_account_number!='')
			{	$TempArray = explode('/',$ExecuteData->other_account_number);
				$temp_array1 = $TempArray[0];
				if( $TempArray[1]== date('Y') )   
				{
					$TempArray_value = explode('-',$TempArray[0]);	
					$num1 = $TempArray_value[1];
					$num2 = $TempArray_value[2];
					$num3 = $TempArray_value[3];
					if($num3 < 99) {  $num3 = $num3+1;   if(strlen($num3)==1) $num3 = '0'.$num3;  $account_number = $num1.'-'.$num2. '-'. $num3;  } 
					else if( $num3 ==99 && $num2 < 99 ) { $num3 = '00'; $num2 = $num2+1; if(strlen($num2)==1) $num2 = '0'.$num2;    $account_number = $num1.'-'.$num2. '-'. $num3;  }
					else if( $num3 ==99 && $num2 == 99 && $num1 < 99) { $num3 = '00'; $num2 = '00';  $num1 = $num1+1; if(strlen($num1)==1) $num1 = '0'.$num1;   $account_number = $num1.'-'.$num2. '-'. $num3;  }
					else { $num3 = '00'; $num2 = '00';  $num1 = $num1+1; if(strlen($num1)==1) $num1 = '0'.$num1;   $account_number = $num1.'-'.$num2. '-'. $num3;  }
					$account_number  = 'D-'.$account_number.'/'.$TempArray[1];	
				}
			}	
		   } 
		else { $account_number  = 'D-00-00-01/'.date('Y'); }
		return $account_number;		
	}		

	function Get_Customer_Account($customer_id)
	 {	
		$account_number='';  
		$CustomerExecute = $this->db->query(" select account_number from ".$this->db->dbprefix."customers where person_id='".$customer_id."' ")->row();
		if(isset($CustomerExecute->account_number) && $CustomerExecute->account_number!='')
		{
		$account_number = $CustomerExecute->account_number;
		}		
		return $account_number;
	}

	function checkvalid_Account_No($account_no)
	{	   
	   $query = " select c.person_id from ".$this->db->dbprefix('customers')." c join ".$this->db->dbprefix('people')." p ON (c.person_id = p.person_id) where c.account_number='".$account_no."' and c.deleted = '0' ";
	   $xecuteData = $this->db->query($query);
	   if($xecuteData->num_rows()>0) return true; else return false;
	}
	
	function check_Patient_Category($Category_Item = NULL, $item_id = NULL, $column_by = NULL )
	{  if($column_by=='account_number') { $where = "and c.account_number='".$item_id."' ";}
	   if($column_by=='customer_id') { $where = "and c.person_id='".$item_id."' ";}
	   $query = " select c.person_id from ".$this->db->dbprefix('customers')." c join ".$this->db->dbprefix('people')." p ON (c.person_id = p.person_id) where c.patient_category='".$Category_Item."' $where and c.deleted = '0' ";
	   $xecuteData = $this->db->query($query);
	   if($xecuteData->num_rows()>0) return true; else return false;
	}
	
	function get_Patient_Data_By_Account_No($account_no)
	{	   
	   $Query = " select p.person_id,p.first_name,p.last_name,c.account_number,c.occupation,c.sex,c.age,c.member_card_no,c.membership_no from ".$this->db->dbprefix('customers')." c join ".$this->db->dbprefix('people')." p ON (c.person_id = p.person_id) where c.account_number='".$account_no."' and c.deleted = '0' ";
	   $ExcuteData = $this->db->query($Query);
	   if($ExcuteData->num_rows()>0) return ($ExcuteData->row()); else return false;
	}
	
	function GetCountry()
	{  
	   //echo " select country from ".$this->db->dbprefix."countries where region='Africa' "; die;
	   $countries = array('' => lang('items_select_country'));
	   $ExecuteData = $this->db->query(" select country from ".$this->db->dbprefix."countries where region='Africa' ");
	   if($ExecuteData->num_rows()>0) 
	     {       
			foreach($ExecuteData->result_array() as $row) {	$countries[$row['country']] = $row['country']; }	  
	     }
		  return $countries; 
	}	

	function GetLabCatgory()
	{
	    $ExecuteData = $this->db->query("select * from  ".$this->db->dbprefix('lab_category')." where category_type='investigation' order by lab_name ");
	    if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
	}	

	function GetLabCatgoryActive()
	{
	  	$lab_investigation = $this->db->dbprefix('lab_category');
	    $ExecuteData = $this->db->query("select * from  $lab_investigation where category_type='investigation' and status='0' order by lab_name ");
	    if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
	}	
		
	function GetAllListDiagnoses($category_type=NULL)
	{   
		$lab_investigation = $this->db->dbprefix('lab_category');
		$ExecuteData = $this->db->query("select * from  $lab_investigation where category_type='".$category_type."' and status='0' order by lab_name ");
		if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
	}		
	
	function GetDiseasList($customer_id,$invest_id=-1,$LabTestType,$search=NULL)
	{
	  	$lab_category = $this->db->dbprefix('lab_category'); 
		$queryString = ''; $data ='';$disabled='';$disabled2='';
		$disesesArray = $this->GetList_ofDiseases($customer_id,$invest_id,$LabTestType);
		$string ='';
		if($search!='') {$string  = " and lab_name LIKE '".$this->db->escape_like_str($search)."%' "; }
		
		if(isset($disesesArray) && $disesesArray!='' && count($disesesArray)>0)
		{  $array = implode(',',$disesesArray);
			if($array!='') { $queryString = " and id in ($array) ";  }
			$Execute = $this->db->query("select id,lab_name from  $lab_category where category_type='diseases' and status='0' $queryString order by lab_name ");
			if($Execute->num_rows()>0)
			{  $disabled ='disabled checked="checked" ';	$disabled2 ='disabled';
				foreach($Execute->result() as $result)  
				 {			 
					$data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled.'> </span></div>';
				 }
				 
				 $queryString = " and id NOT in ($array) ";
				 $ExecuteData = $this->db->query("select id,lab_name from  $lab_category where category_type='diseases' and status='0' $queryString $string  order by lab_name ");
				 if($ExecuteData->num_rows()>0)
				   {
					 foreach($ExecuteData->result() as $result)  
					  {  $disesesArray[] = $result->id;  
						 $array = implode(',',$disesesArray); $queryString = " and id in ($array) ";
						 $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled2.'> </span></div>';
					  }
				   }		
		   }		  
		}
		else 
		 {
	       $ExecuteData = $this->db->query("select id,lab_name from  $lab_category where category_type='diseases' and status='0' $string  order by lab_name ");
	       if($ExecuteData->num_rows()>0) 
			   {
				 foreach($ExecuteData->result() as $result)  
						  {  $disesesArray[] = $result->id;  
							 $array = implode(',',$disesesArray); $queryString = " and id in ($array) ";
							 $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled2.'> </span></div>';
						  }
			   }
		 }
		 return($data);		  
	}	
	
	function GetList_ofDiseases($customer_id,$invest_id,$LabTestType=null)
	{  
	  	$list_diseases = $this->db->dbprefix('list_diseases');
		$labtest_column = 'investigation_id';
		if($LabTestType=='lab_uss_xrays')  { $labtest_column ='uss_xrays_id';} 
		if($LabTestType=='lab_dentals')    { $labtest_column ='dental_id';} 
		if($LabTestType=='lab_eyeclinics') { $labtest_column ='eyeclinic_id';} 
		if($LabTestType=='lab_specialist') { $labtest_column ='specialist_id';} 
		if($LabTestType=='lab_otherservices')  { $labtest_column ='otherservices_id';} 
								     
	    $ExecuteData = $this->db->query("select lab_category_id from $list_diseases where person_id='".$customer_id."' and status='0' and $labtest_column ='".$invest_id."' order by added_date desc");
	    $array =  array(); 
		if($ExecuteData->num_rows()>0){
		   foreach($ExecuteData->result() as $result)  {    $array[] = $result->lab_category_id;  }		
 		   return($array);  
		   }  else	 return false;
	}
	
	function GetDiabetesList($customer_id,$invest_id=-1,$LabTestType, $search=NULL)
	{
	  	$lab_category = $this->db->dbprefix('lab_category');
		$queryString = ''; $data ='';$disabled='';$disabled2='';
		$disesesArray = $this->GetList_Of_Diabetes($customer_id,$invest_id,$LabTestType);
		$string ='';
		if($search!='') {$string  = " and lab_name LIKE '".$this->db->escape_like_str($search)."%' "; }
		if(isset($disesesArray) && $disesesArray!='' && count($disesesArray)>0)
		{  $array = implode(',',$disesesArray);
			if($array!='') { $queryString = " and id in ($array) ";  }
			$Execute = $this->db->query("select id,lab_name from  $lab_category where category_type='diabetes' and status='0' $queryString order by lab_name ");
			if($Execute->num_rows()>0)
			{  $disabled ='disabled checked="checked" ';	$disabled2 ='disabled';
				foreach($Execute->result() as $result)  
				 {			 
					$data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_diabetes[]" value="'.$result->id.'" id="diabetes_'.$result->id.'" '.$disabled.'> </span></div>';
				 }
				 
				 $queryString = " and id NOT in ($array) ";
				 $ExecuteData = $this->db->query("select id,lab_name from  $lab_category where category_type='diabetes' and status='0' $queryString $string order by lab_name ");
				 if($ExecuteData->num_rows()>0)
				   {
					 foreach($ExecuteData->result() as $result)  
					  {  $disesesArray[] = $result->id;  
						 $array = implode(',',$disesesArray); $queryString = " and id in ($array) ";
						 $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_diabetes[]" value="'.$result->id.'" id="diabetes_'.$result->id.'" '.$disabled2.'> </span></div>';
					  }
				   }		
		   }		  
		}
		else 
		 {  
	       $ExecuteData = $this->db->query("select id,lab_name from $lab_category where category_type='diabetes' and status='0' $string order by lab_name ");
	       if($ExecuteData->num_rows()>0) 
			   {
				 foreach($ExecuteData->result() as $result)  
				  {  $disesesArray[] = $result->id;  
					 $array = implode(',',$disesesArray); $queryString = " and id in ($array) "; 
					 $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_diabetes[]" value="'.$result->id.'" id="diabetes_'.$result->id.'" '.$disabled2.'> </span></div>';
				  }
			   }
		 }
		 return($data);		  
	}	
	
	function GetList_Of_Diabetes($customer_id,$invest_id,$LabTestType)
	{
		$labtest_column = 'investigation_id';	   
	    if($LabTestType=='lab_uss_xrays')   { $labtest_column ='uss_xrays_id';} 
		if($LabTestType=='lab_dentals')    { $labtest_column ='dental_id';} 
		if($LabTestType=='lab_eyeclinics') { $labtest_column ='eyeclinic_id';} 
		if($LabTestType=='lab_specialist') { $labtest_column ='specialist_id';} 
		if($LabTestType=='lab_otherservices')  { $labtest_column ='otherservices_id';} 
		$ExecuteData = $this->db->query("select lab_category_id from ".$this->db->dbprefix('list_diabetes')." where person_id='".$customer_id."' and status='0' and $labtest_column ='".$invest_id."' order by added_date desc");
	    $array =  array();
		if($ExecuteData->num_rows()>0){
		   foreach($ExecuteData->result() as $result)  {    $array[] = $result->lab_category_id;  }		
 		   return($array);  
		   }  else	 return false;
	}

	function GetDiseasList_ClinicalNoteMultiple($category_type = NULL, $ClinicalDaignoses = NULL, $search = NULL,$action = NULL)
	{	  
		$queryString   = ''; $data ='';$disabled='';$disabled2=''; $string ='';
		//$category_type = 'clinic_diseases';				
		$lab_category = $this->db->dbprefix('lab_category');		
		if($search!='') {$string  = " and lab_name LIKE '".$this->db->escape_like_str($search)."%' "; }
		
		if(isset($ClinicalDaignoses) && $ClinicalDaignoses!='')
		{  
			if($ClinicalDaignoses!='') { $queryString = " and id in ($ClinicalDaignoses) ";  }
			$Execute = $this->db->query("select id,lab_name from  $lab_category where category_type='".$category_type."' and status='0' $queryString order by lab_name ");
			if($Execute->num_rows()>0)
			{  
				if($action=='view'){ $disabled ='disabled="disabled" checked="checked" '; $disabled2 ='disabled="disabled"'; }
				else { $disabled ='checked="checked" '; }
				foreach($Execute->result() as $result)  
				 {			 
					$data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled.'> </span></div>';
				 }				 
				 $queryString = " and id NOT in ($ClinicalDaignoses) ";
				 $ExecuteData = $this->db->query("select id,lab_name from  $lab_category where category_type='".$category_type."' and status='0' $queryString $string  order by lab_name ");
				 if($ExecuteData->num_rows()>0)
				 {
					foreach($ExecuteData->result() as $result)  
					{  $disesesArray[] = $result->id;  
					   $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled2.'> </span></div>';
					}
				 }		
		   }		  
		}
		else 
		 {
	       $ExecuteData = $this->db->query("select id,lab_name from  $lab_category where category_type='".$category_type."' and status='0' $string  order by lab_name ");
	       if($ExecuteData->num_rows()>0) 
			   {
				 foreach($ExecuteData->result() as $result)  
				  {  $disesesArray[] = $result->id;  
					 $queryString = " and id in ($ClinicalDaignoses) ";
					 $data .= '<div class="mthu_diagnoses_sec">'.$result->lab_name.' <span style="float:right;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->id.'" id="'.$result->id.'" '.$disabled2.'> </span></div>';
				  }
			   }
		 }
		 return($data);		  
	}
	
	
	function GetMedicineCatgory()
	{
  	    $lab_investigation = $this->db->dbprefix('lab_category');
	    $ExecuteData = $this->db->query("select * from  $lab_investigation where category_type='medicine' order by lab_name ");
	    if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
	}	

	function GetLabCatgory_Info($id)
	{
		$this->db->from('lab_investigation');	
		$this->db->where('id',$id);
		$query = $this->db->get();		
		if($query->num_rows()==1) {	return $query->row();	}
    }

	
	function get_months()
	{
		$months = array();
		for($k=1;$k<=12;$k++)
		{
			$cur_month = mktime(0, 0, 0, $k, 1, 2000);
			$months[date("m", $cur_month)] = $this->get_month_translation(date("m", $cur_month));
		}	
		return $months;
	}
		
	function get_month_translation($month_numeric)
	{
		return lang('reports_month_'.$month_numeric);
	}

	function get_days()
	{
		$days = array();		
		for($k=1;$k<=31;$k++)
		{
			$cur_day = mktime(0, 0, 0, 1, $k, 2000);
			$days[date('d',$cur_day)] = date('j',$cur_day);
		}		
		return $days;
	}
		
    function get_years()
	{
		$years = array(); $p= date('Y');
		$limit = 80;
		for($k=0;$k < $limit;$k++)
		{
			$years[$p-$k] = $p - $k;
		}
	   return $years;
	}
	
  function GetAllReapeated_Attendance($customer_id=-1)
		{
			$attendance = $this->db->dbprefix('repeated_attendance');
			$ExecuteData = $this->db->query("select * from $attendance where person_id='".$customer_id."' and status='0' order by attendance_date desc ");
			if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
		}
		
  function check_Attendance($customer_id=-1,$attentdate)
		{ 
		   if($attentdate!='' && $customer_id!='')
		   {
			$attendance = $this->db->dbprefix('repeated_attendance');
			$ExecuteData = $this->db->query("select * from $attendance where person_id='".$customer_id."' and attendance_date ='".$attentdate."' ");
			if($ExecuteData->num_rows()>0)  return $ExecuteData;  else	 return false;
			} else	return false;
		}
		
	function save_Attendance($customer_id=-1,$attentdate)
		{ 
		   if($attentdate!='' && $customer_id!='')
		    {
				$login_uerData = $this->Employee->get_logged_in_employee_info();
				$employee_id = $login_uerData->person_id;
				$attendance = $this->db->dbprefix('repeated_attendance');
				$result = $this->db->query("insert into $attendance set person_id='".$customer_id."', attendance_date ='".$attentdate."',added_by_employee_id='".$employee_id."', status='0',added_date='".date('Y-m-d H:i:s')."'  ");
				if($result)  return true;  else	 return false;
			 } return false;
		}
		
	
		
    function GetLabCatgory_name($id)
	{
		$this->db->from('lab_category');	
		$this->db->where('id',$id);
		$query = $this->db->get();		
		if($query->num_rows()==1) {	$queryData = $query->row(); return($queryData->lab_name);}
		else return false;
    }
	
   function GetPatient_ProfileImage($customer_id=-1)
	{
		$customers = $this->db->dbprefix('customers');
		$this->db->from($customers);
		$this->db->where('person_id',$customer_id);
		$query = $this->db->get();		
		if($query->num_rows()==1) {	$data = $query->row(); if(isset($data->patient_profile_image) && $data->patient_profile_image!='') return($data->patient_profile_image); } 
		return false;
    }
	
	public function Membership_ExpireDate($customer_id)
	{
	  $people    = $this->db->dbprefix('people');
	  $customers = $this->db->dbprefix('customers');
      $Execute_DataQuery = $this->db->query("select p.registration_date from $people p join $customers c on (p.person_id = c.person_id)  where c.person_id='".$customer_id."' ");
	  if(isset($Execute_DataQuery) && $Execute_DataQuery!='' && $Execute_DataQuery->num_rows()>0)
	  {	
	    $Execute_Data = $Execute_DataQuery->row();
		$valid_time_for_expire = $this->config->item('config_tika_patient_expire_time');
		$Registration_ExpireDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($Execute_Data->registration_date)) . " + $valid_time_for_expire"));			
		return($Registration_ExpireDate);		
	  }
      else return false;	  
	}
	
	
	function GetVideos_Data($id)
	{
		$videos = $this->db->dbprefix('hms_videos');
		$this->db->from('hms_videos');	
		$this->db->where('id',$id);
		$query = $this->db->get();		
		if($query->num_rows()==1) {	$queryData = $query->row(); return($queryData->video_name);}
		else return false;
    }
	
	function Videos_Category_List($type)
	{
		$videos = $this->db->dbprefix('hms_videos');
		$this->db->from('hms_videos');	
		$this->db->where('video_type',$type);
		$queryData = $this->db->get();		
		if($queryData->num_rows()>0) {	return($queryData); }
		else return false;
    }
	
	function add_repeated_attendance($customer_id=-1)
	{
	   if($customer_id==-1){redirect(base_url());}	 
		$this->check_action_permission('add_update');		
		$attent_date = $this->input->post('attent_date');
		if($attent_date!='' && $customer_id!='')
		{
          $check_attendance = $this->Customer->check_Attendance($customer_id,$attent_date);		
		  if($check_attendance){ $message='1';}
		  else {
		   $result = $this->Customer->save_Attendance($customer_id,$attent_date);
		   if($result) $message='2';  else $message='0';
		  }
		  echo $message;die;
		}		
	}
		
	function Check_AccountNo($account_no)
	{ 
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.account_number',$account_no);
		$query = $this->db->get();
		if($query->num_rows()==1)return true; else return false;
	}

	function GetAjax_Account_Data($account_no)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.account_number',$account_no);
		$query = $this->db->get();
		if($query->num_rows()==1) {	return $query->row(); }
		else {	return false; }
	}	
	
	function findExtension ($filename)
	{
	   $filename = strtolower($filename) ;
	   $exts = explode(".", $filename) ;
	   $n = count($exts)-1;
	   $exts = $exts[$n];
	  return $exts;
	}
	
	function get_table_data_info($table_name , $item_id) 
	{ 
		$this->db->from($table_name);
		$this->db->where('id',$item_id);		
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$item_obj=new stdClass();
			$fields = $this->db->list_fields($table_name);
			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return $item_obj;
		}
	}
	
	function GetMembershipNo($membership_code , $person_id)
	{   $where ='';
	    if($person_id!='' || $person_id!='-1') { $where = " and p.person_id!='".$person_id."'";}
		$query = $this->db->query("select c.membership_no from ".$this->db->dbprefix('people')." p join ".$this->db->dbprefix('customers')." c on p.person_id = c.person_id where c.membership_no='".$membership_code."' $where "); 
		if($query->num_rows()>0) return true; else return false;
	}
	
	function Search_Patients_Options($string) 
	{
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$StringData='';
		$QueryData = $this->db->query("SELECT c.account_number,p.person_id,p.first_name,p.last_name FROM $people p STRAIGHT_JOIN $customers c ON  p.person_id = c.person_id WHERE deleted =0 and (c.account_number like '".$string."%' OR p.first_name like '".$string."%' OR p.last_name like '".$string."%' OR c.company_name like '".$string."%' 
									OR c.occupation like '".$string."%' OR c.membership_no like '".$string."%' OR c.member_card_no like '".$string."%' OR c.occupation_registration_no like '".$string."%' OR p.person_id like '".$string."%')");		
		$StringData =' <ul class="ipd_search_list ui-autocomplete ui-menu ui-widget ui-widget-content ">';
		$i=0;
		if($QueryData->num_rows()>0)
		{  
			foreach($QueryData->result() as $data)
			{			
			  $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" onclick="getCustomerdata(\''.$data->person_id.'\')" >'.$data->account_number. ' - ' .$data->first_name.' '.$data->last_name.'</a></li>';	
            }			
		} 
		else { $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" >Sorry No Records</a></li>'; }
		$StringData .='</ul>';	
		return $StringData ;
	}
	
	function GetOccupation_ColumData($occupation_id = NULL,$column_name = NULL) 
	{   
		$ExecuteData = $this->db->query("select $column_name from ".$this->db->dbprefix('occupations')." where occupation_id='".$occupation_id."' ")->row();
		if(isset($ExecuteData->$column_name) && $ExecuteData->$column_name!='') return  $ExecuteData->$column_name;
		else return false;
	}
	
	function getClarificationDesciption($item_id=-1,$type=NULL) 
	{   if($type=='investigation') {$table_name = 'lab_investigation';} if($type=='uss_xrays') {$table_name = 'lab_uss_xrays';}
     	$this->load->model('medicinetest');
		$this->db->from($table_name);	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		$QueryData = $query->row(); 
		$dosage_description = $this->medicinetest->get_medicine_drugs_dosage($QueryData->lab_category_id,$QueryData->person_id);
		if(isset($dosage_description)  && $dosage_description!='')	return $dosage_description; else return false;
	}
	
	function getLast_Attendance_date($person_id = NULL , $from_date = NULL , $end_date = NULL) 
	{  $Attendance_Date = array();  
	   if($person_id!='' && $from_date!='' && $end_date!='')
	   {  
			$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('repeated_attendance')." where person_id='".$person_id."' and attendance_date >= '".$from_date."' order by attendance_date asc limit 0,1 ");
			if($ExecuteQuery->num_rows()==1) 
			{ $ExecuteData = $ExecuteQuery->row(); 
			  if($ExecuteData->attendance_date!='' && $ExecuteData->attendance_date != '0000-00-00'){ $Attendance_Date['Attandance_Start_Date'] = $ExecuteData->attendance_date;}
			}
		   $ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('repeated_attendance')." where person_id='".$person_id."' and attendance_date <= '".$end_date."' order by attendance_date desc limit 0,1 ");
		   if($ExecuteQuery->num_rows()==1) 
		   { $ExecuteData = $ExecuteQuery->row();
			 if($ExecuteData->attendance_date!='' && $ExecuteData->attendance_date != '0000-00-00'){ $Attendance_Date['Attandance_End_Date'] = $ExecuteData->attendance_date;}
		   }	  
	   }  	
	   return $Attendance_Date;
	}
	
	function exists_Customer_HealthCard($Card_NO = NULL,$person_id = NULL)
	{   		
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->join('customer_health_cards', 'customer_health_cards.customer_id = customers.person_id','left');
		if(isset($person_id) && $person_id!=''){$this->db->where('customer_health_cards.customer_id !=',$person_id); }
		$this->db->where('customer_health_cards.health_card_no',$Card_NO);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return true;
		}
		else return false;
	}
	
	function Customer_Registration_Items_Category_Type($Type = NULL)
	{		
		$array = array();
		if($Type =='registration_type') { $array = array('0'=>lang('common_registration_type_normal'),'1'=>lang('common_registration_type_rapid')); }
		if($Type =='patient_category') 
		{ 
			$array = array('0'=>lang('common_patient_category_type1'),
						   '1'=>lang('common_patient_category_type2'),
						   '2'=>lang('common_patient_category_exemption'),
						   '3'=>lang('common_patient_temporary_exemption'),
						   '4'=>lang('common_patient_nhif'),
						   '5'=>lang('common_patient_nssf'),
						   '6'=>lang('common_patient_chf')				   
						  );		
		}
		return $array;
	}	
	
	function Customer_Registration_Type_Dropdown($Type_Dropdown = null , $option_name = null , $class = null , $selected = null)
	{	
		$Category_Type = $this->Customer_Registration_Items_Category_Type($Type_Dropdown);
		$OptionData = '<select name="'.$option_name.'" id="'.$option_name.'" class="'.$class.'">';
		if($Type_Dropdown=='patient_category'){ $OptionData .='<option value="all">'.lang('common_patient_category_type_view').'</option>';}
		else { $OptionData .='<option value="all">'.lang('common_registration_type_view').'</option>';}
		if(count($Category_Type)>0){
		foreach($Category_Type as $key=>$value)
		{   if(isset($selected_item) && $selected_item!='' && $selected_item== $key) $selected = 'selected="selected"'; else $selected='';
			$OptionData .='<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
		}}
		$OptionData .='</select>';
		return $OptionData;	
	}
	
	
}

?>

