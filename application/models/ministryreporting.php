<?php
class Ministryreporting extends Person
{	
	function get_AllReporting_Category( $parent_id=NULL )
	{
	   if($parent_id!='' && $parent_id < 1) { return false; }
		$this->db->from('ministry_reporting_category');	
		$this->db->where('parent_id',$parent_id); 
		$this->db->where('status','0');		
	    return $this->db->get();
	}	
	function Get_Reporting_CategoryName($group_id)
	{
		$this->db->select('category_name');	
		$this->db->from('ministry_reporting_category');	
		$this->db->where('id',$group_id);
		$Query = $this->db->get();		
	    if($Query->num_rows()==1){ $QueryData = $Query->row(); return $QueryData->category_name;}
		else return false;
	}
	
	function get_Sub_Category($parent_id = NULL, $sub_category_id = NULL)
	{ 
		if(!$parent_id || $parent_id=='0') { $parent_id='-1';}		
		$Sub_Group_List =  $this->get_AllReporting_Category($parent_id);		
		$option_string = '';
		
			$option_string .= '<div class="field_row clearfix"><label>'.form_label(lang("ministrytool_reporting_type").':', 'reporting_type').'</label>
							   <div class="form_field" id="'.lang("ministrytool_reporting_type").'">
							   <select class="valid" id="reporting_type" name="reporting_type" style="width:200px;margin-top:5px;" onclick="clicktohide(this);">
							   <option value="">'.lang("ministrytool_select_reporting_type").'</option>'; 
		if(isset($Sub_Group_List) && $Sub_Group_List!='' && $Sub_Group_List->num_rows()>0)
		 {		   
		   foreach($Sub_Group_List->result() as $rowdata)
		    {  
				$selected ='';
				if($rowdata->id==$sub_category_id){ $selected ='selected="selected"'; }
				$option_string .='<option value="'.$rowdata->id.'" '.$selected.'>'.ucfirst($rowdata->category_name).'</option>';
			}
		 }
		 $option_string .= '</select></div></div>';	
		
        return $option_string;		
	}
	
}
?>
