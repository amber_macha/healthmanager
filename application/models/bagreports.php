<?php
class Bagreports extends Person
{	
	function exists($item_id)
	{
		$this->db->from('bagreports');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function get_all($limit=10000, $offset=0,$col='id',$order='desc')
	{   $bagreports=$this->db->dbprefix('bagreports');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$SearchString = '';
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='')
		{
		  $SearchString  = " and ".$this->db->dbprefix('bagreports').".status = '".$_SESSION['BAG_REPORTS_STATUS_TYPE']."' ";
		}
		$QueryData = $this->db->query("SELECT $bagreports.*, $people.first_name,$people.last_name FROM ".$bagreports."
						STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$bagreports.".employee_id
						STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						WHERE ".$bagreports.".deleted ='0' and ".$employees.".deleted ='0' $SearchString ORDER BY ".$bagreports.".id ".$order." LIMIT  ".$offset.",".$limit);	
		return $QueryData;
	}	
		function count_all()
	{
		$bagreports=$this->db->dbprefix('bagreports');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$SearchString = '';
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='')
		{
		  $SearchString  = " and ".$this->db->dbprefix('bagreports').".status = '".$_SESSION['BAG_REPORTS_STATUS_TYPE']."' ";
		}
		$QueryData = $this->db->query("SELECT ".$bagreports." .id FROM ".$bagreports."
						STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$bagreports.".employee_id
						STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						WHERE ".$bagreports.".deleted ='0' $SearchString and ".$employees.".deleted ='0' ");			
		return $QueryData->num_rows();
	}	
	function get_info($item_id)
	{
		$this->db->from('bagreports');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields('bagreports');
			foreach ($fields as $field){$person_obj->$field='';}
			return $person_obj;
		}
	}
	function get_Item_ArrayData($item_id) 
	{ 
	    $bagreports=$this->db->dbprefix('bagreports');
		$table_name = $bagreports;
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$QueryData = $this->db->query("SELECT $bagreports.*, $people.first_name,$people.last_name FROM ".$bagreports."
						STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$bagreports.".employee_id
						STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						WHERE ".$bagreports.".id ='".$item_id."' ");	
		if($QueryData->num_rows()==1) {	return $QueryData; }
		else
		{
			$item_obj=new stdClass();
			$fields = $this->db->list_fields($table_name);			
			return $fields;
		}
	}		
	function search($search, $limit=20,$offset=0,$column='id',$orderby='desc')
	{   $SearchString=''; $search_by_date = date('Y-m-d',strtotime($search));
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='')
		{
		  $SearchString  = " and ".$this->db->dbprefix('bagreports').".status = '".$_SESSION['BAG_REPORTS_STATUS_TYPE']."' ";
		}	
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');			
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		description LIKE '%".$this->db->escape_like_str($search)."%' or 
		module_id LIKE '%".$this->db->escape_like_str($search)."%' or 
		added_date LIKE '%".$this->db->escape_like_str($search_by_date)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('bagreports').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' $SearchString");		
		$this->db->order_by($column,$orderby);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	

	function search_count_all($search, $limit=10000)
	{  	$SearchString=''; $search_by_date = date('Y-m-d',strtotime($search));
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='')
		{
		  $SearchString  = " and ".$this->db->dbprefix('bagreports').".status = '".$_SESSION['BAG_REPORTS_STATUS_TYPE']."' ";
		}		
	    $this->db->select('bagreports.id');
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');			
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		description LIKE '%".$this->db->escape_like_str($search)."%' or 
		module_id LIKE '%".$this->db->escape_like_str($search)."%' or 
		added_date LIKE '%".$this->db->escape_like_str($search_by_date)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('bagreports').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' $SearchString");		
		$this->db->limit($limit);
		$result=$this->db->get();
		return $result->num_rows();				
	}
	 	/*	Get search suggestions to find bad reports */
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$SearchString = '';
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='')
		{
		  $SearchString  = " and ".$this->db->dbprefix('bagreports').".status = '".$_SESSION['BAG_REPORTS_STATUS_TYPE']."' ";
		  $col ='registration_date'; $order ='desc'; 
		}	
		
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
				last_name LIKE '".$this->db->escape_like_str($search)."%' or 
				CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('bagreports').".deleted='0' and ".$this->db->dbprefix('employees').".deleted='0' $SearchString");
				
		$this->db->group_by("people.first_name");
		$this->db->order_by("people.first_name", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);	}
		
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('bagreports.deleted','0');	
		$this->db->where('employees.deleted','0');	
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='') { $this->db->where('bagreports.status',$_SESSION['BAG_REPORTS_STATUS_TYPE']); }	
		$this->db->like("module_id",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("module_id");
		$this->db->order_by("module_id", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->module_id); }
		
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('bagreports.deleted','0');	
		$this->db->where('employees.deleted','0');
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='') { $this->db->where('bagreports.status',$_SESSION['BAG_REPORTS_STATUS_TYPE']); }	
		$this->db->like("report_subject",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("report_subject");
		$this->db->order_by("report_subject", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->report_subject); }
		
		$this->db->from('bagreports');
		$this->db->join('employees','employees.person_id=bagreports.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('bagreports.deleted','0');	
		$this->db->where('employees.deleted','0');
		if(isset($_SESSION['BAG_REPORTS_STATUS_TYPE']) && $_SESSION['BAG_REPORTS_STATUS_TYPE']!='') { $this->db->where('bagreports.status',$_SESSION['BAG_REPORTS_STATUS_TYPE']); }	
		$this->db->like("added_date",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("added_date");
		$this->db->order_by("added_date", "asc");	
		$by_name = $this->db->get();  
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> date('d-m-Y',strtotime($row->added_date))); }
		if(count($suggestions > $limit)) { $suggestions = array_slice($suggestions, 0,$limit); }
		return $suggestions;
	}	
	
	/*Inserts or updates a bag reports*/
	function save($item_data,$item_id=false)
	{			
		if (!$item_id or !$this->exists($item_id))
		{   
			$this->db->insert('bagreports',$item_data);	
			$Lastinsert_ID = $this->db->insert_id();			
		}
		else
		{
			$this->db->where('id', $item_id);
			$this->db->update('bagreports',$item_data);
			$Lastinsert_ID = $item_id;
		}		
		return $Lastinsert_ID;
	}
	
	/*Deletes a list of bag reports */	function delete_list($ids)
	{   
		if(count($ids)>0)
		{  
		   $result = false;	
		   for($i=0; $i < count($ids); $i++)
		   {	    						
				$result = $this->db->query("update ".$this->db->dbprefix('bagreports')." set deleted='1' where id ='".$ids[$i]."' ");			
		  }	
		  return $result;
		}  
		return false;
 	}		function cleanup()	{		$this->db->where('deleted', '1');		return $this->db->delete('bagreports');	}
	
	function bags_reports_status()
	{
		$status = array('0' => lang('bag_reports_status_open'),'1' => lang('bag_reports_status_pending'),'2' => lang('bag_reports_under_process'),'3' => lang('bag_reports_status_reject'),'4' => lang('bag_reports_status_close') );
		return $status;
	}
	
	function reports_counter($status = NULL)
	{
		$bagreports=$this->db->dbprefix('bagreports');
		$people=$this->db->dbprefix('people');
		$employees=$this->db->dbprefix('employees');
		$QueryData = $this->db->query("SELECT ".$bagreports." .id FROM ".$bagreports."
						STRAIGHT_JOIN ".$employees." ON ".$employees.".person_id = ".$bagreports.".employee_id
						STRAIGHT_JOIN ".$people." ON ".$people.".person_id = ".$employees.".person_id
						WHERE ".$bagreports.".deleted ='0' and ".$employees.".deleted ='0' and ".$bagreports.".status ='".$status."' ");			
		return $QueryData->num_rows();
	}
	
		
}
?>
