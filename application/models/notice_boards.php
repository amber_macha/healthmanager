<?php
class Notice_boards extends Person
{	
	function exists($item_id)
	{
		$this->db->from('notice_board');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function get_all($limit=10000, $offset=0,$col='id',$order='desc')
	{   
		$this->db->from('notice_board');
		$this->db->where('deleted','0');		
		$this->db->order_by($col,$order);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();		
	}	
		function count_all()
	{
		$this->db->from('notice_board');
		$this->db->where('deleted','0');		
		$result=$this->db->get();
		return $result->num_rows();			
	}	
	function get_info($item_id)
	{
		$this->db->from('notice_board');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields('notice_board');
			foreach ($fields as $field){$person_obj->$field='';}
			return $person_obj;
		}
	}
	function get_Item_ArrayData($item_id) 
	{ 
	    $table_name=$this->db->dbprefix('notice_board');
		$this->db->from('notice_board');	
		$this->db->where('id',$item_id);
		$query = $this->db->get();
		if($query->num_rows()==1) {	return $query; }
		else
		{
			$item_obj=new stdClass();
			$fields = $this->db->list_fields($table_name);			
			return $fields;
		}
	}		
	function search($search, $limit=20,$offset=0,$column='id',$orderby='desc')
	{
		$this->db->from('notice_board');
		$this->db->where('deleted','0');
		$this->db->where("(notice_board_subject LIKE '%".$this->db->escape_like_str($search)."%' or 
		notice_board_description LIKE '%".$this->db->escape_like_str($search)."%' or 
		from_date LIKE '%".$this->db->escape_like_str($search)."%' or 
		to_date LIKE '%".$this->db->escape_like_str($search)."%') and deleted='0' ");
		$this->db->order_by($column,$orderby);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	
	
	/*Get search suggestions to find Notice Board */
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$this->db->from('notice_board');
		$this->db->like('notice_board_subject',$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->where('deleted','0');	
		$this->db->group_by("notice_board_subject");
		$this->db->order_by("notice_board_subject", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->notice_board_subject); }
		
		$this->db->from('notice_board');
		$this->db->like('from_date',$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->where('deleted','0');	
		$this->db->group_by("from_date");
		$this->db->order_by("from_date", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->from_date); }
		
		$this->db->from('notice_board');
		$this->db->like('to_date',$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->where('deleted','0');	
		$this->db->group_by("to_date");
		$this->db->order_by("to_date", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->to_date); }
		
		if(count($suggestions > $limit)) { $suggestions = array_slice($suggestions, 0,$limit); }
		return $suggestions;
	}	
	
	function save($item_data,$item_id=false)
	{			
		if (!$item_id or !$this->exists($item_id))
		{   
			$this->db->insert('notice_board',$item_data);
			return true;			
		}
		else
		{
			$this->db->where('id', $item_id);
			$this->db->update('notice_board',$item_data);
			return true;
		}		
		return false;
	}
	
	function delete_list($ids)
	{   
		if(count($ids)>0)
		{  
		   $result = false;	
		   for($i=0; $i < count($ids); $i++)
		   {	    						
				$result = $this->db->query("update ".$this->db->dbprefix('notice_board')." set deleted='1' where id ='".$ids[$i]."' ");			
		  }	
		  return $result;
		}  
		return false;
 	}		function cleanup()	{		$this->db->where('deleted', '1');		return $this->db->delete('notice_board');	}

	/*	Change Status of items	of any table*/
	function Change_Status( $item_id )
	{  
			$this->db->from('notice_board');
			$this->db->where('id',$item_id);
			$query = $this->db->get();
			if($query->num_rows()>0)
			{ 
			   $item_data = $query->row();
			   if($item_data->status=='1')  $status ='0';  else $status ='1';
			   $item_data = array('status'=>$status);
			   $this->db->where('id',$item_id);
			   return $this->db->update('notice_board',$item_data);
            }				
		return false;
 	}		
}
?>
