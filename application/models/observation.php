<?php
class Observation extends Person
{
	function get_all($customer_id,$limit=10000, $offset=0,$col='observ_id',$order='desc')
	{   
		$observations = $this->db->dbprefix('customers_observations');
		$employees = $this->db->dbprefix('employees');
		$people = $this->db->dbprefix('people');
		$SearchString = '';
		if(isset($_SESSION['OBSERVATION_SEARCH_FROM_DATE']) && $_SESSION['OBSERVATION_SEARCH_FROM_DATE']!=''  && $_SESSION['OBSERVATION_SEARCH_TO_DATE']!='')
		{
		  $SearchString  .= " and (o.observ_added_date >= '".$_SESSION['OBSERVATION_SEARCH_FROM_DATE']."' and o.observ_added_date <= '".$_SESSION['OBSERVATION_SEARCH_TO_DATE']."') ";
		}
		if(isset($_SESSION['OBSERVATION_SEARCH_STATUS']) && $_SESSION['OBSERVATION_SEARCH_STATUS']!='')
		{
		  $SearchString  .= " and ".$this->db->dbprefix('customers_observations').".observ_status = '".$_SESSION['OBSERVATION_SEARCH_STATUS']."' ";
		}	
		$query = "select o.*,p.first_name,p.last_name from ".$this->db->dbprefix('employees')." e 
				 join ".$this->db->dbprefix('people')." p on (p.person_id = e.person_id) 
				 join ".$this->db->dbprefix('customers_observations')." o on (o.employee_id = e.person_id)
				 where o.observ_deleted = '0' and o.person_id = '".$customer_id."' and e.deleted ='0' $SearchString ORDER BY o.observ_id $order LIMIT  ".$offset.",".$limit;
	    $QueryData = $this->db->query($query);
		return $QueryData;	
	}	
	
	function count_all($customer_id)
	{
		$observations = $this->db->dbprefix('customers_observations');
		$employees = $this->db->dbprefix('employees');
		$people = $this->db->dbprefix('people');
		$SearchString = '';
		if(isset($_SESSION['OBSERVATION_SEARCH_FROM_DATE']) && $_SESSION['OBSERVATION_SEARCH_FROM_DATE']!=''  && $_SESSION['OBSERVATION_SEARCH_TO_DATE']!='')
		{
		  $SearchString  .= " and (o.observ_added_date >= '".$_SESSION['OBSERVATION_SEARCH_FROM_DATE']."' and o.observ_added_date <= '".$_SESSION['OBSERVATION_SEARCH_TO_DATE']."') ";
		}
		if(isset($_SESSION['OBSERVATION_SEARCH_STATUS']) && $_SESSION['OBSERVATION_SEARCH_STATUS']!='')
		{
		  $SearchString  .= " and ".$this->db->dbprefix('customers_observations').".observ_status = '".$_SESSION['OBSERVATION_SEARCH_STATUS']."' ";
		}	
		$query = "select o.*,p.first_name,p.last_name from ".$this->db->dbprefix('employees')." e 
				 join ".$this->db->dbprefix('people')." p on (p.person_id = e.person_id) 
				 join ".$this->db->dbprefix('customers_observations')." o on (o.employee_id = e.person_id)
				 where o.observ_deleted = '0' and o.person_id = '".$customer_id."' $SearchString and e.deleted ='0' ";
		$QueryData = $this->db->query($query);
		return $QueryData->num_rows();		
	}		
	
	function search($customer_id,$search,$limit=20,$offset=0,$column='observ_id',$orderby='desc')
	{   $search_by_date = date('Y-m-d',strtotime($search));
		$SearchString = '';
		if(isset($_SESSION['OBSERVATION_SEARCH_FROM_DATE']) && $_SESSION['OBSERVATION_SEARCH_FROM_DATE']!=''  && $_SESSION['OBSERVATION_SEARCH_TO_DATE']!='')
		{
		  $SearchString  .= " and (".$this->db->dbprefix('customers_observations').".observ_added_date >= '".$_SESSION['OBSERVATION_SEARCH_FROM_DATE']."' and ".$this->db->dbprefix('customers_observations').".observ_added_date <= '".$_SESSION['OBSERVATION_SEARCH_TO_DATE']."') ";
		}
		if(isset($_SESSION['OBSERVATION_SEARCH_STATUS']) && $_SESSION['OBSERVATION_SEARCH_STATUS']!='')
		{
		  $SearchString  .= " and ".$this->db->dbprefix('customers_observations').".observ_status = '".$_SESSION['OBSERVATION_SEARCH_STATUS']."' ";
		}		
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');			
		$this->db->where("( first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_blood_pressure LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_pulse_heart_rate LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_respiratory_rate LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_temperature LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_iv LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_oral LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_ngt LIKE '%".$this->db->escape_like_str($search)."%' or							
							observ_vomiting LIKE '%".$this->db->escape_like_str($search)."%' or 
							observ_urine LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_stool LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_treatment LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_added_date LIKE '%".$this->db->escape_like_str($search)."%' or							
							observ_remarks LIKE '%".$this->db->escape_like_str($search)."%' ) 
							and ".$this->db->dbprefix('customers_observations').".person_id='".$customer_id."' and ".$this->db->dbprefix('customers_observations').".observ_deleted='0' $SearchString");		
		$this->db->order_by($column,$orderby);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}	

	function search_count_all($customer_id, $search, $limit=10000)
	{  	$SearchString=''; 
		if(isset($_SESSION['OBSERVATION_SEARCH_FROM_DATE']) && $_SESSION['OBSERVATION_SEARCH_FROM_DATE']!=''  && $_SESSION['OBSERVATION_SEARCH_TO_DATE']!='')
		{
		  $SearchString  .= " and (".$this->db->dbprefix('customers_observations').".observ_added_date >= '".$_SESSION['OBSERVATION_SEARCH_FROM_DATE']."' and ".$this->db->dbprefix('customers_observations').".observ_added_date <= '".$_SESSION['OBSERVATION_SEARCH_TO_DATE']."') ";
		}
		if(isset($_SESSION['OBSERVATION_SEARCH_STATUS']) && $_SESSION['OBSERVATION_SEARCH_STATUS']!='')
		{
		  $SearchString  .= " and ".$this->db->dbprefix('customers_observations').".observ_status = '".$_SESSION['OBSERVATION_SEARCH_STATUS']."' ";
		}		
	    $this->db->select('customers_observations.observ_id');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');				
		$this->db->where("( first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
							CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_blood_pressure LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_pulse_heart_rate LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_respiratory_rate LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_temperature LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_iv LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_oral LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_ngt LIKE '%".$this->db->escape_like_str($search)."%' or							
							observ_vomiting LIKE '%".$this->db->escape_like_str($search)."%' or 
							observ_urine LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_stool LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_treatment LIKE '%".$this->db->escape_like_str($search)."%' or
							observ_added_date LIKE '%".$this->db->escape_like_str($search)."%' or							
							observ_remarks LIKE '%".$this->db->escape_like_str($search)."%' ) 
							and ".$this->db->dbprefix('customers_observations').".person_id='".$customer_id."' and ".$this->db->dbprefix('customers_observations').".observ_deleted='0' $SearchString");				
		$this->db->limit($limit);
		$result=$this->db->get();
		return $result->num_rows();				
	}
	
	/*Gets information about a particular customer*/
	function get_info($customer_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$customer_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);
			//Get all the fields from customer table
			$fields = $this->db->list_fields('customers');
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}		

	function delete_list($items_ids , $method_type = NULL)
	{   
		if($method_type!='' && $method_type == 'observations')
		{
			$table_name = $this->db->dbprefix('customers_observations');
			$cloum_by = 'observ_id';
		}
		else {
			$table_name = $this->db->dbprefix('customers_history');
			$cloum_by = 'id';
		}		
	    $items_ids = implode(',',$items_ids);
	    $query = "delete from $table_name where $cloum_by in ($items_ids) ";
        $result = $this->db->query( $query );
		return  $result;

 	}

    /*Get search suggestions to find observations */
	function get_search_suggestions($customer_id=NULL,$search,$limit=25)
	{
		$suggestions = array();	
		$query = "select p.first_name,p.last_name from ".$this->db->dbprefix('employees')." e 
				 join ".$this->db->dbprefix('people')." p on (p.person_id = e.person_id) 
				 join ".$this->db->dbprefix('customers_observations')." o on (o.employee_id = e.person_id)
				 where o.observ_deleted = '0' and o.person_id = '".$customer_id."' group by p.first_name order by p.first_name asc";
        $query_data = $this->db->query($query);
		foreach($query_data->result() as $row){	$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);}
		
		$this->db->select('customers_observations.observ_treatment');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('customers_observations.observ_deleted','0');
		$this->db->where('customers_observations.person_id',$customer_id);		
		$this->db->like("customers_observations.observ_treatment",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers_observations.observ_treatment");
		$this->db->order_by("customers_observations.observ_treatment", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->observ_treatment); }
		
		$this->db->select('customers_observations.observ_remarks');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('customers_observations.observ_deleted','0');
		$this->db->where('customers_observations.person_id',$customer_id);
		$this->db->like("customers_observations.observ_remarks",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers_observations.observ_remarks");
		$this->db->order_by("customers_observations.observ_remarks", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->observ_remarks); }
		
		$this->db->select('customers_observations.observ_vomiting');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('customers_observations.observ_deleted','0');
		$this->db->where('customers_observations.person_id',$customer_id);
		$this->db->like("customers_observations.observ_vomiting",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers_observations.observ_vomiting");
		$this->db->order_by("customers_observations.observ_vomiting", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->observ_vomiting); }

		$this->db->select('customers_observations.observ_urine');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('customers_observations.observ_deleted','0');
		$this->db->where('customers_observations.person_id',$customer_id);
		$this->db->like("customers_observations.observ_urine",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers_observations.observ_urine");
		$this->db->order_by("customers_observations.observ_urine", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->observ_urine); }
		
		$this->db->select('customers_observations.observ_stool');
		$this->db->from('customers_observations');
		$this->db->join('employees','employees.person_id=customers_observations.employee_id');
		$this->db->join('people','people.person_id=employees.person_id');
		$this->db->where('customers_observations.observ_deleted','0');
		$this->db->where('customers_observations.person_id',$customer_id);
		$this->db->like("customers_observations.observ_stool",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->group_by("customers_observations.observ_stool");
		$this->db->order_by("customers_observations.observ_stool", "asc");	
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)	{ $suggestions[]=array('label'=> $row->observ_stool); }
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}
	

}

?>

