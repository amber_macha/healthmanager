<?php
class Eyeclinics extends Person
{	

  function exists_eyeclinic($item_id)
	{
		$this->db->select('lab_eyeclinic.id');
		$this->db->from('lab_eyeclinic');
		$this->db->join('people', 'people.person_id = lab_eyeclinic.person_id');
		$this->db->where('lab_eyeclinic.id',$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}	
		
	function valid_Active_Lab_category($value=NULL)
	{   
	  	$lab_investigation = $this->db->dbprefix('lab_category');
		if($value!='') 
		{ 	
		    $ExecuteQuery = $this->db->query("select id from ".$this->db->dbprefix('lab_category')." where id in($value) and category_type='eyeclinic' and status='1' ");
			if($ExecuteQuery->num_rows()>0) return true; else return false;
		}
		return false;
     }	

	function get_all($customer_id = NULL , $from_date = NULL , $to_date = NULL)
	{   $searchQuery ='';
     	if($from_date!='' && $to_date){ $searchQuery = " and c.added_date >= '".$from_date."' and c.added_date <= '".$to_date."' "; }
		$LabTest = $this->db->dbprefix('lab_eyeclinic');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');	
	    $query = "select l.name,l.items_generic_code,l.unit_nhif_price,l.nhif_code_number,e.first_name,e.last_name,t.first_name as technical_first_name, t.last_name as technical_last_name,c.*  from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c.lab_category_id = l.item_id) left join $employees t on (c.tested_by_employee_id = t.person_id) where l.deleted='0' and c.person_id = '".$customer_id."' $searchQuery order by c.added_date asc"; 
		$data=$this->db->query($query);
		return $data;
    }	


	function search($customer_id,$search, $column='added_date',$orderby='desc')
	{
	  $searchquery='';
	  if($search!=''){ $searchquery = " and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%' || l.name like '%".$search."%' || t.first_name like '%".$search."%' || t.last_name like '%".$search."%') || CONCAT(e.first_name,' ',e.last_name) LIKE '%".$search."%'  || CONCAT(t.first_name,' ',t.last_name) LIKE '%".$search."%'"; }
	  if($column==''){ $column=' c.added_date '; }   if($orderby==''){ $orderby='desc'; }
		$LabTest = $this->db->dbprefix('lab_eyeclinic');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');
		$query = "select e.first_name,e.last_name,l.name,l.item_status,t.first_name as technical_first_name, t.last_name as technical_last_name,c.*  from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) left join $employees t on (c.tested_by_employee_id = t.person_id) where l.deleted='0' and c.person_id = '".$customer_id."' $searchquery order by $column $orderby"; 
		$data=$this->db->query($query);
        $commentData='<tr style="cursor: pointer;">
						<th><input type="checkbox" id="select_all"></th>
						<th>'.lang('eyeclinic_list').'</th>
						<th>'.lang('common_doctor_name').'</th>';
		if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
		{				
		$commentData.='<th>'.lang('common_send_to_cashier').'</th>';
		}
		$commentData.='<th>&nbsp;</th>
					   <th>&nbsp;</th>
					   <th>'.lang('date').'</th>
					</tr>';
           foreach($data->result() as $data )
		   { $bgdisabled=''; if($data->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
		     $exist_request = $this->functions->existCashierRequest($customer_id,$category_type='eyeclinic',$data->id);	
			 $commentData.=	'<tr>
						<td width="5%" '.$bgdisabled.'><input type="checkbox" value="'.$data->id.'" id="person_'.$data->id.'"></td>
						<td width="25%" '.$bgdisabled.'>'.ucwords(strtolower($data->name)).'</td>
						<td width="18%" '.$bgdisabled.'>'.ucwords(strtolower($data->first_name)).'&nbsp;'.ucwords(strtolower($data->last_name)).'</td>';						
			if($this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
			{
				if($exist_request) { $commentData .= '<td width="20%" '.$bgdisabled.'><a href="javascript:;"><span class="send_not_request">'.lang('common_send_to_cashier').'</span></a></td>'; }
				else { $commentData.= '<td width="20%" '.$bgdisabled.'><a href="javascript:;" onclick="SendRequestTo_Cashier('.$data->id.')"><span class="send_request">'.lang('common_send_to_cashier').'</span></a></td>'; }
			}						
			 if($data->status==0){ $commentData.='<td width="7%" '.$bgdisabled.'><a class="thickbox" title="'.lang('add_report_form').'" href="eyeclinic/addreport/'.$data->person_id.'/'.$data->id.'/width~550">'.lang('add_post_report').'</a></td>'; }
			 else { $commentData.='<td width="7%" '.$bgdisabled.'><span style="background-color: #FF0000;">'.lang('add_post_report').'</span></td>';}
			 $commentData.='<td width="7%" '.$bgdisabled.'><a class="thickbox" title="'.lang('add_view_report_form').'" href="eyeclinic/viewreport/'.$data->person_id.'/'.$data->id.'/width~700">'.lang('add_view_report').'</a></td>
			 <td width="12%" '.$bgdisabled.'>'.date('d/m/Y',strtotime($data->added_date)).'</td>
			 </tr>';
            }	
            $commentData.='';
            return $commentData; 
	}	

	function SearchLabCatgory( $string=NULL ,$value=NULL)
	{ 
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';$commentData='';
		$catgoryType= " and item_type='1' and service_category='4' and deleted='0'";
		if($value!='') { 	$test2 = explode('-',$value); 
		$tempArraySelected = implode(',',$test2); $valid =  " and item_id NOT in ($tempArraySelected) ";
        $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id in ($tempArraySelected) $catgoryType ");
				if($ExecuteData->num_rows()>0)
				{ 
					foreach($ExecuteData->result() as $result)  
					{ $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }			
					  $SearchData_selected.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.' 
												<div class="labcategory_checkbox">
													<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox"  checked="checked" name="lab_category[]" value="'.$result->item_id.'"> </span>
												</div>
										   </div>';
				 	}	
			 	}	
		} 
	    $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid and name LIKE '".$this->db->escape_like_str($string)."%' order by name ");			
		if($ExecuteData->num_rows()>0)
		 { $ids =array(); 
		   foreach($ExecuteData->result() as $result)  
		   { $ids[] = $result->item_id;
			 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }
             $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.'
								<div class="labcategory_checkbox">
									<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'" > </span>
								</div>
							</div>';
		   }
		    $SearchData.= $SearchData_selected;
		    $ids_str= implode(',',$ids); 
            if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }	
		    $ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id NOT in($ids_str)  $catgoryType order by name");
			if($ExecuteData_pend->num_rows()>0)
			       { $ids2 = array();
		            foreach($ExecuteData_pend->result() as $result)  
					  { $ids2[] = $result->item_id; 
						$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }
					    $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.'
											<div class="labcategory_checkbox">
												<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> </span>
											</div>
										</div>';
					  }
				   }
				//echo 'here u r2'.$SearchData; die;	   
		   return $SearchData ;  
		 }		 
		 else {	 
				$ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid order by name");
				if($ExecuteData_pend->num_rows()>0)
					   { 
						foreach($ExecuteData_pend->result() as $result) 
						  { $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }  
							$SearchData.='<div class="medicine-list" '.$bgdisabled.'>'.$result->name.'
											   <div class="labcategory_checkbox">
													<span style="margin-right:50px;"><input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> </span>
											   </div>
											</div>';
						  }
					   }
				return $SearchData_selected.$SearchData;  
		 }
		 return $SearchData ;
	}			

	function get_Report($customer_id,$item_id)
	{
		$LabTest = $this->db->dbprefix('lab_eyeclinic');
		$employees = $this->db->dbprefix('people');
	    $query = "select r.id, r.test_report_details,r.tested_date, t.first_name as technical_first_name, t.last_name as technical_last_name from $LabTest r left join $employees t on (r.tested_by_employee_id = t.person_id) where r.id = '".$item_id."' and r.person_id = '".$customer_id."' "; 
	    $data=$this->db->query($query)->row();
		return $data;
    }		

	function DeleteLabCatgory($value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';
	  	$lab_investigation = $this->db->dbprefix('lab_category');
		if($value!='') 
		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 
		    $result=$this->db->query("delete from $lab_investigation where id in($tempArraySelected) and category_type='eyeclinic' ");
			if($result) return true; else return  false;
		}
     }	 

	function ActiveDeActivateCatgory($value=NULL)
	{   
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';
	  	$lab_investigation = $this->db->dbprefix('lab_category');
		if($value!='') 
		{ 	$test2 = explode('-',$value); $tempArraySelected = implode(',',$test2); 
		     $ExecuteData=$this->db->query("select * from ".$this->db->dbprefix('lab_category')." where id in($tempArraySelected) and category_type='eyeclinic'");
			 foreach($ExecuteData->result() as $result)  
			  {  
			     if($result->status==0) { $status='1';}  if($result->status==1){  $status =0;  }
		         $sucess = $this->db->query("update ".$this->db->dbprefix('lab_category')." set status='".$status."' where category_type='eyeclinic' and id ='".$result->id."' ");
			  }	
			return true; 

		}else return  false;
     }	 

	function import_excel($data1,$data2)
	{
		$success=false; 
		$query = "select * from ".$this->db->dbprefix('lab_category')." where category_type='eyeclinic' and lab_name='".$data1."' ";
		$ExecuteQuery = $this->db->query($query);
		if($ExecuteQuery->num_rows()>0)	
		{  
		   $result = $this->db->query("update ".$this->db->dbprefix('lab_category')." set lab_name='".$data1."',status ='".$data2."' where category_type='eyeclinic' and lab_name='".$data1."' ");
		}
		else{ 
			$result = $this->db->query("insert into ".$this->db->dbprefix('lab_category')." set lab_name='".$data1."',status ='".$data2."',category_type='eyeclinic' "); 
		}
		return true;
	}
	

	function delete_list($customer_id,$items_ids)
	{  
	    $items_ids = implode(',',$items_ids);
	    $query = "delete from ".$this->db->dbprefix('lab_eyeclinic')." where id in ($items_ids) ";
        $result = $this->db->query( $query );
		return  $result;
 	}		

	/*Returns all the customers	*/
	function get_exportData()
	{ 
		$data=$this->db->query("SELECT * FROM ".$this->db->dbprefix('lab_category')." where category_type='eyeclinic' ORDER BY lab_name asc ");
		return $data;
	}
	
	function saveDiseases($customer_id,$invest_id,$value=NULL,$savetype=NULL)
	{
		$ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$employee_id = $login_uerData->person_id;		
		if($savetype=='mtuha_daignoses')  {	$list_diseases = $this->db->dbprefix('list_diseases'); }
		if($savetype=='daibetes')  		  {	$list_diseases = $this->db->dbprefix('list_diabetes'); } 
		if($value!='') 
		{ 	$ArraySelected = explode('-',$value); 
    	     $sucess='';
			for($k=0; $k < count($ArraySelected) ; $k++)
			  {
			     $ExecuteQuery = $this->db->query("select id from $list_diseases where person_id = '".$customer_id."' and lab_category_id = '".$ArraySelected[$k]."' and eyeclinic_id='".$invest_id."' order by added_date desc limit 0,1");
			     if($ExecuteQuery->num_rows()==0)
				   {
				     $sucess = $this->db->query("insert into $list_diseases set lab_category_id ='".$ArraySelected[$k]."', eyeclinic_id='".$invest_id."', person_id = '".$customer_id."',added_by_employee_id='".$login_uerData->person_id."',added_date='".date('Y-m-d')."', status='0' ");
				   }			   
		       } 
			 if($sucess) return true;  else return  false;
	    }else return  false;
	}

    /*	Get search suggestions to find customers */
	function get_search_suggestions($customer_id=NULL,$search,$limit=25)
	{
		$suggestions = array();
     	$LabTest = $this->db->dbprefix('lab_eyeclinic');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');
	    
		$query = "select e.first_name,e.last_name from $LabTest c 
		left join $employees e on (c.added_by_employee_id = e.person_id) 
		left join $lab_category l on (c. lab_category_id = l.item_id) 
		left join $employees t on (c.tested_by_employee_id = t.person_id) where c.status='0' and l.item_status='0' and l.deleted='0' and c.person_id='".$customer_id."' and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%' || CONCAT(e.first_name,' ',e.last_name) LIKE '%".$search."%' ) group by e.first_name order by e.first_name asc";
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);
		}
		
		$query = "select l.name from $LabTest c 
		left join $employees e on (c.added_by_employee_id = e.person_id) 
		left join $lab_category l on (c. lab_category_id = l.item_id) 
		left join $employees t on (c.tested_by_employee_id = t.person_id) where c.status='0' and l.item_status='0' and l.deleted='0' and c.person_id='".$customer_id."' and l.name like '%".$search."%' group by l.name order by l.name asc";
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->name);
		}
		
		$query = "select t.first_name as technical_first_name, t.last_name as technical_last_name from $LabTest c 
		left join $employees e on (c.added_by_employee_id = e.person_id) 
		left join $lab_category l on (c. lab_category_id = l.item_id) 
		left join $employees t on (c.tested_by_employee_id = t.person_id) where c.status='0' and l.item_status='0' and l.deleted='0' and c.person_id='".$customer_id."' and (t.first_name like '%".$search."%' || t.first_name like '%".$search."%' || CONCAT(t.first_name,' ',t.first_name) LIKE '%".$search."%' ) group by t.first_name order by t.first_name asc";
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->technical_first_name.' '.$row->technical_last_name);
		}		
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}	
	

 }

?>

