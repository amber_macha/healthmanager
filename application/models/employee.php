<?php
class Employee extends Person
{
	
	/*Determines if a given person_id is an employee*/
	function Is_Exist_Email($email_id = NULL, $person_id = NULL)
	{
		$this->db->from('employees');	
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.deleted','0');
		$this->db->where('people.email',$email_id);
		if($person_id!='-1') { $this->db->where('employees.person_id !=',$person_id); }
		$query = $this->db->get();		
		return ($query->num_rows()==1);
	}	
	
	/*Determines if a given person_id is an employee*/
	function exists($person_id)
	{
		$this->db->from('employees');	
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.person_id',$person_id);
		$query = $this->db->get();
		
		return ($query->num_rows()==1);
	}	
	
	/*Returns all the employees*/
	function get_all($limit=10000, $offset=0,$col='last_name',$order='asc')
	{	
		$employees=$this->db->dbprefix('employees');
		$people=$this->db->dbprefix('people');
		$data=$this->db->query("SELECT * 
						FROM ".$people."
						STRAIGHT_JOIN ".$employees." ON 										                       
						".$people.".person_id = ".$employees.".person_id
						WHERE deleted =0 and $employees.person_id!='1' ORDER BY ".$col." ". $order." 
						LIMIT  ".$offset.",".$limit);
		return $data;
	}
	
	function count_all()
	{
		$this->db->from('employees');
		$this->db->where('deleted',0);
		$this->db->where('person_id !=','1');
		return $this->db->count_all_results();
	}
	
	/*Returns all the employees*/
	function get_All_Employee_By_Groups($Group_Id = NULL, $Sub_Group_Id = NULL,$offset=0 , $limit=10000)
	{	
		$employees=$this->db->dbprefix('employees');
		$people=$this->db->dbprefix('people');
		$where ='';
		if($Group_Id!='') { $where .= "and $employees.employee_group_id ='".$Group_Id."'" ; }
		if($Sub_Group_Id!='') { $where .= "and $employees.employee_sub_group_id ='".$Sub_Group_Id."'" ; }
		$data = $this->db->query("SELECT ".$employees.".person_id FROM ".$people." STRAIGHT_JOIN ".$employees." ON ".$people.".person_id = ".$employees.".person_id
						WHERE 1=1 $where and ".$employees.".deleted ='0' LIMIT  ".$offset.",".$limit);
		return $data;
	}
	
	
	/*Gets information about a particular employee*/
	function get_info($employee_id)
	{
		$this->db->from('employees');	
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('employees.person_id',$employee_id);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $employee_id is NOT an employee
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from employee table
			$fields = $this->db->list_fields('employees');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	/*
	Gets information about multiple employees
	*/
	function get_multiple_info($employee_ids)
	{
		$this->db->from('employees');
		$this->db->join('people', 'people.person_id = employees.person_id');		
		$this->db->where_in('employees.person_id',$employee_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	/*
	Inserts or updates an employee
	*/
	function save(&$person_data, &$employee_data,&$permission_data, &$permission_action_data, $employee_id=false)
	{
		$success=false;		
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();			
		if(parent::save($person_data,$employee_id))
		{
			if (!$employee_id or !$this->exists($employee_id))
			{
				$employee_data['person_id'] = $employee_id = $person_data['person_id'];
				$success = $this->db->insert('employees',$employee_data);
			}
			else
			{
				$this->db->where('person_id', $employee_id);
				$success = $this->db->update('employees',$employee_data);		
			}			
			//We have either inserted or updated a new employee, now lets set permissions. 
			if($success)
			{
				//First lets clear out any permissions the employee currently has.
				$success=$this->db->delete('permissions', array('person_id' => $employee_id));				
				//Now insert the new permissions
				if($success)
				{
					foreach($permission_data as $allowed_module)
					{
						$success = $this->db->insert('permissions',
						array(
						'module_id'=>$allowed_module,
						'person_id'=>$employee_id));
					}
				}				
				//First lets clear out any permissions actions the employee currently has.
				$success=$this->db->delete('permissions_actions', array('person_id' => $employee_id));				
				if($success)
				{
					foreach($permission_action_data as $permission_action)
					{
						list($module, $action) = explode('|', $permission_action);
						$success = $this->db->insert('permissions_actions',
						array(
						'module_id'=>$module,
						'action_id'=>$action,
						'person_id'=>$employee_id));
					}
				}
			}			
		}		
		$this->db->trans_complete();		
		return $success;
	}
	
	/*
	Deletes one employee
	*/
	function delete($employee_id)
	{
		$success=false;
		
		//Don't let employee delete their self
		if($employee_id==$this->get_logged_in_employee_info()->person_id)
			return false;
		
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		
		//Delete permissions
		if($this->db->delete('permissions', array('person_id' => $employee_id)) && $this->db->delete('permissions_actions', array('person_id' => $employee_id)))
		{	
			$this->db->where('person_id', $employee_id);
			$success = $this->db->update('employees', array('deleted' => 1));
		}
		$this->db->trans_complete();		
		return $success;
	}
	
	/*
	Deletes a list of employees
	*/
	function delete_list($employee_ids)
	{
		$success=false;
		
		//Don't let employee delete their self
		if(in_array($this->get_logged_in_employee_info()->person_id,$employee_ids))
			return false;

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->where_in('person_id',$employee_ids);
		//Delete permissions
		if ($this->db->delete('permissions'))
		{
			//delete from employee table
			$this->db->where_in('person_id',$employee_ids);
			$success = $this->db->update('employees', array('deleted' => 1));
		}
		$this->db->trans_complete();		
		return $success;
 	}
	
	/*
	Get search suggestions to find employees
	*/
	function get_search_suggestions($search,$limit=5,$method_type)
	{
		$suggestions = array();
		if($method_type=='history_activity')
		{   $employee_id= $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];
		    $this->db->select('activity_type');
			$this->db->from('employees_manage_activities');
			$this->db->where('person_id', $employee_id);
			$this->db->where('deleted', 0);
			$this->db->like("activity_type",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("activity_type", "asc");
			$this->db->group_by("activity_type");				
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row){ $suggestions[]=array('label'=> $row->activity_type); }
			
			$this->db->select('activity_type_category');
			$this->db->from('employees_manage_activities');
			$this->db->where('person_id', $employee_id);
			$this->db->where('deleted', 0);
			$this->db->like("activity_type_category",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("activity_type_category", "asc");
			$this->db->group_by("activity_type_category");				
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row){ $suggestions[]=array('label'=> $row->activity_type_category); }
			
			$this->db->select('activity_description');
			$this->db->from('employees_manage_activities');
			$this->db->where('person_id', $employee_id);
			$this->db->where('deleted', 0);
			$this->db->like("activity_description",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("activity_description", "asc");
			$this->db->group_by("activity_description");				
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row){ $suggestions[]=array('label'=> $row->activity_description); }			
		}
	  else 
		{
			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 and ".$this->db->dbprefix('employees').".person_id !='1'");
			$this->db->order_by("last_name", "asc");		
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);		
			}
			
			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');
			$this->db->where('deleted', 0);
			$this->db->where($this->db->dbprefix('employees').'.person_id !=', '1');
			$this->db->like("email",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("email", "asc");		
			$by_email = $this->db->get();
			foreach($by_email->result() as $row)
			{
				$suggestions[]=array('label'=> $row->email);		
			}
			
			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');	
			$this->db->where('deleted', 0);
			$this->db->where($this->db->dbprefix('employees').'.person_id !=', '1');
			$this->db->like("username",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("username", "asc");		
			$by_username = $this->db->get();
			foreach($by_username->result() as $row)
			{
				$suggestions[]=array('label'=> $row->username);		
			}

			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');	
			$this->db->where('deleted', 0);
			$this->db->where($this->db->dbprefix('employees').'.person_id !=', '1');
			$this->db->like("phone_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("phone_number", "asc");		
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row)
			{
				$suggestions[]=array('label'=> $row->phone_number);		
			}
			
			$this->db->select('employees_groups.group_name');
			$this->db->from('employees');
			$this->db->join('employees_groups','employees.employee_group_id=employees_groups.id');	
			$this->db->where('deleted', 0);
			$this->db->where($this->db->dbprefix('employees').'.person_id !=', '1');
			$this->db->like("employees_groups.group_name",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
			$this->db->order_by("employees_groups.group_name", "asc");
			$this->db->group_by("employees_groups.group_name");				
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row)
			{
				$suggestions[]=array('label'=> $row->group_name);		
			}
		}
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	
	}	
	
	/*Preform a search on employees	*/
	function search($search, $limit=20,$offset=0,$column='last_name',$orderby='asc')
	{
		if ($this->config->item('speed_up_search_queries'))
		{

			$query = "
				select *
			from (
           	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name
           	from ".$this->db->dbprefix('employees')."
           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
           	right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
			where first_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
           	order by `".$column."` ".$orderby.") union

		 	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
           	from ".$this->db->dbprefix('employees')."
           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
			right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id           
		    where last_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
           	order by `".$column."` ".$orderby.") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name
         	from ".$this->db->dbprefix('employees')."
          	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
          	right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
			where email like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
          	order by `".$column."` ".$orderby.") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name
        	from ".$this->db->dbprefix('employees')."
        	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
        	right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
			where phone_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
        	order by `".$column."` ".$orderby.") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name
      		from ".$this->db->dbprefix('employees')."
      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
			right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
			where username like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
      		order by `".$column."` ".$orderby.") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name 
      		from ".$this->db->dbprefix('employees')."
      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
			right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
      		where ".$this->db->dbprefix('employees_groups')." group_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
      		order by `".$column."` ".$orderby.") union
			
			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted, ".$this->db->dbprefix('employees_groups').".group_name
    		from ".$this->db->dbprefix('employees')."
    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
    		right_join ".$this->db->dbprefix('employees_groups')." ON ".$this->db->dbprefix('employees').".employee_group_id = ".$this->db->dbprefix('employees_groups').".id
			where CONCAT(`first_name`,' ',`last_name`)  like '".$this->db->escape_like_str($search)."%' and deleted = 0 and $this->db->dbprefix('employees').person_id !='1'
    		order by `".$column."` ".$orderby.")
			) as search_results
			order by `".$column."` ".$orderby." limit ".$this->db->escape((int)$offset).', '.$this->db->escape((int)$limit);

			return $this->db->query($query);
		}
		else
		{ 
			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');	
			$this->db->join('employees_groups','employees.employee_group_id=employees_groups.id','left');	
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			email LIKE '%".$this->db->escape_like_str($search)."%' or 
			phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
			username LIKE '%".$this->db->escape_like_str($search)."%' or 
			group_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 and ".$this->db->dbprefix('employees').".person_id !='1' ");	
			$this->db->order_by($column, $orderby);
			$this->db->limit($limit);
			$this->db->offset($offset);
			return $this->db->get();
		}
	}
	
	function search_count_all($search, $limit=10000)
	{
		if ($this->config->item('speed_up_search_queries'))
		{

			$query = "
				select *
			from (
           	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
           	from ".$this->db->dbprefix('employees')."
           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
           	where first_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
           	order by `last_name` asc limit ".$this->db->escape($limit).") union

		 	(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
           	from ".$this->db->dbprefix('employees')."
           	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
           	where last_name like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
           	order by `last_name` asc limit ".$this->db->escape($limit).") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
         	from ".$this->db->dbprefix('employees')."
          	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
          	where email like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
          	order by `last_name` asc limit ".$this->db->escape($limit).") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
        	from ".$this->db->dbprefix('employees')."
        	join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
        	where phone_number like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
        	order by `last_name` asc limit ".$this->db->escape($limit).") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
      		from ".$this->db->dbprefix('employees')."
      		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
      		where username like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
      		order by `last_name` asc limit ".$this->db->escape($limit).") union

			(select ".$this->db->dbprefix('people').".*, ".$this->db->dbprefix('employees').".username, ".$this->db->dbprefix('employees').".deleted
    		from ".$this->db->dbprefix('employees')."
    		join ".$this->db->dbprefix('people')." ON ".$this->db->dbprefix('employees').".person_id = ".$this->db->dbprefix('people').".person_id
    		where CONCAT(`first_name`,' ',`last_name`)  like '".$this->db->escape_like_str($search)."%' and deleted = 0 and ".$this->db->dbprefix('employees').".person_id !='1'
    		order by `last_name` asc limit ".$this->db->escape($limit).")
			) as search_results
			order by `last_name` asc limit ".$this->db->escape($limit);

			$result=$this->db->query($query);
			return $result->num_rows();
		}
		else
		{
			$this->db->from('employees');
			$this->db->join('people','employees.person_id=people.person_id');	
			$this->db->join('employees_groups','employees.employee_group_id=employees_groups.id','left');			
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			email LIKE '%".$this->db->escape_like_str($search)."%' or 
			phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
			username LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 and ".$this->db->dbprefix('employees').".person_id !='1'");		
			$this->db->order_by("last_name", "asc");
			$result=$this->db->get();				
			return $result->num_rows();
		}
	}
	
	/*check mployee expire password info.*/
	function is_Expire_Password($username,$password)
	{   if($username == '' || $password == '') { return(1); }
		$this->db->select('change_password_date');	
		$this->db->from('employees');	
		$this->db->where('username',$username);
		$this->db->where('password',md5($password));
		$Query = $this->db->get();	
	    if($Query->num_rows()>0)
		{
			$QueryData = $Query->row();	
			if($QueryData->change_password_date =='' || $QueryData->change_password_date =='0000-00-00 00:00;00') { return(3);}
			$now = time(); 
			$password_date = strtotime($QueryData->change_password_date);
			$datediff = $now - $password_date;
			$expire_days = floor($datediff/(60*60*24));
			$default_expire_days = $this->config->item('DEFAULT_EXPIRE_DAYS_LIMIT');
	        if($expire_days > $default_expire_days) return(4); else return (5);  
        }
		return(2);
	}
	
	/*Attempts to login employee and set session. Returns boolean based on outcome.*/
	function login($username, $password)
	{
		$query = $this->db->get_where('employees', array('username' => $username,'password'=>md5($password), 'deleted'=>0), 1);
		if ($query->num_rows() ==1)
		{
			$row=$query->row();
			$current_date = date('d-m-Y H:i:s');//$current_date = time();
			$Login_Data = array('person_id'					=> $row->person_id,
								'EMPLOYEE_USERNAME'   		=> $row->username,
								'EMPLOYEE_ACCOUNT_CODE'   	=> $row->employee_account_code,
							    'EMPLOYEE_START_LOGIN_TIME' => $current_date,
							    'EMPLOYEE_END_LOGIN_TIME'   => $current_date,
							    'EMPLOYEE_PSSWORD_AUTHROZIE'=> '1'
							   );
		   $this->session->set_userdata($Login_Data); 
           $Activity_Data = array('person_id' => $row->person_id,'activity_type' => 'login','activity_description'=> lang('employees_login_successful'));	
           $this->save_activites($Activity_Data,$item_id=-1);								 
		   $this->make_inactive_tika_patient_account();
		   return true;
		}
		return false;
	}	
	
	function get_logged_in_employee_info()
	{  
		if($this->is_logged_in())
		{
			return $this->get_info($this->session->userdata('person_id'));
		}
		
		return false;
	}
	
	 function get_Logged_SessionData($val)
	 {		
		$manage_admin_Val = $this->session->userdata($val);
		if (isset ( $manage_admin_Val ) && ! empty ( $manage_admin_Val )) {
			return $manage_admin_Val;
		} else {
			return FALSE;
		}
	 }
	 
	/*Logs out a user by destorying all session data and redirect to login*/
	function logout()
	{
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'logout','activity_description'=> lang('employees_logout_successful'));
		$this->save_activites($Activity_Data,$item_id=-1);	
		$this->session->sess_destroy();
		redirect('login');
	}
	
	/*
	Determins if a employee is logged in
	*/
	function is_logged_in()
	{
		return $this->session->userdata('person_id')!=false;
	}	
	
	function authentication_check($password)
	{
		$pd=$this->session->userdata('person_id');
	    $pass=md5($password);
		$query = $this->db->get_where('employees', array('person_id' => $pd,'password'=>$pass), 1);
		return $query->num_rows() == 1;
	}
	
	/*
	Determins whether the employee specified employee has access the specific module.
	*/
	function has_module_permission($module_id,$person_id)
	{
		//if no module_id is null, allow access
		if($module_id==null)
		{
			return true;
		}
		
		$query = $this->db->get_where('permissions', array('person_id' => $person_id,'module_id'=>$module_id), 1);
		return $query->num_rows() == 1;
	}
	
	function has_module_action_permission($module_id, $action_id, $person_id)
	{
		//if no module_id is null, allow access
		if($module_id==null)
		{
			return true;
		}
		
		$query = $this->db->get_where('permissions_actions', array('person_id' => $person_id,'module_id'=>$module_id,'action_id'=>$action_id), 1);
		return $query->num_rows() == 1;
	}
	
	function get_employee_by_username_or_email($username_or_email)
	{
		$this->db->from('employees');	
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->where('username',$username_or_email);
		$this->db->or_where('email',$username_or_email);
		$query = $this->db->get();
		
		if ($query->num_rows() == 1)
		{
			return $query->row();
		}
		
		return false;
	}
	
	function update_employee_password($employee_id, $password)
	{
		$employee_data = array('password' => $password,'change_password_date' => date('Y-m-d H:i:s'));
		$this->db->where('person_id', $employee_id);
		$success = $this->db->update('employees',$employee_data);		
		return $success;
	}
	
	/* make disabled account of Tika Patient customer profile after one years from registration date */	
	function make_inactive_tika_patient_account()
	{
	  $current_date = date('Y-m-d');
	  $people    = $this->db->dbprefix('people');
	  $customers = $this->db->dbprefix('customers');
	  $Execute_DataQuery = $this->db->query("select p.person_id,p.registration_date from $people p join $customers c on (p.person_id = c.person_id)  where c.patient_category='1' ");
	  if(isset($Execute_DataQuery) && $Execute_DataQuery!='' && $Execute_DataQuery->num_rows()>0)
	  {
	      foreach($Execute_DataQuery->result() as $data)
		   { $valid_time_for_expire = $this->config->item('config_tika_patient_expire_time');
		     $Registration_ExpireDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($data->registration_date)) . " + $valid_time_for_expire"));			
		     if($current_date > $Registration_ExpireDate)  {   $this->db->query("update $customers set deleted ='1' where person_id='".$data->person_id."' "); }
		   }
	  }	
	}
	
	function get_AllEmployee_Groups( $parent_id=NULL )
	{
	   if($parent_id!='' && $parent_id < 1) { $parent_id='0'; }
		$this->db->from('employees_groups');	
		$this->db->where('parent_id',$parent_id); 
		$this->db->where('status','0');		
		$this->db->order_by('group_name');
	    return $this->db->get();
	}	
	
	function Get_Employee_GroupName($group_id)
	{
		$this->db->select('group_name');	
		$this->db->from('employees_groups');	
		$this->db->where('id',$group_id);
		$Query = $this->db->get();		
	    if($Query->num_rows()==1){ $QueryData = $Query->row(); return $QueryData->group_name;}
		else return false;
	}
	
	function Get_Employee_SubGroupName($group_id,$subgroup_id)
	{
		$this->db->select('group_name');	
		$this->db->from('employees_groups');	
		$this->db->where('parent_id',$group_id); 
		$this->db->where('id',$subgroup_id);
		$Query = $this->db->get();		 
	    if($Query->num_rows()==1){ $QueryData = $Query->row(); return $QueryData->group_name;}
		else return false;
	}
	
	function get_Sub_Groups($parent_id = NULL, $sub_group_id = NULL)
	{ 
		if(!$parent_id || $parent_id=='0') {return false;}		
		$Sub_Group_List =  $this->get_AllEmployee_Groups($parent_id);		
		$option_string = '';
		$option_string .= '<div class="field_row clearfix">'.form_label(lang("common_subgroup_name").':', 'subgroup_name').'
					       <div class="form_field" id="sub_group_list">
						   <select class="valid" id="sub_group_name" name="sub_group_name" style="width: 193px;" onchange="viewSubCategoryItem(this.value);">
						   <option value="">'.lang("common_select_type").'</option>';
		if(isset($Sub_Group_List) && $Sub_Group_List!='' && $Sub_Group_List->num_rows()>0)
		{			 
		    foreach($Sub_Group_List->result() as $rowdata)
		    {  	$selected ='';
				if($rowdata->id==$sub_group_id){ $selected ='selected="selected"'; }
				$option_string .='<option value="'.$rowdata->id.'" '.$selected.'>'.ucfirst($rowdata->group_name).'</option>';
			}				
		}
		$option_string .= '</select></div></div>';
        return $option_string;		
	}
	
	//******** This is the back of sub caegory function get_Sub_Groups()**************//
	function get_Sub_Groups_backup($parent_id = NULL, $sub_group_id = NULL)
	{ 
		if(!$parent_id || $parent_id=='0') {return false;}		
		$Sub_Group_List =  $this->get_AllEmployee_Groups($parent_id);		
		$option_string = '';
		if(isset($Sub_Group_List) && $Sub_Group_List!='' && $Sub_Group_List->num_rows()>0)
		{
			$option_string .= '<div class="field_row clearfix">'.form_label(lang("common_subgroup_name").':', 'subgroup_name').'
							   <div class="form_field" id="sub_group_list">
							   <select class="valid" id="sub_group_name" name="sub_group_name" style="width: 193px;" onchange="viewSubCategoryItem(this.value);">
							   <option value="">'.lang("common_select_type").'</option>'; 
		    foreach($Sub_Group_List->result() as $rowdata)
		    {  
				$selected ='';
				if($rowdata->id==$sub_group_id){ $selected ='selected="selected"'; }
				$option_string .='<option value="'.$rowdata->id.'" '.$selected.'>'.ucfirst($rowdata->group_name).'</option>';
			}
			$option_string .= '</select></div></div>';	
		}
        return $option_string;		
	}
	
	function save_activites( $item_data , $item_id=false)
	{   
		if($this->db->insert($table_name='employees_manage_activities',$item_data))
		{
			return true;
		}
		return false;
	}
	
	function get_all_activities($limit=10000, $offset=0,$col='added_date',$order='desc')
	{   $string='';
	    $employee_id = $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];  
        if(isset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']) && $_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']!='' && $_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']!='')	
		{ $string = " and ( added_date >='".$_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']."' and added_date <= '".$_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']."') "; }
		$data=$this->db->query("SELECT * 
						FROM ".$this->db->dbprefix('employees_manage_activities')."
						WHERE person_id='".$employee_id."' and deleted ='0' $string ORDER BY ".$col." ". $order." 
						LIMIT  ".$offset.",".$limit);
		return $data;
	}
	
	function count_all_activities()
	{   $string='';
	    $employee_id = $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];  
        if(isset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']) && $_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']!='' && $_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']!='')	
		{ $string = " and ( added_date >='".$_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']."' and added_date <= '".$_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']."') "; }
		$ExecuteData = $this->db->query("SELECT * FROM ".$this->db->dbprefix('employees_manage_activities')." WHERE person_id='".$employee_id."' and deleted ='0' $string ");						
		return $ExecuteData->num_rows();
	}
	
		/*
	Preform a search on employees
	*/
	function search_activities($search, $limit=20,$offset=0,$column='added_date',$orderby='desc')
	{
		$string='';
	    $employee_id = $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];  
		if(isset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']) && $_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']!='' && $_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']!='')	
		{ $string = " and ( added_date >='".$_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']."' and added_date <= '".$_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']."') "; }
		$Query = "SELECT * FROM ".$this->db->dbprefix('employees_manage_activities')." 
				  where person_id='".$employee_id."' and deleted='0' $string and (
				  activity_type LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_type_category LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_items_id LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_description LIKE '%".$this->db->escape_like_str($search)."%' ) 
				  ORDER BY added_date desc LIMIT  ".$offset.",".$limit; 
		$ExcuteQuery = $this->db->query($Query);
		return $ExcuteQuery;		
	}
	
	function search_count_all_activities($search, $limit=10000)
	{	
		$string='';
	    $employee_id = $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];  
		if(isset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']) && $_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']!='' && $_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']!='')	
		{ $string = " and ( added_date >='".$_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']."' and added_date <= '".$_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']."') "; }
		$Query = "SELECT id FROM ".$this->db->dbprefix('employees_manage_activities')." 
				  where person_id='".$employee_id."' and deleted='0' $string and (
				  activity_type LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_type_category LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_items_id LIKE '%".$this->db->escape_like_str($search)."%' or
				  activity_description LIKE '%".$this->db->escape_like_str($search)."%' ) "; 
		$ExcuteQuery = $this->db->query($Query);
		return $ExcuteQuery->num_rows();
	}
	
	function Search_Employee_List($string) 
	{
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('employees');
		$StringData='';
		$QueryData = $this->db->query("SELECT c.username,p.person_id,p.first_name,p.last_name FROM $people p STRAIGHT_JOIN $customers c ON  p.person_id = c.person_id WHERE deleted =0 and (c.username like '".$string."%' 
									   OR p.first_name like '".$string."%' OR p.last_name like '".$string."%' OR p.person_id like '".$string."%')");		
		$StringData =' <ul class="ipd_search_list ui-autocomplete ui-menu ui-widget ui-widget-content ">';
		$i=0;
		if($QueryData->num_rows()>0)
		{  
			foreach($QueryData->result() as $data)
			{			
			  $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" onclick="getPFnumberData(\''.$data->person_id.'\', \''.addslashes($data->first_name).' '.addslashes($data->last_name).'\')" >'.$data->person_id.' - '.$data->first_name.' '.$data->last_name.'</a></li>';	
            }			
		} 
		else { $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" >Sorry No Records</a></li>'; }
		$StringData .='</ul>';	
		return $StringData ;
	}
	
	function is_valid_Pass($password,$employee_id)
	{   if($employee_id=='' || $employee_id=='') { return false; }
		$this->db->select('person_id');	
		$this->db->from('employees');	
		$this->db->where('person_id',$employee_id);
		$this->db->where('password',$password);
		$Query = $this->db->get();		
	    if($Query->num_rows()>0) return true; else return false;
	}
	
	function password_expire_message()
	{ 
		$employee_id   = $this->Employee->get_Logged_SessionData('person_id');	
		$employee_info = $this->get_info($employee_id);
		$now = time(); 
		$datediff = $now - strtotime($employee_info->change_password_date);
		$days = floor($datediff/(60*60*24));
		$expire_days = $this->config->item('DEFAULT_EXPIRE_DAYS_LIMIT') - $days;
		$message = "Your password is valid only for $expire_days days";
		return $message; 
	} 
	
	function Generate_Employee_Account()
	 {
        $this->db->select('employees.employee_account_code');
		$this->db->from('employees');		
		$this->db->join('people', 'people.person_id = employees.person_id');
		$this->db->order_by('employees.person_id', 'desc');
		$this->db->limit('1');
		$ExecuteQuery = $this->db->get()->row();		
		if(isset($ExecuteQuery->employee_account_code) && $ExecuteQuery->employee_account_code!='')
		 {		    
			$TempArray_value = explode('/',$ExecuteQuery->employee_account_code);
			if( $TempArray_value[1]!='')
			{
				$num = $TempArray_value[1]+1;	
                $account_number  = $this->config->item('hospital_facility_id').'/'.$num;	
			}
           else	{ $account_number= $this->config->item('hospital_facility_id').'/1';  }
		   } 
		else { $account_number  = $account_number  = $this->config->item('hospital_facility_id').'/1'; }
		return $account_number;		
	}
	
	
}
?>
