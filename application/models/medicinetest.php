<?php
class Medicinetest extends Person
{	
	function valid_Lab_Test($customer_id, $lab_category)
	{
	   $lab_investigation = $this->db->dbprefix('lab_investigation');	
	    $SearchQuery = '';
		if(count($lab_category)>0)
		{
			for($i=0; $i<count($lab_category); $i++)
			{
			   if($SearchQuery=='') { $SearchQuery = "and ( lab_category_id ='".$lab_category[$i]."' "; }  else { $SearchQuery.= " OR lab_category_id ='".$lab_category[$i]."' "; }			}
			   if($SearchQuery!='') { $SearchQuery.=' )'; }
		}	   	   
        $query = "select * from  $lab_investigation  where person_id='".$customer_id."' $SearchQuery and added_date like '".date('Y-m-d')."%'  " ;		$res = $this->db->query($query);	//echo $res->num_rows();	die;	
		if($res->num_rows()>0) return true; else return false;		
    }	
	function get_all($customer_id = NULL , $from_date = NULL , $to_date = NULL)
	{   $searchQuery ='';
     	if($from_date!='' && $to_date){ $searchQuery = " and c.added_date >= '".$from_date."' and c.added_date <= '".$to_date."' "; }
		$LabTest = $this->db->dbprefix('lab_medicine');
		$employees = $this->db->dbprefix('people');
		$lab_category = $this->db->dbprefix('items');	
	    $data = $this->db->query("select l.name,l.items_generic_code,l.unit_nhif_price,l.nhif_code_number,e.first_name,e.last_name,c.* from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) where l.deleted='0' and c.person_id = '".$customer_id."' $searchQuery "); 		return $data;	}	
	function get_medicine_data_ByID($customer_id,$item_ids)	{		$LabTest = $this->db->dbprefix('lab_medicine');		$employees = $this->db->dbprefix('people');		$lab_category = $this->db->dbprefix('items');		    $query = "select e.first_name,e.last_name,l.name,c.* from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id)  where l.deleted='0' and c.id in(".$item_ids.") and c.person_id = '".$customer_id."' "; 		$data=$this->db->query($query);			return $data;	}	
	function search($customer_id,$search, $column='added_date',$orderby='asc')
	{
	  $searchquery='';
	  if($search!=''){ $searchquery = " and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%' || l.name like '%".$search."%') "; }	  if($column==''){ $column=' c.added_date '; }   if($orderby==''){ $orderby='asc'; }	  $LabTest = $this->db->dbprefix('lab_medicine');	  $employees = $this->db->dbprefix('people');	  $lab_category = $this->db->dbprefix('items');	  $query = "select e.first_name,e.last_name,l.name,l.item_status,c.*  from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) where l.deleted='0' and c.person_id = '".$customer_id."' and c.status='0' $searchquery order by $column $orderby"; 	  $data=$this->db->query($query);      $commentData='';      $commentData.='<tr style="cursor: pointer;">					<th><input type="checkbox" id="select_all"></th>					<th>'.lang('medicine_drugs_list').'</th>					<th>'.lang('common_doctor_name').'</th>									<th>'.lang('medicine_quantity').'</th>					<th>'.lang('medicine_dispensing').'</th>					<th><input class="print_selected_all" type="button" value="'.lang('medicine_print_selected').'" onclick="Print_Selected_Dosage();" ></th>					<th>'.lang('date').'</th>					</tr>';           foreach($data->result() as $data )		   { $bgdisabled=''; if($data->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 			 if($data->quantity_dispansed=='1'){ $dosage_class = 'dosage_dispansed_green'; $medicine_dispensed_lang = lang('medicine_dispensed'); }			 else { $dosage_class = 'dosage_dispansed_red'; $medicine_dispensed_lang = lang('medicine_not_dispensed'); } 
			 $commentData.=	'<tr>					<td width="5%" '.$bgdisabled.'><input type="checkbox" value="'.$data->id.'" id="person_'.$data->id.'"></td>					<td width="35%" '.$bgdisabled.'>'.ucwords(strtolower($data->name)).'</td>					<td width="25%" '.$bgdisabled.'>'.ucwords(strtolower($data->first_name)).'&nbsp;'.ucwords(strtolower($data->last_name)).'</td>					<td width="7%" '.$bgdisabled.'>'.$data->quantity.'</td>';			 if($data->quantity_dispansed=='1')
			 { 		
				$commentData.=	'<td width="7%" '.$bgdisabled.'><input class="dosage_dispansed_green" type="button" value="'.lang('medicine_dispensed').'"></td>';			 }
			 else { $commentData.=	'<td width="7%" '.$bgdisabled.'><input class="dosage_dispansed_red" type="button" value="'.lang('medicine_not_dispensed').'" onclick="Change_Status('.$data->id.')" ></td>';}
			 $commentData.=	'<td width="7%" '.$bgdisabled.'><input class="dosage_print" type="button" value="'.lang('medicine_print_this').'" onclick="Print_Dosage('.$data->id.')" ></td>					<td width="15%" '.$bgdisabled.'>'.date('d/m/Y',strtotime($data->added_date)).'</td> 					</tr>'; 
		  }	
          $commentData.='';
          return $commentData; 	
	}	
	function SearchLabCatgory( $string ,$value=NULL,$customer_id)
	{    
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid='';$commentData='';
		$catgoryType= " and item_type='0' and deleted='0'";
	  	$lab_investigation = $this->db->dbprefix('items');
		if($value!='') { 	$test2 = explode('-',$value); 
		$tempArraySelected = implode(',',$test2); $valid =  " and item_id NOT in ($tempArraySelected) ";
        $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id in ($tempArraySelected) $catgoryType ");
		if($ExecuteData->num_rows()>0)
		{ 
			foreach($ExecuteData->result() as $result)  
			{   $lab_id= $result->item_id;  $class_name='dosage_green'; 				$dosage_description = $this->get_medicine_drugs_dosage($result->item_id,$customer_id); 				if(isset($dosage_description) && $dosage_description!=''){ $class_name='dosage_red'; } 
				$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 				$SearchData_selected.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">										<input class="isAgeSelected" type="checkbox"  checked="checked" name="lab_category[]" value="'.$lab_id.'"> &nbsp;										<input type="textbox" style="width:50px;padding: 2px;height: 15px;" name="quntity_'.$result->item_id.'" id="quntity_'.$result->item_id.'" value=""> 										<input type="button" style="width:70px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="updateDosageDesciption('.$result->item_id.')" value="'.lang("medicine_dosage_title").'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 										</div></div>';
			}	
		}	
		} 
	    $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid and name LIKE '".$this->db->escape_like_str($string)."%' order by name ");					if($ExecuteData->num_rows()>0)
		 { $ids =array(); 
		   foreach($ExecuteData->result() as $result)  
		   { 			 $ids[] = $result->item_id; $class_name='dosage_green'; 			 $dosage_description = $this->get_medicine_drugs_dosage($result->item_id,$customer_id);			 if(isset($dosage_description) && $dosage_description!=''){ $class_name='dosage_red'; }
			 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
             $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">							<input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'" > &nbsp; 							<input type="textbox" style="width:50px;padding: 2px;height: 15px;" name="quntity_'.$result->item_id.'" id="quntity_'.$result->item_id.'" value=""> 							<input type="button" style="width:70px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="updateDosageDesciption('.$result->item_id.')" value="'.lang("medicine_dosage_title").'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 							</div></div>';
		   }
		    $SearchData.= $SearchData_selected;	
		    $ids_str= implode(',',$ids); 
            if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }	
		    $ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id NOT in ($ids_str) $catgoryType order by name");
			if($ExecuteData_pend->num_rows()>0)
			       {				    $ids2 =array();
		            foreach($ExecuteData_pend->result() as $result)  
					  { 					    $ids2[] = $result->item_id; $class_name='dosage_green'; 						$dosage_description = $this->get_medicine_drugs_dosage($result->item_id,$customer_id);						if(isset($dosage_description) && $dosage_description!=''){ $class_name='dosage_red'; }
						$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }						
					    $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">									   <input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> &nbsp; 									   <input type="textbox" style="width:50px;padding: 2px;height: 15px;" name="quntity_'.$result->item_id.'" id="quntity_'.$result->item_id.'" value="">  									   <input type="button" style="width:70px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="updateDosageDesciption('.$result->item_id.')" value="'.lang("medicine_dosage_title").'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 									   </div></div>';					  }
				   }
		   return $SearchData ;  
		 }		 
		 else {	 
			$ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid order by name");
			if($ExecuteData_pend->num_rows()>0)
			   { $ids2 =array();
				foreach($ExecuteData_pend->result() as $result)  
				{  				     $ids2[] = $result->item_id; $class_name='dosage_green'; 					 $dosage_description = $this->get_medicine_drugs_dosage($result->item_id,$customer_id);					 if(isset($dosage_description) && $dosage_description!=''){ $class_name='dosage_red'; } 
					 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }					 $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.'<div class="drugs_div_sec">									<input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> &nbsp; 									<input type="textbox" style="width:50px;padding: 2px;height: 15px;" name="quntity_'.$result->item_id.'" id="quntity_'.$result->item_id.'" value=""> 									<input type="button" style="width:70px;padding: 0px;height: 23px;line-height: 0px;" class="'.$class_name.'" onclick="updateDosageDesciption('.$result->item_id.')" value="'.lang("medicine_dosage_title").'" onMouseover="ddrivetip('."'".stripslashes($dosage_description)."'".')" onMouseout="hideddrivetip()"> 									</div></div>';
				 }
			   }			   return $SearchData_selected.$SearchData;
		 }		 
		 return $SearchData;	
	}				
	function Search_MedicneDrugs_Catgory( $string ,$value=NULL)
	{    
        $ids=''; $ids2='';$tempArraySelected='';  $SearchData=''; $SearchData_selected=''; $valid=''; $TempData='';
		$catgoryType= " and item_type='0' and deleted='0'";
	  	$lab_investigation = $this->db->dbprefix('items');
		if($value!='') { 	$test2 = explode('-',$value); 

		$tempArraySelected = implode(',',$test2); $valid =  " and item_id NOT in ($tempArraySelected) ";
        $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id in ($tempArraySelected) $catgoryType ");
		if($ExecuteData->num_rows()>0)
		{   
			foreach($ExecuteData->result() as $result)  
			{   				
				$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
				$SearchData_selected.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">
										<input class="isAgeSelected" type="checkbox"  checked="checked" name="lab_category[]" value="'.$result->item_id.'"> &nbsp;
										</div></div>';
			}	
		}	
		} 
	    $ExecuteData = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid and name LIKE '".$this->db->escape_like_str($string)."%' order by name ");			
		if($ExecuteData->num_rows()>0)
		 { $ids =array(); 
		   foreach($ExecuteData->result() as $result)  
		   { 
			 $ids[] = $result->item_id; 
			 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; } 
             $TempData .=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">
							<input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'" > &nbsp; 
							</div></div>';
		   }

		    if($string==''){ $SearchData .= $SearchData_selected.$TempData; }	else { $SearchData.= $TempData.$SearchData_selected; }
		    $ids_str= implode(',',$ids); 
            if(isset($tempArraySelected) && $tempArraySelected!=''){  $ids_str.=','. $tempArraySelected; }	
		    $ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where item_id NOT in ($ids_str) $catgoryType order by name");
			if($ExecuteData_pend->num_rows()>0)
			   {
				$ids2 =array();
				foreach($ExecuteData_pend->result() as $result)  
				  { 
					$ids2[] = $result->item_id; 
					$bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }						
					$SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">
								   <input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> &nbsp; 
								   </div></div>';
				  }
			   }
		   return $SearchData ;  
		 }		 

		 else {	 
			$ExecuteData_pend = $this->db->query("select item_id,name,item_status from ".$this->db->dbprefix('items')." where 1=1 $catgoryType $valid order by name");
			if($ExecuteData_pend->num_rows()>0)
			   { $ids2 =array();
				foreach($ExecuteData_pend->result() as $result)  
				{  
				     $ids2[] = $result->item_id; 
					 $bgdisabled=''; if($result->item_status==1){$bgdisabled='style="background-color: #FFFF00;"'; }
					 $SearchData.=' <div class="medicine-list" '.$bgdisabled.'>'.$result->name.' <div class="drugs_div_sec">
									<input class="isAgeSelected" type="checkbox" name="lab_category[]" value="'.$result->item_id.'"> &nbsp; 
									</div></div>';
				 }
			   }
			   return $SearchData_selected.$SearchData;
		 }		 
		 return $SearchData;	
	}			

	function get_Report($customer_id,$invest_id)
	{
		$LabTest = $this->db->dbprefix('lab_investigation');
	    $query = "select test_report_details from $LabTest where id = '".$invest_id."' and person_id = '".$customer_id."' "; 
		$data=$this->db->query($query)->row();	
		return $data->test_report_details;
    }		
	function count_all()
	{
		$this->db->from('customers');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	function delete_list($ids)
	{  		$history = $this->db->dbprefix('lab_medicine');
	    $ids = implode(',',$ids);
	    $query = "delete from $history where id in ($ids) ";
        $result = $this->db->query( $query );
		return  $result;
 	}	
	/*Returns all the customers*/
	function get_exportData()
	{  
		$lab_category=$this->db->dbprefix('lab_category');
		$customers=$this->db->dbprefix('customers');
		$data=$this->db->query("SELECT * FROM $lab_category where category_type='medicine' ORDER BY lab_name asc ");
		return $data;
	}    function exist_medicine_drugs_dosage($category_id=-1,$customer_id=-1)	{	    $lab_investigation = $this->db->dbprefix('lab_investigation');	   	           $ExecuteQuery = $this->db->query("select id from  ".$this->db->dbprefix('lab_category_dosage')." where item_id='".$category_id."' and person_id='".$customer_id."' ") ;		if($ExecuteQuery->num_rows)	return true; else return false;		    }				function get_medicine_drugs_dosage($category_id=-1,$customer_id=-1)	{  		$ExecuteQuery = $this->db->query("SELECT * FROM ".$this->db->dbprefix('lab_category_dosage')." where item_id='".$category_id."' and person_id='".$customer_id."' and status='0' ");		if($ExecuteQuery->num_rows==1){ $ExecuteData  = $ExecuteQuery->row(); return $ExecuteData->dosage_description;}		else return false;	}			function get_Dispensed_dosage($customer_id=-1,$medicine_id=-1)	{  		$quantity ='0';		$ExecuteQuery = $this->db->query("SELECT * FROM ".$this->db->dbprefix('lab_medicine_history')." where medicine_id='".$medicine_id."' and person_id='".$customer_id."' and quantity!='' and reason_dispanse_quantity !='' ");		if($ExecuteQuery->num_rows>0)		{ 			 foreach($ExecuteQuery->result() as $rowdata)			 {				$quantity += $rowdata->quantity; 			 } 		}		return $quantity;	}	
	
	function get_Drugs_Quantity($customer_id=-1,$medicine_id=-1)
	{  
		$quantity ='0';
		$ExecuteQuery = $this->db->query("SELECT quantity FROM ".$this->db->dbprefix('lab_medicine_history')." where medicine_id='".$medicine_id."' and person_id='".$customer_id."' and quantity!='' and reason_change_quantity !='' order by added_date desc limit 0,1 ");
		if($ExecuteQuery->num_rows>0)
		{    
		    $ExecuteData = $ExecuteQuery->row();
			return $ExecuteData->quantity;			
		}
		return false;
	}			function search_drugs_profile($customer_id,$search=NULL, $column='added_date',$orderby='desc')	{		$searchquery='';		if($search!=''){ $searchquery = " and (e.first_name like '%".$search."%' || e.last_name like '%".$search."%' || l.name like '%".$search."%') "; }		if($column==''){ $column=' c.added_date '; }   if($orderby==''){ $orderby='desc'; }		$LabTest = $this->db->dbprefix('lab_medicine');		$employees = $this->db->dbprefix('people');		$lab_category = $this->db->dbprefix('items');		$query = "select e.first_name,e.last_name,l.name,c.* from $LabTest c left join $employees e on (c.added_by_employee_id = e.person_id) join $lab_category l on (c. lab_category_id = l.item_id) where l.deleted='0' and c.person_id = '".$customer_id."' and c.status='0' and c.quantity_dispansed='1' $searchquery order by $column $orderby"; 		$data=$this->db->query($query);        $commentData='<tr style="cursor: pointer;">					  <th>'.lang('medicine_drugs_list').'</th>					  <th>'.lang('medicine_drug_status').'</th>									  <th>'.lang('common_count').'</th>					  <th>'.lang('medicine_dispensed').'</th>					  <th>'.lang('common_reason').'</th>					  <th>'.lang('medicine_drug_management').'</th>					  <th>'.lang('common_patient_status').'</th>					 </tr>';           foreach($data->result() as $data )		   {    $view_reason='_';
		   		$dosage_management ='<a style="text-decoration: none;" class="thickbox underline" href="medicine/dosage_management/'.$customer_id.'/'.$data->id.'/width~650" title="'.lang('medicine_dosage_management').'"><span class="send_request">'.lang('medicine_dosage_management').'</span></a>';
				if($data->medicine_status=='0'){ $medicine_status = '<span class="dosage_status_ongoing">'.lang('medicine_dosage_status_ongoing').'</span>'; }				if($data->medicine_status=='1'){ $medicine_status = '<span class="dosage_status_complete">'.lang('medicine_dosage_status_complete').'</span>'; }				if($data->medicine_status=='2')				{ $medicine_status = '<span class="dosage_status_cancel">'.lang('medicine_dosage_status_cancel').'</span>'; 				  $view_reason = '<a class="thickbox" style="color:#ff0000;font-weight:bold;" href="medicine/view_reason/'.$customer_id.'/'.$data->id.'/width~550/height~300" title="'.lang('common_view_reason').'">'.lang('common_view').'</a>';				  $dosage_management ='<span class="send_not_request">'.lang('medicine_dosage_management').'</span>';
				}				if($data->patient_status=='0'){ $patient_status = '<span class="dosage_status_ongoing">'.lang('common_opd').'</span>'; }				if($data->patient_status=='1'){ $patient_status = '<span class="dosage_status_ongoing">'.lang('common_ipd').'</span>'; }				$Dispensed_dosage = $this->get_Dispensed_dosage($customer_id, $data->id);
				$Drugs_Quantity   = $this->get_Drugs_Quantity($customer_id, $data->id);
				if($Drugs_Quantity!=''){ $count_dosage = $Drugs_Quantity; }else { $count_dosage = $data->quantity; }				//$count_dosage =  ($data->quantity - $Dispensed_dosage);
				$commentData .=	'<tr>						<td width="28%">'.ucwords(strtolower($data->name)).'</td>						<td width="20%">'.$medicine_status.'</td>						<td width="5%">'.$count_dosage.'</td>						<td width="5%">'.$Dispensed_dosage.'</td>						<td width="5%">'.$view_reason.'</td>						<td width="22%">'.$dosage_management.'</td>											<td width="10%" class="">'.$patient_status.'</td> 						</tr>'; 		    }	        $commentData.='';        return $commentData; 		}		/*	Get search suggestions to find medicines */	function get_search_suggestions($customer_id=NULL,$search,$limit=25)	{		$suggestions = array();     		    		$query = "select e.first_name from ".$this->db->dbprefix('lab_medicine')." c left join ".$this->db->dbprefix('people')." e on (c.added_by_employee_id = e.person_id) join ".$this->db->dbprefix('items')." l on (c. lab_category_id = l.item_id)  where c.person_id = '".$customer_id."' and c.status='0' and l.item_status='0' and l.deleted='0' and e.first_name like '%".$search."%' group by e.first_name order by e.first_name asc";         $by_name = $this->db->query($query);		foreach($by_name->result() as $row){$suggestions[]=array('label'=> $row->first_name);}
		
		$query = "select e.last_name from ".$this->db->dbprefix('lab_medicine')." c left join ".$this->db->dbprefix('people')." e on (c.added_by_employee_id = e.person_id) join ".$this->db->dbprefix('items')." l on (c. lab_category_id = l.item_id)  where c.person_id = '".$customer_id."' and c.status='0' and l.item_status='0' and l.deleted='0' and e.last_name like '%".$search."%' group by e.last_name order by e.last_name asc";         
		$by_name = $this->db->query($query);
		foreach($by_name->result() as $row){$suggestions[]=array('label'=> $row->last_name);}
		
		$query = "select e.first_name,e.last_name from ".$this->db->dbprefix('lab_medicine')." c left join ".$this->db->dbprefix('people')." e on (c.added_by_employee_id = e.person_id) join ".$this->db->dbprefix('items')." l on (c. lab_category_id = l.item_id)  where c.person_id = '".$customer_id."' and c.status='0' and l.item_status='0' and l.deleted='0' and (CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%' ) group by e.first_name order by e.first_name asc"; 
        $by_name = $this->db->query($query);
		foreach($by_name->result() as $row){$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);}				$query = "select l.name from ".$this->db->dbprefix('lab_medicine')." c left join ".$this->db->dbprefix('people')." e on (c.added_by_employee_id = e.person_id) join ".$this->db->dbprefix('items')." l on (c. lab_category_id = l.item_id)  where c.person_id = '".$customer_id."' and c.status='0' and l.item_status='0' and l.deleted='0' and l.name like '%".$search."%' group by l.name order by l.name asc"; 		$by_name = $this->db->query($query);		foreach($by_name->result() as $row)		{			$suggestions[]=array('label'=> $row->name);		}							if(count($suggestions > $limit))		{			$suggestions = array_slice($suggestions, 0,$limit);		}		return $suggestions;	}		
	function cleanup()
	{
		$customer_data = array('account_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('customers',$customer_data);
	}	
}
?>
