<?php
class Ipd_in_patients extends Person
{
	function exists($person_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function exists_account($account_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.account_number',$account_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function get_customer_id($account_id)
	{
	   $people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$QueryData = $this->db->query("SELECT p.person_id FROM $people p STRAIGHT_JOIN $customers c ON  p.person_id = c.person_id WHERE c.account_number ='".$account_id."' ");		
		if($QueryData->num_rows()==1){ $data =  $QueryData->row(); return($data->person_id); }
	}
	
	function exists_customer_IPD_IN($account_id)
	{
		$people_id =  $this->get_customer_id($account_id);
		if(isset($people_id) && $people_id!='')
		{
			$this->db->from('ipd_in_patient');	
			$this->db->where('ipd_in_patient.person_id',$people_id);
			$this->db->where('ipd_in_patient.discharge_status','0');
			$this->db->where('ipd_in_patient.status','0');
			$query = $this->db->get();
			if($query->num_rows()>0){ $data =  $query->row(); return true; }
		}
		 else return false;
	}
	/*	Returns all the customers	*/
	function Search_Patients($string) 
	{
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$StringData='';
		$QueryData = $this->db->query("SELECT c.account_number,p.person_id,p.first_name,p.last_name FROM $people p STRAIGHT_JOIN $customers c ON  p.person_id = c.person_id WHERE deleted =0 and patient_type!='-1' and (c.account_number like '".$string."%' OR p.first_name like '".$string."%' OR p.last_name like '".$string."%')");		
		$StringData =' <ul class="ipd_search_list ui-autocomplete ui-menu ui-widget ui-widget-content ">';
		$i=0;
		if($QueryData->num_rows()>0)
		{  
			foreach($QueryData->result() as $data)
			{			
			  $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" onclick="getCustomerdata(\''.$data->account_number.'\')" >'.$data->account_number. ' - ' .$data->first_name.' '.$data->last_name.'</a></li>';	
            }			
		} 
		else { $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" >Sorry No Records</a></li>'; }
		$StringData .='</ul>';	
		return $StringData ;	}
	
	function search_block_lists($search_keyword) 
	{
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$StringData=''; $stringQuery= " where patient_block_name!='' ";
		if($search_keyword!=''){ $stringQuery .= " and patient_block_name like '".$search_keyword."%' "; }
		
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$QueryData = $this->db->query("select distinct(patient_block_name) as block_name from $ipd_in_patient  $stringQuery");
		
		$StringData =' <ul class="ipd_search_list ui-autocomplete ui-menu ui-widget ui-widget-content ">';
		$i=0;
		if($QueryData->num_rows()>0)
		{  
			foreach($QueryData->result() as $data)
			{			
			  $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" onclick="getCustomerdata(\''.$data->block_name.'\')" >'.ucwords($data->block_name).'</a></li>';	
            }			
		} 
		$StringData .='</ul>';	
		return $StringData ;
	}
	
	function search_room_name_list($search_keyword) 
	{
		$people=$this->db->dbprefix('people');
		$customers=$this->db->dbprefix('customers');
		$StringData=''; $stringQuery= " where patient_room_name!='' ";
		if($search_keyword!=''){ $stringQuery .= " and patient_room_name like '".$search_keyword."%' "; }
		
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$QueryData = $this->db->query("select distinct(patient_room_name) as room_name from $ipd_in_patient  $stringQuery");
		
		$StringData =' <ul class="ipd_search_list ui-autocomplete ui-menu ui-widget ui-widget-content ">';
		$i=0;
		if($QueryData->num_rows()>0)
		{  
			foreach($QueryData->result() as $data)
			{			
			  $StringData .='<li class="ui-menu-item"><a class="ui-corner-all" onclick="getCustomerRoom(\''.$data->room_name.'\')" >'.ucwords($data->room_name).'</a></li>';	
            }			
		} 
		$StringData .='</ul>';	
		return $StringData ;
	}
	
		
	function save($person_data)
	{
		$success=false;	
        $this->db->trans_start();	
		if ($this->exists_account($person_data))
		{
			$customer_id = $this->get_customer_id($person_data);
			$customer_data = array('person_id'=>$customer_id, 'registration_time'=>date('Y-m-d H:i:s'),'status'=>'0');
			$success = $this->db->insert('ipd_in_patient',$customer_data);		
		}				
		$this->db->trans_complete();		
		return $success;
	}	
	
	function save_old($person_data)
	{
		$success=false;	
        $this->db->trans_start();	
		if ($this->exists_account($person_data))
		{
			$customer_id = $this->get_customer_id($person_data);
			$customer_data = array('person_id'=>$customer_id,'account_number'=>$person_data, 'registration_time'=>date('Y-m-d H:i:s'),'status'=>'0');
			$success = $this->db->insert('ipd_in_patient',$customer_data);		
		}				
		$this->db->trans_complete();		
		return $success;
	}	
	
    function Update_GetData($table,$customer_data,$customer_id,$ipd_patient_id)
	{
		$success=false;	
        $this->db->trans_start();
            
			if ($this->exists($customer_id))
			{				
				$this->db->where('person_id', $customer_id);
				$this->db->where('id', $ipd_patient_id);
				$this->db->where('discharge_status', '0');
				$this->db->where('status', '0');
				$success =  $this->db->update($table,$customer_data);
			}				
		$this->db->trans_complete();		
		return $success;
	}
	
	function Insert_GetData($table,$customer_data,$customer_id)
	{
		$success=false;	
        $this->db->trans_start();
            
			if ($this->exists($customer_id))
			{				
				$success =  $this->db->insert($table,$customer_data);
			}				
		$this->db->trans_complete();		
		return $success;
	}
	
	function get_ipdin_PatientData($people_id = NULL,$ipd_patient_id = NULL)
	{   $QueryString ='';
		if(isset($people_id) && $people_id!='')
		{   if($ipd_patient_id!='') { $QueryString =  " and i.id='".$ipd_patient_id."' "; }
			$ExecuteQuery =$this->db->query("SELECT i.* FROM ".$this->db->dbprefix('ipd_in_patient')." i
											STRAIGHT_JOIN ".$this->db->dbprefix('people')." p ON 	
											i.person_id = p.person_id
											STRAIGHT_JOIN ".$this->db->dbprefix('customers')." c ON 	
											p.person_id = c.person_id
											WHERE c.deleted ='0' and i.status='0' and i.person_id='".$people_id."' $QueryString ");				
			if($ExecuteQuery->num_rows()>0){ $ExecuteData =  $ExecuteQuery->row(); return $ExecuteData; }
		}
		 else return false;
	}
	
	function get_requested_cutomers_admittded_ipd($limit=10000, $offset=0,$col='last_name',$order='asc')
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$manage_cashier_requests = $this->db->dbprefix('manage_cashier_requests');
		$data=$this->db->query("SELECT distinct(p.person_id), p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number,i.* 
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
					    STRAIGHT_JOIN ".$manage_cashier_requests." cr ON 	
						cr.person_id = c.person_id
						WHERE c.deleted ='0' and i.status='0' and cr.status='0' and cr.category_type='customers_admission_ipd' ORDER BY p.first_name,i.discharge_status $order
						LIMIT  ".$offset.",".$limit);		
		return $data;
	}	
	
	function get_all($limit=10000, $offset=0,$col='last_name',$order='asc')
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$data=$this->db->query("SELECT p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number,i.* 
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE c.deleted ='0'  and c.patient_type!='-1' and i.status='0' ORDER BY p.first_name,i.discharge_status $order
						LIMIT  ".$offset.",".$limit);		
		return $data;
	}	
	
	function count_all()
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$data=$this->db->query("SELECT p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number ,i.* 
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE c.deleted ='0' and c.patient_type!='-1' and i.status='0' ");							
		return ($data->num_rows());						
	}

	function search($search, $limit=20,$offset=0,$column='last_name',$order='asc')
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$data=$this->db->query("SELECT p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number ,i.*  
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE (p.first_name like '".$this->db->escape_like_str($search)."%' or p.last_name like '".$this->db->escape_like_str($search)."%'
						or c.account_number like '".$this->db->escape_like_str($search)."%' ) 
						and c.deleted ='0' and c.patient_type!='-1' and i.status='0' ORDER BY p.first_name $order
						LIMIT  ".$offset.",".$limit);						
		return $data;
	}	
	
	function search_count_all($search)
	{
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$data=$this->db->query("SELECT p.person_id,p.first_name,p.last_name,p.registration_date,c.account_number ,i.* 
						FROM ".$ipd_in_patient." i
						STRAIGHT_JOIN ".$people." p ON 	
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE (p.first_name like '".$this->db->escape_like_str($search)."%' or p.last_name like '".$this->db->escape_like_str($search)."%'
						or c.account_number like '".$this->db->escape_like_str($search)."%' ) 
						and c.deleted ='0' and c.patient_type!='-1' and i.status='0' "
					   );				   
		return $data->num_rows();
	}	
	
	/*	Deletes a list of customers	*/
	function delete_list($customer_ids)
	{
		if(count($customer_ids)>0)
		{
			for($i=0; $i<count($customer_ids); $i++)
			{			 
				$this->db->where('person_id', $customer_ids[$i]);
				$this->db->update('ipd_in_patient', array('status' => 1));
			}
		   return true;
		}
		else return false;
 	}

	function GetTotalDeposit_Amount($customer_id = NULL,$ipd_patient_id = NULL)
	{   
	    $total = '0.00'; $payment_per_day_charges='0.00';
		$Payment_array= array();
		if($ipd_patient_id=='') 
		{ 
			$Payment_array['TotalDeposit'] = '0.00';
			$Payment_array['Balance_Amount'] = '0.00';
			$Payment_array['Total_Amount'] = '0.00';
			return($Payment_array);
		}
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$ipd_finance = $this->db->dbprefix('ipd_finance'); 
		$people = $this->db->dbprefix('people');
		$customers = $this->db->dbprefix('customers');
		$ExecuteData = $this->db->query("SELECT f.received_amount,f.payment_time,f.received_slip_no  
						FROM ".$ipd_finance." f						
						STRAIGHT_JOIN ".$ipd_in_patient." i ON 
						((f.ipd_id = i.id ) and (f.person_id = i.person_id ))
						STRAIGHT_JOIN ".$people." p ON 							
						i.person_id = p.person_id
						STRAIGHT_JOIN ".$customers." c ON 	
						p.person_id = c.person_id
						WHERE c.deleted ='0' and i.status='0' and f.ipd_id ='".$ipd_patient_id."' and f.person_id ='".$customer_id."' ");						
		if(isset($ExecuteData) && $ExecuteData!='' && $ExecuteData->num_rows()>0)
		{
			foreach($ExecuteData->result() as $row)
			{
			  $total += $row->received_amount;
			}	
        }				
		$Payment_array['TotalDeposit'] = ($total);
		$payment_per_day_charges = $this->getIPD_Per_Day_Charges($customer_id , $ipd_patient_id);
		$Total_Days     = $this->getTotal_AdmitDays($customer_id , $ipd_patient_id);
		$Total_Amount   = ($payment_per_day_charges * $Total_Days);
		$Balance_Amount = $Total_Amount - $total;
		$Payment_array['Balance_Amount'] = ($Balance_Amount);	
		$Payment_array['Total_Amount']   = ($Total_Amount);	
		return ($Payment_array);						
	}
	
	
	function getTotal_AdmitDays($customer_id , $ipd_patient_id)
	{
		$Total_Days='0';
		if($customer_id!='' && $ipd_patient_id!='')
		{  $ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
			$ExecuteQuery = $this->db->query("SELECT * from $ipd_in_patient where id='".$ipd_patient_id."' and person_id ='".$customer_id."' ");
			if($ExecuteQuery->num_rows()>0)
			{
			  $ExecuteData = $ExecuteQuery->row();
			  $registration_date = date('Y-m-d',strtotime($ExecuteData->registration_time)); 
			  if($ExecuteData->discharge_status=='1' && $ExecuteData->discharge_date!='0000-00-00 00:00:00' && $ExecuteData->discharge_date!='')
			  {			    
				$discharge_date    = date('Y-m-d',strtotime($ExecuteData->discharge_date)); 
		      }	
			  if($ExecuteData->discharge_status=='0' && ($ExecuteData->discharge_date=='0000-00-00 00:00:00' || $ExecuteData->discharge_date==''))
			  {			    
				$discharge_date    = date('Y-m-d'); 
		      }
				$Total_Days = ceil(abs(strtotime($discharge_date) - strtotime($registration_date))/86400)+1;			  
			}		  
		}
		return ($Total_Days);
 	}

	function getIPD_Per_Day_Charges($customer_id , $ipd_patient_id)
	{   $per_day_charges = '0.00';
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$ExecuteQuery = $this->db->query("SELECT payment_per_day_charges from $ipd_in_patient where id='".$ipd_patient_id."' and person_id ='".$customer_id."' ");
		if(isset($ExecuteQuery) && $ExecuteQuery->num_rows()>0)
		{ $ExecuteData = $ExecuteQuery->row(); 
		   return($ExecuteData->payment_per_day_charges);
		}
		else return($per_day_charges);
	}	
	
	function GetValid_Discharge($customer_id , $ipd_patient_id)
	{   
	    $result='0';
		$Finance_Data   = $this->GetTotalDeposit_Amount($customer_id , $ipd_patient_id);
		$TotalDeposit   = $Finance_Data['TotalDeposit'];
		$Balance_Amount = $Finance_Data['Balance_Amount'];		
		$payment_per_day_charges = $this->getIPD_Per_Day_Charges($customer_id , $ipd_patient_id);
		$Total_Days     = $this->getTotal_AdmitDays($customer_id , $ipd_patient_id);
		$Total_Amount   = number_format(($payment_per_day_charges * $Total_Days));		
		if($TotalDeposit=='0.00' && $Balance_Amount=='0.00' && $TotalDeposit == $Total_Amount ){ $result ='1';} //discharge patient without no charges (free hit)
		if($TotalDeposit > '0.00' && $Total_Amount > '0.00' && $Balance_Amount < '0.00' && ($TotalDeposit > $Total_Amount)){ $result ='2';}  //return amount  for payments more amount are deposit
		if($TotalDeposit >'0.00' && $Total_Amount >'0.00' && $Balance_Amount=='0.00' && $TotalDeposit == $Total_Amount){ $result ='3';} //discharge patient, no amount is pending
		if($TotalDeposit >'0.00' && $Total_Amount >'0.00' && $Balance_Amount > '0.00' && $TotalDeposit < $Total_Amount){ $result ='4';} //can not to discharge patient, payments is not final
		if($TotalDeposit=='0.00' && $Total_Amount>'0.00' && $Balance_Amount > '0.00' && $TotalDeposit < $Total_Amount ){ $result ='5';} //discharge patient without no charges (free hit)
		return $result; 
	}	
	
		
	//*******start functions for reports**************//
	function getIpdBlock_List()
	{
	    $ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$QueryData = $this->db->query("select distinct(patient_block_name) as block_name from $ipd_in_patient where patient_block_name!=''");
		if($QueryData->num_rows()>0) return $QueryData;
		else return false;
	}
	
	//**********unused functions*****************//	
	function get_info($customer_id)
	{
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$customer_id);
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			return $query->row();
		}		else
		{			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);
			//Get all the fields from customer table
			$fields = $this->db->list_fields('customers');
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			return $person_obj;
		}
	}	

	function delete($customer_id)
	{
		$this->db->where('person_id', $customer_id);
		return $this->db->update('customers', array('deleted' => 1));
	}
	function cleanup()
	{
		$ipd_finance = $this->db->dbprefix('ipd_finance');
		$ipd_in_patient = $this->db->dbprefix('ipd_in_patient');
		$ExecuteData = $this->db->query("SELECT * from $ipd_in_patient where status='1' ");
       	if($ExecuteData->num_rows()>0)	
		{
		  foreach($ExecuteData->result() as $result)
		  { //echo "delete from $ipd_in_patient where id='".$result->id."' ";die;
		    ////$this->db->query("delete from $ipd_in_patient where id='".$result->id."' ");
			// $this->db->query("delete from $ipd_finance where ipd_id='".$result->id."' ");	
			  $this->db->where('id', $result->id);
		     $this->db->delete('ipd_in_patient');
			 $this->db->where('ipd_id', $result->id);
		     $this->db->delete('ipd_finance');			 
		  }
		  return true;
		}
		return false;		  
	}	
	
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		if ($this->config->item('speed_up_search_queries'))
		{
			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '".$this->db->escape_like_str($search)."%') and deleted=0");
		}
		else
		{
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0");
		}

		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name);	
		}	

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("email",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=array('label'=> $row->email);	
		}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("phone_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("phone_number", "asc");		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row)
		{
			$suggestions[]=array('label'=> $row->phone_number);		
		}		

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("account_number",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('label'=> $row->account_number);	
		}

		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');	
		$this->db->where('deleted',0);		
		$this->db->like("company_name",$search, $this->config->item('speed_up_search_queries') ? 'after' : 'both');
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->company_name);	
		}		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}	
}
?>