<?php
class Laboratories extends Person
{
   function GetSearchLaboratoryList($category_type = NULL , $string = NULL,$string2 = NULL )
	{   
        
		$where =" where category_type = '".$category_type."' ";
        if(isset($string) && $string!=''){ $where .= " and laboratory_name LIKE '".$this->db->escape_like_str($string)."%' "; }
		if($category_type!='laboratory_department'){ if(isset($string2) && $string2!=''){ $where .= " and FIND_IN_SET('".$string2."' , related_item_id) "; } }
		$ExecuteData = $this->db->query("select * from ".$this->db->dbprefix('laboratory_items')." $where order by laboratory_name ");
		$SearchData='<table id="sortable_table" class="tablesorter">'; 
		if($category_type=='department') 
		{ $SearchData.='<tr><th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
							   <th>'.lang("laboratory_department_name").'</th>
							   <th>&nbsp;</th>
						   </tr>';
		}	
		else if($category_type=='laboratory_department') 
		{ $SearchData.='<tr><th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
						<th>'.lang("laboratory_department_name").'</th>
						<th>'.lang("laboratory_specimen_name").'</th>
						<th>&nbsp;</th>
						</tr>';
		}	
		
		else{ $SearchData.='<tr><th><input style="margin-top:5px;" type="checkbox" id="selectall"></th>
							   <th>'.lang("laboratory_specimen_name").'</th>
							   <th>&nbsp;</th>
						   </tr>';
		    }				
		if($ExecuteData->num_rows()>0)
		{  $this->load->model('savesetting');
		   foreach($ExecuteData->result() as $result)  
		   {  $laboratory_name = '_'; 
			 if($category_type=='laboratory_department') 
			 {
			   if(isset($result->laboratory_name) && $result->laboratory_name!='')
			   { $laboratory_name = stripslashes($this->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$result->laboratory_name)); } 

			   $specimen_name = $this->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$result->related_item_id);
			   if(isset($specimen_name) && $specimen_name!='') $specimen_name = stripslashes($specimen_name);else $specimen_name = '_';
				$SearchData.='<tr style="cursor: pointer;">
								<td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->id.'" id="'.$result->id.'"></td>
								<td width="35%">'.$laboratory_name.'</td>
								<td width="35%">'.$specimen_name.'</td>
								<td width="10%"><input class="lab-edit-button" type="button" onclick = "getServiceData('.$result->id.')" value="'.strtoupper(lang("toedit_diabetes")).'" name="importsubmit">
								</td>
							</tr>';					
			 }
			 else{ $laboratory_name = stripslashes($result->laboratory_name);
				   $SearchData.='<tr style="cursor: pointer;">
								 <td width="5%" ><input style="margin-top:5px;" class="selectedId" type="checkbox" value="'.$result->id.'" id="'.$result->id.'"></td>
								 <td width="70%">'.$laboratory_name.'</td>
								 <td width="10%"><input class="lab-edit-button" type="button" onclick = "getServiceData('.$result->id.')" value="'.strtoupper(lang("toedit_diabetes")).'" name="importsubmit">
								 </td>
								</tr>';					
				}
			}				
		}	
		else { 
				if($category_type=='laboratory_department')
				{
					$SearchData.= '<td width="5%">&nbsp;</td><td width="40%"> <div class="no_records_found2">'.lang('common_no_items_found').'</div></td><td width="20%">&nbsp;</td><td width="5%">&nbsp;</td>';		
				}
				else { $SearchData.= '<td width="5%">&nbsp;</td><td width="40%"> <div class="no_records_found2">'.lang('common_no_items_found').'</div></td><td width="15%">&nbsp;</td>'; }		
		}
		$SearchData.='</table>';		 
		 return  $SearchData;
	}
	
	function existsLaboratory_Items($category_type=NULL, $servicename = NULL ,$service_id = NULL, $related_item_id = NULL)
	 {  $chk = " and category_type='".$category_type."' and laboratory_name='".$servicename."' ";	
	    //if(isset($related_item_id) && $related_item_id!='') { $chk .= " and related_item_id='".$related_item_id."' "; }			
		if(isset($service_id) && $service_id!=''){ $chk .= " and id!='".$service_id."' " ; }	
	   	$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('laboratory_items')." where 1=1 $chk ");
        if($ExecuteQuery->num_rows()>0) return true;
        else return false;
    }
	
	function getLabDepartment_DropDownList($related_item_id = NULL , $item_id = NULL)
	{
	   $String ='';		
	   if($related_item_id==''){ return false; }
	   $this->load->model('savesetting');
	   $QueryData = $this->db->query("select * from ".$this->db->dbprefix('laboratory_items')." where status='0' and category_type='laboratory_department' and FIND_IN_SET('".$related_item_id."' , related_item_id) group by laboratory_name order by laboratory_name ");
	   $ListData = '<label class="laboratory_name_text">'.lang('laboratory_lab_department').'</label>
					<div class="form_field">	   
					<select class="department-drodown" id="department_name" name="department_name" onClick="clicktohide(this);">
				   <option value="">'.lang('common_select_type').'</option>';
		if($QueryData->num_rows()>0)
		{		
		  foreach($QueryData->result() as $data)
		  {  $selected ='';
			 $laboratory_name = stripslashes($this->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$data->laboratory_name));
			 if(isset($item_id) && $item_id == $data->id){ $selected = 'selected="selected"'; }
			 $ListData .= '<option value="'.$data->id.'" '.$selected.'>'.$laboratory_name.'</option>';
		  }
		}
		$ListData .= '</select></div>';
		return($ListData);
	}
	
	function count_all_tables($method_type=NULL)
	{  
		if($method_type=='reagent') 
		{  
			$this->db->select('manage_reagent.person_id');
			$this->db->from('manage_reagent');			
			$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->join('employees', 'employees.person_id = manage_reagent.employee_id','left');
			$this->db->where('customers.deleted','0');
		}			
		return $this->db->count_all_results();	
	}
	
	function get_all_tables($method_type=null,$limit=10000, $offset=0,$col = NULL,$order='asc')
	{ 	 
		if($method_type=='reagent') 
		{  
			$this->db->select('manage_reagent.*,people.person_id,people.first_name,people.last_name,customers.account_number');
			$this->db->from('manage_reagent');			
			$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->join('employees', 'employees.person_id = manage_reagent.employee_id','left');
			$this->db->where('customers.deleted','0');			
			$this->db->order_by('people.first_name', $order);
			$this->db->limit($limit);
			$this->db->offset($offset);	
			return $this->db->get();
		 }	
					 
		return $this->db->get();
     }	
	 
	function search_count_all($method_type=null,$specimen_name=null,$department_name=null,$receiept_no=null,$hospital_department=null,$member_pfnumber=null,$receipt_date=NULL,$customer_id=null,$limit=10000)
	{	
		if($method_type=='reagent')
		{	//echo $method_type.'<br>'; echo $specimen_name.'<br>'; echo $department_name.'<br>'; 
			//echo $receiept_no.'<br>';echo $hospital_department.'<br>';echo $member_pfnumber.'<br>';echo $customer_id;die;  
			$this->db->select('manage_reagent.person_id');
			$this->db->from('manage_reagent');			
			$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->join('employees', 'employees.person_id = manage_reagent.employee_id','left');
			$this->db->where('customers.deleted','0');
			if($specimen_name!=''){ $this->db->where('manage_reagent.lab_specimen_id',$specimen_name);	}
			if($department_name!=''){ $this->db->where('manage_reagent.lab_dept_id',$department_name);	}	
			if($hospital_department!=''){ $this->db->where('manage_reagent.hospital_department',$hospital_department);	}
			if($customer_id!='-1'){ $this->db->where('manage_reagent.person_id',$customer_id);	}	
			if($receipt_date!=''){ $this->db->where('manage_reagent.registration_date',$receipt_date);	}			
			if($receiept_no!=''){ $this->db->or_like('manage_reagent.receiept_no',$receiept_no,'both');}
			if($member_pfnumber!=''){ $this->db->or_like('employees.username',$member_pfnumber,'both');}
			$this->db->limit($limit);
			$result=$this->db->get(); 
			return $result->num_rows();		
		}
	}			

	function search($method_type=null,$specimen_name=null,$department_name=null,$receiept_no=null,$hospital_department=null,$member_pfnumber=null,$receipt_date=NULL,$customer_id=null, $limit=20,$offset=0,$column=NULL,$orderby='asc')
	{	
		if($method_type=='reagent')
		{				
			$this->db->select('manage_reagent.*,people.person_id,people.first_name,people.last_name,customers.account_number');
			$this->db->from('manage_reagent');			
			$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->join('employees', 'employees.person_id = manage_reagent.employee_id','left');
			$this->db->where('customers.deleted','0');
			if($specimen_name!=''){ $this->db->where('manage_reagent.lab_specimen_id',$specimen_name);	}
			if($department_name!=''){ $this->db->where('manage_reagent.lab_dept_id',$department_name);	}	
			if($hospital_department!=''){ $this->db->where('manage_reagent.hospital_department',$hospital_department);	}
			if($customer_id!='-1'){ $this->db->where('manage_reagent.person_id',$customer_id);	}	
			if($receipt_date!=''){ $this->db->where('manage_reagent.registration_date',$receipt_date);	}			
			if($receiept_no!=''){ $this->db->or_like('manage_reagent.receiept_no',$receiept_no,'both');}
			if($member_pfnumber!=''){ $this->db->or_like('employees.username',$member_pfnumber,'both');}
			if($member_pfnumber!=''){ $this->db->or_like('employees.username',$member_pfnumber,'both');}

			$this->db->order_by($column,$orderby);
			$this->db->limit($limit);
			$this->db->offset($offset);
			return $this->db->get();
		}
		
	}
	
	//**************Inserts or updates a item by tables name**************************//
	function save($table_name, $item_data, $column_by, $item_id=false)
	{   
		if (!$item_id or !$this->existsTableData($table_name,$column_by,$item_id))
		{   
			if($this->db->insert($table_name,$item_data))
			{
				return true;
			}
			return false;
		}
		$this->db->where($column_by, $item_id);
		return $this->db->update($table_name,$item_data);
	}
		
	function existsTableData($table_name,$column_by,$item_id)
	{
		$this->db->from($table_name);
		$this->db->where($column_by,$item_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function exists_reagent($person_id)
	{
		$this->db->select('manage_reagent.person_id');
		$this->db->from('manage_reagent');			
		$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('manage_reagent.person_id',$person_id);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function exists_receipt($receipt_no)
	{
		$this->db->select('manage_reagent.person_id');
		$this->db->from('manage_reagent');			
		$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.deleted','0');
		$this->db->where('manage_reagent.receiept_no',$receipt_no);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
    /*	Change Status of items	of any table*/
	function Change_Dispatch_Status($method_type=NULL,$item_data=NULL,$items_id=NULL )
	{  
		if($method_type=='reagent') { $table_name  = 'manage_reagent';}	
			$this->db->from($table_name);
			$this->db->where('id',$items_id);
			$query = $this->db->get();
			if($query->num_rows()>0)
			{ 
			   $this->db->where_in('id',$items_id);
			   return $this->db->update($table_name,$item_data);
            }				
		return false;
 	}
	
	/*	Change Status of items	of any table*/
	function Change_Status($method_type, $column_by , $item_id )
	{  
		if($method_type=='reagent') { $table_name  = 'manage_reagent';}	
			$this->db->from($table_name);
			$this->db->where($column_by,$item_id);
			$query = $this->db->get();
			if($query->num_rows()>0)
			{ 
			   $item_data = $query->row();
			   if($item_data->status=='1')  $status ='0';  else $status ='1';
			   $item_data = array('status'=>$status);
			   $this->db->where_in($column_by,$item_id);
			   return $this->db->update($table_name,$item_data);
            }				
		return false;
 	}
	
	function get_Item_ArrayData($method_type , $item_id) 
	{	
		if($method_type=='reagent') 
		 {    
			$table_name='manage_reagent';$table_name2='customers';$table_name3='people';
			$this->db->select('manage_reagent.*,people.person_id,people.first_name,people.last_name,customers.account_number');
			$this->db->from('manage_reagent');			
			$this->db->join('customers', 'customers.person_id = manage_reagent.person_id');
			$this->db->join('people', 'people.person_id = customers.person_id');
			$this->db->where('manage_reagent.id',$item_id);			
			$query = $this->db->get();
		 }			
		 
		if($query->num_rows()==1) {	return $query; }
		else
		{
			//$item_obj=new stdClass();
			$person_obj=parent::get_info(-1);
			$fields = $this->db->list_fields( $table_name);		
			foreach ($fields as $field)	{ $person_obj->$field='';}
			$fields = $this->db->list_fields( $table_name2);		
			foreach ($fields as $field)	{ $person_obj->$field='';}
			$fields = $this->db->list_fields( $table_name3);		
			foreach ($fields as $field)	{ $person_obj->$field='';}
			return $person_obj;			
		}
	}
	
	function GetSpecimenCheckboxData($id,$category_type)
	{	$HtmlData ='';	
		if($category_type=='department'){ $search_type = " and category_type='department' "; }
		if($category_type=='laboratory_specimen'){ $search_type = " and category_type='laboratory_specimen' "; }
		if($category_type=='laboratory_department'){ $search_type = " and category_type='laboratory_department' "; }
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('laboratory_items')." where 1=1 $search_type and id='".$id."' ");
		$this->load->model('savesetting');
		$specimen_list = $this->savesetting->Get_Table_Category_Data('laboratory_items','laboratory_specimen','laboratory_name',$order='asc');
		if($ExecuteQuery->num_rows()>0) 
		   { $ExecuteData = $ExecuteQuery->row();
			 //print_r($ExecuteData);
			if($specimen_list->num_rows()>0){
			$HtmlData ='<div id="checkbox_list_sec2" class="form_field checkbox_list_sec" style="color: #000000;">';
			foreach($specimen_list->result() as $rowdata){
			$checked = ''; 
			$related_item_id = explode(',',$ExecuteData->related_item_id); 
			if (in_array($rowdata->id, $related_item_id, TRUE)){$checked ='checked="checked"';}
			//echo $checked;//die;
				$HtmlData .=' <div class="checkbox_div">  '.ucfirst($rowdata->laboratory_name).' <input class="isAgeSelected" type="checkbox" name="specimen_item_name[]" value="'.$rowdata->id.'" id="'.$rowdata->id.'" '.$checked.' style="width:auto; float:right; margin-top: 4px;"></div>';
				}				
				$HtmlData .= '</div>';
			} 			 
			echo json_encode(array('success'=>true,'name'=>$ExecuteData->laboratory_name,'related_item_data'=>$HtmlData));  die;
			}
		else { echo json_encode(array('success'=>false,'name'=>'','related_item_id'=>''));  die;};

	}	
	
	function delete_list($value=NULL,$category_type=NULL)
	{  
		if($category_type=='department'){ $search_type = " and category_type='department' "; }
		if($category_type=='laboratory_specimen'){ $search_type = " and category_type='laboratory_specimen' "; }
		if($category_type=='laboratory_department'){ $search_type = " and category_type='laboratory_department' "; }
		if($value!='') 
		{ 	$test2 = explode('-',$value);  $tempArraySelected = implode(',',$test2); 
			if($category_type=='laboratory_reagent'){ $result = $this->db->query("delete from ".$this->db->dbprefix('manage_reagent')." where id in($tempArraySelected) ");}
			else {  $result = $this->db->query("delete from ".$this->db->dbprefix('laboratory_items')." where id in($tempArraySelected) $search_type "); }
			if($result) return true; else return  false;
		}
     } 
	 	 
	//************************unused code************************//   
	function delete_list22($method_type , $item_ids)
	{  
	  if($method_type=='diabetes_customers')
	  {		
			if(count($item_ids)>0)
			{
				for($i=0; $i<count($item_ids); $i++)
				{
					$this->db->where_in('person_id',$item_ids[$i]);
					$this->db->delete('customers_diabetescare');		         			 
				}
				return true;
			}
       }
 
		if($method_type=='insulin_treatment') { $table_name = 'insulin_treatments'; }
		if($method_type=='insulincategory')   { $table_name = 'insulin_category'; }		 	
		if(count($item_ids)>0)
		{
			for($i=0; $i<count($item_ids); $i++)
			{
				$this->db->where('id',$item_ids[$i]);
				$this->db->delete($table_name );		         			 
			}
			return true;
		}          
		else return false;
  }
	

}
?>
