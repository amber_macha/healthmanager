<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * FLV Player Class to play the video flash on your application
 *
 * -----------------------------------------------------------------------------
 * Copyright (C) 2008  Dwi Setiyadi (dwi.web.id)
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *------------------------------------------------------------------------------
 * @package     FLV Player
 * @subpackage  Libraries
 * @category    Video Player
 * @author      Dwi Setiyadi (pembelajar)
 * @copyright   Copyright (c) 2008, dwi.web.id
 * @license		http://www.gnu.org/licenses/lgpl.html
 * @link 		http://www.dwi.web.id/
 * @version 	1.0
 *
 * @Thanks to:
 * 1) Includes Jeroen Wijering's FLV Player (Creative Commons "BY-NC-SA" License) v3.12
 *    Website: http://www.jeroenwijering.com/?item=Flash_Video_Player
 *	  License: http://creativecommons.org/licenses/by-nc-sa/2.0/
 * 2) Includes Geoff Stearns' SWFObject Javascript Library (MIT License) v1.5
 *    Website: http://blog.deconcept.com/swfobject/
 *    License: http://www.opensource.org/licenses/mit-license.php
 *
 */

//$videoid = 0; 
 
class Flvplayer
{

	function rendervideo($video, $picture, $width, $height, $videoid) {
		//global $videoid;
		//$videoid++;
		$CI =& get_instance();
		$site_url = $CI->config->slash_item('base_url');
		$output = '';
	
		$output .= "\n" . '<div id="container' . $videoid . '"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</div>
' . "\n";
		$output .= "\n" . '<script type="text/javascript" src="' . $site_url . 'public/app/swfobject.js"></script>' . "\n";
		$output .= '<script type="text/javascript">' . "\n";
		$output .= 'var s' . $videoid . ' = new SWFObject("' . $site_url . 'public/app/mediaplayer.swf","mediaplayer' . $videoid . '","' . $width . '","' . $height . '","7");' . "\n";
		$output .= 's' . $videoid . '.addParam("allowfullscreen","true");' . "\n";
		$output .= 's' . $videoid . '.addVariable("autostart","true");' . "\n";
		$output .= 's' . $videoid . '.addVariable("width","' . $width . '");' . "\n";
		$output .= 's' . $videoid . '.addVariable("height","' . $height . '");' . "\n";
		$output .= 's' . $videoid . '.addVariable("file","' . $site_url .'' . $video . '");' . "\n";
		$output .= 's' . $videoid . '.addVariable("image","' . $site_url .'' . $picture . '");' . "\n";
		//$output .= 's' . $videoid . '.addVariable("bufferlength","0.02");' . "\n";
		$output .= 's' . $videoid . '.write("container' . $videoid . '");' . "\n";
		$output .= '</script>' . "\n";

		return $output;
	}
}
?>
