<?php
class Functions
{	 	
	function Get_Patient_Wating_AppointList()
	{
	    $CI =& get_instance();
		$CI->db->from('appointment_type');	
		$CI->db->where('status','0');
		$CI->db->order_by('id','asc');
		$Query = $CI->db->get();		
		return $Query;
	}
	
	function Get_Patient_Wating_AppointListSection($customer_id=-1)
	{
	    $CI =& get_instance();
		$AppointListSection='<div class="waiting_counter_down">';
	
		$appointList = $this->Get_Patient_Wating_AppointList();
		if(isset($appointList) && $appointList!='' && $appointList->num_rows()>0)
		{
		  foreach($appointList->result() as $row)
		  {
			$total_appoint = $CI->db->query("select id from ".$CI->db->dbprefix('customer_appointment')." where appointment_id='".$row->id."' and status='0' ")->num_rows();
			$AppointListSection .='<div class="waiting_list"> <a href="'.base_url().'history/viewAppointment/'.$row->id.'/width~700" class="thickbox">'.ucwords($row->department_name).' : <span class="count_appoint">'.$total_appoint.'</span> </a/></div>';
		  }
		}
	    $AppointListSection .='</div>';	
		return $AppointListSection;
	}
	
	function Get_All_Wating_AppointList($item_id)
	{  
	    $CI =& get_instance();
		$CI->db->select('customer_appointment.*,customers.account_number,people.first_name,people.last_name,people.first_name');	
		$CI->db->from('customer_appointment');	
		$CI->db->join('customers', 'customers.person_id = customer_appointment.person_id');	
		$CI->db->join('people', 'people.person_id = customers.person_id');			
		$CI->db->where('customer_appointment.status','0');
		$CI->db->where('customer_appointment.appointment_id',$item_id);
		$CI->db->order_by('customer_appointment.id','asc');
		$Query = $CI->db->get();		
		return $Query;
	}
	
	function update_Wating_Appoint( $item_id = NULL ,$action_type = NULL )
	{
	  $CI =& get_instance();
	  if($item_id!='' && $action_type!='')
	  {  if($action_type=='complete') $status ='1';else $status ='2';
        $login_uerData = $CI->Employee->get_logged_in_employee_info();
        $customer_data = array('employee_id'=>$login_uerData->person_id,'close_date'=>date('Y-m-d:H:i:s'),'status'=>$status); 
		$CI->db->where('id', $item_id);
		$success = $CI->db->update('customer_appointment',$customer_data);		
		if($success)return true; else return false;
	   }	
	   else return false;		
	}
	
	function existCashierRequest($customer_id = NULL,$category_type = NULL, $items_id = NULL) 
	{ 
	    $CI =& get_instance();
		$CI->db->from('manage_cashier_requests');		
		$CI->db->where('person_id',$customer_id);
		$CI->db->where('category_type',$category_type);
		if($items_id!=''){ $CI->db->where('lab_test_id',$items_id);	}
		$CI->db->where('status','0');		
		$query = $CI->db->get();
		if($query->num_rows()>0)
		{
			return true;
		}
		else{ return false; }
	}	
	
	
	function getTotal_CashierRequest($category_type = NULL) 
	{ 
	    $CI =& get_instance();
		$CI->db->from('manage_cashier_requests');		
		$CI->db->where('category_type',$category_type);
		$CI->db->where('status','0');		
		$query = $CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else{ return '00'; }
	}
	
	function getCashierRequestData($items_id = NULL) 
	{   if($items_id=='') return false;
	    $CI =& get_instance();
		$CI->db->from('manage_cashier_requests');		
		$CI->db->where('id',$items_id);
		$CI->db->where('status','0');		
		$query = $CI->db->get();
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		else{ return false; }
	}	
	
	//******************GET ALL CASHIER REQUEST*****************************//
	function Get_All_Cashier_RequestList($request_type = NULL, $search=NULL)
	{  
	    $CI =& get_instance();
		$searchquery='';
	    
		
		if($request_type=='dentals'){ $Lab_TestLibrary ='lab_dentals';}
		if($request_type=='investigation'){ $Lab_TestLibrary ='lab_investigation'; }
		if($request_type=='uss_xrays'){ $Lab_TestLibrary ='lab_uss_xrays'; }		
	    if($request_type=='eyeclinic'){ $Lab_TestLibrary ='lab_eyeclinic'; }
		if($request_type=='specialist'){ $Lab_TestLibrary ='lab_specialist'; }
		if($request_type=='otherservice'){ $Lab_TestLibrary ='lab_otherservices'; }
		if($request_type=='medicine_drugs'){ $Lab_TestLibrary ='lab_medicine'; }
		
		if($request_type=='customers_opd' || $request_type=='customers_ipd' || $request_type=='customers_admission_ipd' ) 
		{  if($search!=''){ $searchquery = " and ( p.first_name like '%".$search."%' || p.last_name like '%".$search."%' || CONCAT(p.first_name,' ',p.last_name) LIKE '%".$search."%' ) "; }
	       $query = "select cr.id,cr.category_type,cr.added_date,c.account_number,p.first_name,p.last_name from ".$CI->db->dbprefix('manage_cashier_requests')." cr 
				   left join ".$CI->db->dbprefix('customers')." c on (cr.person_id = c.person_id) 
				   left join ".$CI->db->dbprefix('people')." p on (c.person_id = p.person_id)
				   where cr.status='0' and c.deleted ='0' $searchquery and cr.category_type ='".$request_type."' order by cr.id desc";
	    }
		
		
	   else
	   { 
			if($search!='')
			{ 
			   $searchquery = " and (p.first_name like '%".$search."%' || p.last_name like '%".$search."%'  || CONCAT(p.first_name,' ',p.last_name) LIKE '%".$search."%' "; 
			   if($request_type=='specialist') { $searchquery .= " || lc.lab_name like '%".$search."%' "; }
			   else { $searchquery .= " || lc.name like '%".$search."%' "; }
			   $searchquery .= ")";
			}
			 if($request_type=='specialist') 
			 { 
				$query = "select cr.id,cr.category_type,cr.added_date,c.account_number,p.first_name,p.last_name,lc.lab_name from ".$CI->db->dbprefix('manage_cashier_requests')." cr 
						  left join ".$CI->db->dbprefix('customers')." c on (cr.person_id = c.person_id) 
						  left join ".$CI->db->dbprefix('people')." p on (c.person_id = p.person_id)
						  left join ".$CI->db->dbprefix($Lab_TestLibrary)." lt on (lt.id = cr.lab_test_id)
						  left join ".$CI->db->dbprefix('lab_category')." lc on (lc.id = lt.lab_category_id)					   
						  where cr.status='0' and c.deleted ='0' $searchquery and cr.category_type ='".$request_type."' order by cr.id desc";
			 }
			 else
				 {  
					$query = "select cr.id,cr.category_type,cr.added_date,c.account_number,p.first_name,p.last_name,lc.name from ".$CI->db->dbprefix('manage_cashier_requests')." cr 
							  left join ".$CI->db->dbprefix('customers')." c on (cr.person_id = c.person_id) 
							  left join ".$CI->db->dbprefix('people')." p on (c.person_id = p.person_id)
							  left join ".$CI->db->dbprefix($Lab_TestLibrary)." lt on (lt.id = cr.lab_test_id)
							  left join ".$CI->db->dbprefix('items')." lc on (lc.item_id = lt.lab_category_id)					   
							  where cr.status='0' and c.deleted ='0' and lc.deleted ='0' $searchquery and cr.category_type ='".$request_type."' order by cr.id desc";
				 }
		}	
		  //echo $query; die;
		$Execute_data =  $CI->db->query($query); //return $Execute_data;
		
		$RequestData =' <table class="tablesorter" id="sortable_table">
						<tr>
							<th>'.lang('common_full_name').'</th>
							<th>'.lang('common_request_date').'</th>
							<th>'.lang('sales_procedure').'</th>
							<th'.lang('common_action').'</th>
						</tr>';
       if($Execute_data->num_rows()>0)
       {
          foreach($Execute_data->result() as $rowdata) 
		  {  $sales_procedure='_';
			if($request_type=='specialist') { if(isset($rowdata->lab_name) && $rowdata->lab_name!='') $sales_procedure = $rowdata->lab_name; }
			else { if(isset($rowdata->name) && $rowdata->name!='') $sales_procedure = $rowdata->name; }
			$RequestData .='<tr class="row_bg_css" id="request_'.$rowdata->id.'">
							<td width="20%">'.ucwords(strtolower($rowdata->first_name)).' '. ucwords(strtolower($rowdata->last_name)).'</td>
							<td width="20%">'.date('d-m-Y, H:i:s',strtotime($rowdata->added_date)).'</td>  
							<td width="20%">'.$sales_procedure .'</td>
							<td width="25%"><a href="javascript:;" title="Edit Diagnoses" onclick="UpdateCahier_Request('.$rowdata->id.',2)"><span class="appoint_cancel">'.lang('common_cancel').'</span></a>
							<a href="javascript:;" title="Edit Diagnoses" onclick="UpdateCahier_Request('.$rowdata->id.',1)"><span class="appoint_complete">'.lang('common_complete_and_copy').'</span></a></td>
							</tr>';

         }
       }	   
	   $RequestData .= '</table>'; 
	   return $RequestData;	   
	}	
	
	function update_Cashier_Request_old( $item_id = NULL ,$action_type = NULL )
	{
	  $CI =& get_instance();
	  if($item_id!='' && $action_type=='cancel')
	  { $status ='2';
        $login_uerData = $CI->Employee->get_logged_in_employee_info();
        $customer_data = array('status'=>$status,'close_date'=>date('Y-m-d:H:i:s'),'updated_by_employee_id'=>$login_uerData->person_id); 
		$CI->db->where('id', $item_id);
		$success = $CI->db->update('manage_cashier_requests',$customer_data);		
		if($success)return true; else return false;
	   }	
	   else return false;		
	}
	
	function update_Cashier_Request( $item_id = NULL ,$action_type = NULL )
	{
	  $CI =& get_instance();
	  if($item_id!='' && $action_type!='')
	  { if($action_type=='complete') $status ='1';else $status ='2';
        $login_uerData = $CI->Employee->get_logged_in_employee_info();
        $customer_data = array('status'=>$status,'close_date'=>date('Y-m-d:H:i:s'),'updated_by_employee_id'=>$login_uerData->person_id); 
		$CI->db->where('id', $item_id);
		$success = $CI->db->update('manage_cashier_requests',$customer_data);		
		if($success)return true; else return false;
	   }	
	   else return false;		
	}
	 
}
?>