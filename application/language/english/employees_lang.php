<?php
$lang['employees_id'] = 'Employee ID';
$lang['employees_employee']='Employee';
$lang['employees_new']='New Employee';
$lang['employees_update']='Update Employee';
$lang['employees_confirm_delete']='Are you sure you want to delete the selected employees?';
$lang['employees_none_selected']='You have not selected any employees to delete';
$lang['employees_error_adding_updating'] = 'Error adding/updating employee';
$lang['employees_successful_adding']='You have successfully added employee';
$lang['employees_successful_updating']='You have successfully updated employee';
$lang['employees_successful_deleted']='You have successfully deleted';
$lang['employees_one_or_multiple']='employee(s)';
$lang['employees_cannot_be_deleted']='Could not deleted selected employees, one or more of the employees has processed sales or you are trying to delete yourself :)';
$lang['employees_employee_pf_no']			='Employee PF NO';
$lang['employees_username']					='Username';
$lang['employees_password']					='Password';
$lang['employees_repeat_password']			='Password Again';
$lang['employees_confirm_password']			='Confirm Password';
$lang['employees_current_password']			='Current Password';


$lang['employees_username_required']='Username is a required field';
$lang['employees_username_minlength']='The username must be at least 5 characters';
$lang['employees_password_required']='Password is required';
$lang['employees_password_minlength']='Passwords must be at least 8 characters';
$lang['employees_password_must_match']='Passwords do not match';
$lang['employees_basic_information']='Employee Basic Information';
$lang['employees_login_info']='Employee Login Info';
$lang['employees_permission_info']='Employee Permissions and Access';
$lang['employees_permission_desc']='Check the boxes below to grant access to modules';
$lang['employees_error_updating_demo_admin'] = 'You can not change the demo admin user';
$lang['employees_error_deleting_demo_admin'] = 'You can not delete the demo admin user';
$lang['employees_password_invalid'] 		 = 'Invalid Password, Please try again!';
$lang['employees_login_successful'] 		 = 'You have successfully login';
$lang['employees_logout_successful']		 = 'You have successfully logout'; 
$lang['employees_history']					 = 'History';
$lang['employees_employee_history']			 = 'Employee History';
$lang['employees_activity_type']			 = 'Activity Type';
$lang['employees_activity_type_category']	 = 'Activity Category';
$lang['employees_activity_item_name']	     = 'Item Name';
$lang['employees_activity_description']	     = 'Description';
$lang['employees_module_view']	             = 'You have view Employees Module';
$lang['employees_information_view']	         = 'You have view Employees Information';
$lang['employees_export_successfull'] 		 = 'Export Employees successfull!';
$lang['employees_online_employees']	         = 'Online Employees';
$lang['employees_chat']	         		 	 = 'Chat';
$lang['employees_online']	         		 = 'Online';
$lang['employees_offline']	         		 = 'Offline';
$lang['employees_change_password']	         = 'Change Password';
$lang['employees_change_password_notice']	 = 'Your password is valid only for 90 days';

$lang['employees_change_password_successfull'] = 'You have successfully change password!';
$lang['employees_change_password_error'] 	   = 'Sorry, Error to change password!';
$lang['employees_change_password_form'] 	   = 'Please enter your new password';

$lang['employees_password_contain_digit'] 	   = 'Password should contain at least one digit';
$lang['employees_password_contain_lower_case'] = 'Password should contain at least one lower case';
$lang['employees_password_contain_upper_case'] = 'Password should contain at least one upper case';
$lang['employees_password_digit_limit'] 	   = 'Password should contain at least one special characters';
$lang['employees_both_password_not_same'] 	   = 'Both password are not same';
$lang['employees_invalid_current_password']    = 'Invalid current password, Please try again';



?>