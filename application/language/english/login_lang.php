<?php
$lang['login_login']							= 'Login';
$lang['login_username']							= 'Username';
$lang['login_password']							= 'Password';
$lang['login_go']								= 'Go';
$lang['login_invalid_username_and_password']	= 'Invalid username/password';
$lang['login_invalid_username_or_email']		= 'Invalid Username / E-Mail';

$lang['login_password_expired']	 				= 'Password is expired, Please contact our Technical Team!';
$lang['login_password_setting_error']   	 	= 'Password setting error, Please contact our Technical Team!';
$lang['login_invalid_username_and_password_empty']= 'Invalid username/password empty' ;

$lang['login_welcome_message']					= 'The United Republic of Tanzania';$lang['login_project_title_name']				= 'Health Management Information System';$lang['login_form_title']						= 'Login to Your Account';
$lang['login_version'] 							=  'Version';$lang['login_confirm_password'] 				= 'Confirm password';
$lang['login_reset_password_for']   			= 'Reset password for';
$lang['login_reset_password'] 					= 'Reset password';
$lang['login_reset_password_message']		    = 'You have requested to reset your password, please click the link below in order to complete the password reset process';$lang['login_password_reset_has_been_sent'] 	= 'A password reset e-mail has been sent to your e-mail. Please check your e-mail and click the link.';$lang['login_password_has_been_reset'] 			= 'Your password has been reset, please click the following link: ';$lang['login_passwords_must_match_and_be_at_least_8_characters'] = 'Passwords must match and be at least 8 characters';$lang['login_subscription_cancelled_within_30_days']			 = 'Your subscription was recently cancelled, please contact support@4paytz.com to ensure uninterrupted service';$lang['login_subscription_terminated'] 			= 'Your subscription has been terminated, please contact support@4paytz.com to discuss';$lang['login_mode'] 						    = 'Mode';$lang['login_automatic'] 						= 'Automatic';
?>