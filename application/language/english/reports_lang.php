<?php
$lang['reports_reports'] = 'Reports';
$lang['reports_report'] = 'Report';
$lang['reports_welcome_message'] = 'Welcome to 4pay HPS reports panel. Please select a report to view.';
$lang['reports_sales_summary_report'] = 'Sales Summary Report';
$lang['reports_categories_summary_report'] = 'Categories Summary Report';
$lang['reports_customers_summary_report'] = 'Patients Summary Report';
$lang['reports_suppliers_summary_report'] = 'Department Summary Report';
$lang['reports_items_summary_report'] = 'Items Summary Report';
$lang['reports_employees_summary_report'] = 'Employees Summary Report';
$lang['reports_taxes_summary_report'] = 'Grade Summary Report';
$lang['reports_date'] = 'Date';
$lang['reports_name'] = 'Name';
$lang['reports_quantity_purchased'] = 'Quantity Purchased';
$lang['reports_sale_id'] = 'Sale ID';
$lang['reports_items_purchased'] = 'Items Purchased';
$lang['reports_sold_by'] = 'Sold By';
$lang['reports_sold_to'] = 'Sold To';
$lang['reports_category'] = 'Category';
$lang['reports_customer'] = 'Patient';
$lang['reports_employee'] = 'Employee';

$lang['reports_item'] = 'Item';
$lang['reports_items'] = 'Items';
$lang['reports_supplier'] = 'Department';
$lang['reports_employees'] = 'Employees';
$lang['reports_subtotal'] = 'Subtotal';
$lang['reports_total'] = 'Total';
$lang['reports_tax'] = 'Grade';


$lang['reports_attendance'] 		= 'Attendance';
$lang['reports_expire_items'] 		= 'Expire Items';
$lang['reports_mtuha_reports']		= 'MTUHA Reports';
$lang['reports_tika_customers'] 	= 'TIKA Patient';
$lang['reports_clinical_daignosis'] = 'Clinical Daignosis';



$lang['reports_profit'] = 'Profit';
$lang['reports_report_input'] = 'Report Input';
$lang['reports_type'] = 'Type';
$lang['reports_date_range'] = 'Date Range';
$lang['reports_today'] = 'Today';
$lang['reports_yesterday'] = 'Yesterday';
$lang['reports_last_7'] = 'Last 7 Days';
$lang['reports_this_month'] = 'This Month';
$lang['reports_last_month'] = 'Last Month';
$lang['reports_this_year'] = 'This Year';
$lang['reports_last_year'] = 'Last Year';
$lang['reports_all_time'] = 'All Time';
$lang['reports_detailed_sales_report'] = 'Detailed Sales Report';
$lang['reports_comments'] = 'Comments';
$lang['reports_discount'] = 'Exemption ';
$lang['reports_payment_type'] = 'Payment Type';
$lang['reports_sales'] = 'Sales';
$lang['reports_categories'] = 'Categories';
$lang['reports_customers'] = 'Patients';
$lang['reports_suppliers'] = 'Department';
$lang['reports_items'] = 'Items';
$lang['reports_employees'] = 'Employees';
$lang['reports_taxes'] = 'Grade';
$lang['reports_customer'] = 'Patient';
$lang['reports_tax_percent'] = 'Grade Percent';
$lang['reports_quantity_purchased'] = 'Quantity Purchased';
$lang['reports_serial_number'] = 'Serial #';
$lang['reports_description'] = 'Description';
$lang['reports_date'] = 'Date';
$lang['reports_sales_amount'] = 'Sales amount';
$lang['reports_revenue'] = 'Revenue';
$lang['reports_discounts'] = 'Exemption ';
$lang['reports_discounts_summary'] = 'Exemption Summary';
$lang['reports_discounts_summary_report'] = 'Exemption Summary Report';
$lang['reports_discount_percent'] = 'Exemption Percent';
$lang['reports_count'] = 'Count';
$lang['reports_summary_reports'] = 'Summary Reports';
$lang['reports_graphical_reports'] = 'Graphical Reports';
$lang['reports_Attendance'] = 'Attendance';
$lang['reports_detailed_reports'] = 'Detailed Reports';
$lang['reports_inventory_reports'] = 'Inventory Reports';
$lang['reports_low_inventory'] = 'Low Inventory';
$lang['reports_inventory_summary'] = ' Inventory Summary';
$lang['reports_item_number'] = 'Item Number';
$lang['reports_reorder_level'] = 'Reorder Level';
$lang['reports_low_inventory_report'] = 'Low Inventory Report';
$lang['reports_item_name'] = 'Item Name';
$lang['reports_inventory_summary_report'] = 'Inventory Summary Report';
$lang['reports_payment_type'] = 'Payment Type';
$lang['reports_payments_summary_report'] = 'Payments Summary Report';
$lang['reports_payments'] = 'Payments';
$lang['reports_receivings'] = 'Receivings';
$lang['reports_received_by'] = 'Received By';
$lang['reports_supplied_by'] = 'Supplied by';
$lang['reports_items_received'] = 'Items Received';
$lang['reports_detailed_receivings_report'] = 'Detailed Receivings Report';
$lang['reports_sale_type'] = 'Sale Type';
$lang['reports_all'] = 'All';
$lang['reports_returns'] = 'Returns';
$lang['reports_deleted_sales'] = 'Deleted Sales';
$lang['reports_deleted_sales_report'] = 'Deleted Sales Report';
$lang['reports_receiving_id'] = 'Receiving ID';
$lang['reports_item_kits_summary_report'] = 'Item Kits Summary Report';
$lang['reports_export_to_excel'] = 'Export to Excel';
$lang['reports_month_01'] = 'January';
$lang['reports_month_02'] = 'February';
$lang['reports_month_03'] = 'March';
$lang['reports_month_04'] = 'April';
$lang['reports_month_05'] = 'May';
$lang['reports_month_06'] = 'June';
$lang['reports_month_07'] = 'July';
$lang['reports_month_08'] = 'August';
$lang['reports_month_09'] = 'September';
$lang['reports_month_10'] = 'October';
$lang['reports_month_11'] = 'November';
$lang['reports_month_12'] = 'December';
$lang['reports_giftcards'] = 'Deposit Cards';
$lang['reports_giftcard_summary_report'] = 'Deposit Card Summary Report';

$lang['reports_register_log_title'] = 'Register Logs';
$lang['reports_shift_start'] = 'Shift Start';
$lang['reports_shift_end'] = 'Shift End';
$lang['reports_open_amount'] = 'Open Amount';
$lang['reports_close_amount'] = 'Close Amount';
$lang['reports_cash_sales'] = 'Cash Sales';
$lang['reports_difference'] = 'Difference';

$lang['reports_total_cash_sales'] = 'Total Cash Sales';
$lang['reports_total_shortages'] = 'Total Shortages';
$lang['reports_total_overages'] = 'Total Overages';
$lang['reports_total_difference'] = 'Total Difference';
$lang['reports_inventory_total'] = 'Total Inventory Value';
$lang['reports_inventory_sale_total'] = 'Total Inventory Value By Unit Price';
$lang['reports_expire_items_summary'] = 'Expire Items Summary';

$lang['reports_mtuha_report'] = 'MTUHA OPD monthly Report';
$lang['mthua_report'] = 'MTUHA Report';
$lang['mathua_report_title'] = 'MTUHA OPD MONTHLY REPORT';

$lang['sr_no'] = 'NO';
$lang['mthua_list_descriptions'] = 'Descriptions';
$lang['mthua_OPD_attendance'] = 'OPD attendance';
$lang['mthua_OPD_Diagnoses'] = 'OPD Diagnoses';
$lang['mthua_OPD_new_attendance'] = 'OPD new attendance this month (*)';
$lang['mthua_OPD_repeat_attendance'] = 'OPD repeated attendance';

$lang['mathua_report_age_range1'] = 'Age 0 to 1 Month';
$lang['mathua_report_age_range2'] = 'Age 1 month To 1 Year';
$lang['mathua_report_age_range3'] = 'Age 1 year To 5 Years';
$lang['mathua_report_age_range4'] = 'Age 5 year To And above';

$lang['mathua_report_age_under_5'] 	= 'Age under 5 years';
$lang['mathua_report_age_above_60'] = 'Age above 60 years';
$lang['mathua_report_age_others'] 	= 'Others Case';

$lang['mthua_list_total'] = 'Total';
$lang['mthua_sex_male'] = 'Male';
$lang['mthua_sex_female'] = 'Female';

$lang['tika_registration'] = 'TIKA Patients Report';
$lang['tika_pateint'] = 'TIKA Patient';
$lang['tika_pateint_report_summary'] = 'TIKA Patients Summary Report';

$lang['tika_registration_number']='TIKA Registration No';
$lang['tika_membership_expire_date']='Membership Expire Date';
$lang['tika_total_registeration']='TIKA new Registration';
$lang['tika_attendance_repeaters']='TIKA Attendance Repeaters';
$lang['tika_new_registeration_total_amount']='TIKA new Registration Total';
$lang['tika_patient_total_spend_amount']='TIKA Patients Spend Amount';

//IPD/In-Patient Department 
$lang['ipd_patient_department'] 		= 'IPD / In Patient';
$lang['ipd_patient_department_report']  = 'IPD / In Patient Report';
$lang['ipd_patient_department_summary'] = 'IPD / In Patient Summary Report';
$lang['ipd_choose_block']  				= 'Select Block Name/No';
$lang['reports_ipd_type1'] 				= 'Discharge';
$lang['reports_ipd_type2']				= 'New Admission';
$lang['reports_ipd_type3'] 			    = 'Not Paid';
$lang['reports_ipd_type4']				= 'Paid';
$lang['reports_total_ipd_patient'] 				  = 'Total IPD /In Patients';
$lang['reports_total_ipd_patient_paid_amount']	  = 'Total Paid IPD / In Patient  ';
$lang['reports_total_ipd_patient_balance_amount'] = 'Total Balance IPD / In Patient ';
$lang['reports_sales_type'] = 'Sales Type';

$lang['clinical_daignosis_report_title']   = 'Clinical Daignosis Report'; 
$lang['reports_clinical_daignosis_report'] = 'Clinical Daignosis';
$lang['report_dead_body_title']  		   = 'Dead Body'; 
$lang['report_dead_body_reports'] 		   = 'Dead Body Report';
$lang['report_diabetes_reports'] 		   = 'Diabetes Report';

$lang['reports_doctor_parformance'] 	     = 'Doctors Parformance';
$lang['reports_doctor_parformance_reports']  = 'Doctors Performance Report';
$lang['reports_employee_parformance']	     = 'Employees Parformance';
$lang['reports_employee_parformance_reports']= 'Employees Performance Report';
$lang['reports_doctor_parformance_summary']  = 'Doctors Performance Summary';
$lang['reports_employee_parformance_summary']= 'Employees Performance Summary';

//Transactions reports//
$lang['reports_epay_transactions'] 	     	 = 'EPAY Transactions';
$lang['reports_epay_refund_transactions']    = 'EPAY Refund Transactions';
$lang['report_epay_transactions_reports']    = 'EPAY Transactions Report';
$lang['report_epay_refund_transactions_reports']= 'EPAY Refund Transactions Report';

$lang['report_transactions_date'] 	     	 = 'Transaction Date';


$lang['report_diabetes_reports_title'] 	   		= 'NATIONAL DIABETES PROGRAMME';
$lang['report_diabetes_clinic_services_title']  = 'DIABETES CLINIC SERVICES MONTHLY REPORT';
$lang['report_for_the_month_name'] 	   	  		= 'Report for the month of';
$lang['report_contact_person_name'] 	   	    = 'Contact Person Name';
$lang['report_generate_by'] 	   	   			= 'Report by';
$lang['report_list_view']    	     			= 'You have view Report Module';
$lang['report_graphical_summary_customers_view']= 'You have view Graphical Summary Customers Report';
$lang['report_summary_customers_view']		    = 'You have view Summary Customers Report';
$lang['report_graphical_summary_customers_view']= 'You have view Graphical Summary Customers Report';
$lang['report_specific_customer_view']          = 'You have view Specific Customers Report';
$lang['report_graphical_summary_employees_view']= 'You have view Graphical Summary employees Report';
$lang['report_summary_employees_view']          = 'You have view Summary employees Report';
$lang['report_specific_employee_view']          = 'You have view Specific employees Report';
$lang['report_graphical_summary_sales_view']	= 'You have view Graphical Summary Sales Report';
$lang['report_summary_sales_view']          	= 'You have view Summary Sales Report';
$lang['report_detailed_sales_view']          	= 'You have view Detailed Sales Report';
$lang['report_detailed_register_log_view']      = 'You have view Detailed Register Log Report';
$lang['report_graphical_summary_categories_view']	= 'You have view Graphical Summary Categories Report';
$lang['report_summary_categories_view']          	= 'You have view Summary Categories Report';
$lang['report_graphical_summary_discounts_view']	= 'You have view Graphical Summary Exemption Report';
$lang['report_summary_discounts_view']          	= 'You have view Summary Exemption Report';
$lang['report_graphical_summary_items_view']		= 'You have view Graphical Summary Items Report';
$lang['report_summary_items_view']          		= 'You have view Summary Items Report';
$lang['report_graphical_summary_payments_view']		= 'You have view Graphical Summary Payments Report';
$lang['report_summary_payments_view']          		= 'You have view Summary Payments Report';
$lang['report_graphical_summary_suppliers_view']		= 'You have view Graphical Summary Suppliers Report';
$lang['report_summary_suppliers_view']          		= 'You have view Summary Suppliers Report';
$lang['report_specific_supplier_view']          		= 'You have view Specific Suppliers Report';
$lang['report_graphical_summary_taxes_view']			= 'You have view Graphical Summary Grade Report';
$lang['report_summary_taxes_view']          			= 'You have view Summary Grade Report';
$lang['report_detailed_receivings_view']          		= 'You have view Summary Receivings Report';
$lang['report_inventory_low_view']          			= 'You have view Inventory Low Report';
$lang['report_inventory_summary_view']          		= 'You have view Inventory Summary Report';
$lang['report_deleted_sales_view']          		    = 'You have view Deleted Sales Report';
$lang['report_summary_giftcards_view']          		= 'You have view Cards Summary Report';
$lang['report_detailed_giftcards_view']          		= 'You have view Detailed Cards Report';
$lang['report_detailed_attendance_view']          		= 'You have view Detailed Attendance Report';
$lang['report_expire_items_summary_view']          		= 'You have view Expire Items Summary Report';
$lang['report_mtuha_report_view']          				= 'You have view Mtuha Report';
$lang['report_clinical_daignosis_report_view']          = 'You have view Clinical_Daignosis Report';
$lang['report_tika_patient_summary_view']               = 'You have view Tika Patients Report'; 
$lang['report_ipd_in_patient_summary_view']          	= 'You have view IPD/In Summary Report';
$lang['report_dead_body_summary_view']          		= 'You have view Dead Body Summary Report';
$lang['report_diabetes_summary_view']              		= 'You have view Diabetes Summary Report';  
$lang['report_doctor_parformance_summary_view']         = 'You have view Doctor Performance Report';
$lang['report_employee_parformance_summary_view']       = 'You have view Employee Performance Report';
$lang['report_epay_transactions_summary_view']          = 'You have view EPAY Transactions Summary Report';
$lang['report_epay_refund_transactions_summary_view']	= 'You have view EPAY Refund Transactions Summary Report';
$lang['report_epay_transactions_view_details']  		= 'You have view EPAY Transactions Details Report';

?>