<?php
$lang['common_cleanup_old'] 		= 	'Cleanup old Items';
$lang['common_no_items_found']	    =	'Sorry, No items Found';
$lang['common_common']				=	'common';
$lang['common_submit']				=	'Submit';
$lang['common_next']				=	'Next';
$lang['common_back']				=	'Back';
$lang['common_or']					=	'OR';
$lang['common_powered_by']			=	'Powered by';
$lang['common_welcome']				=	'Welcome';
$lang['common_logout']				=	'Logout';
$lang['common_list_of']				=	'List of';
$lang['common_first_name']			=	'First Name';
$lang['common_full_name']			=	'Name';
$lang['common_email']				=	'E-Mail';
$lang['common_last_name']			=	'Last Name';
$lang['common_phone_number']		=	'Phone Number';
$lang['common_mobile_number']		=	'Mobile No';
$lang['common_telephone_number']	=	'Telephone';
$lang['common_patient_name']		=	'Patient Name';

$lang['common_sex']					=	'Sex';
$lang['common_male']				=	'Male';
$lang['common_female']				=	'Female';
$lang['common_age']					=	'Age';
$lang['common_birth_date']			=	'Birth Date';
$lang['common_address']				=	'Address';
$lang['common_address_1']			=	'Residence Address';
$lang['common_address_2']			=	'Address 2';
$lang['common_city']	 			=	'City';
$lang['common_distict']	 			=	'Distict';
$lang['common_state']	 			=	'State';
$lang['common_zip']		 			=	'Zip';
$lang['common_country']	 			=	'Country';
$lang['common_comments'] 			=	'Comments';
$lang['common_ten_cell_leader']		=	'Ten Cell Leader';
$lang['common_religion']			=	'Religion';
$lang['common_next_of_kin']			=	'Next of Kin';
$lang['common_blood_group']			=	'Blood Group';
$lang['common_edit']				=	'Edit';
$lang['common_add_new']				=	'Add New';
$lang['common_search']				=	'Search';
$lang['common_delete']				=	'Delete';
$lang['common_added_date']		    =   'Added Date'; 
$lang['common_date']				=   'Date'; 
$lang['common_location'] 			= 	'Location';


$lang['common_view_recent_sales']	=	'View Recent Sales';
$lang['common_you_are_using_phppos']=	'You are using 4pay HPS Version';
$lang['common_please_visit_my']		=	'Please visit my';
$lang['common_website']				=	'website';
$lang['common_fields_required_message']	=	'Fields in red are required';
$lang['common_learn_about_project']		=	'to learn the lastest information about the project';
$lang['common_searched_for']			=	'Searched for'; 
$lang['common_first_name_required']		=	'The first name is a required field.';
$lang['common_last_name_required']		=	'The last name is a required field';
$lang['common_email_invalid_format']	=	'The e-mail address is not in the proper format';
$lang['common_email_address_exist']		=	'E-mail address is already exist, Please choose another!';

$lang['common_patient_category_required']= 'The patient category selection is required field.';
$lang['common_enter_account_no']		= 'Enter Patient Account No';
$lang['common_account_number_required']	= 'The Account No is required field';
$lang['common_invalid_account_no']		= 'Invalid Patient Account No';
$lang['common_invalid_date_selection']	= 'Invalid date selection';
$lang['common_invalid_age_selection']	= 'Invalid age selection';

$lang['common_patient_not_registered_nhif_category']= 'This Patient is not Registered in NHIF Department, Please choose another one!';
$lang['common_ipd_in_department_not_registered']	= 'This Patient is not Registered in IPD/In-Patient Department, Please choose another one!';

$lang['common_full_name_required']				= 'The Full name is required field';
$lang['common_address_name_required']			= 'The address name is required field';
$lang['common_registration_type_normal'] 	    = 'Normal Registration';
$lang['common_registration_type_rapid'] 	    = 'Rapid Registration';
$lang['common_patient_category_type1'] 			= 'Cost Shearing';
$lang['common_patient_category_type2'] 			= 'TIKA';
$lang['common_patient_category_exemption'] 		= 'Exemption';
$lang['common_patient_temporary_exemption'] 	= 'Temporary Exemption';
$lang['common_patient_nhif']				    = 'NHIf Patient';
$lang['common_patient_nssf'] 					= 'NSSF Patient';
$lang['common_patient_chf'] 					= 'CHF Patient';
$lang['common_patient_chf'] 					= 'CHF Patient';

$lang['common_patient_category_type'] 			= 'Patient Category';
$lang['common_registration_type'] 	    		= 'Registration Type';
$lang['common_registration_type_view'] 	    	= 'All Registration Type';
$lang['common_patient_category_type_view'] 		= 'All Patient Category';

$lang['common_confirm_search']			=	'You have selected one or more rows, these will no longer be selected after your search. Are you sure you want to submit this search?';
$lang['common_no_persons_to_display']	=	'There are no people to display';
$lang['common_return_policy']			=	'Return Policy';
$lang['common_price']					=	'Price';
$lang['common_inv']						=	'inv';
$lang['common_det']						=	'details';
$lang['common_mailing_lists'] 			= 	'Mailing Lists';
$lang['common_update'] 					= 	'Update';
$lang['common_cumulative'] 				= 	'Cumulative?';
$lang['common_barcode_sheet'] 			= 	'Barcode Sheet';
$lang['common_barcode_labels'] 			= 	'Barcode Labels';
$lang['common_upload_file_not_supported_format'] = 'Your upload file has no data or not in supported format.';
$lang['common_yes'] 					= 'Yes';
$lang['common_no'] 						= 'No';
$lang['common_file_path'] 				= 'File path';
$lang['common_excel_import'] 			= 'Excel Import';
$lang['common_wait'] 					= 'Please wait...';
$lang['common_excel_export'] 			= 'Excel Export';
$lang['choose_file_to_upload']			= 'Choose a File';
$lang['report_posted_date'] 			= 'Report Posted Date';
$lang['common_patient_type_required']	='Patient type is a required field';
$lang['normal_patient'] 				= 'Normal Patient';
$lang['private_patient'] 				= 'Private Patient';
$lang['fast_track_patient'] 			= 'Fast Track Patient';
$lang['call_report'] 					= 'Documentation';
$lang['documentation'] 					= 'Documentation';
$lang['patinte_report_title'] 			= 'Patient Documentation';
$lang['common_referal'] 				= 'Patient Referal';
$lang['common_referal_documentation'] 		 = 'Sysem Referral Documentation';
$lang['common_referal_paper_documentation']  = 'Paper Documentation';

$lang['print_out'] 						= 'Print Out';
$lang['start_date'] 					= 'Start Date';
$lang['end_date']   					= 'End Date';
$lang['go']   							= 'Go';
$lang['reports']   						= 'Result';
$lang['start_date_validation']  		= 'Please Select Start Date';
$lang['end_date_validation']   			= 'Please Select End Date';
$lang['invalid_date_selection']   		= 'Invalid Date Selection';
$lang['day']   							= 'Day';
$lang['month'] 							= 'Month';
$lang['year']  							= 'Year';
$lang['common_student_registration_no']	= 'Student Registration No';
$lang['common_pfnumber']				= 'PF No';
$lang['common_specify_number']			= 'Specify Number';
$lang['common_age_day_required']    	= 'Patient day of age are required';
$lang['common_age_month_required']  	= 'Patient month of age are required';
$lang['common_age_year_required']   	= 'Patient year of age are required';
$lang['common_occupation_required']   	= 'Patient Occupation is a required';
$lang['common_member_pfnumber_required']= 'Patient PF No is a required';
$lang['common_sex_required']			= 'Patient sex type is a required field';
$lang['common_phone_member_required']   = 'Phone Number is a required';

$lang['common_patient_image']			= 'Patient Image';
$lang['common_profile_image']			= 'Profile Photo';
$lang['common_patient_profile']			= 'Vital Sign';
$lang['common_sign']					= 'Sign';

$lang['common_hospital_name']   		= 'Name of Hospital';
$lang['common_hospital_name_select']    = 'Select Hospital';
$lang['common_pdf']    				    = 'PDF';
$lang['common_excle']    				= 'EXCLE';
$lang['common_doc']    					= 'DOC';
$lang['common_mysql']    				= 'MYSQL';
$lang['common_send_to_pdf']    			= 'Send To PDF';
$lang['common_select_file_format'] 		= 'Select Format';
$lang['common_select_file_format_empty']= 'Please select report file format';


$lang['district_name']      			= 'District';
$lang['common_district_name']      		= 'District';
$lang['common_patient_image_type_not_allowed']='This file type is not allowed';
$lang['common_patient_image_size_not_allowed']='This File size is very small';

$lang['common_patient_name']			= 'Patient';
$lang['common_patient_file_no']			= 'Patient File No ';

$lang['common_home_title']				= 'Home';
$lang['common_home_page_logo_title']	= 'GoT - HoMIS';
$lang['common_about_title']				= 'ABOUT GoT - HoMIS';
$lang['common_about_videos']			= 'MY VIDEOS';
$lang['common_about_license']			= 'MY LICENSE';
//$lang['common_about_us_title']			= '4PAY Hospital Management System (4PAY HMS)';

$lang['common_about_us_license_title']  = "LICENSE AGREEMENT";
$lang['common_about_us_video_title'] 	= "Video Headers...";
$lang['common_about_us_license_content']="•  This system is for unlimited number of users<br>
										  •  It should not be copied or sold <br>
										  •  The source code belongs to Kaaya Computing Education and Research Network copyrights<br>
										  •  The Company will not be responsible for damages on computers, but can do computer repairs upon a separate agreement.<br>
										  •  The “client” will be responsible for damages on computers, printers, networking and misuse of the source code.<br>";

$lang['no_record_found']  		  			=   'Sorry no records found!';
$lang['common_management']		  			=	'Management';
$lang['common_administrator']		  		=	'Administrator';
$lang['common_ipd_department']	  			=	'IPD/In-Patient Department';
$lang['common_store_management']  			= 	'Stores Management';
$lang['common_doctor_appointment']			=	'Dr. Appointment Manager';
$lang['common_diabetes_care']	 		 	=   'Diabetes Care';
$lang['common_confirm_printing']	   		=	'Are you sure you want to take print of seleted items?';
$lang['common_confirm_change_status']	    =	'Are you sure you want to change item status?';
$lang['common_change_status_successful']    = 	'You have successfully change item status';
$lang['common_change_status_error'] 		= 	'Error to change item status';
$lang['common_group_name'] 					= 	'Group Name';
$lang['common_group_name_required'] 		= 	'Group Name is a required';
$lang['common_subgroup_name'] 				= 	'Sub Group Name';
$lang['common_subgroup_name_required'] 		= 	'subgroup Group Name is a required';

$lang['common_select_type'] 				= 	'Select Type'; 
$lang['common_select_price'] 				= 	'Select Price'; 
$lang['common_add_more'] 					= 	'Add more';
$lang['common_remove_more_link'] 			= 	'Remove';
$lang['common_upload_images'] 			    = 	'Upload Images';
$lang['common_upload_result'] 			    = 	'Upload Result';

$lang['common_download'] 				    = 	'Download';
$lang['common_add'] 						= 	'Add';
$lang['common_doctor_name'] 				= 	'Doctor Name';
$lang['common_doctor_status'] 				= 	'Doctor Status';
$lang['common_admission_status'] 			= 	'Admission Status';
$lang['common_surgery_status'] 				=   'Surgery';
$lang['common_allegis_food_drugs'] 			=   'Allegis Food / Drugs';
$lang['common_invalid_enter_required']		=   'Please Enter all required field';

$lang['common_update_profile']				=   'Vital Sign'; 
$lang['common_services_status']				=   'Service Status';
$lang['common_send_to_cashier']				=   'Send To Cashier'; 
$lang['common_send_to_cashier_for_ipd']		=   'Send To Cashier for IPD'; 
$lang['common_send_to_cashier_for_admission']=   'Send To Admission'; 
$lang['common_admission']				    =   'Admission'; 
$lang['common_request_date']				=   'Request Date'; 
$lang['common_complete']					=   'Complete'; 
$lang['common_cancel']						=   'Cancel'; 
$lang['common_not_complete']				=   'Not Complete'; 
$lang['common_total_attend_patients'] 		= 	'Total Attend Patients';

$lang['common_total_lab_investigation'] 	= 	'Total Lab Investigation';
$lang['common_total_uss_xrays'] 			= 	'Total USS /X-ray';
$lang['common_total_specialist'] 			= 	'Total Specialist';
$lang['common_total_otherservices'] 		= 	'Total Other Services';
$lang['common_total_eyeclinic'] 			= 	'Total Eye Clinic';

$lang['common_none_selected']				=   'You have not selected any patients';
$lang['common_complete_and_copy']			=	'Complete and Copy'; 

$lang['common_confirm_send_request_for_ipd_admission']       =	'Are you sure you want to send request for IPD Admission ?';
$lang['common_confirm_cashier_send_request_status_for_ipd']  =	'Are you sure you want to send cashier request for IPD?';

$lang['common_confirm_cashier_send_request_status']  =	'Are you sure you want to send request to cashier?';
$lang['common_cashier_send_request_successful']      = 	'You have successfully send request to cashier';
$lang['common_cashier_send_request_error'] 		     = 	'Error to send cashier request';
$lang['common_cashier_send_request_already_exist']   = 	'Sorry! You have already send request to cashier';  
$lang['common_search_profile']						 =  'Search Profile'; 
$lang['common_search_profile_form']					 =  'Search Patient Profile'; 
$lang['common_update_profile_form']					 =  'Update Patient Profile'; 
$lang['common_select_category'] 					 =  'Select Category';
$lang['common_select_category_required']  			 =  'Category name is a required field';
$lang['common_measurement_required'] 				 =  'Measurement data is a required field';
$lang['common_updated_profile_successful']			 =  'You have successfully updated customer profile';
$lang['common_updated_profile_error']     		     =  'Error updating customer profile';
$lang['common_view_profile']						 =  'Search Profile'; 
$lang['common_patient_information']					 =  'Basic Information'; 
//***********BODY Measurement CONTENT******************//
$lang['common_measurement'] 						 =  'Measurement';
$lang['common_pulse_heart_rate'] 					 =  'Pulse/Heart Rate';
$lang['common_pulse_heart_rateper_minute'] 			 =  'Pulse/Heart Rate (Per Minute)';
$lang['common_weight']								 =  'Weight'; 
$lang['common_temperature']							 =  'Temperature'; 
$lang['common_blood_pressure']						 =  'Blood Pressure'; 
$lang['common_height']						 		 =  'Height'; 
$lang['common_respiratory_rate']			 		 =  'Respiratory Rate'; 
$lang['common_respiratory_rate_per_mninut']			 =  'Respiratory Rate (Per Minute)'; 

$lang['common_bp']						 			 =  'BP'; 
$lang['common_pulse']						 		 =  'PULSE'; 
$lang['common_resp']						 		 =  'RESP'; 
$lang['common_temp']						 		 =  'TEMP'; 
$lang['common_iv']						 			 =  'I.V'; 
$lang['common_oral']						 		 =  'oral'; 
$lang['common_ngt']						 			 =  'ngt'; 
$lang['common_vomiting']						 	 =  'Vomiting'; 
$lang['common_urine']						 		 =  'Urine'; 
$lang['common_stool']						 		 =  'Stool'; 
$lang['common_input']						 		 =  'Intake'; 
$lang['common_output']						 		 =  'Output'; 
$lang['common_treatment']							 =  'Treatment';
$lang['common_remarks']								 =  'Remarks';
$lang['common_date_time']							 =  'Date/Time';

$lang['common_from_date']							 =  'From Date'; 
$lang['common_from']								 =  'From'; 
$lang['common_to']									 =  'To'; 
$lang['common_date']								 =  'Date'; 
$lang['common_time']								 =  'Time'; 
$lang['common_print_profile']						 =  'Print Profile'; 
$lang['common_count']						 		 =  'Count'; 
$lang['common_reason'] 	     						 =  'Reason'; 
$lang['common_patient_status'] 						 =  'Patient Status';
$lang['common_status'] 								 =  'Status';
$lang['common_status_active'] 						 =  'Active';
$lang['common_status_inactive'] 					 =  'Inactive';

$lang['common_view'] 								 =  'View';
$lang['common_view_reason'] 						 =  'View Reason';
$lang['common_dosage_count'] 						 =  'Dosage Count';
$lang['common_opd']						 			 = 'OPD';
$lang['common_ipd']					     			 = 'IPD';
$lang['common_submit_comment']					     = 'Submit Comment';
$lang['common_change_count']						 = 'Change Count';
$lang['common_change_reason']					     = 'Change Reason';
$lang['common_cancel_reason']					     = 'Cancel Reason';
$lang['common_dispanse_count']					     = 'Dispanse Count';   
$lang['common_dispanse_reason']					     = 'Dispanse Reason';
$lang['common_birth_date_required']					 = 'Birth Date is a required field';
$lang['common_selected_item_already_exist'] 		 = 'Your selected item is already exists';
$lang['common_successful_adding_selected_item']	 	 = 'You have successfully added selected item';
$lang['common_successful_updating_selected_item']	 = 'You have successfully updated selected item';
$lang['common_error_adding_updating_selected_item']  = 'Error adding/updating selected item';
$lang['common_confirm_delete_items']      			 =	'Are you sure you want to delete seleted items?';
$lang['common_delete_items_empty']      			 =	'Sorry! You have not selected any items!';
$lang['common_successful_delete_selected_items'] 	 =	'You have successfully deleted selected items';
$lang['common_error_deleted_items']      			 =	'Sorry! You have not delted selected items';

$lang['common_medicine']						 	 = 'Medicine';
$lang['common_medicine_drugs']    					 = 'Medicine/Drugs';

$lang['common_service']					     		 = 'Service';
$lang['common_service_type']					 	 = 'Service Type';
$lang['common_laboratory_service']					 = 'Laboratory Service';
$lang['common_radiology_service']					 = 'Radiology Service';
$lang['common_dental_service']					 	 = 'Dental Service';
$lang['common_eye_clinic_service']					 = 'Eye Clinic Service';
$lang['common_others_service']					 	 = 'Other Service';

//*********MEMBERS ACTIVITIES CONTENTS************************//
$lang['common_customer_view_call_reports']           =	'You have view Patients Call Reports';
$lang['common_customer_view_report_card']            =	'You have view Patients Report Card';
$lang['common_customer_view_patient_appointment']    =	'You have view Patients Appointment';
$lang['common_customer_view_patient_profile']   	 =	'You have view Patients Profile';
$lang['common_mtuha_daignoses_added'] 			     =  'Mthuha Diagnoses was added successfully!';
$lang['common_mtuha_daignoses_updated'] 			 =  'Mthuha Diagnoses was updated successfully!';
$lang['common_clinical_daignoses_added'] 			 =  'Clinical Diagnoses was added successfully!';
$lang['common_clinical_daignoses_updated'] 			 =  'Clinical Diagnoses was updated successfully!'; 
$lang['common_daibetes_added'] 			    		 =  'Daibetes was added successfully!';
$lang['common_daibetes_updated'] 			 		 =  'Daibetes was updated successfully!';
$lang['common_customer_view_lab_service']    	     =	'You have view Lab Services';
$lang['common_view_patient_result_pr']    			 =	'You have view patients Result V/R';
$lang['common_customer_view_medicine_drugs']    	 =	'You have view Medicine/Drugs';
$lang['common_view_patient_dental_profile'] 		 =	'You have view Patients Dental Profile';
$lang['common_customer_view_eyeclinic']    	         =	'You have view Patients Eye Clinic';
//*********MEMBERS ACTIVITIES CONTENTS************************//

//*********RESIDENCE CONTENTS************************//
$lang['common_manage_Residence']    =	'Manage Residence';
$lang['common_residence_form']   	=	'List of Residence';
$lang['common_residence_list']   	=	'Residence List';
$lang['common_add_new_residence']  	=	'Add New Residence ';
$lang['common_residence_name']		=	'Residence Name';
$lang['common_residence_name_empty']=	'Please enter Residence Name';
$lang['common_residence_name_exist']=	'Residence Name is already exist!';
$lang['common_residence_name_update']=	'You have updated Residence Name!';
$lang['common_residence_name_added'] =	'You have added Residence Name!';
$lang['common_residence_name_error'] =	'Error to add/update Residence Name!';
$lang['common_residence_select']     =	'Select Residence Name';

//*********OCCUPATIONS CONTENTS************************//
$lang['common_occupation_group'] 	  =	'Occupation Group';
$lang['common_manage_occupation']     =	'Manage Occupation';
$lang['common_occupation_form']   	  =	'List of Occupation';
$lang['common_occupation_list']   	  =	'Occupation List';
$lang['common_add_new_occupation']    =	'Add New Occupation';
$lang['common_occupation_name']		  =	'Occupation Name';
$lang['common_occupation']		  	  =	'Occupation';
$lang['common_occupation_name_empty'] =	'Please enter Occupation Name';
$lang['common_occupation_group_empty'] ='Please enter Occupation Group';
$lang['common_occupation_name_exist'] =	'Occupation Name is already exist!';
$lang['common_occupation_name_update']=	'You have updated Occupation Name!';
$lang['common_occupation_name_added'] =	'You have added Occupation Name!';
$lang['common_occupation_name_error'] =	'Error to add/update Occupation Name!';
$lang['common_occupation_select']     =	'Select Occupation Name';
$lang['common_description']    		  =	'Description';
$lang['common_clarification_description_required']  = 'Please Enter Clarification Description';
$lang['common_clarification_added_successful']      = 'You have successfully added Clarification Description';
$lang['common_clarification_updated_successful']    = 'You have successfully updated Clarification Description';
$lang['common_clarification_added_failed'] 		    = 'Error Added Clarification Description, Please try again!';
$lang['common_clarification']	      = 'Clarification';
$lang['common_enter_receiept_no'] 	  = 'Enter Receipt No';
$lang['common_receiept_no'] 	      = 'Receipt No';
$lang['common_receiept_date'] 	      = 'Receipt Date';
$lang['common_receiept_no_required']  = 'Receipt No is a required field';
$lang['common_select_hospital_department']    = 'Select Hospital Department';
$lang['common_hospital_department_required']  = 'Hospital Department is a required field';
$lang['common_receiept_date_required'] 		  = 'Receipt Date is a required field';
$lang['common_notification_status_successful']= 'You have successfully change stock notification status'; 
$lang['common_config_stock_notification']	  = 'Configuration Stock Notification';
$lang['common_open']		    	  =   'Open'; 
$lang['common_close']		    	  =   'Close'; 
$lang['common_rejected']		   	  =   'Rejected'; 
$lang['common_change_status']		  =   'Change Status'; 
$lang['common_select_status']		  =   'Select Status'; 
$lang['common_select_reason']		  =   'Select reason'; 
$lang['common_stock_notification']	  =   'Stock Notification';
$lang['common_manage_items']		  =   'Manage Items'; 


$lang['common_name_of_patient']	 	 =   'Name of Patient';
$lang['common_sex_type']	 		 =   'Sex M/F';
$lang['common_membership_no']	 	 =   'Membership No';
$lang['common_illness_type_code']	 =   'Type of illness (Code)';
$lang['common_date_of_attendance']	 =   'Date of Attendance';
$lang['common_type'] 				 = 	'Type'; 
$lang['common_codes'] 				 = 	'Codes'; 
$lang['common_costs'] 				 = 	'Costs'; 
$lang['common_type_generic'] 		 = 	'Type (Generic)'; 
$lang['common_quantity_drugs'] 		 = 	'Drugs Quantity'; 
$lang['common_admission_date'] 		 = 	'Admission(Date)'; 
$lang['common_discharged_on'] 		 = 	'Discharged on'; 
$lang['common_no_of_days'] 		 	 = 	'No of Days'; 
//$lang['common_total_costs'] 		 = 	'Total Costs'; 
$lang['common_qualifications'] 		 = 	'Qualifications'; 
$lang['common_signature'] 		 	 = 	'Signature';
$lang['common_tel_phone'] 		 	 = 	'Tel. No';
$lang['common_official_stamp'] 		 = 	'Official Stamp';
$lang['common_reference_code_no']  	 = 	'Reference Code No';
$lang['common_serial_no']  			 = 	'Serial No';
$lang['common_total_days'] 		 	 = 	'Total Days'; 
$lang['common_sub_total'] 		 	 = 	'Sub Total'; 
$lang['common_total'] 		 	     = 	'Total'; 
$lang['common_total_cost'] 		 	 = 	'Total Costs'; 
$lang['common_total_amount'] 		 = 	'Total Amount'; 
$lang['common_total_client'] 		 = 	'Total Client Number'; 
$lang['common_reference_code_no_exist']= 'Your selected Reference Code No is already exists, Please choose another one!'; 

//**********************NHIF PATIENT FORM***************************//
$lang['common_nhif_patient_report'] = 	'NHIF Patient Reports'; 
$lang['common_nhif_form_refernce_title_name'] = 'Form NHIF 2A& B </br>Regulation 18 (1)';
$lang['common_nhif_patient_form_name']  =  'NHIF Patient Confidential Health profiver Claim Form'; 
$lang['common_nhif_patient_form_title'] =  'CONFIDENTIAL<br>THE NHIF - HEALTH PROVIDER IN//OUT PATIENT CLAIM FORM'; 
$lang['common_nhif_patient_form_title2']=  'CONFIDENTIAL THE NHIF - HEALTH PROVIDER IN//OUT PATIENT CLAIM FORM'; 

$lang['common_nhif_form_hospital_name'] =   'Name of <b>Hospital/Health</b> CentrelDisp'; 
$lang['common_nhif_form_file_accreditation_no'] =   'NFILE Accreditation No';
$lang['common_registration_fees'] 		=   'Registration Fees';
$lang['common_particulars_a'] 			=   'A : PARTICULARS';
$lang['common_particulars_b'] 			=   'B : PARTICULARS';
$lang['common_invastigation']  			=   'INVESTIGATIONS';
$lang['common_medicine_category']  		= 	'MEDICINE/DRUGS';
$lang['common_in_patient']  			= 	'IN- PATIENT';
$lang['common_surgery']  				= 	'SURGERY';
$lang['common_name_attending_clinician']= 	'C : Name of attending Clinician';
$lang['common_patient_certification']   = 	'D : Patient Certification'; 
$lang['common_employer_name_address']   = 	"Employer's Name and Address"; 
$lang['common_nhif_patient_certify_received_service_name']   = 	'I certify that I received the above named services. Name'; 
$lang['common_nhif_patient_description_Out_in_patient_management']   = 	'E: Description of Out/In-patient Management/any other additional information (a separate sheet of paper can be used):'; 
$lang['common_claimant_certification']  = 	'<b> F : Claimant Certification </b> I certify that I provided the above services. Name'; 
$lang['common_fill_triplicate_submit_original_form']   = 	'NB: Fill in Triplicate and please submit the original form on monthly basis, and the claim be attached with Monthly Report'; 
$lang['common_any_falsified_information_subject']      = 	'Any falsified information may subject you to prosecution in accordance with NHIF Act No. 8 of 1999.'; 
$lang['common_nhif_report_builder']		= 'NHIF REPORT BUILDER';
$lang['common_nhif_report_not_found']   = 'Sorry! NHIF Report not found'; 
$lang['common_nhif_report_creted_successfull']   = 'NHIF Report was created successfully, Please check in NHIF Report Direcory!!'; 
$lang['common_nhif_report_creted_fail'] = 'Sorry! NHIF Report was not created!!'; 
$lang['common_view_nhif_report'] 		= 'View NHIF Report'; 
$lang['common_view_report'] 		    = 'View Report'; 
$lang['common_selection_list']   		= 'Selection List';
$lang['common_individual_patient']   	= 'Individual Patient';
$lang['common_all_patient']   			= 'All patient';
$lang['common_insurance_company']   	= 'Insurance Company';
$lang['common_company_name']   			= 'Company Name';
$lang['common_company_type']   			= 'Company Type';
$lang['common_company_logo']   			= 'Company Logo';
$lang['common_company_website']   		= 'Website';
$lang['common_update_insurance_company']= 'Update Insurance Company';
$lang['common_department']				= 'Department';
$lang['common_enter_card_no']			= 'Enter Your Card No';
$lang['common_health_card_no']			= 'Health Card No';
$lang['common_health_card_no_required'] = 'Health Card No is a required';
$lang['common_health_card_no_minlength']= 'Health Card No must be 19 characters';
$lang['common_health_card_no_invalid']  = 'Health Card No is not a valid card';
$lang['common_health_card_no_already_exist'] = 'Health Card is already used by another, Please chose another!';
$lang['common_health_card_valid'] 		= 'Valid Health Card!';
$lang['common_transactions'] 			= 'Transactions';
$lang['common_cashiers_name'] 			= 'Cashiers Name';
$lang['common_computer_ip_address']   	= 'Computer IP Address';
$lang['common_cashiers_computer_ip']   	= 'Cashiers Computer IP';
$lang['common_transactions_total']		= 'Total Transactions'; 
$lang['common_transactions_amount']		= 'Total Transactions Amount'; 

$lang['common_transactions_view_details']		= 'View Details';  
$lang['common_transactions_refund_amount']		= 'Total Refund Amount';
$lang['common_transactions_status']				= 'Transaction Status'; 
$lang['common_transactions_status_not_complete']= 'Transaction Not Complete or Failed'; 
$lang['common_transactions_status_complete']	= 'Transaction Complete'; 
$lang['common_transactions_status_refund']	    = 'Transaction Refund Money'; 
$lang['common_transactions_refund_date']	    = 'Transaction Refund Date Time';
$lang['common_transactions_refund_reason']		= 'Refund Transaction Reason';
$lang['common_transactions_refund_employee_name']= 'Transaction Refund Employee By';

?>