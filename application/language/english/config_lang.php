<?php
$lang['config_info']						=	'Hospital Configuration Information';
$lang['config_company']						=	'Facility Name';
$lang['config_address']						=	'Facility Location Details';
$lang['config_phone']						=	'Facility Contacts';
$lang['config_website']						=	'Website';  
$lang['config_fax']							=	'Fax';
$lang['config_default_tax_rate']			=	'Default Tax Rate %';
$lang['config_default_tax_rate_1']			=	'Tax 1 Rate';
$lang['config_default_tax_rate_2']			=	'Tax 2 Rate';
$lang['config_company_required']			=	'Facility name is a required field';
$lang['config_address_required']			=	'Facility Location Details is a required field';
$lang['config_phone_required']				=	'Facility Contacts is a required field';
$lang['config_default_tax_rate_required']	=	'The default tax rate is a required field';
$lang['config_default_tax_rate_number']		=	'The default tax rate must be a number';
$lang['config_company_website_url_required']=	'Facility website is a required field';
$lang['config_company_website_url']			=	'Facility website is not a valid URL (http://...)';
$lang['config_saved_successfully']			=	'Configuration saved successfully';
$lang['config_saved_unsuccessfully']		=	'Configuration could not be saved as it is not allowed in demo mode';
$lang['config_return_policy_required']		=	'Return policy is a required field';
$lang['config_print_after_sale']			=	'Print receipt after sale';
$lang['config_language'] 					= 	'Language';$lang['config_timezone'] 					= 	'Timezone';
$lang['config_currency_symbol'] 			= 	'Currency Symbol';
$lang['config_backup_database'] 			= 	'Backup Database';
$lang['config_restore_database'] 			= 	'Restore Database';
$lang['config_mailchimp_api_key'] 			= 	'Mailchimp API Key'; $lang['config_number_of_items_per_page'] 	= 	'Number Of Items Per Page';
$lang['config_date_format'] 				= 	'Date Format';
$lang['config_time_format'] 				= 	'Time Format';
$lang['config_company_logo'] 				= 	'Hospital Logo';
$lang['config_delete_logo'] 				= 	'Delete Logo';
$lang['config_track_cash'] 					= 	'Track Cash In Register';
$lang['config_optimize_database'] 			= 	'Optimize Database';
$lang['config_database_optimize_successfully'] = 'Optimized Database Successfully';
$lang['config_payment_types'] 				= 	'Payment Types';
$lang['config_speed_up_search_queries'] 	= 	'Speed Up Search Queries?';
$lang['select_sql_file'] 					= 	'select .sql file';
$lang['restore_heading'] 					= 	'This allows you to restore your database';
$lang['type_file'] 							=	'select .sql file from your computer';
$lang['restore'] 							= 	'restore';
$lang['required_sql_file'] 					= 	'No sql file is selected';
$lang['restore_db_success'] 				= 	'DataBase is restored successfully';
$lang['db_first_alert'] 					= 	'Are you sure of restoring the database?';
$lang['db_second_alert'] 					= 	'Present data will be lost , continue?';
$lang['password_error'] 					= 	'Password incorrect';
$lang['password_required'] 					= 	'Password field cannot be blank';
$lang['restore_database_title'] 			= 	'Restore Database';$lang['config_power_options'] 				= 	'Configuration Power Options';$lang['config_member_authorization_form']	= 	'Member Authorization Login Form';$lang['config_nhif_registration_fee']   	= 	'NHIF Registration Fees';$lang['config_notice_board_limit']   		= 	'How Many notice Board To Display';$lang['config_notice_board_email_confirmation']   = 'Send Confirmation Email of Notice Board';$lang['config_select_acility_ownership']		  =	'Select Facility Owner';$lang['config_facility_id']   				= 	'Facility ID';$lang['config_cashiers_computer_ip']   		= 	'Cashiers Computer IP';$lang['config_server_ip']   				= 	'Sever IP';  
$lang['config_computer_ip']   				= 	'Computer IP';
$lang['config_medical_officer_name']   		= 	'Medical Officer In charge';$lang['config_facility_contacts']   		= 	'Facility Contacts';$lang['config_facility_ownership']   		= 	'Facility Ownership';$lang['config_facility_bank_details']   	= 	'Facility Bank Branch';$lang['config_facility_bank_account_no']   	= 	'Facility Bank Account No';$lang['config_facility_bank_name']   		= 	'Facility Bank name';$lang['config_facility_bank_bin_no']   	    = 	'Facility Bank BIN No';
$lang['config_facility_id_required']				=	'Facility ID is a required field';$lang['config_medical_officer_name_required']		=	'Medical Officer In charge is a required field';$lang['config_facility_ownership_required']			=	'Facility Ownership is a required field';$lang['config_facility_bank_details_required']		=	'Facility Bank Details is a required field';$lang['config_facility_bank_account_no_required']	=	'Facility Bank Account No is a required field';
$lang['config_facility_bank_bank_bin_no_required']	=	'Facility Bank BIN No is a required field';
$lang['config_facility_bank_name_required']			=	'Facility Bank name is a required field';
$lang['config_server_ip_required']					=	'Sever IP Address is a required field';$lang['config_serevr_ipaddress_invalid']   			= 	'Sorry! You have entered an invalid Sever IP Address';
$lang['config_cashiers_ipaddress_invalid']   		= 	'Sorry! You have entered an invalid Cashiers Computer IP Address';
$lang['config_ipaddress_invalid']   				= 	'You have entered an invalid IP address!';

?>