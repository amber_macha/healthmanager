<?php
//*********Eye Clinics Module*********************//$lang['eyeclinic_name'] 					= 'Patient Eye Clinic';  
$lang['eyeclinic_new'] 			 	  		= 'New Eye Clinic';
$lang['eyeclinic_category']    	 	  		= 'Eye Clinic Service';
$lang['eyeclinic_commision']    	 	  	= 'Commision';
$lang['eyeclinic_list'] 	  	  	  	  	= 'Eye Clinic  List';
$lang['eyeclinic_header']  			  		= 'Eye Clinic  information';
$lang['select_uss_xrays']  			  		= 'Select Eye Clinic Service';
$lang['eyeclinic_lab']        		 	 	= 'Eye Clinic information';
$lang['eyeclinic_error_adding']				= 'Error adding Eye Clinic';
$lang['eyeclinic_worning_adding'] 	  		= 'Eye Clinic already added';
$lang['eyeclinic_confirm_delete']	  		= 'Are you sure you want to delete the selected Eye Clinic?';
$lang['eyeclinic_none_selected']	 		= 'You have not selected any eye clinic  report to delete';
$lang['eyeclinic_successful_deleted'] 		= 'You have successfully delete ';
$lang['eyeclinic_cannot_be_deleted']  		= 'Could not deleted selected eye clinic report';
$lang['eyeclinic_one_or_multiple']    		= 'eye clinic report(s)';
$lang['eyeclinic_report_successful_added']  = 'You have successfully added Eye Clinic report';
$lang['eyeclinic_services_successful_added']   		= 'You have successfully added Lab Services';
$lang['eyeclinic_services_error_added']        		= 'Error adding Lab Services';
$lang['eyeclinic_services_already_exists']       	= 'Selected Lab Services already exists';
$lang['eyeclinic_services_confirm_delete']     		= 'Are you sure you want to delete the selected Lab Service?';
$lang['eyeclinic_services_empty_delete']       		= 'You have not selected any Lab Service to delete!';
$lang['eyeclinic_services_successful_deleted'] 		= 'You have successfully deleted Lab Services';
$lang['eyeclinic_services_error_deleted']      		= 'Error Delete Lab Services';
$lang['eyeclinic_services_empty_change_status']     = 'You have not selected any Lab Service to change status!';
$lang['eyeclinic_services_change_status']      		= 'Are you sure you want to change status?';
$lang['eyeclinic_services_successful_updated'] 		= 'You have successfully updated Lab Services';
$lang['eyeclinic_services_error_updated']  			= 'Error updating Lab Services';
$lang['eyeclinic_deactivate_not_added']    			= 'Deactivate Lab Services can not add for eye clinic  report';
$lang['eyeclinic_services_empty_lab_services_to_added']= 'You have not selected any lab Services for adding eye clinic  report!';
$lang['eyeclinic_laboratory_error_adding']			   = 'Error adding lab Services for eye clinic  report!';
$lang['eyeclinic_laboratory_successful_added']		= 'You have successfully adding lab Services for eye clinic report!';
$lang['eyeclinic_full_path_to_excel_required'] 		= 'Full path to excel file required';
$lang['eyeclinic_import_successfull'] 		  		= 'Import Eye Clinic successfull';
$lang['eyeclinic_laboratory_report_post']     		= 'Post Eye Clinic  Report';
$lang['eyeclinic_laboratory_report_title']    		= 'Eye Clinic  report';
$lang['eyeclinic_view_lab_report']             		= 'View Eye Clinic  Report V/R';
$lang['eyeclinic_report_successful_added']	    	= 'You have successfully added eye clinic  report';
$lang['eyeclinic_report_error_added']          		= 'Error adding eye clinic  report';
$lang['eyeclinic_lab_services_import_successfull']  = 'Import Eye Clinic Lab Services successfull';
$lang['eyeclinic_lab_services_export_successfull']  = 'Export Eye Clinic lab Services Successfull!';
$lang['eyeclinic_view_patient_result_vr']    	    = 'You have view Eye Clinic Result V/R';

?>