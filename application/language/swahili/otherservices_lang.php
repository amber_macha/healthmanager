<?php
//*********Other Servicess Module*********************//$lang['otherservice_name'] 				  				= 'Patient Other Services ';  
$lang['otherservice_new'] 			 	  				= 'New Other Services ';
$lang['otherservice_category']    	 	  				= 'Other Services';
$lang['otherservice_commision']    	 	  				= 'Commision';
$lang['otherservice_list'] 	  	  	  	  				= 'Other Services List';
$lang['otherservice_header']  			  				= 'Other Services information';
$lang['select_uss_xrays']  			  					= 'Select Other Services';
$lang['otherservice_lab']        		 				= 'Other Services information';
$lang['otherservice_error_adding']						= 'Error adding other services';
$lang['otherservice_worning_adding'] 	  				= 'Other Services already added';
$lang['otherservice_confirm_delete']	  				= 'Are you sure you want to delete the selected other services report?';
$lang['otherservice_none_selected']	 					= 'You have not selected any other services report to delete!';
$lang['otherservice_successful_deleted'] 				= 'You have successfully delete ';
$lang['otherservice_cannot_be_deleted']  				= 'Could not deleted selected other services report';
$lang['otherservice_one_or_multiple']    				= 'other services report(s)';
$lang['otherservice_report_successful_added'] 			= 'You have successfully added Other Services report';
$lang['otherservice_services_successful_added']   		= 'You have successfully added Other Lab Services';
$lang['otherservice_services_error_added']        		= 'Error adding Lab Services';
$lang['otherservice_services_already_exists']       	= 'Selected Lab Services already exists';
$lang['otherservice_services_confirm_delete']     		= 'Are you sure you want to delete the selected Lab Service?';
$lang['otherservice_services_empty_delete']       		= 'You have not selected any Lab Service to delete!';
$lang['otherservice_services_successful_deleted'] 		= 'You have successfully deleted Lab Services';
$lang['otherservice_services_error_deleted']      		= 'Error Delete Lab Services';
$lang['otherservice_services_empty_change_status']      = 'You have not selected any Lab Service to change status!';
$lang['otherservice_services_change_status']      		= 'Are you sure you want to change status?';
$lang['otherservice_services_successful_updated'] 		= 'You have successfully updated Lab Services';
$lang['otherservice_services_error_updated']  			= 'Error updating Lab Services';
$lang['otherservice_deactivate_not_added']    			= 'Deactivate Lab Services can not add for other services report';
$lang['otherservice_services_empty_lab_services_to_added']	= 'You have not selected any Lab Services for adding other services report!';
$lang['otherservice_laboratory_error_adding']				= 'Error adding lab Services for other services report!';
$lang['otherservice_laboratory_successful_added']			= 'You have successfully adding lab Services for other services report!';
$lang['otherservice_full_path_to_excel_required'] 			= 'Full path to excel file required';
$lang['otherservice_import_successfull'] 		  			= 'Import Other Services successfull';
$lang['otherservice_laboratory_report_post']     			= 'Post Other Services Report';
$lang['otherservice_laboratory_report_title']    			= 'Other Services Report'; 
$lang['otherservice_view_lab_report']             			= 'View Other Services Report V/R';
$lang['otherservice_report_successful_added']	    		= 'You have successfully added other services report';
$lang['otherservice_report_error_added']          			= 'Error adding other services report';
$lang['otherservice_lab_services_import_successfull'] 		= 'Import Other Services Lab Services successfull';
$lang['otherservice_lab_services_export_successfull'] 		= 'Export Other Services Lab Services successfull';
$lang['otherservice_lab_services_view']    	 			    = 'You have view Other Lab Services';
$lang['otherservice_lab_services_view_lab_report']          = 'You have view Other Services Lab Report V/R';

?>