<?php
$lang['history_new']='New Comment';
$lang['history_comment']='Comment';
$lang['history_update']='Update Comment';
$lang['history_successful_deleted']='You have successfully deleted';
$lang['history_one_or_multiple']='Comment(s)';
$lang['history_cannot_be_deleted']='Could not deleted selected comments, one or more of the selected comments has sales.';
$lang['history_basic_information']='Comment Information';
$lang['history_account_number']='Account #';$lang['history_taxable']='Grade II & I';
$lang['history_most_imported_some_failed'] = 'Most comments imported. But some were not, here is list of their CODE';
$lang['history_import_successfull'] = 'Import comments successfull';
$lang['history_mass_import_from_excel'] = 'Mass import data from excel sheet';
$lang['history_download_excel_import_template'] = 'Download Import Excel Template (CSV)';
$lang['history_import'] = 'Import';
$lang['history_full_path_to_excel_required'] = 'Full path to excel file required';
$lang['customers_history_import_from_excel'] = 'Import comments from Excel';
$lang['history_duplicate_account_id'] = 'Your import has failed due to duplicate comments id or invalid data, please check your .csv file and try again';$lang['history_cleanup_old_customers'] = 'Cleanup old comments';
$lang['history_cleanup_sucessful'] = 'comments cleaned successfuly';
$lang['history_confirm_cleanup'] = 'Are you sure you want to clean ALL deleted comments? (This will remove account numbers from deleted comments so they can be reused)';$lang['history_employee_name']='Employee Name';
$lang['history_patient_waiting_counter']		=	'Patient Waiting Counter'; 
$lang['history_patient_waiting_counter_list']	=	'List of Waiting Patient Appointment'; 
$lang['history_patient_waiting_list']			=	'List of Patient Appointment'; 
$lang['history_patient_waiting_cancel']			=	'Cancel'; 
$lang['history_patient_waiting_complete']		=	'Complete'; 
$lang['history_patient_waiting_confirm_for_cancel']		=	'Are you sure you want to Cancel selected Patient Appointment?'; 
$lang['history_patient_waiting_confirm_for_complete']	=	'Are you sure you want to Complete selected Patient Appointment?'; 
$lang['history_patient_waiting_update_success']			=	'You have successfully updated selected Patient Appointment!'; 
$lang['history_patient_waiting_update_error']			=	'Error updation for selected Patient Appointment!';  

//*********History Module*********************//$lang['items_select_country']   = 'Select Country';$lang['patient_history'] 		= 'Patient History';$lang['history_new'] 			= 'Patient Comment';$lang['patient_sex']     					= 'Patient Sex';$lang['patient_account']					= 'Patient Account No';$lang['occupation']      					= 'Occupation';$lang['patient_name']    					= 'Patient Name';$lang['doctor_comment']  					= 'Clinical Note';
$lang['doctor_comment_filter_by']  			= 'Clinical Note Filter By';$lang['invastigation']  					= 'Lab Investigation';$lang['medicine']        					= 'Medicine';
$lang['history_drugs']        				= 'Drugs';$lang['uss_xray']        					= 'USS /X-ray';$lang['employee_name']  					= 'Employee Name';$lang['date']            					= 'Date';//$lang['comment']         					= 'Clinical Note';
$lang['history_clinical_note']      		= 'Clinical Note';
$lang['comment_required']					= 'Clinical Note are required';$lang['history_dental']  					= 'Dental';$lang['history_eye_clinic']        			= 'Eye Clinic';$lang['history_specialist']        			= 'Specialist';$lang['history_other_services']        		= 'Other Services';$lang['history_successful_clinical_adding']	 	= 'You have successfully added clinical Note';
$lang['history_error_adding_updating_clinical'] = 'Error adding clinical Note';
$lang['history_error_adding_adding']			= 'Error adding clinical Note';
$lang['history_successful_adding']				= 'You have successfully added';$lang['history_confirm_delete']				= 'Are you sure you want to delete the selected clinical Note?';$lang['history_none_selected']				= 'You have not selected any clinical Note to delete';$lang['comment_successful_deleted'] 		= 'You have successfully delete comments';$lang['comment_cannot_be_deleted']  		= 'Could not deleted selected comments';$lang['comment_one_or_multiple']    		= 'comments(s)';
$lang['history_main_complains']         	= 'Main Complains';
$lang['history_patient_comment']         	= 'Patient comment';
$lang['history_add_clinical_daignosis']     = 'add Clinical Diagnosis';
$lang['history_clinical_daignosis']    		= 'Clinical Diagnosis';
$lang['list_clinical_diseases'] 			= 'List Clinical Diagnoses';
$lang['history_patient_note']   			= 'Patient Note';
$lang['history_historical_present_illness'] = 'Historical of Presenting illness';
$lang['history_past_medical_history'] 		= 'Past medical History';
$lang['history_use_past_medical_history']   = 'Use The Old One';
$lang['history_blood_transfussion']  	    = 'Blood Transfussion';
$lang['history_main_complains_required']    = 'Main Complain is a required';
$lang['history_historical_present_illness_required']    = 'Historical of Presenting illness is a required';
$lang['history_admission_status_required']  = 'Admission Status is a required';
$lang['blood_transfussion_required']  		= 'Blood Transfussion is a required';
$lang['surgery_status_required']  			= 'Surgery is a required';
$lang['history_allegis_food_drugs_required']= 'Allegis Food / Drugs is a required';

//*********PHYSICAL EXAMINATION under histroy Modle*********************//
$lang['history_physical_examination']   				= 'Physical Examination';
$lang['history_system_examination']   					= 'System Examination';
$lang['history_physical_examination_form_title']  		= 'Physical Examination Form Information';
$lang['history_general_appearance']   					= 'General Appearance';
$lang['history_mental_status']   						= 'Mental status';
$lang['history_breasts_stage']   						= 'Tanner Breasts stage';
$lang['history_pubic_hairstage']   						= 'Pubic hair stage';
$lang['history_genitalia_normal']   					= 'Genitalia Normal';
$lang['history_testes_normal_right']   				    = 'Testes Normal : Right';
$lang['history_testes_normal_left']   				    = 'Testes Normal : Left';
$lang['history_eye_pupils_perrl']   				    = 'Eyes : Pupils PERRL';
$lang['history_eye_lenses_right']   				    = 'Eye Lenses Clear : Right';
$lang['history_eye_lenses_left']   				   	    = 'Eye Lenses Clear : left';
$lang['history_thyroid_right_lobe']   				    = 'Thyroid Right Lobe';
$lang['history_thyroid_left_lobe']   				   	= 'Thyroid Left Lobe';
$lang['history_lungs']   				   				= 'Lungs';
$lang['history_clear_to_auscultation']   				= 'Clear to Auscultation';
$lang['history_heart']   								= 'Heart';
$lang['history_heart_regular']   						= 'Regular';
$lang['history_heart_murmur']   						= 'Murmur';
$lang['history_abdomen']   								= 'ABDOMEN'; 
$lang['history_liver_enlarged']   						= 'Liver Enlarged';
$lang['history_spleen_enlarged']   						= 'Spleen Enlarged';
$lang['history_tenderness']   							= 'Tenderness';
$lang['history_ascites']   								= 'Ascites';
$lang['history_neurology']   							= 'NEUROLOGY';
$lang['history_tremor']   								= 'Tremor';
$lang['history_reflexes_biceps']   						= 'Reflexes:  Biceps';
$lang['history_reflexes_knees']   						= 'Reflexes:  Knees';
$lang['history_reflexes_ankles']   						= 'Reflexes:  Ankles';
$lang['history_skin']   								= 'SKIN';
$lang['history_acne']   								= 'Acne';
$lang['history_scoliosis']   							= 'Scoliosis';
$lang['history_striae']   								= 'Striae';
$lang['history_skin_colour']   							= "Colour (Pink/Purplish/Pale)";
$lang['history_skin_colour_pink']   					= 'Pink';
$lang['history_skin_colour_purplish']   				= 'Purplish';
$lang['history_skin_colour_pale']   					= 'Pale'; 
$lang['history_injection_sites']   						= 'Injection sites'; 
$lang['history_right_foot']   							= 'Right Foot'; 
$lang['history_left_foot']   							= 'Left Foot'; 

$lang['history_dorsalis_pedis_pulse']   				= 'Dorsalis Pedis Pulse'; 
$lang['history_tibialis_posterior_pulse']   			= 'Tibialis Posterior Pulse'; 
$lang['history_ulcers']   								= 'Ulcers'; 
$lang['history_signs_of_infection']   					= 'Signs of infection'; 
$lang['history_toenail_abnormalities']   				= 'Toe nail abnormalities'; 
$lang['history_monofilament_test']   					= 'Monofilament Test'; 
$lang['history_describe_deformities_foot']   			= 'Describe other deformities observed in the foot'; 
$lang['history_normal']   								= 'Normal';
$lang['history_abnormal']   							= 'Abnormal'; 


//*********invstigation Module*********************//$lang['lab_technical_name'] = 'Lab Technician';$lang['investigation_new'] = 'New Lab Investigation';$lang['lab_investigation']  = 'Lab Investigation';$lang['lab_investigation_category']  = 'Lab Service';$lang['lab_investigation_commision']  = 'Commision';$lang['lab_investigation_list']  = 'Lab Investigation List';$lang['lab_investigation_header']  = 'Lab investigation information';$lang['select_lab_categort']  = 'Select Item Category';$lang['investigation']        = 'Investigation';$lang['investigation_error_adding'] = 'Error adding lab investigation';$lang['investigation_worning_adding'] = 'Lab investigation already added';$lang['investigation_confirm_delete']='Are you sure you want to delete the selected lab investigation?';
$lang['investigation_successful_added'] = 'You have successfully added lab investigation';$lang['investigation_none_selected']='You have not selected any lab investigation to delete';$lang['investigation_successful_deleted'] = 'You have successfully delete investigation';$lang['investigation_cannot_be_deleted']  = 'Could not deleted selected investigation';$lang['investigation_one_or_multiple']    = 'investigation(s)';$lang['add_report'] = 'Add';$lang['edit_report'] = 'Edit';$lang['add_post_report']     = 'P/R';$lang['add_view_report']     = 'V/R';$lang['add_view_report_form']     = 'View Result V/R';$lang['search_lab_services'] = 'Search Lab Services';$lang['post_lab_investigation_report'] = 'Post Lab Investigation Reports';$lang['postinvestigation_result'] = 'Lab Investigation Result';$lang['view_lab_investigation_report'] = 'View Result V/R';$lang['investigation_result'] = 'View Lab Investigation Result';$lang['add_report_form'] = 'Post Results P/V';$lang['post_investigation_report'] = 'Add Lab Investigation Report';$lang['investigation_report_successful_added'] = 'You have successfully added investigation P/R report';$lang['investigation_report_error_added'] = 'Error adding investigation report';$lang['lab_investigation_admin_services'] = 'Lab Service Admin';$lang['add_lab_servics'] = 'Add';$lang['active_deactive_lab_servics'] = 'Active/DeActive';$lang['export_lab_services'] = 'Excel Export Lab Services';$lang['import_lab_services'] = 'Excel Import Lab Services';$lang['close_div'] = 'Cancel';$lang['lab_services_successful_added'] = 'You have successfully added Lab Services';$lang['lab_services_error_added'] = 'Error adding Lab Services';$lang['lab_services_exists_added'] = 'Lab Services already exists';$lang['lab_services_confirm_delete']='Are you sure you want to delete the selected lab services?';$lang['lab_services_empty_delete']='You have not selected any lab services!';$lang['lab_services_successful_deleted'] = 'You have successfully Lab Services Deleted';
$lang['lab_services_error_deleted'] = 'Error Delete Lab Services';$lang['lab_services_change_status']='Are you sure you want to change status?';$lang['lab_services_successful_updated'] = 'You have successfully updated Lab Services';$lang['lab_services_error_updated'] = 'Error updating Lab Services';$lang['lab_services_show_status'] = 'Lab Services is Deactivate';
$lang['lab_services_empty_active_deactivate']='You have not selected any lab services!';
$lang['lab_services_not_selected'] = 'Deactivate Lab Services can not add for Lab Investigation ';$lang['lab_services_full_path_to_excel_required'] = 'Full path to excel file required';$lang['labservices_import_successfull'] = 'Import Lab Services Successfull!';$lang['labservices_export_successfull'] 			= 'Export Lab Services Successfull!';$lang['labservices_clarification_description_form'] = 'Lab Investigation Clarification Description';


//*********Medicine Module*********************//
$lang['medicine_drugs_profile']  		= 'Drugs Profile';$lang['medicine_new'] 					= 'New Medicine/Drugs';$lang['medicine']  						= 'Medicine';
$lang['medicine_drug_name']  			= 'Drug Name';$lang['medicine_category']  			= 'Medicine/Drugs';
$lang['medicine_medicine_drugs_list']   = 'Medicine/Drugs List';$lang['medicine_list']  				= 'List of Drugs';$lang['medicine_header']  				= 'Medicine/Drugs Information';$lang['medicine_error_adding'] 			= 'Error adding Medicine/Drugs';$lang['medicine_worning_adding'] 		= 'Medicine/Drugs already added';$lang['medicine_exists_added'] 			= 'Medicine/Drugs already exists';$lang['medicine_successful_added'] 		= 'You have successfully added Medicine/Drugs';$lang['medicine_confirm_delete']		= 'Are you sure you want to delete the selected Medicine/Drugs ?';$lang['medicine_none_selected']			= 'You have not selected any medicine/drugs';$lang['medicine_successful_deleted'] 	= 'You have successfully delete medicine/drugs';$lang['lmedicine_error_deleted'] 		= 'Error Delete Medicine/Drugs';$lang['medicine_cannot_be_deleted']  	= 'Could not deleted selected medicine';$lang['medicine_one_or_multiple']    	= 'Medicine(s)';$lang['medicine_drugs_list']    	 	= 'Medicine/Drugs List';$lang['medicine_quantity']    		 	= 'Quantity';$lang['medicine_form_header']    	 	= 'Medicine/Drugs Information';$lang['search_medicine_category'] 	 	= 'Search Drugs/Medicine';$lang['medicine_quantity_not_empty'] 	= 'You must enter to drugs-medicine quantity!';
$lang['medicine_not_selected'] 		 	= 'Deactivate Medicine/Drugs can not added!';$lang['select_medicine']  			 	= 'Select Medicine/Drugs';$lang['export_medicine'] 			 	= 'Excel Export Medicine';$lang['import_medicine'] 			 	= 'Excel Import Medicine';$lang['active_deactive_medicine'] 	 	= 'Active/DeActive Medicine/Drugs';$lang['medicine_servics'] 			 	= 'Add';$lang['medicine_change_status']		 	= 'You have not selected any Medicine/Drugs!';$lang['medicine_successful_updated'] 	= 'You have successfully updated Medicine/Drugs';$lang['medicine_error_updated'] 	 	= 'Error updating Medicine/Drugs';$lang['medicine_import_successfull'] 	= 'Import Medicine/Drugs successfull';$lang['medicine_empty_delete']	     	= 'You have not selected any Medicine/Drugs to delete!';
$lang['medicine_dosage_title']	     	= 'DOSAGE';
$lang['medicine_dosage_desciption']  	= 'Medicine/Drugs Dosage Description';
$lang['medicine_enter_dosage_description']   = 'Dosage Description';
$lang['medicine_dosage_desciption_required'] = 'Please Enter Medicine/Drugs Dosage Description';$lang['medicine_service_name_required']      = 'Medicine/Drugs Name must be selected';
$lang['medicine_dosage_desciption_added_failed'] = 'Error Added Dosage Description, Please try again!';
$lang['medicine_dosage_desciption_added_successful'] = 'You have successfully added Dosage Description';
$lang['medicine_dispensing'] 	     	= 'Dispensing';
$lang['medicine_dispensed'] 		 	= 'Dispensed';
$lang['medicine_not_dispensed'] 	 	= 'Not Dispensed';
$lang['medicine_print_selected'] 	 	= 'Print Selected';
$lang['medicine_print_this'] 	     	= 'Print This';
$lang['medicine_print_selected_empty']  = 'You have not selected any Medicine/Drugs';  
$lang['medicine_drug_status'] 	     	= 'Drug Status'; 
$lang['medicine_drug_management'] 	    = 'Drugs Management'; 
$lang['medicine_dosage_management']   	= 'Dosage Management';
$lang['medicine_dosage_status_ongoing'] = 'On going Dosage';
$lang['medicine_dosage_status_complete']= 'Dosage Complete';
$lang['medicine_dosage_status_cancel']  = 'Dosage Cancel';
$lang['medicine_drug_dispanse']		    = 'Drug Dispanse';
$lang['medicine_drug_cancel'] 			= 'Drug Cancel';
$lang['medicine_dispensing_option'] 	= 'Dispensing Option';
$lang['medicine_automatic_dispencing'] 	= 'Automatic Dispencing';
$lang['medicine_manually_dispencing'] 	= 'Manually Dispencing';
$lang['medicine_drug_comment'] 			= 'Drug Comment';
$lang['medicine_data_successful_added'] = 'You have successfully updated Medicine/Drugs';
$lang['medicine_data_error_adding'] 	= 'Error updated Medicine/Drugs';
$lang['medicine_required_data'] 		= 'Please Enter Required Field';
$lang['medicine_drugs_dosage_confirm_cancel']= 'Are you sure you want to cancel Drugs?';
$lang['medicine_drugs_dispanse_disabled']	 = 'Sorry Drug Dispanse is disable, Please change Dispensing Option';
$lang['medicine_import_successfull'] 	= 'Import Medicine/Drugs Successfull!';
$lang['medicine_export_successfull'] 	= 'Export Medicine/Drugs Successfull!';
$lang['medicine_dispensing_medicine_drugs_print_view']  = 'You have take a print of Medicine/Drugs Dispensing';
$lang['medicine_drugs_profile_view']    = 'You have view Drugs Profile';


//*********items Module*********************//$lang['items_private_price_required']='Selling Private Price is a required field';$lang['items_private_price_number']='Private price must be a number';$lang['items_expire_date']='Item Expire Date';$lang['items_fast_track_price_required']='Selling Fast Track Price is a required field';$lang['items_fast_track_price_number']='Private Fast Track Price must be a number';$lang['patient_type']='Patient Type';$lang['reports_expire_items']='Expire Items';$lang['reports_expire_items2']='Expire Item Report';$lang['item_expire_date']='Expire Date';$lang['items_column_expire_name']='Expire Date';//*********USS X-rays Module*********************//$lang['uss_xrays_name'] 			  = 'USS / X-ray';$lang['uss_xrays_new'] 			 	  = 'New USS / X-ray';$lang['uss_xrays'] 		 			  = 'USS / X-ray';$lang['uss_xrays_category']    	 	  = 'USS / X-ray Service';$lang['uss_xrays_commision']    	  = 'Commision';$lang['uss_xrays_list'] 	  	  	  = 'Lab USS / X-ray List';$lang['uss_xrays_header']  			  = 'Lab USS / X-ray information';$lang['select_uss_xrays']  			  = 'Select USS / X-ray Service';$lang['uss_xrays_lab']        		  = 'Lab USS / X-ray';$lang['search_uss_xrays'] 			  = 'Search USS / X-ray';$lang['uss_xrays_error_adding'] 	  = 'Error adding USS / X-ray';$lang['uss_xrays_worning_adding'] 	  = 'USS / X-ray already added';$lang['uss_xrays_confirm_delete']	  = 'Are you sure you want to delete the selected USS / X-ray?';$lang['uss_xrays_none_selected']	  = 'You have not selected any USS / X-ray to delete';$lang['uss_xrays_successful_deleted'] = 'You have successfully delete ';$lang['uss_xrays_cannot_be_deleted']  = 'Could not deleted selected USS / X-ray';$lang['uss_xrays_one_or_multiple']    = 'USS / X-ray(s)';$lang['uss_xrays_service_successful_added'] = 'You have successfully added USS / X-ray service';$lang['uss_xrays_service_error_added'] = 'Error adding USS / X-ray service';$lang['uss_xrays_admin_services'] 	  = 'Lab Service Admin';$lang['add_uss_xrays'] 				  = 'Add';$lang['active_deactive_uss_xrays']    = 'Active/DeActive USS / X-ray';$lang['export_uss_xrays']			  = 'Excel Export USS / X-ray';$lang['import_uss_xrays'] 			  = 'Excel Import USS / X-ray';$lang['uss_xrays_successful_added']   = 'You have successfully added USS / X-ray ';$lang['uss_xrays_error_added']        = 'Error adding USS / X-ray Service';$lang['uss_xrays_exists_added']       = 'Lab Services already exists';$lang['uss_xrays_confirm_delete']     = 'Are you sure you want to delete the selected USS / X-ray?';$lang['uss_xrays_empty_delete']       = 'You have not selected any USS / X-ray Service to delete!';$lang['uss_xrays_error_deleted']      = 'Error Delete Lab Services';$lang['uss_xrays_change_status']      = 'Are you sure you want to change status?';$lang['uss_xrays_successful_updated'] = 'You have successfully updated USS / X-ray';$lang['uss_xrays_error_updated']      = 'Error updating USS / X-ray';$lang['uss_xrays_show_status']        = 'USS / X-ray is Deactivate';$lang['uss_xrays_deactivate_not_added']        = 'Deactivate Lab Services can not add for USS/X-ray';$lang['uss_xrays_full_path_to_excel_required'] = 'Full path to excel file required';$lang['uss_xrays_post_lab_report']    		   = 'Post USS / X-ray Reports';$lang['uss_post_report_images'] 	           = "Upload USS Report's images";$lang['xrays_post_report_images'] 	           = "Upload X-ray Report's images";$lang['uss_xrays_view_lab_report']             = 'View USS / X-ray Reports V/R';$lang['uss_view_report_images'] 	           = "Lab USS / X-ray";$lang['xrays_view_report_images'] 	           = "USS / X-ray department";$lang['lab_radiologist_name'] 	 	           = "Radiologist";$lang['uss_xrays_post_report_images_required'] = "Select USS / X-ray Report's images";$lang['uss_xrays_post_valid_images_formate']   = "Please upload only jpg,png,gif,jpeg file";$lang['uss_xrays_report_successful_added']	   = 'You have successfully added uss/x-ray report';$lang['uss_xrays_report_error_added']          = 'Error adding uss/x-ray report';$lang['uss_xrays_customer_view_medicine_drugs']= 'You have view patient uss/x-ray';
$lang['uss_xrays_successful_deleted']          = 'You have successfully USS / X-ray Deleted';
$lang['uss_xrays_import_successfull'] 		   = 'Import USS / X-ray Successfull!';
$lang['uss_xrays_export_successfull'] 	       = 'Export USS / X-ray Successfull!';
$lang['uss_xrays_clarification_description_form'] = 'USS / X-ray Clarification Description';

// Diagnoses language $lang['diseases_services_empty']='You have not selected any Mthuha Diagnoses!';$lang['diseases_services_add']='Are you sure you want to add?';$lang['diseases_services_successful_added'] = 'You have successfully added Diagnoses';$lang['diseases_services_error_added'] = 'Error Added Diagnoses!';$lang['list_diseases'] = 'List Diagnoses';$lang['search_diseases'] = 'Search Diagnoses';$lang['diseases'] = 'Mtuha Diagnoses';$lang['add_diseases'] = 'Add Diagnoses';$lang['edit_diseases'] = 'Edit Diagnoses';$lang['diseases_name'] = 'Diagnoses Name';$lang['diseases_code'] = 'Diagnoses Code';$lang['toedit_diseases'] = 'Edit';$lang['add_new_diseases'] = 'Add New Diagnoses';$lang['validate_diseases_name'] = 'Please Enter Diagnoses Name';$lang['validate_diseases_code'] = 'Please Enter Diagnoses Code Number';$lang['newdiseases_updated_successful'] = 'You have successfully updated';$lang['newdiseases_added_successful'] = 'You have successfully added';$lang['newdiseases_added_failed'] = 'Error Added Diagnoses, Please try again!';$lang['newdiseases_added_exists'] = 'Your selected Diagnoses Name or Code Number is already exists!';$lang['diseases_services_empty_for_deletion']='You have not selected any Diagnoses!';$lang['diseases_services_confirm_deletion']='Are you sure you want to delete the selected Diagnoses';$lang['diseases_services_deleted'] = 'Diagnoses was deleted successfully!';// Diabetes language $lang['diabetes_services_empty']='You have not selected any Diabetes Care!';$lang['diabetes_services_successful_added'] = 'You have successfully added Diabetes';$lang['diabetes_services_error_added'] = 'Error Added Diabetes!';$lang['list_diabetes'] = 'List Diabetes';$lang['search_diabetes'] = 'Search Diabetes'; $lang['add_diabetes'] = 'Add Diabetes';$lang['edit_diabetes'] = 'Edit Diabetes';$lang['diabetes_name'] = 'Diabetes Name';$lang['diabetes_code'] = 'Diabetes Code';$lang['toedit_diabetes'] = 'Edit';$lang['add_new_diabetes'] = 'Add New Diabetes';$lang['validate_diabetes_name'] = 'Please Enter Diabetes Name';$lang['validate_diabetes_code'] = 'Please Enter Diabetes Code Number';$lang['newdiabetes_updated_successful'] = 'You have successfully updated';$lang['newdiabetes_added_successful'] = 'You have successfully added';$lang['newdiabetes_added_failed'] = 'Error Added Diabetes, Please try again!';$lang['newdiabetes_added_exists'] = 'Your selected Diabetes Name or Code Number is already exists!';$lang['diabetes_services_empty_for_deletion']='You have not selected any Diabetes!';$lang['diabetes_services_confirm_deletion']='Are you sure you want to delete the selected Diabetes';$lang['diabetes_services_deleted'] = 'Diabetes was deleted successfully!';
?>