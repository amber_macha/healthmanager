<?php$lang['dead_body_new']								=	'New Dead Body';
$lang['dead_body_customer']							=	'Dead Body';
$lang['dead_body_update']							=	'Update Dead Body';
$lang['dead_body_confirm_delete']					=	'Are you sure you want to delete the selected Dead Body?';
$lang['dead_body_none_selected']					=	'You have not selected any dead body to delete';$lang['dead_body_selected_item_already_exist']	    =   'Selected item is already exist!';$lang['dead_body_cleanup_sucessful']	    		=   'Dead Body cleaned successfuly!';
$lang['dead_body_error_adding_updating']			=   'Error adding/updating dead body';
$lang['dead_body_successful_adding']				=	'You have successfully added dead body';
$lang['dead_body_successful_updating']				=	'You have successfully updated dead body';
$lang['dead_body_successful_deleted']				=	'You have successfully deleted';
$lang['dead_body_one_or_multiple']					=	'Dead Body(s)';
$lang['dead_body_cannot_be_deleted']				=	'Could not deleted selected dead body, one or more of the selected dead body has sales.';
$lang['dead_body_basic_information']				=	'Dead Body Information';
$lang['dead_body_most_imported_some_failed']		= 	'Most dead body imported. But some were not, here is list of their CODE';
$lang['dead_body_import_successfull']				= 	'Import dead body successfull';
$lang['dead_body_mass_import_from_excel']  		 	= 	'Mass import data from excel sheet';
$lang['dead_body_download_excel_import_template']	= 	'Download Import Excel Template (CSV)';
$lang['dead_body_import']							= 	'Import';
$lang['dead_body_full_path_to_excel_required']  	= 	'Full path to excel file required';
$lang['dead_body_import_dead_body_from_excel']   	= 	'Import dead body from Excel'; 
$lang['dead_body_duplicate_account_id'] 		 	= 	'Your import has failed due to duplicate dead body id or invalid data, please check your .csv file and try again';$lang['dead_body_cleanup_old_customers'] 			= 	'Cleanup old dead body';
$lang['dead_body_cleanup_sucessful']				= 	'Dead Body cleaned successfuly';
$lang['dead_body_confirm_cleanup'] 					= 	'Are you sure you want to clean ALL deleted old dead? (This will remove account numbers from deleted old dead so they can be reused)';$lang['dead_body_trible'] 							= 	'Trible Name';$lang['dead_body_date'] 							=	'Date of Death';$lang['dead_body_upload_certificate'] 				= 	'Upload Certificate';$lang['dead_body_view_certificate'] 				= 	'Certificate';$lang['dead_body_download'] 						= 	'Download';//**********ADD DEAD BODY FORM*********************//$lang['dead_body_tribe_name'] 		       			=	'Trible Name'; $lang['dead_body_reason_of_death'] 					= 	'Reason Of Death';$lang['dead_body_car_no_brought_dead_body'] 		= 	'Car No that bring dead body to the Hospital';$lang['dead_body_name_of_drive_relative'] 			= 	'Name of Drive or relative';$lang['dead_body_relative_relationship'] 		    = 	'Relative Relationship'; $lang['dead_body_relative_mobile_no'] 		   		= 	'Mobile No of Relative';$lang['dead_body_name_required']					=	'The Death body name is required field';$lang['dead_body_sex_type_required']				=	'The sex type is a required field';$lang['dead_body_death_date_required']				=	'Death Date is a required field';$lang['dead_body_reason_of_death_required']			=	'Reson of death is a required field';$lang['dead_body_car_no_brought_required']			=	'Car No that bring dead body to the Hospital is a required field';$lang['dead_body_mobile_no_relative_required']		=	'Mobile No of Relative is a required field';$lang['dead_body_age_day_required']					=	'Death Body day of age is a required field';$lang['dead_body_age_month_required']				=	'Death Body month of age is a required field';$lang['dead_body_age_year_required']				=	'Death Body year of age is a required field';$lang['dead_body_diagnoses'] 						=   'Death Body Diagnoses';$lang['dead_body_diagnoses_form'] 					=   'Death Body Diagnoses Form'; $lang['dead_body_list_diagnoses'] 					=   'List of Death Body Diagnoses'; 
?>