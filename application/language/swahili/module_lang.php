<?php$lang['module_home']					=	'Home';
$lang['module_customers']				=	'Patients';$lang['module_customers_desc']			=	'Add, Update, Delete, and Search patients';
$lang['module_suppliers']				=	'Department';
$lang['module_suppliers_desc']			=	'Add, Update, Delete, and Search department';
$lang['module_employees']				=	'Employees';
$lang['module_employees_desc']			=	'Add, Update, Delete, and Search employees';
$lang['module_sales']					=	'Sales (F2)';
$lang['module_sales_desc']				=	'Process sales and returns';
$lang['module_reports']					=	'Reports';
$lang['module_reports_desc']			=	'View and generate reports';
$lang['module_items']					=	'Items';
$lang['module_items_desc']				=	'Add, Update, Delete, and Search items';
$lang['module_config']					=	'Hospital Config';
$lang['module_config_desc']				=	'Change the store\'s configuration';
$lang['module_receivings']				=	'Receivings';
$lang['module_receivings_desc']			=	'Process Purchase orders';
$lang['module_giftcards']				=	'Deposit Cards';
$lang['module_giftcards_desc']			=	'Add, Update, Delete and Search deposit cards';
$lang['module_payments']				=	'Payments';
$lang['module_item_kits']				=	'Item Kits';
$lang['module_item_kits_desc']			=	'Add, Update, Delete and Search Item Kits';
$lang['module_action_add_update']		= 	'Add, Update';
$lang['module_action_delete'] 			= 	'Delete';
$lang['module_action_search_customers'] = 	'Search customers';
$lang['module_action_search_items'] 	= 	'Search items';
$lang['module_action_search_item_kits'] = 	'Search Item Kits';
$lang['module_action_search_suppliers'] =	'Search suppliers';
$lang['module_see_cost_price'] 			= 	'See cost price';$lang['module_see_unit_price'] 			= 	'See Unit Price';$lang['module_see_private_price'] 		= 	'See Private Price';$lang['module_see_fast_track_price'] 	= 	'See Fast Track Price';$lang['module_see_nhif_price'] 			= 	'See NHIF Price';$lang['module_see_promo_price'] 		= 	'See Promo Price';$lang['module_suspended_sales'] 	    = 	'Suspended Sales';
$lang['module_action_search_employees'] = 	'Search employees';$lang['module_action_employees_chat'] = 	'Employees Chat';
$lang['module_edit_sale_price'] 		= 	'Edit Sale Price';
$lang['module_create_giftcard'] 		= 	'Create';
$lang['module_cash'] 					= 	'Cash';
$lang['module_giftcard'] 				= 	'Deposit Card';
$lang['module_credit'] 					= 	'NHIF';
$lang['module_debit'] 					= 	'CONTRACT';
$lang['module_check']				    = 	'DEPARTMENT';
//new items after 08-11-2013
$lang['module_history']					=	'Patient History';
$lang['module_history_desc']			=	'Add, Update, Delete, and Search';
$lang['module_action_search_history']   = 	'Search History';$lang['module_action_patient_profile']  = 	'Patient Profile';
$lang['module_investigation']			=	'Patient Investigation';
$lang['module_investigation_desc']		=	'Add, Update, Delete, and Search';
$lang['module_action_search_investigation'] = 'Search Investigation';$lang['module_action_update_profile'] 	= 'Update profile';$lang['module_action_P_R'] 				= 'Add P/R';$lang['module_action_V_R'] 				= 'Add V/R';
//*********Medicine Module*********************//
$lang['module_medicine']				=	'Medicine/Drugs';
$lang['module_medicine_desc']			=	'Add, Update, Delete, and Search';
$lang['module_action_search_medicine']  =   'Search Medicine/Drugs';$lang['module_action_drugs_profile']    =   'Drugs Profile';
//*********X-rays Module*********************//$lang['module_uss_xrays']				=	'Patient USS / X-ray';$lang['module_uss_xrays_desc']			=	'Add, Update, Delete, and Search';$lang['module_action_search_uss_xrays'] = 	'Search USS / X-ray';//*********Dental Module*********************//$lang['module_dental']					=	'Patient Dental';$lang['module_dental_desc']				=	'Add, Update, Delete, and Search';$lang['module_action_search_dental'] 	= 	'Search Dental';//*********Eye Clinic Module*********************//$lang['module_eyeclinic']				=	'Patient Eye Clinic';$lang['module_eyeclinic_desc']			=	'Add, Update, Delete, and Search';$lang['module_action_search_eyeclinic'] = 	'Search Eye Clinic';//*********Specilist Module*********************//$lang['module_specialist']				=	'Patient Specialist';$lang['module_specialist_desc']			=	'Add, Update, Delete, and Search';$lang['module_action_search_specialist']=	'Search Specialist';//*********Other Services Module*********************//$lang['module_otherservice']			=	'Other Services';$lang['module_otherservice_desc']		=	'Add, Update, Delete, and Search';$lang['module_action_search_otherservice'] = 'Search Other Services';//*********IPD/In-Patient Department Module*********************//$lang['reports_ipd_in_patient'] = 'IPD/In-Patient Department';$lang['module_ipd_in_patient'] = 'IPD/In-Patient Department';$lang['module_ipd_in_patient_desc'] = 'Add, Update, Delete, and Search';$lang['module_action_search_ipd_in_patient'] = 'Search IPD/In-Patient';//*********Diabetes Module*********************//
$lang['module_diabetes_care']			=	'Diabetes Care';$lang['module_customers_desc']			=	'Add, Update, Delete, and Search Diabetes Care';$lang['module_action_search_diabetes']	=	'Search Diabetes';//*********Dead Body Module*********************//$lang['module_dead_body']			    =	'Dead Body';$lang['module_dead_body_desc']			=	'Add, Update, Delete, and Search Dead Body';$lang['module_action_search_dead_body']	=	'Search Dead Body';$lang['reports_dead_body'] 				=   'Dead Body';//*********Ministry Reporting Tool*********************//$lang['module_ministry_reporting']			    =	'Ministry Reporting Tool';$lang['module_ministry_reporting_desc']			=	'Add, Update, Delete, and Search Ministry Reporting Tool';$lang['module_action_search_ministry_reporting']=	'Search Ministry Reporting Tool';//*********Laboratoty Administrator module*********************//$lang['module_laboratory']				 =	'Laboratory';$lang['module_laboratory_desc']			 =	'Add, Update, Delete, and Search Laboratory';$lang['module_action_search_laboratory'] = 	'Search Laboratory';//*********Laboratoty Administrator module*********************//$lang['module_nhif_patient_form']		 =	'NHIF Patient Form';$lang['module_nhif_patient_form_desc']	 =	'Search & View NHIF Patient Form';$lang['module_action_nhif_patient_form'] = 	'View NHIF Patient Report';//*********Laboratoty Administrator module*********************//$lang['module_rch']						 =	'RCH –Reproductive & Child Health';$lang['module_rch_desc']				 =	'Add, Update, Delete, and Search RCH';$lang['module_action_search_rch'] 		 = 	'Search RCH';//*********MD4 & MD5 Tool module*********************//$lang['module_tools']					 =	'MD4 & MD5 Tool';$lang['module_tools_desc']				 =	'Add, Update, Delete, and Search MD4 & MD5 Tool';$lang['module_action_search_tools']		 = 	'Search MD4 & MD5 Tool';
?>
