<?php
//*********Dentals Module*********************//$lang['dental_name'] 				  	= 'Patient Dental';  
$lang['dental_new'] 			 	  	= 'New Dental';
$lang['dental_category']    	 	  	= 'Dental Service';
$lang['dental_commision']    	 	  	= 'Commision';
$lang['dental_list'] 	  	  	  	  	= 'Dentist List';
$lang['dental_header']  			  	= 'Dentist information';
$lang['select_uss_xrays']  			  	= 'Select Dental Service';
$lang['dental_lab']        		 	 	= 'Dentist information';

$lang['dental_error_adding']			= 'Error adding Dental';
$lang['dental_worning_adding'] 	  		= 'Dental already added';
$lang['dental_confirm_delete']	  		= 'Are you sure you want to delete the selected Dental?';
$lang['dental_none_selected']	 		= 'You have not selected any dental report to delete';
$lang['dental_successful_deleted'] 		= 'You have successfully delete ';
$lang['dental_cannot_be_deleted']  		= 'Could not deleted selected dental report';
$lang['dental_one_or_multiple']    		= 'dental report(s)';
$lang['dental_report_successful_added'] = 'You have successfully added Dental report';
$lang['dental_services_successful_added']   		= 'You have successfully added Lab Services';
$lang['dental_services_error_added']        		= 'Error adding Lab Services';
$lang['dental_services_already_exists']       		= 'Selected Lab Services already exists';
$lang['dental_services_confirm_delete']     		= 'Are you sure you want to delete the selected Lab Service?';
$lang['dental_services_empty_delete']       		= 'You have not selected any Lab Service to delete!';
$lang['dental_services_successful_deleted'] 		= 'You have successfully deleted Lab Services';
$lang['dental_services_error_deleted']      		= 'Error Delete Lab Services';
$lang['dental_services_empty_change_status']       	= 'You have not selected any Lab Service to change status!';
$lang['dental_services_change_status']      		= 'Are you sure you want to change status?';
$lang['dental_services_successful_updated'] 		= 'You have successfully updated Lab Services';
$lang['dental_services_error_updated']  			= 'Error updating Lab Services';
$lang['dental_deactivate_not_added']    			= 'Deactivate Lab Services can not add for dental report';
$lang['dental_services_empty_lab_services_to_added']= 'You have not selected any lab Services for adding dental report!';
$lang['dental_laboratory_error_adding']				= 'Error adding lab Services for dental report!';
$lang['dental_laboratory_successful_added']			= 'You have successfully adding lab Services for dental report!';

$lang['dental_full_path_to_excel_required'] = 'Full path to excel file required';
$lang['dental_laboratory_report_post']     	= 'Post Dental Report';
$lang['dental_laboratory_report_title']    	= 'Dental report';
$lang['dental_view_lab_report']             = 'View Dental Report V/R';
$lang['dental_lab_technician_name'] 	 	= "Dental Technician";
$lang['dental_report_successful_added']	    = 'You have successfully added dental report';
$lang['dental_report_error_added']          = 'Error adding dental report';
$lang['dental_lab_services_import_successfull'] = 'Import Dental Lab Services successfull';
$lang['dental_import_successfull'] 			= 'Import Dental lab Services Successfull!';
$lang['dental_export_successfull'] 			= 'Export Dental lab Services Successfull!';

?>