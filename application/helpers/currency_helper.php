<?php
function to_currency($number)
{
	$CI =& get_instance();
	$currency_symbol = $CI->config->item('currency_symbol') ? $CI->config->item('currency_symbol') : '$';
	if($number >= 0)
	{
		return $currency_symbol.number_format($number, 2, '.', ',');
    }
    else
    {
    	return '-'.$currency_symbol.number_format(abs($number), 2, '.', ',');
    }
}


function to_currency_no_money($number)
{
	return number_format($number, 2, '.', '');
}

function to_weight($number)
{
	$CI =& get_instance();
	$mesurement_symbol = $CI->config->item('weight_symbol') ? $CI->config->item('weight_symbol') : 'Kg';
	if($number >= 0)
	{
		//return $mesurement_symbol.number_format($number, 2, '.', ',');
		return '-'.$mesurement_symbol.$number;
    }
    else
    {
    	//return '-'.$mesurement_symbol.number_format(abs($number), 2, '.', ',');
		return '-'.$mesurement_symbol.$number;
    }
}

function to_temperature($number)
{
	$CI =& get_instance();
	$mesurement_symbol = $CI->config->item('temperature_symbol') ? $CI->config->item('temperature_symbol') : '°C';
	if($number >= 0)
	{
		//return $mesurement_symbol.number_format($number, 2, '.', ',');
		return '-'.$mesurement_symbol.$number;
    }
    else
    {
    	return '-'.$mesurement_symbol.$number;
		//return '-'.$mesurement_symbol.number_format(abs($number), 2, '.', ',');
    }
}

function to_height($number)
{
	$CI =& get_instance();
	$mesurement_symbol = $CI->config->item('height_symbol') ? $CI->config->item('height_symbol') : '°C';
	if($number >= 0)
	{
		//return $mesurement_symbol.number_format($number, 2, '.', ',');
		return '-'.$mesurement_symbol.$number;
    }
    else
    {
    	//return '-'.$mesurement_symbol.number_format(abs($number), 2, '.', ',');
		return '-'.$mesurement_symbol.$number;
    }
}

?>