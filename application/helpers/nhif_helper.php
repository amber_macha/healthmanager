<?php 	
function get_nhif_manage_table_data_rows($method_type,$items,$from_date,$to_date,$controller)
{	$CI =& get_instance();
	$table_data_rows='';
	if($items->num_rows()==0)
	{
	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('items_no_items_to_display')."</div></tr></tr>";	}	
	else 
	{		
		if($method_type=='nhif_reports') { foreach($items->result() as $item){ $table_data_rows .= get_nhif_data_row_list($method_type,$item,$from_date,$to_date,$controller); } }
	}
	return $table_data_rows;
}
function get_nhif_manage_table($method_type,$items,$from_date, $to_date,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';	
	
	if($method_type=='nhif_reports')
	{	$headers = array(lang('common_first_name'),lang('common_last_name'),$CI->lang->line('documentation'),
						  $CI->lang->line('common_membership_no'), $CI->lang->line('common_sex'), $CI->lang->line('common_age'), $CI->lang->line('common_total_amount'));	
	}
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;		
		if ($count == 1) { $table.="<th class='leftmost'>$header</th>"; 	}
		elseif ($count == count($headers))	{ $table.="<th class='rightmost'>$header</th>";	}
		else {	$table.="<th>$header</th>";	}
	}

	$table.='</tr></thead><tbody>';
	$table.=get_nhif_manage_table_data_rows($method_type,$items,$from_date,$to_date,$controller);
	$table.='</tbody></table>';
	return $table;
}

function get_nhif_data_row_list($method_type,$item,$from_date,$to_date,$controller)
{  
	$CI =& get_instance();		
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$CI->load->model('savesetting'); 
	$Total_Amount 	 = $CI->savesetting->get_nhif_Total_Amount($item->person_id,$from_date,$to_date);	
	$from_date = strtotime($from_date);$to_date = strtotime($to_date);	
	$table_data_row='<tr>';
	$table_data_row.='<td width="15%" align="left">'.$item->first_name.'</td>';
	$table_data_row.='<td width="15%" align="left">'.$item->last_name.'</td>';
	$table_data_row.='<td width="15%" align="left">'.anchor($controller_name."/view/$item->id/$from_date/$to_date/width~1075", lang('documentation'),array('title'=>lang('common_view_nhif_report'),'class'=>"thickbox")).'</td>';
	$table_data_row.='<td width="20%" align="left">'.$item->membership_no.'</td>';
	$table_data_row.='<td width="10%" align="left">'.$item->sex.'</td>';
	$table_data_row.='<td width="10%" align="left">'.$item->age.'</td>';
	$table_data_row.='<td width="20%" align="left">'.to_currency($Total_Amount).'</td>';
	$table_data_row.='</tr>';
	return $table_data_row;
}

?>