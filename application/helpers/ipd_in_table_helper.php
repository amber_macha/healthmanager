<?php

/* Gets the html table to manage people.*/
function get_ipd_in_patient_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	$controller_name=strtolower(get_class($CI));
	if($controller_name!='ipd_in_patient')
	{
	$headers = array('<input type="checkbox" id="select_all" />', 
						lang('common_full_name'),
						lang('ipd_in_admit_date'),
						lang('ipd_in_admit_total_days'),
						lang('customers_account_numbers'),
					);		
	}
	else {
	$headers = array('<input type="checkbox" id="select_all" />', 
						lang('common_full_name'),
						lang('ipd_in_admit_date'),
						lang('ipd_in_admit_total_days'),
						lang('customers_account_numbers'),
						lang('ipd_in_management'),
						lang('ipd_in_paid_amount'),
						lang('ipd_in_balance_amount')
					);		
	}				
	$table.='<tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr>';
	$table.=get_ipd_in_patient_data_rows($people,$controller);
	$table.='</table>';
	return $table;
}

/* Gets the html data rows for the people.*/
function get_ipd_in_patient_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_ipd_in_data_row($person,$controller);
	}	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}	
	return $table_data_rows;
}
	
function get_ipd_in_data_row($person,$controller)
{
	$CI = & get_instance();
	$controller_name=strtolower(get_class($CI));
	//$width = $controller->get_form_width();	
	$width = '550';	
	$disbaled_row = '';  $span='';	
	$Total_Days = $CI->ipd_in_patients->getTotal_AdmitDays($person->person_id,$person->id); 

	$link = site_url('history/index/'.$person->person_id);	
	$management_url = anchor($controller_name."/manage/$person->person_id/$person->id/width~$width", lang('ipd_in_management'),array('class'=>'thickbox','title'=>lang('ipd_in_view_form_title')));
	if(isset($person->discharge_status) && $person->discharge_status==1)
	{   $disbaled_row = 'style="background-color: #FFFF00;" '; 
		$management_url = '<span class="disble_managelink">'.lang('ipd_in_management').'</span>';
	    $span ='class="disble_managelink"';
	}		 
		$GetpaymentArray = $CI->ipd_in_patients->GetTotalDeposit_Amount($person->person_id,$person->id);		
	    $table_data_row = '';
	    $table_data_row.= '<tr>';	
		$table_data_row.='<td width="5%" '.$disbaled_row.'><input type="checkbox" id="'.$person->id.'" value="'.$person->person_id.'"/></td>';
		$table_data_row.='<td width="15%" '.$disbaled_row.'> <span '.$span.'>'.ucwords($person->first_name).'&nbsp;'.ucwords($person->last_name).'</span></td>';
		$table_data_row.='<td width="15%" '.$disbaled_row.'> <span '.$span.'>'.date('d-m-Y',strtotime($person->registration_time)).'</span></td>';	
		$table_data_row.='<td width="15%" '.$disbaled_row.'> <span '.$span.'>'.$Total_Days.'</span></td>';
		$table_data_row.='<td width="15%" '.$disbaled_row.'> <span '.$span.'>'.$person->account_number.'</span></td>';
		if($controller_name=='ipd_in_patient')
		{
			$table_data_row.='<td width="15%" '.$disbaled_row.'>'.$management_url.'</td>';
			$table_data_row.='<td width="15%" '.$disbaled_row.'>'.@to_currency($GetpaymentArray['TotalDeposit']).'</td>';	
			$table_data_row.='<td width="15%" '.$disbaled_row.'>'.@to_currency($GetpaymentArray['Balance_Amount']).'</td>';
		}
		$table_data_row.='</tr>';	
	return $table_data_row;
}

?>