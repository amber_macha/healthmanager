<?php 
	/*Gets the html data rows for the items.*/
	
function get_Lab_manage_highlight_data_rows($method_type,$items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';	
	//print_r($items->person_id);die;
	if($method_type=='reagent')  { foreach($items->result() as $item){ $table_data_rows = get_Lab_reagent_data_row_list($method_type,$item,$controller);  } }
	return $table_data_rows;}
/*Gets the html data rows for the items.*/
function get_Lab_manage_table_data_rows($method_type,$items,$controller)
{	$CI =& get_instance();
	$table_data_rows='';
	if($items->num_rows()==0)
	{
	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('items_no_items_to_display')."</div></tr></tr>";	}	
	else 
	{		
		if($method_type=='reagent')  { foreach($items->result() as $item){ $table_data_rows .= get_Lab_reagent_data_row_list($method_type,$item,$controller);  } }
	}
	return $table_data_rows;
}
	 //************************** MANAGE METER TYPE RECORSD.*****************************************************//
	function get_Lab_manage_table($method_type,$items,$controller)
	{
		$CI =& get_instance();
		$table='<table class="tablesorter" id="sortable_table">';	
		
		if($method_type=='reagent')
		{	$headers = array('<input type="checkbox" id="select_all"/>',lang('common_patient_name'),lang('laboratory_specimen_name'),$CI->lang->line('laboratory_lab_department'),
							  $CI->lang->line('ipd_in_receiept_no'), $CI->lang->line('common_added_date'), $CI->lang->line('common_status'));	
		}
		
		$table.='<thead><tr>';
		$count = 0;
		foreach($headers as $header)
		{
			$count++;		
			if ($count == 1) { $table.="<th class='leftmost'>$header</th>"; 	}
			elseif ($count == count($headers))	{ $table.="<th class='rightmost'>$header</th>";	}
			else {	$table.="<th>$header</th>";	}
		}

		$table.='</tr></thead><tbody>';
		$table.=get_Lab_manage_table_data_rows($method_type,$items,$controller);
		$table.='</tbody></table>';
		return $table;
	}

	function get_Lab_reagent_data_row_list($method_type,$item,$controller)
	{  
		$CI =& get_instance();		
		$controller_name=strtolower(get_class($CI));
		$width = $controller->get_form_width();
		$CI->load->model('savesetting');
		$edit_image ='<img title="Edit" src="'.base_url().'images/ico_edit_16.png">';
		$CI->load->model('savesetting');
		$specimen_name   = $CI->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$item->lab_specimen_id);
		$department_id = $CI->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$item->lab_dept_id);
		$department_name = stripslashes($CI->savesetting->get_Table_Column_Data('laboratory_items','laboratory_name','id',$department_id)); 
		//if($item->status=='0') { $status_image ='<img title="Active" src="'.base_url().'images/active.png">'; $status_title ='Inactive'; }
        //else { $status_image ='<img title="Inactive" src="'.base_url().'images/inactive.png">';$status_title ='Active';}
		$item_status = lang('common_open');
		if($item->status=='1') { $item_status = lang('common_close'); }
		if($item->status=='2') { $item_status = lang('common_rejected'); }
		$table_data_row='<tr>';
		$table_data_row.="<td width='5%'><input type='checkbox' id='item_$item->id' value='".$item->id."'/></td>";
		$table_data_row.='<td width="20%" align="left">'.$item->first_name.' '.$item->last_name.'</td>';
		$table_data_row.='<td width="22%" align="left">'.$specimen_name.'</td>';
		$table_data_row.='<td width="22%" align="left">'.$department_name.'</td>';
		$table_data_row.='<td width="15%" align="left">'.$item->receiept_no.'</td>';
		$table_data_row.='<td width="15%" align="left">'.date('d-m-Y',strtotime($item->registration_date)).'</td>';
		$table_data_row.='<td>'.anchor($controller_name."/update_status/$item->id/$item->person_id/width~500/height~400", $item_status,array('title'=>lang('common_change_status'),'class'=>"thickbox")).'</td>';
		$table_data_row.='</tr>';
		return $table_data_row;
	}
	
	
		


?>