<?php 
	/*Gets the html data rows for the items.*/
	
function get_manage_highlight_data_rows($method_type,$items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';	
	if($method_type=='diabetes_customers')  { foreach($items->result() as $item){ $table_data_rows = get_diabetescustomers_data_row_list($method_type,$item,$controller);  } }
	if($method_type=='insulin_treatment')   { foreach($items->result() as $item){ $table_data_rows = get_insulintreatment_data_row_list($method_type,$item,$controller);  } }

	return $table_data_rows;}
/*Gets the html data rows for the items.*/
function get_manage_table_data_rows($method_type,$items,$controller)
{	$CI =& get_instance();
	$table_data_rows='';	
	if($items->num_rows()==0)
	{
	$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('items_no_items_to_display')."</div></tr></tr>";	}	
	else 
	{		
		if($method_type=='diabetes_customers')  { foreach($items->result() as $item){ $table_data_rows .= get_diabetescustomers_data_row_list($method_type,$item,$controller);  } }
		if($method_type=='insulin_treatment')   { foreach($items->result() as $item){ $table_data_rows .= get_insulintreatment_data_row_list($method_type,$item,$controller);  } }

	}
	return $table_data_rows;
}
	 //************************** MANAGE METER TYPE RECORSD.*****************************************************//
	function get_manage_table($method_type,$items,$controller)
	{
		$CI =& get_instance();
		$table='<table class="tablesorter" id="sortable_table">';	
		
		if($method_type=='diabetes_customers')
		{	$headers = array('<input type="checkbox" id="select_all"/>',lang('common_last_name'),lang('common_first_name'),$CI->lang->line('diabetes_registration_no'),
							  $CI->lang->line('patient_history'),$CI->lang->line('common_update_profile'), $CI->lang->line('common_added_date'),$CI->lang->line('common_action'));	
		}
		if($method_type=='insulin_treatment')
		{	$headers = array('<input type="checkbox" id="select_all"/>',lang('diabetes_before_breakfast'),lang('diabetes_before_lunch'),$CI->lang->line('diabetes_before_dinner'),$CI->lang->line('common_added_date'),$CI->lang->line('common_action'));	
		}
		$table.='<thead><tr>';
		$count = 0;
		foreach($headers as $header)
		{
			$count++;		
			if ($count == 1) { $table.="<th class='leftmost'>$header</th>"; 	}
			elseif ($count == count($headers))	{ $table.="<th class='rightmost'>$header</th>";	}
			else {	$table.="<th>$header</th>";	}
		}

		$table.='</tr></thead><tbody>';
		$table.=get_manage_table_data_rows($method_type,$items,$controller);
		$table.='</tbody></table>';
		return $table;
	}

	function get_diabetescustomers_data_row_list($method_type,$item,$controller)
	{
		$CI =& get_instance();		
		$controller_name=strtolower(get_class($CI));
		$width = $controller->get_form_width();
		$edit_image ='<img title="Edit" src="'.base_url().'images/ico_edit_16.png">';
		if(isset($item->birth_date) && $item->birth_date!='') $birth_date = date('d-m-Y',strtotime($item->birth_date)); else $birth_date='_';		
		$table_data_row='<tr>';
		$table_data_row.="<td width='5%'><input type='checkbox' id='item_$item->person_id' value='".$item->person_id."'/></td>";
		$table_data_row.='<td width="14%">'.$item->last_name.'</td>';
		$table_data_row.='<td width="14%">'.$item->first_name.'</td>';
		$table_data_row.='<td width="25%">'.$item->clinic_registration_no.'</td>';
		$table_data_row.='<td width="15%">'.anchor("history/index/$item->person_id/diabetes", lang('patient_history')).'</td>';
		$table_data_row.='<td width="15%">'.anchor("customers/update_profile/$item->person_id/diabetes/width~$width/height~400",lang('common_update_profile'),array('class'=>'thickbox','title'=>lang('common_update_profile'))).'</td>';
		$table_data_row.='<td width="12%">'.date('d-m-Y',strtotime($item->registration_date)).'</td>';
		$table_data_row.='<td width="3%" align="center">'.anchor($controller_name."/edit_diabetes_customer/$item->person_id/width~$width", $edit_image ,array('class'=>'thickbox','title'=>'Edit Form')).'</td>';		
		$table_data_row.='</tr>';
		return $table_data_row;
	}
	
	function get_insulintreatment_data_row_list($method_type,$item,$controller)
	{
		$CI =& get_instance();		
		$controller_name=strtolower(get_class($CI));
		$width = $controller->get_form_width();
		if($item->status=='0') { $status_image ='<img title="Active" src="'.base_url().'images/active.png">'; $status_title ='Inactive'; }
						  else { $status_image ='<img title="Inactive" src="'.base_url().'images/inactive.png">';$status_title ='Active'; }
		$edit_image ='<img title="Edit" src="'.base_url().'images/ico_edit_16.png">';
		$table_data_row='<tr>';
		$table_data_row.="<td width='5%'><input type='checkbox' id='item_$item->id' value='".$item->id."'/></td>";
		$table_data_row.='<td width="20%">'.$item->breakfast_insulin.'</td>';
		$table_data_row.='<td width="20%">'.$item->lunch_insulin.'</td>';
		$table_data_row.='<td width="20%">'.$item->dinner_insulin.'</td>';
		$table_data_row.='<td width="10%">'.date('d-m-Y',strtotime($item->added_date)).'</td>';
		$table_data_row.='<td width="7%" align="center"><a title="Change Status" href="javascript:;" onclick="Change_Status('.$item->id.')">'.$status_image .'</a>&nbsp;&nbsp;'.anchor($controller_name."/view/insulin_treatment/$item->id/width~$width", $edit_image ,array('class'=>'thickbox','title'=>'Edit Form')).'</td>';		
		$table_data_row.='</tr>';
		return $table_data_row;
	}
		


?>