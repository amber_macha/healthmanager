<?php

/*
Gets the html table to manage people.
*/
function get_people_manage_table($people,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	$controller_name=strtolower(get_class($CI));
	if($controller_name=='customers') { 
	$headers = array('<input type="checkbox" id="select_all" />', 
	lang('common_last_name'),
	lang('common_first_name'),
	lang('patient_history'),
	lang('common_update_profile'),
	lang('call_report'),
	'&nbsp',
	'&nbsp;&nbsp;',
	'&nbsp;&nbsp;');
	}
	else 
	{
		$headers = array('<input type="checkbox" id="select_all" />', 
	lang('common_last_name'),
	lang('common_first_name'),
	lang('common_email'),
	lang('common_group_name'),
	lang('common_phone_number'),
	'&nbsp');
	
	
	}
	$table.='<thead><tr>';

	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_people_manage_table_data_rows($people,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the people.
*/
function get_people_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_person_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_person_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$start_of_time =  date('Y-m-d', 0);
	$today = date('Y-m-d');	
	$link = site_url('reports/specific_'.($controller_name == 'customers' ? 'customer' : 'employee').'/'.$start_of_time.'/'.$today.'/'.$person->person_id.'/all/0');
	$patient_history = site_url('history/index/'.$person->person_id);
	$patient_report_history = site_url('history/patient_report/'.$person->person_id);
	
	$table_data_row='<tr>';	
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
	$table_data_row.='<td width="19%"><a href="'.$link.'" class="underline">'.$person->last_name.'</a></td>';
	$table_data_row.='<td width="15%"><a href="'.$link.'" class="underline">'.$person->first_name.'</a></td>';
	if($controller_name=='customers')
	 {	
	  $table_data_row.='<td width="17%"><a href="'.$patient_history.'" class="underline">'.lang('patient_history').'</a></td>';
	   $table_data_row.='<td width="15%">'.anchor($controller_name."/update_profile/$person->person_id/width~800/", lang('common_update_profile'),array('class'=>'thickbox','title'=>lang('common_update_profile'))).'</td>';
	 }
	else {  $group_name  = $CI->Employee->Get_Employee_GroupName($person->employee_group_id);
			$table_data_row.='<td width="25%">'.mailto($person->email,$person->email, array('class' => 'underline')).'</td>';
			$table_data_row.='<td width="15%"><a href="javascrip:;" class="underline">'.$group_name.'</a></td>';
		 }
	if($controller_name=='customers')
	 {	
	  $table_data_row.='<td width="12%"><a href="'.$patient_report_history.'" class="underline">'.lang('call_report').'</a></td>';
	 }
	else { $table_data_row.='<td width="15%">'.$person->phone_number.'</td>';	}
	
		
	$table_data_row.='<td width="5%" >'.anchor($controller_name."/view/$person->person_id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';
	if($controller_name == 'customers')
	{
			$table_data_row.='<td width="5%" >'.anchor($controller_name."/view_card/$person->person_id/", 'Card').'</td>';
			
			$table_data_row.='<td width="5%" >'.anchor($controller_name."/repeated_attendance/$person->person_id/", lang('repeated_attendance_url'),array('title'=>lang('view_repeated_attendance'),'rel'=>"facebox")).'</td>';
			
			
			//$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/generate_barcode_labels/$person->person_id/", '&nbsp; BC ',array('title'=>'Print Barcode','target'=>'_blank')).'</td>';///

	} 
	$table_data_row.='</tr>';
	
	return $table_data_row;
}

/*
Gets the html table to manage suppliers.
*/
function get_supplier_manage_table($suppliers,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';	
	$headers = array('<input type="checkbox" id="select_all" />',
	lang('suppliers_company_name'),
	lang('common_last_name'),
	lang('common_first_name'),
	lang('common_email'),
	lang('common_phone_number'),
	'&nbsp');
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	
	$table.='</tr></thead><tbody>';
	$table.=get_supplier_manage_table_data_rows($suppliers,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the supplier.
*/
function get_supplier_manage_table_data_rows($suppliers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
	}
	
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='8'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_supplier_data_row($supplier,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$supplier->person_id' value='".$supplier->person_id."'/></td>";
	$table_data_row.='<td width="17%">'.$supplier->company_name.'</td>';
	$table_data_row.='<td width="17%">'.$supplier->last_name.'</td>';
	$table_data_row.='<td width="17%">'.$supplier->first_name.'</td>';
	$table_data_row.='<td width="22%">'.mailto($supplier->email,$supplier->email).'</td>';
	$table_data_row.='<td width="17%">'.$supplier->phone_number.'</td>';		
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$supplier->person_id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';				
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage items.
*/
function get_items_manage_table($items,$controller)
{
	$CI =& get_instance();
	$has_cost_price_permission = $CI->Employee->has_module_action_permission('items','see_cost_price', $CI->Employee->get_logged_in_employee_info()->person_id);
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('items_item_number'),
	$CI->lang->line('items_name'),
	$CI->lang->line('items_category'),
	);
	
	if($has_cost_price_permission)
	{
		$headers = array_merge($headers, array($CI->lang->line('items_cost_price')));
	}
	
	$headers = array_merge($headers, array(
	$CI->lang->line('items_unit_price'),
	$CI->lang->line('items_tax_percents'),
	$CI->lang->line('items_quantity'),
	$CI->lang->line('items_inventory'),
	'&nbsp;'
	));
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='</tbody></table>';
	return $table;
}


function get_items_manage_table_for_items($items,$controller)
{
	$CI =& get_instance();
	$has_cost_price_permission = $CI->Employee->has_module_action_permission('items','see_cost_price', $CI->Employee->get_logged_in_employee_info()->person_id);
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	$CI->lang->line('items_item_number'),
	$CI->lang->line('items_name'),
	$CI->lang->line('items_column_expire_name'),
	);
	
	if($has_cost_price_permission)
	{
		$headers = array_merge($headers, array($CI->lang->line('items_cost_price')));
	}
	
	$headers = array_merge($headers, array(
	$CI->lang->line('items_unit_price'),
	$CI->lang->line('items_quantity'),
	$CI->lang->line('items_inventory'),
	'&nbsp;'
	));
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='</tbody></table>';
	return $table;
}


/*
Gets the html data rows for the items.
*/
function get_items_manage_table_data_rows($items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row_for_itemlist($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}



function get_item_data_row_for_itemlist($item,$controller)
{
	$CI =& get_instance();
	$has_cost_price_permission = $CI->Employee->has_module_action_permission('items','see_cost_price', $CI->Employee->get_logged_in_employee_info()->person_id);
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$bgcolor='';
	$edit_image ='<img title="Edit" src="'.base_url().'images/ico_edit_16.png">';	 
	if($item->item_status=='0') { $status_image ='<img title="Active" src="'.base_url().'images/active.png">'; $status_title ='Inactive'; }
    else { $status_image ='<img title="Inactive" src="'.base_url().'images/inactive.png">';$status_title ='Active';$bgcolor ='style="background-color:yellow"'; }
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%' $bgcolor ><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
	$table_data_row.='<td width="15%" '.$bgcolor.'>'.$item->item_number.'</td>';
	$table_data_row.='<td width="18%" '.$bgcolor.'>'.$item->name.'</td>';
	$table_data_row.='<td width="11%" '.$bgcolor.'>'.date('d-m-Y',strtotime($item->items_expire_date)).'</td>';
	if ($has_cost_price_permission)
	{
		$table_data_row.='<td width="11%" align="right" '.$bgcolor.'>'.to_currency($item->cost_price).'</td>';
	}
	$table_data_row.='<td width="11%" align="right" '.$bgcolor.'>'.to_currency($item->unit_price).'</td>';
	$table_data_row.='<td width="11%" '.$bgcolor.'>'.$item->quantity.'</td>';
	$table_data_row.='<td width="8%" '.$bgcolor.'>'.anchor($controller_name."/inventory/$item->item_id/width~$width", lang('common_inv'),array('class'=>'thickbox','title'=>lang($controller_name.'_count'))).'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width~$width", lang('common_det'),array('class'=>'thickbox','title'=>lang($controller_name.'_details_count'))).'</td>';//inventory details	
	$table_data_row.='<td width="10%" '.$bgcolor.' class="rightmost" align="center"><a title="Change Status" href="javascript:;" onclick="Change_Status('.$item->item_id.')">'.$status_image .'</a>&nbsp;'.anchor($controller_name."/view/$item->item_id/width~$width", $edit_image,array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}


function get_item_data_row_updateList($item,$controller)
{
	$CI =& get_instance();
	$has_cost_price_permission = $CI->Employee->has_module_action_permission('items','see_cost_price', $CI->Employee->get_logged_in_employee_info()->person_id);
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$bgcolor='';
	$edit_image ='<img title="Edit" src="'.base_url().'images/ico_edit_16.png">';	 
	if($item->item_status=='0') { $status_image ='<img title="Active" src="'.base_url().'images/active.png">'; $status_title ='Inactive'; }
	else { $status_image ='<img title="Inactive" src="'.base_url().'images/inactive.png">';$status_title ='Active';  $bgcolor ='style="background-color:yellow"'; }
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%' $bgcolor><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
	$table_data_row.='<td width="15%" '.$bgcolor.'>'.$item->item_number.'</td>';
	$table_data_row.='<td width="15%" '.$bgcolor.'>'.$item->name.'</td>';
	$table_data_row.='<td width="11%" '.$bgcolor.'>'.date('d-m-Y',strtotime($item->items_expire_date)).'</td>';
	if ($has_cost_price_permission)
	{
		$table_data_row.='<td width="11%" align="right" '.$bgcolor.'>'.to_currency($item->cost_price).'</td>';
	}
	$table_data_row.='<td width="11%" align="right" '.$bgcolor.'>'.to_currency($item->unit_price).'</td>';
	$table_data_row.='<td width="11%" '.$bgcolor.'>'.$item->quantity.'</td>';
	$table_data_row.='<td width="8%" '.$bgcolor.'>'.anchor($controller_name."/inventory/$item->item_id/width~$width", lang('common_inv'),array('class'=>'thickbox','title'=>lang($controller_name.'_count'))).'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width~$width", lang('common_det'),array('class'=>'thickbox','title'=>lang($controller_name.'_details_count'))).'</td>';//inventory details	
	$table_data_row.='<td width="10%" '.$bgcolor.' class="rightmost" align="center"><a title="Change Status" href="javascript:;" onclick="Change_Status('.$item->item_id.')">'.$status_image .'</a>&nbsp;'.anchor($controller_name."/view/$item->item_id/width~$width", $edit_image,array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';		

	$table_data_row.='</tr>';
	return $table_data_row;
}


function get_item_data_row($item,$controller)
{
	$CI =& get_instance();
	$has_cost_price_permission = $CI->Employee->has_module_action_permission('items','see_cost_price', $CI->Employee->get_logged_in_employee_info()->person_id);
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
	$table_data_row.='<td width="15%">'.$item->item_number.'</td>';
	$table_data_row.='<td width="15%">'.$item->name.'</td>';
	$table_data_row.='<td width="11%">'.$item->category.'</td>';
	if ($has_cost_price_permission)
	{
		$table_data_row.='<td width="11%" align="right">'.to_currency($item->cost_price).'</td>';
	}
	$table_data_row.='<td width="11%" align="right">'.to_currency($item->unit_price).'</td>';
	$table_data_row.='<td width="11%">'.$tax_percents.'</td>';	
	$table_data_row.='<td width="11%">'.$item->quantity.'</td>';
	$table_data_row.='<td width="8%">'.anchor($controller_name."/inventory/$item->item_id/width~$width", lang('common_inv'),array('class'=>'thickbox','title'=>lang($controller_name.'_count'))).'&nbsp;&nbsp;&nbsp;&nbsp;'.anchor($controller_name."/count_details/$item->item_id/width~$width", lang('common_det'),array('class'=>'thickbox','title'=>lang($controller_name.'_details_count'))).'</td>';//inventory details	
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$item->item_id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_giftcards_manage_table( $giftcards, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	lang('giftcards_giftcard_number'),
	lang('giftcards_card_value'),
	lang('giftcards_customer_name'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_giftcards_manage_table_data_rows( $giftcards, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_giftcards_manage_table_data_rows( $giftcards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($giftcards->result() as $giftcard)
	{
		$table_data_rows.=get_giftcard_data_row( $giftcard, $controller );
	}
	
	if($giftcards->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('giftcards_no_giftcards_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_giftcard_data_row($giftcard,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$link = site_url('reports/detailed_'.$controller_name.'/'.$giftcard->customer_id.'/0');
	$cust_info = $CI->Customer->get_info($giftcard->customer_id);
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='giftcard_$giftcard->giftcard_id' value='".$giftcard->giftcard_id."'/></td>";
	$table_data_row.='<td width="15%">'.$giftcard->giftcard_number.'</td>';
	$table_data_row.='<td width="20%">'.to_currency($giftcard->value).'</td>';
	$table_data_row.='<td width="15%"><a class="underline" href="'.$link.'">'.$cust_info->first_name. ' '.$cust_info->last_name.'</a></td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$giftcard->giftcard_id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage item kits.
*/
function get_item_kits_manage_table( $item_kits, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />', 
	lang('items_item_number'),
	lang('item_kits_name'),
	lang('item_kits_description'),
	lang('items_unit_price'),
	lang('items_tax_percents'),
	'&nbsp', 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the item kits.
*/
function get_item_kits_manage_table_data_rows( $item_kits, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
	}
	
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('item_kits_no_item_kits_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_item_kit_data_row($item_kit,$controller)
{

	$CI =& get_instance();
	
	$item_kit_tax_info=$CI->Item_kit_taxes->get_info($item_kit->item_kit_id);
	$tax_percents = '';
	foreach($item_kit_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);

	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='item_kit_$item_kit->item_kit_id' value='".$item_kit->item_kit_id."'/></td>";
	$table_data_row.='<td width="15%">'.$item_kit->item_kit_number.'</td>';
	$table_data_row.='<td width="15%">'.$item_kit->name.'</td>';
	$table_data_row.='<td width="20%">'.$item_kit->description.'</td>';
	$table_data_row.='<td width="20%" align="right">'.(!is_null($item_kit->unit_price) ? to_currency($item_kit->unit_price) : '').'</td>';
	$table_data_row.='<td width="20%">'.$tax_percents.'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$item_kit->item_kit_id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';
	$table_data_row.='</tr>';
	return $table_data_row;
}



/*
Gets the html table to manage giftcards.
*/
function get_deadbody_manage_table( $person, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	$headers = array('<input type="checkbox" id="select_all" />', 
						lang('common_full_name'),
						lang('common_sex'),
						lang('common_mobile_number'),
						lang('common_city'),
						lang('common_country'),						
						lang('dead_body_date'),
						lang('dead_body_view_certificate'),
						'&nbsp;&nbsp;',
					);	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_deadbody_manage_table_data_rows( $person, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_deadbody_manage_table_data_rows( $people, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_deadbody_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_deadbody_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$download_url='_';
	if(isset($person->death_certificate_with_signature) && $person->death_certificate_with_signature!='' && $person->death_certificate_with_signature!='0')
	{
	  $download_url =  '<a href="javascript:;" onclick="download_certificate('.$person->id.');">'.lang('common_download').'</a>';
	}
	$today = date('Y-m-d');	
	$table_data_row='<tr>';	
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->id' value='".$person->id."'/></td>";
	$table_data_row.='<td width="25%">'.$person->patient_name.'</td>';
	$table_data_row.='<td width="10%">'.$person->patient_sex.'</td>';	
	$table_data_row.='<td width="10%">'.$person->mobile_no_relative.'</td>';
	$table_data_row.='<td width="15%">'.$person->city_name.'</td>';	
	$table_data_row.='<td width="10%">'.$person->country_name.'</td>';
	$table_data_row.='<td width="15%">'.date('d-m-Y',strtotime($person->date_of_death)).'</td>';
	$table_data_row.='<td width="5%" >'.$download_url.'</td>';	
	$table_data_row.='<td width="5%" >'.anchor($controller_name."/view/$person->id/width~$width", lang('common_edit'),array('class'=>'thickbox','title'=>lang($controller_name.'_update'))).'</td>';
 
	$table_data_row.='</tr>';
	
	return $table_data_row;
}

function get_patient_history_menu($customer_id=-1)
{
	$CI =& get_instance();		
	$table_data_row = '<div class="history-menu-div"> 
				<div class="history-menu menu-left1"><a href="history/index/'.$customer_id.'"><span class="titleC">'.lang('doctor_comment').'</span></a></div>
				<div class="history-menu menu-left"><a href="investigation/index/'.$customer_id.'"><span class="titleC">'.lang('invastigation').'</span></a></div>
				<div class="history-menu menu-left"><a href="medicine/index/'.$customer_id.'"><span class="titleC">'.lang('history_drugs').'</span></a></div>
                <div class="history-menu menu-left"><a href="uss_xrays/index/'.$customer_id.'"><span class="titleC">'.lang('uss_xray').'</span></a></div>
    		    <div class="history-menu menu-left1"><a href="dental/index/'.$customer_id.'"><span class="titleC">'.lang('history_dental').'</span></a></div>
				<div class="history-menu menu-left"><a href="eyeclinic/index/'.$customer_id.'"><span class="titleC">'.lang('history_eye_clinic').'</span></a></div>
				<div class="history-menu menu-left"><a href="specialist/index/'.$customer_id.'"><span class="titleC">'.lang('history_specialist').'</span></a></div>
                <div class="history-menu menu-left"><a href="otherservice/index/'.$customer_id.'"><span class="titleC">'.lang('history_other_services').'</span></a></div>
		  </div>';  
	return $table_data_row;
}

?>