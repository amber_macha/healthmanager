<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**/
$autoload['packages'] = array();
/**/
$autoload['libraries'] = array('database','form_validation','session','user_agent', 'pagination','functions');
/**/
$autoload['helper'] = array('form','url','table','text','currency', 'html', 'download', 'base64', 'mailchimp', 'language', 'file', 'assets','array');
/**/
$autoload['config'] = array();
/**/
$autoload['language'] = array('common', 'config', 'customers', 'employees', 'error', 'items', 'login', 'module', 'reports', 'sales','suppliers','receivings','giftcards', 'item_kits','ipd_in_patient','history');
/**/
$autoload['model'] = array('Appconfig','Person','Customer','Employee','Module','Item', 'Item_taxes', 'Sale', 'Supplier','Inventory','Receiving','Giftcard', 'Item_kit', 'Item_kit_taxes', 'Item_kit_items', 'Appfile', 'Module_action');

/* End of file autoload.php */
/* Location: ./application/config/autoload.php */