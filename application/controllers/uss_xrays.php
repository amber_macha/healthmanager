<?php
require_once ("person_controller.php");
class Uss_xrays extends Person_controller
{
	function __construct()
	{
		parent::__construct('uss_xrays');
		$this->load->model('uss_xray');
	}		
	function index($customer_id=-1)
	{ 
	    if($customer_id==-1){redirect(base_url());}	
		$this->check_action_permission('search');
		$exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers/');}
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = '';
		$data['controller_name'] = strtolower(get_class());
		$data['form_width'] = $this->get_form_width();
		$data['person_info']=$this->Customer->get_info($customer_id);	
		$data['manage_AllLabTest'] = $this->uss_xray->search($customer_id,$search=''); 	
		$data['manage_history_menu']=get_patient_history_menu($customer_id);
		$data['per_page'] = '20';
		$this->load->view('uss_xrays/manage',$data);
	}	
	function view($customer_id=-1)
	{
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$data['person_id'] = $customer_id;		
	    $data['lab_category_list'] = $this->uss_xray->SearchLabCatgory($search='',$value='',$customer_id);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'uss_xrays_service','activity_items_id'=>$customer_id,'activity_description'=>lang('uss_xrays_customer_view_medicine_drugs'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//	
		$this->load->view("uss_xrays/form",$data);
	}	
	function addreport($customer_id=-1,$invest_id=-1)
	{	
		if($customer_id==-1 || $invest_id==-1){redirect(base_url());}   
		$this->check_action_permission('action_P_R');
		$data['person_id'] = $customer_id;	
		$data['invest_id'] = $invest_id;
		$this->load->view("uss_xrays/addreport",$data);
	}	
	function savereport($customer_id=-1,$items_id=-1)
	{
		if($customer_id==-1 || $items_id==-1){redirect(base_url());}	
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$report_details = addslashes($this->input->post('investigation_report'));
		
		$uss_file_name          =  $_FILES["uss_file_name"]["name"];
		$xrays_file_name        =  $_FILES["xrays_file_name"]["name"];
		$default_file_extension = $this->config->item('DEFAULT_USS_XRAYS_IMAGE_TYPE');
		$upload_uss_report_Images = array(); $upload_xrays_report_Images = array();
		if(count($uss_file_name)>0)
			{ 
			   $dir_dest = 'images/patient_report_images/uss_reports/'.$customer_id.'/'.$items_id.'/uss_images/';
			   if (!file_exists($dir_dest)) {  mkdir($dir_dest, 0777, true); }
				for($i=0;$i<count($uss_file_name);$i++)
				{					 
					$ext = $this->Customer->findExtension($_FILES['uss_file_name']['name'][$i]);
					if($ext == 'jpg' || $ext == 'png' || $ext == 'gif' || $ext == 'jpeg')
					{
						if($_FILES['uss_file_name']['name'][$i]!='')
						{	
							$fileNameNoExtension = preg_replace("/\.[^.]+$/", "", basename($uss_file_name[$i]));
							$uploadFileName = $customer_id.'_'.$items_id.'_'.time().'_'.$fileNameNoExtension.".".$default_file_extension;
							$path = $dir_dest .$uploadFileName;
							move_uploaded_file($_FILES["uss_file_name"]["tmp_name"][$i],$path);
							$upload_uss_report_Images[] = $uploadFileName;
						}
					}										
				}
			}				
		if(count($xrays_file_name)>0)
		{     
		   $dir_dest = 'images/patient_report_images/uss_reports/'.$customer_id.'/'.$items_id.'/xrays_images/';
		   if (!file_exists($dir_dest)) {  mkdir($dir_dest, 0777, true); }
			for($i=0;$i<count($xrays_file_name);$i++)
			{					 
				$ext = $this->Customer->findExtension($_FILES['xrays_file_name']['name'][$i]);
				if($ext == 'jpg' || $ext == 'png' || $ext == 'gif' || $ext == 'jpeg')
				{
					if($_FILES['xrays_file_name']['name'][$i]!='')
					{					
						$fileNameNoExtension = preg_replace("/\.[^.]+$/", "", basename($xrays_file_name[$i]));
						$uploadFileName = $customer_id.'_'.$items_id.'_'.time().'_'.$fileNameNoExtension.".".$default_file_extension;
						$path = $dir_dest .$uploadFileName;
						move_uploaded_file($_FILES["xrays_file_name"]["tmp_name"][$i],$path);
						$upload_xrays_report_Images[] = $uploadFileName;
					}
				}										
			}
		}		
		if(count($upload_uss_report_Images)>0  || count($upload_xrays_report_Images)>0) 
		{  
		   $uss_report = stripslashes($this->input->post('uss_report'));
		   $xrays_report = stripslashes($this->input->post('xrays_report'));
		   
           $uss_report_Images   = implode(',',$upload_uss_report_Images); 	
		   $xrays_report_Images = implode(',',$upload_xrays_report_Images); 			   
		   $success= $this->db->query("update ".$this->db->dbprefix('lab_uss_xrays')." set uss_report_images='".$uss_report_Images."', xrays_report_images='".$xrays_report_Images."', uss_report_details='".$uss_report."', xrays_report_details='".$xrays_report."',tested_by_employee_id = '".$login_uerData->person_id."', tested_date='".date('Y-m-d h:i:s')."',status='1' where id='".$items_id."' and person_id='".$customer_id."' and status='0' ");		   if($success)
		   { echo json_encode(array('success'=>true,'message'=>lang('uss_xrays_report_successful_added'), 'person_id'=>$customer_id) );
		     //***save employee activites in database***//
			  $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'result_PR','activity_items_id'=>$items_id,'activity_description'=>lang('uss_xrays_report_successful_added'));	
			  $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			  //***save employee activites in database***//
		   }			}
		else { echo json_encode(array('success'=>true,'message'=>lang('uss_xrays_report_error_added'), 'person_id'=>$customer_id) );}
	}	
	function viewreport($customer_id=-1,$invest_id=-1)
	{	
	    if($customer_id==-1 || $invest_id==-1){ redirect(base_url()); } 
		$this->check_action_permission('action_V_R');
		$search='';
		$data['person_id'] = $customer_id;	 
		$data['invest_id'] = $invest_id;			
		$data['reportdata'] = $this->uss_xray->get_Report($customer_id,$invest_id);			$data['diseases_list'] = $this->Customer->GetDiseasList($customer_id,$invest_id,$LabTestType='lab_uss_xrays',$search);
		$data['diabetes_list'] = $this->Customer->GetDiabetesList($customer_id,$invest_id,$LabTestType='lab_uss_xrays',$search);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'result_VR','activity_items_id'=>$invest_id,'activity_description'=>lang('common_view_patient_result_pr'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//				$this->load->view("uss_xrays/viewreport",$data);
	} 		
	function save($customer_id=-1)
	{
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		if($customer_id=='-1' || $this->input->post('lab_category')=='')
		 {
		    echo json_encode(array('success'=>false,'message'=>lang('uss_xrays_error_adding'), 'person_id'=>$customer_id)); die;
		 }
		if($customer_id!='-1' && $this->input->post('lab_category')!='')
		 {	//$lab_category  = implode(',',$this->input->post('lab_category'));
			//$this->load->model('labtest');		
			//$valid = $this->labtest->valid_Active_Lab_category($lab_category);
			//if($valid) { echo json_encode(array('success'=>false,'message'=>lang('uss_xrays_deactivate_not_added'), 'person_id'=>$customer_id)); die;}
			$lab_category  = $this->input->post('lab_category');
			for($i=0; $i<count($lab_category); $i++) 
			{
			  $person_data = array('lab_category_id'=>$lab_category[$i],'person_id'=>$customer_id,'added_by_employee_id'=>$login_uerData->person_id,'added_date'=>date('Y-m-d h:i:s'),'status'  =>'0');			  $success = $this->db->insert('lab_uss_xrays' ,$person_data);
			}
			if($success)
			{ echo json_encode(array('success'=>true,'message'=>lang('uss_xrays_successful_added'), 'person_id'=>$customer_id) );
			  //***save employee activites in database***//
			  $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'uss_xrays_service','activity_items_id'=>$customer_id,'activity_description'=>lang('uss_xrays_successful_added'));	
			  $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			  //***save employee activites in database***//
			}
		 }
		 else { echo json_encode(array('success'=>false,'message'=>lang('uss_xrays_error_adding'), 'person_id'=>$customer_id));}
	}
	function delete($customer_id=-1)
	{
		$this->check_action_permission('delete');
		$to_delete=$this->input->post('ids');
		$bad_words = array('on');
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		if($this->uss_xray->delete_list($customer_id,$to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('uss_xrays_successful_deleted').' '.	count($to_delete).' '.lang('uss_xrays_one_or_multiple')));		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('uss_xrays_cannot_be_deleted')));
		}
	}	
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Customer->search_count_all($search);
			$table_data = $this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		else
		{
			$config['total_rows'] = $this->Customer->count_all();
			$table_data = $this->Customer->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		$config['base_url'] = site_url('customers/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}	
	function search($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$search_data=$this->uss_xray->search($customer_id,$search, $this->input->post('order_col') ,$this->input->post('order_dir'));
		$pagination='';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));	
	}	
	function getservices($customer_id=-1)
	{	
         $search = addslashes($this->input->post('service')); 
		  $value = addslashes($this->input->post('checkedval'));
		echo $search_data=$this->uss_xray->SearchLabCatgory($search,$value,$customer_id);die;  
	}	
	function activeDeActivate($customer_id=-1)
	{
		$this->check_action_permission('search');
		$value = $this->uri->segment(4);	
		$this->load->model('labtest');		
		$success=$this->labtest->ActiveDeActivateCatgory($value); 
		if($success==1)
		{  $message = lang('uss_xrays_successful_updated'); 
		   //***save employee activites in database***//
			$test2 = explode('-',$value); $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'uss_xrays_service','activity_items_id'=>$activity_items_id,'activity_description'=>lang('uss_xrays_successful_updated'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {  $message = lang('uss_xrays_error_updated'); }
		echo $message; die;	
	}
	
	function searchDiseasList($customer_id=-1)
	 {	
		$searchtype = addslashes($this->input->post('searchtype')); 
		$search = addslashes($this->input->post('service'));
		$value = addslashes($this->input->post('checkedval'));
		$invest_id = addslashes($this->input->post('invest_id'));
		if($searchtype == 'mtuha_daignoses') { $search_data = $this->Customer->GetDiseasList($customer_id,$invest_id,$LabTestType='lab_uss_xrays',$search);}
		if($searchtype == 'daibetes')		 { $search_data = $this->Customer->GetDiabetesList($customer_id,$invest_id,$LabTestType='lab_uss_xrays',$search); }		
        echo $search_data; die;
	 }
	function saveDiseases($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$savetype = $this->uri->segment(4);
		$invest_id = $this->uri->segment(5);
		$value = $this->uri->segment(6);		
		$success=$this->uss_xray->saveDiseases($customer_id,$invest_id, $value,$savetype);
		if($success==1)
		{   $message = lang('diseases_services_successful_added'); 
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'uss_xrays_service','activity_items_id'=>$invest_id,'activity_description'=>lang('diseases_services_successful_added'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {  $message = lang('diseases_services_error_added'); }
		echo $message; die;	
	}

    function suggest()
	{
		$suggestions = $this->uss_xray->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}	
	function get_form_width()
	{			
		return 550;
	}
	
	function view3dmotion($item_type=-1,$customer_id=-1,$item_id=-1)
	{	
		$this->check_action_permission('add_update');
		$total_report_images='';
		if($customer_id!=-1 && $item_id!=-1 && ($item_type=='uss_images' || $item_type=='xrays_images'))
		{ 
		    if(!$this->Customer->exists($customer_id) || !$this->uss_xray->exists_uss_xrays($item_id)) { redirect('home');}
			$search='';			
			$reportdata  = $this->uss_xray->get_Report($customer_id,$item_id);
			
            if(isset($reportdata->uss_report_images) && $reportdata->uss_report_images!='' && $item_type=='uss_images') 
			{
				$total_report_images = explode(',',$reportdata->uss_report_images);
			    if(count($total_report_images>0)) { $report_images_path = base_url().'images/patient_report_images/uss_reports/'.$customer_id.'/'.$item_id.'/uss_images'; }
			}

			if(isset($reportdata->xrays_report_images) && $reportdata->xrays_report_images!='' && $item_type=='xrays_images') 
			{
				$total_report_images = explode(',',$reportdata->xrays_report_images);
			    if(count($total_report_images>0)) { $report_images_path = base_url().'images/patient_report_images/uss_reports/'.$customer_id.'/'.$item_id.'/xrays_images'; }
			}
			if($total_report_images=='' || count($total_report_images)==0) { if($customer_id=-1) redirect('uss_xrays/index/'.$customer_id); else redirect('home');  }
			
			$data['report_images_path'] = $report_images_path;	
			$data['person_id'] = $customer_id;	 
			$data['item_id']   = $item_id;			
			$this->load->view("partial/view3dmotion",$data);
	    }
       else { redirect('home'); } 	  
	}   
	
	function addClarificationDesciption($customer_id=-1)
	{
	 $description = mysql_real_escape_string($this->input->post('description')); 
	 $item_id  = $this->input->post('service_id');
	 $result ='0';
	 $this->load->model('medicinetest');
	 if($description!='' && $item_id!='')
	 {	
		$existData = $this->medicinetest->exist_medicine_drugs_dosage( $item_id ,$customer_id );
		if($existData)
		 {	
			$result = $this->db->query("update ".$this->db->dbprefix('lab_category_dosage')." set dosage_description ='".$description."' where person_id='".$customer_id."' and item_id='".$item_id."' ");
		    $activity_description = lang('common_clarification_updated_successful');
		} 
		 else 
		 {				
			$result = $this->db->query("insert into ".$this->db->dbprefix('lab_category_dosage')." set item_id='".$item_id."', dosage_description ='".$description."',person_id='".$customer_id."' ");
		    $activity_description = lang('common_clarification_added_successful');
		 }
			if($result){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'uss_xrays','activity_type_category'=>'lab_services','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		   }	
	 }
		echo $result; die;		
	}
}
?>