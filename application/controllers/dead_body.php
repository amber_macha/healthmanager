<?php
require_once ("person_controller.php");
class Dead_body extends Person_controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('deadbody');
		$this->load->language('deadbody');
	}
		
	function index()
	{  	
		$this->check_action_permission('search');
		$config['base_url'] = site_url('dead_body/sorting');
		$config['total_rows'] = $this->deadbody->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_deadbody_manage_table($this->deadbody->get_all($data['per_page']),$this);
		$this->load->view('dead_body/manage',$data);
	}
	
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->deadbody->search_count_all($search);
			$table_data = $this->deadbody->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'person_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->deadbody->count_all();
			$table_data = $this->deadbody->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'person_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('dead_body/sorting');
		$config['per_page'] = $per_page;  
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_deadbody_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
		
	}
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->deadbody->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'person_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('dead_body/search');
		$config['total_rows'] = $this->deadbody->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_deadbody_manage_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/*Gives search suggestions based on what is being searched for */
	function suggest()
	{
		$suggestions = $this->deadbody->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/* Loads the customer edit form */
	function view($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$data['dead_body_id'] 	= 	$customer_id;
		$data['person_info']	=	$this->deadbody->get_info($customer_id);
		$data['months'] 		= 	$this->Customer->get_months();
        $data['days'] 			= 	$this->Customer->get_days();
        $data['years'] 			= 	$this->Customer->get_years();
		$data['diseases_list']  = 	$this->Customer->GetDiseasList_ClinicalNoteMultiple('dead_body',$data['person_info']->lab_category_id,$search='');;
		$this->load->view("dead_body/form",$data);
	}	
	
	function searchDeadBodyDiagnosesList($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$dead_body_id = $this->input->post('dead_body_id'); 
		$search 		  = addslashes($this->input->post('service')); 
		$checkedval	   	  = addslashes($this->input->post('checkedval'));
		$person_info      = $this->deadbody->get_info($customer_id);
		$success = $this->Customer->GetDiseasList_ClinicalNoteMultiple('dead_body',$person_info->lab_category_id,$search);
		echo $success; die;	
	}
	
	/* Inserts/updates a customer */
	function save($customer_id=-1)
	{		
	  $this->check_action_permission('add_update');
	  $customer_id =  $this->input->post('dead_body_id');	
	  $account_no =  $this->input->post('account_no');	 
	  if($this->input->post('first_name')=='' || $this->input->post('patient_address')=='' || $this->input->post('date_of_birth')=='' || $this->input->post('date_of_death')=='') 
	  {	
		echo json_encode(array('success'=>false,'message'=>lang('dead_body_error_adding_updating'),'person_id'=>-1));die; 
	  }		 
	 if($this->input->post('account_no')!='')
	 { 
	    if(!$this->Customer->Check_AccountNo($account_no)){ echo json_encode(array('success'=>false,'message'=>lang('customers_account_no_invalid'),'person_id'=>-1));die; }
	 }	 
	 if($this->input->post('account_no')!='')
	 { 
	   if($this->deadbody->Exist_Patient_Dead_Body($account_no,$customer_id)){ echo json_encode(array('success'=>false,'message'=>lang('dead_body_selected_item_already_exist'),'person_id'=>-1));die;}
	 }		
	 
       $birth_date='';$date_of_death=''; 
	   if($this->input->post('date_of_birth')!='') { $birth_date = date('Y-m-d',strtotime($this->input->post('date_of_birth')));}	
	   if($this->input->post('date_of_death')!='') { $date_of_death = date('Y-m-d',strtotime($this->input->post('date_of_death')));}	
	   $dead_body_data = array(	'patient_tribe'		=>	stripslashes($this->input->post('patient_tribe')),									
								'reason_of_death'	=>	$this->input->post('reason_of_death'),
								'date_of_death'		=>	$date_of_death,
								'car_no_brought_dead_body_in_hospital'	=>	$this->input->post('car_no_brought_dead_body'),
								'name_of_drive_relative'				=>	$this->input->post('name_of_drive_relative'),
								'relationship'							=>	$this->input->post('relationship'),
								'mobile_no_relative'					=>	$this->input->post('mobile_no_relative'),		
							  );
		 $customer_data = array('dateOfBirth'=>	$birth_date,								
								'sex'		 =>	$this->input->post('sex'),
								'age'		 =>	$this->input->post('age_year'),
								'age_month'	 =>	$this->input->post('age_month'),
								'age_day'	 =>	$this->input->post('age_day')								
								);	
		 $person_data  =  array('first_name' =>	stripslashes($this->input->post('first_name')),
								'last_name'	 =>	stripslashes($this->input->post('last_name')),
								'address_2'	 =>	stripslashes($this->input->post('patient_address')),
								'city'		 =>	$this->input->post('city_name'),
								'country'	 =>	$this->input->post('nationality')
							  );						
		
		if(!$customer_id or !$this->deadbody->exists($table_name='customers_dead_body',$customer_id))
		{ 
		  $temp_customer_data = array('other_account_number'=>$this->Customer->Generate_Dead_Body_AccountNo());
		  $customer_data = array_merge($temp_customer_data,$customer_data); 
		}
        if($this->input->post('lab_category')!='')
		{
			$temp_array = array('lab_category_id' => implode(',',$this->input->post('lab_category')));
			$dead_body_data = array_merge($dead_body_data,$temp_array);
        }			
		//print_r($customer_data);die; 
		$added_array = array('added_date'=>date('Y-m-d'));
		if($customer_id==-1){ $dead_body_data = array_merge($dead_body_data,$added_array); }
		$Customer_Insert_ID = $this->deadbody->save($dead_body_data,$customer_data,$person_data,$customer_id);

		$Patient_Profile_Image	 ='';
		$Patient_Oldimage 		 = $this->deadbody->GetDeadBody_document($customer_id,'profile_image');
		$deadbody_certificate_Old  = $this->deadbody->GetDeadBody_document($customer_id,'profile_cerificate');
	
	    //****DEAD BODY PROFILE IMAGE UPLAOD************//	  
        if(isset($_FILES['patient_image']['name']) && $_FILES['patient_image']['name']!='')
	    {   
		    $this->load->library('Upload'); 
		    $size=getimagesize($_FILES['patient_image']['tmp_name']);
		    if($size[0]<150){ echo json_encode(array('success'=>false,'message'=>lang('common_patient_image_size_not_allowed'),'person_id'=>-1)); die;}
			  		
				$dir_dest = 'images/dead_body_document/dead_body_images/'; 
				$handle = new Upload($_FILES['patient_image']);
				$FindImage = $person_data['first_name'].'_'.$Customer_Insert_ID;
				$error = NULL;
				if ($handle->uploaded) 
				{					
					$handle->file_new_name_body = $FindImage;
					$handle->image_resize          = true;
					//$handle->image_y               = 224;
					$handle->image_x               = 150;
					$handle->image_ratio_crop = true;
					$handle->file_name_body_pre = 'thumb_';
					$handle->Process($dir_dest);

					$handle->file_new_name_body = $FindImage;
					$handle->image_ratio_crop= true;
					$handle->image_resize= true;
					$handle->image_ratio=true;
					$handle->image_ratio_x          = true;
					$handle->image_ratio_y          = true;
					
					$handle->image_x    = $size[0];
					$handle->image_y    = $size[1];
					$handle->Process($dir_dest);
					$New_patient_image = $handle->file_dst_name;			
					
					if ($handle->processed) {
					$handle-> Clean();
					}else{
					$error = $handle->error	;
					}
		        }		
	  }
	  	if(isset($New_patient_image) && $New_patient_image!='') 
		{ 
			$Patient_Profile_Image = $New_patient_image;
			if($Patient_Oldimage!='') { $Patient_Oldimage_thumb = 'thumb_'.$Patient_Oldimage; @unlink($dir_dest.$Patient_Oldimage); @unlink($dir_dest.$Patient_Oldimage_thumb); }
		}
		else { $Patient_Profile_Image = $Patient_Oldimage; }		  
	  
	  //****DEATH CERIFICATE UPLAOD************//	  
	  if(isset($_FILES['death_certificate']['name']) && $_FILES['death_certificate']['name']!='')
		{ 
			$dir_dest = 'images/dead_body_document/dead_body_certificate/';										 
			$ext = $this->Customer->findExtension($_FILES['death_certificate']['name']);
			if($ext == 'jpg' || $ext == 'png' || $ext == 'gif' || $ext == 'jpeg' || $ext == 'pdf' || $ext == 'doc' || $ext == 'docx')
			{						
				$fileNameNoExtension 	  = preg_replace("/\.[^.]+$/", "", basename($_FILES['death_certificate']['name']));
				$deadbody_certificate_new = $fileNameNoExtension.'_'.$Customer_Insert_ID.".".$ext;
				$path = $dir_dest .$deadbody_certificate_new;
				move_uploaded_file($_FILES["death_certificate"]["tmp_name"],$path);
			}	
		}
		if(isset($deadbody_certificate_new) && $deadbody_certificate_new!='') 
		{ 
		$deadbody_certificate = $deadbody_certificate_new; if($deadbody_certificate_Old!='') { @unlink($dir_dest.$deadbody_certificate_Old);}
		}
		else { $deadbody_certificate = $deadbody_certificate_Old; }		
	   //****DEATH CERIFICATE UPLAOD************//	 		
		
	
		$document_array =array('dead_body_images' => $Patient_Profile_Image, 'death_certificate_with_signature' => $deadbody_certificate);
		//print_r($customer_data);die;
		$this->load->model('savesetting');
		if($this->savesetting->saveUpdateTable('customers_dead_body',$document_array,'person_id',$Customer_Insert_ID))
		{
		if($customer_id==-1)
		{
			echo json_encode(array('success'=>true,'message'=>lang('dead_body_successful_adding').' '. $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$Customer_Insert_ID));
		}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('dead_body_successful_updating').' '.$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$Customer_Insert_ID));
			}
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('dead_body_error_adding_updating').' '.	$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	}
	
	
	public function validate_image($image = NULL) 
	{		
		$file_name      =   $image['name'];
		$allowed_ext    =   array('jpg', 'jpeg', 'png', 'gif', 'bmp');
		$ext            =   strtolower(end(explode('.', $file_name)));
		$allowed_file_types =   array('image/jpeg','image/jpg','image/gif','image/png');
		$file_type          =   $image['type'];
		if(!in_array($ext, $allowed_ext) && !in_array($file_type, $allowed_file_types)) { return '2'; }
		if($image['size'] < 2500) {	return '3';	}
		else {	return true; }
	}
	

	/*	This deletes customers from the customers table	*/
	function delete()
	{
		$this->check_action_permission('delete');
		$customers_to_delete=$this->input->post('ids');
		
		if($this->deadbody->delete_list($customers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted').' '.
			count($customers_to_delete).' '.lang('customers_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_deadbody_data_row($this->deadbody->get_info($item_id),$this);
		echo $data_row;
	}
	
	function download($item_id=-1)
	{
		if($item_id!='-1')
		{ 
		  $deadbody_certificate = $this->deadbody->GetDeadBody_document($item_id,'profile_cerificate');
			if(isset($deadbody_certificate) && $deadbody_certificate!='')
			{ 
				$file_path =  'images/dead_body_document/dead_body_certificate/'.$deadbody_certificate;
				$this->load->library('downloadfile');	
				set_time_limit(0);
				$this->downloadfile->output_file($file_path, ''.$deadbody_certificate.'', 'text/plain');				
			}
       	}
	}

	function excel_import()
	{
		$this->check_action_permission('add_update');
		$this->load->view("dead_body/excel_import", null);
	}
	
	/* added for excel expert */
	function excel_export() {
		$data = $this->deadbody->get_all($limit=10000, $offset=0,$col='person_id',$order='desc')->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array('Account No', 'Name', 'Tribe', 'Address','Date of Birth', 'Sex', 'Day of Age','Month of Age','Year of Age','Date of Death', 'Reson of death','Car No that bring dead body in Hospital', 'Name of Drive or relative', 'Relation ship','mobile No of relative','City Name', 'Country Name', 'Added Date');
		$rows[] = $row;
		foreach ($data as $r) {
		if(isset($r->dateOfBirth) && $r->dateOfBirth!='' && $r->dateOfBirth!='0000-00-00'){$date_of_birth = date('d-m-Y',strtotime($r->dateOfBirth));}  else {$date_of_birth='';}
			$row = array(
				$r->other_account_number,
				$r->first_name .' '.$r->last_name,
				$r->patient_tribe,
				$r->address_2,
				$date_of_birth,
				$r->sex,
				$r->age_day,
				$r->age_month,
				$r->age,
				date('d-m-Y',strtotime($r->date_of_death)),
				$r->reason_of_death,
				$r->car_no_brought_dead_body_in_hospital,
				$r->name_of_drive_relative,
				$r->relationship,				
				$r->mobile_no_relative,
				$r->city,
				$r->country,
				date('d-m-Y',strtotime($r->added_date))
			);
			$rows[] = $row;
		}
		
		$content = array_to_csv($rows);
		force_download('dead_body_export_'.date('d-m-Y') . '.csv', $content);
		exit;
	}

	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$this->db->trans_start();
				
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					 if($data[0]!='Account No')
					 { 
						$customer_data = array(
						'account_number'=>$data[0],
						'patient_name'=>$data[1],
						'patient_tribe'=>$data[2],
						'patient_address'=>$data[3],
						'date_of_birth'=>$data[4],
						'patient_sex'=>$data[5],
						'age_year'=>$data[6],
						'age_month'=>$data[7],
						'age_day'=>$data[8],
						'date_of_death'=>date('Y-m-d',strtotime($data[9])),
						'reason_of_death'=>$data[10],
						'car_no_brought_dead_body_in_hospital'=>$data[11],
						'name_of_drive_relative'=>$data[12],
						'relationship'=>$data[13],
						'mobile_no_relative'=>$data[14],
						'city_name'=>$data[15],
						'country_name'=>$data[16],
						'added_date'=>date('Y-m-d',strtotime($data[17]))
						);					
						if(!$this->deadbody->save_import_data($customer_data,$data[0]))
						{	
							echo json_encode( array('success'=>false,'message'=>lang('dead_body_duplicate_account_id')));die;							
						}
					}	
				}
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
		}
		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('dead_body_import_successfull')));
	}
	
	function cleanup()
	{
		$this->deadbody->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('dead_body_cleanup_sucessful')));
	}
	
	/*	get the width for the add/edit form	*/
	function get_form_width()
	{			
		return 550;
	}
	
	//*********START DIAGNOSES CODE************************************//
	function dead_body_diagnoses() 
	{
		$this->check_action_permission('add_update');
		$this->load->model('labtest');
		$data['diseases_list'] = $this->labtest->GetSearchAllDiseasList($search='',$type_diseases='dead_body');		
		$this->load->view("dead_body/form_dead_body_diagnoses",$data);
	}

	 function addnewDiagnoses($service_id=-1)  
	 {	
	     $this->check_action_permission('add_update');
		 $this->load->model('labtest');
		 $servicename   = addslashes($this->input->post('servicename')); 
		 $codeNumber    = addslashes($this->input->post('codeNumber'));
		 $category_type = addslashes($this->input->post('category_type'));
		 $message ='';
		 if($servicename!='' && $codeNumber!='') 
		 {
		    $valid = $this->labtest->existsDiagnoses($servicename, $codeNumber,$service_id,$category_type);
			if($valid=='no') { $message = '0'; }
			if($valid=='yes')
			 {     
				  $category_type = 'dead_body';				
			      $lab_category = $this->db->dbprefix('lab_category');
				  if(isset($service_id) && $service_id!='-1'){ 			  
				   $result = $this->db->query("update ".$this->db->dbprefix('lab_category')." set lab_name ='".$servicename."',code_number ='".$codeNumber."' where category_type='dead_body' and id = '".$service_id."' ");
				  }
				  else { 
				   $result = $this->db->query("insert into ".$this->db->dbprefix('lab_category')." set lab_name ='".$servicename."',code_number ='".$codeNumber."',category_type='dead_body',status='0' ");
				  }
			   if($result) { $message = '1'; }  else { $message = '2'; }
			 }
		 } 
		 echo $message; die;		
	 }
	 

	function searchDiagnosesList($customer_id=-1)
	{	
		$this->load->model('labtest');
		$searchtype = addslashes($this->input->post('searchtype')); 
		$search = addslashes($this->input->post('service'));
		$search_data = $this->labtest->GetSearchAllDiseasList($search ,$type_diseases='dead_body');
		echo $search_data; die;
	}	
	 
	function getDiagnosesData()
	{	
        $this->load->model('labtest');	
		$services_id   = addslashes($this->input->post('services_id'));
		$category_type = addslashes($this->input->post('category_type'));
		echo $search_data=$this->labtest->GetDiseasData_BYID($services_id,$category_type);die;
	}	 
	
}
?>