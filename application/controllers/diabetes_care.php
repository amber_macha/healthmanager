<?php
require_once ("secure_area.php");
class Diabetes_care extends Secure_area 
{
	function __construct()
	{
		parent::__construct();	
		$this->load->language('diabetes_care');
		$this->load->model('diabetescare');
		$this->load->helper('diabetecare_helper');
	}
	
	function index()
	{
	    $this->check_action_permission('search');
	    $data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'diabetes','activity_items_id'=>'','activity_description'=>lang('diabetes_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("diabetes_care/home",$data);
	}
	
	//*************START FACILTY REGISTRATION FUNCTIONLITY**********************//
	function facility_registration()
	{		
		$this->check_action_permission('add_update');
		$ExecuteQuery = $this->diabetescare->Get_Facility_Data();
		$data['Diabetes_Data'] = $ExecuteQuery;
		$data['form_width']=$this->get_form_width();
		$this->load->view("diabetes_care/facility_registration",$data);
	}
	
	function facility_save($item_id=-1)
	{
	   $Facility_Data = array('facility_name' 			=> $this->input->post('facility_name'),
							  'facility_zone_name' 		=> $this->input->post('zone_name'),
							  'facility_region_id' 		=> $this->input->post('region_name'),
							  'facility_distict_id' 	=> $this->input->post('distict_name'),
							  'facility_location' 		=> $this->input->post('facility_location'),
							  'facility_health_owner' 	=> $this->input->post('facility_owner_health'),
							  'country_name' 			=> $this->input->post('nationality')
							 ); 
			
		if($item_id=='-1') { $Temp_Array = array('added_date' => date('Y-m-d H:i:s')); $Facility_Data = array_merge($Facility_Data , $Temp_Array); }					 
		if($this->diabetescare->facility_save($Facility_Data,$item_id))
		{
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('diabetes_items_successful_adding').' '.	$this->input->post('facility_name'),'person_id'=>$item_id));
			}
			else 
			{
				echo json_encode(array('success'=>true,'message'=>lang('diabetes_items_successful_updating').' '. $this->input->post('facility_name'),'person_id'=>$item_id));
			}
		}				
	}
	//*************END FACILTY REGISTRATION FUNCTIONLITY**********************//
		
	//****************START CLINI SECTONS FUNCTIONLITY***********************//
	function diabetes_customers()
	{		
		$this->check_action_permission('search');
		$method_type 			 = $this->router->fetch_method();
		$config['base_url']		 = site_url('diabetes_care/sorting_customers');
		$config['total_rows'] 	 = $this->diabetescare->count_all_tables('diabetes_customers');
		$config['per_page']		 = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] 	 = $this->pagination->create_links();
		$data['controller_name'] = strtolower(get_class());
		$data['form_width']		 = $this->get_form_width();
		$data['per_page'] 		 = $config['per_page'];
		$data['manage_table']	 = get_manage_table($method_type ,$this->diabetescare->get_all_tables('diabetes_customers',$data['per_page']),$this);
		$data['add_new_title'] 	 = lang('diabetes_registration_patient');
		$data['page_title'] 	 = lang('diabetes_patients_listing_page_title');
		$data['method_type'] 	 = $method_type;
		$this->load->view("diabetes_care/manage",$data);		
	}	
	
	function sorting_customers()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$method_type 			 = 'diabetes_customers';
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows']= $this->diabetescare->search_count_all( $method_type ,$search);
			$table_data = $this->diabetescare->search($method_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'DESCRIPTION' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows']= $this->diabetescare->count_all_tables($method_type );
			$table_data = $this->diabetescare->get_all_tables($method_type,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'DESCRIPTION' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url']		 = site_url('diabetes_care/sorting_customers');
		$config['per_page'] 	 = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']		 = $this->pagination->create_links();
		$data['manage_table']	 = get_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}	
	
	//*************START FACILTY REGISTRATION FUNCTIONLITY**********************//
	function clinic_schedule_manager()
	{		
		$this->check_action_permission('add_update');
		$data['method_type']	= $this->router->fetch_method();
		$data['form_width']		= $this->get_form_width();
		$data['months'] 		= $this->Customer->get_months();
		$data['days'] 			= $this->Customer->get_days();
		$data['years'] 			= $this->diabetescare->get_years();
		$this->load->view("diabetes_care/clinic_schedule_manager",$data);
	}
	
	function search()
	{
		$this->check_action_permission('search');	
		$search=$this->input->post('search');
		//$search_type=$this->input->post('search_type');	
		$search_type 		= $this->uri->segment(3);		
		$per_page = 50;
		$search_data=$this->diabetescare->search($search_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'plan_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('portal_settings/search/');
		$config['total_rows'] = $this->diabetescare->search_count_all($search_type,$search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($search_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function view($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$method_type 		= $this->uri->segment(3);
		$item_id 		= $this->uri->segment(4);
		$this->load->model('savesetting');
		$data = array();		
		$data['method_type'] 	  = $method_type;
		$data['insulin_category'] = $this->savesetting->Get_Table_Data('insulin_category','insulin_name'); 
		$data['item_info']	 	  = $this->diabetescare->get_Item_ArrayData($method_type,$item_id);
		$this->load->view("diabetes_care/form_$method_type",$data);
	}	
	
	function edit_diabetes_customer($customer_id=-1)
	{
		$this->check_action_permission('add_update');
		if($customer_id==-1){ redirect('diabetes_care/diabetes_customers'); }
		$data['method_type'] 	  = 'diabetes_customers';
		$data['person_info']	= $this->diabetescare->get_Item_ArrayData($data['method_type'] ,$customer_id);
		$data['patient_info']	= $this->diabetescare->get_Item_ArrayData($data['method_type'] ,$customer_id);
		$this->load->model('savesetting');	
		$data['ResidenceData'] = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
		$data['OccupatiosData']=  $this->savesetting->Get_Table_Data('occupations','occupation_name');
		$data['months'] 		= $this->Customer->get_months();
		$data['days'] 			= $this->Customer->get_days();
		$data['years'] 			= $this->Customer->get_years();
		$this->load->view("diabetes_care/form_customers",$data);			
	}
	
	function get_patient_profile($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$method_type 		= 'patient_profile';
		$item_id 		= $this->uri->segment(4);
		$data = array();		
		$data['method_type'] = $method_type;
		$this->load->view("diabetes_care/form_$method_type",$data);
	}	
	
	function GetRegion($zone_name=-1, $region_name=-1)
	{		
		echo $RegionData = $this->diabetescare->Region_DropDownList($zone_name , $region_name); die;		
	}
	
	function GetHospitals($zone_name=NULL, $hospital_id=-1)
	{		
		echo $RegionData = $this->diabetescare->Hospital_DropDownList($zone_name , $hospital_id); die;		
	}
	
	function suggest()
	{  
        $method_type = $this->uri->segment(3);
		$suggestions = $this->diabetescare->get_search_suggestions( $method_type, $this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	function delete()
	{
		$this->check_action_permission('delete');		
		$method_type 		= 	$this->uri->segment(3);
		$items_to_delete	=	$this->input->post('ids');

		if($this->diabetescare->delete_list($method_type, $items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('diabetes_successful_deleted').' '.count($items_to_delete).' '.lang('diabetes_one_or_multiple'),'item_id'=>-1));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('diabetes_cannot_be_deleted'),'item_id'=>-1));
		}
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_manage_highlight_data_rows($method_type,$this->Setting->get_Item_ArrayData($method_type,$item_id),$this);
		echo $data_row;
	}
	
	function cleanup()
	{
		$this->diabetescare->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('customers_cleanup_sucessful')));
	}
	
	function excel_export() 
	{
		$this->check_action_permission('add_update');
		$method_type = $this->uri->segment(3);
		$data = $this->diabetescare->get_all_tables($method_type)->result_object();
		$this->load->helper('report');
		
		$rows = array();
		$row = array('Persoon ID','Account Number','Clinic Registration No','First Name', 'Last Name', 'Phone Number', 'Address 1', 'Address 2', 'country', 
					 'Patient Type', 'Patient Category', 'leader', 'religion', 'kin', 'Sex', 'Occupation', 'Nationality', 'Father Name', 'Father Telephone Number', 'Mother Name', 'Mother Telephone Number', 'Guardians Name', 'Guardians Telephone Number', 'Ethnicity', 'Date of Birth', 'Year of diagnosis of Diabetes', 'Year of diagnosis of Hypertension', 'Year of diagnosis of Abnormal Lipids', 'Education Level', 'House of mud and wattle, grass(straw) thatched', 'House with electricity or solar power supply', 'House with piped water', 'House with flushing toilet inside house', 'House with kitchen inside','Location of Residence', 'Number of relatives with diabetes', 'Number of relatives with hypertension', 'Registration Date');
		$rows[] = $row;				
		foreach ($data as $r)
		{
			if($r->patient_category==0){ $patient_category='Cost Shearing';} if($r->patient_category==1){ $patient_category='TIKA'; }
			if($r->house_mud_wattle_grass_thatched==0) $house_mud_wattle_grass_thatched='NO'; if($r->house_mud_wattle_grass_thatched==1) $house_mud_wattle_grass_thatched='YES';
			if($r->house_with_electricity_solar_powe_supply==0) $house_with_electricity_solar_powe_supply='NO'; if($r->house_with_electricity_solar_powe_supply==1) $house_with_electricity_solar_powe_supply='YES';
			if($r->house_with_piped_water==0) $house_with_piped_water='NO'; if($r->house_with_piped_water==1) $house_with_piped_water='YES';
			if($r->house_with_flushing_toilet_inside_house==0) $house_with_flushing_toilet_inside_house='NO'; if($r->house_with_flushing_toilet_inside_house==1) $house_with_piped_water='YES';
			if($r->house_with_kitchen_inside==0) $house_with_kitchen_inside='NO'; if($r->house_with_kitchen_inside==1) $house_with_kitchen_inside='YES';
			
		$row = array(
				$r->person_id,
				$r->account_number,
				$r->clinic_registration_no,
				$r->first_name,
				$r->last_name,
				$r->phone_number,
				$r->address_1,
				$r->address_2,
				$r->country,
				$r->patient_type,
				$patient_category,
				$r->leader,
				$r->religion,
				$r->kin,
				$r->sex,
				$r->occupation,
				$r->nationality,				
				$r->father_name,
				$r->father_contact_no,
				$r->mother_name,
				$r->mother_contact_no,
				$r->guardians_name,
				$r->guardians_contact_no,
				$r->ethnicity,
				$r->birth_date,
				$r->year_diagnosis_of_diabetes,
				$r->year_diagnosis_of_hypertension,	
				$r->year_diagnosis_of_abnormal_lipids,	
				$r->education_level,
				$house_mud_wattle_grass_thatched,
				$house_with_electricity_solar_powe_supply,
				$house_with_piped_water,
				$house_with_flushing_toilet_inside_house,
				$house_with_kitchen_inside,
				$r->location_of_residence,
				$r->number_of_relatives_with_diabetes,
				$r->number_of_relatives_with_hypertension,
				$r->registration_date
			);		
		  $rows[] = $row;	
		}
		$content = array_to_csv($rows);
		force_download('diabetes_care_customers_export'.date('d-m-Y') . '.csv', $content);
		exit;
	}
	
	function excel_import()
	{
		$this->check_action_permission('add_update');
		$method_type = $this->uri->segment(3);
		$data['method_type'] = $method_type;
		$this->load->view("diabetes_care/excel_import", $data);
	}
	
	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$message = lang('items_excel_import_failed'); 
		$method_type = $this->input->post('method_type');
		
		$failCodes = array();
		if($_FILES['file_path']['error']!=UPLOAD_ERR_OK){ echo json_encode(array('success'=>false,'message'=> lang('items_excel_import_failed'),'person_id'=>-1));die; }
		else
		{
		  if(($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE){   
		  	
			
			$file = fopen($_FILES['file_path']['tmp_name'],"r");
			//print_r(fgetcsv($file));die;
			fgetcsv($handle);
			while (($data = fgetcsv($handle)) !== FALSE) 
			{ //print_r($data);
			    $registration_date='';
				if($data[10]=='Cost Shearing'){ $patient_category='0';} if($data[10]=='TIKA'){ $patient_category='1'; }
				if($data[29]=='NO')	 { $house_mud_wattle_grass_thatched='0';} if($data[29]=='YES'){ $house_mud_wattle_grass_thatched='1'; }
				if($data[30]=='NO')	 { $house_with_electricity_solar_powe_supply='0';} if($data[30]=='YES'){ $house_with_electricity_solar_powe_supply='1'; }
				if($data[31]=='NO')	 { $house_with_flushing_toilet_inside_house='0';} if($data[31]=='YES'){ $house_with_flushing_toilet_inside_house='1'; }
				if($data[32]=='NO')	 { $house_with_piped_water='0';} if($data[32]=='YES'){ $house_with_piped_water='1'; }
				if($data[33]=='NO')	 { $house_with_kitchen_inside='0';} if($data[33]=='YES'){ $house_with_kitchen_inside='1'; }	
				if($data[37]!='')	 { $registration_date = date('Y-m-d'); } 					
				$person_id =  $data[0];	 	 
			    $person_data = array('person_id'					=>	$data[0],
									'account_number'				=>	$data[1],
									'clinic_registration_no'		=>	$data[2],
									'first_name'					=>	$data[3],
									'last_name'						=>	$data[4],
									'phone_number'					=>	$data[5],
									'address_1'						=>	$data[6],
									'address_2'						=>	$data[7],
									'country'						=>	$data[8],
									'patient_type'					=>	$data[9],
									'patient_category'				=>	$patient_category,
									'leader'						=>	$data[11],
									'religion'						=>	$data[12],
									'kin'							=>	$data[13],									
									'sex'							=>	$data[14],
									'occupation'					=>	$data[15],
									'nationality'					=>	$data[16],
									'father_name'					=>	$data[17],
									'father_contact_no'				=>	$data[18],
									'mother_name'					=>	$data[19],
									'mother_contact_no'				=>	$data[20],
									'guardians_name'				=>	$data[21],
									'guardians_contact_no'			=>	$data[22],									
									'ethnicity'						=>	$data[23],
									'birth_date'					=>	$data[24],
									'year_diagnosis_of_diabetes'	=>	$data[25],
									'year_diagnosis_of_hypertension'=>	$data[26],
									'year_diagnosis_of_abnormal_lipids' =>	$data[27],
									'education_level'					=>	$data[28],
									'house_mud_wattle_grass_thatched'	=>	$house_mud_wattle_grass_thatched,
									'house_with_electricity_solar_powe_supply' =>	$house_with_electricity_solar_powe_supply,
									'house_with_flushing_toilet_inside_house'  =>	$house_with_flushing_toilet_inside_house,								
									'house_with_piped_water'				   =>	$house_with_piped_water,
									'house_with_kitchen_inside'				   =>	$house_with_kitchen_inside,									
									'location_of_residence'					   =>	$data[34],
									'number_of_relatives_with_diabetes'	   	   =>	$data[35],
									'number_of_relatives_with_hypertension'	   =>	$data[36],
									'registration_date'	  					   =>	$registration_date
									);			 	 	 	 	 	 	  	 	
				//print_r($person_data); die;

			   if($data[0]!='Persoon ID'){  $this->diabetescare->import_excel( $method_type, $person_id, $person_data); }			
			} 
		      echo json_encode(array('success'=>true,'message'=>lang('diabetes_import_successfull'),'person_id'=>-1));die;				
			}
		}
		  echo json_encode(array('success'=>false,'message'=>$message,'person_id'=>-1));die;		
	}

		//****************START CLINI SECTONS FUNCTIONLITY***********************//
	function insulin_treatment()
	{		
		$this->check_action_permission('search');
		$method_type 			 = $this->router->fetch_method();
		$config['base_url']		 = site_url('diabetes_care/sorting_insulin_treatment');
		$config['total_rows'] 	 = $this->diabetescare->count_all_tables($method_type);
		$config['per_page']		 = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] 	 = $this->pagination->create_links();
		$data['controller_name'] = strtolower(get_class());
		$data['form_width']		 = $this->get_form_width();
		$data['per_page'] 		 = $config['per_page'];
		$data['manage_table']	 = get_manage_table($method_type ,$this->diabetescare->get_all_tables($method_type,$data['per_page']),$this);
		$data['add_new_title'] 	 = lang('diabetes_add_new_insulin');
		$data['page_title'] 	 = lang('diabetes_insulin_treatment');
		$data['method_type'] 	 = $method_type;
		$this->load->view("diabetes_care/manage",$data);		
	}	
	
	function sorting_insulin_treatment()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$method_type 			 = 'insulin_treatment';
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows']= $this->diabetescare->search_count_all( $method_type ,$search);
			$table_data = $this->diabetescare->search($method_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'DESCRIPTION' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows']= $this->diabetescare->count_all_tables($method_type );
			$table_data = $this->diabetescare->get_all_tables($method_type,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'DESCRIPTION' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url']		 = site_url('diabetes_care/sorting_insulin_treatment');
		$config['per_page'] 	 = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']		 = $this->pagination->create_links();
		$data['manage_table']	 = get_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}	
	
	function chanage_status()
	{	
		$this->check_action_permission('add_update');			
		$items_id = $this->input->post('items_id');
		$method_type = $this->input->post('method_type');
		$table_name='insulin_treatments';
		if($items_id!='')
		{
			$item_data = $this->diabetescare->get_Item_ArrayData($method_type,$items_id);
			if($item_data->status=='1')  $status ='0';  else $status ='1';
			$item_data = array('status'=>$status);
			$this->db->where('id',$items_id);
			$result = $this->db->update('insulin_treatments',$item_data);		
		   if($result) { echo json_encode(array('success'=>true,'message'=>lang('common_change_status_successful'),'person_id'=>$items_id)); }
		}	
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'item_id'=>$items_id));	}	 
	}
	
	//*********START DIAGNOSES CODE************************************//
	function insulin_category($customer_id=-1)
	{
		$this->check_action_permission('add_update');
		$search='';
		$data['category_list'] = $this->diabetescare->searchAllInsulin_CategoryList($search);		
		$this->load->view("diabetes_care/insulin_treatment/add_insulin_categoryform",$data); 
	}	
	
	function searchInsulin_categoryList($customer_id=-1)
	 {	
		$search = addslashes($this->input->post('service')); 		
		echo $search_data = $this->diabetescare->searchAllInsulin_CategoryList($search);die;
	 }
	
	 function getInsulinBrandData_Byid()
	 {	
	    $services_id = addslashes($this->input->post('services_id')); 
		echo $search_data=$this->diabetescare->GetInsulinBrandData_BYID($services_id);die;
	 }
	 

	
	function get_form_width()
	{			
		return 550;
	}
}
?>