<?php
require_once ("secure_area.php");
class Ministry_reporting extends Secure_area 
{
	function __construct()
	{
		parent::__construct('ministry_reporting');	
		$this->load->language('ministryreporting'); 
		$this->load->helper('report');
		$this->load->model('Ministryreporting');
	}
	
	function index()
	{
	    $data['controller_name']=strtolower(get_class());
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();
		$report_date_range_simple='';
		$selected_month =  date('m');	$selected_day  =  date('d'); $selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 =  date('d'); $selected_year2 = date('Y');
		$data['selected_month']  	 =  $selected_month; $data['selected_day']  = $selected_day;	
		$data['selected_year']       =  $selected_year;
		$data['selected_month2'] 	 =  $selected_month2; 
		$data['selected_day2']	     =  $selected_day2;	
		$data['selected_year2'] 	 =  $selected_year2;
		$data['selected_date_range'] =  $report_date_range_simple;			
		$data['Authority_List']  	 =  $this->Ministryreporting->get_AllReporting_Category($parent_id=0);
		$data['Reporting_TypeData']	 =  $this->Ministryreporting->get_Sub_Category( $parent_id=0 , $sub_group_id=0 ); 
		$this->load->view("ministry_reporting_tool/home",$data);
	}

	function SubCategoryList()
	{
	    $SubGroupsData='';
		$group_name = $this->input->post('group_name');
		$sub_group_name = $this->input->post('sub_group_name');
		if($group_name==''){ echo json_encode(array('success'=>false,'groupData'=>$SubGroupsData));	die;}
		if($group_name!='')
		{
			$SubGroupsData = $this->Ministryreporting->get_Sub_Category( $group_name, $sub_group_name); 
			echo json_encode(array('success'=>true,'groupData'=>$SubGroupsData));
		}		
	}	
}
?>