<?php
require_once ("person_controller.php");
class Medicine extends Person_controller
{
	function __construct()
	{
		parent::__construct('medicine'); 
		$this->load->model('patienthistory');
		$this->load->model('medicinetest');
	}		
	function index($customer_id=-1)
	{
	    if($customer_id==-1){redirect(base_url());}		
		$this->check_action_permission('search');
	    $exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers/');}
		$data['Diabetes_Customer_Type'] = $this->uri->segment(4);
		$config['base_url'] 		 = site_url('customers/sorting');
		$config['total_rows'] 		 = '';
		$data['controller_name'] 	 = strtolower(get_class());
		$data['method_name']		 = $this->router->fetch_method(); 
		$data['search_url'] 		 = 'search';
		$data['form_width'] 		 = $this->get_form_width();
		$data['person_info']		 = $this->Customer->get_info($customer_id);	
		//$data['manage_AllLabTest']   = $this->medicinetest->get_all($customer_id); 
		$data['medicine_Drugs_List'] = $this->medicinetest->search($customer_id,$search='');
		$data['manage_history_menu'] = get_patient_history_menu($customer_id);		
		$data['per_page'] 			 = '20';
		$data['list_title_name'] 	 = lang('medicine_header');
		if(isset($_SESSION['PRINTING_MEDICINE_LIST']) && $_SESSION['PRINTING_MEDICINE_LIST']!=''){ unset($_SESSION['PRINTING_MEDICINE_LIST']); }
		$this->load->view('medicine/manage',$data);	}	
	function view($customer_id=-1)
	{	
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$data['controller_name'] = strtolower(get_class());
		$data['person_id'] = $customer_id;		
		$data['lab_category'] = $this->medicinetest->SearchLabCatgory($search='',$value='',$customer_id);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_medicine_drugs'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//		
		$this->load->view("medicine/form",$data);
	}
	function save($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$lab_investigation = $this->db->dbprefix('lab_medicine');	
		if($customer_id=='-1' || $this->input->post('lab_category')=='')
		 { 
		    echo json_encode(array('success'=>false,'message'=>lang('medicine_error_adding'), 'person_id'=>$customer_id));
		 }		 
		if($customer_id!='-1' && $this->input->post('lab_category')!='')
		 { //$lab_category  = implode(',',$this->input->post('lab_category'));
		   //$this->load->model('labtest');
		   //$valid = $this->labtest->valid_Active_Lab_category($lab_category);
		   //if($valid) { echo json_encode(array('success'=>false,'message'=>lang('medicine_not_selected'), 'person_id'=>$customer_id)); die;}
		   $lab_category  = $this->input->post('lab_category');		  
		   for($i=0; $i<count($lab_category); $i++) 
			{	
			  $Qt = $this->input->post('quntity_'.$lab_category[$i]);
			  if($Qt=='')
				 {
					echo json_encode(array('success'=>false,'message'=>lang('medicine_quantity_not_empty'), 'person_id'=>$customer_id) );die;					 } 
			  $check_dosage = $this->medicinetest->exist_medicine_drugs_dosage( $lab_category[$i] ,$customer_id );
			  if(!$check_dosage) { echo json_encode(array('success'=>false,'message'=>lang('medicine_dosage_desciption_required'), 'person_id'=>$customer_id) );die;	}							}
			  
			for($i=0; $i<count($lab_category); $i++) 			{	
			  $quntity = $this->input->post('quntity_'.$lab_category[$i]);						  $person_data = array('lab_category_id'=>$lab_category[$i],'person_id'=>$customer_id,'added_by_employee_id'=>$login_uerData->person_id,'quantity'=>$quntity,'added_date'=>date('Y-m-d h:i:s'),'status'  =>'0' );			  $success = $this->db->insert('lab_medicine' ,$person_data);
			}	
			 if($success)
			 {  echo json_encode(array('success'=>true,'message'=>lang('medicine_successful_added'), 'person_id'=>$customer_id) );
			   //***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$customer_id,'activity_description'=>lang('medicine_successful_added'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
			 }			 }
		 else { echo json_encode(array('success'=>false,'message'=>lang('medicine_error_adding'), 'person_id'=>$customer_id));}	}
	function delete()
	{		
		$this->check_action_permission('delete');
		$to_delete=$this->input->post('ids');
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }		
		if($this->medicinetest->delete_list($to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('medicine_successful_deleted').' '.	count($to_delete).' '.lang('investigation_one_or_multiple')));		    //***save employee activites in database***//
			$activity_items_id = implode(',',$to_delete);
			$activity_description = lang('investigation_successful_deleted').' '.	count($to_delete).' '.lang('investigation_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('medicine_cannot_be_deleted')));		}
	}
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Customer->search_count_all($search);			$table_data = $this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		else
		{
			$config['total_rows'] = $this->Customer->count_all();
			$table_data = $this->Customer->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		$config['base_url'] = site_url('customers/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}	
	function search($customer_id=-1)
	{		
		$this->check_action_permission('search');
		 $search=$this->input->post('search');				$search_data=$this->medicinetest->search($customer_id,$search, $this->input->post('order_col') ,$this->input->post('order_dir'));				$pagination='';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));					}	
	function getservices($customer_id=-1)
	{	
		$search = addslashes($this->input->post('service'));
		$value = addslashes($this->input->post('checkedval')); 
		echo $search_data=$this->medicinetest->SearchLabCatgory($search,$value,$customer_id);die;   
	}	
		function activeDeActivate($customer_id=-1)
	{		
		$this->check_action_permission('search');
		$value = $this->uri->segment(4);
		$this->load->model('labtest');		
		$success=$this->labtest->ActiveDeActivateCatgory($value); 
		if($success==1)
		{ 	$message = lang('medicine_successful_updated');
			//***save employee activites in database***//
			$test2 = explode('-',$value); $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$activity_items_id,'activity_description'=>lang('medicine_successful_updated'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//	
		}
		else {  $message = lang('medicine_error_updated'); }
		echo $message; die;	
	}
	
	function suggest($customer_id=-1)
	{
		$suggestions = $this->medicinetest->get_search_suggestions($customer_id, $this->input->get('term'),100);
		echo json_encode($suggestions);
	}	
	function get_form_width()
	{			
		return 550;
	}		 function getMedicne_Drugs_Data($customer_id=-1)	 {			$search_data='';		$item_id = addslashes($this->input->post('services_id'));		
		$lab_name = $this->Customer->GetLabCatgory_name( $item_id);
		$ExecuteQuery = $this->db->query("SELECT * FROM ".$this->db->dbprefix('lab_category_dosage')." where item_id='".$item_id."' and person_id='".$customer_id."' and status='0' ");
		if($ExecuteQuery->num_rows()>0) 		{ 			$ExecuteData = $ExecuteQuery->row();			$search_data = json_encode(array('success'=>true,'description'=>$ExecuteData->dosage_description,'service_name'=>$lab_name)); 		}		else { $search_data = json_encode(array('success'=>true,'description'=>'','service_name'=>$lab_name)); } 				echo $search_data; 	 }	 	function addDosageDesciption($customer_id=-1)	 {		 $description = mysql_real_escape_string($this->input->post('description')); 		 $item_id  = $this->input->post('service_id');		 $result ='0';		 if($description!='' && $item_id!='')		 {	
            $existData = $this->medicinetest->exist_medicine_drugs_dosage( $item_id ,$customer_id );
            if($existData)
			 {					$result = $this->db->query("update ".$this->db->dbprefix('lab_category_dosage')." set dosage_description ='".$description."' where person_id='".$customer_id."' and item_id='".$item_id."' ");			 } 
			 else 
			 {				
				$result = $this->db->query("insert into ".$this->db->dbprefix('lab_category_dosage')." set item_id='".$item_id."', dosage_description ='".$description."',person_id='".$customer_id."' ");
			 }
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$item_id,'activity_description'=>lang('medicine_dosage_desciption_added_successful'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//		 }		 echo $result; die;			}
	
	function chanage_status()
	{	
		$this->check_action_permission('add_update');			
		$items_id = $this->input->post('items_id');
		$table_name='lab_medicine';
		if($items_id!='')
		{
			$item_data = $this->Customer->get_table_data_info($table_name,$items_id);
			if($item_data->quantity_dispansed=='1') { echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'item_id'=>$items_id)); die;}   
			$quantity_dispansed ='1';
			$item_data = array('quantity_dispansed'=>$quantity_dispansed);
			$this->db->where('id',$items_id);
			$result = $this->db->update($table_name,$item_data);		
		   if($result) 
		   { echo json_encode(array('success'=>true,'message'=>lang('common_change_status_successful'),'person_id'=>$items_id)); 
			 //***save employee activites in database***//
			 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$items_id,'activity_description'=>lang('common_change_status_successful'));	
			 $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			 //***save employee activites in database***//
		   }
		}	
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'item_id'=>$items_id));	}	 
	}

	function setPrint_Dosage($customer_id=-1)
	{	  
	   $items_id = $this->input->post('items_id');
	   if($customer_id==-1 && $items_id=-1) { redirect('customer');}	
	   $_SESSION['PRINTING_MEDICINE_LIST'] = $items_id;	
	   echo json_encode(array('success'=>true)); 	
	}
	
	function print_dosage($customer_id=-1)
	{	$item_ids=-1;
        if(isset($_SESSION['PRINTING_MEDICINE_LIST']) && $_SESSION['PRINTING_MEDICINE_LIST']!=''){ $item_ids =  $_SESSION['PRINTING_MEDICINE_LIST']; }
	    if($customer_id==-1 && $item_ids=-1) { redirect('customer');}	   
		$data['controller_name'] = strtolower(get_class());
		$data['person_id'] = $customer_id;
		$data['person_info']=$this->Customer->get_info($customer_id);	
		$data['manage_AllLabTest'] = $this->medicinetest->get_medicine_data_ByID($customer_id,$item_ids); 
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$customer_id,'activity_description'=>lang('medicine_dispensing_medicine_drugs_print_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//		
		$this->load->view("medicine/dispensing_print",$data);		
	}
	
	function drugs_profile($customer_id=-1)
	{
	    if($customer_id==-1){redirect(base_url());}
		
		$this->check_action_permission('drugs_profile');
	    $exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers');}
		$data['Diabetes_Customer_Type'] = $this->uri->segment(4);
		$config['base_url'] 	 	= site_url('customers/sorting');
		$data['controller_name'] 	= strtolower(get_class());
		$data['method_name']		= $this->router->fetch_method(); 
		$data['search_url'] 	 	= 'search_drugs_profile';
		$data['form_width'] 	 	= $this->get_form_width();
		$data['person_info']	    = $this->Customer->get_info($customer_id);	
		$data['medicine_Drugs_List']= $this->medicinetest->search_drugs_profile($customer_id , $search=''); 
		$data['manage_history_menu']= get_patient_history_menu($customer_id);		
		$data['per_page'] 			= '20';
		$data['list_title_name'] 	 = lang('medicine_drugs_profile');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$customer_id,'activity_description'=>lang('medicine_drugs_profile_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//	
		$this->load->view('medicine/manage',$data);
	}
	
	function search_drugs_profile($customer_id=-1)
	{		
		$this->check_action_permission('drugs_profile');
		$search=$this->input->post('search');		
		$search_data=$this->medicinetest->search_drugs_profile($customer_id,$search, $this->input->post('order_col') ,'desc');		
		$pagination='';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));				
	}

	function view_reason($customer_id=-1,$item_id=-1)
	{	
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		$data['reason_info'] = $this->savesetting->get_Table_Column_Data('lab_medicine','reason_cancel_drugs','id',$item_id);	
		$this->load->view("medicine/form_dosage_reason",$data);
	}	
	
	function dosage_management($customer_id=-1,$item_id=-1)
	{	
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		$data['customer_id']   = $customer_id;
		$data['medicine_id']   = $item_id;
		$data['person_info']   = $this->Customer->get_info($customer_id);
		$lab_category_id 	   = $this->savesetting->get_Table_Column_Data('lab_medicine','lab_category_id','id',$item_id);
		$data['drug_name']     = $this->savesetting->get_Table_Column_Data('lab_category','lab_name','id',$lab_category_id);
		$Dispensed_dosage      = $this->medicinetest->get_Dispensed_dosage($customer_id, $item_id);
		$medicine_data         = $this->savesetting->Get_Table_Row_Data('lab_medicine','id',$item_id);
		$Drugs_Quantity  	   = $this->medicinetest->get_Drugs_Quantity($customer_id, $item_id);
		if($Drugs_Quantity!=''){ $count_dosage = $Drugs_Quantity; }else { $count_dosage = $medicine_data->quantity; }
		
		$data['count_dosage']  = $count_dosage;
		$data['medicine_data'] = $medicine_data;
		$this->load->view("medicine/form_dosage_management",$data);
	}
	
	function save_medicine_history($customer_id=-1,$medicine_id=-1,$action_type=-1 )
	{
	  	 //$action_type = $this->input->post('action_type');
		 $dispensing_options = $this->input->post('dispensing_option'); 
		 $patient_status     = $this->input->post('patient_status'); 
		 $medicine_comment   = $this->input->post('medicine_comment'); 		 
		 $change_count    	 = $this->input->post('change_count'); 
		 $change_reason    	 = $this->input->post('change_reason'); 
	     $cancel_reason    	 = $this->input->post('cancel_reason');		 
		 $dispanse_count     = $this->input->post('dispanse_count'); 
	     $dispanse_reason    = $this->input->post('dispanse_reason');		 
		 $this->load->model('savesetting');
		 $login_uerData = $this->Employee->get_logged_in_employee_info();
		 
		 if($customer_id!=-1 && $medicine_id!=-1 && $action_type!='')
		 {   
            if($action_type=='dosage_comment') 
			{  
				$item_data = array('dispensing_options' => $dispensing_options,'patient_status' => $patient_status,'medicine_comment' => $medicine_comment); 
			    $succes = $this->savesetting->save_data('lab_medicine',$item_data,$medicine_id);
			}
			
			if($action_type=='change_count') 
			{  
				$item_data = array('medicine_id' => $medicine_id,'person_id' => $customer_id,'employee_id' => $login_uerData->person_id, 'quantity' => $change_count,'reason_change_quantity' => $change_reason); 
			    $succes = $this->savesetting->addTableData('lab_medicine_history',$item_data,$medicine_id);
			}
			
			if($action_type=='cancelDrugs') 
			{  
				$item_data = array('medicine_status' => '2','reason_cancel_drugs' => $cancel_reason,'employee_by_cancel'=>$login_uerData->person_id); 
			    $succes    = $this->savesetting->save_data('lab_medicine',$item_data,$medicine_id);
			}
			
			if($action_type=='change_dispanse') 
			{  
				$item_data = array('medicine_id' => $medicine_id,'person_id' => $customer_id,'employee_id' => $login_uerData->person_id, 'quantity' => $dispanse_count,'reason_dispanse_quantity' => $dispanse_reason); 
			    $succes = $this->savesetting->addTableData('lab_medicine_history',$item_data,$medicine_id);
			}
			
			if($succes)
			{
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'medicine','activity_type_category'=>'medicine_drugs','activity_items_id'=>$customer_id,'activity_description'=>lang('medicine_data_successful_added'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//	
				echo json_encode(array('success'=>true,'message'=>lang('medicine_data_successful_added'), 'person_id'=>$customer_id));die;
			}
			else
			{
				echo json_encode(array('success'=>false,'message'=>lang('medicine_data_error_adding'), 'person_id'=>$customer_id));die;
			}
		 } 
	     else { echo json_encode(array('success'=>false,'message'=>lang('medicine_required_data'), 'person_id'=>$customer_id) );die;}
	}	
	
}
?>