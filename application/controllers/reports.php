<?php
require_once ("secure_area.php");
class Reports extends Secure_area 
{	
	function __construct()
	{
		parent::__construct('reports');
		$this->load->helper('report');		
		$this->load->model('customer');
	}
		
	//Initial report listing screen
	function index()
	{
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'reports_information','activity_items_id'=>'','activity_description'=>lang('report_list_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/listing",array());	 
	}

	function _get_common_report_data()
	{
		$data = array();
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		$data['selected_month']=date('m');
		$data['selected_day']=date('d');
		$data['selected_year']=date('Y');	
		$data['input_label']='customerssummary';	
	
		return $data;
	}
	

	//Input for reports that require only a date range and an export to excel. (see routes.php to see that all summary reports route here)
	function date_input_excel_export()
	{
		$data = $this->_get_common_report_data();
		$this->load->view("reports/date_input_excel_export",$data);	
	}
	
	/** added for register log */
	function date_input_excel_export_register_log()
	{
		$data = $this->_get_common_report_data();
		$this->load->view("reports/date_input_excel_register_log.php",$data);	
	}
	
	/** also added for register log */
	
	function detailed_register_log($start_date = NULL, $end_date = NULL, $sale_type = NULL, $export_excel=0)
	{
		$this->load->model('reports/Detailed_register_log');
		$model = $this->Detailed_register_log;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		$overallSummaryData = array(
			'total_cash_sales'=>0,
			'total_shortages'=>0,
			'total_overages'=>0,
			'total_difference'=>0
		);
		
		foreach($report_data['summary'] as $row)
		{
			$summary_data[] = array(
				array('data'=>$row['first_name'] . ' ' . $row['last_name'], 'align'=>'left'), 
				array('data'=>date(get_date_format(), strtotime($row['shift_start'])) .' '.date(get_time_format(), strtotime($row['shift_start'])), 'align'=>'left'), 
				array('data'=>date(get_date_format(), strtotime($row['shift_end'])) .' '.date(get_time_format(), strtotime($row['shift_end'])), 'align'=>'left'), 
				array('data'=>to_currency($row['open_amount']), 'align'=>'right'), 
				array('data'=>to_currency($row['close_amount']), 'align'=>'right'), 
				array('data'=>to_currency($row['cash_sales_amount']), 'align'=>'right'),
				array('data'=>to_currency($row['difference']), 'align'=>'right')
			);
			
			$overallSummaryData['total_cash_sales'] += $row['cash_sales_amount'];
			if ($row['difference'] > 0) {
				$overallSummaryData['total_overages'] += $row['difference'];
			} else {
				$overallSummaryData['total_shortages'] += $row['difference'];
			}
			
			$overallSummaryData['total_difference'] += $row['difference'];
		}

		$data = array(
			"title" =>lang('reports_register_log_title'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $summary_data,
			"details_data" => array(),
			"summary_data" => $overallSummaryData,
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'register_log','activity_items_id'=>'','activity_description'=>lang('report_detailed_register_log_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular", $data);
	}
	
	//Summary sales report
	function summary_sales($start_date, $end_date, $sale_type, $export_excel=0,$sex='Male',$registration_type='all',$category_type='all',$age_min='-1',$age_max='-1')
	{	if($registration_type=='all') $registration_type=''; if($category_type=='all') $category_type=''; 
		if($sex=='all') $sex=''; if($age_min=='-1') $age_min='';if($age_max=='-1') $age_max=''; 
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'sex' => $sex, 'age_min' => $age_min, 'age_max' => $age_max, 'registration_type' => $registration_type, 'category_type' => $category_type ));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array( array('data'=>date(get_date_format(), strtotime($row['sale_date'])), 'align'=>'left'), 
									 array('data'=>to_currency($row['subtotal']), 'align'=>'right'), 
									 array('data'=>to_currency($row['total']), 'align'=>'right')									
								   );
		}

		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'sales','activity_items_id'=>'','activity_description'=>lang('report_summary_sales_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary categories report
	function summary_categories($start_date, $end_date, $sale_type, $export_excel=0,$sex='Male',$registration_type='all',$category_type='all',$age_min='-1',$age_max='-1')
	{
		if($registration_type=='all') $registration_type=''; if($category_type=='all') $category_type='';
		if($sex=='all') $sex=''; if($age_min=='-1') $age_min='';if($age_max=='-1') $age_max=''; 	
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['category'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
         //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'categories','activity_items_id'=>'','activity_description'=>lang('report_summary_categories_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary customers report
	function summary_customers($start_date, $end_date, $sale_type, $export_excel=0,$sex='Male',$registration_type='all',$category_type='all',$age_min='-1',$age_max='-1')
	{   
		//if($sex=='female') $sex='Female'; if($sex=='male') $sex='Male'; 
		if($registration_type=='all') $registration_type=''; if($category_type=='all') $category_type=''; 
		if($sex=='all') $sex=''; if($age_min=='-1') $age_min='';if($age_max=='-1') $age_max=''; 
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'sex' => $sex, 'age_min' => $age_min, 'age_max' => $age_max, 'registration_type' => $registration_type, 'category_type' => $category_type ));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'sex' => $sex));
		
		$tabular_data = array();
		$report_data = $model->getData($sex,$age_min,$age_max);
		 
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['customer'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
			
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'patients','activity_items_id'=>'','activity_description'=>lang('report_summary_customers_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	
	//Summary items report
	function summary_items($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align' => 'left'), array('data'=>$row['quantity_purchased'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_items_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'items','activity_items_id'=>'','activity_description'=>lang('report_summary_items_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary item kits report
	function summary_item_kits($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align' => 'left'), array('data'=>$row['quantity_purchased'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'), array('data'=>to_currency($row['total']), 'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
		
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary employees report
	function summary_employees($start_date, $end_date, $sale_type, $export_excel=0,$sex='all',$registration_type='all',$category_type='all',$age_min='-1',$age_max='-1')
	{	
		if($registration_type=='all') $registration_type=''; if($category_type=='all') $category_type=''; 
		if($sex=='all') $sex=''; if($age_min=='-1') $age_min='';if($age_max=='-1') $age_max=''; 
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'sex' => $sex, 'age_min' => $age_min, 'age_max' => $age_max, 'registration_type' => $registration_type, 'category_type' => $category_type ));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$tabular_data = array();
		$report_data = $model->getData();
		foreach($report_data as $row)
		{  
			$tabular_data[] = array(array('data'=>$row['employee'], 'align'=>'left'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align'=>'right'));
		}
		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'employees','activity_items_id'=>'','activity_description'=>lang('report_summary_employees_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary taxes report
	function summary_taxes($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['percent'], 'align'=>'left'), array('data'=>to_currency($row['tax']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'grades','activity_items_id'=>'','activity_description'=>lang('report_summary_taxes_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	//Summary discounts report
	function summary_discounts($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->library('sale_lib');
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));		
		$tabular_data = array();
		$report_data = $model->getData(); 
		//*************appliy Exampition code 14-12-2013*******************//	
		$sale_typeQuery=''; $Calculate_discount_Cost='0.00';
		$sales = $this->db->dbprefix('sales');
		$sales_items = $this->db->dbprefix('sales_items');
		$sales_payments = $this->db->dbprefix('sales_payments');
		if(isset($sale_type) && $sale_type!=''){
		if ($sale_type == 'sales')	{ $sale_typeQuery = " and si.quantity_purchased > 0 "; }
		if ($sale_type == 'returns') { $sale_typeQuery = " and si.quantity_purchased < 0 "; }
		}
            $subtotal = '0.00';  $total = '0.00';
			foreach($report_data as $row)
			{  
			    $ExecuteQuery = " select s.customer_id from ".$this->db->dbprefix('sales ')." s left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
					   join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
					   where s.deleted='0' and s.suspended='0' and si.discount_percent = '".$row['discount_percent']."' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' and i.supplier_id='".$row['supplier_id']."' group by s.customer_id";
				$count_patient = $this->db->query($ExecuteQuery)->num_rows();
				//die;
				$sale_typeQuery = " and si.quantity_purchased > 0 and i.supplier_id='".$row['supplier_id']."' and discount_percent = '".$row['discount_percent']."'"; 		
				$SalesQuery = "select s.sale_id,si.quantity_purchased,si.item_cost_price,si.item_unit_price,si.discount_percent from ".$this->db->dbprefix('sales')." s 
				left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
				join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
				join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
				join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
				where s.deleted='0' and s.suspended='0' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' $sale_typeQuery group by si.sale_id";
				$SalesQueryData = $this->db->query($SalesQuery);
				
				$Calculate_discount_Cost = 0; $total_tax = 0;				
				if($SalesQueryData->num_rows()>0) {  
				foreach($SalesQueryData->result() as $result)	
				{  
					foreach($this->sale_lib->get_taxes( $result->sale_id ) as $tax)	{ $total_tax +=$tax; }
					$Calculate_discount_Cost += (($result->item_unit_price * $result->quantity_purchased) * $result->discount_percent /100); 
				} }	
				//$subtotal += $row['subtotal']; //$total += $row['total'];
				$total_with_tax = $Calculate_discount_Cost + $total_tax;
				$subtotal 	   += $Calculate_discount_Cost; 
				$total   	   += $total_with_tax; 
				$tabular_data[] = array(array('data'=>$row['supplier'],'align'=>'left'),array('data'=>$row['discount_percent'],'align'=>'left'),array('data'=>$count_patient,'align'=>'left'),array('data'=>to_currency($total_with_tax),'align'=>'left'));
			}
		$getSummaryDataArray = array();
		$getSummaryDataArray['subtotal'] = $subtotal; 
		$getSummaryDataArray['total'] = $total; 
		$getSummaryData = $getSummaryDataArray; 
		//*******appliy Exampition code 14-12-2013**********//
		//*************Exemption Summary data code*********//
		$customer_All_Male   = $model->Exemption_Summary($start_date, $end_date , $sex_type = 'Male');
		$customer_All_Female = $model->Exemption_Summary($start_date, $end_date , $sex_type = 'Female');
		$customer_under_5    = $model->Exemption_Summary($start_date, $end_date , $sex_type = '', $age_range = 5);		
		$customer_above_60   = $model->Exemption_Summary($start_date, $end_date , $sex_type = '', $age_range = 60);  
		$customer_all_ages   = $model->Exemption_Summary($start_date, $end_date);  
		$customer_all_others = $customer_all_ages - ( $customer_under_5 + $customer_above_60 );
		if($customer_all_others < 0) { $customer_all_others = '0';}
		
		$data = array("title" 	 	 => lang('reports_discounts_summary_report'),
					  "subtitle" 	 => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
					  "headers"  	 => $model->getDataColumns(),
					  "data" 	 	 => $tabular_data,
					  "summary_data" => $getSummaryData,
					  "export_excel" => $export_excel,
					  "customer_All_Male" 	=> $customer_All_Male,
					  "customer_All_Female" => $customer_All_Female,
					  "customer_under_5" 	=> $customer_under_5,
					  "customer_above_60" 	=> $customer_above_60,
					  "customer_all_others" => $customer_all_others
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'exemption','activity_items_id'=>'','activity_description'=>lang('report_summary_discounts_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***// 		
		$this->load->view("reports/tabular",$data);
	}
	
	function summary_payments($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$tabular_data = array();
		$report_data = $model->getData();
		
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['payment_type'], 'align'=>'left'),array('data'=>to_currency($row['payment_amount']), 'align'=>'right'));
		}

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
         //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'payments','activity_items_id'=>'','activity_description'=>lang('report_summary_payments_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);
	}
	
	
	function graphical_summary_suppliers($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_suppliers_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'department','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_suppliers_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}

	//Summary suppliers report
	function summary_suppliers($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$tabular_data = array();
		$report_data = $model->getData();
		//print_r(		$report_data);
		foreach($report_data as $row)
		{
		  $ExecuteQuery = " select s.customer_id from ".$this->db->dbprefix('sales ')." s left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
					   join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
					   where s.deleted='0' and s.suspended='0' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' and i.supplier_id='".$row['supplier_id']."' and s.customer_id!='' group by s.customer_id";
		  $count_patient = $this->db->query($ExecuteQuery)->num_rows();
			$tabular_data[] = array(array('data'=>$row['supplier'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'),array('data'=>$count_patient,'align' =>'right'),array('data'=>to_currency($row['total']),'align' => 'right'));
		}

		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'department','activity_items_id'=>'','activity_description'=>lang('report_summary_suppliers_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***// 
		$this->load->view("reports/tabular",$data);
	}	
	
	function specific_supplier($start_date, $end_date, $supplier_id, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Specific_supplier');
		$model = $this->Specific_supplier;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'supplier_id' =>$supplier_id, 'sale_type' => $sale_type));
		
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'supplier_id' =>$supplier_id, 'sale_type' => $sale_type));
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		if($report_data!=''){
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));
				
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}
		}
		$supplier_info = $this->Supplier->get_info($supplier_id);
		$data = array(
					"title" => $supplier_info->first_name .' '. $supplier_info->last_name.' '.lang('reports_report'),
					"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
					"headers" => $model->getDataColumns(),
					"summary_data" => $summary_data,
					"details_data" => $details_data,
					"overall_summary_data" => $model->getSummaryData(),
					"export_excel" => $export_excel
		);
	    //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'department','activity_items_id'=>'','activity_description'=>lang('report_specific_supplier_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular_details",$data);
	}	
		
	
	//Input for reports that require only a date range. (see routes.php to see that all graphical summary reports route here)
	function date_input()
	{
		$data = $this->_get_common_report_data();
		$this->load->view("reports/date_input",$data);	
	}
	
	//Graphical summary sales report
	function graphical_summary_sales($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_sales_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'sales','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_sales_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_sales_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[strtotime($row['sale_date'])]= $row['total'];
		}

		$data = array(
			"title" => lang('reports_sales_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/line",$data);

	}
	
	//Graphical summary items report
	function graphical_summary_items($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_items_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_items_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'items','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_items_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_items_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_items');
		$model = $this->Summary_items;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['name']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_items_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}
	
	//Graphical summary item kits report
	function graphical_summary_item_kits($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_item_kits_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);

		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_item_kits_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_item_kits');
		$model = $this->Summary_item_kits;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['name']] = $row['total'];
		}

		$data = array(
			"title" => lang('reports_item_kits_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}
	
	//Graphical summary customers report
	function graphical_summary_categories($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_categories_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'categories','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_categories_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_categories_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_categories');
		$model = $this->Summary_categories;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['category']] = $row['total'];
		}
		
		$data = array(
			"title" => lang('reports_categories_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}
	
	
	//The actual graph data
	function graphical_summary_suppliers_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['supplier']] = $row['total'];
		}
		
		$data = array(
			"title" => lang('reports_suppliers_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}
	
	function graphical_summary_employees($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_employees_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'employees','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_employees_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_employees_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_employees');
		$model = $this->Summary_employees;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['employee']] = $row['total'];
		}
		
		$data = array(
			"title" => lang('reports_employees_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}
	
	function graphical_summary_taxes($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_taxes_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'grades','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_taxes_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_taxes_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_taxes');
		$model = $this->Summary_taxes;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['percent']] = $row['tax'];
		}
		
		$data = array(
			"title" => lang('reports_taxes_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}
	
	//Graphical summary customers report
	function graphical_summary_customers($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_customers_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'patients','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_customers_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_customers_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_customers');
		$model = $this->Summary_customers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['customer']] = $row['total'];
		}
		
		$data = array(
			"title" => lang('reports_customers_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/pie",$data);
	}
	
	//Graphical summary discounts report
	function graphical_summary_discounts($start_date, $end_date, $sale_type)
	{   echo 'Under Maintenance'; die; 
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$data = array(
			"title" => lang('reports_discounts_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_discounts_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'exemption','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_discounts_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_discounts_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		//echo 'here u r'; print_r($report_data); die;
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['discount_percent']] = $row['discount_percent'];
		}
		
		$data = array(
			"title" => lang('reports_discounts_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}
	
	function graphical_summary_payments($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"graph_file" => site_url("reports/graphical_summary_payments_graph/$start_date/$end_date/$sale_type"),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"summary_data" => $model->getSummaryData()
		);
         //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'payments','activity_items_id'=>'','activity_description'=>lang('report_graphical_summary_payments_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/graphical",$data);
	}
	
	//The actual graph data
	function graphical_summary_payments_graph($start_date, $end_date, $sale_type)
	{
		$this->load->model('reports/Summary_payments');
		$model = $this->Summary_payments;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$report_data = $model->getData();
		
		$graph_data = array();
		foreach($report_data as $row)
		{
			$graph_data[$row['payment_type']] = $row['payment_amount'];
		}
		
		$data = array(
			"title" => lang('reports_payments_summary_report'),
			"data" => $graph_data
		);

		$this->load->view("reports/graphs/bar",$data);
	}
	function specific_customer_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_customer');
		
		$customers = array();
		foreach($this->Customer->get_all()->result() as $customer)
		{
			$customers[$customer->person_id] = $customer->account_number .' - '.$customer->first_name .' '.$customer->last_name;
		}
		$data['specific_input_data'] = $customers;
		$this->load->view("reports/specific_input",$data);	
	}

	function specific_customer($start_date, $end_date, $customer_id=-1, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Specific_customer');
		$model = $this->Specific_customer;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'customer_id' =>$customer_id, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'customer_id' =>$customer_id, 'sale_type' => $sale_type));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));
			
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}
        $patient_nhif_details = '';
		$customer_info = $this->Customer->get_info($customer_id);
		if($customer_info->patient_category==4) { $patient_nhif_details = lang('customers_member_card_no').' : '.$customer_info->member_card_no. ' , ' .lang('customers_membership_no').' : '.$customer_info->membership_no .'<br>';}
		$data = array(
			"title" => $customer_info->first_name .' '. $customer_info->last_name.' ('.$customer_info->account_number.') ',
			"subtitle" => $patient_nhif_details.date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'patients','activity_items_id'=>'','activity_description'=>lang('report_specific_customer_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular_details",$data);
	}
	
	function specific_employee_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_employee');
		
		$employees = array();
		foreach($this->Employee->get_all()->result() as $employee)
		{
			$employees[$employee->person_id] = $employee->first_name .' '.$employee->last_name;
		}
		$data['specific_input_data'] = $employees;
		$this->load->view("reports/specific_input",$data);	
	}

	function specific_employee($start_date, $end_date, $employee_id, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Specific_employee');
		$model = $this->Specific_employee;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'employee_id' =>$employee_id, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'employee_id' =>$employee_id, 'sale_type' => $sale_type));
		$headers = $model->getDataColumns();
		$report_data = $model->getData();		
		$summary_data = array();
		$details_data = array();
		
		if($report_data!=''){
			foreach($report_data['summary'] as $key=>$row)
			{
				$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));
				
				foreach($report_data['details'][$key] as $drow)
				{
					$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
				}
			}
       }
		$employee_info = $this->Employee->get_info($employee_id);
		$data = array(
			"title" => $employee_info->first_name .' '. $employee_info->last_name.' '.lang('reports_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'employees','activity_items_id'=>'','activity_description'=>lang('report_specific_employee_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***// 
		$this->load->view("reports/tabular_details",$data);
	}
	     
	
	function detailed_sales($start_date, $end_date, $sale_type, $export_excel=0,$sex='Male',$registration_type='all',$category_type='all',$age_min='-1',$age_max='-1')
	{
		if($registration_type=='all') $registration_type=''; if($category_type=='all') $category_type=''; 
		if($sex=='all') $sex=''; if($age_min=='-1') $age_min='';if($age_max=='-1') $age_max=''; 
		$this->load->model('reports/Detailed_sales');
		$model = $this->Detailed_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'sex' => $sex, 'age_min' => $age_min, 'age_max' => $age_max, 'registration_type' => $registration_type, 'category_type' => $category_type ));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		foreach($report_data['summary'] as $key=>$row)
		{
		   if($row['items_purchased'] >0 || $row['items_purchased']=='0')
		   {
			  $summary_data[] = array(	array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=>'left'), 
										array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=>'left'), 
										array('data'=>$row['items_purchased'], 'align'=>'left'), 
										array('data'=>$row['employee_name'], 'align'=>'left'), 
										array('data'=>$row['customer_name'], 'align'=>'left'), 
										array('data'=>to_currency($row['subtotal']), 'align'=>'right'), 
										array('data'=>to_currency($row['total']), 'align'=>'right'), 
										//array('data'=>to_currency($row['tax']), 'align'=>'right'),
										//array('data'=>to_currency($row['profit']), 'align'=>'right'), 
										array('data'=>$row['payment_type'], 'align'=>'right'), 
										array('data'=>$row['comment'], 'align'=>'right')
									);
			}
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_number']) ? $drow['item_number'] : $drow['item_kit_number'], 'align'=>'left'), array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=>'left'), array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['serialnumber'], 'align'=>'left'), array('data'=>$drow['description'], 'align'=>'left'), array('data'=>$drow['quantity_purchased'], 'align'=>'left'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'), array('data'=>$drow['discount_percent'].'%', 'align'=>'left'));
			}
		}
		 $this->load->model('reports/transaction_summary');
		
		$data = array(
			"title" =>lang('reports_detailed_sales_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel,
			"EPAY_SummaryData" => $this->transaction_summary->getSummaryData($start_date,$end_date) 
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'sales','activity_items_id'=>'','activity_description'=>lang('report_detailed_sales_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular_details",$data);
	}
	
    function specific_supplier_input()
	{
		$data = $this->_get_common_report_data();
		$data['specific_input_name'] = lang('reports_supplier');

		$suppliers = array();
		foreach($this->Supplier->get_all()->result() as $supplier)
		{
			$suppliers[$supplier->person_id] = $supplier->first_name .' '.$supplier->last_name;
		}
		$data['specific_input_data'] = $suppliers;
		$this->load->view("reports/specific_input",$data);
	}
	
	function deleted_sales($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Deleted_sales');
		$model = $this->Deleted_sales;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=>'left'), array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=>'left'), array('data'=>$row['items_purchased'], 'align'=>'left'), array('data'=>$row['employee_name'], 'align'=>'left'), array('data'=>$row['customer_name'], 'align'=>'left'), array('data'=>to_currency($row['subtotal']), 'align'=>'right'), array('data'=>to_currency($row['total']), 'align'=>'right'), array('data'=>to_currency($row['tax']), 'align'=>'right'),array('data'=>to_currency($row['profit']), 'align'=>'right'), array('data'=>$row['payment_type'], 'align'=>'left'), array('data'=>$row['comment'], 'align'=>'left'));
			
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=>'left'), array('data'=>$drow['category'], 'align'=>'left'), array('data'=>$drow['serialnumber'], 'align'=>'left'), array('data'=>$drow['description'], 'align'=>'left'), array('data'=>$drow['quantity_purchased'], 'align'=>'left'), array('data'=>to_currency($drow['subtotal']), 'align'=>'right'), array('data'=>to_currency($drow['total']), 'align'=>'right'), array('data'=>to_currency($drow['tax']), 'align'=>'right'),array('data'=>to_currency($drow['profit']), 'align'=>'right'), array('data'=>$drow['discount_percent'].'%', 'align'=>'left'));
			}
		}

		$data = array(
			"title" =>lang('reports_deleted_sales_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'deleted_sales','activity_items_id'=>'','activity_description'=>lang('report_deleted_sales_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular_details",$data);
	}
	
	function detailed_receivings($start_date, $end_date, $sale_type, $export_excel=0)
	{
		$this->load->model('reports/Detailed_receivings');
		$model = $this->Detailed_receivings;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Receiving->create_receivings_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('receivings/edit/'.$row['receiving_id'], 'RECV '.$row['receiving_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format(), strtotime($row['receiving_date'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>$row['supplier_name'], 'align'=> 'left'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));
			
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>$drow['name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$data = array(
			"title" =>lang('reports_detailed_receivings_report'),
			"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'receivings','activity_items_id'=>'','activity_description'=>lang('report_detailed_receivings_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***/
		$this->load->view("reports/tabular_details",$data);
	}
			
	function excel_export()
	{
		$this->load->model('savesetting');
		$data['multi_stores_items'] = $this->savesetting->Get_Table_Category_Data('lab_category','multi_stores','lab_name');
		$this->load->view("reports/excel_export",$data);		
	}
	
	function inventory_low($export_excel=0,$store_id=-1)
	{   
		$this->load->model('reports/Inventory_low');
		$model = $this->Inventory_low;
		$model->setParams(array('store_id'=>$store_id));  
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>$row['description'], 'align'=> 'left'),array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align'=> 'right'), array('data'=>$row['quantity'], 'align'=> 'left'), array('data'=>$row['reorder_level'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_low_inventory_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);
		
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'inventory','activity_items_id'=>'','activity_description'=>lang('report_inventory_low_view'));	 
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***// 
		$this->load->view("reports/tabular",$data);	
	}
	
	function inventory_summary($export_excel=0,$store_id=-1)
	{
		$this->load->model('reports/Inventory_summary');
		$model = $this->Inventory_summary;
		$tabular_data = array();
		$model->setParams(array('store_id'=>$store_id));  
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>$row['description'], 'align'=> 'left'), array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align' => 'right'), array('data'=>$row['quantity'], 'align'=> 'left'), array('data'=>$row['reorder_level'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_inventory_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'inventory','activity_items_id'=>'','activity_description'=>lang('report_inventory_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);	
	}
	
	function summary_giftcards($export_excel = 0)
	{
		$this->load->model('reports/Summary_giftcards');
		$model = $this->Summary_giftcards;
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['giftcard_number'], 'align'=> 'left'),array('data'=>to_currency($row['value']), 'align'=> 'left'), array('data'=>$row['customer_name'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_giftcard_summary_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => $model->getSummaryData(array()),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'giftcards','activity_items_id'=>'','activity_description'=>lang('report_summary_giftcards_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***// 
		$this->load->view("reports/tabular",$data);			
	}
	
	function detailed_giftcards_input()
	{
		$data['specific_input_name'] = lang('reports_customer');
		
		$customers = array();
		foreach($this->Customer->get_all()->result() as $customer)
		{
			$customers[$customer->person_id] = $customer->first_name .' '.$customer->last_name;
		}
		$data['specific_input_data'] = $customers;
		$this->load->view("reports/detailed_giftcards_input",$data);	
	}
	
	function detailed_giftcards($customer_id, $export_excel = 0)
	{
		$this->load->model('reports/Detailed_giftcards');
		$model = $this->Detailed_giftcards;
		$model->setParams(array('customer_id' =>$customer_id));

		$this->Sale->create_sales_items_temp_table(array('customer_id' =>$customer_id));
		
		$headers = $model->getDataColumns();
		$report_data = $model->getData();
		
		$summary_data = array();
		$details_data = array();
		
		foreach($report_data['summary'] as $key=>$row)
		{
			$summary_data[] = array(array('data'=>anchor('sales/edit/'.$row['sale_id'], lang('common_edit').' '.$row['sale_id'], array('target' => '_blank')), 'align'=> 'left'), array('data'=>date(get_date_format().'-'.get_time_format(), strtotime($row['sale_time'])), 'align'=> 'left'), array('data'=>$row['items_purchased'], 'align'=> 'left'), array('data'=>$row['employee_name'], 'align'=> 'left'), array('data'=>to_currency($row['subtotal']), 'align'=> 'right'), array('data'=>to_currency($row['total']), 'align'=> 'right'), array('data'=>to_currency($row['tax']), 'align'=> 'right'),array('data'=>to_currency($row['profit']), 'align'=> 'right'), array('data'=>$row['payment_type'], 'align'=> 'left'), array('data'=>$row['comment'], 'align'=> 'left'));
			
			foreach($report_data['details'][$key] as $drow)
			{
				$details_data[$key][] = array(array('data'=>isset($drow['item_name']) ? $drow['item_name'] : $drow['item_kit_name'], 'align'=> 'left'), array('data'=>$drow['category'], 'align'=> 'left'), array('data'=>$drow['serialnumber'], 'align'=> 'left'), array('data'=>$drow['description'], 'align'=> 'left'), array('data'=>$drow['quantity_purchased'], 'align'=> 'left'), array('data'=>to_currency($drow['subtotal']), 'align'=> 'right'), array('data'=>to_currency($drow['total']), 'align'=> 'right'), array('data'=>to_currency($drow['tax']), 'align'=> 'right'),array('data'=>to_currency($drow['profit']), 'align'=> 'right'), array('data'=>$drow['discount_percent'].'%', 'align'=> 'left'));
			}
		}

		$customer_info = $this->Customer->get_info($customer_id);
		$data = array(
			"title" => $customer_info->first_name .' '. $customer_info->last_name.' '.lang('giftcards_giftcard'). ' '.lang('reports_report'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"summary_data" => $summary_data,
			"details_data" => $details_data,
			"overall_summary_data" => $model->getSummaryData(),
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'giftcards','activity_items_id'=>'','activity_description'=>lang('report_detailed_giftcards_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular_details",$data);
	}
	
	
	function expire_items_summary($export_excel=0)
	{
		$this->load->model('reports/expire_items_summary');
		$model = $this->expire_items_summary;
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align' => 'right'), array('data'=>$row['quantity'], 'align'=> 'left'), array('data'=>$row['reorder_level'], 'align'=> 'left') , array('data'=>$row['reorder_level'], 'align'=> 'left'), array('data'=>$row['items_expire_date'], 'align'=> 'left'));
		}

		$data = array(
			"title" => lang('reports_expire_items_summary'),
			"subtitle" => '',
			"headers" => $model->getDataColumns(),
			"data" => $tabular_data,
			"summary_data" => 'expire_items_reports',
			"export_excel" => $export_excel
		);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'expire_items','activity_items_id'=>'','activity_description'=>lang('report_expire_items_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/tabular",$data);	
	}
	
	
	//Summary suppliers report
	function mtuha_report($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{		
		$this->load->model('reports/Summary_mtuhareport');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();
		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$data['export_excel'] = $export_excel;
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];
		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		}   	
		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	

		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'mtuha_report','activity_items_id'=>'','activity_description'=>lang('report_mtuha_report_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//		
		$this->load->view("reports/summary_mtuha_report",$data);
	}
	
	
	//Summary Tika patents report
	function tika_patient_summary($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{		
		$this->load->model('reports/summary_tika_patient');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();		
		if($start_date=='' || $end_date=='') {  $start_date = $end_date = date('Y').'-'.date('m').'-'.date('d'); }		
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];
		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		} 
		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	
		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }	        
		$data['patient_data'] = $this->summary_tika_patient->getData($start_date,$end_date);	
		$data['reapeated_attendance'] = $this->summary_tika_patient->GetAllReapeated_Attendance_By_Date($start_date,$end_date);
		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'tika_patient','activity_items_id'=>'','activity_description'=>lang('report_tika_patient_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_tika_patient_report",$data);
	}
		
		
	//Summary IPD-IN Patents report
	function ipd_in_patient_summary($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{	
		$this->load->model('reports/summary_tika_patient');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days'] = get_days();
		$data['years'] = get_years();		
		if($start_date=='' || $end_date=='') {  $start_date = $end_date = date('Y').'-'.date('m').'-'.date('d'); }		

		$report_date_range_simple=''; $Execle_Patient_Data ='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];		} 		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	
		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }	 
        $this->load->model('ipd_in_patients');
		$this->load->model('reports/summary_ipd_in_patient');
		$data['Block_List'] = $this->ipd_in_patients->getIpdBlock_List();
		 
		$Block_type = $this->uri->segment(5);
		$report_type = $this->uri->segment(6);
		$export_excel = $this->uri->segment(7);	
		
		if($Block_type=='')  { $Block_type='showAll';}	
		if($report_type=='') { $report_type ='0'; }	
		if($export_excel=='') { $export_excel ='0'; }		
		if($export_excel=='1') { $Execle_Patient_Data = $this->summary_ipd_in_patient->get_Execle_Patient_Summary($start_date, $end_date, $Block_type, $report_type);}
		$data['Block_type']  = $Block_type;	
		$data['report_type'] = $report_type;		
		$data['table_data_array'] = $this->summary_ipd_in_patient->ipd_Patient_Summary($start_date, $end_date, $Block_type, $report_type);
		$data['export_excel'] = $export_excel;		
        $data['Execle_Patient_Data'] = $Execle_Patient_Data;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'ipd_in_patient','activity_items_id'=>'','activity_description'=>lang('report_ipd_in_patient_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_ipd_in_patient_report",$data);
	}
	
	//Summary Clinical Dagnoses Report
	function clinical_daignosis_report($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{		
		$this->load->model('reports/Summary_clinicaldaignosisreport');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();
		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$data['export_excel'] = $export_excel;
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		}   	
		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	
		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'mtuha_report','activity_items_id'=>'','activity_description'=>lang('report_clinical_daignosis_report_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_clinical_daignosis_report",$data);
	}
	
		//Summary Dead Body Report
	function dead_body_summary($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{		
		$this->load->model('reports/Summary_deadbodyreport');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();
		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$data['export_excel'] = $export_excel;
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];
		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		}   	
		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	
		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'dead_body','activity_items_id'=>'','activity_description'=>lang('report_dead_body_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_deadbody_report",$data);
	}
	
	//Summary Dead Body Report
	function diabetes_summary($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{		
		$this->load->model('reports/summary_diabetesreport');		
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();
		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$data['export_excel'] = $export_excel;
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];
		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		}   	
		
		$data['selected_month']  = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2'] = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;	
		foreach(get_months() as $key=>$value){ if($key==$selected_month) $data['Selected_Month_Name'] = $value; }
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$data['Reported_BY'] = $login_uerData->first_name. ' '.$login_uerData->last_name;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'diabetes','activity_items_id'=>'','activity_description'=>lang('report_diabetes_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_diabetes_report",$data);
	}	
	
	//Summary Doctor Performance report
	function doctor_parformance_summary($start_date=NULL, $end_date=NULL,$group_id=NULL,$sub_group_id=NULL)
	{
		$this->load->model('reports/Summary_doctor_parformance');
		$model = $this->Summary_doctor_parformance;
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();		
		if($start_date=='' && $end_date=='') {$start_date = $end_date = date('Y-m-d');}		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];
		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		} 
		$start_date = $start_date.' '.'00:00:00'; 
		$end_date   = $end_date.' '.'23:59:59';
		$tabular_data = array();
		$report_data = $model->getData($start_date,$end_date,$group_id,$sub_group_id);
		$Total_Appointment = '0';$Total_Cancel_Appointment = '0';$Total_Complete_Appointment = '0';
		foreach($report_data as $row)
		{   $total = $model->getTotal_Attend_appointment($row['employee_id'],$start_date,$end_date);
			$Total_Appointment += $total; 			
			$total_complete = $model->getTotal_Attend_appointment($row['employee_id'],$start_date,$end_date,1);
		    $Total_Complete_Appointment += $total_complete; 
			$total_cancel   = $model->getTotal_Attend_appointment($row['employee_id'],$start_date,$end_date,2);
			$Total_Cancel_Appointment += $total_cancel; 
			
			$tabular_data[] = array(array('data'=>$row['first_name'].' '.$row['last_name'], 'align' => 'left'), array('data'=>$total, 'align' => 'right'), array('data'=>$total_cancel, 'align' => 'right'), array('data'=>$total_complete, 'align' => 'right'));
		}       
		
		$data['headers'] 	  = $model->getDataColumns();
		$data['data'] 	 	  = $tabular_data;
		$data['Total_Appointment'] = $Total_Appointment;
		$data['Total_Cancel'] 	   = $Total_Cancel_Appointment;
		$data['Total_Complete']    = $Total_Appointment;
		$data['selected_month']    = $selected_month;	 $data['selected_day']  = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2']   = $selected_month2; $data['selected_day2'] = $selected_day2;	$data['selected_year2'] = $selected_year2;
		$data['selected_date_range'] = $report_date_range_simple;
		$data['GroupsData']	    =   $this->Employee->get_AllEmployee_Groups($parent_id=0); 
		$data['SubGroupsData']	=   $this->Employee->get_Sub_Groups(  $group_id,$sub_group_id ); 
		$data['group_id']		=   $group_id; 
		$data['sub_group_id']	=   $sub_group_id;
		
		$data['title'] = lang('reports_doctor_parformance_summary');	
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'reports_information','activity_items_id'=>'','activity_description'=>lang('report_doctor_parformance_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_doctor_parformance",$data);
	}
	
	//Summary Employee Performance report
	function employee_parformance_summary($start_date=NULL, $end_date=NULL,$export_excel=NULL)
	{  
		$this->load->model('reports/Summary_employee_parformance');
		$model = $this->Summary_employee_parformance;
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();		
		if($start_date=='' && $end_date=='') {$start_date = $end_date = date('Y-m-d');}		
		$selected_month = date('m');	$selected_day =  date('d');	$selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d');	$selected_year2 = date('Y');	
		$data['export_excel'] = $export_excel;
		$report_date_range_simple='';
		if(isset($start_date) && $start_date!='' && $end_date!='')
		{ $report_date_range_simple = $start_date.'/'.$end_date;   
		 $arr1 = explode('-',$start_date); 
		 $selected_year = $arr1[0];  $selected_month = $arr1[1];  $selected_day = $arr1[2];		
		 $arr2 = explode('-',$end_date);
		 $selected_year2 = $arr2[0];  $selected_month2 = $arr2[1];  $selected_day2 = $arr2[2];
		} 
		$start_date = $start_date.' '.'00:00:00'; 
		$end_date   = $end_date.' '.'23:59:59';
		$tabular_data = array();		
		$report_data = $model->getData($start_date,$end_date);		
		$Total_Lab_Reports = '0';$Total_USS_XRAYS_Reports = '0';$Total_EYECLINIC_Reports = '0';$Total_SPECILIST_Reports = '0';$Total_OTHERSERVICES_Reports = '0';
		
		foreach($report_data as $key=>$value)
		{   $employee_id = $value;
			$total = 0.00;
			$Lab_Reports = $model->GetTotal_Generate_Reports('lab_investigation',$employee_id,$start_date,$end_date);
			$Total_Lab_Reports += $Lab_Reports; 
			
			$Total_USS_XRAYS = $model->GetTotal_Generate_Reports('lab_uss_xrays',$employee_id,$start_date,$end_date);
			$Total_USS_XRAYS_Reports += $Total_USS_XRAYS; 

			$Total_EYECLINIC = $model->GetTotal_Generate_Reports('lab_eyeclinic',$employee_id,$start_date,$end_date);
			$Total_EYECLINIC_Reports += $Total_EYECLINIC; 
			
			$Total_SPECILIST = $model->GetTotal_Generate_Reports('lab_specialist',$employee_id,$start_date,$end_date);
			$Total_SPECILIST_Reports += $Total_SPECILIST;
			
			$Total_OTHERSERVICES = $model->GetTotal_Generate_Reports('lab_otherservices',$employee_id,$start_date,$end_date);
			$Total_OTHERSERVICES_Reports += $Total_OTHERSERVICES;
			$employee_info = $this->Employee->get_info($employee_id);
			$tabular_data[] = array(array('data'=>$employee_info->first_name.' '.$employee_info->last_name, 'align' => 'left'), array('data'=>$Lab_Reports, 'align' => 'center'), array('data'=>$Total_USS_XRAYS, 'align' => 'center'), array('data'=>$Total_EYECLINIC, 'align' => 'center'), array('data'=>$Total_SPECILIST, 'align' => 'center'), array('data'=>$Total_OTHERSERVICES, 'align' => 'center') );
		} 		
		$data['Total_Reports']    	   		= ($Total_Lab_Reports + $Total_USS_XRAYS_Reports + $Total_EYECLINIC_Reports + $Total_SPECILIST_Reports + $Total_OTHERSERVICES_Reports);
		$data['Total_Lab_Reports'] 	   	    = $Total_Lab_Reports;
		$data['Total_USS_XRAYS_Reports']    = $Total_USS_XRAYS_Reports;
		$data['Total_EYECLINIC_Reports'] 	= $Total_EYECLINIC_Reports;
		$data['Total_SPECILIST_Reports']    = $Total_SPECILIST_Reports;
		$data['Total_OTHERSERVICES_Reports']= $Total_OTHERSERVICES_Reports;
		$data['headers'] 	  	    = $model->getDataColumns();
		$data['data'] 	 	  	    = $tabular_data;
		$data['selected_month']     = $selected_month;	 $data['selected_day'] = $selected_day;	$data['selected_year']  = $selected_year;
		$data['selected_month2']    = $selected_month2; $data['selected_day2'] = $selected_day2;$data['selected_year2'] = $selected_year2;
		$data['selected_date_range']= $report_date_range_simple;	
		$data['title'] = lang('reports_employee_parformance_summary');	
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'reports_information','activity_items_id'=>'','activity_description'=>lang('report_employee_parformance_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/summary_employee_parformance",$data);
	}
		
	function epay_transactions_summary($start_date = NULL, $end_date = NULL,$export_excel = NULL)
	{	
	    $this->load->model('reports/transaction_summary');	
		$model = $this->transaction_summary;		
		$data['report_date_range_simple'] = get_simple_date_ranges();
        if(!isset($start_date) || ($start_date=='' && $end_date=='')) {	$start_date = $end_date = date('Y-m-d'); }
		$report_date_range_simple = $start_date.'/'.$end_date;  
		$data['selected_date_range']= $report_date_range_simple;		
		$data['start_date']  = $start_date;
		$data['end_date'] 	 = $end_date; 
		$data['export_excel']= $export_excel;
		$data['headers'] 	 = $model->getDataColumns();
		$data['ReportsData'] = $model->getData($start_date,$end_date);
		$data['SummaryData'] = $model->getSummaryData($start_date,$end_date);
		$data['controller_name'] 	 = strtolower(get_class());
		//print_r($data['ReportsData']); die;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'view_epay_transactions','activity_items_id'=>'','activity_description'=>lang('report_epay_transactions_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/epay_transactions/summary_epay_transactions_report",$data);
	}
	
	function epay_refund_transactions_summary($start_date = NULL, $end_date = NULL,$export_excel = NULL)
	{	
	    $this->load->model('reports/transaction_summary');	
		$model = $this->transaction_summary;		
		$data['report_date_range_simple'] = get_simple_date_ranges();
        if(!isset($start_date) || ($start_date=='' && $end_date=='')) {	$start_date = $end_date = date('Y-m-d'); }
		$report_date_range_simple = $start_date.'/'.$end_date;  
		$data['selected_date_range']= $report_date_range_simple;		
		$data['start_date']  = $start_date;
		$data['end_date'] 	 = $end_date; 
		$data['export_excel']= $export_excel;
		$data['headers'] 	 = $model->getDataColumns();
		$data['ReportsData'] = $model->getData($start_date,$end_date,'2');
		$data['SummaryData'] = $model->getSummaryData($start_date,$end_date,'2');
		$data['controller_name'] 	 = strtolower(get_class());
		//print_r($data['ReportsData']); die;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'view_epay_transactions','activity_items_id'=>'','activity_description'=>lang('report_epay_refund_transactions_summary_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("reports/epay_transactions/summary_epay_refund_transactions_report",$data);
	}
	
	function view_epay_transactions_summary($items_id=-1)
	{	
		$this->load->model('reports/transaction_summary');	
		$model = $this->transaction_summary;		
		$data['items_info']  = $model->get_info($items_id);
		//print_r($data['items_info']);
		$data['person_info'] = $this->Customer->get_info($data['items_info']->customer_id);  
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'transactions','activity_type_category'=>'view_epay_transactions','activity_items_id'=>$items_id,'activity_description'=>lang('report_epay_transactions_view_details'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//				
		$this->load->view("reports/epay_transactions/form",$data);
	}
			
}
?>

