<?php
require_once ("person_controller.php");
class Employees extends Person_controller
{
	function __construct()
	{
		parent::__construct('employees');
	}	
	function index()
	{
		$this->check_action_permission('search');
		$config['base_url'] = site_url('employees/sorting');
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_people_manage_table($this->Employee->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>'','activity_description'=>lang('employees_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		$this->load->view('people/manage',$data);
	}
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Employee->search_count_all($search);
			$table_data = $this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		else
		{
			$config['total_rows'] = $this->Employee->count_all();
			$table_data = $this->Employee->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		$config['base_url'] = site_url('employees/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	/* added for excel expert */
	function excel_export() {
		$data = $this->Employee->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("Employee PF NO","User Name", "First Name", "Last Name",'Group Name','Sub Group Name', "E-Mail", "Phone Number", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Comments");		$rows[] = $row;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>'','activity_description'=>lang('employees_export_successfull'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		foreach ($data as $r) {
		   $employee_group_name = $this->Employee->Get_Employee_GroupName($r->employee_group_id);
		    $employee_sub_group_name = $this->Employee->Get_Employee_SubGroupName($r->employee_group_id,$r->employee_sub_group_id);
		   $row = array($r->employee_account_code,
						$r->username,
						$r->first_name,
						$r->last_name,
						$employee_group_name,
						$employee_sub_group_name,
						$r->email,
						$r->phone_number,
						$r->address_1,
						$r->address_2,
						$r->city,
						$r->state,
						$r->zip,
						$r->country,
						$r->comments
			);
			$rows[] = $row;
		}
		$content = array_to_csv($rows);
		force_download('employees_export' . '.csv', $content);
		exit;
	}	
	/*Returns employee table data rows. This will be called with AJAX.*/
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;		$search_data=$this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		$config['base_url'] = site_url('employees/search');
		$config['total_rows'] = $this->Employee->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	/*Gives search suggestions based on what is being searched for*/
	function suggest($method_type=NULL)
	{
		$suggestions = $this->Employee->get_search_suggestions($this->input->get('term'),100,$method_type);
		echo json_encode($suggestions);
	}
	/*Loads the employee edit form*/
	function view($employee_id=-1)
	{
		$this->check_action_permission('add_update');
		$data['controller_name']=strtolower(get_class());
		$data['person_info']	=	$this->Employee->get_info($employee_id);
		$data['all_modules']	=	$this->Module->get_all_modules();		$data['person_info']	=	$this->Employee->get_info($employee_id);		$data['GroupsData']	    =   $this->Employee->get_AllEmployee_Groups($parent_id=0); 
		$data['SubGroupsData']	=   $this->Employee->get_Sub_Groups( $data['person_info']->employee_group_id , $data['person_info']->employee_sub_group_id ); 
		if($employee_id!=-1)
		{	//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>$employee_id,'activity_description'=>lang('employees_information_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//
		}
		$this->load->view("employees/form",$data);
	}
	/*Inserts/updates an employee*/
	function save($employee_id=-1)
	{    
		$this->check_action_permission('add_update');   
		if($this->input->post('email')!='')
		{  //echo 'here u r'; die;
			$isvalid_email = $this->Employee->Is_Exist_Email($this->input->post('email'),$employee_id);
			if($isvalid_email) {echo json_encode(array('success'=>false,'message'=>lang('common_email_address_exist').' '. $this->input->post('email'),'person_id'=>$employee_id));die;}
		}
		$person_data= array('first_name'=>$this->input->post('first_name'),
							'last_name'=>$this->input->post('last_name'),							'email'=>$this->input->post('email'),
							'phone_number'=>$this->input->post('phone_number'),
							'address_1'=>$this->input->post('address_1'),
							'address_2'=>$this->input->post('address_2'),
							'qualifications'=>$this->input->post('qualifications'),
							'city'=>$this->input->post('city'),
							'state'=>$this->input->post('state'),
							'zip'=>$this->input->post('zip'),
							'country'=>$this->input->post('country'),
							'comments'=>$this->input->post('comments')
							);
		$permission_data = $this->input->post("permissions")!=false ? $this->input->post("permissions"):array();
		$permission_action_data = $this->input->post("permissions_actions")!=false ? $this->input->post("permissions_actions"):array();
		//Password has been changed OR first time password set
		if($this->input->post('password')!='')
		{		   $employee_data=array('username'			   => $this->input->post('username'),
								'password'			   => md5($this->input->post('password')),								'employee_group_id'    => $this->input->post('group_name'),
								'employee_sub_group_id'=> $this->input->post('sub_group_name'),
								'change_password_date' => date('Y-m-d H:i:s')								);
		}
		else //Password not changed
		{
			$employee_data=array('username'=>$this->input->post('username'),'employee_group_id'=>$this->input->post('group_name'),'employee_sub_group_id'=>$this->input->post('sub_group_name'));
		}
		
		if($employee_id==-1)
		{  $employee_code = $this->Employee->Generate_Employee_Account();
		   $registration_array = array('registration_date'=>date('Y-m-d'));
		   $person_data = array_merge($person_data,$registration_array); 
		   $temp_employee_data = array('employee_account_code'=>$employee_code);
		   $employee_data = array_merge($employee_data,$temp_employee_data); 
		}
		if($this->Employee->save($person_data,$employee_data,$permission_data, $permission_action_data, $employee_id))
		{
			//New employee
			if($employee_id==-1)
			{	echo json_encode(array('success'=>true,'message'=>lang('employees_successful_adding').' '. $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_data['person_id']));
			    $employee_id = $employee_data['person_id'];
				$employee_info = $this->Employee->get_info($employee_data['person_id']);
				$activity_description = lang('employees_successful_adding').' '. $person_data['first_name'].' '.$person_data['last_name']; 
			}			else //previous employee			{	$employee_info = $this->Employee->get_info($employee_id);
				echo json_encode(array('success'=>true,'message'=>lang('employees_successful_updating').' '. $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$employee_id));
			    $activity_description = lang('employees_successful_updating').' '. $person_data['first_name'].' '.$person_data['last_name']; 
			}
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>$employee_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//				
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('employees_error_adding_updating').' '.	$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}	}
	/*	This deletes employees from the employees table	*/
	function delete()
	{
		$this->check_action_permission('delete');
		$employees_to_delete=$this->input->post('ids');
		if (($_SERVER['HTTP_HOST'] == 'demo.4pay.com' || $_SERVER['HTTP_HOST'] == 'demo.4pay.com') && in_array(1,$employees_to_delete))
		{
			//failure
			echo json_encode(array('success'=>false,'message'=>lang('employees_error_deleting_demo_admin')));
		}		elseif($this->Employee->delete_list($employees_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('employees_successful_deleted').' '.
			count($employees_to_delete).' '.lang('employees_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('employees_cannot_be_deleted')));
		}
	}	function get_row()	{		$person_id = $this->input->post('row_id');		$data_row=get_person_data_row($this->Employee->get_info($person_id),$this);		echo $data_row;	}
	
	function get_SubGroupList()
	{
	    $SubGroupsData='';
		$group_name = $this->input->post('group_name');
		$sub_group_name = $this->input->post('sub_group_name');
		if($group_name==''){ echo json_encode(array('success'=>false,'groupData'=>$SubGroupsData));	die;}
		if($group_name!='')
		{
			$SubGroupsData = $this->Employee->get_Sub_Groups( $group_name, $sub_group_name ); 
			echo json_encode(array('success'=>true,'groupData'=>$SubGroupsData));
		}		
	}	
	
	function history_activity($employee_id=-1) 
	{   
		$this->check_action_permission('search');
		if($employee_id==-1) { redirect('employees');}
		$_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'] = $employee_id;	
        unset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']);  unset($_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']);		
		$config['base_url'] = site_url('employees/sorting_activity');
		$config['total_rows'] = $this->Employee->count_all_activities();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['method_type'] = 'history_activity';
		$data['employee_id'] = $employee_id;
		$data['manage_table']=get_history_activity_manage_table($this->Employee->get_all_activities($data['per_page']),$this);
		$this->load->view('employees/manage_history_activity',$data);
	}

	function sorting_activity() 
	{
		$this->check_action_permission('search');
		$employee_id = $_SESSION['SEARCH_HISTORY_ACTIVITY_MEMBER_ID'];  
		if($employee_id==-1) { redirect('employees');}
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Employee->search_count_all_activities($search);
			$table_data = $this->Employee->search_activities($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'added_date' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->Employee->count_all_activities();
			$table_data = $this->Employee->get_all_activities($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'added_date' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('employees/sorting_activity');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_history_activity_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}

	function search_activities($method_type=NULL)
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		if(isset($_POST['search_start_date']) && $_POST['search_start_date']!='' && $_POST['search_end_date']!='')
		{
		   $_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE'] = date('Y-m-d',strtotime($this->input->post('search_start_date'))).' 00:00:00';
		   $_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']   = date('Y-m-d',strtotime($this->input->post('search_end_date'))).' 23:59:59';
		}
		else { unset($_SESSION['SEARCH_HISTORY_ACTIVITY_START_DATE']);  unset($_SESSION['SEARCH_HISTORY_ACTIVITY_END_DATE']); }
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->Employee->search_activities($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'added_date' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('employees/search_activities');
		$config['total_rows'] = $this->Employee->search_count_all_activities($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		if($method_type=='search_by_date') { $data['manage_table']=get_history_activity_manage_table($search_data,$this);}
		else { $data['manage_table']=get_history_activity_table_data_rows($search_data,$this); }
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function online()
	{
		$this->check_action_permission('employees_chat');
		$config['base_url'] = site_url('employees/online_sorting');
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_online_employee_manage_table($this->Employee->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>'','activity_description'=>lang('employees_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		$this->load->view('people/manage',$data);
	}

	function online_sorting()
	{
		$this->check_action_permission('employees_chat');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Employee->search_count_all($search);
			$table_data = $this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->Employee->count_all();
			$table_data = $this->Employee->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url'] = site_url('employees/online_sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_online_employee_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function search_online()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('employees/search');
		$config['total_rows'] = $this->Employee->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_online_employee_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function stock_notfications()
	{
		$this->check_action_permission('employees_chat');
		$config['base_url'] = site_url('employees/stock_notfications_sorting');
		$config['total_rows'] = $this->Employee->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_stock_notfications_employee_manage_table($this->Employee->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>'','activity_description'=>lang('employees_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		$this->load->view('people/manage_stock_notfications',$data);
	}

	function stock_notfications_sorting()
	{
		$this->check_action_permission('employees_chat');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Employee->search_count_all($search);
			$table_data = $this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->Employee->count_all();
			$table_data = $this->Employee->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url'] = site_url('employees/stock_notfications_sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_stock_notfications_employee_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function search_stock_notfications()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->Employee->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('employees/stock_notfications_sorting');
		$config['total_rows'] = $this->Employee->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_stock_notfications_employee_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}	
	
	function chanage_status()
	{	
		$this->check_action_permission('add_update');			
		$person_id = $this->input->post('person_id');
		$method_type = $this->input->post('method_type');
		$activity_description = lang('common_change_status_successful');	
		if($person_id!='' && $method_type!='')
		{   $this->load->model('savesetting');
			if($this->savesetting->Change_Status($person_id,$method_type)) 
			{ if($method_type=='stock_notfications') { $activity_description = lang('common_notification_status_successful'); } 
			  //***save employee activites in database***//
			   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>$person_id,'activity_description'=>$activity_description);	
			   $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			  //***save employee activites in database***//
			  echo json_encode(array('success'=>true,'message'=>$activity_description,'item_id'=>$person_id)); 
			}
		}	
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'person_id'=>$person_id));	}	 
	}
	
	function search_employee()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->Employee->Search_Employee_List($search_keyword); die;
	}
	
	
	function change_password()
	{
		$this->check_action_permission('change_password'); 
		$data['controller_name']=strtolower(get_class());
		$this->load->view('employees/change_password',$data);
	}
	
	function password_setting()
	{   $this->check_action_permission('change_password'); 
		$current_password  = $this->input->post('current_password');
		$user_password 	   = $this->input->post('user_password');
		$confirm_password  = $this->input->post('user_confirm_password');
		if($current_password == '' || $user_password == '' || $confirm_password == '') { echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'expire_message'=>'','item_id'=>-1)); die;}
		if($user_password != $confirm_password) { echo json_encode(array('success'=>false,'message'=>lang('employees_both_password_not_same'),'expire_message'=>'','item_id'=>-1)); die;}
		$employee_id = $this->Employee->get_Logged_SessionData('person_id');
		$isvalid = $this->Employee->is_valid_Pass( md5($current_password),$employee_id);
		if(!$isvalid) { echo json_encode(array('success'=>false,'message'=>lang('employees_invalid_current_password'),'expire_message'=>'','item_id'=>-1)); die;}
		$person_data = array('password'=>md5($user_password),'change_password_date'=>date('Y-m-d H:i:s'));
		$this->load->model('savesetting');
		$result = $this->savesetting->saveUpdateTable('employees', $person_data, 'person_id', $employee_id);
		if($result) 
		{ 
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'employees','activity_type_category'=>'employee_information','activity_items_id'=>'','activity_description'=>lang('employees_change_password_successfull'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>lang('employees_change_password_successfull'),'expire_message'=>$this->Employee->password_expire_message(),'item_id'=>-1));		
		}
		else {echo json_encode(array('success'=>false,'message'=>lang('employees_change_password_error'),'expire_message'=>'','item_id'=>-1));}		
	}
	
	function get_form_width()
	{
		return 880;
	}
}
?>