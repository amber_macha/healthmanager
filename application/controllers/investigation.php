<?php
require_once ("person_controller.php");
class Investigation extends Person_controller
{
	function __construct()
	{
		parent::__construct('investigation');
		$this->load->model('patienthistory');
		$this->load->model('labtest');
	}		
	function index($customer_id=-1)
	{
	   if($customer_id==-1){redirect(base_url());}	
		$this->check_action_permission('search');		$exists = $this->Customer->exists($customer_id); 
		if(!$exists) {redirect('customers');}
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = '';
		$data['controller_name'] = strtolower(get_class());
		$data['form_width'] = $this->get_form_width();
		$data['person_info']=$this->Customer->get_info($customer_id);
		$data['manage_AllLabTest'] = $this->labtest->search($customer_id,$search=''); 
		$data['manage_history_menu']=get_patient_history_menu($customer_id);	
		$data['per_page'] = '20';
		$this->load->view('investigation/manage',$data);
	}	
	function view($customer_id=-1)
	{
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$data['person_id'] = $customer_id;		
		$data['lab_category'] = $this->labtest->SearchLabCatgory($search=null,$value=null,$customer_id);		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'lab_services','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_lab_service'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("investigation/form",$data);
	}
	
	function addreport($customer_id=-1,$invest_id=-1)
	{	
		if($customer_id==-1 || $invest_id==-1){redirect(base_url());}   
		$this->check_action_permission('action_P_R');
		$data['person_id'] = $customer_id;	
		$data['invest_id'] = $invest_id;
		$this->load->view("investigation/addreport",$data);
	}	
	function savereport($customer_id=-1,$invest_id=-1)
	{	$upload_report_image ='';
		if($customer_id==-1 || $invest_id==-1){redirect(base_url());}	
		$this->check_action_permission('add_update');
		$login_uerData  = $this->Employee->get_logged_in_employee_info();
		$report_details = addslashes($this->input->post('investigation_report'));
		$report_image   =  $_FILES["report_image"]["name"];
		
		$default_file_extension = $this->config->item('DEFAULT_USS_XRAYS_IMAGE_TYPE');
		if($_FILES['report_image']['name']!='')
		{   $dir_dest = 'images/patient_report_images/investigation_reports/'.$customer_id.'/'.$invest_id.'/';
		    if (!file_exists($dir_dest)) {  mkdir($dir_dest, 0777, true); }
			$ext = $this->Customer->findExtension($_FILES['report_image']['name']);
			if($ext == 'jpg' || $ext == 'png' || $ext == 'gif' || $ext == 'jpeg')
			{
				$fileNameNoExtension = preg_replace("/\.[^.]+$/", "", basename($report_image));
				$uploadFileName = $customer_id.'_'.$invest_id.'_'.time().'_'.$fileNameNoExtension.".".$default_file_extension;
				$path = $dir_dest .$uploadFileName;
				move_uploaded_file($_FILES["report_image"]["tmp_name"],$path);
				$upload_report_image = $uploadFileName;
			}
			else { echo json_encode(array('success'=>false,'message'=>lang('common_patient_image_type_not_allowed'), 'person_id'=>$customer_id)); die;}
		}
		
		if(isset($report_details) && $report_details!='')
		{  //$success='1';
		   $success= $this->db->query("update ".$this->db->dbprefix('lab_investigation')." set test_report_details='".$report_details."',lab_report_images='".$upload_report_image."', tested_by_employee_id = '".$login_uerData->person_id."', tested_date='".date('Y-m-d h:i:s')."',status='1' where id='".$invest_id."' and person_id='".$customer_id."' and status='0' ");		   if($success)
		   { echo json_encode(array('success'=>true,'message'=>lang('investigation_report_successful_added'), 'person_id'=>$customer_id) );
			  //***save employee activites in database***//
			  $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'result_PR','activity_items_id'=>$invest_id,'activity_description'=>lang('investigation_report_successful_added'));	
			  $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			  //***save employee activites in database***//
		  }
		}
		else { echo json_encode(array('success'=>false,'message'=>lang('investigation_report_error_added'), 'person_id'=>$customer_id) );}	
	}
	function viewreport($customer_id=-1,$invest_id=-1)
	{	
	    if($customer_id==-1 || $invest_id==-1){redirect(base_url());} 
		$this->check_action_permission('action_V_R');
		$search='';
		$data['person_id'] = $customer_id;	 
		$data['invest_id'] = $invest_id;			
		$data['reportdata'] = $this->labtest->get_Report($customer_id,$invest_id);			$data['diseases_list'] = $this->Customer->GetDiseasList($customer_id,$invest_id,$LabTestType='investigation',$search);
		$data['diabetes_list'] = $this->Customer->GetDiabetesList($customer_id,$invest_id,$LabTestType='investigation',$search);				//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'result_VR','activity_items_id'=>$invest_id,'activity_description'=>lang('common_view_patient_result_pr'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//			
		$this->load->view("investigation/viewreport",$data);
	} 

	function view3dmotion($customer_id=-1,$item_id=-1)
	{	
		$this->check_action_permission('add_update');
		$total_report_images='';
		if($customer_id!=-1 && $item_id!=-1)
		{ 
		    if(!$this->Customer->exists($customer_id)) { redirect('home');}
			$reportdata  = $this->labtest->get_Report($customer_id,$item_id);
            if(isset($reportdata->lab_report_images) && $reportdata->lab_report_images!='') 
			{
			    $report_images_path = base_url().'images/patient_report_images/investigation_reports/'.$customer_id.'/'.$item_id; 
			}			
			else { redirect('investigation/index/'.$customer_id); }
			
			$data['report_images_path'] = $report_images_path;	
			$data['person_id'] = $customer_id;	 
			$data['item_id']   = $item_id;			
			$this->load->view("partial/view3dmotion",$data);
	    }
       else { redirect('home'); } 	  
	}     	
	function save($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$lab_investigation = $this->db->dbprefix('lab_investigation');				if($customer_id=='-1' || $this->input->post('lab_category')=='')
		 { 
		    echo json_encode(array('success'=>false,'message'=>lang('investigation_error_adding'), 'person_id'=>$customer_id));
		 }
		if($customer_id!='-1' && $this->input->post('lab_category')!='')
		 {  //$lab_category  = implode(',',$this->input->post('lab_category'));
			//$valid = $this->labtest->valid_Active_Lab_category($lab_category);
			//if($valid) { echo json_encode(array('success'=>false,'message'=>lang('lab_services_not_selected'), 'person_id'=>$customer_id)); die;}
		    $lab_category  = $this->input->post('lab_category');
			for($i=0; $i<count($lab_category); $i++) 
			{	
			 $person_data = array('lab_category_id'=>$lab_category[$i],'person_id'=>$customer_id,'added_by_employee_id'=>$login_uerData->person_id,'added_date'=>date('Y-m-d h:i:s'),'status'  =>'0'	);			 $success = $this->db->insert('lab_investigation' ,$person_data);
			}	
			if($success)
			{ 
			   echo json_encode(array('success'=>true,'message'=>lang('investigation_successful_added'), 'person_id'=>$customer_id) );
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'lab_services','activity_items_id'=>$customer_id,'activity_description'=>lang('investigation_successful_added'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
			}	
		 }
		 else { echo json_encode(array('success'=>false,'message'=>lang('investigation_error_adding'), 'person_id'=>$customer_id));}
	}
	function delete()
	{
		$this->check_action_permission('delete');
		$to_delete=$this->input->post('ids');
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		if($this->labtest->delete_list($to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('investigation_successful_deleted').' '.	count($to_delete).' '.lang('investigation_one_or_multiple')));			//***save employee activites in database***//
			$activity_items_id = implode(',',$to_delete);
			$activity_description = lang('investigation_successful_deleted').' '.	count($to_delete).' '.lang('investigation_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'lab_services','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('investigation_cannot_be_deleted')));		}
	}
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;		if ($search)
		{
			$config['total_rows'] = $this->Customer->search_count_all($search);			$table_data = $this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		else
		{
			$config['total_rows'] = $this->Customer->count_all();			$table_data = $this->Customer->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}
		$config['base_url'] = site_url('customers/sorting');		$config['per_page'] = $per_page; 		$this->pagination->initialize($config);		$data['pagination'] = $this->pagination->create_links();		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));				}	
	function search($customer_id=-1)	{	
		$this->check_action_permission('search');		$search=$this->input->post('search');				$search_data=$this->labtest->search($customer_id,$search, $this->input->post('order_col') ,$this->input->post('order_dir'));				$pagination='';		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));					}	
	function getservices($customer_id=-1)
	{	
		$search = addslashes($this->input->post('service')); 
		$value = addslashes($this->input->post('checkedval'));
		echo $search_data=$this->labtest->SearchLabCatgory($search,$value,$customer_id);die;  
	}
	   	function activeDeActivate($customer_id=-1)
	{
		$this->check_action_permission('search');
		$value = $this->uri->segment(4);	
		$success=$this->labtest->ActiveDeActivateCatgory($value); 
		if($success==1)
		{  
			$message = lang('lab_services_successful_updated'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value); $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'lab_services','activity_items_id'=>$activity_items_id,'activity_description'=>lang('lab_services_successful_updated'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//	
		}
		else {  $message = lang('lab_services_error_updated'); }
		echo $message; die;	
	}
	
   function suggest($customer_id=-1)
	{
		$suggestions = $this->labtest->get_search_suggestions($customer_id,$this->input->get('term'),100);
		echo json_encode($suggestions);
	}	
	function get_form_width(){ return 550;	}			function searchDiseasList($customer_id=-1)	 {			$searchtype = addslashes($this->input->post('searchtype')); 
		$search = addslashes($this->input->post('service'));		$value = addslashes($this->input->post('checkedval'));
		$invest_id = addslashes($this->input->post('invest_id'));
		if($searchtype == 'mtuha_daignoses') { $search_data = $this->Customer->GetDiseasList($customer_id,$invest_id,$LabTestType='investigation',$search);}
		if($searchtype == 'daibetes')		 { $search_data = $this->Customer->GetDiabetesList($customer_id,$invest_id,$LabTestType='investigation',$search); }
		echo $search_data; die;	 }		function saveDiseases($customer_id=-1)	{			$this->check_action_permission('search');
		$savetype = $this->uri->segment(4);		$invest_id = $this->uri->segment(5);
		$value = $this->uri->segment(6);				$success=$this->labtest->saveDiseases($customer_id,$invest_id, $value,$savetype);		if($success==1){  $message = lang('diseases_services_successful_added'); }		else {  $message = lang('diseases_services_error_added'); }		echo $message; die;		}
		//*********START DIAGNOSES CODE************************************//	function diseasesForm($customer_id=-1)	{		if($customer_id==-1 ){redirect(base_url());} 		$this->check_action_permission('add_update');		$search='';		$data['person_id'] = $customer_id;				$data['diseases_list'] = $this->labtest->GetSearchAllDiseasList($search);	
		$this->load->model('medicinetest');
		$data['medicine_list'] = $this->medicinetest->Search_MedicneDrugs_Catgory($search='',$value='');		$this->load->view("investigation/mthuha_diabetes_viewform",$data);	}		
	function searchAllDiseasList($customer_id=-1)
	 {	
		$search = addslashes($this->input->post('service')); 
		echo $search_data=$this->labtest->GetSearchAllDiseasList($search);die;
	 }
	 
	//*********START DAIBETES CODE************************************//
	function DiabetesForm($customer_id=-1)
	{
		if($customer_id==-1 ){redirect(base_url());} 
		$this->check_action_permission('add_update');
		$search='';
		$data['person_id'] = $customer_id;		
		$data['diseases_list'] = $this->labtest->GetSearchAllDiabetes_CareList($search);
		$this->load->model('medicinetest');
		$data['medicine_list'] = $this->medicinetest->Search_MedicneDrugs_Catgory($search='',$value='');		
		$this->load->view("investigation/diabetes_viewform",$data);
	}	
		function searchAllDiabetes_CareList($customer_id=-1)	 {			$search = addslashes($this->input->post('service')); 		echo $search_data=$this->labtest->GetSearchAllDiabetes_CareList($search);die;	 }	 	 function addnewDiagnoses($customer_id=-1)	 {		    $this->check_action_permission('add_update');		// $type = $this->uri->segment(3);		 $service_id = $this->uri->segment(3);				 $servicename 	 = addslashes($this->input->post('servicename')); 		 $codeNumber 	 = addslashes($this->input->post('codeNumber'));
		 $category_items = $this->input->post('category_items');
		 $category_type  = addslashes($this->input->post('category_type'));
		 		 $message ='';		 if($servicename!='' && $codeNumber!='')		 {		   $valid = $this->labtest->existsDiagnoses($servicename, $codeNumber,$service_id,$category_type);			if($valid=='no') { $message = '0'; }			if($valid=='yes')			 {     
				if($category_type=='mtuha_daignoses')   { $category = 'diseases'; $activity_type_category = 'mtuha_daignoses'; }
				if($category_type=='clinical_daignoses'){ $category = 'clinic_diseases'; $activity_type_category = 'clinical_daignoses';}
				if($category_type=='daibetes')		    { $category = 'diabetes'; $activity_type_category = 'daibetes';}
				
			      $lab_category = $this->db->dbprefix('lab_category');				  if(isset($service_id) && $service_id!=''){ 			  					$result = $this->db->query("update $lab_category set lab_name ='".$servicename."',code_number ='".$codeNumber."',related_item_id ='".$category_items."' where category_type='".$category."' and id = '".$service_id."' ");					$activity_items_id = $service_id;
					if($category_type=='mtuha_daignoses'){ $activity_description = lang('common_mtuha_daignoses_updated'); }
					if($category_type=='clinical_daignoses'){ $activity_description = lang('common_clinical_daignoses_updated');}
					if($category_type=='daibetes')		 { $activity_description = lang('common_daibetes_updated');}	
					
				  }				  else {					$result = $this->db->query("insert into $lab_category  set lab_name ='".$servicename."',code_number ='".$codeNumber."',related_item_id ='".$category_items."',category_type='".$category."',status='0' ");					$activity_items_id = $this->db->insert_id();
					if($category_type=='mtuha_daignoses'){ $activity_description = lang('common_mtuha_daignoses_added'); }
					if($category_type=='clinical_daignoses'){ $activity_description = lang('common_clinical_daignoses_added');}
					if($category_type=='daibetes')		 { $activity_description = lang('common_daibetes_added');}
				  }			    if($result) { $message = '1'; }  else { $message = '2'; }
				//***save employee activites in database***//				
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>$activity_type_category,'activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//  			 }		 } 		 echo $message; die;			 }	 	 function getDiseasData_byid($customer_id=-1)	 {			$services_id = addslashes($this->input->post('services_id'));
		$category_type = addslashes($this->input->post('category_type'));		echo $search_data=$this->labtest->GetDiseasData_BYID($services_id,$category_type);die;	 }	 	 	 function deleteAllDiseasList($customer_id=-1)	  {				//$this->check_action_permission('search');		$message='';		$category_type = $this->uri->segment(3);
		$value = $this->uri->segment(4);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);			$success=$this->labtest->deleteAllDiagnoses($value,$category_type);			if($success==1)
		{  $message = lang('lab_services_successful_deleted'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'daignoses','activity_items_id'=>$activity_items_id,'activity_description'=>lang('lab_services_successful_deleted'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}		else {  $message = lang('diseases_services_deleted'); }		echo $message; die;		}
	
	function addClarificationDesciption($customer_id=-1)
	{
	 $description = mysql_real_escape_string($this->input->post('description')); 
	 $item_id  = $this->input->post('service_id');
	 $result ='0';
	 $this->load->model('medicinetest');
	 if($description!='' && $item_id!='')
	 {	
		$existData = $this->medicinetest->exist_medicine_drugs_dosage( $item_id ,$customer_id );
		if($existData)
		 {	
			$result = $this->db->query("update ".$this->db->dbprefix('lab_category_dosage')." set dosage_description ='".$description."' where person_id='".$customer_id."' and item_id='".$item_id."' ");
		    $activity_description = lang('common_clarification_updated_successful');
		} 
		 else 
		 {				
			$result = $this->db->query("insert into ".$this->db->dbprefix('lab_category_dosage')." set item_id='".$item_id."', dosage_description ='".$description."',person_id='".$customer_id."' ");
		    $activity_description = lang('common_clarification_added_successful');
		 }
			if($result){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>'lab_services','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		   }	
	 }
		echo $result; die;		
	}
}
?>