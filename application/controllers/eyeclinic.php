<?php
require_once ("person_controller.php");
class Eyeclinic extends Person_controller
{
	function __construct()
	{
		parent::__construct('eyeclinic');
		$this->load->language('eyeclinics');
		$this->load->model('eyeclinics');
	}		

	function index($customer_id=-1)
	{ 
	    if($customer_id==-1){redirect(base_url());}	
		$this->check_action_permission('search');
		$exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers/');}
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = '';
		$data['controller_name'] = strtolower(get_class());
		$data['form_width'] = $this->get_form_width();
		$data['person_info']=$this->Customer->get_info($customer_id);	
		$data['manage_AllLabTest'] = $this->eyeclinics->search($customer_id,$search=''); 	
		$data['manage_history_menu']=get_patient_history_menu($customer_id);
		$data['per_page'] = '20';
		$this->load->view('eyeclinics/manage',$data);
	}
	

	function view($customer_id=-1)
	{
		if($customer_id==-1){redirect(base_url());}	   
		$this->check_action_permission('add_update');
		$data['person_id'] = $customer_id;		
	    $data['lab_category_list'] = $this->eyeclinics->SearchLabCatgory($search='',$value='');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'lab_services','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_eyeclinic'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//	
		$this->load->view("eyeclinics/form",$data);
	}	

	function addreport($customer_id=-1,$invest_id=-1)
	{	
		if($customer_id==-1 || $invest_id==-1){redirect(base_url());}   
		$this->check_action_permission('action_P_R');
		$data['person_id'] = $customer_id;	
		$data['invest_id'] = $invest_id;
		$this->load->view("eyeclinics/addreport",$data);
	}	

	function savereport($customer_id=-1,$item_id=-1)
	{	
		if($customer_id==-1 || $item_id==-1){redirect(base_url());}	
		$this->check_action_permission('add_update');
		$login_uerData  = $this->Employee->get_logged_in_employee_info();
		$report_details = addslashes($this->input->post('laboratory_report'));
		if(isset($report_details) && $report_details!='')
		{ 
		   $success= $this->db->query("update ".$this->db->dbprefix('lab_eyeclinic')." set test_report_details='".$report_details."',tested_by_employee_id = '".$login_uerData->person_id."', tested_date='".date('Y-m-d h:i:s')."',status='1' where id='".$item_id."' and person_id='".$customer_id."' and status='0' ");
		   if($success)
		   {  echo json_encode(array('success'=>true,'message'=>lang('eyeclinic_report_successful_added'), 'person_id'=>$customer_id) );
		      //***save employee activites in database***//
			  $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'result_PR','activity_items_id'=>$item_id,'activity_description'=>lang('eyeclinic_report_successful_added'));	
			  $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			  //***save employee activites in database***//
		   }	
		}
		else { echo json_encode(array('success'=>true,'message'=>lang('eyeclinic_report_error_added'), 'person_id'=>$customer_id) );}
	}

	function viewreport($customer_id=-1,$item_id=-1)
	{	
	    if($customer_id==-1 || $item_id==-1){ redirect(base_url()); } 
		$this->check_action_permission('action_V_R');
		$search=''; 
		$data['person_id'] 	   = $customer_id;	 
		$data['item_id'] 	   = $item_id;			
		$data['reportdata']    = $this->eyeclinics->get_Report($customer_id,$item_id);	
		$data['diseases_list'] = $this->Customer->GetDiseasList($customer_id,$item_id,$LabTestType='lab_eyeclinics',$search);
		$data['diabetes_list'] = $this->Customer->GetDiabetesList($customer_id,$item_id,$LabTestType='lab_eyeclinics',$search);	
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'result_VR','activity_items_id'=>$item_id,'activity_description'=>lang('eyeclinic_view_patient_result_vr'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//	
		$this->load->view("eyeclinics/viewreport",$data);
	}   
	
	function save($customer_id=-1)
	{
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		if($customer_id=='-1' || $this->input->post('lab_category')=='')
		 {
		    echo json_encode(array('success'=>false,'message'=>lang('eyeclinic_laboratory_error_adding'), 'person_id'=>$customer_id)); die;
		 }
		if($customer_id!='-1' && $this->input->post('lab_category')!='')
		 { 	//$lab_category  = implode(',',$this->input->post('lab_category'));
			//$this->load->model('labtest');
			//$valid = $this->labtest->valid_Active_Lab_category($lab_category);
			//if($valid) { echo json_encode(array('success'=>false,'message'=>lang('eyeclinic_deactivate_not_added'), 'person_id'=>$customer_id)); die;}
			$lab_category  = $this->input->post('lab_category');
			for($i=0; $i<count($lab_category); $i++) 
			{
			  $person_data = array('lab_category_id'=>$lab_category[$i],'person_id'=>$customer_id,'added_by_employee_id'=>$login_uerData->person_id,'added_date'=>date('Y-m-d h:i:s'),'status'  =>'0');
			  $success = $this->db->insert('lab_eyeclinic' ,$person_data);
			}
			if($success){
				echo json_encode(array('success'=>true,'message'=>lang('eyeclinic_laboratory_successful_added'), 'person_id'=>$customer_id) );
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'lab_services','activity_items_id'=>$customer_id,'activity_description'=>lang('eyeclinic_laboratory_successful_added'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
			}
		 }
		 else { echo json_encode(array('success'=>false,'message'=>lang('eyeclinic_laboratory_error_adding'), 'person_id'=>$customer_id));}
	}

	function delete($customer_id=-1)
	{
		$this->check_action_permission('delete');
		$to_delete=$this->input->post('ids');
		$bad_words = array('on');
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		if($this->eyeclinics->delete_list($customer_id,$to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('eyeclinic_successful_deleted').' '.	count($to_delete).' '.lang('eyeclinic_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$to_delete);
			$activity_description = lang('eyeclinic_successful_deleted').' '.	count($to_delete).' '.lang('eyeclinic_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'lab_services','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('eyeclinic_cannot_be_deleted')));
		}
	}	

	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Customer->search_count_all($search);
			$table_data = $this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->Customer->count_all();
			$table_data = $this->Customer->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}

		$config['base_url'] = site_url('customers/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);

		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}	

	function search($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$search_data=$this->eyeclinics->search($customer_id,$search, $this->input->post('order_col') ,$this->input->post('order_dir'));
		$pagination='';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));	
	}	

	function getservices($customer_id=-1)
	{	
         $search = addslashes($this->input->post('service')); 
		  $value = addslashes($this->input->post('checkedval'));
		echo $search_data=$this->eyeclinics->SearchLabCatgory($search,$value);die;  
	}	
	
	function activeDeActivate($customer_id=-1)
	{
		$this->check_action_permission('search');
		$value = $this->uri->segment(4);	
		$this->load->model('labtest');
		$success=$this->labtest->ActiveDeActivateCatgory($value); 
		if($success==1)
		{  $message = lang('eyeclinic_services_successful_updated'); 
		   //***save employee activites in database***//
			$test2 = explode('-',$value); $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'eyeclinic','activity_type_category'=>'lab_services','activity_items_id'=>$activity_items_id,'activity_description'=>lang('eyeclinic_services_successful_updated'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		   //***save employee activites in database***//
		}
		else {  $message = lang('eyeclinic_services_error_updated'); }
		echo $message; die;	
	}	

	function searchDiseasList($customer_id=-1)
	 {	
		$searchtype = addslashes($this->input->post('searchtype')); 
		$search = addslashes($this->input->post('service'));
		$value = addslashes($this->input->post('checkedval'));
		$item_id = addslashes($this->input->post('item_id'));
		if($searchtype == 'mtuha_daignoses') { $search_data = $this->Customer->GetDiseasList($customer_id,$item_id,$LabTestType='lab_eyeclinics',$search);}
		if($searchtype == 'daibetes')		 { $search_data = $this->Customer->GetDiabetesList($customer_id,$item_id,$LabTestType='lab_eyeclinics',$search); }		
        echo $search_data; die;
	 }
	function saveDiseases($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$savetype = $this->uri->segment(4);
		$item_id = $this->uri->segment(5);
		$value = $this->uri->segment(6);		
		$success=$this->eyeclinics->saveDiseases($customer_id,$item_id, $value,$savetype);
		if($success==1){  $message = lang('diseases_services_successful_added'); }
		else {  $message = lang('diseases_services_error_added'); }
		echo $message; die;	
	}
  

    function suggest($customer_id=-1)
	{
		$suggestions = $this->eyeclinics->get_search_suggestions($customer_id,$this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	

	function get_form_width()
	{			
		return 550;
	}

}

?>