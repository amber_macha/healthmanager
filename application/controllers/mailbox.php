<?php
require_once ("person_controller.php");
class Mailbox extends Person_controller
{
	function __construct()
	{
		parent::__construct('mailbox');
		$this->load->model('mailboxes');
		$this->load->language('mailbox');
		$this->load->helper('viewdatatable');
	}
		
	function index()
	{  	
		$this->check_action_permission('search');
		$data['method_type'] = 'inbox';	
		$config['base_url'] = site_url('mailbox/sorting');
		$config['total_rows'] = $this->mailboxes->count_all($data['method_type']);
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_manage_table($data['method_type'],$this->mailboxes->get_all($data['method_type'],$data['per_page']),$this);		
		$data['total_inbox'] = $config['total_rows'];
		$data['total_outbox']= $this->mailboxes->count_all('outbox');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>'','activity_description'=>lang('mailbox_inbox_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('mailbox/manage',$data);
	}
	
	function sorting()
	{
		$this->check_action_permission('search');
		$method_type = 'inbox';
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->mailboxes->search_count_all($method_type,$search);
			$table_data = $this->mailboxes->search($method_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->mailboxes->count_all($method_type);
			$table_data = $this->mailboxes->get_all($method_type,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('mailbox/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']  = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
		
	}
	
	function outbox()
	{  	
		$this->check_action_permission('search');
		$data['method_type'] = 'outbox';	
		$config['base_url'] = site_url('mailbox/sorting_outbox');
		$config['total_rows'] = $this->mailboxes->count_all($data['method_type']);
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_manage_table($data['method_type'],$this->mailboxes->get_all($data['method_type'],$data['per_page']),$this);		
		$data['total_inbox'] = $this->mailboxes->count_all('inbox');
		$data['total_outbox']= $this->mailboxes->count_all('outbox');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>'','activity_description'=>lang('mailbox_outbox_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('mailbox/manage',$data);
	}
	
	function sorting_outbox()
	{
		$this->check_action_permission('search');
		$method_type = 'outbox';
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->mailboxes->search_count_all($method_type,$search);
			$table_data = $this->mailboxes->search($method_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->mailboxes->count_all($method_type);
			$table_data = $this->mailboxes->get_all($method_type,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('mailbox/sorting_outbox');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']  = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));			
	}
	
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function search()
	{
		$this->check_action_permission('search');
		$method_type = $this->input->post('method_type');
		$search		 = $this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->mailboxes->search($method_type,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('mailbox/search');
		$config['total_rows'] = $this->mailboxes->search_count_all($method_type,$search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$search_data,$this);
		$data['inbox_counter'] = '(' .$this->mailboxes->Mails_Counter() .'/'.$this->mailboxes->count_all('inbox'). ')';
		$data['outbox_counter']= '(' .$this->mailboxes->count_all('outbox'). ')';
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination'],'inbox_counter'=>$data['inbox_counter'],'outbox_counter'=>$data['outbox_counter']));  
	} 

	function inboxMailCount()
	{
		$inbox_counter  = '(' .$this->mailboxes->Mails_Counter() .'/'.$this->mailboxes->count_all('inbox'). ')';
		$outbox_counter = '(' .$this->mailboxes->count_all('outbox'). ')';
		echo json_encode(array('inbox_counter'=>$inbox_counter,'outbox_counter'=>$outbox_counter));  
	}	
	
	/*Gives search suggestions based on what is being searched for */
	function suggest($method_type=NULL)
	{
		$suggestions = $this->mailboxes->get_search_suggestions($method_type,$this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/* Loads the customer edit form */
	function view($view_type = NULL, $item_id = -1)
	{	
		if($view_type=='send'){	$this->check_action_permission('send'); }
		else 
		{ 	$this->check_action_permission('view'); 
			$item_data = array('read_status' => '1','read_date'=> date('Y-m-d H:i:s'));
			$this->mailboxes->save($item_data,$item_id);
		}
		$data['view_type'] 	   = $view_type;
		$data['method_type']   = 'mailbox';
		$data['item_info']     = $this->mailboxes->get_info($item_id);
		$data['GroupsData']	   = $this->Employee->get_AllEmployee_Groups($parent_id=0);
		$data['SubGroupsData'] = $this->Employee->get_Sub_Groups( 6 , -1); 		
		if($item_id!=-1 && $view_type=='show'){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>$item_id,'activity_description'=>lang('mailbox_email_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}		
		$this->load->view("mailbox/form",$data);
	}	
	
	/* Inserts/updates a customer */
	function save($item_id=-1)
	{		
		$this->check_action_permission('send');	
		if($this->input->post('email_type')=='' || $this->input->post('mail_subject')=='' || $this->input->post('mail_content')=='') 
		{	
			echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'person_id'=>-1));die; 
		}
	    $success ='';		
		$email_type  = $this->input->post('email_type');
		$Login_Employee_id = $this->Employee->get_Logged_SessionData('person_id');
		$Mail_Data = array('from_employee_id'=> $Login_Employee_id,
						   'mail_subject'	 => stripslashes($this->input->post('mail_subject')),
						   'mail_body'		 => stripslashes($this->input->post('mail_content')),
						   'read_status'	 => '0',
						   'added_date'	     => date('Y-m-d H:i:s')
						  );
	  //** send mail for all Employees** //						  
	  if($email_type==0) 
	   {			  
			 $EmployeeData =  $this->Employee->get_All_Employee_By_Groups();
			 if($EmployeeData->num_rows()>0)
			 {
			   foreach($EmployeeData->result() as $RowData)
			   {  
				   if($Login_Employee_id != $RowData->person_id)
					{	$temp_array = array('to_employee_id' => $RowData->person_id);
						$Mail_Data  = array_merge($Mail_Data,$temp_array);
						$success =  $this->mailboxes->save($Mail_Data,$item_id);
					}	
			  }}			 		
	   }
	   //** send mail for Groups Subgroups Employees **//
	   if($email_type==1) 
	   {
			if($this->input->post('group_name')==''){ echo json_encode(array('success'=>false,'message'=>lang('common_group_name_required'),'person_id'=>-1));die; }
			 //** send mail for Subgroups Employees **//
			if($this->input->post('group_name')!='' && $this->input->post('sub_group_name')!='' )
			{   
				 $EmployeeData =  $this->Employee->get_All_Employee_By_Groups($this->input->post('group_name'), $this->input->post('sub_group_name'));
				 if($EmployeeData->num_rows()>0)
				 {
				   foreach($EmployeeData->result() as $RowData)
				   {  
					   if($Login_Employee_id != $RowData->person_id)
						{	$temp_array = array('to_employee_id' => $RowData->person_id);
							$Mail_Data  = array_merge($Mail_Data,$temp_array);
							$success =  $this->mailboxes->save($Mail_Data,$item_id);
						}	
				  }}		
			 }
			  //** send mail for all Groups Employees **//
			if($this->input->post('group_name')!='' && $this->input->post('sub_group_name')=='' )
			{   
				 $EmployeeData =  $this->Employee->get_All_Employee_By_Groups($this->input->post('group_name'));
				 if($EmployeeData->num_rows()>0)
				 {
				   foreach($EmployeeData->result() as $RowData)
				   {  
					   if($Login_Employee_id != $RowData->person_id)
						{	$temp_array = array('to_employee_id' => $RowData->person_id);
							$Mail_Data  = array_merge($Mail_Data,$temp_array);
							$success =  $this->mailboxes->save($Mail_Data,$item_id);
						}	
				  }}		
			 }
	  }
	  //** send mail for Individual Employees **//
	  if($email_type==2) 
	  {
		if($this->input->post('employee_id')==''){ echo json_encode(array('success'=>false,'message'=>lang('mailbox_employee_name_required'),'person_id'=>-1));die; }
		else {   
			  if(!$this->Employee->exists($this->input->post('employee_id'))) { echo json_encode(array('success'=>false,'message'=>lang('mailbox_employee_name_invalid'),'person_id'=>-1));die; }
			  if($Login_Employee_id != $this->input->post('employee_id'))
			  {	  $temp_array = array('to_employee_id' => $this->input->post('employee_id'));
				  $Mail_Data  = array_merge($Mail_Data,$temp_array);			  
				  $success =  $this->mailboxes->save($Mail_Data,$item_id);
			  }			  
			}
	  } 
        	
		//$item_data  = array_merge($item_data,$temp_array);
		if($success)
		{   
			echo json_encode(array('success'=>true,'message'=>lang('mailbox_email_send_successful'),'person_id'=>$item_id));
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>'','activity_description'=>lang('mailbox_email_send_successful'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}
		else
		{	
			echo json_encode(array('success'=>false,'message'=>lang('mailbox_email_send_error'),'person_id'=>-1));
		}
	}	

	function delete()
	{
		$this->check_action_permission('delete');
		$items_to_delete=$this->input->post('ids');
		
		if($this->mailboxes->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('mailbox_successful_deleted').' '.	count($items_to_delete).' '.lang('mailbox_one_or_multiple')));
			//***save employee activites in database***//
			$activity_items_id = implode(',',$items_to_delete); 
			$activity_description = lang('mailbox_successful_deleted').' '. count($items_to_delete).' '.lang('mailbox_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {	echo json_encode(array('success'=>false,'message'=>lang('mailbox_cannot_be_deleted')));	}
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_manage_highlight_data_rows($method_type,$this->mailboxes->get_Item_ArrayData($item_id),$this);
		echo $data_row;
	}	
	
	function cleanup()
	{
		$this->mailboxes->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('mailbox_cleanup_sucessful')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'mailbox','activity_type_category'=>'mailbox_details','activity_items_id'=>'','activity_description'=>lang('mailbox_cleanup_sucessful'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	}
	
	/*get the width for the add/edit form	*/
	function get_form_width()
	{			
		return 550;
	}
	

	
}
?>