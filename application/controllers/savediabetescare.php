<?php
require_once ("secure_area.php");
class Savediabetescare extends Secure_area 
{
	function __construct()
	{
		parent::__construct();	
		$this->load->language('diabetes_care');
		$this->load->model('diabetescare');
	}
	public function index()	{  redirect('diabetes_care');	}
	
	function getCustomer_PatientData()
	{
		$account_no =  $this->input->post('account_no');
	    $valid= $this->diabetescare->checkvalid_Account_No($account_no);
		if(!$valid) { echo 'failed'; die;}	
		$exist= $this->diabetescare->exist_diabetes_patients_by_account_no($account_no);
		if($exist) { echo 'alreadyexists'; die;}	
		$Cust_Data = $this->diabetescare->get_Patient_Data_By_Account_No($account_no);
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{   $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $this->Customer->get_info($customer_id);
			$patient_info 			= $this->diabetescare->get_Item_ArrayData($method_type='diabetes_customers' ,$customer_id);
			$data['patient_info']	= $patient_info;
			$data['months'] 		= $this->Customer->get_months();
			$data['days'] 			= $this->Customer->get_days();
			$data['years'] 			= $this->Customer->get_years();
			$this->load->model('savesetting');	
			$data['ResidenceData'] = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
			$data['OccupatiosData']=  $this->savesetting->Get_Table_Data('occupations','occupation_name');	
			$data['method_type'] 	  = 'diabetes_customers';
			$this->load->view("diabetes_care/form_customers",$data);
		}	
	}
	
	function save_Diabetes_CustomerData_Step1($customer_id=-1)
	{
		$this->check_action_permission('add_update');	
		if($customer_id=='-1') { echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating'),'item_id'=>$customer_id));die; }		
		$exist = $this->diabetescare->exist_diabetes_patients($customer_id);	
		$customerdata = array('last_updated' => date('Y-m-d H:i:s') );
		if(!$exist) 
		{ 
		   $Facility_Data = $this->diabetescare->Get_Facility_Data();
		   if($Facility_Data->facility_region_id=='' || $Facility_Data->facility_region_id=='' || $Facility_Data->facility_region_id=='')
		   {
			echo json_encode(array('success'=>false,'message'=>lang('diabetes_facility_name_must_registraion'),'item_id'=>$customer_id));die; 
		   }
			$clinic_registration_no = $Facility_Data->facility_region_id.'/'.$Facility_Data->facility_distict_id.'/'.$Facility_Data->facility_name.'/'.$customer_id;
			 $temp_array = array('person_id'			  => $customer_id,
								 'delete'			  	  => '0',
								 'clinic_registration_no' => $clinic_registration_no,
								 'registration_date'	  => date('Y-m-d')
								  );
			 $customerdata =  array_merge( $customerdata,$temp_array ); 
			 $result = $this->db->insert( 'customers_diabetescare',$customerdata );
			 if($result) { echo json_encode(array('success'=>true,'message'=>lang('diabetes_items_successful_adding'),'item_id'=>$customer_id));}				
			 else { echo json_encode(array('success'=>false,'message'=>lang('diabetes_items_error_adding_updating'),'item_id'=>$customer_id));}				 
		}
	   else {		   
			  $this->db->where('person_id',$customer_id);
			  $this->db->where('delete','0');
			  $result = $this->db->update('customers_diabetescare',$customerdata);	
			  if($result) { echo json_encode(array('success'=>true,'message'=>lang('diabetes_items_successful_updating'),'item_id'=>$customer_id));}				
			  else { echo json_encode(array('success'=>false,'message'=>lang('diabetes_items_error_adding_updating'),'item_id'=>$customer_id));}					  
			}			
	}
	
	function save_Diabetes_CustomerData_Step2($customer_id=-1)
	{
		$this->check_action_permission('add_update');	
		if( $customer_id=='-1') { echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating'),'item_id'=>$customer_id));die; }
		$birth_date = $this->input->post('age_year').'-'.$this->input->post('age_month').'-'.$this->input->post('age_day');
		$customerdata =  array(	'father_name'				=>	$this->input->post('father_name'),
								'father_contact_no' 		=>	$this->input->post('father_contact_no'),
								'mother_name'				=>	$this->input->post('mother_name'),
								'mother_contact_no'			=>	$this->input->post('mother_contact_no'),
								'guardians_name'			=>	$this->input->post('guardians_name'),
								'guardians_contact_no'		=>	$this->input->post('guardians_contact_no'),								
								'ethnicity'					=>	$this->input->post('ethnicity'),
								'year_diagnosis_of_diabetes'=>	$this->input->post('year_diagnosis_of_diabetes'),
								'year_diagnosis_of_hypertension'			=>	$this->input->post('year_diagnosis_of_hypertension'),
								'year_diagnosis_of_abnormal_lipids'			=>	$this->input->post('year_diagnosis_of_abnormal_lipids'),
								'education_level'							=>	$this->input->post('education_level'),
								'house_mud_wattle_grass_thatched'		    =>	$this->input->post('house_mud_wattle_grass_thatched'),
								'house_with_electricity_solar_powe_supply'  => $this->input->post('house_with_electricity_solar_powe_supply'),
								'house_with_piped_water'					=>	$this->input->post('house_with_piped_water'),
								'house_with_flushing_toilet_inside_house'	=>	$this->input->post('house_with_flushing_toilet_inside_house'),
								'house_with_kitchen_inside'					=>	$this->input->post('house_with_kitchen_inside'),
								'location_of_residence' 					=> $this->input->post('location_of_residence'),
								'number_of_relatives_with_diabetes'			=>	$this->input->post('number_of_relatives_with_diabetes'),
								'number_of_relatives_with_hypertension'		=>	$this->input->post('number_of_relatives_with_hypertension')					
							  ); 
		$BirthData = array('dateOfBirth' => $birth_date );	
		$this->db->where('person_id',$customer_id);
		$result = $this->db->update('customers',$BirthData);	
		
		$this->db->where('person_id',$customer_id);
		$this->db->where('delete','0');
		$result = $this->db->update('customers_diabetescare',$customerdata);	
		if($result) { echo json_encode(array('success'=>true,'message'=>lang('diabetes_items_successful_updating'),'item_id'=>$customer_id));}				
		else { echo json_encode(array('success'=>false,'message'=>lang('diabetes_items_error_adding_updating'),'item_id'=>$customer_id));}	
	}
	
	function save_insulin_treatment($item_id=-1)
	{
		$this->check_action_permission('add_update');	
		$this->load->model('savesetting');
		if($this->input->post('before_breakfast')=='' && $this->input->post('before_lunch')=='' && $this->input->post('before_dinner')=='') 
		{ echo json_encode(array('success'=>false,'message'=>lang('diabetes_insulin_treatment_category_required'),'item_id'=>$item_id));die; }
		$exist = $this->diabetescare->exist_insulin_treatment($this->input->post('before_breakfast'),$this->input->post('before_lunch'),$this->input->post('before_dinner'),$item_id);	
		if($exist) { echo json_encode(array('success'=>false,'message'=>lang('common_selected_item_already_exist'),'item_id'=>$item_id));die; } 
		
		$item_data =  array('before_breakfast'		=>	$this->input->post('before_breakfast'),
							'before_lunch'			=>	$this->input->post('before_lunch'),
							'before_dinner'			=>	$this->input->post('before_dinner'),
							'treatments_comments'   =>  $this->input->post('comments')								
						   );
		if($item_id==-1){ $temp_array = array('added_date' =>date('Y-m-d')); $item_data = array_merge($item_data, $temp_array); }
		
		if($this->savesetting->save_data($table_name='insulin_treatments', $item_data, $item_id))
		{			
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('common_successful_adding_selected_item'),'person_id'=>$item_data));				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('common_successful_updating_selected_item'),'person_id'=>$item_data));
			}	
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('common_error_adding_updating_selected_item'),'person_id'=>$item_data));
		} 			
	}
	
	function addnewInsulinCategory($item_id=-1)
	 {	
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		
         if($this->input->post('servicename')=='' && $this->input->post('comments')=='')
		 {
			echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'item_id'=>$item_id));die;  
		 }
		 $exist = $this->diabetescare->exists_InsulinBrand_Name($this->input->post('servicename'), $item_id);
		if($exist) { echo json_encode(array('success'=>false,'message'=>lang('common_selected_item_already_exist'),'item_id'=>$item_id));die; }
		
		$item_data 	  =  array('insulin_name' => $this->input->post('servicename'),'insulin_comments' => $this->input->post('comments'));
		if($item_id==-1){ $temp_array = array('added_date' =>date('Y-m-d')); $item_data = array_merge($item_data, $temp_array); }
		//print_r($item_data);
		//echo $item_id; die;
		
		if($this->savesetting->save_data($table_name='insulin_category', $item_data, $item_id))
		{			
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('common_successful_adding_selected_item'),'person_id'=>$item_data));				
			}
			else 
			{
				echo json_encode(array('success'=>true,'message'=>lang('common_successful_updating_selected_item'),'person_id'=>$item_data));
			}	
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('common_error_adding_updating_selected_item'),'person_id'=>$item_data));
		}		   
	} 
	
	function delete($customer_id=-1)
	  {		
		//$this->check_action_permission('search');
		$message='';
		$category_type = $this->uri->segment(3);
		$value = $this->uri->segment(4);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);		
		$success=$this->diabetescare->delete_list($category_type,$to_delete);	
		if($success==1){  $message = lang('common_successful_delete_selected_items'); }
		else {  $message = lang('common_error_deleted_items'); }
		echo $message; die;	
	}

}
?>