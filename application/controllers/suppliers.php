<?php
require_once ("person_controller.php");
class Suppliers extends Person_controller
{
	function __construct()
	{
		parent::__construct('suppliers');
	}	
	function index()
	{		$this->check_action_permission('search');		$config['base_url'] = site_url('suppliers/sorting');		$config['total_rows'] = $this->Supplier->count_all();		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 		$this->pagination->initialize($config);		$data['pagination'] = $this->pagination->create_links();		$data['controller_name']=strtolower(get_class());		$data['form_width']=$this->get_form_width();		$data['per_page'] = $config['per_page'];		$data['manage_table']=get_supplier_manage_table($this->Supplier->get_all($data['per_page']),$this);				//***save employee activites in database***//		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>'','activity_description'=>lang('suppliers_list_module_view'));			$this->Employee->save_activites($Activity_Data,$item_id=-1); 		//***save employee activites in database***//		$this->load->view('suppliers/manage',$data);
	}	
	function sorting()	{		$this->check_action_permission('search');		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;		if ($search)		{			$config['total_rows'] = $this->Supplier->search_count_all($search);			$table_data = $this->Supplier->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}		else		{			$config['total_rows'] = $this->Supplier->count_all();			$table_data = $this->Supplier->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		}		$config['base_url'] = site_url('suppliers/sorting');		$config['per_page'] = $per_page; 		$this->pagination->initialize($config);		$data['pagination'] = $this->pagination->create_links();		$data['manage_table']=get_supplier_manage_table_data_rows($table_data,$this);		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));		}
	
	/* added for excel expert */
	function excel_export() {
		$data = $this->Supplier->get_all()->result_object();		$this->load->helper('report');		$rows = array();		$row = array("Supplier ID","Department Name", "First Name", "Last Name", "E-Mail", "Phone Number", "Address 1", "Address 2", "City", "State", "Zip", "Country", "Comments", "Account Number","Registration Date ");		$rows[] = $row;		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>'','activity_description'=>lang('suppliers_items_export_successfull'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		foreach ($data as $r) {			$row = array(				$r->person_id,
				$r->company_name,				$r->first_name,				$r->last_name,				$r->email,				$r->phone_number,				$r->address_1,				$r->address_2,				$r->city,				$r->state,				$r->zip,				$r->country,				$r->comments,				$r->account_number,
				date('d-m-Y',strtotime($r->registration_date)) 			);
			$rows[] = $row;		}	
		$content = array_to_csv($rows);		force_download('suppliers_export' . '.csv', $content);		exit;
	}
	
	function excel()
	{
		$data = file_get_contents("import_items.csv");
		$name = 'import_items.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->check_action_permission('add_update');
		$this->load->view("suppliers/excel_import", null);
	}

	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$this->db->trans_start();
		$msg = 'do_excel_import';
		$failCodes = array();		
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg,'person_id'=>$supplier_id) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE)
				{  
				 if($data[0]!='Supplier ID') //Skip first row
				 {  
				    if(isset($data[14]) && $data[14]!='') $registration_date = date('Y-m-d',strtotime($data[14]));else $registration_date = date('Y-m-d');
					$supplier_id = $data[0];
					$person_data=array( 'first_name'		=>	$data[2],
										'last_name'			=>	$data[3],					
										'email'				=>	$data[4],
										'phone_number'		=>	$data[5],					
										'address_1'			=>	$data[6],
										'address_2'			=>  $data[7],					
										'city'				=>	$data[8],
										'state'				=>	$data[9],
										'zip'				=>	$data[10],
										'country'			=>	$data[11],
										'comments'			=>	$data[12],
										'registration_date'	=>	$registration_date
									);	
					$supplier_data=array('company_name'=>$data[1],'account_number'=>$data[13]=='' ? null:$data[13] );
					
					if(!$this->Supplier->saveImportData($person_data,$supplier_data,$supplier_id))
					{ 
                        echo json_encode( array('success'=>false,'message'=>lang('suppliers_duplicate_item_ids'),'person_id'=>$supplier_id));	die;					
					}
					//else { echo json_encode( array('success'=>false,'message'=>lang('suppliers_duplicate_item_ids'))); die; }
				}
			 }
		    }	
			else
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format'),'person_id'=>$supplier_id));die;
			}
		}

		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('suppliers_items_import_successfull')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>'','activity_description'=>lang('suppliers_items_import_successfull'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	}
   
	/*	Returns supplier table data rows. This will be called with AJAX.*/
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;		$search_data=$this->Supplier->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');		$config['base_url'] = site_url('suppliers/search');		$config['total_rows'] = $this->Supplier->search_count_all($search);		$config['per_page'] = $per_page ;		$this->pagination->initialize($config);						$data['pagination'] = $this->pagination->create_links();		$data['manage_table']=get_supplier_manage_table_data_rows($search_data,$this);		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	/*	Gives search suggestions based on what is being searched for*/
	function suggest()
	{
		$suggestions = $this->Supplier->get_search_suggestions($this->input->get('term'),100);		echo json_encode($suggestions);	}	
	/*	Loads the supplier edit form*/
	function view($supplier_id=-1)
	{
		$this->check_action_permission('add_update');		$data['controller_name']=strtolower(get_class());
		$data['person_info']=$this->Supplier->get_info($supplier_id);
		if($supplier_id!=-1)
		{
		   //***save employee activites in database***//
		   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>$supplier_id,'activity_description'=>lang('suppliers_items_information_view'));	
		   $this->Employee->save_activites($Activity_Data,$item_id=-1); 
		   //***save employee activites in database***//		}
		$this->load->view("suppliers/form",$data);
	}	
	/*
	Inserts/updates a supplier
	*/
	function save($supplier_id=-1)
	{		$this->check_action_permission('add_update');				$person_data = array(		'first_name'=>$this->input->post('first_name'),		'last_name'=>$this->input->post('last_name'),		'email'=>$this->input->post('email'),		'phone_number'=>$this->input->post('phone_number'),		'address_1'=>$this->input->post('address_1'),		'address_2'=>$this->input->post('address_2'),		'city'=>$this->input->post('city'),		'state'=>$this->input->post('state'),		'zip'=>$this->input->post('zip'),		'country'=>$this->input->post('country'),		'comments'=>$this->input->post('comments')		);
		$supplier_data=array(		'company_name'=>$this->input->post('company_name'),		'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),		);
		if($this->Supplier->save($person_data,$supplier_data,$supplier_id))		{			if ($this->config->item('mailchimp_api_key'))			{				$this->Person->update_mailchimp_subscriptions($this->input->post('email'), $this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('mailing_lists'));			}
			//New supplier			if($supplier_id==-1)			{				echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_adding').' '. $supplier_data['company_name'],'person_id'=>$supplier_data['person_id']));			    $activity_description = lang('suppliers_successful_adding').' '. $supplier_data['company_name'];
				$supplier_id = $supplier_data['person_id'];
			}
			else //previous supplier			{				echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_updating').' '. $supplier_data['company_name'],'person_id'=>$supplier_id));			    $activity_description = lang('suppliers_successful_updating').' '. $supplier_data['company_name'];
			}
			 //***save employee activites in database***//
		      $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>$supplier_id,'activity_description'=>$activity_description);	
		      $this->Employee->save_activites($Activity_Data,$item_id=-1); 
		     //***save employee activites in database***//		}
		else//failure		{	
			echo json_encode(array('success'=>false,'message'=>lang('suppliers_error_adding_updating').' '.			$supplier_data['company_name'],'person_id'=>-1));		}
	}	
	/*This deletes suppliers from the suppliers table*/
	function delete()
	{
		$this->check_action_permission('delete');
		$suppliers_to_delete=$this->input->post('ids');
		if($this->Supplier->delete_list($suppliers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('suppliers_successful_deleted').' '.	count($suppliers_to_delete).' '.lang('suppliers_one_or_multiple')));             //***save employee activites in database***//
			$activity_items_id = implode(',',$suppliers_to_delete); 
			$activity_description = lang('suppliers_successful_deleted').' '. count($suppliers_to_delete).' '.lang('suppliers_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'department','activity_type_category'=>'department_information','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('suppliers_cannot_be_deleted')));		}
	}
	/*	Gets one row for a supplier manage table. This is called using AJAX to update one row.	*/
	function get_row()
	{
		$person_id = $this->input->post('row_id');		$data_row=get_supplier_data_row($this->Supplier->get_info($person_id),$this);		echo $data_row;	}		
	function search_suppliers()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->Supplier->Search_Supplier_List($search_keyword); die;
	}
	
	/*get the width for the add/edit form*/
	function get_form_width()	{					return 550;	}
}
?>