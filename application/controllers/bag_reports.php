<?php
require_once ("person_controller.php");
class Bag_reports extends Person_controller
{
	function __construct()
	{
		parent::__construct('bag_reports');
		$this->load->model('bagreports');
		$this->load->language('bagreports');
		$this->load->helper('viewdatatable');
	}
		
	function index()
	{  	
		$this->check_action_permission('search');
		$data['method_type'] = 'bag_reports';	
		$config['base_url'] = site_url('bag_reports/sorting');
		$config['total_rows'] = $this->bagreports->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_manage_table($data['method_type'],$this->bagreports->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'bag_reports','activity_type_category'=>'bag_reports_details','activity_items_id'=>'','activity_description'=>lang('bag_reports_list_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('bag_reports/manage',$data);
	}
	
	function sorting()
	{
		$this->check_action_permission('search');
		$method_type = 'bag_reports';
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->bagreports->search_count_all($search);
			$table_data = $this->bagreports->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->bagreports->count_all();
			$table_data = $this->bagreports->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('bag_reports/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']  = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
		
	}
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function search()
	{
		$this->check_action_permission('search');
		$method_type = 'bag_reports';
		$search	     =	$this->input->post('search');		
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->bagreports->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('bag_reports/search');
		$config['total_rows'] = $this->bagreports->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function ToFiltterBagList()
	{
		$this->check_action_permission('search');
		$method_type = 'bag_reports';
		$search = '';
		$Search_Status_Type=$this->input->post('search_status_type');
		if($Search_Status_Type!='')
		{ 
			$_SESSION['BAG_REPORTS_STATUS_TYPE'] = $Search_Status_Type; 
		}
		else {unset($_SESSION['BAG_REPORTS_STATUS_TYPE']); }	 
		
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->bagreports->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('bag_reports/search');
		$config['total_rows'] = $this->bagreports->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_manage_table_data_rows($method_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}	
	
	/*Gives search suggestions based on what is being searched for */
	function suggest()
	{
		$suggestions = $this->bagreports->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/* Loads the customer edit form */
	function view($view_type = NULL, $item_id = -1)
	{	
		if($view_type=='edit'){	$this->check_action_permission('add_update'); }
		else {	$this->check_action_permission('view'); }
		$data['view_type'] 	 = $view_type;
		$data['method_type'] = 'bag_reports';
		$data['item_info'] = $this->bagreports->get_info($item_id);
		if($item_id!=-1 && $view_type=='show'){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'bag_reports_details','activity_items_id'=>$item_id,'activity_description'=>lang('bag_reports_details_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}
		$this->load->view("bag_reports/form",$data);
	}	
	
	/* Inserts/updates a customer */
	function save($item_id=-1)
	{		
	  $this->check_action_permission('add_update');	
	  $item_info = $this->bagreports->get_info($item_id);
	  if($item_info->status==4){ echo json_encode(array('success'=>false,'message'=>lang('bag_reports_error_adding_updating'),'person_id'=>-1));die; }	
	  if($this->input->post('report_subject')=='' || $this->input->post('report_description')=='' || $this->input->post('module_id')=='' || $this->input->post('reports_status')=='') 
	  {	
		echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'person_id'=>-1));die; 
	  }	 
	 
	   $item_data =  array( 'module_id'		    =>	$this->input->post('module_id'),
							'report_subject'    =>	stripslashes($this->input->post('report_subject')),
							'description'	    =>	stripslashes($this->input->post('report_description')),
							'action_explanation'=>	stripslashes($this->input->post('action_explanation')),
							'status'		    =>	$this->input->post('reports_status'),
							'reject_reason'	    =>	stripslashes($this->input->post('reject_reason'))
							);
		
        if($item_id=='-1')
		{
			$temp_array = array('employee_id' => $this->Employee->get_Logged_SessionData('person_id'),'added_date'=>date('Y-m-d H:i:s'));
			$item_data  = array_merge($item_data,$temp_array);			
			//$item_id = $this->bagreports->save($item_data,$item_id);
        }
		else{
			$temp_array = array('updated_by_employee_id' => $this->Employee->get_Logged_SessionData('person_id'),'last_updated_date'=>date('Y-m-d H:i:s'));
			$item_data  = array_merge($item_data,$temp_array);
		}		
	
		if($this->bagreports->save($item_data,$item_id))
		{
			if($item_id==-1)
			{   $item_id = $this->db->insert_id();
				echo json_encode(array('success'=>true,'message'=>lang('bag_reports_successful_adding').' '. $item_data['report_subject'],'person_id'=>$item_id));
				$activity_description = lang('bag_reports_successful_adding').' '. $item_data['report_subject'];	
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('bag_reports_successful_updating').' '.$item_data['report_subject'],'person_id'=>$item_id));
			    $activity_description = lang('bag_reports_successful_updating').' '. $item_data['report_subject'];
			}
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'bag_reports','activity_type_category'=>'bag_reports_details','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//	
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('bag_reports_error_adding_updating').' '.$item_data['report_subject'],'person_id'=>-1));
		}
	}	

	function delete()
	{
		$this->check_action_permission('delete');
		$items_to_delete=$this->input->post('ids');		
		if($this->bagreports->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('bag_reports_successful_deleted').' '.	count($items_to_delete).' '.lang('bag_reports_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$items_to_delete); 
			$activity_description = lang('bag_reports_successful_deleted').' '. count($items_to_delete).' '.lang('bag_reports_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'bag_reports','activity_type_category'=>'bag_reports_details','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {	echo json_encode(array('success'=>false,'message'=>lang('bag_reports_cannot_be_deleted')));	}
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_manage_highlight_data_rows($method_type,$this->bagreports->get_Item_ArrayData($item_id),$this);
		echo $data_row;
	}	
	
	function cleanup()
	{
		$this->bagreports->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('bag_reports_cleanup_sucessful')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'bag_reports','activity_type_category'=>'bag_reports_details','activity_items_id'=>'','activity_description'=>lang('bag_reports_cleanup_sucessful'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	}
	
	/*get the width for the add/edit form	*/
	function get_form_width()
	{			
		return 550;
	}
	

	
}
?>