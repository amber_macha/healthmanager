<?php
require_once ("secure_area.php");
class Nhif_patient_form extends Secure_area 
{
	function __construct()
	{
		parent::__construct('nhif_patient_form');	
		$this->load->language('laboratory'); 
		$this->load->model('laboratories');
		$this->load->helper('laboratory_helper');
	}   
	
	
	function index()
	{
	    $this->check_action_permission('view_nhif_patient');
	    $data['controller_name']=strtolower(get_class()); 		
		$this->load->view("nhif_patient/manage_nhif_patient_form",$data);
	}
	
	function getCustomer_PatientData()
	{  
		$account_no     =  $this->input->post('account_no');
		$from_date      =  $this->input->post('from_date');
		$to_date        =  $this->input->post('to_date');
		$reference_code =  $this->input->post('reference_code');
		if($account_no=='' || $from_date=='' || $to_date=='' || $reference_code==''){ echo '0'; die;}
		if( strtotime($from_date) > strtotime($to_date)) { echo '1'; die;}
	    $valid= $this->Customer->checkvalid_Account_No($account_no);
		if(!$valid) { echo '2'; die;}	
		$exist= $this->Customer->check_Patient_Category($patient_category =4,$account_no,'account_number');
		if(!$exist) { echo '3'; die;}
		$this->load->model('savesetting'); $this->load->model('ipd_in_patients');		
		$Ref_Exist = $this->savesetting->check_Reference_Code($reference_code);		
		if($Ref_Exist) { echo '4'; die;}
		$exists_Customer_IPD_IN = $this->ipd_in_patients->exists_customer_IPD_IN($account_no);
		if(!$exists_Customer_IPD_IN) { echo '5'; die;}
			
		$from_date_1 = date('Y-m-d',strtotime($from_date));
		$to_date_1   = date('Y-m-d',strtotime($to_date));
		$Cust_Data = $this->Customer->get_Patient_Data_By_Account_No($account_no);
		$data['Cust_Data'] = $Cust_Data;
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{   
		    $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $this->Customer->get_info($Cust_Data->person_id);
			$Item_Data = array( 'person_id' 	   => $Cust_Data->person_id ,
							    'from_date'        => $from_date_1,
							    'to_date'		   => $to_date_1,
							    'reference_code_no'=> $reference_code,
							    'employee_id'      => $this->Employee->get_Logged_SessionData('person_id'),
							    'added_date'	   => date('Y-m-d H:i:s')
							   );
			$this->savesetting->addTableData('nhif_patient_reports',$Item_Data);	  
			$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 			
			$data['reference_code'] = $reference_code;
			$data['ResidenceData'] = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
			$data['OccupatiosData']=  $this->savesetting->Get_Table_Data('occupations','occupation_name');
			
			$this->load->model('medicinetest'); $this->load->model('otherservices');
			$this->load->model('labtest');      $this->load->model('dentals'); 			
			$this->load->model('uss_xray'); 	$this->load->model('eyeclinics');				
			$Attendance_Date ='';
			//$data['Cust_Data'] = $this->Customer->get_Patient_Data_By_Account_No($account_no);			
			//$data['person_info'] = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			//PATIENT MTHUA DIAGNOSIS LIST
			$Diagnosis_List ='';
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
			$data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();

			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
			
			$this->load->view("nhif_patient/manage_nhif_patient",$data);
		}	
	}
	
	function index2()
	{
	    $this->check_action_permission('view_nhif_patient');
	    $data['controller_name']=strtolower(get_class());
		$this->load->model('ipd_in_patients');  $this->load->model('labtest');
		$this->load->model('medicinetest'); 	$this->load->model('otherservices');
		$this->load->model('savesetting');$this->load->model('uss_xray'); 
		$this->load->model('dentals'); $this->load->model('eyeclinics');
		$Attendance_Date ='';
		$from_date_1 = '2015-03-10'; $to_date_1 = '2015-03-15';
		$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 
			$account_no = '00-00-03/2015';
			$data['reference_code'] = 'Test00021';
			$Cust_Data = $this->Customer->get_Patient_Data_By_Account_No($account_no);
		    $data['Cust_Data'] = $Cust_Data;
			$data['ResidenceData'] = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
			$data['OccupatiosData']=  $this->savesetting->Get_Table_Data('occupations','occupation_name');
			
			$this->load->model('medicinetest'); $this->load->model('otherservices');
			$this->load->model('labtest');      $this->load->model('dentals'); 			
			$this->load->model('uss_xray'); 	$this->load->model('eyeclinics');				
			$Attendance_Date ='';
			//$data['Cust_Data'] = $this->Customer->get_Patient_Data_By_Account_No($account_no);			
			//$data['person_info'] = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			//PATIENT MTHUA DIAGNOSIS LIST
			$Diagnosis_List ='';
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
	        $data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//print_r($Attendance_Date);die;
			//if($Attendance_Date > '0000-00-00 00') { $Attendance_Date = date('d-m-Y', strtotime($Attendance_Date)); }
			//$data['Attendance_Date'] = $Attendance_Date;
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();

			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
		    $this->load->view("nhif_patient/manage_nhif_patient",$data);
	}
	
	
	function get_form_width()
	{			
		return 600;
	}

}
?>