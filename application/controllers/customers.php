<?php
require_once ("person_controller.php");
class Customers extends Person_controller
{
	function __construct()
	{
		parent::__construct('customers');
	}	
		
	function index($search_type=NULL,$start_date=NULL,$end_date=NULL)
	{		
		$this->check_action_permission('search');		
		
		$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE'] ='';
		$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY'] ='';
		$_SESSION['CUSTOMER_SEARCH_START_DATE'] ='';		
		$_SESSION['CUSTOMER_SEARCH_END_DATE'] ='';  
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = $this->Customer->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table']=get_people_manage_table($this->Customer->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'customer_information','activity_items_id'=>'','activity_description'=>lang('customers_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		$this->load->view('people/manage',$data);
	}
	
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Customer->search_count_all($search);
			$table_data = $this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->Customer->count_all();
			$table_data = $this->Customer->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url'] = site_url('customers/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
		
	}
	
	function searchFilterCustomers()
	{	 	
		$registration_type = $this->input->post('registration_type');
		$patient_category  = $this->input->post('patient_category');
		if($registration_type=='all') $registration_type='';
		if($patient_category=='all') $patient_category='';
		$_SESSION['CUSTOMERS_SEARCH_REGISTRATION_TYPE'] = $registration_type;
		$_SESSION['CUSTOMERS_SEARCH_PATIENT_CATEGORY']  = $patient_category;		
		$from_date 		   = $this->input->post('from_date');
		$to_date 		   = $this->input->post('to_date');
		$_SESSION['CUSTOMER_SEARCH_START_DATE'] = ''; $_SESSION['CUSTOMER_SEARCH_END_DATE'] = '';
		if(isset($from_date) && $from_date!='' && $to_date!='')
		{
			$_SESSION['CUSTOMER_SEARCH_START_DATE']   = date('Y-m-d',strtotime($from_date));
			$_SESSION['CUSTOMER_SEARCH_END_DATE'] 	  = date('Y-m-d',strtotime($to_date));
		}
		$search ='';
		$method_type = strtolower(get_class());
		$per_page	 = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$config['base_url']   = site_url('customers/sorting');
		$config['total_rows'] = $this->Customer->search_count_all($search);
		$config['per_page']   = $per_page;
		$this->pagination->initialize($config);				
		$data['pagination']   = $this->pagination->create_links();
		$data['manage_table'] = get_people_manage_table_data_rows($this->Customer->search($search,$per_page),$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/*Returns customer table data rows. This will be called with AJAX.*/
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->Customer->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('customers/search');
		$config['total_rows'] = $this->Customer->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_people_manage_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Customer->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/*	Loads the customer edit form*/
	function view($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');	
		$data['person_info']=$this->Customer->get_info($customer_id);
		$data['months'] = $this->Customer->get_months();
		$data['days'] = $this->Customer->get_days();
        $data['years'] = $this->Customer->get_years();
		$data['ResidenceData'] 	  = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
		$data['OccupatiosData']	  = $this->savesetting->Get_Table_Data('occupations','occupation_name');
		if($customer_id!=-1)
		{	//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'customer_information','activity_items_id'=>$customer_id,'activity_description'=>lang('customers_information_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//
		}		
		$this->load->view("customers/form",$data);
	}	
	
	function view_card($customer_id=-1)
	{
		$data['person_info']=$this->Customer->get_info($customer_id);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'view_report_card','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_report_card'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
		$this->load->view("customers/view_out_pateint_card",$data);
	}
		 
	/*Inserts/updates a customer*/
	function save($customer_id=-1)
	{		
	  $this->check_action_permission('add_update');	
	  $this->load->model('savesetting');
	 // if($this->input->post('health_card_no')!='') {  $this->check_validHealthCard($customer_id,$validAction =1);  }	 
	  $Membership_No = ''; $Membership_CardNo = ''; $ResponseData='';$DateOfBirth=''; $cardStatus=''; $CardStatusID='';	$StatusDescription=''; $IsValidCard=''; 
	  $PFNumber = ''; $CHNationalID=''; $IsValidCard=''; $ExpiryDate=''; $LatestContribution=''; $AuthorizationStatus=''; $AuthorizationNo=''; $LatestAuthorization=''; $Remarks='';
	  $occupation_registration_no='';	  
	  if($this->input->post('member_card_no')!='')
	  {	 
		  $url="https://verification.nhif.or.tz/NHIFService/breeze/Verification/GetCard?CardNo=".$this->input->post('member_card_no');				
		  $Response_String = @file_get_contents($url);
		  if(isset($Response_String) && $Response_String!='')
		  {
		    $ResponseData = json_decode($Response_String);
		    if(isset($ResponseData) && $ResponseData!='')
			{
				$Membership_No     = stripslashes($ResponseData->MembershipNo);
				$Membership_CardNo = stripslashes($ResponseData->CardNo);				
				$CardStatusID 	   = stripslashes($ResponseData->CardStatusID);
				$StatusDescription = stripslashes($ResponseData->StatusDescription);
				$IsValidCard       = stripslashes($ResponseData->IsValidCard);
				$PFNumber          = stripslashes($ResponseData->PFNumber);
				$CHNationalID      = stripslashes($ResponseData->CHNationalID);
				$ExpiryDate        = stripslashes($ResponseData->ExpiryDate);
				$LatestContribution= stripslashes($ResponseData->LatestContribution);
				$AuthorizationStatus = stripslashes($ResponseData->AuthorizationStatus);
				$AuthorizationNo   	 = stripslashes($ResponseData->AuthorizationNo);
				if(isset($ResponseData->LatestAuthorization)){ $LatestAuthorization = stripslashes($ResponseData->LatestAuthorization);}
				$Remarks 			 = stripslashes($ResponseData->Remarks);
				if($ResponseData->CardStatus=='Active') { $cardStatus ='0';} else {$cardStatus ='1';}
				if($ResponseData->DateOfBirth!='')
				{ $Birthstring = explode('T',$ResponseData->DateOfBirth);
				  $DateOfBirth = $Birthstring[0];
				}
				if($Membership_No!='')
				{
				  $getValid = $this->Customer->GetMembershipNo($Membership_No,$customer_id);
				  if($getValid){  echo json_encode(array('success'=>false,'message'=>lang('customers_membership_no_already_used'),'person_id'=>-1));  die; }
				}
			}
		 }
		 else{ echo json_encode(array('success'=>false,'message'=>lang('customers_invalid_card'),'person_id'=>-1));  die; }					
	  }
	  
	  if($this->input->post('first_name')=='') { echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating'),'person_id'=>-1));die; }
	  $Patient_Profile_Image='';
	  $Patient_Oldimage = $this->Customer->GetPatient_ProfileImage($customer_id);	  
	  if(isset($_FILES['patient_image']['name']) && $_FILES['patient_image']['name']!='')
	  {   $this->load->library('Upload'); 
		  $size=getimagesize($_FILES['patient_image']['tmp_name']);
		  if($size[0]<150){ echo json_encode(array('success'=>false,'message'=>lang('common_patient_image_size_not_allowed'),'person_id'=>-1)); die;}
			  		
				$dir_dest = 'images/patient_images/'; 
				$handle = new Upload($_FILES['patient_image']);
				$FindImage = $this->input->post('first_name').'_'.time();
				$error = NULL;
				if ($handle->uploaded) 
				{					
					$handle->file_new_name_body = $FindImage;
					$handle->image_resize          = true;
					//$handle->image_y               = 224;
					$handle->image_x               = 150;
					$handle->image_ratio_crop = true;
					$handle->file_name_body_pre = 'thumb_';
					$handle->Process($dir_dest);

					$handle->file_new_name_body = $FindImage;
					$handle->image_ratio_crop= true;
					$handle->image_resize= true;
					$handle->image_ratio=true;
					$handle->image_ratio_x          = true;
					$handle->image_ratio_y          = true;
					
					$handle->image_x    = $size[0];
					$handle->image_y    = $size[1];
					$handle->Process($dir_dest);
					$New_patient_image = $handle->file_dst_name;			
					
					if ($handle->processed) {
					$handle-> Clean();
					}else{
					$error = $handle->error	;
					}
		        }		
	  }
	  	if(isset($New_patient_image) && $New_patient_image!='' && $New_patient_image!='0') 
		{ 
		$Patient_Profile_Image = $New_patient_image; if($Patient_Oldimage!='') { $Patient_Oldimage_thumb = 'thumb_'.$Patient_Oldimage; @unlink($dir_dest.$Patient_Oldimage); @unlink($dir_dest.$Patient_Oldimage_thumb); }
		}
		else { $Patient_Profile_Image = $Patient_Oldimage; }		  
	    
		if($this->input->post('registration_no')!=''){ $occupation_registration_no = $this->input->post('registration_no'); }
		$age_day = $this->input->post('age_day');	
		$age_month = $this->input->post('age_month');
		$age_year = $this->input->post('age_year');
		
		$person_data = array('first_name'=>$this->input->post('first_name'),
							 'last_name'=>$this->input->post('last_name'),
							 'phone_number'=>$this->input->post('phone_number'),
							 'address_1'=>$this->input->post('address_1'),
							 'address_2'=>$this->input->post('address_2'),
							 'country'=>$this->input->post('country')
							);		
		$customer_data=array('registration_type'=>	$this->input->post('registration_type'),
							 'patient_type'		=>	$this->input->post('patient_type'),
							 'patient_category'	=>	$this->input->post('patient_type_category'),
							 'insurance_company'=>	$this->input->post('insurance_company'),
							 'sex'				=>	$this->input->post('sex'),
							 'age'				=>	$age_year,
							 'age_month'		=>	$age_month,
							 'age_day'			=>	$age_day,
							 'leader'			=>	$this->input->post('leader'),
							 'religion'			=>	$this->input->post('religion'),
							 'kin'				=>	$this->input->post('kin'),		
							 'occupation'		=>	$this->input->post('occupation'),
							 'nationality'		=>	$this->input->post('nationality'),							 
							 'patient_profile_image'=>$Patient_Profile_Image,
							 'member_card_no' 	=> $Membership_CardNo,
							 'membership_no'  	=> $Membership_No,
							 'dateOfBirth'    	=> $DateOfBirth,
							 'cardStatus'     	=> $cardStatus,
							 'cardStatusID'	  	=> $CardStatusID,
							 'statusDescription'=> $StatusDescription,
							 'isvalid_Card'     => $IsValidCard,
							 'pFNumber'         => $PFNumber,
							 'cHNationalID'     => $CHNationalID,
							 'expiry_Date'      => $ExpiryDate,
							 'latestContribution'  => $LatestContribution,
							 'authorizationStatus' => $AuthorizationStatus,
							 'authorizationNo'	   => $AuthorizationNo,
							 'latestAuthorization' => $LatestAuthorization,
							 'remarks' 			   => $Remarks,
							 'occupation_registration_no'=> $occupation_registration_no,
						 );
		
		if($customer_id==-1)
		{ $registration_array = array('registration_date'=>date('Y-m-d'));
		  $person_data = array_merge($person_data,$registration_array); 
		  if($this->Customer->Generate_Customer_Account()==false) { echo json_encode(array('success'=>false,'message'=>lang('customers_error_generate_account').' '.$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));die;}
		  $temp_customer_data = array('account_number'=>$this->Customer->Generate_Customer_Account());
		  $customer_data = array_merge($temp_customer_data,$customer_data); 
		}
		if($Patient_Profile_Image!='' && $Patient_Profile_Image!='0'){ $Profile_Image = 'thumb_'.$Patient_Profile_Image;} else { $Profile_Image ='';}
		if($this->Customer->save($person_data,$customer_data,$customer_id))
		{	
			$ExistCard = $this->savesetting->existsTableData('customer_health_cards','customer_id',$customer_id);			
			if(!$ExistCard)
			{	if($customer_id==-1) $card_customer_id = $customer_data['person_id'];  else $card_customer_id = $customer_id;	
				$health_card_data = array('health_card_no'			 =>	$this->input->post('health_card_no'),
										  'customer_id'				 =>	$card_customer_id,
										  'card_issed_by_employee_id'=>	$this->Employee->get_logged_in_employee_info()->person_id,
										  'added_date'				 => date('Y-m-d H:i:s'),
										  'health_card_status'		 => '0',
										 );						 

			}
			else{ $health_card_data = array('health_card_no' =>	$this->input->post('health_card_no')); }			
			$this->savesetting->saveUpdateTable('customer_health_cards',$health_card_data,'customer_id',$customer_id);			
			if($customer_id==-1)
			{	$person_info = $this->Customer->get_info($customer_data['person_id']);
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_adding').' '. $person_data['first_name'].' '.$person_data['last_name'],'account_number'=>$person_info->account_number,'person_id'=>$customer_data['person_id']));
			    $activity_description = lang('customers_successful_adding').' '.$person_data['first_name'].' '.$person_data['last_name'];
			    $activity_items_id    = $customer_data['person_id'];
			}
			else //previous customer
			{	$person_info = $this->Customer->get_info($customer_id);
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating').' '. $person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));
				$activity_description = lang('customers_successful_updating').' '.$person_data['first_name'].' '.$person_data['last_name'];
			    $activity_items_id = $customer_id;
			}
				
			//***save employee activities in database***//
			 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
             $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
			
			//Start Code for Add Update Customers Data at Centralized Server*******//
			    /*
				$patient_profile_photo = ''; $Card_Issed_Employee_Name ='';
				$Added_By_Employee = $this->Employee->get_logged_in_employee_info()->employee_account_code;	
				if($Profile_Image!=''){ $patient_profile_photo = base_url().'images/patient_images/'.$Profile_Image; }				
				if($person_info->card_issed_by_employee_id!=''){ $Card_Issed_Employee_Name = $this->savesetting->get_Table_Column_Data('employees','employee_account_code','person_id',$person_info->card_issed_by_employee_id); }
			 	$sUrl = GOVE_TANZANIA_CENTERLIZE_SERVER.'/accessremotcontrol/Save_CutomersData'; 
				$RemoteData = "registration_type=$person_info->registration_type&patient_type_category=$person_info->patient_category&member_card_no=$person_info->member_card_no&membership_no=$person_info->membership_no&insurance_company=$person_info->insurance_company&first_name=$person_info->first_name&last_name=$person_info->last_name&phone_number=$person_info->phone_number&address=$person_info->address_2&country=$person_info->country&patient_type=$person_info->patient_type&sex=$person_info->sex&age_day=$person_info->age_day&age_month=$person_info->age_month&age_year=$person_info->age&nationality=$person_info->nationality&registration_date=$person_info->registration_date&health_card_no=$person_info->health_card_no&card_issed_by=$Card_Issed_Employee_Name&card_issed_date=$person_info->added_date&account_number=$person_info->account_number&added_by_employee_id=$Added_By_Employee&patient_profile_photo=$patient_profile_photo"; 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $sUrl);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $RemoteData);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				$Response = curl_exec($ch); 
				curl_close($ch);
				$Response =  json_decode($Response, true);
			     */			
			//End Code for Add Update Customers Data at Centralized Server*******//
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_adding_updating').' '.	$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
	}
		
	public function validate_image($image = NULL) 
	{		
		$file_name      =   $image['name'];
		$allowed_ext    =   array('jpg', 'jpeg', 'png', 'gif', 'bmp');
		$ext            =   strtolower(end(explode('.', $file_name)));
		$allowed_file_types =   array('image/jpeg','image/jpg','image/gif','image/png');
		$file_type          =   $image['type'];
		if(!in_array($ext, $allowed_ext) && !in_array($file_type, $allowed_file_types)) { return '2'; }
		if($image['size'] < 2500) {	return '3';	}
		else {	return true; }
	}
			
	function detailed_attendance()
	{	
		$report_data=$this->Customer->getAttendance();
		$tabular_data = array();
        if(isset($report_data) && $report_data!=''){
		foreach($report_data as $row)
		{
			$customer_data=$this->Customer->get_info($row['customer_id']);
			$tabular_data[] = array(array('data'=>$row['customer_id'], 'align'=> 'left'),
									array('data'=>$customer_data->first_name, 'align'=> 'left'),
									array('data'=>$customer_data->last_name, 'align'=> 'left'),
									array('data'=>$customer_data->age, 'align'=> 'left'),
									array('data'=>$customer_data->company_name, 'align'=> 'left'));
		}}		
		$columns=array(array('data'=>'Customer Id', 'align'=> 'left'),array('data'=>'First Name', 'align'=> 'left'),array('data'=>'Last Name', 'align'=> 'left'),array('data'=>'Age', 'align'=> 'left'),array('data'=>'Company', 'align'=> 'right'));
		$data = array(
			"title" => 'Customers Attendance',
			"subtitle" => 'for Last 30 days',
			"headers" => $columns,
			"data" => $tabular_data,
			"export_excel" => 0
		);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'attendance','activity_items_id'=>'','activity_description'=>lang('report_detailed_attendance_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	 $this->load->view("customers/tabular",$data);
	}
		
	function generate_barcode_labels($item_ids)
	{
		$result = array();

		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$cust_info = $this->Customer->get_info($item_id);

			$result[] = array('name' =>$cust_info->first_name.' '.$cust_info->last_name, 'id'=> number_pad($item_id, 11));
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		$this->load->view("barcode_labels", $data);
	}

	/*
	This deletes customers from the customers table
	*/
	function delete()
	{
		$this->check_action_permission('delete');
		$customers_to_delete=$this->input->post('ids');
		
		if($this->Customer->delete_list($customers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted').' '.	count($customers_to_delete).' '.lang('customers_one_or_multiple')));
			//***save employee activites in database***//
			$activity_description = lang('customers_successful_deleted').' '. count($customers_to_delete).' '.lang('customers_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_items_id'=>implode(',',$customers_to_delete),'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	
	function excel()
	{
		$data = file_get_contents("import_customers.csv");
		$name = 'import_customers.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->check_action_permission('add_update');
		$this->load->view("customers/excel_import", null);
	}
	
	/* added for excel expert */
	function excel_export() {
		$data = $this->Customer->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array('First Name', 'Last Name', 'age', 'age_month', 'age_day', 'Address 1', 'Address 2', 'City', 'registration_date', 'Country', 'Account Number', 'Taxable', 'sex', 'Company Name');
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				$r->first_name,
				$r->last_name,
				$r->age,
				$r->age_month,
				$r->age_day,
				$r->address_1,
				$r->address_2,
				$r->city,
				$r->registration_date,
				$r->country,
				//$r->comments,
				$r->account_number,
				$r->taxable ? 'y' : '',
				$r->sex,
				$r->company_name
			);
			$rows[] = $row;
		}
		
		$content = array_to_csv($rows);
		//***save employee activites in database***//
		 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_items_id'=>'','activity_description'=>lang('customers_excel_import_successful'));	
         $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
		force_download('customers_export' . '.csv', $content);
		exit;
	}

	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$this->db->trans_start();
				
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$person_data = array(
					'first_name'=>$data[0],
					'last_name'=>$data[1],
					'email'=>$data[2],
					'phone_number'=>$data[3],
					'address_1'=>$data[4],
					'address_2'=>$data[5],
					'city'=>$data[6],
					'state'=>$data[7],
					'zip'=>$data[8],
					'country'=>$data[9],
					'comments'=>$data[10]
					);
					
					$customer_data=array(
					'account_number'=>$data[11]=='' ? null:$data[11],
					'taxable'=>$data[12]=='' ? 0:1,
					'company_name' => $data[13],
					);
					
					if(!$this->Customer->save($person_data,$customer_data))
					{	
						echo json_encode( array('success'=>false,'message'=>lang('customers_duplicate_account_id')));
						return;
					}
				}
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_items_id'=>'','activity_description'=>lang('customers_excel_export_successful'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
		}
		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('customers_import_successfull')));
	}
	
	function cleanup()
	{
		$this->Customer->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('customers_cleanup_sucessful')));
	}
	
	/*get the width for the add/edit form*/
	function get_form_width()
	{			
		return 550;
	}
	
	function repeated_attendance($customer_id=-1,$attent_date=NULL)
	{
	   if($customer_id==-1){redirect(base_url());}	 
		$this->check_action_permission('add_update');
		$data['controller_name'] = strtolower(get_class());
		$data['patient_id'] = $customer_id;
		$data['manage_report'] = $this->Customer->GetAllReapeated_Attendance($customer_id);		
		$month = date('m'); $year = date('Y'); $day = date('d');		
		if(isset($attent_date) && $attent_date!='')
		{
		  $attent_date_array = explode('-',$attent_date);
		  $year = $attent_date_array[0]; $month = $attent_date_array[1]; $day = $attent_date_array[2];
		}
		$month = $month-1; if($month < 0) { $month=0; }		
		$data['year']  = $year; 	
		$data['month'] = $month; 
		$data['day']   = $day; 		
		$this->load->view('customers/manage_repeated_attendance',$data);
	}
	
	function add_repeated_attendance($customer_id=-1)
	{
	   if($customer_id==-1){redirect(base_url());}	 
		$this->check_action_permission('add_update');		
		$attent_date = $this->input->post('attent_date');
		if($attent_date!='' && $customer_id!='')
		{
          $check_attendance = $this->Customer->check_Attendance($customer_id,$attent_date);		
		  if($check_attendance){ $message='1';}
		  else {
		   $result = $this->Customer->save_Attendance($customer_id,$attent_date);
		   if($result) $message='2';  else $message='0';
		  }
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'repeated_attendance','activity_items_id'=>$customer_id,'activity_description'=>lang('added_repeated_attendance_success'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		  echo $message;die;
		}		
	}

	
	public function GetAccountData($item_id=-1)
    {	 
  	    $account_no	= $this->input->post('account_no')=='' ? null:$this->input->post('account_no');
		if($account_no==''){ echo json_encode(array('success'=>false,'data'=>'','message'=>lang('customers_account_no_empty'))); die;}			
		if(!$this->Customer->Check_AccountNo($account_no)){ echo json_encode(array('success'=>false,'message'=>lang('customers_account_no_invalid')));die;}
	    $Executedata = $this->Customer->GetAjax_Account_Data($account_no);	
		if(isset($Executedata) && $Executedata!='')
		{   
		  //if(isset($Executedata->address_1) && $Executedata->address_1!='')  $address = $Executedata->address_1; else $address = $Executedata->address_2; 		
		  echo json_encode(array('success'=>true,
								 'customer_id'=>$Executedata->person_id,
								 'customer_name'=>$Executedata->first_name.' '.$Executedata->last_name,
								 'first_name'=>$Executedata->first_name,
								 'last_name'=>$Executedata->last_name,
								 'address'=>$Executedata->address_2,
								 'age_day'=>$Executedata->age_day,
								 'age_month'=>$Executedata->age_month,
								 'age_year'=>$Executedata->age,
								 'sex'=>$Executedata->sex,
								 'city_name'=>$Executedata->city,
								 'nationality'=>$Executedata->country
								 )
							);die;
		}
		else 
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_account_no_invalid')));
		}
	}	
	
	/*Loads the update customers profile form */
	function update_profile($customer_id=-1,$customer_type=-1)
	{		
		if($customer_id==-1){ redirect('customer'); }
		$this->check_action_permission('update_profile');
		$this->load->model('savesetting');		
		$data['customer_id'] = $customer_id;
		if($customer_type=='diabetes') { $category_name = 'diabetes_measurement';} else { $category_name = 'body_measurement'; }
		$data['category_list']= $this->savesetting->Get_Table_Category_Data('measurement_category',$category_name,'category_name');
		$this->load->view("customers/form_update_profile",$data);
	}
	
	function save_profile($customer_id=-1)
	{		
		$this->check_action_permission('update_profile');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		if($customer_id==-1 || $this->input->post('category_name')=='' || $this->input->post('measurement_data')=='')
		{
		   echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'), 'person_id'=>$customer_id)); die;	
		}		
		$person_data = array('category_id'	  => $this->input->post('category_name'),							 
							 'recording_data' => $this->input->post('measurement_data'),
							 'person_id'	  => $customer_id,							 
							 'employee_id'	  => $login_uerData->person_id							
							);
		$success = $this->db->insert('body_measurement_recording',$person_data);
		if($success) 
		{ 
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_type_category'=>'measurement' ,'activity_items_id'=>$customer_id,'activity_description'=>lang('common_updated_profile_successful'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>lang('common_updated_profile_successful'),'person_id'=>$customer_id));			
		}
		else { echo json_encode(array('success'=>false,'message'=>lang('common_updated_profile_error'), 'person_id'=>$customer_id)); }
	}	

	/*Loads the search customers profile form */
	function search_patient_reports($customer_id=-1) 
	{	
        if($customer_id==-1){ redirect('customer'); }	
		$this->check_action_permission('patient_profile');	
		$data['person_id']   = $customer_id;		
		$this->load->view("customers/form_search_patient_reports",$data);
	}
	
	function view_patient_reports($customer_id=-1)   
	{	
	    $this->load->model('savesetting');	// echo 'here u'. $this->config->item('weight_symbol');  
		$start_date = $this->input->post('start_date');
		$end_date   = $this->input->post('end_date');
		if($start_date=='' && $end_date=='') { $start_date = $end_date = date('d-m-Y'); }
		$start_date = date('Y-m-d',strtotime($start_date)).' 00:00:00';
		$end_date   = date('Y-m-d',strtotime($end_date)).' 24:59:59'; 
		
		$data['temperature_data']    	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='1',$column_name='recording_time',$start_date,$end_date);	
    	$data['blood_preasure_data']	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='2',$column_name='recording_time',$start_date,$end_date);	
    	$data['pulseheart_data']     	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='3',$column_name='recording_time',$start_date,$end_date);	
		$data['respiratory_rate_data']	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='4',$column_name='recording_time',$start_date,$end_date);	
		$data['weight_data'] 		 	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='6',$column_name='recording_time',$start_date,$end_date);	
		$data['height_data'] 		 	= $this->savesetting->Get_body_Measurement_Data($customer_id,$category_type='7',$column_name='recording_time',$start_date,$end_date);	
	    $data['person_id']   = $customer_id;	   
		$data['person_info'] = $this->Customer->get_info($customer_id);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'patient_profile','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_patient_profile'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//		
		$this->load->view("customers/form_view_patient_reports",$data);
	}
		
	function pManageChart($search_type=NULL,$start_date=NULL,$end_date=NULL)
	{  	
		$this->check_action_permission('search');		
		if($search_type!='' && $start_date!='' && $end_date!='')
		{ 
			$_SESSION['CUSTOMER_SEARCH_TYPE'] 		= $search_type; 
			$_SESSION['CUSTOMER_SEARCH_START_DATE'] = date('Y-m-d',strtotime($start_date)); 
			$_SESSION['CUSTOMER_SEARCH_END_DATE'] 	= date('Y-m-d',strtotime($end_date)); 
		}
		else {unset($_SESSION['CUSTOMER_SEARCH_TYPE']);unset($_SESSION['CUSTOMER_SEARCH_START_DATE']);unset($_SESSION['CUSTOMER_SEARCH_END_DATE']); }
		
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = $this->Customer->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		//$data['per_page'] = '10000';
		$data['manage_table']=get_people_manage_table($this->Customer->get_all($data['per_page']),$this);
		$this->load->view('pManageChart/index',$data);
	}
	
	function get_List_IPD_Admitted_Patient()
	{		
		$this->load->model('ipd_in_patients');
		$this->load->helper('ipd_in_table_helper');	 	
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower('ipd_in_patient');	
		$data['manage_table'] = get_ipd_in_patient_manage_table($this->ipd_in_patients->get_requested_cutomers_admittded_ipd(),$data['controller_name']);		
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function get_List_IPD_Admitted_Patient_old()
	{		
		$this->load->model('ipd_in_patients');
		$this->load->helper('ipd_in_table_helper');	 	
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower('ipd_in_patient');
		$data['manage_table'] = get_ipd_in_patient_manage_table($this->ipd_in_patients->get_all(),$data['controller_name']);	
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	//**********START CHECK VALID MEMBERSHIP NO for registeration for making unique Function********//		
	function Exist_Membership_No()
	{	$customer_id = $this->input->post('person_id');	
		if($this->input->post('member_code')!='')
		{	 
		  $getValid = $this->Customer->GetMembershipNo($this->input->post('member_code'),$customer_id);
		  if($getValid){  echo json_encode(array('success'=>true,'message'=>lang('customers_membership_no_exist'))); die; }
		  else {  echo json_encode(array('success'=>false,'message'=>''));   }
		}	
	}

  //**************CUSTOMERS RESIDENCE FUNCTIONS************************//
	function residence($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		$data['Residence_List'] = $this->savesetting->GetResidenceList($search='');		
		$this->load->view("customers/form_residence",$data);
	}

	function saveResidenceData($item_id=-1)
	 {	
	    $this->check_action_permission('add_update');	
		$this->load->model('savesetting');
	    $residence_name = addslashes($this->input->post('residence_name')); 
		if($residence_name==''){  echo json_encode(array('success'=>false,'message'=>lang('common_residence_name_empty'))); die; }
		
		$exist = $this->savesetting->existsResidence($residence_name,$item_id);
		if($exist) { echo json_encode(array('success'=>false,'message'=>lang('common_residence_name_exist'))); die; }
		
		$item_data = array( 'residence_name' => $residence_name, 'last_updated' => date('Y-m-d H:i:s'));
		
		if($item_id==-1) 
		{ $temp_array = array('added_date' => date('Y-m-d'), 'employee_id' => $this->Employee->get_Logged_SessionData('person_id')); 
		  $item_data = array_merge($item_data, $temp_array);
		}
		if($this->savesetting->saveResidenceData($item_data, $item_id))
		{	
			if($item_id==-1){ echo json_encode(array('success'=>true,'message'=>lang('common_residence_name_added'))); die; } 
			else{ echo json_encode(array('success'=>true,'message'=>lang('common_residence_name_update'))); die; }					
		}
		else{ echo json_encode(array('success'=>true,'message'=>lang('common_residence_name_error')));  }			
	 }	
	
	function searchResidence($customer_id=-1) 
	 {	
	    $this->load->model('savesetting');
		$search = addslashes($this->input->post('searchKeyword')); 
		$search_data = $this->savesetting->GetResidenceList($search);
		echo json_encode(array('success'=>true,'htmldata'=>$search_data)); 
	 }
	 
	 function getResidenceData($customer_id=-1)
	 {	$this->load->model('savesetting');
		$item_id = addslashes($this->input->post('item_id'));
		$category_type = addslashes($this->input->post('category_type'));
		echo $search_data=$this->savesetting->GetResidenceData_BYID($item_id);die;
	 }
	
     function deleteResidenceData($customer_id=-1)
	 {	$this->load->model('savesetting');	
		$message='';
		$value = $this->uri->segment(3);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);	
		$success=$this->savesetting->deleteResidenceData($value);	
		if($success==1)
		{  $message = lang('common_successful_delete_selected_items'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$activity_items_id,'activity_description'=>lang('lab_services_successful_deleted'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {  $message = lang('common_error_deleted_items'); }
		echo $message; die;	
	}

	function search_patients()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->Customer->Search_Patients_Options($search_keyword); die;
	}
    
	function search_patient_BYID($customer_id=-1)
	{	
	  if($customer_id!=-1)
	  {
		$customer_info   = $this->Customer->get_info($customer_id);
		$custmer_data = $customer_info->account_number. ' - ' .$customer_info->first_name.' '.$customer_info->last_name;
		echo json_encode(array('success'=>true,'custmer_data'=>$custmer_data)); die;
	  }
	  else {echo json_encode(array('success'=>false,'custmer_data'=>'')); die;}
	}
	
	
	function Customer_ProfileData($customer_id=-1)
	{	
	  if($customer_id!=-1)
	  {  
		$profile_Datafield  = $this->input->post('profile_Datafield');
		$customer_info   	= $this->Customer->get_info($customer_id);
		$custmer_data 		= $customer_info->$profile_Datafield;
		echo json_encode(array('success'=>true,'custmer_data'=>$custmer_data)); die;
	  }
	  else {echo json_encode(array('success'=>false,'custmer_data'=>'')); die;}
	}
	
     //**************START MANAGE CUSTOMERS OCCUPATION FUNCTIONS************************//
	function occupations($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		$data['Residence_List'] = $this->savesetting->GetOccupationList($search='');			
		$this->load->view("customers/form_occupations",$data);
	}

	function saveOccupationData($item_id=-1)
	 {	
	    $this->check_action_permission('add_update');	
		$this->load->model('savesetting');
	    $occupation_name = addslashes($this->input->post('occupation_name')); 
		$group_name = addslashes($this->input->post('group_name')); 
		if($occupation_name==''){ echo json_encode(array('success'=>false,'message'=>lang('common_occupation_name_empty'))); die; }
		if($group_name=='')		{ echo json_encode(array('success'=>false,'message'=>lang('common_occupation_group_empty'))); die; }
		
		$exist = $this->savesetting->existsOccupation($occupation_name,$item_id);
		if($exist) { echo json_encode(array('success'=>false,'message'=>lang('common_residence_name_exist'))); die; }
		
		$item_data = array('occupation_name' => $occupation_name,'occupation_group' => $group_name, 'last_updated' => date('Y-m-d H:i:s'));
		
		if($item_id==-1) 
		{ $temp_array = array('added_date' => date('Y-m-d'), 'employee_id' => $this->Employee->get_Logged_SessionData('person_id')); 
		  $item_data = array_merge($item_data, $temp_array);
		}
		if($this->savesetting->saveOccupationData($item_data, $item_id))
		{	
			if($item_id==-1){ echo json_encode(array('success'=>true,'message'=>lang('common_occupation_name_added'))); die; } 
			else{ echo json_encode(array('success'=>true,'message'=>lang('common_occupation_name_update'))); die; }					
		}
		else{ echo json_encode(array('success'=>true,'message'=>lang('common_occupation_name_error')));  }			
	 }	
	
	function searchOccupation($customer_id=-1) 
	 {	
	    $this->load->model('savesetting');
		$search = addslashes($this->input->post('searchKeyword')); 
		$search_data = $this->savesetting->GetOccupationList($search);
		echo json_encode(array('success'=>true,'htmldata'=>$search_data)); 
	 }
	 
	 function getOccupationData($customer_id=-1)
	 {	$this->load->model('savesetting');
		$item_id = addslashes($this->input->post('item_id'));
		$category_type = addslashes($this->input->post('category_type'));
		echo $search_data=$this->savesetting->GetOccupationData_BYID($item_id);die;
	 }
	
     function deleteOccupationData($customer_id=-1)
	 {	$this->load->model('savesetting');	
		$message='';
		$value = $this->uri->segment(3);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);	
		$success=$this->savesetting->deleteOccupationData($value);	
		if($success==1)
		{   $message = lang('common_successful_delete_selected_items'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$activity_items_id,'activity_description'=>lang('lab_services_successful_deleted'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else { $message = lang('common_error_deleted_items'); }
		echo $message; die;	
	}
    //**END MANAGE CUSTOMERS OCCUPATION FUNCTIONS**//	
	
	 function getOccupationGroupData($customer_id=-1)
	 {	$this->load->model('savesetting');
		$occupation_id = $this->input->post('occupation_id');
		$group_name = $this->input->post('group_name');
		echo $search_data = $this->savesetting->GetOccupationGroup($occupation_id,$group_name);die;
	 }
	 
	 function check_validHealthCard($customer_id = NULL,$validAction =0)
	 {  
		$Card_NO = $this->input->post('health_card_no');
		if($Card_NO=='') { echo json_encode(array('success'=>false,'message'=>lang('common_health_card_no_required'))); die; }
		if($Card_NO!='') 
		{   
			if(strlen($Card_NO)!=19) { echo json_encode(array('success'=>false,'message'=>lang('common_health_card_no_minlength'))); die; }
			//$this->load->model('savesetting');
			$ExistCard = $this->Customer->exists_Customer_HealthCard($Card_NO,$customer_id);
			if($ExistCard){ echo json_encode(array('success'=>false,'message'=>lang('common_health_card_no_already_exist'))); die; }
			
			//******Check data at Centeralize server***********//
			$Customer_Account = $this->Customer->Get_Customer_Account($customer_id);
			$sUrl = GOVE_TANZANIA_CENTERLIZE_SERVER.'/accessremotcontrol/check_ValidHealthCard'; 
			$sData = "health_card_no=$Card_NO&account_no=$Customer_Account";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $sUrl);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			$Response = curl_exec($ch); 
			curl_close($ch);
			$Response =  json_decode($Response, true); 
			if($validAction==1 && $Response['success']==false){ echo json_encode(array('success'=>false,'message'=>$Response['message'])); die; }
			if($validAction==0)
			{
				if($Response['success']){ echo json_encode(array('success'=>true,'message'=>$Response['message'])); die; }
				else { echo json_encode(array('success'=>false,'message'=>$Response['message'])); die;}
			}
			//******Check data at Centeralize server***********/
		}
		else { if($validAction==0) echo json_encode(array('success'=>true,'message'=>lang('common_health_card_no_required'))); }
	 }
	 
	
}
?>