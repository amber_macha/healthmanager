<?php
require_once ("secure_area.php");
class Home extends Secure_area 
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('flvplayer');
	}
	
	function index()
	{ 
	   $data['controller_name']=strtolower(get_class());
	   $this->load->view("home",$data);
	}
	
	function view_videos($videos_id)
	{		
		$this->check_action_permission('add_update');
		$videos = $this->db->dbprefix('hms_videos');
		$video_title =''; $video_name = '';
		$ExecuteQuery = $this->db->query("select * from $videos where id='".$videos_id."'");
		if($ExecuteQuery->num_rows()>0)
		{
		$ExecuteData = $ExecuteQuery->row();
	    $video_title = $ExecuteData->video_title;
		$video_name= $ExecuteData->video_name;
		}		
		$data['video_title']= $video_title;
		$data['video_name']= $video_name;		
		$this->load->view("videos/playvideo",$data);
	}
	
	function logout()
	{
		$this->Employee->logout();
	}
	
	//login employee for any ativities
	function login()
	{  
	   $user_data = $this->Employee->get_logged_in_employee_info();
	   $this->session->set_userdata('EMPLOYEE_PSSWORD_AUTHROZIE','0');         	   
	   $data['controller_name']= strtolower(get_class());
	   $this->load->view("login/action_authorization_login",$data);
	}
	
	//login employee for any ativities
	function authorization_login()
	{  
		$user_data = $this->Employee->get_logged_in_employee_info();
		$customer_id = $user_data->person_id;	   
		if($this->input->post('user_password')=='') { echo json_encode(array('success'=>false,'message'=>lang('employees_password_required'),'item_id'=>$customer_id));die; }
		$password  = $this->input->post('user_password');
		$user_data = $this->Employee->get_logged_in_employee_info();
		$current_siteurl = $this->Employee->get_Logged_SessionData('CURRENT_SITEURL');
		$query = $this->db->get_where('employees', array('username' => $user_data->username,'password'=>md5($password),'person_id' => $user_data->person_id, 'deleted'=>0), 1);
		if($query->num_rows() ==1)
		{   $current_date = date('d-m-Y H:i:s');
			$Login_Data = array('EMPLOYEE_START_LOGIN_TIME' => $current_date,'EMPLOYEE_END_LOGIN_TIME' => $current_date,'EMPLOYEE_PSSWORD_AUTHROZIE'=> '1' );
			$this->session->set_userdata($Login_Data);
			echo json_encode(array('success'=>true,'message'=>lang('employees_login_successful'),'current_siteurl'=>$current_siteurl,'item_id'=>$customer_id));
		} 
		else {echo json_encode(array('success'=>false,'message'=>lang('employees_password_invalid'),'item_id'=>$customer_id));}
	}
	
}
?>