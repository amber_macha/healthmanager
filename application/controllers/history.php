<?php
require_once ("person_controller.php");
class History extends Person_controller
{
	function __construct()
	{
		parent::__construct('history');
		$this->load->model('patienthistory');
	}

	function index($customer_id=-1)
	{
	    if($customer_id==-1){redirect(base_url());}
		$this->check_action_permission('search');
		$exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers/');}
		$Diabetes_Customer_Type = $this->uri->segment(4);
		if($Diabetes_Customer_Type!='diabetes') { $Diabetes_Customer_Type ='customers'; }
		$data['Diabetes_Customer_Type'] = $Diabetes_Customer_Type;
		$config['base_url']   = site_url('customers/sorting');
		$config[''] = '';
		$data['controller_name'] 	= strtolower(get_class());
		$data['form_width'] 		= $this->get_form_width();
		$data['person_info']        = $this->Customer->get_info($customer_id);	
		$data['manage_comment'] 	= $this->patienthistory->search($customer_id, $search = '', $data['Diabetes_Customer_Type']); 
		$data['manage_history_menu']= get_patient_history_menu($customer_id);	
		$data['SubGroupsData']	    = $this->Employee->get_AllEmployee_Groups($parent_id=6);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'history_information','activity_items_id'=>'','activity_description'=>lang('history_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//		
		$this->load->view('customers_history/manage',$data);
	}	
	
	function save($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$login_uerData = $this->Employee->get_logged_in_employee_info();
		$history=$this->db->dbprefix('customers_history');	
		$person_data = array(
		'person_id'=>$customer_id,
        'employee_id'=>$login_uerData->person_id, 		
		'comments'=>$this->input->post('comments'),
		'add_date'=>date('Y-m-d h:i:s')
		);

		if($customer_id!='-1' && $this->input->post('comments')!='')
		 {  			
			$success = $this->db->insert($history,$person_data);
			echo json_encode(array('success'=>true,'message'=>lang('history_successful_adding'), 'person_id'=>$customer_id) );
			//***save employee activities in database***//
			 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'history','activity_items_id'=>$customer_id,'activity_description'=>lang('history_successful_adding'));	
             $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//	
		 }
		else
		{	
			echo json_encode(array('success'=>false,'message'=>lang('history_error_adding_adding'), 'person_id'=>$customer_id));
		}
	}		

	function delete($observations = NULL)
	{		
		$this->check_action_permission('delete');
		$cmments_to_delete=$this->input->post('ids');
		$bad_words = array('on');	
		foreach($cmments_to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($cmments_to_delete[$key]);	} }	
		if($this->patienthistory->delete_list($cmments_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('comment_successful_deleted').' '. count($cmments_to_delete).' '.lang('comment_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$cmments_to_delete);
			$activity_description = lang('comment_successful_deleted').' '. count($cmments_to_delete).' '.lang('comment_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'clinical_daignoses','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('comment_cannot_be_deleted')));
		}
	}	

	function search($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$search		 =	$this->input->post('search');
		$method_type =  $this->input->post('method_type');	
		$Diabetes_Customer_Type = $this->uri->segment(4);
		$find_group_id =  $this->input->post('find_category_id');	
		if($method_type=='search_by_group'){ $search_data =	$this->patienthistory->searchFilterComment($customer_id,$find_group_id, $Diabetes_Customer_Type); }
		else {$search_data=$this->patienthistory->search($customer_id,$search, $Diabetes_Customer_Type); }		
		$pagination='';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));				
	}	
	
	function getFilterComment($customer_id=-1,$Diabetes_Customer_Type)
	{	
		$this->check_action_permission('search');
		$find_group_id	=	$this->input->post('Group_id');		
		$search_data	=	$this->patienthistory->searchFilterComment($customer_id,$find_group_id,$Diabetes_Customer_Type);		
		$pagination		=	'';
		echo json_encode(array('manage_table' => $search_data, 'pagination' => $pagination));				
	}		

	/* Gives search suggestions based on what is being searched for */
	function suggest($customer_id=-1)
	{
		$suggestions = $this->patienthistory->get_search_suggestions($customer_id,$this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function patient_report($customer_id=-1)
	{
	   if($customer_id==-1){redirect(base_url());}
		$this->load->model('diabetescare');	  
		$this->load->language('diabetes_care');		
		$this->check_action_permission('search');
		$config['base_url'] = site_url('customers/sorting');
		$config['total_rows'] = '';
		$data['controller_name'] = strtolower(get_class());
		$data['form_width'] = $this->get_form_width();
		$data['person_info']=$this->Customer->get_info($customer_id);	
		$start_date = date('Y-m-d').' 00:00:00';
		$end_date =date('Y-m-d').' 23:59:59';		
		//$data['medicine_report_array'] = $this->patienthistory->get_Medicine_ReportArray($customer_id);
		$data['manage_report'] = $this->patienthistory->get_search_Report( $customer_id, $start_date, $end_date);			
		if($_POST)
		{
			 $documentation 	 = $this->input->post('documentation');
			 $documentation_type = $this->input->post('documentation_type');
			 $report_type 	 	 = $this->input->post('report_type'); 
			 $end_date			 = $this->input->post('end_date');
			 $start_date		 = $this->input->post('start_date');
			 $end_date			 = $this->input->post('end_date');
		     if($start_date!='' && $end_date!='' && $start_date <= $end_date)
				{ $start_date = date('Y-m-d',strtotime($start_date)); $end_date = date('Y-m-d',strtotime($end_date));
				  $data['manage_report'] = $this->patienthistory->get_search_Report( $customer_id, $start_date, $end_date ); 	
				}
		}
			 
		$this->load->view('customers_history/manage_patient_report',$data);
	}
	
	function pdf_ReportTables($customer_id=NULL, $start_date=NULL, $end_date=NULL)
	{
		$this->load->library('pdf/cezpdf');
		$patient_pdf_Report = $this->patienthistory->get_patient_pdf_Report( $customer_id, $start_date, $end_date );
        //print_r($patient_pdf_Report); die;		
		$db_data[] = array('investigation' => 'Jon Doe2', 'reports' => '111-222-3333', 'medicine' => 'jdoe@someplace.com', 'history_clinical_note' => 'jdoe@someplace.com');
		$db_data[] = array('investigation' => 'Jane Doe', 'reports' => '222-333-4444', 'medicine' => 'jane.doe@something.com', 'history_clinical_note' => 'jdoe@someplace.com');
		//$db_data[] = array('name' => 'Jon Smith', 'phone' => '333-444-5555', 'email' => 'jsmith@someplacepsecial.com');
		
		$col_names = array(
			'investigation' => lang('investigation'),
			'reports' => lang('reports'),
			'medicine' => lang('medicine'),
			'history_clinical_note' => lang('history_clinical_note'),
			
		);
		
		$this->cezpdf->ezTable($patient_pdf_Report, $col_names, 'Contact List', array('width'=>550));
		$this->cezpdf->ezStream();
	}
	
	function patient_report_search($customer_id=-1)
	{
		if($customer_id==-1){ redirect(base_url());}	 
		$this->check_action_permission('search');
		 $documentation		 = $this->input->post('patient_documentation');
		 $documentation_type = $this->input->post('patient_documentation_type');
		 $report_type 		 = $this->input->post('view_report_type');
		 $end_date			 = $this->input->post('end_date');
		 $start_date		 = $this->input->post('start_date');
		 $end_date			 = $this->input->post('end_date');		 
		 
		//generate excle file**//
		$data = $this->Customer->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array('First Name', 'Last Name', 'E-Mail', 'Phone Number', 'Address 1', 'Address 2', 'City', 'State', 'Zip', 'Country', 'Comments', 'Account Number', 'Taxable', 'Company Name');
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				$r->first_name,
				$r->last_name,
				$r->email,
				$r->phone_number,
				$r->address_1,
				$r->address_2,
				$r->city,
				$r->state,
				$r->zip,
				$r->country,
				$r->comments,
				$r->account_number,
				$r->taxable ? 'y' : '',
				$r->company_name
			);
			$rows[] = $row;
		}
		
		$content = array_to_csv($rows);
		//***save employee activites in database***//
		 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type' => 'customers','activity_items_id'=>'','activity_description'=>lang('customers_excel_import_successful'));	
         $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
		//force_download('customers_export' . '.csv', $content); exit;
		//generate excle file**//
		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'call_report','activity_items_id'=>$customer_id,'activity_description'=>lang('common_customer_view_call_reports'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
		if($start_date!='' && $end_date!='' && $start_date <= $end_date)
		{ $start_date = date('Y-m-d',strtotime($start_date)); $end_date = date('Y-m-d',strtotime($end_date));
		  $manage_report = $this->patienthistory->get_search_Report( $customer_id, $start_date, $end_date );	
		  if($manage_report!=''){echo $manage_report; die;}
        }		
		die;
	}
	
	function viewAppointment($item_id=-1)
	{	
		if($item_id==-1 ){redirect(base_url());} 
		$this->check_action_permission('add_update');
		$data['appoinment_list'] = $this->functions->Get_All_Wating_AppointList($item_id);	
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'patient_appointment','activity_items_id'=>'','activity_description'=>lang('common_customer_view_patient_appointment'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
		$this->load->view("customers_history/viewAppointment",$data);
	}
	
	function updateappointment($action_type=-1,$item_id=-1)
	{	
		if($action_type==-1 && $item_id==-1 ){ redirect(base_url()); } 
	    $result = $this->functions->update_Wating_Appoint($item_id ,$action_type );	
		if( $result)
		{
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'patient_appointment','activity_items_id'=>$item_id,'activity_description'=>lang('history_patient_waiting_update_success'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>lang('history_patient_waiting_update_success'))); die;
		}	 
		else { echo json_encode(array('success'=>false,'message'=>lang('history_patient_waiting_update_error'))); }	
	}
	
	function get_AppointmentSection($customer_id=-1)
	{	
	  if($customer_id!=-1) { echo json_encode(array('success'=>true,'dataList'=>$this->functions->Get_Patient_Wating_AppointListSection($customer_id))); }
	}
		
	function view($customer_id=-1,$clinical_note_id=-1,$Customer_Type='customers',$action = 'add')
	{	
		$this->check_action_permission('add_update'); 
		$this->load->model('savesetting'); 
		//$data['Diabetes_Customer_Type'] = $this->uri->segment(5);		
		$data['Diabetes_Customer_Type'] = $Customer_Type;	
		$data['person_id']     = $customer_id;	
		$data['clinical_note_id'] = $clinical_note_id;	
		$data['person_info']   = $this->patienthistory->get_clinical_note_info($clinical_note_id);
		$data['physical_info']   = $this->savesetting->Get_Table_Row_Data('physical_examination','history_id',$clinical_note_id);
		
		$ClinicalDaignoses     = $this->patienthistory->get_patient_Clinical_Diagnosis($clinical_note_id); 
		$data['diseases_list'] = $this->Customer->GetDiseasList_ClinicalNoteMultiple($category_type = 'clinic_diseases', $ClinicalDaignoses, $search='',$action);
		$main_complains =''; $disabled ='';
		if($clinical_note_id==-1) 
		{   
			$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('customers_history')." where person_id='".$customer_id."' order by id desc limit 0,1");
			if($ExecuteQuery->num_rows()>0) { $ExecuteData = $ExecuteQuery->row(); $main_complains = $ExecuteData->main_complains; }
		}
		else { $main_complains  = $data['person_info']->main_complains; }
		if($action=='view') { $disabled = 'disabled="disabled"';}
		$data['main_complains'] = $main_complains;
		$data['disabled_data']  = $disabled;
		$data['action']  		= $action;		
		$this->load->view("customers_history/form_patient_note",$data);
	}	

	function searchClinicalDiseasList($customer_id=-1)
	{	
		$this->check_action_permission('search');
		$clinical_note_id = $this->input->post('clinical_note_id'); 
		$search 		  = addslashes($this->input->post('service')); 
		$checkedval	   	  = addslashes($this->input->post('checkedval'));
		$person_info      = $this->patienthistory->get_clinical_note_info($clinical_note_id); 
		$ClinicalDaignoses= $this->patienthistory->get_patient_Clinical_Diagnosis($clinical_note_id);  		
		$success = $this->Customer->GetDiseasList_ClinicalNoteMultiple($category_type = 'clinic_diseases',$ClinicalDaignoses,$search);
		echo $success; die;	
	}
	
	function saveClinicalDianosis($customer_id=-1,$clinical_note_id=-1,$action = NULL)
	{		
		$this->check_action_permission('add_update');
		$this->load->model('savesetting'); 		
		$Diagnosis_Data	= array();	
		$main_complains		=	mysql_real_escape_string($this->input->post('main_complains'));
		$present_illness 	=	mysql_real_escape_string ($this->input->post('present_illness'));
		if($main_complains=='' || $present_illness=='') {echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'), 'person_id'=>-1)); die;}
		$login_uerData = $this->Employee->get_logged_in_employee_info();		
		$item_data = array( 'main_complains'				=>	$main_complains,
						    'historical_present_illness' 	=>	$present_illness, 
							'physical_examination_details' 	=>	mysql_real_escape_string ($this->input->post('physical_examination_details')), 
							'system_examination_details' 	=>	mysql_real_escape_string ($this->input->post('system_examination_details')), 
							'admission_status'			 	=> $this->input->post('admission_status'),
							'admission_status_description' 	=> $this->input->post('admission_status_content'),
							'blood_transfussion'		 	=> $this->input->post('admission_status'),
							'blood_transfussion_description'=> $this->input->post('blood_transfussion_content'),
							'surgery_status'				=> $this->input->post('admission_status'),
							'surgery_status_description'	=> $this->input->post('surgery_status_content'),
							'allegis_food_drugs'			=> $this->input->post('admission_status'),
							'allegis_food_drugs_description'=> $this->input->post('allegis_food_drugs_content'),
							'person_id'					 	=> $customer_id,
							'employee_id'				 	=> $login_uerData->person_id
						  );
		 $physical_data = array('person_id'					=> $customer_id,
								'general_appearance'			=> $this->input->post('general_appearance'),
								'mental_status' 				=> $this->input->post('mental_status'), 						
								'breasts_stage'			 		=> $this->input->post('breasts_stage'),
								'pubic_hair_stage'			 	=> $this->input->post('pubic_hair_stage'),
								'genitalia_normal' 				=> $this->input->post('genitalia_normal'),
								'testes_normal_right'		 	=> $this->input->post('testes_normal_right'),
								'testes_normal_left'			=> $this->input->post('testes_normal_left'),
								'eye_pupils_perrl'				=> $this->input->post('eye_pupils_perrl'),
								'eye_lenses_right'				=> $this->input->post('eye_lenses_right'),
								'eye_lenses_left'				=> $this->input->post('eye_lenses_left'),
								'thyroid_right_lobe'			=> $this->input->post('thyroid_right_lobe'),
								'thyroid_left_lobe'			 	=> $this->input->post('thyroid_left_lobe'),
								'history_lungs'				 	=> $this->input->post('history_lungs'),					
								'heart_regular'			 		=> $this->input->post('admission_status'),
								'heart_murmur' 					=> $this->input->post('heart_murmur'),
								'liver_enlarged'		 		=> $this->input->post('liver_enlarged'),
								'spleen_enlarged'				=> $this->input->post('spleen_enlarged'),
								'tenderness'					=> $this->input->post('tenderness'),
								'ascites'						=> $this->input->post('ascites'),
								'tremor'						=> $this->input->post('tremor'),
								'reflexes_biceps'				=> $this->input->post('reflexes_biceps'),
								'reflexes_knees'			 	=> $this->input->post('reflexes_knees'),
								'reflexes_ankles'				=> $this->input->post('reflexes_ankles'),					
								'skin_acne'						=> $this->input->post('skin_acne'),
								'skin_scoliosis'			 	=> $this->input->post('skin_scoliosis'),
								'skin_striae'				 	=> $this->input->post('skin_striae'),					
								'skin_colour'			 		=> $this->input->post('skin_colour'),
								'injection_sites' 				=> $this->input->post('injection_sites'),
								'right_dorsalis_pedis_pulse'	=> $this->input->post('right_dorsalis_pedis_pulse'),
								'right_tibialis_posterior_pulse'=> $this->input->post('right_tibialis_posterior_pulse'),
								'right_ulcers'					=> $this->input->post('right_ulcers'),
								'right_signs_of_infection'		=> $this->input->post('right_signs_of_infection'),
								'right_toenail_abnormalities'	=> $this->input->post('right_toenail_abnormalities'),
								'right_monofilament_test_normal'=> $this->input->post('right_monofilament_test_normal'),
								'left_dorsalis_pedis_pulse'		=> $this->input->post('left_dorsalis_pedis_pulse'),
								'left_tibialis_posterior_pulse'	=> $this->input->post('left_tibialis_posterior_pulse'),
								'left_ulcers'					=> $this->input->post('left_ulcers'),					
								'left_signs_of_infection'		=> $this->input->post('left_signs_of_infection'),
								'left_toenail_abnormalities'	=> $this->input->post('left_toenail_abnormalities'),
								'left_monofilament_test_normal' => $this->input->post('left_monofilament_test_normal'),
								'describe_deformities_foot'		=> $this->input->post('describe_deformities_foot')
							  );	
		if($this->savesetting->save_data($table_name='customers_history',$item_data,$clinical_note_id))
		{
			if($clinical_note_id==-1){ $history_id = $this->db->insert_id(); } else { $history_id = $clinical_note_id;}
			if(isset($_POST['lab_category']) && $_POST['lab_category']!='')
			{				 			
				  $Diagnosis_Data = array('history_id' => $history_id,	
										  'person_id'  => $customer_id,	
									      'added_date' => date('Y-m-d H:i:s'),
										  'employee_id'=> $login_uerData->person_id
										 );				
				$this->savesetting->save_clinic_diagnoses_data($Diagnosis_Data,$_POST['lab_category'],$customer_id,$history_id);
			}
			if($action=='diabetes')
			{
			 $Temp_Array = array('history_id' => $history_id);	
			 $physical_data = array_merge($physical_data, $Temp_Array); 
			 $this->savesetting->saveUpdateTable('physical_examination',$physical_data,'history_id',$history_id);			 
			}
			if($clinical_note_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('history_successful_clinical_adding'),'person_id'=>$customer_id));	
				$activity_description = lang('history_successful_clinical_adding');				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('history_successful_clinical_updating'),'person_id'=>$customer_id));
				$activity_description = lang('history_successful_clinical_updating');
			}
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'clinical_note','activity_items_id'=>$customer_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//			
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('history_error_adding_updating_clinical'),'person_id'=>$customer_id));
		}
	}
	
	function getClinic_PreviousReports($customer_id=-1)
	 {	
		$searchstring  = '';
		$clinical_note_id = addslashes($this->input->post('clinical_note_id'));
		if($clinical_note_id>0) { $searchstring = " and id < '".$clinical_note_id."' "; }
		$ExecuteQuery = $this->db->query("select * from ".$this->db->dbprefix('customers_history')." where person_id='".$customer_id."' $searchstring order by id desc limit 0,1");
		if($ExecuteQuery->num_rows()>0) 
		   { 
		     $ExecuteData = $ExecuteQuery->row(); 
			 $data['person_info'] = $ExecuteQuery->row();
			 $data['disabled_data'] = '';
			  $this->load->view("customers_history/previouspatient_loadReports",$data); 
			 
			}	
	}			
	 
	//*********START DIAGNOSES CODE************************************//
	function diseasesForm($customer_id=-1)
	{
		if($customer_id==-1 ){redirect(base_url());} 
		$this->check_action_permission('add_update');
		$search='';
		$data['person_id'] = $customer_id;		
		$this->load->model('labtest');
		$data['diseases_list'] = $this->labtest->GetSearchAllDiseasList($search,$type_diseases='clinical_daignoses');
		$this->load->model('medicinetest');
		$data['medicine_list'] = $this->medicinetest->Search_MedicneDrugs_Catgory($search='',$value='');	
		$this->load->view("customers_history/clinical_diabetes_viewform",$data);
	}	
	
	function searchAllDiseasList($customer_id=-1)
	 {	
	    $this->load->model('labtest');
		$search = addslashes($this->input->post('service')); 
		echo $search_data = $this->labtest->GetSearchAllDiseasList($search,$type_diseases='clinical_daignoses');die;
	 }

	function profileview($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$data['person_info']=$this->Customer->get_info($customer_id);
		 $data['months'] = $this->Customer->get_months();
        $data['days'] = $this->Customer->get_days();
        $data['years'] = $this->Customer->get_years();
		$this->load->view("customers/form",$data);
	}

	function physical_examination($customer_id=-1,$clinical_note_id=-1)
	{	
		$this->check_action_permission('add_update'); 
		$this->load->model('savesetting');
		$data['Diabetes_Customer_Type'] = $this->uri->segment(5);	
		$data['person_id']     = $customer_id;	
		$data['clinical_note_id'] = $clinical_note_id;	
		$data['person_info']   		= $this->patienthistory->get_clinical_note_info($clinical_note_id);
		
		if($clinical_note_id==-1) {  $disabled ='';}
		$data['disabled_data']  = $disabled;		
		$this->load->view("customers_history/form_physical_examination",$data);
	}
	
	function get_form_width()
	{
		return 550;
	}
	
}

?>