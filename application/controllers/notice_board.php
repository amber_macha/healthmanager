<?php
require_once ("person_controller.php");
class Notice_board extends Person_controller
{
	function __construct()
	{
		parent::__construct('notice_board');
		$this->load->model('notice_boards');
		$this->load->language('notice_board');
		$this->load->helper('viewdatatable');
	}
		
	function index()
	{  	
		$this->check_action_permission('search');
		$data['method_type'] = 'notice_board';	
		$data['form_width']=$this->get_form_width();
		$data['controller_name']=strtolower(get_class());
		$data['per_page']  = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
	    $data['manage_table']=get_manage_table($data['method_type'],$this->notice_boards->get_all($this->config->item('notice_board_limit')),$this);		
		$data['pagination']  = '';
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'notice_board_details','activity_items_id'=>'','activity_description'=>lang('notice_board_list_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('notice_board/manage',$data);
	}	
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function search()
	{
		$this->check_action_permission('search');
		$method_type = 'notice_board';
		$search=$this->input->post('search');
		$per_page = $this->config->item('notice_board_limit');
		$search_data=$this->notice_boards->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$data['pagination'] = '';
		$data['manage_table']= get_manage_table_data_rows($method_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/*Gives search suggestions based on what is being searched for */
	function suggest()
	{
		$suggestions = $this->notice_boards->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/* Loads the customer edit form */
	function view($view_type = NULL, $item_id = -1)
	{	
		if($view_type=='edit'){	$this->check_action_permission('add_update'); }
		if($view_type=='edit' && $item_id==-1 && $this->notice_boards->count_all()==$this->config->item('notice_board_limit')) { echo lang('notice_board_limit_overload'); die;}
		else {	$this->check_action_permission('view'); }
		$data['view_type'] 	 = $view_type;
		$data['item_info'] = $this->notice_boards->get_info($item_id);
		if($item_id!=-1 && $view_type=='show'){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'notice_board_details','activity_items_id'=>$item_id,'activity_description'=>lang('notice_board_details_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}
		$this->load->view("notice_board/form",$data);
	}	
	
	/* Inserts/updates a customer */
	function save($item_id=-1)
	{		
	  $this->check_action_permission('add_update');	
	  if($this->input->post('notice_board_subject')=='' || $this->input->post('notice_board_description')=='' || $this->input->post('from_date')=='' || $this->input->post('to_date')=='') 
	  {	
		echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'person_id'=>-1));die; 
	  }	 
	   if($item_id==-1 && $this->notice_boards->count_all()==$this->config->item('notice_board_limit')) { echo json_encode(array('success'=>false,'message'=>lang('notice_board_limit_overload'),'person_id'=>-1));die; }

	   $item_data =  array( 'notice_board_subject'	  =>	stripslashes($this->input->post('notice_board_subject')),
							'notice_board_description'=>	stripslashes($this->input->post('notice_board_description')),
							'from_date'	=>	date('Y-m-d',strtotime($this->input->post('from_date'))),
							'to_date'	=>	date('Y-m-d',strtotime($this->input->post('to_date'))),							
							'status'	=>	$this->input->post('notice_status')
							);		
        if($item_id=='-1')
		{
			$temp_array = array('employee_id' => $this->Employee->get_Logged_SessionData('person_id'),'added_date'=>date('Y-m-d H:i:s'));
			$item_data  = array_merge($item_data,$temp_array);			
			//$item_id = $this->notice_boards->save($item_data,$item_id);
        }
		else{
			$temp_array = array('updated_by_employee_id' => $this->Employee->get_Logged_SessionData('person_id'),'last_updated_date'=>date('Y-m-d H:i:s'));
			$item_data  = array_merge($item_data,$temp_array);
		}		
	
		if($this->notice_boards->save($item_data,$item_id))
		{	//**Notification confirmation Email for all employee**//
			if($this->config->item('notice_board_email_confirmation')==1)
			{   $this->load->model('mailboxes');
				$Login_Employee_id = $this->Employee->get_Logged_SessionData('person_id');
				$Mail_Data = array('from_employee_id'=> $Login_Employee_id,
									'mail_subject'	 => stripslashes($this->input->post('notice_board_subject')),
									'mail_body'		 => stripslashes($this->input->post('notice_board_description')),
									'read_status'	 => '0',
									'added_date'	 => date('Y-m-d H:i:s'));					
				$EmployeeData =  $this->Employee->get_All_Employee_By_Groups();
				if($EmployeeData->num_rows()>0)
				{
				foreach($EmployeeData->result() as $RowData)
				{ 	$temp_array = array('to_employee_id' => $RowData->person_id);
					$Mail_Data  = array_merge($Mail_Data,$temp_array);
					$success =  $this->mailboxes->save($Mail_Data,$item_id);							
				}}			  
			}
			if($item_id==-1)
			{  $item_id = $this->db->insert_id();		
				echo json_encode(array('success'=>true,'message'=>lang('notice_board_successful_adding').' '. $item_data['notice_board_subject'],'person_id'=>$item_id));
				$activity_description = lang('notice_board_successful_adding').' '. $item_data['notice_board_subject'];				
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>lang('notice_board_successful_updating').' '.$item_data['notice_board_subject'],'person_id'=>$item_id));
			    $activity_description = lang('notice_board_successful_updating').' '. $item_data['notice_board_subject'];
			}
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('notice_board_error_adding_updating').' '.$item_data['notice_board_subject'],'person_id'=>-1));
		}
	}	

	function delete()
	{
		$this->check_action_permission('delete');
		$items_to_delete=$this->input->post('ids');
		
		if($this->notice_boards->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('notice_board_successful_deleted').' '.	count($items_to_delete).' '.lang('notice_board_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$items_to_delete); 
			$activity_description = lang('notice_board_successful_deleted').' '. count($items_to_delete).' '.lang('notice_board_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'notice_board_details','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {	echo json_encode(array('success'=>false,'message'=>lang('notice_board_cannot_be_deleted')));}
	}
	
	function chanage_status()
	{	
		$this->check_action_permission('add_update');			
		$items_id = $this->input->post('items_id');
		if($items_id!='')
		{
			if($this->notice_boards->Change_Status($items_id)) 
			{  echo json_encode(array('success'=>true,'message'=>lang('common_change_status_successful'),'item_id'=>$items_id)); 
			  //***save employee activites in database***//
			   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'change_status','activity_items_id'=>$items_id,'activity_description'=>lang('common_change_status_successful'));	
			   $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			  //***save employee activites in database***//
			}
		}	
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'person_id'=>$items_id));	}	 
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_manage_highlight_data_rows($method_type,$this->notice_boards->get_Item_ArrayData($item_id),$this);
		echo $data_row;
	}	
	
	function cleanup()
	{
		$this->notice_boards->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('notice_board_cleanup_sucessful')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'notice_board','activity_type_category'=>'notice_board_details','activity_items_id'=>'','activity_description'=>lang('notice_board_cleanup_sucessful'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	}
	
	/*get the width for the add/edit form	*/
	function get_form_width()
	{			
		return 550;
	}
	

	
}
?>