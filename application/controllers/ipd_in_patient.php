<?php
require_once ("person_controller.php");
class Ipd_in_patient extends Person_controller
{
	function __construct()
	{
		parent::__construct('customers');
		$this->load->model('ipd_in_patients');
		$this->load->helper('ipd_in_table_helper');
	}
		
	function index()
	{  	
		$this->check_action_permission('search');
		$config['base_url'] = site_url('ipd_in_patient/sorting');
		$config['total_rows'] = $this->ipd_in_patients->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['manage_table'] = get_ipd_in_patient_manage_table($this->ipd_in_patients->get_all($data['per_page']),$this);		
		$this->load->view('ipd_in_patients/manage',$data);
	}
		
	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->ipd_in_patients->search_count_all($search);
			$table_data = $this->ipd_in_patients->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->ipd_in_patients->count_all();
			$table_data = $this->ipd_in_patients->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url'] = site_url('ipd_in_patient/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_ipd_in_patient_manage_table($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));			
	}
	
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->ipd_in_patients->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'last_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		//print_r($search_data); die;
		$config['base_url'] = site_url('ipd_in_patient/search');
		$config['total_rows'] = $this->ipd_in_patients->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_ipd_in_patient_manage_table($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function view($customer_id=-1)
	{		
		$this->check_action_permission('add_update');
		$data['person_info']=$this->Customer->get_info($customer_id);
		 $data['months'] = $this->Customer->get_months();
        $data['days'] = $this->Customer->get_days();
        $data['years'] = $this->Customer->get_years();
		$this->load->view("ipd_in_patients/form",$data);
	}
	
	function manage($customer_id=-1,$ipd_patient_id=-1)
	{		
		$this->check_action_permission('add_update');
		$data['person_id'] = $customer_id;
		$data['ipd_patient_id'] = $ipd_patient_id;
		$data['person_info']=$this->ipd_in_patients->get_ipdin_PatientData($customer_id,$ipd_patient_id);
		$this->load->view("ipd_in_patients/manage_ipd_form",$data);
	}
	
	function add_daily_charges($customer_id=-1,$ipd_patient_id=-1) 
	{
		$feecharge  = $this->input->post('feecharge');
		$customer_data = array('payment_per_day_charges'=>$feecharge);
		if($this->ipd_in_patients->Update_GetData('ipd_in_patient',$customer_data,$customer_id,$ipd_patient_id))
		{				
		   echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_assign_payment_per_day') ));				
		}
		 else{ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_failed_assign_payment_per_day') ));}		
	}
	
	function add_room_patients($customer_id=-1,$ipd_patient_id=-1) 
	{
		$block_name  = $this->input->post('block_name');
		$room_name  = $this->input->post('room_name');
		$customer_data = array('patient_block_name'=>$block_name,'patient_room_name'=>$room_name);
		if($this->ipd_in_patients->Update_GetData('ipd_in_patient',$customer_data,$customer_id,$ipd_patient_id))
		{				
		   echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_assign_room') ));				
		}
		 else{ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_failed_assign_room') ));}		
	}
	
	function received_IPDPayment($customer_id=-1) 
	{
		$payment_amount  = $this->input->post('payment_amount');
		$slip_receiept  = $this->input->post('slip_receiept');
		$ipd_id_number  = $this->input->post('ipd_id_number');
		if($customer_id=='' || $payment_amount=='' || $slip_receiept==''){echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_received_payment_selection') ));}	
		$customer_data = array('received_amount'=>$payment_amount,'received_slip_no'=>$slip_receiept,'person_id'=>$customer_id,'ipd_id'=>$ipd_id_number,'status'=>'0','payment_time'=>date('Y-m-d H:i:s') );
		if($this->ipd_in_patients->Insert_GetData('ipd_finance',$customer_data,$customer_id))
		{				
		   echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_received_payment') ));				
		}
		 else{ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_failed_received_payment') ));}		
	}
	
	function discharges_patients($customer_id=-1,$ipd_patient_id=-1) 
	{		
		$ipd_patient_id  = $this->input->post('customer_ipd_id');
		$result = $this->ipd_in_patients->GetValid_Discharge($customer_id,$ipd_patient_id);
		$customer_data = array('discharge_date'=>date('Y-m-d H:i:s'),'discharge_status'=>'1');
		if($result =='1' || $result =='3')
		{  if($this->ipd_in_patients->Update_GetData('ipd_in_patient',$customer_data,$customer_id,$ipd_patient_id))
		    {
		     echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_discharge') ));	die;
			}
			else{ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_discharge') )); die;}	
		}
		if($result =='2'){ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_discharge_deposit') )); die;}
		if($result =='4' || $result =='5'){ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_discharge_deposit_pending') )); die;}	
		else{ echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_discharge') ));  die;}		
	}
	
	function closeform($customer_id=-1) 
	{		
		 echo json_encode(array('success'=>true,'message'=>'' ));	
	}
	
	function search_patients()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->ipd_in_patients->Search_Patients($search_keyword); die;
	}
	
	function search_block_list()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->ipd_in_patients->search_block_lists($search_keyword); die;
	}
	
	function search_room_name_list()
	{	
          $search_keyword  = $this->input->post('search_keyword');
		  echo $search_data = $this->ipd_in_patients->search_room_name_list($search_keyword); die;
	}
	
	function search_account_no() 
	{
		$customers=$this->db->dbprefix('customers');
		$pid  = $this->input->post('pid');
		$QueryData = $this->db->query("SELECT account_number FROM $customers WHERE person_id='".$pid."' ");		
		if($QueryData->num_rows()>0)
		{ $data = $QueryData->row();	echo $data->account_number; die; } 		
	}
	
	function suggest()
	{
		$suggestions = $this->ipd_in_patients->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}	
	
	function save($customer_id=-1)
	{		
		$search  = $this->input->post('search_keyword');
		$person_id = $this->ipd_in_patients->get_customer_id($search);		
		if($search=='' or !$this->ipd_in_patients->exists_account($search)) {	echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_selecting_patinet_toadd'),'person_id'=>-1)); die; }
		$exists = $this->ipd_in_patients->exists_customer_IPD_IN($search);
		if($exists) { echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_already_exist_pateint'),'person_id'=>$person_id)); die; }
	
		if($this->ipd_in_patients->save($search))
		{ //echo $person_id; die;			
			echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_adding').' '. $search,'person_id'=>$person_id));		
		}
		else //failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('ipd_in_patient_error_adding_updating').' '. $search,'person_id'=>$person_id));		
		}
	}
		
	function delete()
	{
		$this->check_action_permission('delete');
		$customers_to_delete=$this->input->post('ids');		
		if($this->ipd_in_patients->delete_list($customers_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_successful_deleted').' '.
			count($customers_to_delete).' '.lang('customers_one_or_multiple')));
		}
		else { echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted'))); }
	}
	

	
	function excel()
	{
		$data = file_get_contents("import_customers.csv");
		$name = 'import_customers.csv';
		force_download($name, $data);
	}
	
	function excel_import()
	{
		$this->check_action_permission('add_update');
		$this->load->view("ipd_in_patients/excel_import", null);
	}
	
	/* added for excel expert */
	function excel_export() {
		$data = $this->ipd_in_patients->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array('First Name', 'Last Name', 'Date of Admited', 'Account No', 'Patient ID','Number of Days', 'Assign Payment Per Day','Total Amount', 'Deposit Amount', 'Balance', 'Block Name/No', 'Room No', 'Discharge Date', 'Discharge Status');
		$rows[] = $row;
		foreach ($data as $r) {
		$Total_Days  	= $this->ipd_in_patients->getTotal_AdmitDays( $r->person_id , $r->id );
		$Finance_Data   = $this->ipd_in_patients->GetTotalDeposit_Amount( $r->person_id , $r->id );
		$payment_per_day_charges   = $this->ipd_in_patients->getIPD_Per_Day_Charges( $r->person_id , $r->id );
		$TotalDeposit   = $Finance_Data['TotalDeposit'];
		$Balance_Amount = $Finance_Data['Balance_Amount'];	
		
		$Total_Amount   = number_format(($payment_per_day_charges * $Total_Days));
		if($r->discharge_status=='1') $discharge_status ='YES'; else $discharge_status ='NO';
			$row = array(
				$r->first_name,
				$r->last_name,
				$r->registration_time,			
				$r->account_number,
				$r->person_id,
				$Total_Days,
				$r->payment_per_day_charges,
				$Total_Amount,
				$TotalDeposit,
				$Balance_Amount,
				$r->patient_block_name,
				$r->patient_room_name,
				$r->discharge_date,
				$discharge_status
			);
			$rows[] = $row;
		}		
		$content = array_to_csv($rows);
		force_download('IPD_In_Patient_Department_export' . '.csv', $content);
		exit;
	}

	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$this->db->trans_start();
				
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE) 
				{
					$person_data = array(
					'first_name'=>$data[0],
					'last_name'=>$data[1],
					'email'=>$data[2],
					'phone_number'=>$data[3],
					'address_1'=>$data[4],
					'address_2'=>$data[5],
					'city'=>$data[6],
					'state'=>$data[7],
					'zip'=>$data[8],
					'country'=>$data[9],
					'comments'=>$data[10]
					);
					
					$customer_data=array(
					'account_number'=>$data[11]=='' ? null:$data[11],
					'taxable'=>$data[12]=='' ? 0:1,
					'company_name' => $data[13],
					);
					
					if(!$this->Customer->save($person_data,$customer_data))
					{	
						echo json_encode( array('success'=>false,'message'=>lang('customers_duplicate_account_id')));
						return;
					}
				}
			}
			else 
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
		}
		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('customers_import_successfull')));
	}
	
	function cleanup()
	{
		$this->ipd_in_patients->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('ipd_in_patient_cleanup_sucessful')));
	}
	
	/*get the width for the add/edit form*/
	function get_form_width()
	{			
		return 550;
	}
	
	
	
	
	

	
}
?>