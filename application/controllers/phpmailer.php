<?php
require_once ("secure_area.php");
class Phpmailer extends Secure_area
{
	function __construct()
	{
		parent::__construct();	
		//$this->load->model('savesetting');
		//$this->load->helper('report');	
		//$this->load->model('customer');
	}
	function send_stock_notification()
	{
	  $this->send_low_inventory_report();
	  $this->send_summary_discounts_report();
	  $this->send_summary_suppliers();
	}
	
	function send_low_inventory_report() 
	{ 	
		//echo 'here u r'; print_r($model->getDataColumns()); die;
		$this->load->model('reports/Inventory_low'); 
		$model = $this->Inventory_low;
		$model->setParams(array());
		$tabular_data = array();
		$report_data = $model->getData(array());
		foreach($report_data as $row)
		{
			$tabular_data[] = array(array('data'=>$row['name'], 'align'=> 'left'), array('data'=>$row['item_number'], 'align'=> 'left'), array('data'=>$row['description'], 'align'=> 'left'),array('data'=>to_currency($row['cost_price']), 'align'=> 'right'),array('data'=>to_currency($row['unit_price']), 'align'=> 'right'), array('data'=>$row['quantity'], 'align'=> 'left'), array('data'=>$row['reorder_level'], 'align'=> 'left'));
		} 
		//echo 'here u r'; print_r($model->getDataColumns()); die;
	   $Notification_Data = array("title" => lang('reports_low_inventory_report'),"headers" => $model->getDataColumns(),"data" => $tabular_data,"summary_data" => $model->getSummaryData(array()) );
       $this->load->model('phpemailsender');
	   $mail_sucess = $this->phpemailsender->send_Items_notification($Notification_Data , $Notification_type='low_inventory_report');
	   if($mail_sucess) echo 'We have send of Low Inbentory List';
	   else 'Error !, We can not to send of Low Inbentory List';   
	}
	

	function send_summary_discounts_report()
	{   $start_date = $end_date = date('Y-m-d'); $sale_type ='all'; 
		$this->load->library('sale_lib');
		$this->load->model('reports/Summary_discounts');
		$model = $this->Summary_discounts;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));		
		$tabular_data = array();
		$report_data = $model->getData(); 
		//*************appliy Exampition code 14-12-2013*******************//	
		$sale_typeQuery=''; $Calculate_discount_Cost='0.00';
		$sales = $this->db->dbprefix('sales');
		$sales_items = $this->db->dbprefix('sales_items');
		$sales_payments = $this->db->dbprefix('sales_payments');
		if(isset($sale_type) && $sale_type!=''){
		if ($sale_type == 'sales')	{ $sale_typeQuery = " and si.quantity_purchased > 0 "; }
		if ($sale_type == 'returns') { $sale_typeQuery = " and si.quantity_purchased < 0 "; }
		}
            $subtotal = '0.00';  $total = '0.00';
			foreach($report_data as $row)
			{  
			    $ExecuteQuery = " select s.customer_id from ".$this->db->dbprefix('sales ')." s left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
					   join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
					   where s.deleted='0' and s.suspended='0' and si.discount_percent = '".$row['discount_percent']."' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' and i.supplier_id='".$row['supplier_id']."' group by s.customer_id";
				$count_patient = $this->db->query($ExecuteQuery)->num_rows();
				//die;
				$sale_typeQuery = " and si.quantity_purchased > 0 and i.supplier_id='".$row['supplier_id']."' and discount_percent = '".$row['discount_percent']."' "; 		
				$SalesQuery = "select s.sale_id,si.quantity_purchased,si.item_cost_price,si.item_unit_price,si.discount_percent from ".$this->db->dbprefix('sales')." s 
				left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
				join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
				join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
				join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
				where s.deleted='0' and s.suspended='0' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' $sale_typeQuery ";
				$SalesQueryData = $this->db->query($SalesQuery);
				
				$Calculate_discount_Cost = 0; $total_tax = 0;				
				if($SalesQueryData->num_rows()>0) {  
				foreach($SalesQueryData->result() as $result)	
				{   foreach($this->sale_lib->get_taxes( $result->sale_id ) as $tax)	{ $total_tax +=$tax; }
					$Calculate_discount_Cost += (($result->item_unit_price * $result->quantity_purchased) * $result->discount_percent / 100 ); 
				} }	
				//$subtotal += $row['subtotal']; //$total += $row['total'];
				$total_with_tax = $Calculate_discount_Cost + $total_tax;
				$subtotal 	   += $Calculate_discount_Cost; 
				$total   	   += $total_with_tax; 
				$tabular_data[] = array(array('data'=>$row['supplier'],'align'=>'left'),array('data'=>$row['discount_percent'],'align'=>'left'),array('data'=>$count_patient,'align'=>'left'),array('data'=>to_currency($total_with_tax),'align'=>'left'));
			}
		$getSummaryDataArray = array();
		$getSummaryDataArray['subtotal'] = $subtotal; 
		$getSummaryDataArray['total'] = $total; 
		$getSummaryData = $getSummaryDataArray; 
		//*******appliy Exampition code 14-12-2013**********//
		//*************Exemption Summary data code*********//
		$customer_All_Male   = $model->Exemption_Summary($start_date, $end_date , $sex_type = 'Male');
		$customer_All_Female = $model->Exemption_Summary($start_date, $end_date , $sex_type = 'Female');
		$customer_under_5    = $model->Exemption_Summary($start_date, $end_date , $sex_type = '', $age_range = 5);		
		$customer_above_60   = $model->Exemption_Summary($start_date, $end_date , $sex_type = '', $age_range = 60);  
		$customer_all_ages   = $model->Exemption_Summary($start_date, $end_date);  
		$customer_all_others = $customer_all_ages - ( $customer_under_5 + $customer_above_60 );
		if($customer_all_others < 0) { $customer_all_others = '0';}
		
		$Notification_Data = array("title" 	 	 => lang('reports_discounts_summary_report'),
					  "subtitle" 	 => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
					  "headers"  	 => $model->getDataColumns(),
					  "data" 	 	 => $tabular_data,
					  "summary_data" => $getSummaryData,
					  "customer_All_Male" 	=> $customer_All_Male,
					  "customer_All_Female" => $customer_All_Female,
					  "customer_under_5" 	=> $customer_under_5,
					  "customer_above_60" 	=> $customer_above_60,
					  "customer_all_others" => $customer_all_others
		);        	
		//$this->load->view("reports/tabular",$Notification_Data);		
	   $this->load->model('phpemailsender');
	   $mail_sucess = $this->phpemailsender->send_Items_notification($Notification_Data , $Notification_type='summary_discounts_report');
	   if($mail_sucess) echo 'We have send of Exemption Summary Report List';
	   else 'Error !, We can not to send of Exemption Summary Report List'; 	   
	}

	function send_summary_suppliers()
	{   $start_date = $end_date = date('Y-m-d'); $sale_type ='all';
	
		$this->load->model('reports/Summary_suppliers');
		$model = $this->Summary_suppliers;
		$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));

		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type));
		$tabular_data = array();
		$report_data = $model->getData();
		foreach($report_data as $row)
		{
		  $ExecuteQuery = " select s.customer_id from ".$this->db->dbprefix('sales ')." s left join ".$this->db->dbprefix('sales_items ')." si on (s.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('sales_payments')." p on (p.sale_id = si.sale_id) 
					   join ".$this->db->dbprefix('items')." i on (i.item_id = si.item_id) 
					   join ".$this->db->dbprefix('suppliers')." sp on (sp.person_id = i.supplier_id) 
					   where s.deleted='0' and s.suspended='0' and date(s.sale_time) BETWEEN '".$start_date."' and '".$end_date."' and i.supplier_id='".$row['supplier_id']."' and s.customer_id!='' group by s.customer_id";
		  $count_patient = $this->db->query($ExecuteQuery)->num_rows();
			$tabular_data[] = array(array('data'=>$row['supplier'], 'align' => 'left'), array('data'=>to_currency($row['subtotal']), 'align' => 'right'),array('data'=>$count_patient,'align' =>'right'),array('data'=>to_currency($row['total']),'align' => 'right'));
		}

		$Notification_Data = array("title" => lang('reports_suppliers_summary_report'),
									"subtitle" => date(get_date_format(), strtotime($start_date)) .'-'.date(get_date_format(), strtotime($end_date)),
									"headers" => $model->getDataColumns(),
									"data" => $tabular_data,
									"summary_data" => $model->getSummaryData()
								  );
	   $this->load->model('phpemailsender');
	   $mail_sucess = $this->phpemailsender->send_Items_notification($Notification_Data , $Notification_type='summary_suppliers');
	   if($mail_sucess) echo 'We have send of Suppliers Summary Report List';
	   else 'Error !, We can not to send of Suppliers Summary Report List'; 
	
	}
	
			
}



?>