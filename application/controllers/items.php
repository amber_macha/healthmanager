<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Items extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('items');
	}	
	
	function index()
	{	
		$this->check_action_permission('search');
		$config['base_url'] = site_url('items/sorting');
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['per_page'] = $config['per_page'];
		$data['method_type'] = 'items';	
		$data['List_Title_Name'] = lang('module_'.$data['controller_name']);	
		$data['manage_table']=get_items_manage_table_for_items($this->Item->get_all($data['per_page']),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>'','activity_description'=>lang('items_list_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('items/manage',$data);
	}

	function sorting()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->Item->search_count_all($search);
			$table_data = $this->Item->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows'] = $this->Item->count_all();
			$table_data = $this->Item->get_all($per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url'] = site_url('items/sorting');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_items_manage_table_data_rows($table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}


	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Item->find_item_info($item_number));
	}
	
	function search()
	{
		$this->check_action_permission('search');
		$search=$this->input->post('search');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->Item->search($search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('items/search');
		$config['total_rows'] = $this->Item->search_count_all($search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_items_manage_table_data_rows($search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Item->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Item->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}


	function get_row($method_type = null)
	{   
		$item_id = $this->input->post('row_id');
		if($method_type=='insurance_company')
		{   $this->load->model('savesetting');
			$this->load->helper('viewdatatable_helper');
			$data_row = get_manage_highlight_data_rows($method_type,$this->savesetting->Get_Table_Row_Data_InArray('insurance_company','id',$item_id),$this);
		}
		else {	$data_row=get_item_data_row_updateList($this->Item->get_info($item_id),$this); }
		echo $data_row;
	}

	function get_info($item_id=-1)
	{
		echo json_encode($this->Item->get_info($item_id));
	}

	function view($item_id=-1)
	{
		$this->check_action_permission('add_update');
       	// Alain promotion price...
        $this->load->helper('report');
        $data = array();
        $data['months'] = get_months();
        $data['days'] = get_days();
        $data['years'] = $this->Item->get_years_list();

		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$this->load->model('savesetting');
		$data['multi_stores_items'] = $this->savesetting->Get_Table_Category_Data('lab_category','multi_stores','lab_name');
		$suppliers = array('' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}

		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['default_tax_2_cumulative']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_cumulative') : '';
		//Alain Promo Price
        if($item_id==-1)
        {
        	$data['selected_start_year']=0;
        	$data['selected_start_month']=0;
        	$data['selected_start_day']=0;
        	$data['selected_end_year']=0;
			$data['selected_end_month']=0;
			$data['selected_end_day']=0;
        }
        else
        {
        	list($data['selected_start_year'],$data['selected_start_month'],$data['selected_start_day'])=explode('-',$data['item_info']->start_date);
        	list($data['selected_end_year'],$data['selected_end_month'],$data['selected_end_day'])=explode('-',$data['item_info']->end_date);
 		}
		if($item_id!=-1){
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_information_view'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
		}
		$this->load->view("items/form",$data);
	}

	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$data['item_info']=$this->Item->get_info($item_id);		  
		  //***save employee activites in database***//
		   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_inventory_view'));	
		   $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		  //***save employee activites in database***//		  
		$this->load->view("items/inventory",$data);
	}

	function count_details($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$data['item_info']=$this->Item->get_info($item_id);
		//***save employee activites in database***//
		 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_details_count_view'));	
		 $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("items/count_details",$data);
	} //------------------------------------------- Ramel

	function generate_barcodes($item_ids)
	{
		$result = array();

		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('name' =>$item_info->name.': '.to_currency($item_info->unit_price), 'id'=> number_pad($item_id, 11));
		}

		$data['items'] = $result;
		$data['scale'] = 2;
		$this->load->view("barcode_sheet", $data);
	}

	function generate_barcode_labels($item_ids)
	{
		$result = array();

		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('name' =>$item_info->name.': '.to_currency($item_info->unit_price), 'id'=> number_pad($item_id, 11));
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		$this->load->view("barcode_labels", $data);
	}

	function bulk_edit()
	{
		$this->check_action_permission('add_update');		
		$data = array();
		$suppliers = array('' => lang('items_do_nothing'), '-1' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name']. ' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers'] = $suppliers;
		$data['allow_alt_desciption_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_allow_alt_desc'),
			0 =>lang('items_change_all_to_not_allow_allow_desc'));

		$data['serialization_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_serialized'),
			0 =>lang('items_change_all_to_unserialized'));
		$this->load->view("items/form_bulk", $data);
	}

	function save($item_id=-1)
	{
		$this->check_action_permission('add_update');
		//echo $this->input->post('specialized');die;
		$items_generic_code = $this->input->post('items_generic_code');
		$nhif_code 			= $this->input->post('nhif_code');
		if($items_generic_code!='' && $items_generic_code!='0')
		{
		  $getValid = $this->Item->valid_Item_Code('items_generic_code',$items_generic_code,$item_id);
		  if($getValid){ echo json_encode(array('success'=>false,'message'=>$items_generic_code.','.lang('items_generic_code_exist'),'item_id'=>$item_id));  die; }
		}
		if($nhif_code!='' && $nhif_code!='0')
		{
		  $getValid = $this->Item->valid_Item_Code('nhif_code_number',$nhif_code,$item_id);
		  if($getValid){ echo json_encode(array('success'=>false,'message'=>$nhif_code.','.lang('items_nhif_code_exist'),'item_id'=>$item_id));  die; }
		}
		$service_category=''; 
		if($this->input->post('item_category')==1) { $service_category = $this->input->post('service_category');} 
		$service_sub_category1 = '0';  $service_sub_category2 = '0';  $service_sub_category3 = '0';  $service_sub_category4 = '0';
		if($this->input->post('service_category')==5)
		{
		if($this->input->post('specialized')==1) $service_sub_category1 = '1';
		if($this->input->post('major')==1) 	$service_sub_category2 = '1'; 
		if($this->input->post('minor')==1) $service_sub_category3 = '1'; 
		if($this->input->post('procedure')==1) $service_sub_category4 = '1'; 
		}
		$items_expire_d = $this->input->post('items_expire_d');	
		$items_expire_m = $this->input->post('items_expire_m');
		$items_expire_y = $this->input->post('items_expire_y');
		$items_expire_date = $items_expire_y.'-'.$items_expire_m.'-'.$items_expire_d;
		$item_data = array( 'item_type'=>$this->input->post('item_category'),
							'lab_category_id'=>$this->input->post('multi_store_item'),
							'service_category'=>$service_category,
							'name'=>$this->input->post('name'),
							'description'=>$this->input->post('description'),
							'category'=>$this->input->post('category'),
							'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
							'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
							'cost_price'=>$this->input->post('cost_price'),
							'unit_price'=>$this->input->post('unit_price'),
							'unit_private_price'=>$this->input->post('private_price'),
							'unit_fast_track_price'=>$this->input->post('fast_track_price'),
							'unit_nhif_price'=>$this->input->post('nhif_price'),
							//Alain Promo price start
							'promo_price'	=>	$this->input->post('promo_price') ? $this->input->post('promo_price') : 0.00,
							'start_date'	=>	$this->input->post('hdn_start_date') ? $this->input->post('hdn_start_date') : '1969-01-01'   ,
							'end_date'		=>	$this->input->post('hdn_end_date') ? $this->input->post('hdn_end_date') : '1969-01-01',
							//Alain Promo price end
							'quantity'			    => $this->input->post('quantity'),
							'reorder_level'		    => $this->input->post('reorder_level'),
							'items_expire_date'	    => $items_expire_date,
							'nhif_code_number'	    => $this->input->post('nhif_code'),
							'items_generic_code'    => $items_generic_code,
							'service_sub_category1' => $service_sub_category1,
							'service_sub_category2' => $service_sub_category2,
							'service_sub_category3' => $service_sub_category3,
							'service_sub_category4' => $service_sub_category4,
							//insurance company price start
							'unit_nssf_price'			=>	$this->input->post('unit_nssf_price') ? $this->input->post('unit_nssf_price') : 0.00,
							'unit_strategis_insurance'	=>	$this->input->post('unit_strategis_insurance') ? $this->input->post('unit_strategis_insurance') : 0.00,
							'unit_jubilee_insurance'	=>	$this->input->post('unit_jubilee_insurance') ? $this->input->post('unit_jubilee_insurance') : 0.00,
							'unit_arr_insurance'		=>	$this->input->post('unit_arr_insurance') ? $this->input->post('unit_arr_insurance') : 0.00,
							'unit_reliance_insurance'	=>	$this->input->post('unit_reliance_insurance') ? $this->input->post('unit_arr_insurance') : 0.00,
							'unit_resolution_insurance'	=>	$this->input->post('unit_resolution_insurance') ? $this->input->post('unit_resolution_insurance') : 0.00,
							'unit_billing_patient_price'=>	$this->input->post('unit_billing_patient_price') ? $this->input->post('unit_billing_patient_price') : 0.00,
							'unit_nic_insurance'		=>	$this->input->post('unit_nic_insurance') ? $this->input->post('unit_nic_insurance') : 0.00,
							'unit_phoenix_insurance'	=>	$this->input->post('unit_phoenix_insurance') ? $this->input->post('unit_phoenix_insurance') : 0.00,
							'unit_imara_insurance'		=>	$this->input->post('unit_imara_insurance') ? $this->input->post('unit_imara_insurance') : 0.00,
							//insurance company price end
							'location'				=> $this->input->post('location'),
							'allow_alt_description' => $this->input->post('allow_alt_description') ? $this->input->post('allow_alt_description') : 0 ,
							'is_serialized'			=> $this->input->post('is_serialized') ? $this->input->post('is_serialized') : 0
						 );

		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		if($this->Item->save($item_data,$item_id))
		{
			//New item
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '. $item_data['name'],'item_id'=>$item_data['item_id']));
				$item_id = $item_data['item_id'];
				$activity_description = lang('items_successful_adding').' '. $item_data['name'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '. $item_data['name'],'item_id'=>$item_id));
				$activity_description = lang('items_successful_updating').' '. $item_data['name'];
				//***********SEND STOCK NOTIFICATIONS CODE**********//
				$this->load->model('phpemailsender');
				$Employee_Data 		= $this->Employee->get_logged_in_employee_info();	
				$employee_name 		= $Employee_Data->first_name . ' '.$Employee_Data->last_name;				
				$employee_id   		= $Employee_Data->person_id;
				$items_item_number  = $this->input->post('item_number');
				$item_name 	   		= $this->input->post('name');
				$Category 	   		= $this->input->post('category');
				$items_cost_price 	= $this->input->post('cost_price');
				$item_category 	  	= $this->input->post('item_category');
				if($item_category==0) 
				{   
					$item_type = lang('common_medicine');
					$Quantity = $this->input->post('quantity');
					$reorder_level = $this->input->post('reorder_level');
					$items_expire_date = $items_expire_d.'-'.$items_expire_m.'-'.$items_expire_y;
				}					
				else { $item_type = lang('common_service'); $Quantity='';$reorder_level='';$items_expire_date=''; }
				$Notification_Data = array('items_item_number'  =>	$items_item_number,
										   'item_name'			=>	$item_name,
										   'Category'			=>	$Category,
										   'item_type'			=>  $item_type,
										   'items_cost_price'	=>  $items_cost_price,
										   'Quantity'			=>	$Quantity,
										   'reorder_level'		=>	$reorder_level,
										   'items_expire_date'	=>	$items_expire_date,
										   'employee_name'		=>	$employee_name,
										   'employee_id'		=>	$employee_id							
										  );
				$mail_sucess = $this->phpemailsender->send_Items_notification( $Notification_Data , $Notification_type = 'stock_item_updation' );
				//***********SEND STOCK NOTIFICATIONS CODE**********//
			
			}            
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
			//***save employee activites in database***//
			
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>lang('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity')
			);
			$this->Inventory->insert($inv_data);
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			$tax_cumulatives = $this->input->post('tax_cumulatives');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k], 'cumulative' => isset($tax_cumulatives[$k]) ? $tax_cumulatives[$k] : '0' );
				}
			}
			$this->Item_taxes->save($items_taxes_data, $item_id);			
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.	$item_data['name'],'item_id'=>-1));
		}
	}

	//Ramel Inventory Tracking
	function save_inventory($item_id=-1)
	{
		$this->check_action_permission('add_update');		
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		$inv_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_items'=>$item_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$this->input->post('trans_comment'),
			'trans_inventory'=>$this->input->post('newquantity')
		);
		$this->Inventory->insert($inv_data);
		//Update stock quantity
		$item_data = array(
		'quantity'=>$cur_item_info->quantity + $this->input->post('newquantity')
		);
		if($this->Item->save($item_data,$item_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.$cur_item_info->name,'item_id'=>$item_id));
			//***save employee activites in database***//
		    $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_successful_updating').' '.$cur_item_info->name);	
		    $this->Employee->save_activites($Activity_Data,$item_id); 								 
		    //***save employee activites in database***//
			
			//***********SEND STOCK INVENTORY NOTIFICATIONS CODE**********//
				$this->load->model('phpemailsender');
				$Employee_Data 		= $this->Employee->get_logged_in_employee_info();	
				$employee_name 		= $Employee_Data->first_name . ' '.$Employee_Data->last_name;				
				$employee_id   		= $Employee_Data->person_id;
				$new_item_info 		= $this->Item->get_info($item_id);
				$items_expire_date = date('d-m-Y',strtotime($cur_item_info->items_expire_date));								
				$Notification_Data = array('items_item_number'  =>	$cur_item_info->item_number,
										   'item_name'			=>	$cur_item_info->name,
										   'Category'			=>	$cur_item_info->category,
										   'item_type'			=>  lang('common_medicine'),
										   'items_cost_price'	=>  $cur_item_info->cost_price,
										   'Quantity_Old'		=>	$cur_item_info->quantity,
										   'Quantity_New'		=>	$new_item_info->quantity,
										   'Inventory_Comments'	=>	$this->input->post('trans_comment'),
										   'reorder_level'		=>	$cur_item_info->reorder_level,
										   'items_expire_date'	=>	$items_expire_date,
										   'employee_name'		=>	$employee_name,
										   'employee_id'		=>	$employee_id							
										  );
				if($cur_item_info->item_type==0){ $mail_sucess = $this->phpemailsender->send_Items_notification($Notification_Data,$Notification_type='stock_item_inventory_updation'); }
				//***********SEND STOCK INVENTORY NOTIFICATIONS CODE**********//
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.
			$cur_item_info->name,'item_id'=>-1));
		}
	}

	function bulk_update()
	{   
		$this->check_action_permission('add_update');
		$items_to_update=$this->input->post('item_ids');
		$item_data = array();
		foreach($_POST as $key=>$value)
		{
			if ($key == 'submit')
			{
				continue;
			}
			//This field is nullable, so treat it differently
			if ($key == 'supplier_id')
			{
				if ($value!='')
				{
					$item_data["$key"]=$value == '-1' ? null : $value;
				}
			}
			elseif($value!='' and !(in_array($key, array('item_ids', 'tax_names', 'tax_percents', 'tax_cumulatives'))))
			{
				$item_data["$key"]=$value;
			}
		}

		//Item data could be empty if tax information is being updated
		if(empty($item_data) || $this->Item->update_multiple($item_data,$items_to_update))
		{
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			$tax_cumulatives = $this->input->post('tax_cumulatives');

			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k], 'cumulative' => isset($tax_cumulatives[$k]) ? $tax_cumulatives[$k] : '0' );
				}
			}

			if (!empty($items_taxes_data))
			{
				$this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);
			}

			echo json_encode(array('success'=>true,'message'=>lang('items_successful_bulk_edit')));
			//***save employee activites in database***//
		    $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>implode(',',$items_to_update),'activity_description'=>lang('items_successful_bulk_edit'));	
		    $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		    //***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_updating_multiple')));
		}
	}

	function delete() 
	{
		$this->check_action_permission('delete');		
		$items_to_delete=$this->input->post('ids');

		if($this->Item->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.	count($items_to_delete).' '.lang('items_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$items_to_delete); 
			$activity_description = lang('items_successful_deleted').' '. count($items_to_delete).' '.lang('items_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}

	function chanage_status()
	{	
		$this->check_action_permission('add_update');			
		$items_id = $this->input->post('items_id');
		$method_type = $this->input->post('method_type');				
		if($items_id!='' && $method_type!='')
		{
		if($this->Item->Change_Status($items_id,$method_type)) 
		{  echo json_encode(array('success'=>true,'message'=>lang('common_change_status_successful'),'item_id'=>$items_id)); 
		  //***save employee activites in database***//
		   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$items_id,'activity_description'=>lang('common_change_status_successful'));	
		   $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		  //***save employee activites in database***//
		}
		}	
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'item_id'=>$items_id));	}	 
	}
	
	
	function excel()
	{
		$data = file_get_contents("import_items.csv");
		$name = 'import_items.csv';
		force_download($name, $data);
	}

	/* added for excel expert */
	function excel_export() {
		$data = $this->Item->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("Item Type","Service Type","UPC/EAN/ISBN", "Item Name", "Category", "Supplier ID",
					  "Cost Price", "Unit Price", "Private Price", "Fast Track Price", "NHIF Price","Multi Price","Promo Price","Start","Finish",
					  "Tax 1 Name", "Tax 1 Percent", "Tax 2 Name ", "Tax 2 Percent", "Tax 2 Cumulative","Quantity", "Reorder Level","Item Expire Date", 
					  "NHIF code Number","Location", "Description", "Allow Alt Description", "Item has Serial Number","Item Status");
		$rows[] = $row;
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>'','activity_description'=>lang('items_export_successfull'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		foreach ($data as $r) {
			$taxdata = $this->Item_taxes->get_info($r->item_id);
			if (sizeof($taxdata) >= 2) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = $taxdata[1]['name'];
				$r->taxp1 = $taxdata[1]['percent'];
				$r->cumulative = $taxdata[1]['cumulative'] ? 'y' : '';
			} else if (sizeof($taxdata) == 1) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			} else {
				$r->taxn = '';
				$r->taxp = '';
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			}
			$service_category='0';
			if($r->item_type=='1') $item_type = "Service"; else $item_type = "Medicine";
			if($r->service_category == '1') { $service_category = "Laboratory Service";} if($r->service_category == '2') { $service_category = "Radiology Service";}
			if($r->service_category =='3') { $service_category = "Dental Service";}     if($r->service_category == '4') { $service_category = "Eye Clinic Service";}
			if($r->service_category == '5') { $service_category = "Other Service";} 
			if($r->item_status=='0') $item_status = "Active"; else  $item_status = "Inactive";
			$row =array($item_type,
						$service_category,
						$r->item_number,
						$r->name,
						$r->category,
						$r->supplier_id,
						$r->cost_price,
						$r->unit_price,
						$r->unit_private_price,
						$r->unit_fast_track_price,
						$r->unit_nhif_price,
						$r->unit_multi_price,
						$r->promo_price,
						date('d-m-Y',strtotime($r->start_date)),
						date('d-m-Y',strtotime($r->end_date)), 
						$r->taxn,
						$r->taxp,
						$r->taxn1,
						$r->taxp1,
						$r->cumulative,
						$r->quantity,
						$r->reorder_level,
						date('d-m-Y',strtotime($r->items_expire_date)),
						$r->nhif_code_number,
						$r->location,
						$r->description,
						$r->allow_alt_description,
						$r->is_serialized ? 'y' : '',
						$item_status,
					 );
			$rows[] = $row;
		}
		$content = array_to_csv($rows);
		force_download('items_export.csv', $content);
		exit;
	}

	function excel_import()
	{
		$this->check_action_permission('add_update');
		$this->load->view("items/excel_import", null);
	}

	function do_excel_import()
	{
		$this->check_action_permission('add_update');
		$this->db->trans_start();
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE)
			{
				//Skip first row
				fgetcsv($handle);
				while (($data = fgetcsv($handle)) !== FALSE)
				{  
				 if($data[0]!='Item Type')
				 {  $service_category='0'; 
				    $items_expire_date = date('Y-m-d'); $start_date = $end_date = date('Y-m-d'); 
					if($data[0]=='Service') $item_type = "1"; else $item_type = "0";
					if($data[1] == 'Laboratory Service') { $service_category = "1";} if($data[1] == 'Radiology Service') { $service_category = "2";}
					if($data[1] == 'Dental Service') { $service_category = "3";}     if($data[1] == 'Eye Clinic Service') { $service_category = "4";}
					if($data[1] == 'Other Service') { $service_category = "5";} 
			        
					if($data[13]!='' || $data[13]!='0000-00-00'){ $start_date = date('Y-m-d',strtotime($data[13])); }
					if($data[14]!='' || $data[14]!='0000-00-00'){ $end_date = date('Y-m-d',strtotime($data[14])); }
					if($data[22]!='' || $data[22]!='0000-00-00'){ $items_expire_date = date('Y-m-d',strtotime($data[22])); }
				    if($data[28] =='Active' ) $item_status = '0'; else  $item_status = "1";
					$item_data = array(
					'item_type'				=>	$item_type,
					'service_category'		=>	$service_category,
					'item_number'			=>	$data[2],
					'name'					=>	$data[3],						
					'category'				=>	$data[4],
					'supplier_id'			=>  $this->Supplier->exists($data[5]) ? $data[5] : $this->Supplier->find_supplier_id($data[5]),					
					'cost_price'			=>	$data[6],
					'unit_price'			=>	$data[7],
					'unit_private_price'	=>	$data[8],
					'unit_fast_track_price'	=>	$data[9],
					'unit_nhif_price'		=>	$data[10],
					'unit_multi_price'		=>	$data[11],
					'promo_price'			=>	$data[12],					
					'start_date'			=>	$start_date,
					'end_date'				=>	$end_date,					
					'quantity'				=>	$data[20],
					'reorder_level'			=>	$data[21],
					'items_expire_date' 	=>  $items_expire_date,
					'nhif_code_number' 		=>  $data[23],
					'location' 				=>  $data[24],
					'description'			=>	$data[25],
					'allow_alt_description' =>  $data[26] != '' ? '1' : '0',
					'is_serialized'			=>  $data[27] != '' ? '1' : '0',
					'item_status'			=>	$item_status
					);					
					
					if($this->Item->save($item_data))
					{  
						$items_taxes_data = null;
						//tax 1
						if( is_numeric($data[16]) && $data[16]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[15], 'percent'=>$data[16], 'cumulative' =>'0');
						}

						//tax 2
						if( is_numeric($data[18]) && $data[18]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[17], 'percent'=>$data[17], 'cumulative'=> $data[19] != '' ? '1' : '0' );
						}

						// save tax values
						if(count($items_taxes_data) > 0)
						{
							$this->Item_taxes->save($items_taxes_data, $item_data['item_id']);
						}

							$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
							$emp_info=$this->Employee->get_info($employee_id);
							$comment ='Qty CSV Imported';
							$excel_data = array
								(
								'trans_items'=>$item_data['item_id'],
								'trans_user'=>$employee_id,
								'trans_comment'=>$comment,
								'trans_inventory'=>$data[13]
								);
								$this->db->insert('inventory',$excel_data);
						//------------------------------------------------Ramel
					}
					else//insert or update item failure
					{
						echo json_encode( array('success'=>false,'message'=>lang('items_duplicate_item_ids')));
						return;
					}
				}
			 }
		    }	
			else
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
		}

		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('items_import_successful')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>'','activity_description'=>lang('items_import_successful'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
	}
	
	function insurance_company()
	{	
		$this->check_action_permission('search');
		$this->load->model('savesetting');
		$this->load->helper('viewdatatable_helper');
		
		$data['pagination'] = '';
		$data['controller_name']=strtolower(get_class());
		$data['form_width']  = $this->get_form_width();
		$data['per_page']    = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;		
		$data['method_type'] = 'insurance_company';	
		$data['List_Title_Name'] = lang('common_insurance_company');	
	    $data['manage_table']= get_manage_table($data['method_type'],$this->savesetting->Get_Table_Data('insurance_company','company_name'),$this);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>'','activity_description'=>lang('items_insurance_company_view_list'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view('items/manage',$data);
	}
	
	function view_insurance_company($item_id=-1)
	{
		$this->check_action_permission('add_update');
        $this->load->model('savesetting');
		$data['item_info']= $this->savesetting->Get_Table_Row_Data('insurance_company','id',$item_id);		
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_insurance_company_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//		
		$this->load->view("items/form_insurance_company",$data);
	}
	
	/*Inserts/updates a customer*/
	function save_insurance_company($item_id=-1)
	{ $Company_Logo ='';	  
	  $this->check_action_permission('add_update');	
	  $this->load->model('savesetting');	  
	  if($this->input->post('company_name')=='' || $this->input->post('company_profile')=='' || $this->input->post('company_phone_no')=='' || $this->input->post('company_address')=='')
	  { echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'),'item_id'=>-1));die; }
	  $exists = $this->savesetting->exists_insurance_company($this->input->post('company_name'),$item_id);
      if($exists) { echo json_encode(array('success'=>false,'message'=>lang('items_company_name_exist'),'item_id'=>-1));die; }
	  $Company_Oldimage = $this->savesetting->get_Table_Column_Data('insurance_company','company_logo','id',$item_id);	  
	  if(isset($_FILES['company_logo']['name']) && $_FILES['company_logo']['name']!='')
	  {   $this->load->library('Upload'); 
		  $size=getimagesize($_FILES['company_logo']['tmp_name']);
		  if($size[0]<100){ echo json_encode(array('success'=>false,'message'=>lang('common_patient_image_size_not_allowed'),'item_id'=>-1)); die;}			  		
				$dir_dest = 'images/insurance_company_logo/'; 
				$handle = new Upload($_FILES['company_logo']);
				$FindImage = $this->input->post('company_name').'_'.time();
				$error = NULL;
				if ($handle->uploaded) 
				{					
					$handle->file_new_name_body = $FindImage;
					$handle->image_resize          = true;
					$handle->image_y               = 100;
					$handle->image_x               = 100;
					$handle->image_ratio_crop = true;
					$handle->file_name_body_pre = 'thumb_';
					$handle->Process($dir_dest);

					$handle->file_new_name_body = $FindImage;
					$handle->image_ratio_crop= true;
					$handle->image_resize= true;
					$handle->image_ratio=true;
					$handle->image_ratio_x          = true;
					$handle->image_ratio_y          = true;
					
					$handle->image_x    = $size[0];
					$handle->image_y    = $size[1];
					$handle->Process($dir_dest);
					$New_patient_image = $handle->file_dst_name;						
					if ($handle->processed) {
					$handle-> Clean();
					}else{ $error = $handle->error;	}
		        }		
	  }
	  	if(isset($New_patient_image) && $New_patient_image!='' && $New_patient_image!='0') 
			{ 
			$Company_Logo = $New_patient_image; if($Company_Oldimage!='') { $Company_Oldimage_thumb = 'thumb_'.$Company_Oldimage; @unlink($dir_dest.$Company_Oldimage); @unlink($dir_dest.$Company_Oldimage_thumb); }
			}
			else { $Company_Logo = $Company_Oldimage; }	  		
			
		$item_data	=  array('company_name'	   => $this->input->post('company_name'),
							 'company_profile' => $this->input->post('company_profile'),							
							 'company_phone_no'=> $this->input->post('company_phone_no'),	
							 'company_address' => $this->input->post('company_address'),	
							 'company_website' => $this->input->post('company_website'),	
							 'company_logo'    => $Company_Logo							
						 );
		if($this->savesetting->save_data('insurance_company',$item_data,$item_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '. $item_data['company_name'],'item_id'=>$item_id));
			//***save employee activites in database***//
			 $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>$item_id,'activity_description'=>lang('items_successful_updating').' '.$item_data['company_name']);	
             $this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.$item_data['company_name'],'item_id'=>-1));
		}
	}
			
	function cleanup()
	{
		$this->Item->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('items_cleanup_sucessful')));
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'item_information','activity_items_id'=>'','activity_description'=>lang('items_cleanup_sucessful'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//
	}
	
	//*********START MULTI STORES FUNCTIONS CODE************************************//
	function MultiStores() 
	{
		$this->check_action_permission('add_update');
		$this->load->model('labtest');
		$data['MultiStoreList'] = $this->labtest->GetSearchAllDiseasList($search='',$category_type='multi_stores');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'multi_stores','activity_items_id'=>'','activity_description'=>lang('items_multi_store_item_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
		//***save employee activites in database***//		
		$this->load->view("items/form_multi_stores",$data);
	}

	 function addnewMultiStore($service_id=-1)  
	 {	
	     $this->check_action_permission('add_update');
		 $this->load->model('labtest');
		 $servicename   = addslashes($this->input->post('servicename')); 
		 if($servicename!='') 
		 {			    
			$valid = $this->labtest->existsDiagnoses($servicename, $codeNumber ='',$service_id,'multi_stores');
			if($valid=='no') { echo json_encode(array('success'=>false,'message'=>lang('items_multi_store_name_exist'))); die;}		
		    $this->load->model('savesetting');
		    $Item_Data =array('lab_name' => $servicename, 'category_type' =>'multi_stores');
		    if($this->savesetting->saveUpdateTable('lab_category',$Item_Data,'id',$service_id))
		    {   
			 
			 if($service_id==-1) 
			 { echo json_encode(array('success'=>true,'message'=>lang('items_multi_store_added_success')));
			   $activity_description = lang('items_multi_store_added_success').' '.$servicename;
			 }
			 else { echo json_encode(array('success'=>true,'message'=>lang('items_multi_store_update_success'))); 
					$activity_description = lang('items_multi_store_added_success').' '.$servicename;
			 }
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'multi_stores','activity_items_id'=>'','activity_description'=>$activity_description);	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
		    }
		 } 	 
		else { echo json_encode(array('success'=>false,'message'=>lang('items_multi_store_added_update_failed'))); }		
	 }
	 
	function searchMultiStore($customer_id=-1)
	{	
		$this->load->model('labtest');
		$search = addslashes($this->input->post('service'));
		$search_data = $this->labtest->GetSearchAllDiseasList($search ,$category_type='multi_stores');
		echo $search_data; die;
	}	
	 
	function getMultiStoreData()
	{	
        $this->load->model('labtest');	
		$services_id   = addslashes($this->input->post('services_id'));
		echo $search_data=$this->labtest->GetDiseasData_BYID($services_id,'multi_stores');die;
	}
	
	function deleteMultiStoreList($value=-1)
	  {		
		//$this->check_action_permission('search');
		$message='';
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);
		$this->load->model('labtest');		
		$success=$this->labtest->deleteAllDiagnoses($value,'multi_stores');	
		if($success==1)
		{  $message = lang('items_multi_store_successful_deleted'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'items','activity_type_category'=>'multi_stores','activity_items_id'=>$activity_items_id,'activity_description'=>$message);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {  $message = lang('items_multi_store_error_deleted'); }
		echo $message; die;	
	}
	
	//*********END MULTI STORES FUNCTIONS CODE************************************//
	
	/*get the width for the add/edit form*/
	function get_form_width()
	{
		return 650;
	}
}
?>