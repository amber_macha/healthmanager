<?php
require_once ("secure_area.php");
class Nhif_patient extends Secure_area 
{
	function __construct()
	{
		parent::__construct('nhif_patient');	
		$this->load->helper('nhif_helper');
		$this->load->model('savesetting');  
		$this->load->model('ipd_in_patients');		
	    $this->load->model('eyeclinics');  
		$this->load->model('uss_xray'); 
		$this->load->model('medicinetest'); 
		$this->load->model('otherservices');
		$this->load->model('labtest');      
		$this->load->model('dentals'); 			
	} 	
	
	function index()
	{
	    $this->check_action_permission('add_update'); 
	    $data['controller_name']=strtolower(get_class()); 		
		$this->load->view("nhif_patient/nhif_patient_form",$data);
	}
	
	function getCustomer_PatientData()
	{  
		$account_no     =  $this->input->post('account_no');
		$from_date      =  $this->input->post('from_date');
		$to_date        =  $this->input->post('to_date');
		$reference_code =  $this->input->post('reference_code');
		if($account_no=='' || $from_date=='' || $to_date=='' || $reference_code==''){ echo '0'; die;}
		if( strtotime($from_date) > strtotime($to_date)) { echo '1'; die;}
	    $valid= $this->Customer->checkvalid_Account_No($account_no);
		if(!$valid) { echo '2'; die;}	
		$exist= $this->Customer->check_Patient_Category($patient_category =4,$account_no,'account_number');
		if(!$exist) { echo '3'; die;}
		$this->load->model('ipd_in_patients');		
		$Ref_Exist = $this->savesetting->check_Reference_Code($reference_code);		
		if($Ref_Exist) { echo '4'; die;}
		$exists_Customer_IPD_IN = $this->ipd_in_patients->exists_customer_IPD_IN($account_no);
		if(!$exists_Customer_IPD_IN) { echo '5'; die;}
			
		$from_date_1 = date('Y-m-d',strtotime($from_date));
		$to_date_1   = date('Y-m-d',strtotime($to_date));
		$Cust_Data = $this->Customer->get_Patient_Data_By_Account_No($account_no);
		$data['Cust_Data'] = $Cust_Data;
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{   
		    $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $this->Customer->get_info($Cust_Data->person_id);
			$Item_Data = array( 'person_id' 	   => $Cust_Data->person_id ,
							    'from_date'        => $from_date_1,
							    'to_date'		   => $to_date_1,
							    'reference_code_no'=> $reference_code,
							    'employee_id'      => $this->Employee->get_Logged_SessionData('person_id'),
							    'added_date'	   => date('Y-m-d H:i:s')
							   );
			$this->savesetting->addTableData('nhif_patient_reports',$Item_Data);	  
			$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 			
			$data['reference_code'] = $reference_code;
			$data['ResidenceData'] = $this->savesetting->Get_Table_Data('residence','residence_name',$order='asc',$limit=10000);
			$data['OccupatiosData']=  $this->savesetting->Get_Table_Data('occupations','occupation_name');			
									
			$Diagnosis_List ='';
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
			$data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();

			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
			
			$this->load->view("nhif_patient/create_nhif_patient_form_reports",$data);
		}	
	}
	
	
	function create_reports($report_type = NULL,$from_date = NULL,$to_date = NULL)
	{   $account_no     =  $this->input->post('account_no'); 
		if($report_type=='' || $from_date=='' || $to_date=='') { echo json_encode(array('success'=>false,'message'=>lang('common_select_file_format_empty'))); die;}
		if(strtotime($from_date) > strtotime($to_date)) { echo json_encode(array('success'=>false,'message'=>lang('common_invalid_date_selection'))); die;  }
		if($account_no!=''){
			$valid= $this->Customer->checkvalid_Account_No($account_no);
			if(!$valid) { echo json_encode(array('success'=>false,'message'=>lang('common_invalid_account_no'))); die; }
		}
		$exist_reports = $this->savesetting->exist_nhif_patient_reports($from_date,$to_date,$account_no); 		
		if(!$exist_reports) { echo json_encode(array('success'=>false,'message'=>lang('common_nhif_report_not_found'))); die; }
	    //****GET NHIF PATIENT RECORDS*****//
		$from_date = date('Y-m-d',strtotime($from_date));
		$to_date   = date('Y-m-d',strtotime($to_date));
	    $nhif_Reports_Data = $this->savesetting->get_nhif_patient_reports( $from_date , $to_date, $account_no );
		$result = '';		
		foreach($nhif_Reports_Data->result() as $RowData) 
		{
		  if($report_type=='pdf')   { $result = $this->create_reports_PDF( $RowData->account_number, $RowData->reference_code_no, $from_date , $to_date ); }
		  if($report_type=='execle'){ $result = $this->create_reports_EXECLE( $RowData->account_number, $RowData->reference_code_no, $from_date , $to_date ); }
		  if($report_type=='doc')   { $result = $this->create_reports_DOC( $RowData->account_number, $RowData->reference_code_no, $from_date , $to_date ); }
		}		   
		if($result){echo json_encode(array('success'=>true,'message'=>lang('common_nhif_report_creted_successfull')));die; }
		else { echo json_encode(array('success'=>false,'message'=>lang('common_nhif_report_creted_fail'))); die; }   		
	}
	
	function create_reports_PDF($account_no = NULL , $reference_code = NULL ,$from_date_1 = NULL , $to_date_1 = NULL)
	{ 			
		$Cust_Data = $this->Customer->get_Patient_Data_By_Account_No($account_no);
		$data['Cust_Data'] = $Cust_Data;
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{  	$Diagnosis_List ='';
			$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 			
		    $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $this->Customer->get_info($Cust_Data->person_id);				  
			$data['reference_code'] = $reference_code;									
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
			$data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();
			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
			$this->load->library('html2pdf');	    
			//Set folder to save PDF to
			$pdf_file_path = "./images/nhif_patient/$Cust_Data->person_id/";
			if (!file_exists($pdf_file_path)) { mkdir($pdf_file_path, 0777, true); }			
			$this->html2pdf->folder($pdf_file_path);				
			//Set the filename to save/download as
			$report_file_name = date('d.m.Y',strtotime($from_date_1)).'_'.date('d.m.Y',strtotime($to_date_1)).'.pdf';
			$this->html2pdf->filename("$report_file_name");	     
			//Set the paper defaults
			$this->html2pdf->paper('a2', 'portrait');			
			$this->html2pdf->html($this->load->view('nhif_patient/create_nhif_reports', $data, true));  
			//PDF was successfully saved or downloaded echo 'PDF saved';
			if($this->html2pdf->create('save')) { return true;}	 else {return false;}		
		}	
	}
	
	
	function view_Nhif_Reports($report_type = NULL, $from_date_1 = NULL, $to_date_1 = NULL)
	{	
	    $account_no =  $this->input->post('account_no'); 	
		if($report_type=='' || $account_no=='' || $from_date_1=='' || $to_date_1==''){ echo '0'; die;}
		if( strtotime($from_date_1) > strtotime($to_date_1)) { echo '1'; die;}
	    $valid= $this->Customer->checkvalid_Account_No($account_no);
		if(!$valid) { echo '2'; die;}	
		$exist= $this->Customer->check_Patient_Category($patient_category =4,$account_no,'account_number');
		if(!$exist) { echo '3'; die;}		
		$exist_reports = $this->savesetting->exist_nhif_patient_reports($from_date_1, $to_date_1, $account_no);		
		if(!$exist_reports) { echo '4'; die;} 		
				
		$nhif_Reports_Data = $this->savesetting->get_nhif_patient_reports( $from_date_1 , $to_date_1, $account_no )->row();
		//print_r($nhif_Reports_Data->reference_code_no);
		$Cust_Data = $this->Customer->get_Patient_Data_By_Account_No($account_no);
		$data['Cust_Data'] = $Cust_Data;
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{  	$Diagnosis_List ='';
			$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 			
		    $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $this->Customer->get_info($Cust_Data->person_id);				  
			$data['reference_code'] = $nhif_Reports_Data->reference_code_no;									
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
			$data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();
			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
			$this->load->view("nhif_patient/create_nhif_patient_form_reports",$data);	
		}			
	}	
	
	function view($item_id = NULL,$fromDate = NULL, $toDate = NULL)  
	{	
		$from_date_1 = date('Y-m-d',$fromDate);$to_date_1 = date('Y-m-d',$toDate);		
	    $nhif_table_data   = $this->savesetting->Get_Table_Row_Data('nhif_patient_reports','id',$item_id);
		$Cust_Data 		   = $this->Customer->get_info($nhif_table_data->person_id);		
		$nhif_Reports_Data = $this->savesetting->get_nhif_patient_reports( $from_date_1 , $to_date_1, $Cust_Data->account_number)->row();
		$data['Cust_Data'] = $Cust_Data;		
		if(isset($Cust_Data->person_id) && $Cust_Data->person_id!='')
		{  	$Diagnosis_List ='';
			$from_date = $from_date_1 .' 00:00:00'; $to_date = $to_date_1 .' 23:59:59'; 			
		    $customer_id 			= $Cust_Data->person_id;
			$data['person_info']	= $Cust_Data;				  
			$data['reference_code'] = $nhif_table_data->reference_code_no;									
			$MTHUA_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diseases',$data['Cust_Data']->person_id,$from_date,$to_date);
			$Diagnosis_List = $MTHUA_Diagnosis;
			//PATIENT DIABETES CARE DIAGNOSIS LIST
			$Diabetes_Diagnosis = $this->savesetting->get_Patient_All_Diagnosis_InString($diagnosis_type='diabetes',$data['Cust_Data']->person_id,$from_date,$to_date);
			if($Diabetes_Diagnosis!=''){ $Diagnosis_List = $Diagnosis_List .','.$Diabetes_Diagnosis;  } else { $Diagnosis_List = $Diabetes_Diagnosis;}
			$data['Diagnosis_List'] =  $Diagnosis_List; 
			$data['ipdin_info']	    = $this->ipd_in_patients->get_ipdin_PatientData($data['Cust_Data']->person_id);
			$data['Total_Days'] 	= $this->ipd_in_patients->getTotal_AdmitDays($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$GetpaymentArray 		= $this->ipd_in_patients->GetTotalDeposit_Amount($data['Cust_Data']->person_id,$data['ipdin_info']->id);
			$data['Total_Costs'] 	= $GetpaymentArray['Total_Amount'];			
			
			$data['Clinical_Doctor_Details']= $this->savesetting->get_First_Clinical_Diagnosis_Doctor($data['Cust_Data']->person_id,$from_date,$to_date); 
			$data['Attendance_Date'] = $this->Customer->getLast_Attendance_date($data['Cust_Data']->person_id,$from_date_1,$to_date_1);
			//calculate Investigation data//
			$data['Investigation_Data'] = $this->labtest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['USS_xrays_Data']     = $this->uss_xray->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Dentals_Data']       = $this->dentals->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Eye_Clinic']         = $this->eyeclinics->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Total_Investigation'] = $data['Investigation_Data']->num_rows() + $data['USS_xrays_Data']->num_rows() + $data['Dentals_Data']->num_rows() + $data['Eye_Clinic']->num_rows();
			$data['Medicine_Data'] 	    = $this->medicinetest->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['otherservices'] 	    = $this->otherservices->get_all($data['Cust_Data']->person_id,$from_date,$to_date);
			$data['Maximum_Limit']  	= max($data['Total_Investigation'], $data['Medicine_Data']->num_rows(), $data['otherservices']->num_rows());
			$this->load->view("nhif_patient/create_nhif_patient_form_reports",$data);	
		}			
	}
	
	function nhif_reports()
	{
	    $this->check_action_permission('view_nhif_patient');
		$this->load->helper('report');
	    $data['controller_name']=strtolower(get_class());
		$data['report_date_range_simple'] = get_simple_date_ranges();
		$data['months'] = get_months();
		$data['days']   = get_days();
		$data['years']  = get_years();
		$selected_month =  date('m');	$selected_day  = date('d'); $selected_year = date('Y');	
		$selected_month2 = date('m');	$selected_day2 = date('d'); $selected_year2 = date('Y');
		$data['selected_month']  	 =  $selected_month; $data['selected_day']  = $selected_day;	
		$data['selected_year']       =  $selected_year;
		$data['selected_month2'] 	 =  $selected_month2; 
		$data['selected_day2']	     =  $selected_day2;	
		$data['selected_year2'] 	 =  $selected_year2;
		
        //**************************************//		
		$from_date = $to_date= date('Y-m-d'); $account_no = '';
		$Total_Amount = '0.00';	
		$_SESSION['NHIF_FROM_DATE'] = $from_date; $_SESSION['NHIF_TO_DATE'] = $to_date; $_SESSION['NHIF_ACCOUT_NO'] = $account_no; 
		$this->load->model('ipd_in_patients');
		$nhif_Reports_Data 	  = $this->savesetting->get_nhif_patient_reports( $from_date, $to_date,$account_no);	
		$total_client_male 	  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date, $sex_type=0,$account_no);
		$total_client_female  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date,$sex_type=1,$account_no);	
	    if($nhif_Reports_Data->num_rows()>0)
		{
		  foreach($nhif_Reports_Data->result() as $rowdata)
		  {			
			$Total_Amount 	+= $this->savesetting->get_nhif_Total_Amount($rowdata->person_id,$from_date,$to_date);
		  }
        }		
		$data['count_table_data'] = '<div id="summary_table">
										<table class="tablesorter report" id="sortable_table">
											<tr><td align="left">'.lang("common_total_amount").'</td> <td align="center" class="summary_bgcolor">'.to_currency($Total_Amount).'</td></tr>
											<tr><td align="left">'.lang("common_total_client").'</td> <td align="center">'.$nhif_Reports_Data->num_rows().'</td></tr>
											<tr><td align="left">'.lang("common_male").'</td>		  <td align="center" class="summary_bgcolor">'.$total_client_male->num_rows().'</td></tr>
											<tr><td align="left">'.lang("common_female").'</td>		  <td align="center">'.$total_client_female->num_rows().'</td></tr>
										</table>
									  </div>';									  
		$config['per_page']		 = 10; 
		$this->load->model('savesetting');
		$method_type 			 = 'nhif_reports';	
		$config['base_url'] 	 = site_url('nhif_patient/sorting_nhif_reports');
		$config['total_rows'] 	 = $nhif_Reports_Data->num_rows();
		
		$this->pagination->initialize($config);
		$data['pagination'] 	 = $this->pagination->create_links();
		$data['controller_name'] = strtolower(get_class());
		$data['per_page'] 		 = $config['per_page'];
		$data['method_type'] 	 = $method_type;			
		$search_data = $this->savesetting->get_nhif_patient_reports($from_date, $to_date,$account_no,$config['per_page']);
		$data['manage_table'] = get_nhif_manage_table($method_type,$search_data,$from_date, $to_date,$this);		
		//**************************************//
		
		$this->load->view("nhif_patient/nhif_reports",$data);
	}		
	
	function sorting_nhif_reports()
	{			
		$account_no = $_SESSION['NHIF_ACCOUT_NO'];
		$from_date  = $_SESSION['NHIF_FROM_DATE'];
		$to_date    = $_SESSION['NHIF_TO_DATE'];
		$this->load->model('ipd_in_patients');
		$method_type = 'nhif_reports';	$Total_Amount = '0.00';	
		$nhif_Reports_Data 	  = $this->savesetting->get_nhif_patient_reports( $from_date, $to_date,$account_no);	
		$total_client_male 	  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date, $sex_type=0,$account_no);
		$total_client_female  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date,$sex_type=1,$account_no);	
	    if($nhif_Reports_Data->num_rows()>0)
		{
		  foreach($nhif_Reports_Data->result() as $rowdata)
		  {			
			$Total_Amount 	+= $this->savesetting->get_nhif_Total_Amount($rowdata->person_id,$from_date,$to_date);
		  }
        }		
		$count_table_data = '<div id="summary_table">
										<table class="tablesorter report" id="sortable_table">
											<tr><td align="left">'.lang("common_total_amount").'</td> <td align="center" class="summary_bgcolor">'.to_currency($Total_Amount).'</td></tr>
											<tr><td align="left">'.lang("common_total_client").'</td> <td align="center">'.$nhif_Reports_Data->num_rows().'</td></tr>
											<tr><td align="left">'.lang("common_male").'</td>		  <td align="center" class="summary_bgcolor">'.$total_client_male->num_rows().'</td></tr>
											<tr><td align="left">'.lang("common_female").'</td>		  <td align="center">'.$total_client_female->num_rows().'</td></tr>
										</table>
									  </div>';
		$per_page=10;
		$config['total_rows'] = $nhif_Reports_Data->num_rows();
		$config['base_url'] = site_url('nhif_patient/sorting_nhif_reports');
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();		
		$search_data = $this->savesetting->get_nhif_patient_reports($from_date, $to_date,$account_no,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0 );
		$manage_table = get_nhif_manage_table_data_rows($method_type,$search_data,$from_date, $to_date,$this);
		echo json_encode(array('manage_table' => $manage_table, 'pagination' => $pagination, 'count_table_data' => $count_table_data));
	}
	
	function search_nhif_reports()
	{
		$account_no = $this->input->post('account_no');
		$from_date  = $this->input->post('from_date');
		$to_date    = $this->input->post('to_date');
		//$from_date = '2015-03-01'; $to_date = '2015-04-10';
		$method_type = 'nhif_reports';	
		$_SESSION['NHIF_FROM_DATE'] = $from_date; $_SESSION['NHIF_TO_DATE'] = $to_date; $_SESSION['NHIF_ACCOUT_NO'] = $account_no; 
		$nhif_Reports_Data 	  = $this->savesetting->get_nhif_patient_reports( $from_date, $to_date,$account_no);	
		$total_client_male 	  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date, $sex_type=0,$account_no);
		$total_client_female  = $this->savesetting->get_nhif_patient_reports_by_sex( $from_date, $to_date,$sex_type=1,$account_no);	
	    $Total_Amount = '0.00';	
		if($nhif_Reports_Data->num_rows()>0)
		{
		  foreach($nhif_Reports_Data->result() as $rowdata)
		  {			
			$Total_Amount 	+= $this->savesetting->get_nhif_Total_Amount($rowdata->person_id,$from_date,$to_date);
		  }
        }	
		$count_table_data = '<div id="summary_table">
								<table class="tablesorter report" id="sortable_table">
									<tr><td align="left">'.lang("common_total_amount").'</td> <td align="center" class="summary_bgcolor">'.to_currency($Total_Amount).'</td></tr>
									<tr><td align="left">'.lang("common_total_client").'</td> <td align="center">'.$nhif_Reports_Data->num_rows().'</td></tr>
									<tr><td align="left">'.lang("common_male").'</td>		  <td align="center" class="summary_bgcolor">'.$total_client_male->num_rows().'</td></tr>
									<tr><td align="left">'.lang("common_female").'</td>		  <td align="center">'.$total_client_female->num_rows().'</td></tr>
								</table>
							  </div>';
		$per_page=1;		
		$search_data = $this->savesetting->get_nhif_patient_reports($from_date, $to_date,$account_no,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'added_date' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url']   = site_url('nhif_patient/sorting_nhif_reports');
		$config['total_rows'] = $nhif_Reports_Data->num_rows();
		$config['per_page']   = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();		
		$data['manage_table'] = get_nhif_manage_table_data_rows($method_type,$search_data,$from_date, $to_date,$this);	
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination'], 'count_table_data' => $count_table_data));
	}	

	function get_form_width()
	{			
		return 600;
	}

}
?>