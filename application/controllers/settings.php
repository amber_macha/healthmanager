<?php
class Settings extends CI_Controller
{
	function __construct()
	{
		parent::__construct('settings');	
		$this->load->model('savesetting');
	}
	
	function SendRequestToCashier($customer_id=-1)
	{	
		if(!$this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
		{ 
			echo json_encode(array('success'=>false,'message'=>lang('error_no_permission'),'item_id'=>-1)); die; 
		}
		$category_type  = $this->input->post('category_type');
		$items_id 	    = $this->input->post('items_id');
		if($items_id!='' && $customer_id!=-1)
		{
			$exist = $this->functions->existCashierRequest($customer_id,$category_type,$items_id);
			if($exist) { echo json_encode(array('success'=>false,'message'=>lang('common_cashier_send_request_already_exist'),'item_id'=>-1));die;}
			$login_uerData = $this->Employee->get_logged_in_employee_info();	
			$item_data = array( 'category_type'	=>	$category_type,
								'lab_test_id' 	=>	$items_id, 		
								'person_id'		=> $customer_id,								
								'employee_id'	=> $login_uerData->person_id,
								'status'		=> '0',
								'added_date'    => date('Y-m-d H:i:s') 
						  );			
			if($this->savesetting->save_data($table_name='manage_cashier_requests',$item_data,$items_id))
			{
				echo json_encode(array('success'=>true,'message'=>lang('common_cashier_send_request_successful'),'item_id'=>$customer_id)); 
				$activity_type = 'customers';
				//***save employee activites in database***// 
				if($category_type=='dentals'){$activity_type = 'dental'; } if($category_type=='investigation'){$activity_type = 'investigation'; }
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>$activity_type,'activity_type_category'=>'send_to_cashier','activity_items_id'=>$items_id,'activity_description'=>lang('common_cashier_send_request_successful'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
			  die;	
			}
			
		}
		 else { echo json_encode(array('success'=>false,'message'=>lang('common_cashier_send_request_error'),'item_id'=>$customer_id)); }
	 }
	 
	function SendRequestToCashier_FromCustomers()
	{	
		if(!$this->Employee->has_module_action_permission('customers', 'send_to_cashier', $this->Employee->get_logged_in_employee_info()->person_id))
		{ 
			echo json_encode(array('success'=>false,'message'=>lang('error_no_permission'),'item_id'=>-1)); die; 
		}
	    $success='';
		$category_type  = $this->input->post('category_type');
		$items_id 	    = $this->input->post('items_id');
		$items_list  = explode('-',$items_id);
		//$bad_words = array('on');
		//foreach($items_list as $key => $value) { if(in_array($value, $bad_words)) { unset($items_list[$key]); } }
		if($items_id!='' && count($items_list)>0)
		{
			$login_uerData = $this->Employee->get_logged_in_employee_info();			
			for($k=0; $k<count($items_list); $k++)
			{
			   $exist = $this->functions->existCashierRequest($items_list[$k],$category_type);
			   if(!$exist && $items_list[$k]>0) 
			   {
				  $item_data = array( 'category_type' => $category_type,
									  'person_id'	  => $items_list[$k],								
									  'employee_id'   => $login_uerData->person_id,
									  'status'		  => '0',
									  'added_date'    => date('Y-m-d H:i:s') 
							  );	
			      $success = $this->savesetting->save_data($table_name='manage_cashier_requests',$item_data,$items_id);	
				}			  
		    }								  
			if($success)
			{
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'customers','activity_type_category'=>'send_to_cashier','activity_items_id'=>implode(',',$items_list),'activity_description'=>lang('common_cashier_send_request_successful'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//
				echo json_encode(array('success'=>true,'message'=>lang('common_cashier_send_request_successful'),'item_id'=>-1)); die;	
			}
			else { echo json_encode(array('success'=>false,'message'=>lang('common_cashier_send_request_already_exist'),'item_id'=>-1)); }			
		}
		 else { echo json_encode(array('success'=>false,'message'=>lang('common_cashier_send_request_error'),'item_id'=>-1)); }
	 }
	
			
}



?>