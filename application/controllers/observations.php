<?php
require_once ("person_controller.php");
class Observations extends Person_controller
{
	function __construct()
	{
		parent::__construct('history');
		$this->load->model('observation');
	}

	function index($customer_id=-1)
	{   	
		$this->check_action_permission('search');
		if($customer_id==-1){redirect(base_url());}
		$exists = $this->Customer->exists($customer_id); if(!$exists) {redirect('customers');}
		unset($_SESSION['OBSERVATION_SEARCH_FROM_DATE']);unset($_SESSION['OBSERVATION_SEARCH_TO_DATE']);unset($_SESSION['OBSERVATION_SEARCH_STATUS']);
		$data['controller_name'] = strtolower(get_class());
		$data['page_title']      = lang('history_patient_observations');
		$data['method_type']     = $data['controller_name'];
		$config['base_url']      = site_url('observations/sorting/'.$customer_id);
		$config['uri_segment']   = 4;
		$config['total_rows'] 	 = $this->observation->count_all($customer_id);
		$config['per_page'] 	 = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination'] 	 = $this->pagination->create_links();		
		$data['form_width']		 = $this->get_form_width();
		$data['per_page'] 		 = $config['per_page'];
		$data['manage_table']	 = get_observations_manage_table($data['method_type'],$this->observation->get_all($customer_id,$data['per_page']),$this);
		$data['form_width'] 	 = $this->get_form_width();
		$data['person_info']     = $this->Customer->get_info($customer_id);	
		$data['customer_id']     = $customer_id;	
		$data['manage_history_menu']= get_patient_history_menu($customer_id);	
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'observations','activity_items_id'=>'','activity_description'=>lang('history_observations_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		$this->load->view('customers_history/manage_observations',$data);
	}
	
	function sorting($customer_id=-1)
	{
		$this->check_action_permission('search');
		$method_type = strtolower(get_class());
		$search=$this->input->post('search');		
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		if ($search)
		{
			$config['total_rows'] = $this->observation->search_count_all($customer_id,$search);
			$table_data = $this->observation->search($customer_id,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'observ_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		else
		{
			$config['total_rows'] = $this->observation->count_all($customer_id);
			$table_data = $this->observation->get_all($customer_id,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'observ_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		}
		$config['base_url'] = site_url('observations/sorting/'.$customer_id);
		$config['uri_segment']   = 4;
		$config['per_page'] = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']  = $this->pagination->create_links();
		$data['manage_table']= get_observations_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	 		
	}
	
	function search($customer_id=-1)
	{
		$this->check_action_permission('search');
		$method_type = strtolower(get_class());
		$search	     =	$this->input->post('search');
		$_SESSION['OBSERVATION_SEARCH_STATUS'] = $this->input->post('find_category_id');
		$per_page=$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$search_data=$this->observation->search($customer_id,$search,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'observ_id' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'desc');
		$config['base_url'] = site_url('observations/search/'.$customer_id);
		$config['uri_segment']   = 4;
		$config['total_rows'] = $this->observation->search_count_all($customer_id,$search);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']= get_observations_manage_table_data_rows($method_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	function searchObservations($customer_id=-1)
	{	 	
		$this->check_action_permission('search');
		$from_date = $this->input->post('from_date');
		$to_date   = $this->input->post('to_date');					
		if($from_date!='' && $to_date!='')
		{
			$_SESSION['OBSERVATION_SEARCH_FROM_DATE'] = date('Y-m-d',strtotime($from_date)).' 00:00:00';
			$_SESSION['OBSERVATION_SEARCH_TO_DATE']   = date('Y-m-d',strtotime($to_date)).' 23:59:59';
		}
		else { $_SESSION['OBSERVATION_SEARCH_FROM_DATE']=''; $_SESSION['OBSERVATION_SEARCH_TO_DATE']=''; }
		$method_type = strtolower(get_class());
		$config['base_url']   = site_url('observations/sorting/'.$customer_id);
		$config['total_rows'] = $this->observation->count_all($customer_id);
		$config['per_page']   = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['pagination']   = $this->pagination->create_links();
		$data['manage_table'] =	get_observations_manage_table_data_rows($method_type,$this->observation->get_all($customer_id,$config['per_page']),$this);
		echo json_encode(array('manage_table'=>$data['manage_table'],'pagination'=>$data['pagination']));
	}
	
	function view($customer_id=-1,$item_id=-1)
	{	
		$this->check_action_permission('add_update'); 
		$this->load->model('savesetting'); 
		$data['person_id'] = $customer_id;	
		$data['item_id']   = $item_id;	
		$data['item_info'] = $this->savesetting->Get_Table_Row_Data('customers_observations','observ_id',$item_id);
		if($item_id!='-1'){	
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'observations','activity_items_id'=>$item_id,'activity_description'=>lang('history_observations_view_details'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//
		}
		$this->load->view("customers_history/form_observations",$data);
	}	
	
	function save($customer_id=-1,$item_id=-1)
	{	
		$this->check_action_permission('add_update');
		$this->load->model('savesetting'); 		
		$login_uerData = $this->Employee->get_logged_in_employee_info();		
		$item_data = array( 'observ_status'			 => $this->input->post('observation_status'),
							'observ_blood_pressure'  => $this->input->post('observation_blood_pressure'),
							'observ_pulse_heart_rate'=> $this->input->post('observation_pulse_heart_rate'),
							'observ_respiratory_rate'=> $this->input->post('observation_respiratory_rate'),
							'observ_temperature'	 => $this->input->post('observation_temperature'),
							'observ_iv'				 => $this->input->post('observation_iv'),
							'observ_oral'			 => $this->input->post('observation_oral'),
							'observ_ngt'			 => $this->input->post('observation_ngt'),
							'observ_vomiting'		 => $this->input->post('observation_vomiting'),
							'observ_urine'			 => $this->input->post('observation_urine'),
							'observ_stool' 		 	 => $this->input->post('observation_stool'),
							'observ_treatment'		 => $this->input->post('observation_treatment'),
							'observ_remarks'		 => $this->input->post('observation_remarks'),
							'person_id'				 => $customer_id,
							'employee_id'			 => $login_uerData->person_id,
							'observ_added_date' 	 => date('Y-m-d H:i:s'),
						  );
		if($this->savesetting->saveUpdateTable($table_name='customers_observations',$item_data,'observ_id',$item_id))
		{
			$activity_items_id = $this->db->insert_id();			
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('history_successful_adding_observation'),'person_id'=>$customer_id));
				$activity_description = lang('history_successful_adding_observation');				
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('history_successful_updated_observation'),'person_id'=>$customer_id));
				$activity_items_id = $item_id;
				$activity_description = lang('history_successful_updated_observation');
			}
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'observations','activity_items_id'=>$item_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//			
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('history_error_adding_updating_observation'),'person_id'=>$customer_id));
		}
	}		
	
	function delete($observations = NULL)
	{		
		$this->check_action_permission('delete');
		$items_to_delete=$this->input->post('ids');
		$bad_words = array('on');	
		foreach($items_to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($items_to_delete[$key]);	} }	
		if($this->observation->delete_list($items_to_delete , $method_type = 'observations'))
		{
			echo json_encode(array('success'=>true,'message'=>lang('history_observation_successful_deleted').' '. count($items_to_delete).' '.lang('history_observation_one_or_multiple')));
		    //***save employee activites in database***//
			$activity_items_id = implode(',',$items_to_delete);
			$activity_description = lang('comment_successful_deleted').' '. count($items_to_delete).' '.lang('history_observation_one_or_multiple');
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'observations','activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else { echo json_encode(array('success'=>false,'message'=>lang('history_observation_error_deleted'))); }
	}		

	/* Gives search suggestions based on what is being searched for */
	function suggest($customer_id=-1)
	{
		$suggestions = $this->observation->get_search_suggestions($customer_id,$this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	function create_reports_PDF($customer_id=-1)
	{ 	$from_date 		  = $this->input->post('from_date');
		$to_date 		  = $this->input->post('to_date');
		if($from_date!='' && $to_date!='')
		{
			$_SESSION['OBSERVATION_SEARCH_FROM_DATE'] = date('Y-m-d',strtotime($from_date)).' 00:00:00';
			$_SESSION['OBSERVATION_SEARCH_TO_DATE']   = date('Y-m-d',strtotime($to_date)).' 23:59:59';
		}
		else { $_SESSION['OBSERVATION_SEARCH_FROM_DATE']=''; $_SESSION['OBSERVATION_SEARCH_TO_DATE']=''; }			
		$method_type = 'observations_pdf';
		$data['company_name'] = $this->config->item('company');
		$data['manage_table'] = get_observations_manage_table($method_type,$this->observation->get_all($customer_id),$this); 
		$this->load->library('pdf/html2pdf');	    
		//Set folder to save PDF to4
		$pdf_file_path = "./images/observations/reports/pdf/";
		$this->html2pdf->folder($pdf_file_path);				
		//Set the filename to save/download as
		$report_file_name = 'observations_Reports_'.time().'.pdf';
		//$report_file_name = 'test.pdf';
		$this->html2pdf->filename("$report_file_name");	     
		//Set the paper defaults
		$this->html2pdf->paper('a2', 'portrait');			
		$this->html2pdf->html($this->load->view('customers_history/observations_reports_PDF', $data, true));  
		//PDF was successfully saved or downloaded echo 'PDF saved';
		$sucess = $this->html2pdf->create('save');
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'history','activity_type_category'=>'observations','activity_items_id'=>'','activity_description'=>lang('history_observation_download_pdf'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
		if($sucess)	
		{ echo json_encode(array('success'=>true,'message'=>'images/observations/reports/pdf/'.$report_file_name)); }	
		else { echo json_encode(array('success'=>false,'message'=>lang('transactions_error_to_download_pdf_file'))); }     		
	}	
	
	function get_form_width()
	{
		return 550;
	}
	


}

?>