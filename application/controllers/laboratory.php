<?php
require_once ("secure_area.php");
class Laboratory extends Secure_area 
{
	function __construct()
	{
		parent::__construct('laboratory');	
		$this->load->language('laboratory'); 
		$this->load->model('laboratories');
		$this->load->helper('laboratory_helper');
	}
	

	function index()
	{
	    $this->check_action_permission('search');
	    $data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'reports','activity_type_category'=>'diabetes','activity_items_id'=>'','activity_description'=>lang('diabetes_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		//***save employee activites in database***//
		$this->load->view("laboratory/home",$data);
	}
	
	//*********Management Department Registration Code*****************************//
	function manage_department($item_id=-1)
	{
		//if($item_id==-1 ){redirect(base_url());} 
		$this->check_action_permission('add_update');
		$data['department_list'] = $this->laboratories->GetSearchLaboratoryList($category_type='department',$search='');				
		$this->load->view("laboratory/manage_department",$data);
	}	
	
	//*********Specimen Management Code*****************************//
	function manage_specimen($item_id=-1)
	{
		//if($item_id==-1 ){redirect(base_url());} 
		$this->check_action_permission('add_update');
		$data['department_list'] = $this->laboratories->GetSearchLaboratoryList($category_type='laboratory_specimen',$search='');				
		$this->load->view("laboratory/manage_specimen",$data);
	}
	
	//*********Laboratory Department Code***********************//
	function manage_laboratory($item_id=-1)
	{
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		$data['laboratory_department'] = $this->laboratories->GetSearchLaboratoryList($category_type='laboratory_department',$search='');	
		$data['department_list'] = $this->savesetting->Get_Table_Category_Data('laboratory_items','department','laboratory_name',$order='asc',$limit=1000);
		$data['specimen_list']   = $this->savesetting->Get_Table_Category_Data('laboratory_items','laboratory_specimen','laboratory_name',$order='asc',$limit=1000);
		$this->load->view("laboratory/manage_laboratory",$data); 
	}    
	
	function savelaboratory($item_id=-1)
	 {	
	    $this->check_action_permission('add_update');		
		 $servicename = addslashes($this->input->post('servicename')); 
		 $specimen_name = addslashes($this->input->post('specimen_name')); 
		 $category_type = addslashes($this->input->post('category_type')); 
		 $message ='0';
		 if($category_type=='department') 
		 { if($servicename=='') { echo json_encode(array('success'=>false,'message'=>lang('laboratory_department_name_empty'))); die; } 
		   $valid = $this->laboratories->existsLaboratory_Items($category_type , $servicename, $item_id );
		   if($valid) { echo json_encode(array('success'=>false,'message'=>lang('laboratory_department_name_exist'))); die; }	
		 }
		 if($category_type=='laboratory_specimen') 
		 { if($servicename=='') { echo json_encode(array('success'=>false,'message'=>lang('laboratory_specimen_name_empty'))); die; } 
		   $valid = $this->laboratories->existsLaboratory_Items($category_type , $servicename, $item_id );
		   if($valid) { echo json_encode(array('success'=>false,'message'=>lang('laboratory_specimen_name_exist'))); die; }	
		 }
		 
		 if($category_type=='laboratory_department') 
		 { 
			if($servicename=='' ) { echo json_encode(array('success'=>false,'message'=>lang('laboratory_department_name_empty'))); die;} 
			if($specimen_name=='') {  echo json_encode(array('success'=>false,'message'=>lang('laboratory_specimen_selection_empty'))); die;}
			$valid = $this->laboratories->existsLaboratory_Items($category_type , $servicename, $item_id, $specimen_name );
			if($valid) { echo json_encode(array('success'=>false,'message'=>lang('laboratory_department_name_exist'))); die;}	
		 }	    
			if($category_type=='department') { $activity_type_category = 'department'; }
			if($category_type=='laboratory_specimen')  { $activity_type_category = 'laboratory_specimen'; }
			if($category_type=='laboratory_department'){ $activity_type_category = 'laboratory_department'; }
				
			if(isset($item_id) && $item_id!='-1'){ 			  
				$result = $this->db->query("update ".$this->db->dbprefix('laboratory_items')." set laboratory_name ='".$servicename."',related_item_id ='".$specimen_name."' where category_type='".$activity_type_category."' and id = '".$item_id."' ");
				$activity_items_id = $item_id;
				$activity_description = lang('laboratory_items_updated_successfull');
			}
		   else 
			{   $result = $this->db->query("insert into ".$this->db->dbprefix('laboratory_items')." set laboratory_name ='".$servicename."',related_item_id ='".$specimen_name."',category_type='".$activity_type_category."',status='0' ");
				$activity_items_id = $this->db->insert_id();
				$activity_description = lang('laboratory_items_added_successfull');
			}
			if($result) 
			{ 
			//***save employee activites in database***//				
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'laboratory','activity_type_category'=>$activity_type_category,'activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>$activity_description)); die;
		  }
		 else{ echo json_encode(array('success'=>false,'message'=>lang('laboratory_added_item_failed'))); die;}		  
	 }
	 
	function searchAll_LaboratoryList($customer_id=-1)
	 {	
		$search = addslashes($this->input->post('service')); 
		$specimen_name = addslashes($this->input->post('specimen_name')); 
		$category_type = addslashes($this->input->post('category_type')); 
		echo $search_data=$this->laboratories->GetSearchLaboratoryList($category_type, $search, $specimen_name);die;
	 }
	 
	 function savedepartment($item_id=-1)
	 {	
	    $this->check_action_permission('add_update');
		// $type = $this->uri->segment(3);
		 //$service_id = $this->uri->segment(3);		
		 $servicename = addslashes($this->input->post('servicename')); 
		 $message ='';
		 if($servicename!='')
		 {
		   $valid = $this->laboratories->existsLaboratory_Items($servicename, $codeNumber,$item_id,$category_type);
			if($valid=='no') { $message = '0'; }
			if($valid=='yes')
			 {     
				if($category_type=='mtuha_daignoses')   { $category = 'diseases'; $activity_type_category = 'mtuha_daignoses'; }
				if($category_type=='clinical_daignoses'){ $category = 'clinic_diseases'; $activity_type_category = 'clinical_daignoses';}
				if($category_type=='daibetes')		    { $category = 'diabetes'; $activity_type_category = 'daibetes';}
				
			      $lab_category = $this->db->dbprefix('lab_category');
				  if(isset($item_id) && $item_id!=''){ 			  
					$result = $this->db->query("update $lab_category set lab_name ='".$servicename."',code_number ='".$codeNumber."' where category_type='".$category."' and id = '".$service_id."' ");
					$activity_items_id = $item_id;
					if($category_type=='mtuha_daignoses'){ $activity_description = lang('common_mtuha_daignoses_updated'); }
					if($category_type=='clinical_daignoses'){ $activity_description = lang('common_clinical_daignoses_updated');}
					if($category_type=='daibetes')		 { $activity_description = lang('common_daibetes_updated');}	
					
				  }
				  else {
					$result = $this->db->query("insert into $lab_category  set lab_name ='".$servicename."',code_number ='".$codeNumber."',category_type='".$category."',status='0' ");
					$activity_items_id = $this->db->insert_id();
					if($category_type=='mtuha_daignoses'){ $activity_description = lang('common_mtuha_daignoses_added'); }
					if($category_type=='clinical_daignoses'){ $activity_description = lang('common_clinical_daignoses_added');}
					if($category_type=='daibetes')		 { $activity_description = lang('common_daibetes_added');}
				  }
			    if($result) { $message = '1'; }  else { $message = '2'; }
				//***save employee activites in database***//				
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'investigation','activity_type_category'=>$activity_type_category,'activity_items_id'=>$activity_items_id,'activity_description'=>$activity_description);	
				$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
				//***save employee activites in database***//  
			 }
		 } 
		 echo $message; die;		
	 }
	
	 function getLaboratoryData_byid($customer_id=-1)
	 {	
		$services_id = addslashes($this->input->post('services_id'));
		$category_type = addslashes($this->input->post('category_type'));
		echo $search_data=$this->laboratories->GetSpecimenCheckboxData($services_id,$category_type);die;
	 }	
	 
	 function deleteAllLaboratoryList($customer_id=-1)
	  {		
		$this->check_action_permission('search');
		$message='';
		$category_type = $this->uri->segment(3);
		$value = $this->uri->segment(4);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);
		$success=$this->laboratories->delete_list($value,$category_type);			
			
		if($success==1)
		{  $message = lang('laboratory_items_deleted_successfull'); 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'laboratory','activity_type_category'=>$category_type,'activity_items_id'=>$activity_items_id,'activity_description'=>lang('laboratory_items_deleted_successfull'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
		}
		else {  $message = lang('laboratory_items_deleted_failed'); }
		echo $message; die;	
	}
	
	function deleteAllReagent($customer_id=-1)
	  {		
		$this->check_action_permission('search');
		$message='';
		$category_type = $this->uri->segment(3);
		$value = $this->uri->segment(4);
		$to_delete = explode('-',$value);
		$bad_words = array('on');	
		foreach($to_delete as $key => $value) {	if(in_array($value, $bad_words)) { unset($to_delete[$key]);	} }	
		$value = implode('-',$to_delete);
		$success=$this->laboratories->delete_list($value,$category_type);				
		if($success==1)
		{ 
			//***save employee activites in database***//
			$test2 = explode('-',$value);  $activity_items_id = implode(',',$test2);
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'laboratory','activity_type_category'=>$category_type,'activity_items_id'=>$activity_items_id,'activity_description'=>lang('laboratory_items_deleted_successfull'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1);								 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>lang('laboratory_items_deleted_successfull'),'person_id'=>-1)); die;
		}
		else {  echo json_encode(array('success'=>false,'message'=>lang('laboratory_items_deleted_failed'),'person_id'=>-1)); }		
	}
	
	function reagent_management()
	{
	    $this->check_action_permission('search');
	    $data['controller_name'] = strtolower(get_class());
		$config['per_page']		 = 2; 
		$this->load->model('savesetting');
		$data['specimen_list']   = $this->savesetting->Get_Table_Category_Data('laboratory_items','laboratory_specimen','laboratory_name',$order='asc',$limit=1000);
		$data['category_list']	 = $this->savesetting->Get_Table_Category_Data('measurement_category','hospital_department','category_name');
		$method_type 			 = 'reagent';
		$config['base_url']		 = site_url('laboratory/sorting_reagent');
		$config['total_rows'] 	 = $this->laboratories->count_all_tables($method_type,'manage_reagent');
		
		$this->pagination->initialize($config);
		$data['pagination'] 	 = $this->pagination->create_links();
		$data['controller_name'] = strtolower(get_class());
		$data['form_width']		 = $this->get_form_width();
		$data['per_page'] 		 = $config['per_page'];
		$string = $this->laboratories->get_all_tables($method_type,$data['per_page']);
		$data['manage_table']	 = get_Lab_manage_table($method_type ,$this->laboratories->get_all_tables($method_type,$data['per_page']),$this);
		$data['add_new_title'] 	 = lang('diabetes_registration_patient');
		$data['page_title'] 	 = lang('diabetes_patients_listing_page_title');
		$data['method_type'] 	 = $method_type;		
		$this->load->view("laboratory/reagent_management",$data);
	}
	
	function sorting_reagent()
	{
		$this->check_action_permission('search');
		$search='';
		$method_type = 'reagent';
		$per_page=2;
		if ($search)
		{
			$config['total_rows']= $this->laboratories->search_count_all( $method_type ,$specimen_name='',$department_name='',$receiept_no='',$hospital_department='',$member_pfnumber='',$customer_id='');
			$table_data = $this->laboratories->search($method_type,$specimen_name='',$department_name='',$receiept_no='',$hospital_department='',$member_pfnumber='',$customer_id='',$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'first_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		else
		{
			$config['total_rows']= $this->laboratories->count_all_tables($method_type );
			$table_data = $this->laboratories->get_all_tables($method_type,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'first_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		}
		$config['base_url']		 = site_url('laboratory/sorting_reagent');
		$config['per_page'] 	 = $per_page; 
		$this->pagination->initialize($config);
		$data['pagination']		 = $this->pagination->create_links();
		$data['manage_table']	 = get_Lab_manage_table_data_rows($method_type,$table_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));	
	}	
	
	/* Returns customer table data rows. This will be called with AJAX. */
	function search($customer_id=-1)
	{
		$this->check_action_permission('search');
		$method_type 	 = $this->input->post('method_type');
		$specimen_name	 = $this->input->post('specimen_name');
		$department_name = $this->input->post('department_name');
		$receiept_no 	 = $this->input->post('receiept_no');
		$hospital_department = $this->input->post('hospital_department');
		if($this->input->post('receipt_date')!=''){ $receipt_date = date('Y-m-d',strtotime($this->input->post('receipt_date'))); }else {$receipt_date='';}
		$member_pfnumber = $this->input->post('member_pfnumber');		
		$per_page=2;
		$search_data=$this->laboratories->search($method_type,$specimen_name,$department_name,$receiept_no,$hospital_department,$member_pfnumber,$receipt_date,$customer_id,$per_page,$this->input->post('offset') ? $this->input->post('offset') : 0, $this->input->post('order_col') ? $this->input->post('order_col') : 'first_name' ,$this->input->post('order_dir') ? $this->input->post('order_dir'): 'asc');
		$config['base_url'] = site_url('laboratory/sorting_reagent');
		$config['total_rows'] = $this->laboratories->search_count_all($method_type,$specimen_name,$department_name,$receiept_no,$hospital_department,$member_pfnumber,$receipt_date,$customer_id);
		$config['per_page'] = $per_page ;
		$this->pagination->initialize($config);				
		$data['pagination'] = $this->pagination->create_links();
		$data['manage_table']=get_Lab_manage_table_data_rows($method_type,$search_data,$this);
		echo json_encode(array('manage_table' => $data['manage_table'], 'pagination' => $data['pagination']));
	}
	
	 function getDepartment()
	 {	
		$specimen_item = addslashes($this->input->post('specimen_item'));
		$this->load->model('savesetting');
		$search_data=$this->laboratories->getLabDepartment_DropDownList($specimen_item);
		echo json_encode(array('success'=>false,'datalist'=>$search_data));		  
	 }	
	
	function save($customer_id=-1)
	{
		$this->check_action_permission('add_update');		
		$department_name     = $this->input->post('department_name');
		$specimen_item_name  = $this->input->post('specimen_item_name');
		$customer_id 		 = $this->input->post('specific_input_data');
		$receiept_no 		 = $this->input->post('receiept_no');
		$hospital_department = $this->input->post('hospital_department');
		$member_pfnumber 	 = $this->input->post('member_pfnumber');
		$registration_date 	 = date('Y-m-d',strtotime($this->input->post('receipt_date')));
		if($customer_id=='' || $this->input->post('specimen_item_name')=='' || $this->input->post('department_name')=='' || $this->input->post('receiept_no')=='' || $this->input->post('hospital_department')=='')
		 {
		    echo json_encode(array('success'=>false,'message'=>lang('common_invalid_enter_required'), 'person_id'=>$customer_id)); die;
		 }
        $result = $this->laboratories->exists_receipt($receiept_no);
        if($result) { echo json_encode(array('success'=>false,'message'=>lang('laboratory_receiept_no_exist'), 'person_id'=>$customer_id)); die;}
		$item_data = array('lab_dept_id'	 	 => $this->input->post('department_name'),
						   'lab_specimen_id'     => $this->input->post('specimen_item_name'),
						   'receiept_no'		 => $this->input->post('receiept_no'),
						   'hospital_department' => $this->input->post('hospital_department'),
						   'registration_date' 	 => $registration_date,
						   'employee_id' 		 => $member_pfnumber,
						   'add_by_employee_id'	 => $this->Employee->get_Logged_SessionData('person_id'),
						   'person_id'			 => $customer_id);		
		if($this->laboratories->save('manage_reagent',$item_data,'person_id', $customer_id))
		{	$activity_description = lang('laboratory_reagent_successful_adding');		
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'laboratory','activity_type_category'=>'laboratory_reagent','activity_items_id'=>$customer_id,'activity_description'=>$activity_description);	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			echo json_encode(array('success'=>true,'message'=>$activity_description,'person_id'=>$customer_id));
			//***save employee activites in database***//
		}
		else { echo json_encode(array('success'=>false,'message'=>lang('laboratory_reagent_error_adding_updating'),'person_id'=>-1)); }
	}
	
	function get_row()
	{
		$method_type = $this->uri->segment(3); 
		$item_id = $this->input->post('row_id');
		$data_row = get_Lab_manage_highlight_data_rows($method_type,$this->laboratories->get_Item_ArrayData($method_type,$item_id),$this);
		echo $data_row;
	}
	
	//*********Laboratory Department Code***********************//
	function update_status($items_id=-1,$customer_id=-1)
	{   
		$this->check_action_permission('add_update');
		$this->load->model('savesetting');
		if($items_id==-1 || $customer_id==-1) {redirect('laboratory/reagent_management');}
		$data['items_data'] 	 = $this->savesetting->Get_Table_Row_Data('manage_reagent','id',$items_id);	
		$data['category_list']   = $this->savesetting->Get_Table_Category_Data('measurement_category','laboratory_rejection_reason','category_name');
		$data['items_id'] 		 = $items_id;
		$data['customer_id'] 	 = $customer_id;
		$this->load->view("laboratory/form_update_status",$data); 
	}
	
    function chanage_status($items_id=-1,$customer_id=-1)
	{	
		$this->check_action_permission('add_update');	
		$method_type 	= $this->input->post('method_type');
		$status_type 	= $this->input->post('status_type');	
		$status_reasion = $this->input->post('status_reasion');	
		if($method_type=='' || $items_id==-1 || $customer_id==-1) { redirect('laboratory/reagent_management'); }
		$item_data = array('status'=>$status_type,'status_reason'=>$status_reasion,'updated_employee_by'=>$this->Employee->get_Logged_SessionData('person_id'));
		if($this->laboratories->Change_Dispatch_Status($method_type,$item_data,$items_id)) 
		{  echo json_encode(array('success'=>true,'message'=>lang('common_change_status_successful'),'person_id'=>$items_id)); 
		  //***save employee activites in database***//
		   $Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'laboratory','activity_type_category'=>'laboratory_reagent','activity_items_id'=>$items_id,'activity_description'=>lang('common_change_status_successful'));	
		   $this->Employee->save_activites($Activity_Data,$item_id=-1); 								 
		  //***save employee activites in database***//
		}
		
		else {	echo json_encode(array('success'=>false,'message'=>lang('common_change_status_error'),'person_id'=>$items_id));	}	 
	}	
	
	function get_form_width()
	{			
		return 600;
	}

}
?>