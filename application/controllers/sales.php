<?php
require_once ("secure_area.php");
class Sales extends Secure_area
{
	function __construct()
	{
		parent::__construct('sales');
		$this->load->library('sale_lib');
	}

	function index()
	{   //UNSET($_SESSION['EPAY_SALES_TRANSACTION_ID']);UNSET($_SESSION['ADEED_COMMENTS_TRANSACTION_ID']);
		if ($this->config->item('track_cash')) {
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'sales','activity_type_category'=>'sales_information','activity_items_id'=>'','activity_description'=>lang('sales_module_view'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//		
			if ($this->input->post('opening_amount') != '') {
				$now = date('Y-m-d H:i:s');				
				$cash_register = new stdClass();				
				$cash_register->employee_id = $this->session->userdata('person_id');
				$cash_register->shift_start = $now;
				$cash_register->open_amount = $this->input->post('opening_amount');
				$cash_register->close_amount = 0;
				$cash_register->cash_sales_amount = 0;
				$this->Sale->insert_register($cash_register);				
				redirect(site_url('sales'));
			} else if ($this->Sale->is_register_log_open()) {
				$this->_reload(array(), false);
			} else { 
				$this->load->view('sales/opening_amount');
			}
		} else {
			$this->_reload(array(), false);
		}
	}
	
	function closeregister() 
	{
		if (!$this->Sale->is_register_log_open()) 
		{
			redirect(site_url('home'));
			return;
		}
		$cash_register = $this->Sale->get_current_register_log();
		$continueUrl = $this->input->get('continue');
		if ($this->input->post('closing_amount') != '') {
			$now = date('Y-m-d H:i:s');
			$cash_register->shift_end = $now;
			$cash_register->close_amount = $this->input->post('closing_amount');
			$cash_register->cash_sales_amount = $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, $cash_register->shift_end);			
			unset($cash_register->register_log_id);
			$this->Sale->update_register_log($cash_register);
			if ($continueUrl == 'logout') {
				redirect(site_url('home/logout'));
			} else {
				redirect(site_url('home'));
			}
		} else {
			$this->load->view('sales/closing_amount', array(
				'continue'=>$continueUrl ? "?continue=$continueUrl" : '',
				'closeout'=>to_currency($cash_register->open_amount + $this->Sale->get_cash_sales_total_for_shift($cash_register->shift_start, date("Y-m-d H:i:s")))
			));
		}
	}
	
	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),100);
		
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->get('term'),100));
		echo json_encode($suggestions);
	}

	function customer_search()
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function select_customer()
	{
		$data = array();
		$customer_id = $this->input->post("customer");
		if ($this->Customer->exists($customer_id,$method_type='sales'))
		{
			$this->sale_lib->set_customer($customer_id);	
		}
		else
		{
			$data['error']=lang('sales_unable_to_add_customer');
		}
		$this->_reload($data);
	}

	function change_mode()
	{
		$mode = $this->input->post("mode");
		$this->sale_lib->set_mode($mode);
		$this->_reload();
	}
	
	function set_comment() 
	{
 	  $this->sale_lib->set_comment($this->input->post('comment'));
	}
	
	function set_email_receipt()
	{
 	  $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
	}

	//Alain Multiple Payments
	function add_payment()
	{		
		$data=array();
		$this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			if ( $this->input->post('payment_type') == lang('sales_gift_card') )
				$data['error']=lang('sales_must_enter_numeric_giftcard');
			else
				$data['error']=lang('sales_must_enter_numeric');
				
 			$this->_reload($data);
 			return;
		}
		$payment_type=$this->input->post('payment_type');
		if(isset($_SESSION['EPAY_SALES_TRANSACTION_ID']) && $_SESSION['EPAY_SALES_TRANSACTION_ID']!='') {$payment_type = 'epay'; }		
		if ( $payment_type == lang('sales_giftcard') )
		{
			if(!$this->Giftcard->exists($this->Giftcard->get_giftcard_id($this->input->post('amount_tendered'))))
			{
				$data['error']=lang('sales_giftcard_does_not_exist');
				$this->_reload($data);
				return;
			}
			
			$payments = $this->sale_lib->get_payments();
			$payment_type=$this->input->post('payment_type').':'.$this->input->post('amount_tendered');
			$current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $current_payments_with_giftcard;
			if ( $cur_giftcard_value <= 0 && $this->sale_lib->get_total() > 0)
			{
				$data['error']=lang('sales_giftcard_balance_is').' '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) ).' !';
				$this->_reload($data);
				return;
			}
			elseif ( ( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ) > 0 )
			{
				if($this->Giftcard->get_giftcard_balanceshow( $this->input->post('amount_tendered') )){
				$this->sale_lib->set_giftcard(to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ));
				$this->sale_lib->clear_giftcard();
					} 
					else 
					{
 					
					};
				$data['warning']=lang('sales_giftcard_balance_is').' '.to_currency( $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) - $this->sale_lib->get_total() ).' !';
			}
			$payment_amount=min( $this->sale_lib->get_amount_due(), $this->Giftcard->get_giftcard_value( $this->input->post('amount_tendered') ) );
		}
		else
		{
			$payment_amount=$this->input->post('amount_tendered');
		}
		
		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount ) )
		{
			$data['error']=lang('sales_unable_to_add_payment');
		}
		
		$this->_reload($data);
	}

	//Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$this->sale_lib->delete_payment(rawurldecode($payment_id));
		$this->_reload();
	}

	function add()
	{
		$data=array();
		$mode = $this->sale_lib->get_mode();
		$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
		$quantity = $mode=="sale" ? 1:-1;

		if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode=='return')
		{
			$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
		{ 
			$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt, $quantity);
		}
		elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity))
		{    //echo 'case db1'; 
			$data['error']=lang('sales_unable_to_add_item');
		}
		
		if($this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
		{  //echo 'case db2'; 
			$data['warning'] = lang('sales_quantity_less_than_zero');
		}
		$this->_reload($data);
	}

	function edit_item($line)
	{
		$data= array();

		$this->form_validation->set_rules('price', 'lang:items_price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|numeric');

        $description = $this->input->post("description");
        $serialnumber = $this->input->post("serialnumber");
		$price = $this->input->post("price");
		$quantity = $this->input->post("quantity");
		$discount = $this->input->post("discount");


		if ($this->form_validation->run() != FALSE)
		{
			$this->sale_lib->edit_item($line,$description,$serialnumber,$quantity,$discount,$price);
		}
		else
		{
			$data['error']=lang('sales_error_editing_item');
		}
		
		if($this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		{
			$data['warning'] = lang('sales_quantity_less_than_zero');
		}


		$this->_reload($data);
	}

	function delete_item($item_number)
	{
		$this->sale_lib->delete_item($item_number);
		$this->_reload();
	}

	function delete_customer()
	{
		$this->sale_lib->delete_customer();
		if(isset($_SESSION['EPAY_SALES_TRANSACTION_ID'])) UNSET($_SESSION['EPAY_SALES_TRANSACTION_ID']);
		if(isset($_SESSION['ADEED_COMMENTS_TRANSACTION_ID'])) UNSET($_SESSION['ADEED_COMMENTS_TRANSACTION_ID']);
		$this->_reload();
	}

	function complete()
	{   $data['acc_no']='';  $registration_no='';
		$data['cart']=$this->sale_lib->get_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format());
		$customer_id=$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$data['payments']=$this->sale_lib->get_payments();
		$data['giftcard']=$this->sale_lib->get_giftcard();
		$data['amount_change']=$this->sale_lib->get_amount_due() * -1;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		
        $data['valid_admission_fee'] = $this->valid_admission_fee();
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
			if($cust_info->patient_type=='-1')	$data['acc_no'] = $cust_info->other_account_number; else $data['acc_no'] = $cust_info->account_number; 
			$registration_no = '<div id="employee">'.lang('common_student_registration_no').':'. $cust_info->occupation_registration_no.'</div>';
		}
		
		$data['registration_no'] = $registration_no; 
		//SAVE sale to database
		$sale_id = $this->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'], $this->sale_lib->get_suspended_sale_id(), 0,$data['valid_admission_fee']);
		$data['sale_id']='TDRRH '.$sale_id;
		if ($data['sale_id'] == 'TDRRH -1')
		{
			$data['error_message'] = lang('sales_transaction_failed');
		}
		else
		{	
		    //SAVE Appointment in database//
		    if(isset($_SESSION['PATIENT_WAITING_APPOINTMENT_SALE']) && $_SESSION['PATIENT_WAITING_APPOINTMENT_SALE']!='' && count($_SESSION['PATIENT_WAITING_APPOINTMENT_SALE'])>0)
			{
			  $waiting_appointment = $_SESSION['PATIENT_WAITING_APPOINTMENT_SALE'];
			  $total = count($waiting_appointment); //print_r($total);die;
			  for($k=0; $k < $total;  $k++)
			  {
			    $insert_data = array('appointment_id'=>$waiting_appointment[$k],'sale_receipt_no'=>$data['sale_id'],'person_id'=>$customer_id,'added_date'=>date('Y-m-d: H:i:s'),'status'=>'0');
				$this->db->insert('customer_appointment', $insert_data);
			  }
			  unset($_SESSION['PATIENT_WAITING_APPOINTMENT_SALE']);
			}
				//********START CODE FOR UPDATE DAYABSE OF GOV AND CENTRALIZED DATABASE********//	
				if(isset($_SESSION['EPAY_SALES_TRANSACTION_ID']) && $_SESSION['EPAY_SALES_TRANSACTION_ID']!='')
				{	
					$Cards_Sales_Data = array('sale_id' =>$sale_id);
					$this->db->where('customer_id',$customer_id);
					$this->db->where('epay_transaction_id',$_SESSION['EPAY_SALES_TRANSACTION_ID']);
					$this->db->update('manage_cards_transactions',$Cards_Sales_Data);
					
					$facility_sale_id = $this->config->item('hospital_facility_id').'/'.$sale_id;				
					$ShopingUrl = GOVE_TANZANIA_CENTERLIZE_SERVER."/epayments/Update_Facility_Transactions"; 
					$EpaySaleData = "Facility_Sale_ID=$facility_sale_id&EPAY_Transaction_ID=".$_SESSION['EPAY_SALES_TRANSACTION_ID'];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $ShopingUrl);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $EpaySaleData);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					$Response = curl_exec($ch);
					curl_close($ch);
					if(isset($_SESSION['EPAY_SALES_TRANSACTION_ID'])) UNSET($_SESSION['EPAY_SALES_TRANSACTION_ID']);
					if(isset($_SESSION['ADEED_COMMENTS_TRANSACTION_ID'])) UNSET($_SESSION['ADEED_COMMENTS_TRANSACTION_ID']);
				}	
				//********END CODE FOR UPDATE DAYABSE OF GOV AND CENTRALIZED DATABASE********//
				
				//***save employee activites in database***//
				$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'sales','activity_type_category'=>'sales_information','activity_items_id'=>$data['sale_id'],'activity_description'=>lang('sales_transaction_succssfull'));	
				$this->Employee->save_activites($Activity_Data,$item_id=-1); 
				//***save employee activites in database***//
			if ($this->sale_lib->get_email_receipt() && !empty($cust_info->email))
			{
				$this->load->library('email');
				$config['mailtype'] = 'html';				
				$this->email->initialize($config);
				$this->email->from($this->config->item('email'), $this->config->item('company'));
				$this->email->to($cust_info->email); 

				$this->email->subject(lang('sales_receipt'));
				$this->email->message($this->load->view("sales/receipt_email",$data, true));	
				$this->email->send();
			}
		}
		
		$this->load->view("sales/receipt",$data);
		$this->sale_lib->clear_all();
	}
	
	function email_receipt($sale_id)
	{
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=$this->sale_lib->get_amount_due($sale_id) * -1;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$data['sale_id']='TDRRH '.$sale_id;
		if (!empty($cust_info->email))
		{
			$this->load->library('email');
			$config['mailtype'] = 'html';				
			$this->email->initialize($config);
			$this->email->from($this->config->item('email'), $this->config->item('company'));
			$this->email->to($cust_info->email); 

			$this->email->subject(lang('sales_receipt'));
			$this->email->message($this->load->view("sales/receipt_email",$data, true));	
			$this->email->send();
		}

		$this->sale_lib->clear_all();
	}
	
	function receipt($sale_id=-1)
	{
		if($sale_id==-1){redirect('sales');}
		$data['acc_no']=''; $registration_no=''; 
		$sale_info = $this->Sale->get_info($sale_id)->row_array(); 
		if($sale_info['sale_id']=='' || $sale_info['sale_id']=='0'){ redirect('sales');}
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=$this->sale_lib->get_amount_due($sale_id) * -1;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['acc_no']=$cust_info->account_number;
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
			$registration_no = '<div id="employee">'.lang('common_student_registration_no').':'. $cust_info->occupation_registration_no.'</div>';
		}
		$data['registration_no'] = $registration_no; 
		$data['sale_id']='TDRRH '.$sale_id;
		$this->load->view("sales/receipt",$data);
		$this->sale_lib->clear_all();

	}
	
	function edit($sale_id)
	{
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}		

		$data['employees'] = array();
		foreach ($this->Employee->get_all()->result() as $employee)
		{
			$data['employees'][$employee->person_id] = $employee->first_name . ' '. $employee->last_name;
		}

		$data['sale_info'] = $this->Sale->get_info($sale_id)->row_array();
				
		
		$this->load->view('sales/edit', $data);
	}
	
	function delete($sale_id)
	{
		$data = array();
		
		if ($this->Sale->delete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		
		$this->load->view('sales/delete', $data);
		
	}
	
	function undelete($sale_id)
	{
		$data = array();
		
		if ($this->Sale->undelete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}
		
		$this->load->view('sales/undelete', $data);
		
	}
	
	function save($sale_id)
	{
		$sale_data = array(
			'sale_time' => date('Y-m-d', strtotime($this->input->post('date'))),
			'customer_id' => $this->input->post('customer_id') ? $this->input->post('customer_id') : null,
			'employee_id' => $this->input->post('employee_id'),
			'comment' => $this->input->post('comment')
		);
		
		if ($this->Sale->update($sale_data, $sale_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('sales_successfully_updated')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('sales_unsuccessfully_updated')));
		}
	}
	
	function _payments_cover_total()
	{
		$total_payments = 0;

		foreach($this->sale_lib->get_payments() as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		/* Changed the conditional to account for floating point rounding */
		if ( ( $this->sale_lib->get_mode() == 'sale' ) && ( ( to_currency_no_money( $this->sale_lib->get_total() ) - $total_payments ) > 1e-6 ) )
		{
			return false;
		}
		
		return true;
	}
	function reload()
	{
		$this->_reload();
	}
	
	function _reload($data=array(), $is_ajax = true)
	{
		$person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->sale_lib->get_cart();
		$data['modes']=array('sale'=>lang('sales_sale'),'return'=>lang('sales_return'));
		$data['mode']=$this->sale_lib->get_mode();
		$data['items_in_cart'] = $this->sale_lib->get_items_in_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['items_module_allowed'] = $this->Employee->has_module_permission('items', $person_info->person_id);
		$data['comment'] = $this->sale_lib->get_comment();
		$data['email_receipt'] = $this->sale_lib->get_email_receipt();
		$data['payments_total']=$this->sale_lib->get_payments_total();
		$data['amount_due']=$this->sale_lib->get_amount_due();
		$data['payments']=$this->sale_lib->get_payments();
		$data['valid_admission_fee'] = $this->valid_admission_fee();
		
	/* 	$data['payment_options']=array(
			lang('sales_cash') => lang('sales_cash'),
			lang('sales_check') => lang('sales_check'),
			lang('sales_giftcard') => lang('sales_giftcard'),
			lang('sales_debit') => lang('sales_debit'),
			lang('sales_credit') => lang('sales_credit')
		); */
		
		$payment_options_vj=$this->Module_action->get_permission_actions($person_info->person_id);
		$vj=array();
		foreach($payment_options_vj as $row){
		$vj[lang('sales_'.$row['action_id'])]=lang('sales_'.$row['action_id']);
		
		}
		$data['payment_options']=$vj;
		foreach($this->Appconfig->get_additional_payment_types() as $additional_payment_type)
		{
			$data['payment_options'][$additional_payment_type] = $additional_payment_type;
		}

		$customer_id=$this->sale_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name.' '.$info->last_name.($info->company_name==''  ? '' :' ('.$info->company_name.')');
			$data['customer_email']=$info->email;
			$data['customer_id']=$customer_id;
		}
		$data['payments_cover_total'] = $this->_payments_cover_total();
		
		if ($is_ajax)
		{
			$this->load->view("sales/register",$data);
		}
		else
		{
			$this->load->view("sales/register_initial",$data);
		}
	}

    function cancel_sale()
    {
    	$this->sale_lib->clear_all();
		UNSET($_SESSION['EPAY_SALES_TRANSACTION_ID']);UNSET($_SESSION['ADEED_COMMENTS_TRANSACTION_ID']);
		//***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'sales','activity_type_category'=>'sales_information','activity_items_id'=>'','activity_description'=>lang('sales_cancel_items'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//
    	$this->_reload();

    }
	
	function suspend()
	{
		$data['cart']=$this->sale_lib->get_cart();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['total']=$this->sale_lib->get_total();
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format());
		$customer_id=$this->sale_lib->get_customer();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		//Alain Multiple payments
		$data['payments']=$this->sale_lib->get_payments();
		$data['amount_change']=$this->sale_lib->get_amount_due() * -1;
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$total_payments = 0;
		foreach($data['payments'] as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}		
		$sale_id = $this->sale_lib->get_suspended_sale_id();
		//SAVE sale to database
		$data['sale_id']='TDRRH '.$this->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'], $sale_id, 1);
		if ($data['sale_id'] == 'TDRRH -1')
		{
			$data['error_message'] = lang('sales_transaction_failed');
		}
		$this->sale_lib->clear_all();
		UNSET($_SESSION['EPAY_SALES_TRANSACTION_ID']);UNSET($_SESSION['ADEED_COMMENTS_TRANSACTION_ID']);		
		$this->_reload(array('success' => lang('sales_successfully_suspended_sale')));
	}
	
	function suspended()
	{
		$data = array();
		$data['suspended_sales'] = $this->Sale->get_all_suspended()->result_array();
		$this->load->view('sales/suspended', $data);
	}
	
	function unsuspend()
	{
		$sale_id = $this->input->post('suspended_sale_id');
		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_sale($sale_id);
		$this->sale_lib->set_suspended_sale_id($sale_id);
    	$this->_reload(array(), false);
	}
	
	function delete_suspended_sale()
	{
		$suspended_sale_id = $this->input->post('suspended_sale_id');
		if ($suspended_sale_id)
		{
			$this->sale_lib->delete_suspended_sale_id();
			$this->Sale->delete($suspended_sale_id);
		}
    	$this->_reload(array('success' => lang('sales_successfully_deleted')), false);
	}
	
	function valid_admission_fee()
	{
		$admission_fee = $this->sale_lib->get_admission_fee();
		if(isset($admission_fee) && $admission_fee!='')
		{
		  $data = array('payment_type'=>'Tika', 'Admission_Fee'=>$admission_fee);
		  return($data);
		}		
	}
	
	function view_cashier_requests($request_type=-1)
	{	
		if($request_type==-1 ){redirect(base_url());} 
		$data['request_type']  = $request_type;
		$data['requests_list'] = $this->functions->Get_All_Cashier_RequestList($request_type);
        //***save employee activites in database***//
		$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'sales','activity_type_category'=>'send_to_cashier','activity_items_id'=>'','activity_description'=>lang('sales_view_send_to_cashier_list'));	
		$this->Employee->save_activites($Activity_Data,$item_id=-1); 
		//***save employee activites in database***//		
		$this->load->view("sales/view_cashier_requests",$data);
	}
	
	function update_CashierRequest($action_type=-1,$item_id=-1)
	{	
		if($action_type==-1 && $item_id==-1 ){ redirect(base_url()); } 
	    $result = $this->functions->update_Cashier_Request($item_id ,$action_type);	
		if( $result) 
		{  
			//***save employee activites in database***//
			$Activity_Data = array('person_id' =>$this->Employee->get_Logged_SessionData('person_id'),'activity_type'=>'sales','activity_type_category'=>'send_to_cashier','activity_items_id'=>'','activity_description'=>lang('sales_send_request_to_cashier_update_success'));	
			$this->Employee->save_activites($Activity_Data,$item_id=-1); 
			//***save employee activites in database***//
			echo json_encode(array('success'=>true,'message'=>lang('sales_send_request_to_cashier_update_success'))); die;
		}	 
		else { echo json_encode(array('success'=>false,'message'=>lang('sales_send_request_to_cashier_update_error'))); }	
	}
	
	function getCashiertReuestTotal($RequestType=-1)
	{   
		$CountRequest = $this->functions->getTotal_CashierRequest($RequestType);		
		echo json_encode(array('success'=>true,'CountRequest'=>$CountRequest)); die;	 
	}	
	
	function search_Casher_Request($customer_id=-1)
	{	
		$search = addslashes($this->input->post('request_name')); 
		$request_type  = addslashes($this->input->post('request_type'));
		echo $search_data=$this->functions->Get_All_Cashier_RequestList($request_type, $search);die;  
	}
	
	function getEPayments($customer_id=-1)
	{ 		
		if($customer_id==-1){ echo json_encode(array('success'=>false,'message'=>lang('customers_selected_empty'))); die;}
		$Payable_Amount = $this->input->post('Total_Amount');
		$Health_Card_NO = $this->input->post('health_card_no');
		$person_info  = $this->Customer->get_info($customer_id);		
		if($person_info->person_id==''){ echo json_encode(array('success'=>false,'message'=>lang('customers_selected_empty'))); die; }
		if($person_info->person_id==''){ echo json_encode(array('success'=>false,'message'=>lang('customers_selected_empty'))); die; }
		
		if($Payable_Amount < 0){ echo json_encode(array('success'=>false,'message'=>lang('sales_payment_amount_invalid'))); die; } 
        if($Health_Card_NO==''){ echo json_encode(array('success'=>false,'message'=>lang('common_health_card_no_required'))); die; } 
		if(strlen($Health_Card_NO)!='19'){ echo json_encode(array('success'=>false,'message'=>lang('common_health_card_no_minlength'))); die; } 		
		
		//********START CODE FOR CENTRALIZED DATABASE********//
		$ShopingUrl = GOVE_TANZANIA_CENTERLIZE_SERVER."/epayments/getEPayments"; 
		$Facility_ID = $this->config->item('hospital_facility_id');
		$Employee_Code = $this->Employee->get_logged_in_employee_info()->employee_account_code;
		$Computer_IP = $_SERVER['REMOTE_ADDR']; 
		$ShopingData = "Payable_Amount=$Payable_Amount&Health_Card_NO=$Health_Card_NO&Customer_Account_No=$person_info->account_number&Facility_ID=$Facility_ID&Employee_Code=$Employee_Code&Computer_IP=$Computer_IP";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ShopingUrl);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $ShopingData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		$Response = curl_exec($ch);
		curl_close($ch);
		//print_r($Response);die; 
		$Response =  json_decode($Response, true);
		if($Response['success']==true)   
		{   
			if($Response['EPAY_Sales_Transaction_ID']!='')
			{ 
				$_SESSION['EPAY_SALES_TRANSACTION_ID'] = $Response['EPAY_Sales_Transaction_ID']; 
				$SalesData = array( 'customer_id'		  => $customer_id,
								    'transaction_amount' => $Payable_Amount,
									'transaction_time'	  => date('Y-m-d H:i:s'),
									'employee_id'		  => $this->Employee->get_logged_in_employee_info()->person_id,
									'computer_ip_address'=> $Computer_IP,
									'epay_transaction_id'=> $Response['EPAY_Sales_Transaction_ID'],
									'transaction_status' => '1'									 
								  );
				$this->load->model('savesetting');					
				$result = $this->savesetting->saveUpdateTable('manage_cards_transactions',$SalesData,'transaction_id',$item_id=-1);
				echo json_encode(array('success'=>true,'message'=> '<b>'.lang('sales_trans_id').'</b> : <span class="sales_id">'.$Response['EPAY_Sales_Transaction_ID']. ' </span><br>'.$Response['message'])); 
			} 					
		} else { echo json_encode(array('success'=>false,'message'=>$Response['message'])); }		
		
	  //********END CODE FOR CENTRALIZED DATABASE********//
	}
	
	//********START CODE FOR DISCOUNT FOR SALES ITEMS********//
	function discount_all()
	{
		$discount_all_percent = (int)$this->input->post('discount_all_percent');
		$this->sale_lib->discount_all($discount_all_percent);
		$this->_reload();
	}
	
}
?>