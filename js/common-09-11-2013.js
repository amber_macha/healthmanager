function get_dimensions() 
{
	var dims = {width:0,height:0};
	
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    dims.width = window.innerWidth;
    dims.height = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    dims.width = document.documentElement.clientWidth;
    dims.height = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    dims.width = document.body.clientWidth;
    dims.height = document.body.clientHeight;
  }
  
  return dims;
}

function set_feedback(text, classname, keep_displayed)
{
	if(text!='')
	{
		$('#feedback_bar').removeClass();
		$('#feedback_bar').addClass(classname);
		$('#feedback_bar').text(text);
		$('#feedback_bar').slideDown(250);
		var text_length = text.length;
		var text_lengthx = text_length*50;

		if(!keep_displayed)
		{
			$('#feedback_bar').show();
			
			setTimeout(function()
			{
				$('#feedback_bar').slideUp(250, function()
				{
					$('#feedback_bar').removeClass();
				});
			},text_lengthx);
		}
	}
	else
	{
		$('#feedback_bar').hide();
	}
}


$(document).keydown(function(event) 
{
	if (event.keyCode == 113)
	{
		window.location = SITE_URL + "/sales";
	}
});


function clicktohide(obj)
	{
		document.getElementById(obj.id).style.background = "";
	}
function validateCustomer()
{  
  email = document.getElementById('email').value;
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (document.getElementById('first_name').value == "") { document.getElementById('first_name').style.background = "#FFFF00"; return false; } 
	if (document.getElementById('last_name').value == "") { document.getElementById('last_name').style.background = "#FFFF00"; return false; } 
	if (document.getElementById('email').value == "") { document.getElementById('email').style.background = "#FFFF00"; return false; } 	
	if(email!='' && reg.test(email) == false){document.getElementById('email').style.background = "#FFFF00";  return false;}	 
	if (document.getElementById('phone_number').value == "") { document.getElementById('phone_number').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('address_1').value == "") { document.getElementById('address_1').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('city').value == "") { document.getElementById('city').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('state').value == "") { document.getElementById('state').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('country').value == "") { document.getElementById('country').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('religion').value == "") { document.getElementById('religion').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('sex').value == "") { document.getElementById('sex').style.background = "#FFFF00"; return false; } 		 
	if (document.getElementById('age').value == "") { document.getElementById('age').style.background = "#FFFF00"; return false; } 	
	if (document.getElementById('occupation').value == "") { document.getElementById('occupation').style.background = "#FFFF00"; return false; } 		   
	if (document.getElementById('nationality').value == "") { document.getElementById('nationality').style.background = "#FFFF00"; return false; } 
	
}
