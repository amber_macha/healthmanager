INSERT INTO `gary`.`phppos_modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES ('add_update', 'giftcards', 'module_action_add_update', '62');
INSERT INTO `gary`.`phppos_modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES ('action_delete', 'giftcards', 'module_action_delete', '63');

INSERT INTO `gary`.`phppos_modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES ('cash', 'payments', 'module_cash', '62');

INSERT INTO `gary`.`phppos_modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES ('gift_card', 'payments', 'module_gift_card', '63'), ('credit_card', 'payments', 'module_credit_card', '64');