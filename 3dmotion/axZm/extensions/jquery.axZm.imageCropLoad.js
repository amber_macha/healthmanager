/**
* Plugin: jQuery AJAX-ZOOM, jquery.axZm.imageCropLoad.js
* Copyright: Copyright (c) 2010-2014 Vadim Jacobi
* License Agreement: http://www.ajax-zoom.com/index.php?cid=download
* Version: 4.1.9
* Extension Version 1.0
* Date: 2014-09-02
* URL: http://www.ajax-zoom.com
* Documentation: http://www.ajax-zoom.com/index.php?cid=docs
* Example: http://www.ajax-zoom.com/examples/example35_clean.php
*/

;(function($){

	var getParameterByName = function(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	        
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	// Load json file with some error handling
	var getJSONdataFromFile = function(){
		var cropJsonURL = opt.cropJsonURL;
		
		if (!cropJsonURL){return;}
		url = cropJsonURL.replace(/[^a-zA-Z0-9-_:\/.?&=]/g, '');
		//aZcR_jsFileName
		$.ajax({
			url: cropJsonURL,
			dataType: 'json',
			cache: false,
			success: function(d){
				proceedJSONdata(d);
			},
			error: function(a){
 				var status = a.status;
 				var txt = 'Error loading JSON file!';
				if (status == '200'){
					$.fn.axZm.zoomAlert('Loaded JSON file ('+cropJsonURL+') contains errors', txt, false, false);
				}else{
					$.fn.axZm.zoomAlert('An error '+a.status+' occurred while loading JSON from '+cropJsonURL, txt, false, false);
				}
			}
		});
	};
	
	// Process json data after loading
	var proceedJSONdata = function(d){
		if (!$.isArray(d) || d.length == 0){
			// Alert directly in AJAX-ZOOM
			$.fn.axZm.zoomAlert(
				'JSON passed from ('+opt.cropJsonURL+') is not an array or array is empty', 
				'Error loading crops', 
				false, 
				false
			);
			return;
		}
		
		var arrCount = d.length, // number of crops from json
			i = 0; // 
		
		$(d).each(function(m, o) {

			var dataObj = {
					thumbNumber: m+1,
					crop: o.crop,
					url: o.url ? o.url : null,
					qString: o.qString ? o.qString : null,
					zoomID: o.zoomID ? o.zoomID : null,
					imgName: o.imgName,
					contentLocation: o.contentLocation ? o.contentLocation : null,
					title: o.title ? o.title : null,
					descr: o.descr ? o.descr : null
				},
				imgCropperPath;
			
			// Look where to get the croped thumb from
			if (dataObj.url){
				imgCropperPath = dataObj.url;
				dataObj.qString = imgCropperPath.replace($.fn.axZm.installPath()+'zoomLoad.php?', '')
			}else if (dataObj.qString){
				imgCropperPath = $.fn.axZm.installPath()+'zoomLoad.php?'+dataObj.qString;
				dataObj.url = imgCropperPath;
			} else if (o.contentLocation){
				// directly from cache 
				imgCropperPath = o.contentLocation;
			}
			
			if (!imgCropperPath){
				i++;
				return;
			}
			
			dataObj.imgCropperPath = imgCropperPath;
			
			var tempImg = new Image();
			
			// Preload image
			$(tempImg)
			.error(function(a) {
				if (typeof console != 'undefined'){
					console.log('Error loading thumb: '+imgCropperPath);
				}
				i++;
			})
			.load(function(){
				
				// Create li ellement
				newLi = $('<li />')
				// Set data attr to be able to fine it later over selector
				.attr('data-cropNum', dataObj.thumbNumber)
				.data('crop', dataObj)
				// Append thumb image to li
				.append(
					$('<img>')
					.attr('src', imgCropperPath)
				);
				
				// Use axZmThumbSlider "appendThumb" API to add the thumb to the slider
				$('#'+opt.sliderID).axZmThumbSlider(
					'appendThumb', 
					newLi, 
					// Click event
					function(){
						
						// Read data previously attached to the image
						var azJcrop = $(this).data('crop');
						
						// handleDescr will be executed after spinTo or zoomTo
						var handleDescr = function(){
							if ($.isFunction(opt.handleTexts)){
								opt.handleTexts(azJcrop.title, azJcrop.descr);
							}else if ((azJcrop.title || azJcrop.descr) && typeof console != 'undefined'){
								console.log('AJAX-ZOOM: no function defined to handle title and desciption');
							}
						};
						
						// Parameters from JSON as object
						var zoomToParameters = {
							x1: azJcrop.crop[0],
							y1: azJcrop.crop[1],
							x2: azJcrop.crop[2],
							y2: azJcrop.crop[3]
						};
						
						// Callback for 2D
						var zoomToCallback = function(){
							// Trigger AJAX-ZOOM API 
							// http://www.ajax-zoom.com/index.php?cid=docs#api_zoomTo
							$.fn.axZm.zoomTo(
								// Add handleDescr as callback to the options object passed to zoomTo
								$.extend({},zoomToParameters,{callback: handleDescr})
							);
						};
						

						// 2D image and same image from gallery selected
						if ($.axZm.zoomGA[$.axZm.zoomID]['img'] == azJcrop.imgName && !$.axZm.spinMod){
							zoomToCallback();
						}
						else{
							// 360 / 3D mode
							if ($.axZm.spinMod){
								// Trigger AJAX-ZOOM API 
								// http://www.ajax-zoom.com/index.php?cid=docs#api_spinTo
								$.fn.axZm.spinTo(
									azJcrop.imgName, 
									opt.spinToSpeed || 'auto', 
									opt.spinToMotion || null, // easing
									handleDescr, // callback
									zoomToParameters
								);
							}else{
								// Trigger AJAX-ZOOM API 
								// http://www.ajax-zoom.com/index.php?cid=docs#api_zoomSwitch
								$.fn.axZm.zoomSwitch(azJcrop.imgName, null, true, null, zoomToCallback);
							}
						}
						
					}, 
					// Callback after thumb added
					function(el, no){
						// After thumb added
						i++;
						if (i == arrCount){
							// If it is last thumb sort them in the right order
							// (they are appended as sooon as the images are preloaded in the above code, 
							// so it is not guaranteed that they are appended in the correct order)
							$('#cropSlider').axZmThumbSlider('sortByData', 'data-cropNum');
						}
					}
				); 
				
				
			}).attr('src', imgCropperPath);
		
		});
	};
	
	var opt;
	
	$.axZmImageCropLoad = function(a){
		
		// opt.cropJsonURL - url of the json with crop data
		// opt.sliderID - if of the slider
		// opt.spinToSpeed - optional pass duration of spinning when clicked on the thumb
		// opt.spinToMotion - optional pass easing type of the spinning animation
		// opt.handleTexts - function after spin
		
		if (!a.cropJsonURL){
			a.cropJsonURL = getParameterByName('cropJsonURL');
		}
		
		opt = a;
		
		getJSONdataFromFile();
	};
	
})(jQuery);