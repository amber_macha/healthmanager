<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AJAX-ZOOM 360/3D Responsive example</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="imagetoolbar" content="no">

<?php
if (strpos($_SERVER['HTTP_USER_AGENT'],"iPhone")){
	echo "<meta name=\"viewport\" content=\"width=device-width,  minimum-scale=0.5, maximum-scale=0.5, user-scalable=no\">";
}else{
	echo "<meta name=\"viewport\" content=\"width=device-width,  minimum-scale=1, maximum-scale=1, user-scalable=no\">";
}
?>
 
<style type="text/css" media="screen"> 
	html {font-family: Tahoma, Arial; font-size: 10pt;}
 	p {text-align: justify; text-justify: newspaper; margin-top: 0px;}
</style>
 
<!--  Include jQuery core into head section -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<!--  AJAX-ZOOM javascript && CSS -->
<link href="../axZm/axZm.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../axZm/jquery.axZm.js"></script>

</head>
<body>

<div style="height: 100px; background-color: #B9CC52">
	<h2 style="margin: 0; padding: 10px;">AJAX-ZOOM 360/3D responsive example, no PHP code in this file. 
	Integration can be done on the template level.
	</h2>	
</div>

<!-- Basically only an element with any certain ID is needed, we call it AZplayerParentContainer here -->
<div style="height: 400px; float: left; width: 80%;" id="AZplayerParentContainer"></div>

<!-- We have made this right element with buttons absolute because while animating jQuery not always makes the best job when animating %, which might break the layout -->
<div style="height: 400px; position: absolute; z-index: 1; top: 100px; right: 0px; width: 20%; background-color: #CECECE" id="naviToTheRight">
	<div style="padding: 10px">
		<input type="button" style="width: 170px; text-align: left;" value="Resize to 50% width" onclick="jQuery.fn.axZm.resizeStart(3000); jQuery('#AZplayerParentContainer, #naviToTheRight').stop(true, false).css('width', '50%'); setTimeout(function(){jQuery.fn.axZm.resizeStart(1)})"><br>
		<input type="button" style="width: 170px; text-align: left;" value="Resize to 500px height" onclick="jQuery.fn.axZm.resizeStart(3000); jQuery('#AZplayerParentContainer, #naviToTheRight').stop(true, false).css('height', '500px'); setTimeout(function(){jQuery.fn.axZm.resizeStart(1)})"><br>
		<input type="button" style="width: 170px; text-align: left;" value="Resize with animation" onclick="jQuery.fn.axZm.resizeStart(3000); jQuery('#AZplayerParentContainer').stop(true, false).animate({height: '700px', width: '70%'},{queue: false, easing: 'easeOutCirc', duration: 1500, complete: function(){jQuery.fn.axZm.resizeStart(1);}}); jQuery('#naviToTheRight').stop(true, false).animate({height: '700px', width: '30%'},{queue: false, easing: 'easeOutCirc', duration: 1500})"><br>		
		
		<!-- jQuery.fn.axZm.fillArea can be triggered in certain callbacks, same can be achieved with autoZoom option set in php config file -->
		<input type="button" style="width: 170px; text-align: left;" value="API $.fn.axZm.fillArea" onclick="jQuery.fn.axZm.fillArea({ callback: function(){} });"><br>		
		<input type="button" style="width: 170px; text-align: left;" value="Reset size and zoom" onclick="jQuery.fn.axZm.resizeStart(3000); jQuery('#AZplayerParentContainer').stop(true, false).css({height: '400px', width: '80%'}); jQuery('#naviToTheRight').stop(true, false).css({height: '400px', width: '20%'}); jQuery.fn.axZm.resizeStart(1,  jQuery.fn.axZm.zoomReset);"><br>
	</div>
</div>


<script type="text/javascript">
	// If you are triggering jQuery.fn.axZm.openFullScreen outside of jQuery(document).ready,
	// then make sure it done after your parent container, in this case "AZplayerParentContainer"
	jQuery(document).ready(function() {
		// Define some callbacks
		var callbacks = {
			onBeforeStart: function(){
				// Some of the options can be set directly as js var in this callback, e.g. 
				// jQuery.axZm.spinReverse = true;
				// jQuery.axZm.spinReverseZ = true;
				
				// Optionally perform some DOM manipulations
				jQuery('.zoomContainer').css({backgroundColor: '#FFFFFF'});
			},
 
			onLoad: function(){
				jQuery.axZm.fullScreenExitText = false;
			}
		}
			
		// Define your custom parameter query string
		// example=17 has many presets for 360 images*
		// 3dDir - best of all absolute path to the folder with 360/3D images
		
		// * By defining the query string parameter example=17 
		// some default settings from /axZm/zoomConfig.inc.php are overridden in 
		// /axZm/zoomConfigCustom.inc.php after elseif ($_GET['example'] == 17){. 
		// So if changes in /axZm/zoomConfig.inc.php have no effect - 
		// look for the same options /axZm/zoomConfigCustom.inc.php; 
		// To quickly check a different set of options you can write example=spinIpad
		// which is already preset in /axZm/zoomConfigCustom.inc.php
		
		// Documentation - http://www.ajax-zoom.com/index.php?cid=docs#api_openFullScreen
		jQuery.fn.axZm.openFullScreen('../axZm/', 'example=17&3dDir=/pic/zoom4d/Uvex_Occhiali', callbacks, 'AZplayerParentContainer', false, true);
	});
</script>

</body>
</html>