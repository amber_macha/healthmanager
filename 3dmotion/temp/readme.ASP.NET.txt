Ajax-Zoom PHP component can run efficiently on ASP.NET 4.0 as a native fully managed 64-bit .NET application, as a part of ASP.NET web site. 
This is achieved thanks to Phalanger PHP compiler. 

You do not need to install PHP on your Server!

Requirements:
IIS 7 (or later) with ASP.NET 4.0 Web Site configured or Visual Studio 2010 for development.

The basic installation id fairly easy:
1.	Extract the download package somewhere into ASP.NET site. 
2.	Ensure folders \pic\{cache|temp|zoomthumb|zoomgallery|zoomtiles|zoommap|zoomtiles_80|*} are writable
3.	Merge provided "web.config" into your "web.config"
4.	Ensure Bin/Dynamic is writable for your IIS user account
5.	Example project: You can extract the archive into the root of your ASP.NET server, 
	or open it as an existing web site from Visual Studio 2010 from any location 
	(note you must set empty "Virtual Path" in "Properties" of the web site). 
	Also you might need to set "Enable 32-Bit Applications" under Advanced Settings -> General 
	if you have 64bit operating system.
6.	If you experince any troubles try to adjust: 
	$zoom['config']['fpPP']
	$zoom['config']['installPath']
	in /axZm/zoomConfig.inc.php manually.