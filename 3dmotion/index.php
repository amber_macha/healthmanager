<?php
/**
* Plugin: jQuery AJAX-ZOOM
* Copyright: Copyright (c) 2010-2014 Vadim Jacobi
* License Agreement: http://www.ajax-zoom.com/index.php?cid=download
* Version: 4.1.6
* Date: 2014-05-01
* URL: http://www.ajax-zoom.com
* Documentation: http://www.ajax-zoom.com/index.php?cid=docs
*/
ini_set("display_errors", 0);
if(!session_id()){session_start();}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AJAX-ZOOM Demo Installation</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css" media="screen"> 
	html {font-family: Tahoma, Arial; font-size: 10pt; margin: 0; padding: 0;}
	body {margin: 0; padding: 0;}
	h2 {padding:0px; margin: 20px 0px 10px 0px; font-size: 16pt;}
	h3 {padding:0px; margin: 20px 0px 10px 0px; font-size: 14pt;}
	p { }
	.exampleImg{
		border: #000000 2px solid;
		cursor: pointer;
		margin: 0px 7px 10px 0px;
		overflow: hidden;
		width: 300px;
		height: 169px;
		background-repeat: no-repeat;
		background-position: center;
	}
	
	.rbox{
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
		border-radius: 4px 4px 4px 4px;
	}
	
	.shadow{
		-moz-box-shadow: 0px 2px 2px #C2C2C2;
		box-shadow: 0px 2px 2px #C2C2C2;
		-webkit-box-shadow: 0px 2px 2px #C2C2C2;	
	}
	
	.exampleHead{
		background-color: #000000; 
		background-color: rgba(0,0,0,0.9); 
		line-height: 20px; 
		margin: 5px; 
		min-width: 30px;
		padding: 0px 3px 3px 3px; 
		border: #FFFFFF 2px solid;
		color: #FFFFFF;
		display: inline-block;
		text-decoration: none;
		position: relative;
		float: right;
		top: -8px;
	}
	.clearfix:after {content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0;}	 
	.clearfix {display: inline-block;}
	html[xmlns] .clearfix {display: block;}
	* html .clearfix {height: 1%;}
</style>
</head>
<body>
<?php

function makeLink($string){
	$string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i", "$1http://$2",$string);
	$string = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\">$1</A>",$string);
	$string = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i","<A HREF=\"mailto:$1\">$1</A>",$string);	
	return $string;
}


function readtxt($f){
	$return = '';
	$filename = $f;
    $ini_handle = fopen($filename, "r");
    $return = fread($ini_handle, filesize($filename));
	$return = nl2br($return);
	return makeLink($return);
}

function natIndex($array, $reverse){
	$i=1; $nArray=array();
	natcasesort($array);
	if ($reverse){
		$array = array_reverse($array);
	}
	foreach ($array as $k=>$v){
		$nArray[$i]=$v;
		$i++;
	}
	return $nArray;
}

function checkRemoteFile($url){
	if (function_exists('curl_version')){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(curl_exec($ch) !== false){
	        return $url;
	    }
	    else{
	        return 'axZm/icons/firefox.jpg';
	    }
	}else{
		return $url;
	}
}

function getIonCubeVersion(){
    if (function_exists('ioncube_loader_iversion')) {
        $liv = ioncube_loader_iversion();
        $lv = sprintf("%d.%d.%d", $liv / 10000, ($liv / 100) % 100, $liv % 100);
        return $lv;
	}else{
		return '';
	}
}

echo "<DIV style='width: 928px; margin: 0px auto;'>\n";
	echo "<h1>AJAX-ZOOM Demo Installation</h1>";
	echo "Local examples do not contain high-resolution images. Please find these and other examples with high-resolution images at<br>
	<a href='http://www.ajax-zoom.com/index.php?cid=examples' target='_blank'>http://www.ajax-zoom.com/index.php?cid=examples</a><br><br>";
	
	// This info is for AJAX-ZOOM team if you ask us to look why AJAX-ZOOM does not work for you
	echo 'PHP: ' . PHP_VERSION .'<br>';
	echo 'OS: ' . PHP_OS .'<br>';
	echo 'Safe mode: ' . ini_get('safe_mode') .'<br>';
	echo 'Sapi: ' . PHP_SAPI .'<br>';
	if (getIonCubeVersion()){
		 echo 'Ioncube loader version: '. getIonCubeVersion().'<br>';
	}
 
	
	if (ini_set ("memory_limit", "128M") === false){
		echo '<span style="color: red">Warning: AJAX-ZOOM will be not able to change memory_limit with ini_set() and make image tiles for very large images</span><br>';
	} 
	
	if (ini_set ("max_execution_time", "90") === false){
		echo '<span style="color: red">Warning: AJAX-ZOOM will be not able to set max_execution_time with ini_set() dynamically and make image tiles for very large images </span><br>';
	} 	
	
	
	echo "<h2>Readme.txt</h2>";
	echo "<DIV>";
	if (file_exists('readme.txt')){
		echo readtxt('readme.txt');
	}else{
		echo 'readme.txt is not present...';
	}
	echo "</DIV>";

	// Some tests
	$error = '';
	$warning = '';
	$info = '';
	
	if (!defined('PHALANGER')){
		$php_version = phpversion();
		
		if (intval($php_version) < 5){
			$error = '<li>You need PHP 5 to run AJAX-ZOOM. Currently you are running PHP version: '.$php_version.'</li>';
		}
		if (!function_exists('gd_info')){
			$error .= '<li>GD Lib is not installed on your server. AJAX-ZOOM needs it to operate properly.</li>';
		}
		
		$extensions = get_loaded_extensions();
		$ionCube = false;
		$gettext = false;
	
		foreach ($extensions as $k=>$v){
			if (stristr($v, 'ioncube')){
				$ionCube = true;
			}

			if (stristr($v, 'gettext')){
				$gettext = true;
			}
		}
	
		if (!$ionCube){
			if (!ini_get('enable_dl')){
				$error .= '<li>It seems that ionCube is not installed and 
				because dynamically loaded extensions aren\'t enabled it is essential to take care about this problem!!! 
				</li>';
			}else{
				$error .= '<li>It seems that ionCube is not installed on your server. ';
			}
			
			$error .= "
				Please make sure Ioncube is installed. You can download the loaders and 
				the \"Loader Wizard\" (PHP script to help with installation) for free at 
				<a href='' target='_blank'>http://www.ioncube.com/loaders.php</a>
			";
			
			$error .= '</li>';
		}else{
			// check ioncube version
			$ionCubeVersion = getIonCubeVersion();
			if (version_compare($ionCubeVersion, '4.0', '<')){
				$error .= "<li>You Ioncube loader version () is too old. Please install a newer version.
				</li>";
			}
		}
		
		if (!$gettext){
			$warning .= '<li>For some options "gettext" is needed to be installed on the server.
			</li>';
		}
		
		if ($error){
			echo "<h2>Error</h2>";
			echo "<DIV style='padding: 3px; margin: 5px 0px 5px 0px; border: red 5px solid' class='rbox'><ul>$error</ul></DIV>";
		}	
	
		if ($warning){
			echo "<h2>Warning</h2>";
			echo "<DIV style='padding: 3px; margin: 5px 0px 5px 0px; border: orange 5px solid' class='rbox'><ul>$warning</ul></DIV>";
		}		
	
		if ($info){
			echo "<h2>Info</h2>";
			echo "<DIV style='padding: 3px; margin: 5px 0px 5px 0px; border: gray 5px solid' class='rbox'><ul>$info</ul></DIV>";
		}
	}
	
	if (!$error){
		echo "<h2>Congratulations</h2>";
		echo '<DIV style="padding: 10px; margin: 5px 0px 5px 0px; border: green 5px solid" class="rbox">
		AJAX-ZOOM should run on this server. In case You get an error stating, 
		that images could not be found or broken layout, 
		please open /axZm/zoomConfig.inc.php and set these options manually:<br><br>
		<ul>
			<li><a href="http://www.ajax-zoom.com/index.php?cid=docs#installPath" target="_blank">$zoom[\'config\'][\'installPath\']</a><br><br>
				Replace:<br>
				$zoom[\'config\'][\'installPath\'] = $axZmH->installPath();<br><br>
				with:<br>
				$zoom[\'config\'][\'installPath\'] = \'\';<br>
				or if the path to your application is \'/shop\', then set:
				$zoom[\'config\'][\'installPath\'] = \'/shop\';<br><br>
			</li>
			
			<li><a href="http://www.ajax-zoom.com/index.php?cid=docs#fpPP" target="_blank">$zoom[\'config\'][\'fpPP\']</a><br><br>
			Server root path to www directory, 
			e.g. \'/srv/www/htdocs/web123\' or \'/home/yourdomain/public_html\'. 
			Usually it is $_SERVER[\'DOCUMENT_ROOT\']; without slash at the end. 
			Set this option manually if it does not produce correct results!
			</li>
		</ul>
		<br>
		';
		
		if ( ini_get('safe_mode') ){
			echo '<div class="rbox" style="border: 5px solid red; padding: 10px; font-size: 120%">
			Attention - PHP "Safe Mode" is enabled!<br><br>One known issue with safe_mode is that when AJAX-ZOOM creates subfolders and tries to put image tiles for each image in them, 
			the subfolders are created, but because of save_mode turned on it is not allowed to write in them. 
			You could try to solve this problem by changing the owner of AJAX-ZOOM files and folders. Mostly the FTP owner and PHP owner are different.
			</div><br><br>';
		}
		
		echo '<h3>Reading this will save your time and protect your nerves:</h3>';
		echo 'Each example in the download package as well as webshop plugin uses a special configuration options set. 
		Default options in "/axZm/zoomConfig.inc.php" are overridden in "zoomConfigCustom.inc.php" which is included at the bottom of "zoomConfig.inc.php". 
		This happens by passing an extra parameter "example=[some value]" to AJAX-ZOOM directly from examples or plugins over query string. 
		To find out which "example" value is used see sourcecode of the implementation in question or inspect 
		an ajax call to "/axZm/zoomLoad.php" with Firebug or an other developer tool. 
		Thus your specific options set can be found in "zoomConfigCustom.inc.php" after elseif ($_GET[\'example\'] == [some value]){. 
		Please note that the value of example parameter passed over the query string to AJAX-ZOOM does not always correspond to the number of an example 
		found in /examples folder of the download package. 
		<br><br>
		Because AJAX-ZOOM is updated very frequently and its options base grows accordingly, 
		the best practice is to copy options you need to change from "zoomConfig.inc.php" to "zoomConfigCustom.inc.php" 
		after elseif ($_GET[\'example\'] == [some value]). Ofcourse you can create your own [some value] in "zoomConfigCustom.inc.php". 
		By keeping "zoomConfig.inc.php" as it is ($zoom[\'config\'][\'licenceKey\'] and $zoom[\'config\'][\'licenceType\'] can be copied as well 
		at the beginning of zoomConfigCustom.inc.php before the if statement to serve all examples) 
		you will be able to update your customized implementation by simply overwriting all files except "zoomConfigCustom.inc.php" and custom css file. 
		<br><br>
		Important: in the CSS file (/axZm/axZm.css) please do not add any width, height, margin or other "px" values, 
		unless they are already present there and You just want to change them! Since/axZm/axZm.css is updated from time to time as well, 
		you can override CSS classes in /axZm/axZmCustom.css (add !important if needed) and / or in 
		/axZm/styles/default/style.css; The default style is "default". In order to change the template into your own (skin AJAX-ZOOM) 
		simply set <a href="http://www.ajax-zoom.com/index.php?cid=docs#buttonSet" target="_blank">$zoom[\'config\'][\'buttonSet\']</a> option,  
		create a subfolder under /axZm/icons/[yourTemplate] and put your buttons into it (copy them from /axZm/icons/default at first). 
		Also create a new CSS file in /axZm/styles/[yourTemplate]/style.css;

		';
		
		echo "<h3>360 & 3D info:</h3>";
		echo '
		
		<b>VERY IMPORTANT THINGs TO TAKE ACCOUNT OF WITH 360 OR 3D</b><br>
		Every image must have an unique filename!!! This is also the case if images are prepared for completly different 360s or 3D; 
		If all your sourceimages happen to have the same filenames (e.g. spin001.jpg, spin002.jpg, [...], spin036.jpg), 
		you could then prefix each image of a spin e.g. with the product ID or something else logical 
		to ensure this uniqueness, e.g.<br>
		
		/360_source_images/123/123_spin001.jpg, <br>
		/360_source_images/123/123_spin002.jpg, <br>
		/360_source_images/123/123_spin003.jpg, <br>
		[...], <br>
		/360_source_images/123/123_spin036.jpg <br>
		<br><br>
		
		<b>3D (MULTIROW / SPHECICAL)</b><br>
		The only difference between regular 360 spin and multirow is that original images are placed in subfolders of the target folder. 
		E.g. the path along with the example parameter passed to AJAX-ZOOM is "example=spinIpad&3dDir=/pic/zoomVR/nike"; 
		Now if it is 3D (multirow) and not 360, then images of each row are placed in subfolders of the target 3dDir parameter, 
		e.g. /pic/zoomVR/nike/0, /pic/zoomVR/nike/15, /pic/zoomVR/nike/30, /pic/zoomVR/nike/45, 
		/pic/zoomVR/nike/60, /pic/zoomVR/nike/75, /pic/zoomVR/nike/90; 
		It is not important how these subfolders are named and you also do not need to define these subfolder names anywhere. 
		AJAX-ZOOM will instantly detect them and procede all the images in them.<br><br>
		';

		
		echo "<br><br>
		Have fun with AJAX-ZOOM. ";
		
		echo '</DIV>';
		

	}
	
	echo "<h2>Local Examples</h2>";
	echo "<p>Local examples do not contain high-resolution images. 
	Please find these and other examples with high-resolution images at <br>".makeLink('http://www.ajax-zoom.com/index.php?cid=examples')." 
	</p>";

	echo "<DIV class='clearfix'>";
	
	$files = scandir('examples');
	$files = natIndex($files, false);
	$prevNum = '';
	$num = '';
	$n = 0;
	foreach (array_reverse($files, true) as $k=>$file){
		if (strstr($file,'example')){
			if ($num){$prevNum = $num;}
			$num = intval(str_replace(array('example','.php'),'',basename($file)));
			if (basename($file) != 'example1.php' && basename($file) != 'example33_vario.php'){ // do not use example1.php
				$n++;
				if (intval($n/3) == $n/3){
					$rightMargin = '; margin-right: 0;';
				}else{
					$rightMargin = '';
				}
				$bbb = explode('_',basename($file),2); 
				if ($bbb[1]){$bbb[1] = str_replace('.php','',$bbb[1]);}
				
				if ($bbb[1]){$thumbName = 'image-zoom_'.$num.'_'.$bbb[1].'.jpg';}
				else {$thumbName = 'image-zoom_'.$num.'.jpg';}
				
				$exampleThumb = checkRemoteFile('http://www.ajax-zoom.com/pic/layout/'.$thumbName);
				
				echo "<a href='examples/".basename($file)."' target='_blank' style='display: block; float: left' class='rbox'>
				<div style='background-image: url(".$exampleThumb.")".$rightMargin."' class='exampleImg rbox shadow'>
					<div class='exampleHead rbox shadow'>".basename($file)."</div>
				</div>
				</a>";
				 
			}
		}
	}
	echo "</DIV>";
	
	 
	echo '<DIV style="height: 100px">';
	
	 
	echo '</DIV>';
	 
	
echo '<DIV>';
?>
</body>
</html>