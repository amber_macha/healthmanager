<?php
	//require "pdml.php";
?>
<pdml>
<head>
<title>Sample PDML document</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script>

function pdf_report() {
        $.ajax({
            type:"POST",
            url: "http://localhost/pdftest/pdml/example3.php",
           // contentType: "application/json; charset=utf-8",
            success: OnComplete,
            error: OnFail
        });
    }

    function OnComplete(result) { 
        alert(result);
         }

            function OnFail(result) {   
            alert('Request failed');
        }
</script>





</head>
<body>
Text and Markup goes here.<br>
<font face=courier size=10pt>
<cell width=150 height=200% align=center border=1 color=#ff00ff>This is a cell.</cell>
<cell width=150 align=center border=1 color=#ff00ff>This is a cell.</cell>
<cell width=150 align=left border=1 color=#ff00ff>This is a cell.</cell><br>
<cell width=150></cell>
<cell width=150 height=300% align=center border=#ff00ff>This is a cell.</cell>
<cell width=150 align=center border=#ff00ff>This is a cell.</cell><br>
<a href="http://www.yahoo.com/">
<cell width=150 align=center border=#ff00ff>This is a cell.</cell></a>
<cell width=150></cell>
<cell width=150 align=right border=#ff00ff>This is a cell.</cell><br>
<cell width=150 align=center border=#ff00ff>This is a cell.</cell>
<cell width=150></cell>
<cell width=150 align=center border=#ff00ff next=down>This is a cell.</cell>


<circle from="50%,50%" radius=500% yradius=200% fillcolor=#0000ff />
<circle from="50%,50%" radius=400% yradius=200% fillcolor=#00ff00/>
<circle from="50%,50%" radius=300% yradius=200% fillcolor=#ff0000 />
<circle from="50%,50%" radius=200% fillcolor=#ffff00 />
<circle from="50%,50%" radius=150% fillcolor=#00FFff />
<circle from="50%,50%" radius=100% fillcolor=#ff00ff />
<circle from="50%,50%" radius=50% fillcolor=#000000 />
</font>
<br height=2cm>
<column count=3 height=5cm break=line>
<multicell width=90%>
The year 1866 was marked by a bizarre development, an unexplained and downright inexplicable phenomenon that surely no one has forgotten. Without getting into those rumors that upset civilians in the seaports and deranged the public mind even far inland, it must be said that professional seamen were especially alarmed. Traders, shipowners, captains of vessels, skippers, and master mariners from Europe and America, naval officers from every country, and at their heels the various national governments on these two continents, were all extremely disturbed by the business.
In essence, over a period of time several ships had encountered "an enormous thing" at sea, a long spindle-shaped object, sometimes giving off a phosphorescent glow, infinitely bigger and faster than any whale.
The relevant data on this apparition, as recorded in various logbooks, agreed pretty closely as to the structure of the object or creature in question.
</multicell>
</column>
<a href="http://localhost/pdftest/pdml/example2/"><cell width=150 align=center border=#ff00ff>make pdf</cell></a>
<cell width=150 align=center border=#ff00ff> <button onclick="pdf_report()">Click Save Me</button></Button></cell>

</body>
</pdml>
